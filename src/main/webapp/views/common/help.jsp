<!DOCTYPE html>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=7,IE=8,IE=9" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Help</title>

<script language="javascript" type="text/javascript">
	
</script>

</head>

<body>
	<jsp:include page="/views/header.jsp"></jsp:include>
	<div class="container-fluid" style="padding-left: 10px;padding-top: 15px;" id="contentContainer">
		<h2 style="color: #ea0a8e;">Help</h2>
		<p> For help or effect use of this tool, please refer to this <a href="${pageContext.request.contextPath}/resources/helpDoc/EDAT_TOOL_QUICK _START GUIDE_RO v1.0_FINAL _R.pdf" target="_blank">document.</a></p>
	</div>
	<jsp:include page="/views/footer.jsp"></jsp:include>
</body>

</html>