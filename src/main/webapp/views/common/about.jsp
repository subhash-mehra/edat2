<!DOCTYPE html>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=7,IE=8,IE=9" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>About EDAT</title>

<script language="javascript" type="text/javascript">
	
</script>

</head>

<body>
	
	<jsp:include page="/views/header.jsp"></jsp:include>
	<div class="container-fluid" style="padding-left: 10px;padding-top: 15px;" id="contentContainer">
		<h2 style="color: #ea0a8e;">About</h2>
		<p>Engineering Data Access for Testing (EDAT) tool provides a Graphical User Interface access to most of the systems in the Engineering LAB, where provisioned test data can be validated.</p>
	
	</div>
	<jsp:include page="/views/footer.jsp"></jsp:include>
</body>

</html>