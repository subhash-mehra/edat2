<div style="width:480px" class="iam-main-panel hideOthers">
	<label class="label1" id="clientid_lbl">Client ID</label>
	<select	class='form-control searchTypeClientId' onchange="onSelectClientID(this)">
	</select>
<!-- 	<div class="iamClientId_others hideOthers"> -->
		
<!-- 	</div> -->

	<input type="text" class="form-control iam_search_otherTxt hideOthers"></input>
	<label class="label1 iamSecretKeyLbl" style="margin-top:5px;margin-bottom:5px">Secret Key</label>
	<input type="text" style="margin-top:5px;margin-bottom:5px" class="form-control iamSecretKey"></input>
	<br><br>	
	<div class="iamSection">
		<label class="label1">Scope Type<span style="color: red;">*</span></label>
		<select class="scopeList_iamsearch iam-scope-grant" multiple="multiple" onchange="onSelectScopes(this)">
			<option>TMO_ID_profile</option>
			<option>Associated_lines</option>
			<option>Line_addresses</option>
			<option>Associated_customers</option>
			<option>Customer_addresses</option>
		</select><br>
		<label class="label1">Grant Type<span style="color: red;">*</span></label>
		<select	class="groupTypeList_iamsearch iam-scope-grant" multiple="multiple" onchange="onSelectGrantType(this)">
			<option>Authorization_code</option>
			<option>Refresh_token</option>
			<option>Client_credentials</option>
		</select>
	</div>
</div>