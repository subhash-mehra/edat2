<!DOCTYPE html>
<%@page import="com.tmobile.session.SessionConstants"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="com.tmobile.edat.admin.model.UserAccess"%>
<%@page import="com.tmobile.session.SessionConstants"%>
<%@page import="com.tmobile.edat.admin.model.ServerConfig"%>
<%@page import="java.util.List"%>

<html>
<title>EDAT Search Tool</title>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="refresh" content="${pageContext.session.maxInactiveInterval}; url=logout" />


<link href="<c:url value="/resources/css/bootstrap/bootstrap-table.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value='/resources/css/bootstrap/bootstrap-select.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/jquery-ui.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/dataTables.jqueryui.css'/>" rel="stylesheet">
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/core/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/core/bootstrap.min.js" />"></script>
<script type="text/javascript" src="resources/js/constants.js"></script>
<script type="text/javascript" src="resources/js/admin.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/tabController.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/common.js" />"></script>

<script type="text/javascript" src="<c:url value='/resources/lib/jquery-ui.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/lib/jquery.dataTables.min.js'/>"></script>
 <% if(session.getAttribute("SessionConstants")!=null){%>
 <c:set var="defaultServerSessionConstant" value='<%=((SessionConstants)session.getAttribute("SessionConstants")).getDEFAULT_SERVER() %>' scope="session"/>
 <%
	 }else{%>
	 <c:set var="defaultServerSessionConstant" value="0" scope="session"/>
	 
	 <%
		 }%>
<script type="text/javascript">
	window.history.forward();
</script>

</head>
<%
	UserAccess userAccess = null;
	userAccess = (UserAccess) session.getAttribute("userAccess");
%>
<form name="header" id="header" onsubmit="return goToModule('admin')" method="get">


		<div class="navbar navbar-default header" role="navigation" style="margin-bottom:5px">
		<div class="container-fluid">
			<a class="navbar-brand" href="/edat/usd" style="height:auto"><img src="/edat/resources/images/T-MobileLogo.png" /></a>
			<a class="navbar-brand" href="/edat/usd" style="height:auto"><img src="/edat/resources/images/T-MobileHead.png" id="headImg" style="position: absolute; right: 50%; top: 20px;"/></a>
			

			
			<ul class="nav navbar-nav navbar-right" style="margin:18px 0 0">
				
				<!-- <li><a href="/edat/about">About</a></li>
				<li><a href="/edat/help">Help</a></li>
				<li><a href="/edat/support">Support</a></li>
				
				 -->
				
				<li>
					<p class="navbar-text" style="margin: 6px -85px;">Environments:</p>
					<select name="hdr_server_config" id="hdr_server_config" class="fieldset small" style="width:auto; margin-right: 10px;">
					<% if(session.getAttribute("serverConfigList")==null || ((List<ServerConfig>)session.getAttribute("serverConfigList")).size()==0){ %> <option value="0">Default Server</option> <%} %>
					 <c:forEach var="server" items="${sessionScope.serverConfigList}"> 
					 <option value="<c:out value="${server.id}"/>" <c:if test = "${server.id == sessionScope.defaultServerSessionConstant}"> selected </c:if> ><c:out value="${server.serverName}"/></option>
		           	</c:forEach>
					</select>
				</li>
				<li>
					<div class="dropdown">
					  <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
					    <%=userAccess != null ? (String) " "
						+ userAccess.getFirstName() + " "
						+ userAccess.getLastName() : " - Unkonwn"%>
					    <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
					    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:showPasswordWindow('password')">Change Password</a></li>
					    <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:goToModule('logout')">Logout</a></li>
					  </ul>
					</div>
				</li>
				
				<li style="padding-left: 5px;">
					<div class="dropdown">
					  <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-expanded="true">
					    About
					    <span class="caret"></span>
					  </button> 
					  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
					    <li role="presentation"><a role="menuitem" tabindex="-1" href="/edat/about">About</a></li>
					    <li role="presentation"><a role="menuitem" tabindex="-1" href="/edat/help" >Help</a></li>
					     <li role="presentation"><a role="menuitem" tabindex="-1" href="/edat/support">Support</a></li>
					  </ul>
					</div>
				</li>
				
			</ul>
		</div>
	</div>

	<div style="width: 100%; background-color: #f2f8fe;">
		<div>
			<ul class="nav nav-pills" role="tablist" id="mainNav"
				style="height: 100%">
				<c:if
					test='${(fn:contains(sessionScope.roleList, "R") || fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
					<li><a href="javascript:goToModule('usd')" id="usdData">USD
							Data</a></li>
					<li><a href="javascript:goToModule('cntb')" id="cntb">CNTDB</a></li>
					<li><a href="javascript:goToModule('msg')" id="messagingData">Msg. Data</a></li>
				</c:if>
				<c:if
					test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
					<li><a href="javascript:goToModule('dus')" id="dusUpdate">Update DUS</a></li>
				</c:if>
				<c:if
					test='${(fn:contains(sessionScope.roleList, "GF") || fn:contains(sessionScope.roleList, "GFR") || fn:contains(sessionScope.roleList, "GFW") || fn:contains(sessionScope.roleList, "GFRW") ||  fn:contains(sessionScope.roleList, "ADMIN"))}'>

					<li><a href="javascript:goToModule('gflex')" id="gflex">GFlex Data</a></li>
				</c:if>
				<c:if
					test='${(fn:contains(sessionScope.roleList, "R") || fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>

					<li><a href="javascript:goToModule('inSdp')" id="inSdp">IN/SDP
							Data</a></li>
				</c:if>
				<c:if
					test='${(fn:contains(sessionScope.roleList, "R") || fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
				<li><a href="javascript:goToModule('otaTool')" id="otaTool">OTA Data</a></li>
				</c:if>
				<c:if test='${fn:contains(sessionScope.roleList, "ADMIN")}'>
					<li><a href="javascript:goToModule('admin')" id="admin">Admin</a></li>

				</c:if>

			</ul>
		</div>
	</div>
	<script language="javascript" type="text/javascript">
		window.history.forward();
		
		/*$(window).scroll(function(){
		    if ($(window).scrollTop() >= 80) {
			       $('div.header+div').addClass('fixed-nav');
			      //$('.col-lg-2 > *').addClass('fixed-side-nav');
		    }
		    else {
			       $('div.header+div').removeClass('fixed-nav');
			       //$('.col-lg-2  > *').removeClass('fixed-side-nav');
		    }
		});*/
		
		$(document).ready(resizeHandler);
		
		$(window).resize(resizeHandler);
		function resizeHandler(){
			var h = $(window).height();
		    $("#contentMain > .col-lg-2, #contentMain > .col-lg-10, #contentMain > .col-lg-12, #contentContainer").each(function(i,e){
		    	$(e).height(h - $(e).offset().top - 20);
		    });
		}
		
		
	</script>


	<input type="hidden" name="csrfPreventionSalt" id="csrfPreventionSalt1"
		value="<%=request.getAttribute("csrfPreventionSalt")%>" />
</form>
