<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<c:if test='${!(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
<c:redirect url="/accessError"></c:redirect>
</c:if>
<jsp:include page="/views/header.jsp"></jsp:include>

<script type="text/javascript"
	src="<c:url value="/resources/js/dusUpdate.js" />"></script>

<script type="text/javascript">
document.title="Update Dus";
</script>
<body>

<div class="container-fluid" style="padding-left: 10px;padding-top: 15px;" id="contentContainer">
<div id="contentJspId" class="contentJspDiv" >
	<div class="panel panel-primary">
		<div class="panel-heading" id="panelHeading">Search DUS</div>
		<div class="panel-body">
			<h5>
				<b><i>Note</i>:Required fields are indicated with the red
					asterisk(<span style="color: red;">*</span>).</b>
			</h5>

			<div id="searchBox" class="input-group">
				<label style="float: left;" class="label1">Search Type<span
					style="color: red;">*</span></label> <select id="searchType"
					class="form-control" onchange='clearText()'>
					<option value="MSISDN">MSISDN(1)</option>
					<option value="BAN">BAN</option>
				</select> <input type="text" id="searchText" class="form-control">
				<button id="search" class="btn btn-primary">Search</button>
			</div>
		</div>
	</div>
	<div id="loading"></div>
	
	<div class="panel panel-primary" id="msg" style="display: none;">
					<div class="panel-heading" ></div>
					<div class="panel-body" >Customer not found!</div>
				</div>
	
</div>


	
<div id="result">
	<div class="panel panel-primary" id="dusMsisdnResult" style="display: none;">
		<div class="panel-heading">Update DUS  : amfCounters</div>
		<div class="panel-body" >
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>CounterId</th>
						<th>Counter Value</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	
<div class="panel panel-primary" id="extraInfo"
		style="display: none;">
		<div class="panel-heading" id="extraInfoHeading"></div>
		<div class="panel-body" id="extraInfoBody"></div>
</div>
	<div id="banResult" style="display: none" class="row">
						<ul class="col-lg-2 sb">
						</ul>
						<div class="col-lg-10">
							<div class="panel panel-primary">
								<div class="panel-heading">Counters:</div>
								<div class="panel-body">
									<table id="counters" class="table">
										<tr>
											<th>Counter ID</th>
											<th>Counter Value</th>
										</tr>
									</table>
								</div>
							</div>
							<div class="panel panel-primary">
								<div class="panel-heading">Active Offers:</div>
								<div class="panel-body">
									<table id="offers" class="table">
										<tr>
											<th>Offers</th>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
							
		</div>
	
		
</div>
	
	<input type="hidden" name="csrfPreventionSalt"
		value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
</body>

<jsp:include page="/views/footer.jsp"></jsp:include>
</html>