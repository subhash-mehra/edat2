<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<jsp:include page="/views/header.jsp"></jsp:include>
<script type="text/javascript">
document.title="CNTDB Delete";
</script>
<script src="${pageContext.request.contextPath}/resources/js/cntb.js"></script>
<%
	Map<String,Object>dataMap=new HashMap<String,Object>();
String message="";
String failInput="";
String failuerCount="";
String successCount="";
if(request.getAttribute("dataMap")!=null)
	{
 		dataMap=(Map)request.getAttribute("dataMap");
	}
	if(dataMap.get("message")!=null)
	{
	message=(String)dataMap.get("message");
	}
	if(dataMap.get("failInput")!=null)
	{
		failInput=(String)dataMap.get("failInput");
	}
	if(dataMap.get("failuerCount")!=null)
	{
		failuerCount=(String)dataMap.get("failuerCount");
	}
	
	if(dataMap.get("successCount")!=null)
	{
		successCount=(String)dataMap.get("successCount");
	}
%>
<body>




	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">
		<script type="text/javascript">
			$(document).ready(function() {
				$("ul.sb #cntbDelete").parent().addClass("selected");
			});

			$('#loading').empty();
		</script>
		<form name="buttonForm" id="buttonForm" method="get" class="col-lg-2"
			style="padding: 0 0 0 15px;">
			<ul class="sb" style="width: 100%">
				<li><a id="cntbSearch" href="javascript:cntbShow('search')">CNTDB Search</a></li>
				<c:if test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "BLRW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
				<li><a id="cntbAdd" href="javascript:cntbShow('add')">CNTDB Add</a></li>
				<li><a id="cntbDelete" href="javascript:cntbShow('delete')">CNTDB Delete</a></li>
				</c:if>
			</ul>

		</form>
		
		<div id="contentJspId" class="contentJspDiv col-lg-10"
			style="height: 0px;">

			<div class="panel panel-primary">
				<div class="panel-heading" id="panelHeading">CNTDB Delete:</div>
				<div class="panel-body">
					<div id="messageHeading"
			style="font-weight: bold; font-size: medium; color: red;">
						<%=message%><br>
						 <%if(failInput!=""){ %>
						Failure input are<%=" "+failInput %><br>
						<%} %>
						<%if(successCount!=""){ %>
						Success Input count--<%=" "+successCount %><br>
						<%} %>
						<%if(failuerCount!=""){ %>
						Failure Input count--<%=" "+failuerCount %><br>
						<%} %>
		</div>
					<div class="row" style="padding-left: 15px;">
					<h5><b><i>Note</i>:Please enter MSISDN or IMSI or both.</b></h5>
						<div class="col-md-6" id="searchBox">
							<label class="label1">MSISDN(1)</label>
							<input type="text" id="msisdn" class="fieldset" style="margin-bottom: 0px;">
							<label id="tacHint" style="font-weight: 100;margin-left: 202px;margin-bottom: 10px;"><span style="color: red;">(Hint:msisdn can be of 10 or 14 or 15 digits excluding 1.)</span></label>
							<label class="label1">IMSI</label>
							<input type="text" id="imsi" class="fieldset">
							<button id="delete" class="btn btn-primary">Delete</button>
						</div>
						<div id="batchSearchBox" class="col-md-6">
							<form id="myForm" enctype="multipart/form-data"
								action="batchDelete" method="post"
								onsubmit="return Checkfiles();">
								<c:if
									test='${(fn:contains(sessionScope.roleList, "ADMIN") || fn:contains(sessionScope.roleList, "BLR") || fn:contains(sessionScope.roleList, "BLRW"))}'>
									<fieldset style="height: 171px; margin-top: -8px;">
										<legend>Batch Delete:</legend>
										<input type="file" name="file" id="fileselect"></input>
										<button type="submit" class="btn btn-primary"
											style="margin-top: 10px;">Batch Delete</button>
									</fieldset>
								</c:if>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div id="loading" style="margin-left: -180px; margin-top: -137px; display: inline;" ></div>
		</div>

	</div>





	<input type="hidden" name="csrfPreventionSalt"
		value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
	<input type="hidden" id="searchTextVal" value="delete" />
</body>
<jsp:include page="/views/footer.jsp"></jsp:include>
</html>