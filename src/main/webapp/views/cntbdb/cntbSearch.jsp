<jsp:include page="/views/header.jsp"></jsp:include>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<script type="text/javascript">
document.title="CNTDB Search";
</script>
<script	src="${pageContext.request.contextPath}/resources/js/cntb.js"></script>
<body>
	<div class="row" style="height:25px; background-color: #eee"></div>
	<div class="row" id="contentMain">	
	<script type="text/javascript">
		$(document).ready(function(){
		$("ul.sb #cntbSearch").parent().addClass("selected");
		$('#loading').empty();
	});
</script>
	<form name="buttonForm" id="buttonForm" method="get"  class="col-lg-2" style="padding: 0 0 0 15px;">
			<ul class="sb" style="width:100%">
			<li><a id="cntbSearch" href="javascript:cntbShow('search')" >CNTDB Search</a></li>
			<c:if test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "BLRW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>	
				        <li><a id="cntbAdd" href="javascript:cntbShow('add')">CNTDB Add</a></li>
				<li><a id="cntbDelete" href="javascript:cntbShow('delete')">CNTDB Delete</a></li>
				</c:if>
		</ul>
	
	</form>
	
	
	<div id="contentJspId" class="contentJspDiv col-lg-10" style="height: 0px;">
		
	<div class="panel panel-primary" >
	  <div class="panel-heading" id="panelHeading">CNTDB Search:</div>
	   <div class="panel-body">
	   <div id="messageHeading"	style="font-weight: bold; font-size: medium; color: red;">
	   
						<%
						Map<String,Object>dataMap=new HashMap<String,Object>();
						List<String> msisdnAttributeList = new ArrayList<String>();
						List<String> imsiAttributeList = new ArrayList<String>();
						String failMsisdn="";
						String successCount="";
						String failuerCount="";
						if(request.getAttribute("dataMap")!=null)
						{
					 		dataMap=(Map)request.getAttribute("dataMap");
						}
						if(dataMap.get("failMsisdn")!=null)
			 			{
			 				failMsisdn=(String)dataMap.get("failMsisdn");
			 			}
			 			if(dataMap.get("successCount")!=null)
			 			{
			 				successCount=(String)dataMap.get("successCount");
			 				System.out.println("================================================"+successCount);
			 			}
			 			if(dataMap.get("failuerCount")!=null)
			 			{
			 				failuerCount=(String)dataMap.get("failuerCount");
			 			}
			 			
						if(dataMap.get("batchSearchFlag")!=null)
						{
					 		if(dataMap.get("batchSearchFlag").equals("true"))
					 		{ %>
						 <%if(failMsisdn!=""){ %>
						Fail input are<%=" "+failMsisdn %><br>
						<%} %>
						<%if(successCount!=""){ %>
						Success Input count--<%=" "+successCount %><br>
						<%} %>
						<%if(failuerCount!=""){ %>
						Fail Input count--<%=" "+failuerCount %><br>
						<%} }}%> 
					</div>
	<div  class="row" style="padding-left: 15px;">
		
		
		<h5><b><i>Note</i>:Please enter MSISDN or IMSI or both.</b></h5>
					<div class="col-md-6" id="searchBox">
						<label class="label1">MSISDN(1)</label>
						<input type="text" id="msisdn"  class="fieldset" style="margin-bottom: 0px;" oncopy="return true" onpaste="return true" alt="a cat">
						<label id="tacHint" style="font-weight: 100;margin-left: 198px;margin-bottom: 10px;"><span style="color: red;">(Hint:msisdn can be of 10 or 14 or 15 digits excluding 1.)</span></label>
						<label class="label1">IMSI</label>
						<input type="text" id="imsi" class="fieldset" oncopy="return true" onpaste="return true">
						<button id="search" class="btn btn-primary"> Search</button>
						
					</div>
					<div id="batchSearchBox" class="col-md-6" >
						<form id="frmImport" enctype="multipart/form-data" method="post" action="batchSearch" onsubmit="return Checkfiles();">	
						<c:if test='${(fn:contains(sessionScope.roleList, "ADMIN") || fn:contains(sessionScope.roleList, "BLR") || fn:contains(sessionScope.roleList, "BLRW"))}'>	
						<fieldset style="height: 172px;margin-top: -8px;">
							<legend>Batch Search:</legend>
	    					<input type="file" name="importData" id="fileselect"></input>
	    					<input type="submit" id="btnImport" value="Batch Search" class="btn btn-primary" style="margin-top: 10px;">    					
  						</fieldset>
  						</c:if>
						</form>
					</div>
	</div>	
	</div>
	</div>
	
	<%
	
	if(request.getAttribute("dataMap")!=null)
	{
 		dataMap=(Map)request.getAttribute("dataMap");
	}
		if(dataMap.get("batchSearchFlag")!=null)
		{
 		if(dataMap.get("batchSearchFlag").equals("true"))
 		{
 			if(dataMap.get("msisdnAttributeList")!=null)
 			{
 				msisdnAttributeList=(List)dataMap.get("msisdnAttributeList");
 			}
 			if(dataMap.get("imsiAttributeList")!=null)
 			{
 				imsiAttributeList=(List)dataMap.get("imsiAttributeList");
 			}
 			
 			
	 %>
	 
	 <div id="msisdnResultDiv" class="panel panel-primary" style="visibility: hidden;">
					<div class="panel-heading" id="msisdnResultDivContentHeading"></div>
					<div id="msisdnResultDivContentBody" class="panel-body"></div>
				</div>	
				
				
				<div id="imsiResultDiv" class="panel panel-primary" style="visibility: hidden;">
					<div class="panel-heading" id="imsiResultDivContentHeading"></div>
					<div id="imsiResultDivContentBody" class="panel-body"></div>
				</div>	
				
				
	
	<script type="text/javascript">
	geberateTableForMSISDNBatch('<%=msisdnAttributeList%>');
	 </script>
	 
	 
	 <script type="text/javascript">
	 geberateTableForIMSIBatch('<%=imsiAttributeList%>');
	 </script>

	<%}}else{ %>
	
		<%} %>
		
		
	
		
		<div id="loading" style="margin-left: -128px; margin-top: -150px; display: inline;" ></div>

	<!-- 	<div id="nap-tab" style="padding: 10px;">
			<div class="panel panel-primary" style="display: none;" id="panelDiv">
				<div class="panel-heading" id="cnbtContentHeading" style="display: none;"></div>
				<div id="cnbtContentBody" class="panel-body" style="display: none;"></div>
				<div class="Clear"></div>
				<div class="panel-heading" id="cnbtIMSIContentHeading" style="display: none;"></div>
				<div id="cnbtIMSIContentBody" class="panel-body" style="display: none;"></div>
			</div>
			
			
		</div> -->
		
				<div id="msisdnResultDiv" class="panel panel-primary" style="visibility: hidden;">
					<div class="panel-heading" id="msisdnResultDivContentHeading"></div>
					<div id="msisdnResultDivContentBody" class="panel-body"></div>
				</div>	
				
				
				<div id="imsiResultDiv" class="panel panel-primary" style="visibility: hidden;">
					<div class="panel-heading" id="imsiResultDivContentHeading"></div>
					<div id="imsiResultDivContentBody" class="panel-body"></div>
				</div>	

		
			
		
		
		</div>
		
		
	</div>
	
	

	<div id="msg" style="display:none">
			No Record found.
	</div>
	<input type="hidden" name="csrfPreventionSalt"
			value="<c:out value='<%=request.getAttribute("csrfPreventionSalt") %>'/>" />
			<input type="hidden" id="searchTextVal"
			value="<c:out value='search'/>" />
</body>

<jsp:include page="/views/footer.jsp"></jsp:include>
</html>