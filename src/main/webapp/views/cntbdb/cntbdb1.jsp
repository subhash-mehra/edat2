<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<jsp:include page="/views/header.jsp"></jsp:include>
<jsp:include page="/views/adminJsp/sessionExpire.jsp"></jsp:include>

<body>



	<script type="text/javascript">
		function clearText() {
			document.getElementById('searchText').value = '';
		}

		function empltyCheck() {
			var searchBox = document.getElementById('searchText').value;
			if (searchBox.trim().length == 0) {
				alert('Please enter the value');
				return false;
			} else {
				return true;
			}
		}

		var uid;
		var jsonDusRes;

		$(document)
				.ready(
						function() {

							$("#cntb").parent().addClass("active");

							$('#search')
									.click(
											function(event) {
												if (!empltyCheck()) {
													return;
												}

												var searchText = $(
														"#searchText").val();
												var urlAction;
												var searchJson = {};

												if ($("#searchType").val() == "MSISDN") {

													urlAction = 'msisdn';
													searchJson.msisdn = searchText;
													//searchJson.ban = "";

												} else {
													urlAction = 'imsi';
													searchJson.ban = searchText;
													searchJson.msisdn = "";
												}

												$
														.ajax({
															url : '${pageContext.request.contextPath}/cntb/'
																	+ urlAction,

															data : JSON
																	.stringify(searchJson),
															type : "POST",

															beforeSend : function(
																	xhr) {
																xhr
																		.setRequestHeader(
																				"Accept",
																				"application/json");
																xhr
																		.setRequestHeader(
																				"Content-Type",
																				"application/json");
															},
															success : function(
																	result) {
																jsonDusRes = result;
																genrateUI(result);
																var counters = jsonDusRes.counters;
																uid = jsonDusRes.uid;
																if (urlAction == "msisdn") {

																	$(
																			'#msisdnResult tbody tr')
																			.show();
																	$(
																			"#imsiResult")
																			.hide();
																	$("#msg")
																			.hide();
																	clearText();
																	//alert(counters.length);

																} else if (urlAction == "imsi") {

																	if (jsonDusRes.length > 0) {
																		$(
																				"#msisdnResult")
																				.hide();
																		$(
																				"#imsiResult")
																				.show();
																		$(
																				"#msg")
																				.hide();
																	}
																}
															}
														});
											});

						});

		function genrateUI(result) {

			console.log(result);
			var htmlStr = "<div class='panel panel-primary'><div class='panel-heading'>Accounts details  & Balance and date</div><div class='panel-body'>"

			htmlStr += "<table class='table' data-toggle='table' border='1' data-height='180' id='usdNapProfileTable'>";
			htmlStr += "<tr>";
			htmlStr += "<th>" + "UID" + "</th>";
			htmlStr += "<th>" + "CID" + "</th>";
			htmlStr += "<th>" + "IMSI" + "</th>";
			htmlStr += "<th>" + "MSISDN" + "</th>";
			htmlStr += "</tr>";
			htmlStr += "<tr>";
			for ( var keys in result) {
				if (keys != 'statusCode' && keys != 'applicationError') {

					htmlStr += "<td>" + result[keys] + "</td>";

				}
			}

			htmlStr += "</tr> ";

			htmlStr += "</table></div></div>";

			$("#msisdnResult").empty();
			$("#msisdnResult").html(htmlStr);
			$("#searchText").empty();

		}
	</script>

	<div id="searchBox" class="input-group">
		<span class="input-group-addon">Search:</span> <select id="searchType"
			class="form-control" onchange='clearText()'><option
				value="MSISDN">MSISDN</option>
			<option value="IMSI">IMSI</option></select> <input type="text" id="searchText"
			class="form-control">
		<button id="search" class="btn btn-default">Search</button>
	</div>

	<div id="msisdnResult"></div>

	<div id="imsiResult"></div>



	<div id="msg" style="display: none">No Record found.</div>
	<input type="hidden" name="csrfPreventionSalt"
		value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
</body>
</html>