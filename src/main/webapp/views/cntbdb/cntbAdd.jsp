<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<jsp:include page="/views/header.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/resources/js/cntb.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap-filestyle.min.js" />"></script>
<script type="text/javascript">
document.title="CNTDB Add";
</script>
<%
	Map<String,Object>dataMap=new HashMap<String,Object>();

String message="";
int failuerCount=0;
int successCount=0;
String failuerMsg="";

if(request.getAttribute("dataMap")!=null)
	{
 		dataMap=(Map)request.getAttribute("dataMap");
	}
	if(dataMap.get("message")!=null)
	{
	message=(String)dataMap.get("message");
	}
	
	if(dataMap.get("failuerCount")!=null)
	{
		failuerCount=(Integer)dataMap.get("failuerCount");
	}
	if(dataMap.get("successCount")!=null)
	{
		successCount=(Integer)dataMap.get("successCount");
	}
	if(dataMap.get("failuerMsg")!=null)
	{
		failuerMsg=(String)dataMap.get("failuerMsg");
	}
	
%>
<body>




	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">
		<script type="text/javascript">
			$(document).ready(function() {
				$("ul.sb #cntbAdd").parent().addClass("selected");
			});
			$('#loading').empty();
		</script>
		<form name="buttonForm" id="buttonForm" method="get" class="col-lg-2"
			style="padding: 0 0 0 15px;">
			<ul class="sb" style="width: 100%">
				<li><a id="cntbSearch" href="javascript:cntbShow('search')">CNTDB Search</a></li>
				<c:if test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "BLRW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
				<li><a id="cntbAdd" href="javascript:cntbShow('add')">CNTDB Add</a></li>
				<li><a id="cntbDelete" href="javascript:cntbShow('delete')">CNTDB Delete</a></li>
				</c:if>
			</ul>

		</form>



	<div id="contentJspId" class="contentJspDiv col-lg-10" style="height: 0px;">
	<div class="panel panel-primary" >
	<div class="panel-heading" id="panelHeading">CNTDB Add:</div>
	   <div class="panel-body">
					<div id="messageHeading"
						style="font-weight: bold; font-size: medium; color: red;">
						<%=message%><br>
						<%if(successCount!=0){ %>
						<%="Success Input:-"+successCount%><br>
						<%} %>
						<%if(failuerCount!=0){ %>
						<%="Failure Input:-"+failuerCount%><br>
						<%} %>
						
						<%if(failuerMsg!=""){ %>
						<%=failuerMsg%><br>
						<%} %>
						
					</div>


					<div class="row" style="padding-left: 15px;">
					
						<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5> 
						<div class="col-md-6" id="searchBox">
							<label class="label1">MSISDN(1)<span style="color: red;">*</span></label>
							<input type="text" id="msisdn" class="fieldset" style="margin-bottom: 0px;"> 
							<label id="tacHint" style="font-weight: 100;margin-left: 198px;margin-bottom: 10px;"><span style="color: red;">(Hint:msisdn can be of 10 or 14 or 15 digits excluding 1.)</span></label>
							<label class="label1">IMSI<span style="color: red;">*</span></label>
								 <input	type="text" id="imsi" class="fieldset">
								 <label class="label1">Operator ID</label>
								 <input type="text" id="custId"	class="fieldset">
								 <label class="label1">ENC KEY</label>
								 <input type="text" id="encKey"	class="fieldset" style="width: 270px;" value="5E281D8EA8FF8EAED53C27E2B0718687">
								 <label class="label1">ICC ID</label>
								 <input	type="text" id="accId" class="fieldset" value="1">
								 <label	class="label1">ALOG ID</label> 
								 <input type="text" id="alogId"	class="fieldset" value="9">
								 <label class="label1">KDB ID</label>
								 <input	type="text" id="kdbId" class="fieldset" value="61"> 
								 <label class="label1">acsub ID</label>
								<input type="text" id="acsub" class="fieldset" value="2">
								<button id="add" class="btn btn-primary">Add</button>
							</div>

						<div id="batchSearchBox" class="col-md-6">
							<form id="myForm" enctype="multipart/form-data" action="batchAdd"
								onsubmit="return Checkfiles();" method="post">
								<c:if
									test='${(fn:contains(sessionScope.roleList, "ADMIN") || fn:contains(sessionScope.roleList, "BLR") || fn:contains(sessionScope.roleList, "BLRW"))}'>
									<fieldset style="height: 447px; margin-top: -8px;">
										<legend>Batch Add:</legend>
										<input type="file" class="filestyle"
											data-buttonName="btn-primary" name="file" id="fileselect"></input>
										<button type="submit" class="btn btn-primary"
											style="margin-top: 10px;">Batch Add</button>
									</fieldset>
								</c:if>
							</form>
						</div>
					</div>
				</div>


			</div>
		
	<div id="loading" style="margin-left: -128px; margin-top: -331px; display: inline;" ></div>
	
	
		</div>
		</div>
	<input type="hidden" name="csrfPreventionSalt"
		value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
	<input type="hidden" id="searchTextVal" value="add" />"
</body>

<jsp:include page="/views/footer.jsp"></jsp:include>
</html>