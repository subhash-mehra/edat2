<!DOCTYPE html>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<script type="text/javascript" src="resources/js/admin.js"></script>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Access Denied</title>
<!-- Bootstrap -->
<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/login.css" rel="stylesheet">
<script type="text/javascript">
	window.history.forward();
</script>
</head>
<div class="login-box">
	    <img src="/edat/resources/images/T-MobileHead.png" style="margin-top:-4px; float:right;width:100px;"/>
	     
	<form name="validUser" method="get" id="validUser">
	
		<h3 class="text-center">
			<img src="/edat/resources/images/T-MobileLogo.png" />
		</h3>
		<h3 style="text-align: center; font-size: 20px;">Oops!! You do not have permission to access this page!!</h3>
		<div class="Clear"></div>
		<br />
		<button type="submit" name="submit" id="submit"
			onclick="loginForValidUser();" label="Login"
			class="btn btn-primary btn-md" style="margin-left: 218px;">Login</button>
	</form>
</div>

</body>
</html>