<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn" %>
<jsp:include page="/views/header.jsp"></jsp:include>
<script type="text/javascript"
	src="<c:url value="/resources/js/usdData.js" />"></script>
	<script type="text/javascript" src="<c:url value="http://jqueryvalidation.org/files/dist/jquery.validate.min.js" />"></script>
	
<script type="text/javascript">
document.title="NAP";
/* $(document).ready(function(){
	$("#usdTabList>li:nth-child(7)").click(function(){
		$("#searchEirTabs li").slideToggle(500);
	});
}); */


$(document).ready(function(){
	$("#eir").click(function(){
		$("#searchEirTabs li").slideToggle(500);
		$(this).toggleClass("expanded");
	});
});


</script>
<style>
 .display{
   table-layout: fixed;
    word-wrap:break-word; 
    }
</style>
	
<body>
	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">

		<div class="col-lg-2" style="padding: 0 0 0 15px;">
			<div class="relative-tab-container">

				<ul class="sb" id="usdTabList">
					<li id="nap"><a class="usd" href="#">NAP</a></li>
					<li id="dus"><a class="usd" href="#">DUS</a></li>
					<li id="pcrf"><a class="usd" href="#">PCRF</a></li>
					<li id="uma"><a class="usd" href="#">UMA</a></li>
					<li id="hlr"><a class="usd" href="#">HLR</a></li>
					<li id="hss"><a class="usd" href="#">HSS</a></li>
					
					 <li id="eir" ><a class="usd" href="#">EIR</a><span class="icon-expand"></span>
					<!--  <img src="/edat/resources/images/Arrow-Icon-small.jpg" id="arrowdown"
						style="float: right; margin-top: -32px; margin-right:2px;" /> --></li>
					 <div id="searchEirTabs">
						<li id="searchEir"><a class="usd" href="#">Search EIR</a></li>
						<c:if test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>	
						<li id="addEir"><a class="usd" href="#">Add IMSI-IMEI:</a></li>
						<li id="unlockEir"><a class="usd" href="#"> IMSI-IMEI
								lock/unlock</a></li>
								</c:if>
						<li id="searchTac"><a class="usd" href="#">Search TAC</a></li>
						<c:if test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>	
						<li id="addtac"><a class="usd" href="#">Add TAC</a></li>
<!-- 						<li id="enableMMS"><a class="usd" href="#">Enable MMS</a></li> -->
						<li id="updatetac"><a class="usd" href="#">Update TAC</a></li>
						</c:if>
					 </div> 
				</ul>

			</div>
		</div>
		<div class="contentJspDiv col-lg-10">
			<div class="panel panel-primary">
				<div class="panel-heading" id="panelHeading">USD Data</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div id="searchBox" class="input-grp">
					<!-- <div id="usdSearchDropDown"> -->
					<label class="label1" id="searchLbl">Search Type <span style="color: red;">*</span> </label>
					
						
						<!--  <div id="dropDownSpan"></div>  -->
						<select id="searchType" name="searchType" class="fieldset pull-left" onchange="resetUI()"></select> 
					
						<input type="text" id="searchText" class="fieldset" >
						<label id="tacHint" style="display: none; font-weight: 100;margin-left: 365px;"><span style="color: red;">(Hint:first 8 digit of IMEI.)</span></label>
						
					<!-- </div>	 -->
						<div class="Clear"></div>
						<div id="eirInput">
							<label class="label1" id="labelTAC">TAC</label>
							 <input type="text"	id="inputTAC" class="fieldset">
							<label class="label1">IMEI<span style="color: red;">*</span></label>
							 <input type="text"	id="inputIMEI" class="fieldset" style="margin-bottom: 0px;">
							 <span style="color: red;margin-top: 0px;margin-left: 176px;">(Hint:1st 14 digit of IMEI.)</span>
							 <label class="label1" style="margin-top: 27px;">LockImsiImei</label>
							 <select id='inputLockImsiImei' class='fieldset' style="width:161px;">
						     	<option value='FALSE' selected="selected">FALSE</option>
								<option value='TRUE'>TRUE</option>
								
							</select>
							
							 <label class="label1">FastBlackList</label>
							  <select id='inputFastBlackList' class='fieldset' style="width:161px;">
						     	<option value='FALSE' selected="selected">FALSE</option>
								<option value='TRUE'>TRUE</option>
								
							</select>
							
							<label class="label1">DualImsi</label>
							  <select id='inputDualImsi' class='fieldset' style="width:161px;">
						     	<option value='FALSE' selected="selected">FALSE</option>
								<option value='TRUE'>TRUE</option>
								
							</select>
								
							<div class="Clear"></div>
							<br /> 
							<div class="Clear"></div>
							<br />
						</div>

						<div id="addTacInput">
							<label class="label1">Device Manufacturer <span style="color: red;">*</span></label> <input
								type="text" id="deviceManufacturer" class="form-control">
							<div class="Clear"></div>
							<br />
							<div class="Clear"></div>
							<br /> <label class="label1">Device Model <span style="color: red;">*</span></label> <input
								type="text" id="deviceModel" class="form-control">
							<div class="Clear"></div>
							<br /> <label class="label1">Device Bands</label> <input
								type="text" id="deviceBands" class="form-control" placeholder="900/1800/1900">
							<div class="Clear"></div>
							<br /> <label class="label1">Device Screen Resolution</label> <input
								type="text" id="deviceScreenResolution" class="form-control" placeholder="500x400">
							<div class="Clear"></div>
							<br />
							<div class="Clear"></div>
							<br /> <label class="label1">Device Technologies</label> <input
								type="text" id="deviceTechnologies" class="bigfieldset" placeholder="TAC2=,Capability=MMS-access=IP,SUPL=2,remunl=no,mmsblank=yes" style="width: 530px;">
							<div class="Clear"></div>
							<br />
							
							
						</div>
						<input type="hidden" id="enableMMSFlag" value="false">
					 	<div id="enableLockImsiImei">
					 	<label class="label1">UID</label>
					 	<input type="text" id="eirImsiUnlockUID" readonly="readonly" class="fieldset" style="width:250px;">
					 	<label class="label1">Imei History</label>
					 	<input type="text" readonly="readonly" id="eirUnlockImeiHistory" class="fieldset" style="width:250px;">
					 	<label class="label1">Lock Imsi/Imei</label> 
							<select id='lockImsiImei' class='fieldset' style="width:250px;">
						     	<option value='FALSE' selected="selected">FALSE</option>
								<option value='TRUE'>TRUE</option>
								
							</select>
							<button id="update" class="btn btn-primary">Update</button>
						</div> 

						<button id="search" class="btn btn-primary">Search</button>
						
					</div>
				</div>
			</div>
			<div>
				<ul class="tab-menu nav-justified" style="width: 100%"
					id="napDropDown">
					<li id="profile" class="nap-sub-menu-head selected">Profile</li>
					<li id="socs" class="nap-sub-menu-head">SOCs</li>
					<li id="partners" class="nap-sub-menu-head">Partners</li>
				</ul>
			</div>
			<div id="loading"></div>
			<div id="nap-tab">
				<div class="panel panel-primary" id="resultPanel" style="visibility: hidden;">
					<div class="panel-heading" id="usdContentHeading" ></div>
					<div id="usdContentBody" class="panel-body"></div>
				</div>
				<div class="panel panel-primary" id="extraInfo">
					<div class="panel-heading" id="extraInfoHeading"></div>
					<div class="panel-body" id="extraInfoBody"></div>
				</div>
				<div class="panel panel-primary" id="extrainfo2"></div>
				
				<div class="panel panel-primary" id="extraInfo3">
					<div class="panel-heading" id="extraInfoHeading3"></div>
					<div class="panel-body" id="extraInfoBody3"></div>
				</div>
				
				<div class="panel panel-primary" id="extraInfoimpi">
					<div class="panel-heading" id="extraInfoHeading4"></div>
					<div class="panel-body" id="extraInfoBody4"></div>
				</div>
				
				<div class="panel panel-primary" id="extraInfo5">
					<div class="panel-heading" id="extraInfoHeading5"></div>
					<div class="panel-body" id="extraInfoBody5"></div>
				</div>
				
				<div class="panel panel-primary" id="extraInfonoti">
					<div class="panel-heading" id="extraInfoHeading6"></div>
					<div class="panel-body" id="extraInfoBody6"></div>
				</div>
			</div>
		</div>
	</div>

</body>
<jsp:include page="/views/footer.jsp"></jsp:include>
</html>