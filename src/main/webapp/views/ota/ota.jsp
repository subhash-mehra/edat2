<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn" %>
<jsp:include page="/views/header.jsp"></jsp:include>

<script type="text/javascript"
	src="<c:url value="/resources/js/otaTools.js" />"></script>
<script type="text/javascript">
document.title="OTA- Get Subscription";
</script>
<style>
 .display{
   table-layout: fixed;
    word-wrap:break-word; 
    }
</style>
<body>


	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">

		<div class="col-lg-2" style="padding: 0 0 0 15px;">
			<div class="relative-tab-container">
				<ul class="sb" id="otaMenuItems" style="width: 100%">
					<li id="subscription"><a class="usd" href="#">Get
							Subscription</a></li>
							<c:if test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>	
				<!-- 	<li id="activate"><a class="usd" href="#">Activate</a></li>
					<li id="deacivate"><a class="usd" href="#">Deacivate</a></li>
					<li id="update"><a class="usd" href="#">Update</a></li> -->
					</c:if>
				</ul>
			</div>
		</div>

		<div id="otaContents" class="contentJspDiv col-lg-10">
			<div class="panel panel-primary">
				<div class="panel-heading" id="panelHeading">OTA Search:</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div id="searchBox" class="input-group">
						<label class="label1">Search Type<span style="color: red;">*</span></label>
						<select id='searchType' class='form-control pull-left' onchange='resetUI();'></select>
						<input type="text" id="searchText" class="form-control">

						<button id="search" class="btn btn-primary">Search</button>
					</div>
				</div>
			</div>

			<div id="loading"></div>
			<div class="panel panel-primary" id="otaInfo">
				<div class="panel-heading" id="otaContentHeading"></div>
				<div class="panel-body" id="otaContentBody"></div>
			</div>
		</div>
	</div>


</body>
<jsp:include page="/views/footer.jsp"></jsp:include>
</html>