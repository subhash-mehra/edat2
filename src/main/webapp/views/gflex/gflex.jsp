<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="java.util.List"%>
<jsp:include page="/views/header.jsp"></jsp:include>
<script type="text/javascript"
	src="<c:url value="/resources/js/gFlex.js" />"></script>
<script src="<c:url value='/resources/lib/jquery-ui.js'/>"></script>

<script type="text/javascript">
document.title="GFlex Search Msisdn";
	$(function() {
		$("#fromDate").datepicker();
	});

	$(function() {
		$("#toDate").datepicker();
	});
</script>
<body>


	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">

		<div class="col-lg-2" style="padding: 0 0 0 15px;">
			<div class="relative-tab-container">
				<ul class="sb" id="gFlexTabList" style="width: 100%">
					<li id="searchMsisdn" class="selected"><a class="usd" href="#">Search
							MSISDN</a></li>
					<c:if
						test='${(fn:contains(sessionScope.roleList, "GFW") || fn:contains(sessionScope.roleList, "GFRW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
						<li id="addMsisdn"><a class="usd" href="#">Add MSISDN</a></li>
						<li id="updateMsisdn"><a class="usd" href="#">Update
								MSISDN</a></li>
						<li id="deleteMsisdn"><a class="usd" href="#">Delete
								MSISDN</a></li>
						<li id="cleanUp"><a class="usd" href="#">CleanUp</a></li>
					</c:if>
					<c:if
						test='${(fn:contains(sessionScope.roleList, "GFRW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
					
					<li id="reports"><a class="usd" href="#">Report</a></li>
					</c:if>
				</ul>
			</div>
		</div>

		<div id="contentJspId" class="contentJspDiv col-lg-10">
			<div id="messageHeading"
				style="font-weight: bold; font-size: medium; color: red display: none;"></div>
			<div class="panel panel-primary" id="searchDiv">
				<div class="panel-heading" id="panelHeading">MSISDN Search</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div id="searchBox" class="input-group">
						<label class="label1 pull-left">MSISDN(1)<span style="color: red;">*</span></label> <input
							type="text" id="searchText" class="fieldset pull-left">
						<button id="search" class="btn btn-primary pull-left">Search</button>
					</div>
				</div>
			</div>

			<%--Div for CleanUp --%>
			<div id="cleanupDiv" style="display: none">

				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForcleanup">CleanUp</div>
					<div class="panel-body">
					<h5><b><i>Note</i>: Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
						<div id="searchBox" class="input-group">
							<label class="label1">From Date<span style="color: red;">*</span></label> <input type="text"
								id="fromDate" class="fieldset"> <label class="label1">To
								Date<span style="color: red;">*</span></label> <input type="text" id="toDate" class="fieldset">
							<button id="cleanUpBtn" class="btn btn-primary">Delete</button>
						</div>
					</div>
				</div>
			</div>

			<div id="loading"></div>
			<div id="nap-tab" style="padding: 10px;">
				<div class="panel panel-primary" id="gflexContents" style="margin-top:-11px;">
					<div class='panel-heading' id='gflContentHeading'>Gflex
						Report</div>
					<div class="panel-body" id="gflexBody">
					<h5><b><i>Note</i>: Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
						<div class="input-group" style="display:block;">
							<label class="label1">MSISDN<span style="color: red;">*</span></label><input type="text"
								id="msisdnID" class="fieldset"> <label class="label1">Value</label>
							<select id='updateMsisdnValue' class='fieldset'><option
									value='-1'>Select Value</option>
								<option value='211142119'>211142119 Lab GB SIM (Titan)</option>
								<option value='211142156'>211142156 Lab GSM SIM (Lab
									HLR)</option>
								<option value='001179002'>001179002 Production SIM
									(West STP Alias)</option>
							</select> <label class="label1">Delete Date</label><input type="text"
								id="deleteDate" class="fieldset">
							<button id="saveBtn" class="btn btn-primary pull-left"
								style="margin-left: 200px;">Submit</button>

							<button id="canBtn" class="btn btn-primary pull-left"
								style="margin-left: 20px;">Cancel</button>
						</div>
					</div>
					<div id="gflexResponse">
						<div id="gflexResponseBody" class="panel-body"></div>
					</div>
				</div>
				
				<div class="panel panel-primary" id="errorMainPannel" style="display: none;">
					<div class="panel-heading" id="ErrorResponseHeading" style="display: none;"></div>
					<div id="ErrorResponseBody" class="panel-body" style="display: none;"></div>
				</div>
		
			</div>
		</div>
		
	</div>

	<!-- <div id="msisdnResult"></div> -->



</body>

<jsp:include page="/views/footer.jsp"></jsp:include>
</html>