<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
   <!-- <meta http-equiv="X-UA-Compatible" content="IE=7,IE=8,IE=9" /> -->
   
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EDAT</title>
    <!-- Bootstrap -->
	<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet">
	
    <link href="resources/css/login.css" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/resources/core/jquery.1.10.2.min.js" />"></script>
	<script type="text/javascript" src="resources/js/admin.js"></script>
  </head>
  <body>
  
  <%
  
  Map <String,Object>dataMap=new HashMap<String,Object>();
  String message="";
  if(request.getAttribute("dataMap")!=null)
  {
	  dataMap=(Map)request.getAttribute("dataMap");
  }
  
  if(dataMap.get("message")!=null)
  {
	  message=(String)dataMap.get("message");
  }
  
  if(dataMap.get("validUser")!=null)
  {
	  message=(String)dataMap.get("validUser");
  }
  %>
	   <div class="login-box">
	         <img src="resources/images/T-MobileHead.png" style="margin-top:-4px; float:right;width:100px;"/>
	         <div style="clear:both"></div>
			<h3 class="text-center"> Welcome to <img src="resources/images/T-MobileLogo.png" /></h3>
			<!-- <h3 class="text-center"> Please Log In </h3> -->
			<form action="doLogin" method="post" id="loginRequest">
			<h3><span><%=message %></span></h3>
				<label for="user" class="loginlabel">Log In</label><br/>
				<input type="text" name="loginName" id="loginName" placeholder="Username" class="fieldset" style="margin-bottom:10px"/>
				<label for="password" class="loginlabel">Password</label><br/>
				<input type="password" name="password" id="password" placeholder="Password" class="fieldset"/>
				<button type="submit" name="submit" id="submit" label="Log In" class="btn btn-primary btn-md" style="margin-top:10px; float:right">Log In</button>
				<a class="navbar-brand" href="javascript:showLoginHelp()" style="height:auto;color: #ea0a8e;" >Help</a> 
				
			</form>
			<div style="clear:both"></div>
		</div>
        <script type="text/javascript" src="resources/lib/bootstrap.min.js"></script>
  </body>
</html>