<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%
List<String> userRole  = new ArrayList<String>();
if(session.getAttribute("roleName")!=null)
userRole = (List<String>)session.getAttribute("roleName");
%>
<c:set var="roleList" value="<%=userRole%>" scope="session"/>
<html>
<head>

<meta http-equiv="X-UA-Compatible" content="IE=7,IE=8,IE=9" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>EDAT Search Tool</title>
<link rel="stylesheet"
	href="resources/css/bootstrap/bootstrap-table.css">
<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/bootstrap/bootstrap-select.min.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="resources/css/style.css">
<script type="text/javascript" src="resources/js/admin.js"></script>
<script language="javascript" type="text/javascript">
	window.history.forward();
</script>

</head>

<body>

	<jsp:include page="header.jsp"></jsp:include>
	<c:choose>
	<c:when test='${(fn:contains(sessionScope.roleList,"GF") || fn:contains(sessionScope.roleList,"GFR") || fn:contains(sessionScope.roleList,"GFW") || fn:contains(sessionScope.roleList,"GFRW")) }'>
	<script>
	goToModule('gflex');
	</script>
	</c:when>
	<c:otherwise>
	<script>	
		goToModule('usd');	
	</script>
	</c:otherwise>
	</c:choose>	
	<jsp:include page="/views/footer.jsp"></jsp:include>
</body>

</html>