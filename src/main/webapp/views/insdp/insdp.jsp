<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<jsp:include page="/views/header.jsp"></jsp:include>

<script type="text/javascript" src="resources/js/admin.js"></script>
<script type="text/javascript" src="resources/js/functions.js"></script>
<script src="<c:url value='/resources/js/insdpDataPopulation.js'/>"></script>
<script src="<c:url value='/resources/js/common.js'/>"></script>
<script>
document.title="GetAccount";
	$(function() {
		$("#datepicker").datepicker();
	});
	$(function() {
		$("#expDatedatepicker").datepicker();
	});
	$(function() {
		$("#feeExpDatedatepicker").datepicker();
	});
	$(function() {
		$("#actSerfeeExpDatedatepicker").datepicker();
	});
	$(function() {
		$("#deleteOrgdatepicker").datepicker();
	});
	$(function() {
		$("#newOrgdatepicker").datepicker();
	});
	$(function() {
		$("#updateOrgdatepicker").datepicker();
	});

	$(function() {
		$("#actOrignTimedatepicker").datepicker();
	});
	$(function() {
		$("#actSupexpDatedatepicker").datepicker();
	});
	$(function() {
		$("#actSerfeeExpDatedatepicker").datepicker();
	});
</script>

<style>
.relative-tab-container {
	display: block;
}

#tabContainer div ul.tab-menu li {
	height: 40px;
}

#updateServiceField {
	display: none
}

#updateServiceClass {
	display: none
}

#updateAccumulatorDetails {
	display: none
}

#updateSubscriberSeg {
	display: none
}

#createSubscriber {
	display: none
}

#deleteSubscriber {
	display: none
}

#activateSubscriber {
	display: none
}

#updateBalanceAndDate {
	display: none
}

#updateServiceClass {
	display: none
}
#getAccumulatorssearchBox {
	display: none
}

#updateDaBalDetails {
	display: none
}

#updateOfferDetails {
	display: none
}

#getOffersearchBox {
	display: none
}


</style>
<body>



	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">

		<div class="col-lg-2" style="padding: 0 0 0 15px;">
			<div class="relative-tab-container">
				<ul class="sb" id="insdpTabs"
					style="padding-top: 10px; margin-bottom: 0; background-color: #eee;">
					<li id="acc" class="activeUSD"><a class="usd" href="#">GetAccount</a></li>
					<li id="balDat"><a class="usd" href="#">GetBalanceAndDate</a></li>
					<li id="getAccumulators"><a class="usd" href="#">Get Accumulators</a></li>
					<!-- <li id="getOffer"><a class="usd" href="#">Get Offer</a></li> -->
					<c:if
						test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
						<li id="createsubscr"><a class="usd" href="#">Create
								Subscriber</a></li>
						<li id="activeSub"><a class="usd" href="#">Activate
								Subscriber</a></li>
						<li id="updateBal"><a class="usd" href="#">Update Relative Bal</a></li>
						
						<li id="updateDABal"><a class="usd" href="#">Update DA Bal</a></li>
								
						<li id="updateAccum"><a class="usd" href="#">Update Accumulators</a></li>
						<li id="updateserCal"><a class="usd" href="#">Update
								Service Class</a></li>						
						<li id="updateSubSegment"><a class="usd" href="#">Update
								Subscriber Segment</a></li>
						
						<li id=updateOffer><a class="usd" href="#">Update Offer</a></li>
						
						<li id="deleteSub"><a class="usd" href="#">Delete
								Subscriber</a></li>
					</c:if>
				</ul>
			</div>
		</div>
		<div id="contentJspId" class="contentJspDiv col-lg-10">
				<div class="panel panel-primary" id="searchBox" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="searchBoxHeading">Accounts &	Accumulators Search:</div>
	   			<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
				
					<div class="input-group">
					<label style="float: left;" class="label1">Search Type<span style="color: red;">*</span></label>
						<select id="searchType" class="form-control"
							onchange='clearText();'><option value="MSISDN">MSISDN</option></select>
						<input type="text" id="searchText" class="form-control">
						<button id="search" class="btn btn-primary"
							onClick="getedatData(this);">Search</button>
					</div>
				</div>
			</div>

			<!--  Update Service class  -->
			<div class="panel panel-primary" id="updateServiceClass" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Update Service</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<!-- <label class="label1">origin Time</label> <input type="text"
							id="datepicker" class="fieldset"> -->
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="servicemsisdn" class="fieldset"> <label
							class="label1">Service Class</label> <input type="text"
							id="serviceClass" class="fieldset">
						<button id="search" class="btn btn-primary"
							onClick="getedatData(this);">Update Service</button>
					</div>
				</div>
			</div>
<!-- Get Accumulators -->
	<div class="panel panel-primary" id="getAccumulatorssearchBox" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="getAccumulatorssearchBoxHeading">Get Accumulators:</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<!-- <label class="label1">origin Time</label> <input type="text"
							id="datepicker" class="fieldset"> -->
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="getAccumulatorsMsisdn" class="fieldset">
						<button id="search" class="btn btn-primary"
							onClick="getedatData(this);">Search</button>
					</div>
				</div>
			</div>
		
		
		
		
			<!-- <div class="panel panel-primary" id="getAccumulatorssearchBox" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="getAccumulatorssearchBoxHeading">Get Accumulators:</div>
	   			<div class="panel-body" >
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
				
					<div class="input-group" style="display:block;">
					<label  class="label1">Search Type<span style="color: red;">*</span></label>
						<select id="searchType" class="form-control"
							onchange='clearText();'><option value="MSISDN">MSISDN</option></select>
						<input type="text" id="getAccumulatorsMsisdn" class="fieldset">
						<button id="getAccumulatorsSearch" class="btn btn-primary"
							onClick="getedatData(this);">Search</button>
					</div>
				</div>
			</div> -->
			
			<!-- End -->
			
			<!-- Get Offer -->
		<div class="panel panel-primary" id="getOffersearchBox" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="getOffersearchBoxHeading">Get Offer:</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<!-- <label class="label1">origin Time</label> <input type="text"
							id="datepicker" class="fieldset"> -->
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="getOfferMsisdn" class="fieldset">
						<button id="getOfferBtn" class="btn btn-primary"
							onClick="getedatData(this);">Search</button>
					</div>
				</div>
			</div>
		
		
			<!--  Update balance Details  -->

			<div class="panel panel-primary" id="updateBalanceAndDate" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Update Balance</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<label class="label1">Supervision Expiry Date<span style="color: red;">*</span></label> <input
							type="text" id="expDatedatepicker" class="fieldset"> <label
							class="label1">Service FeeExpiry Date<span style="color: red;">*</span></label> <input type="text"
							id="feeExpDatedatepicker" class="fieldset"> <label
							class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="UpdateBalanceMsisdn" class="fieldset"> <label
							class="label1">Adjustment Amount Relative<span style="color: red;">*</span></label> <input
							type="text" id="AmountRelativeForUpdateBalance" class="fieldset">
							<label id="tacHint" style="display: inline; font-weight: 100;margin-left: 344px;"><span style="color: red;">(Hint:for $10.25,enter 1025.)</span></label>
						<div class="Clear"></div>
						<button id="search" class="btn btn-primary"
							onClick="getedatData(this);">Update Balance</button>
					</div>
				</div>
			</div>

			<!--  Create Subscriber/install Subscriber  -->


			<div class="panel panel-primary" id="createSubscriber" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Create Subscriber</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5> 
					<div class="input-group" style="display:block;">
						<!-- label class="label1">Origin Time</label>
						 <input type="text"	id="newOrgdatepicker" class="fieldset"> -->
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="newmsisdn" class="fieldset"> <label class="label1">Service
							Class New <span style="color: red;">*</span></label> <input type="text" id="newServiceClass"
							class="fieldset"> <label class="label1">Language
							ID New<span style="color: red;">*</span></label> <input type="text" id="newLanguage" class="fieldset" onkeypress="">
							<input type="hidden" id="rowFlag" value="0">
							 <label class="label1">UssdEndOfCallNotificationID</label>
							  <input type="text" id="ussdEndOfCallNotificationID" class="fieldset" onkeypress="" placeholder="6" >
							<input type="hidden" id="rowFlag" value="0">
							
						<button id="btnAddd" class="btn btn-primary">Add New
							Service Offerings</button>
						<button id="search" class="btn btn-primary"
							style="margin-left: 10px;" onClick="getedatData(this);">Create
							Subscriber</button>
						<div id="nap-tab" style="padding: 0px;">
							<div class="panel panel-primary" id="dynaTable" style="margin-top: 20px;">
								<div class="panel-heading" id="sdpContentHeading"></div>
								<div class="panel-body">
									<table class='table' id="sdpContentBody" width="100%">
										<tr>
											<!-- 	<th width="30%">Service Name</th> -->
											<th width="30%">Service Offering ID</th>
											<th width="30%">ActiveFlag</th>
											<th width="10%">Action</th>
										</tr>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<!--   Update Accumulators Details  -->

			<div class="panel panel-primary" id="updateAccumulatorDetails" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Update
					Accumulator</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="Accmsisdn" class="fieldset"> <label class="label1">Accumulator
							Id<span style="color: red;">*</span></label> <input type="text" id="AccId" class="fieldset"> <label
							class="label1">Accumulator Value<span style="color: red;">*</span></label> <input type="text"
							id="AccVal" class="fieldset">

						<!-- <button id="btnAddd2" class="btn btn-primary">Add New
							Accumulator Info</button> -->
						<button id="search" class="btn btn-primary"
							onClick="getedatData(this);">Update Accumulator</button>
						<!-- <div id="nap-tab2" style="padding: 0px;">
							<div class="panel panel-primary" id="dynaTable2">
								<div class="panel-heading" id="sdpContentHeading"></div>
								<div class="panel-body">
									<table class='table' id="sdpContentBody2" width="100%">
										<tr>
											<th width="30%">Service Name</th>
											<th width="30%">Value</th>
											<th width="30%">ActiveFlag</th>
											<th width="10%">Action</th>
										</tr>
									</table>
								</div>
							</div>

						</div> -->
					</div>
				</div>
			</div>
			
			
				<!--   Update Update DA Bal  -->

			<div class="panel panel-primary" id="updateDaBalDetails" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="DaBalpanelHeading">Update DA Bal</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="UpdateDABalmsisdn" class="fieldset"> <label class="label1">Dedicated Account ID<span style="color: red;">*</span></label>
							 <input type="text" id="dedicatedAccountId" class="fieldset">
							  <label class="label1">Dedicated Account Value<span style="color: red;">*</span></label>
							   <input type="text" id="dedicatedAccountValue" class="fieldset">

					
						<button id="updateDABalBtn" class="btn btn-primary"
							onClick="getedatData(this);">Update DA Bal</button>
						
					</div>
				</div>
			</div>
			
			
				<!--   Update Offer  -->

			<div class="panel panel-primary" id="updateOfferDetails" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="updateOfferpanelHeading">Update Offer</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="offerMsisdn" class="fieldset"> 
							  <label class="label1">Offer Id<span style="color: red;">*</span></label>
							   <input type="text" id="offerId" class="fieldset">

					
						<button id="updateOfferBtn" class="btn btn-primary"
							onClick="getedatData(this);">Update Offer</button>
						
					</div>
				</div>
			</div>

			<!--  Update Subscriber segment   -->

			<div class="panel panel-primary" id="updateSubscriberSeg" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Update Subscriber</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
					<div class="input-group" style="display:block;">
						<!-- <label class="label1">origin Time</label>
						 <input type="text"	id="updateOrgdatepicker" class="fieldset">
						 -->
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="updatedmsisdn" class="fieldset">
						<button id="btnAddd1" class="btn btn-primary">Add New
							Service Offerings</button>
						<button id="search" class="btn btn-primary"
							style="margin-left: 10px;" onClick="getedatData(this);">Update
							Subscriber</button>
						<div id="nap-tab1" style="padding: 0px;">
							<div class="panel panel-primary" id="dynaTable1" style="margin-top: 20px;">
								<div class="panel-heading" id="sdpContentHeading"></div>
								<div class="panel-body">
									<table class='table' id="sdpContentBody1" width="100%">
										<tr>
											<th width="30%">Service Offering ID</th>
											<th width="30%">ActiveFlag</th>
											<th width="10%">Action</th>
										</tr>
									</table>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!--  Activate Subscriber   -->


			<div class="panel panel-primary" id="activateSubscriber" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Activate
					Subscriber</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5> 
					<div class="input-group" style="display:block;">
						<!-- <label class="label1">Origin Time</label> <input type="text"
							id="actOrignTimedatepicker" class="fieldset"> -->
						<label class="label1">Supervision Expiry Date<span style="color: red;">*</span></label> <input
							type="text" id="actSupexpDatedatepicker" class="fieldset">
							
						<label class="label1">Service FeeExpiry Date<span style="color: red;">*</span></label> <input
							type="text" id="actSerfeeExpDatedatepicker" class="fieldset">
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="activeSubscriberMsisdn" class="fieldset"> 
							
							<label
							class="label1">Adjustment Amount Relative</label> <input
							type="text" id="adjustmentAmountRelative" class="fieldset">
							<label id="tacHint" style="display: inline; font-weight: 100;margin-left: 344px;"><span style="color: red;">(Hint:for $10.25,enter 1025.)</span></label>
						<div class="Clear"></div>
						<button id="search" class="btn btn-primary"
							onClick="getedatData(this);">Activate</button>
					</div>
				</div>
			</div>

			<!--  Update Service class  -->

			<div class="panel panel-primary" id="deleteSubscriber" style="padding-left: 0px; padding-top: 0px;padding-right: 0px;margin-top:9px;">
				<div class="panel-heading" id="panelHeading">Delete Subscriber</div>
				<div class="panel-body">
				<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5> 
					<div class="input-group">
						<!-- <label class="label1">origin Time</label> <input type="text"
							id="deleteOrgdatepicker" class="fieldset"> -->
						<label class="label1">MSISDN<span style="color: red;">*</span></label> <input type="text"
							id="deletedmsisdn" class="fieldset pull-left">
						<button id="search" class="btn btn-primary pull-left"
							onClick="getedatData(this);">Delete</button>
					</div>
				</div>
			</div>


			<div class="tab-content active">
				<div id="insdp" class="table-responsive accounts"
					style="padding-top: 10px;"></div>

			</div>
			
		
		</div>
		<div id="loading" style="padding-top: 150px; z-index:999"></div>
	</div>
	
</body>
<jsp:include page="/views/footer.jsp"></jsp:include>
</html>