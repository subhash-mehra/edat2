<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<jsp:include page="/views/header.jsp"></jsp:include>
<link href="<c:url value="/resources/css/jqtree.css" />" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/lib/tree.jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/tree.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/msgData.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/iamModule.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/messagingAutoComplete.js"></script>



<script type="text/javascript">


document.title="SMSC Aci Search";
$(document).ready(function(){
	$("#smcsAci").addClass("selected");
	});

$(document).ready(function(){
	$("#smscMavenier").click(function(){
		$("#smsc-sub-menu li").slideToggle(500);
		$(this).toggleClass("expanded");
	});
});


$(document).ready(function(){
	$("#mmscMain").click(function(){
		$("#mmsc-sub-menu li").slideToggle(500);
		$(this).toggleClass("expanded");
	});
});

$(document).ready(function(){
	$("#smcsAci").click(function(){
		$("#smsc-aci-sub-menu li").slideToggle(500);
		$(this).toggleClass("expanded");
	});
});

$(document).ready(function(){
	$("#iam").click(function(){
		$("#iam-sub-menu li").slideToggle(500);
		$(this).toggleClass("expanded");
	});
});


</script>
<body>

	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">

		<div class="col-lg-2" style="padding: 0 0 0 15px;">
			<div class="relative-tab-container">
				<ul class="sb" id="msgTabList" style="width: 100%">

					<li id="smcsAci"><a class="usd" href="#"
						style="padding-top: 6px;">SMSC-Aci</a><span class="icon-expand"></span>
						<!-- <img src="/edat/resources/images/Arrow-Icon-small.jpg" id="arrowdownForSMSCAci" onclick="changeImageForSMSCAci();"/> -->
					</li>
					<div id="smsc-aci-sub-menu">
						<li id="smcs"><a class="usd" href="#">Search</a></li>

						<c:if
							test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
							<li id="smscAddSubscriber"><a class="usd" href="#">Add
									Subscriber</a></li>
							<li id="smscModifySubscriber"><a class="usd" href="#">Modify
									Subscriber</a></li>
							<li id="smscDeleteSubscriber"><a class="usd" href="#">Delete
									Subscriber</a></li>
						</c:if>
					</div>

					<li id="mmscMain"><a class="usd" href="#"
						style="padding-top: 6px;">MMSC</a> <span class="icon-expand"></span>
						<!-- <img src="/edat/resources/images/Arrow-Icon-small.jpg" id="arrowdownForMMSC" onclick="changeImageForMMSC();"/> -->
					</li>
					<div id="mmsc-sub-menu">

						<li id="mmsc"><a class="usd" href="#">Search</a></li>

						<c:if
							test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>

							<li id="putSubscriber"><a class="usd" href="#">Add/Modify</a></li>
							<li id="mmscDelete"><a class="usd" href="#">Delete</a></li>
						</c:if>
					</div>

					<li id="iam"><a class="usd" href="#">IAM</a> <span
						class="icon-expand"></span></li>
					<div id="iam-sub-menu">
						<li id="iam_search"><a class="usd" href="#">IAM Search</a></li>
						<li id="iam_token"><a class="usd" href="#">IAM Generate	Token</a></li>
						<li id="iam_link"><a class="usd" href="#">IAM Link</a></li>
						<li id="iam_unlink"><a class="usd" href="#">IAM Unlink</a></li>
						<li id="iam_lock"><a class="usd" href="#">IAM Lock</a></li>
						<li id="iam_unlock"><a class="usd" href="#">IAM UnLock</a></li>
						<li id="iam_privacy_vault"><a class="usd" href="#">Privacy Vault</a></li>
					</div>
					<!-- <li id="smscUpdate"><a class="usd" href="#">SMSC Update</a></li> -->
					<li id="smscMavenier"><a class="usd" href="#"
						style="padding-top: 6px;">SMSC-Mav</a><span class="icon-expand"></span>
					</li>
					<div id="smsc-sub-menu">
						<li id="getSubscriber"><a class="usd" href="#">GetSubscriber</a>

							<c:if
								test='${(fn:contains(sessionScope.roleList, "RW") || fn:contains(sessionScope.roleList, "ADMIN"))}'>
								<li id="addSubscriber"><a class="usd" href="#">AddSubscriber</a>
								</li>
								<li id="modifySubscriber"><a class="usd" href="#">ModifySubscriber</a>
								</li>
								<li id="deleteSubscriberForSMCS"><a class="usd" href="#">DeleteSubscriber</a>
								</li>
								<!-- <li id="deleteSubscriber"><a class="usd" href="#">DeleteSubscriber</a></li> -->
								<!-- <li id="deleteSubscriberService"><a class="usd" href="#" title="DeleteSubscriber by service specific">DeleteSubscriber by service specific</a>
                        </li>
                        
                        </li>
                        <li id="getSubscriberService"><a class="usd" href="#" title="GetSubscriber by service specific">GetSubscriber
							by service specific</a>
                        </li> -->
					</div>
					</c:if>


				</ul>
			</div>
		</div>

		<div id="contentJspId" class="contentJspDiv col-lg-10">
			<div id="messageHeading"
				style="font-weight: bold; font-size: medium; color: red; display: none;"></div>
			<div id="SMSCDiv">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">SMSC-Aci Search:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">Search<span style="color: red;">*</span></label>
							<!--  <div id="dropDownSpan"></div> -->
							<select id='searchType' class='form-control pull-left'>
							</select>
							<!-- <label class="label1">IMSI</label> -->
							<input type="text" id="mdtSearch" class="form-control">
							<br><br>
							
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<button id="search" class="btn btn-primary pull-left">Search</button>
						</div>
					</div>
				</div>
				<div class="tree">
-
-                               </div>

			</div>

			<div id="iam_token_panel" class="hideTokenPanel">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">IAM Token
						Generation:</div>
					<div class="panel-body">
						<div id="searchBox" class="input-group">
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<br>
							<button id="generateToken" class="btn btn-primary pull-left" onclick="generateToken()">Generate Token</button>
						</div>
					</div>
				</div>
				<table class='table' id="accessToken_table" width="100%">
					<tr>
						<th width="20%">Access Token</th>
						<th width="20%">Token Type</th>
						<th width="50%">Scope</th>
						<th width="10%">Expire In</th>
					</tr>
				</table>
			</div>
			
			<!-- IAM Link Panel Start -->
			<div id="iam_link_panel" class="hideTokenPanel">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">IAM Link:</div>
					<div class="panel-body">
						<div id="searchBox_iam_link" class="input-group" style="width: 800px;">
							<label class="label1">Profile</label>
							<select id='searchTypeLink' class='form-control pull-left'>
								<option>MSISDN</option>
								<option>IMSI</option>
								<option>User ID</option>
								<option>Email</option>
							</select>
							<input id="msisdnLinkInput"
								type="text" class="fieldset"> 
							<label class="labelIamLink">Link Type: </label> 
							<select id="scopeList" class="form-control pull-left" >
								<option>Email</option>
								<option>UID</option>
								<option>User</option>
							</select>
							<input	type="text" id="userIdInput" class="fieldset">							
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<button id="buttonIAMLink" class="btn btn-primary pull-left" style="width:80px"  onclick="linkProfile()" >Link</button>
						</div>
					</div>
				</div>
			</div>
			<!-- IAM Link Panel End -->
			
			<!-- IAM UnLink Panel Start -->
			<div id="iam_unlink_panel" class="hideTokenPanel">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">IAM UnLink:</div>
					<div class="panel-body">
						<div id="searchBox_iam_unlink" class="input-group" style="width: 800px;">
							<label class="label1">Profile</label>
							<select id='searchTypeUnlink' class='form-control pull-left'>
								<option>MSISDN</option>
								<option>IMSI</option>
								<option>User ID</option>
								<option>Email</option>
							</select>
							<input id="msisdnUnlinkInput"
								type="text" class="fieldset"> 
							<label class="labelIamUnLink">Link Type: </label> 
							<select id="scopeListUnlink" class="form-control pull-left" >
								<option>Email</option>
								<option>UID</option>
								<option>User</option>
							</select>
							<input	type="text" id="userIdInputUnlink"  class="fieldset">	
							<label class="labelIamUnLink">Revoke Permission: </label> 
							<select id='revokePermissonValue' class='form-control pull-left'>
								<option>true</option>
					
			<option>false</option>
							</select>
							<br/><br/>
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<button id="buttonIAMUnLink" class="btn btn-primary pull-left" style="width:80px" onclick="unLinkProfile()">UnLink</button>
						</div>
					</div>
				</div>
			</div>
		<!-- Iam Lock Panel start -->	
			<div id="iam_lock_panel" class="hideLockPanel">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">IAM Lock:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox_iam_lock" class="input-group" style="width: 800px;">
					
							<label style="float: left;" class="label1">Lock Type<span style="color: red;">*</span></label>
							<select id="searchTypeLock" class='form-control pull-left'>
								
							</select>
							<input type="text" id="lock_type_value" class="fieldset">
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<button id="lock_account" class="btn btn-primary pull-left" >Lock</button>
						</div>
					</div>
				</div>
				<table class='table' id="accessLock_table" width="100%">
					<tr>
						<th width="20%">Profile Type</th>
					</tr>
				</table>
			</div>
			
			<!-- Iam Lock Panel end -->
			
			
			<!-- Iam UnLock Panel start -->	
			<div id="iam_unlock_panel" class="hideUnLockPanel">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">IAM UnLock:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox_iam_Unlock" class="input-group" style="width: 800px;">
					
							<label style="float: left;" class="label1">UnLock Type<span style="color: red;">*</span></label>
							<select id="searchTypeUnLock" class='form-control pull-left'>
								
							</select>
							<input type="text" id="unlock_type_value" class="fieldset" >
							<label  class="label1">is_account_locked</label>
							<select id="is_account_locked" class='form-control pull-left'>
								<option value="true">true</option>
								<option value="false">false</option>
							</select>
							<br/><br/>
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<button id="unlock_account" class="btn btn-primary pull-left" >UnLock</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Iam UnLock Panel end -->
			
				<div id="iam_privacy_vault_panel">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">SMSC-Aci Search:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">Search<span style="color: red;">*</span></label>
							<!--  <div id="dropDownSpan"></div> -->
							<select id='privacy_vault_search_type' class='form-control pull-left' onchange='getPrivateVaultOfferId(this)'>
							</select>
							<!-- <label class="label1">IMSI</label> -->
							<input type="text" id="privacy_vault_mdt_search" class="form-control hideOthers">
							<br><br>
							
							<jsp:include page="/views/common/iamCommonElmnt.jsp"></jsp:include>
							<button id="search" class="btn btn-primary pull-left" onclick="offerSearchPrivacyVault()">Offer Search</button>
						</div>
					</div>
				</div>
			</div>
			
			<%--For SMSC Modify Search --%>


			<div id="SMSCSearchModifyDiv" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">SMSC-Modifiy
						Subscriber Search:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">MSISDN<span style="color: red;">*</span></label>
							<!--    <div id="dropDownSpan"></div> -->
							<!-- <label class="label1">IMSI</label> -->
							<input type="text" id="modifySearchText"
								class="form-control pull-left">
							<button id="modifySearch" class="btn btn-primary pull-left">Search</button>
						</div>
					</div>
				</div>
			</div>



			<%--For SMSC Delete Search --%>


			<div id="SMSCSearchdeleteDiv" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeading">SMSC-Delete
						Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">MSISDN<span style="color: red;">*</span></label>
							<!--     <div id="dropDownSpan"></div> -->
							<!-- <label class="label1">IMSI</label> -->
							<input type="text" id="deleteSearchText"
								class="form-control pull-left">
							<button id="deleteSearch" class="btn btn-primary pull-left">Delete</button>
						</div>
					</div>
				</div>
			</div>

			<div id="mmscDeleteDiv" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForMmscDelete">MMSC
						Delete Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">MSISDN<span style="color: red;">*</span></label>
							<input type="text" id="mmscDeleteMsisdn"
								class="form-control pull-left">
							<button id="mmscDeleteBtn" class="btn btn-primary pull-left">Delete</button>
						</div>
					</div>
				</div>
			</div>


			<!--  Add Subscriber  -->
			<div id="addSubscriberDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForAddSubscriber">Add
						Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">

							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="addSubscriberMsisdn" class="fieldset">
							<label class="label1">IMSI<span style="color: red;">*</span>
							</label> <input type="text" id="imsiAdd" class="fieldset"> <label
								class="label1">SubStatus<span style="color: red;">*</span></label>
							<select name="subStatusSelect" id="subStatusSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('subStatusSelect','subStatus');">
								<option value="">Select</option>
								<option value="ACT">ACT</option>
								<option value="DAC">DAC</option>
								<option value="RSM">RSM</option>
								<option value="SUS">SUS</option>
								<option value="RST">RST</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="subStatus"
								name="subStatus"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" id="subStatus" class="fieldset"> -->
							<label class="label1">Language<span style="color: red;">*</span></label>
							<select name="languageSelect" id="languageSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('languageSelect','language');">
								<option value="">Select</option>
								<option value="En">En</option>
								<option value="Sp">Sp</option>
								<option value="OTH">Other</option>

							</select> <input type="text" class="fieldset" id="language"
								name="language"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--    <input type="text" id="language" class="fieldset"> -->
							<label class="label1">Sub Type<span style="color: red;">*</span></label>
							<select name="subTypeSelect" id="subTypeSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('subTypeSelect','subType');">
								<option value="">Select</option>
								<option value="POSTPAID">POSTPAID</option>
								<option value="PREPAID">PREPAID</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="subType" name="subType"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" id="subType" class="fieldset"> -->
							<label class="label1">Operator Id<span
								style="color: red;">*</span></label> <select name="operatorIdSelect"
								id="operatorIdSelect" class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('operatorIdSelect','operatorId');">
								<option value="">Select</option>
								<option value="ARGOS">ARGOS</option>
								<option value="CINTEX">CINTEX</option>
								<option value="Default">Default</option>
								<option value="GOSMART">GOSMART</option>
								<option value="LYCA">LYCA</option>
								<option value="M2M">M2M</option>
								<option value="METRO">METRO</option>
								<option value="MVNE">MVNE</option>
								<option value="MVNE2">MVNE2</option>
								<option value="NOVA">NOVA</option>
								<option value="PLINTRON">PLINTRON</option>
								<option value="ROMO">ROMO</option>
								<option value="SIMPLE">SIMPLE</option>
								<option value="SOLAVEI">SOLAVEI</option>
								<option value="SOUTHERNLINC">SOUTHERNLINC</option>
								<option value="TARGET">TARGET</option>
								<option value="TFRAME">TFRAME</option>
								<option value="TMOUS">TMOUS</option>
								<option value="TMOUS_EF">TMOUS_EF</option>
								<option value="TMOUS_EF_ON">TMOUS_EF_ON</option>
								<option value="TMOUS_ES">TMOUS_ES</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_FA">TMOUS_FA</option>
								<option value="TMOUS_FA_EF">TMOUS_FA_EF</option>
								<option value="TMOUS_FA_EF_ON">TMOUS_FA_EF_ON</option>
								<option value="TMOUS_FA_ES">TMOUS_FA_ES</option>
								<option value="TMOUS_FA_ES_ON">TMOUS_FA_ES_ON</option>
								<option value="TMOUS_FA_ON">TMOUS_FA_ON</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_MB">TMOUS_MB</option>
								<option value="TMOUS_MB_ON">TMOUS_MB_ON</option>
								<option value="TMOUS_ON">TMOUS_ON</option>
								<option value="TMOUS_TF">TMOUS_TF</option>
								<option value="TMOUS_TF_ON">TMOUS_TF_ON</option>
								<option value="TRACFONE">TRACFONE</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="operatorId"
								name="operatorId"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="operatorId" class="fieldset"> -->
							<label class="label1">Profile Id<span style="color: red;">*</span></label>
							<select name="profileIdSelect" id="profileIdSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('profileIdSelect','profileId');">
								<option value="">Select</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="profileId"
								name="profileId"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" id="profileId" class="fieldset"> -->
							<label class="label1">IMSEnabled</label> <select
								name="imsEnabled" id="imsEnabled" class="fieldsetSelect">
								<option value="">Select</option>
								<option value="TRUE">TRUE</option>
								<option value="FALSE">FALSE</option>
							</select>
							<!--   <input type="text" id="imsEnabled" list="IMSEnabledVal" value="TRUE" class="fieldset" /> -->
							<button id="addSubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">Add Subscriber</button>
						</div>
					</div>
				</div>
			</div>


			<!--  Modify SMSC Subscriber  -->
			<div id="modifySMSCSubscriberDiv" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForModifySMSCSubscriber">Modify
						SMSC Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group" style="width: 800px;">


							<label class="label1">Telephone Number<span
								style="color: red;">*</span></label> <input type="text" class="fieldset"
								id="smscModifyTelephone"> <label class="label1">Subscribe
								Profile Number</label> <input type="text" id="smscModifyProfileNo"
								class="fieldset"> <label class="label1">Subscriber
								Address<span style="color: red;">*</span>
							</label> <input type="text" id="smscModifySubAddress" class="fieldset">
							<!--  <label class="label1">UID</label> -->
							<input type="hidden" class="fieldset" id="smscModifyUID">
							<label class="label1">Gender</label> <select name="gender"
								id="smscModifyGender" class="fieldsetSelect">
								<option value="Unlimited">Unlimited</option>
								<option value="Limited">Limited</option>
							</select> <label class="label1">CN</label> <select
								name="smscModifyCNSelect" id="smscModifyCNSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscModifyCNSelect','smscModifyCN');">
								<option value="LYCA">LYCA</option>
								<option value="M2M">M2M</option>
								<option value="METRO">METRO</option>
								<option value="MVNE">MVNE</option>
								<option value="MVNE2">MVNE2</option>
								<option value="NOVA">NOVA</option>
								<option value="PLINTRON">PLINTRON</option>
								<option value="ROMO">ROMO</option>
								<option value="SIMPLE">SIMPLE</option>
								<option value="SOLAVEI">SOLAVEI</option>
								<option value="SOUTHERNLINC">SOUTHERNLINC</option>
								<option value="TARGET">TARGET</option>
								<option value="TFRAME">TFRAME</option>
								<option value="TMOUS" selected="selected">TMOUS</option>
								<option value="TMOUS_EF">TMOUS_EF</option>
								<option value="TMOUS_EF_ON">TMOUS_EF_ON</option>
								<option value="TMOUS_ES">TMOUS_ES</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_FA">TMOUS_FA</option>
								<option value="TMOUS_FA_EF">TMOUS_FA_EF</option>
								<option value="TMOUS_FA_EF_ON">TMOUS_FA_EF_ON</option>
								<option value="TMOUS_FA_ES">TMOUS_FA_ES</option>
								<option value="TMOUS_FA_ES_ON">TMOUS_FA_ES_ON</option>
								<option value="TMOUS_FA_ON">TMOUS_FA_ON</option>
								<option value="TMOUS_MB">TMOUS_MB</option>
								<option value="TMOUS_MB_ON">TMOUS_MB_ON</option>
								<option value="TMOUS_ON">TMOUS_ON</option>
								<option value="TMOUS_TF">TMOUS_TF</option>
								<option value="TMOUS_TF_ON">TMOUS_TF_ON</option>
								<option value="TRACFONE">TRACFONE</option>
								<option value="WALMART">WALMART</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscModifyCN"
								name="smscModifyCN"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" class="fieldset" id="smscModifyCN"> -->
							<label class="label1">Mail</label> <input type="text"
								class="fieldset" id="smscModifyMail"> <label
								class="label1">Given Name</label> <input type="text"
								class="fieldset" id="smscModifyGivenName"> <label
								class="label1">SN<span style="color: red;">*</span></label> <select
								name="smscModifySNSelect" id="smscModifySNSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscModifySNSelect','smscModifySN');">
								<option value="TMO" selected="selected">TMO</option>
								<option value="MVE">MVE</option>
								<option value="IDT">IDT</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscModifySN"
								name="smscModifySN"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" class="fieldset" id="smscModifySN"> -->
							<!--  <label class="label1">Unique Identifier</label> -->
							<input type="hidden" class="fieldset"
								id="smscModifyUniqueIdentifier"> <label class="label1">User
								Agent Profile</label> <select name="smscModifyUserAgentProfileSelect"
								id="smscModifyUserAgentProfileSelect" class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscModifyUserAgentProfileSelect','smscModifyUserAgentProfile');">
								<option value="Postpaid" selected="selected">Postpaid</option>
								<option value="Controlled">Controlled</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset"
								id="smscModifyUserAgentProfile"
								name="smscModifyUserAgentProfile"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" class="fieldset" id="smscModifyUserAgentProfile"> -->
							<label class="label1">User Agent String</label> <select
								name="smscModifyUserAgentStringSelect"
								id="smscModifyUserAgentStringSelect" class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscModifyUserAgentStringSelect','smscModifyUserAgentString');">
								<option value="Active" selected="selected">Active</option>
								<option value="Hard Suspended">Hard Suspended</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset"
								id="smscModifyUserAgentString" name="smscModifyUserAgentString"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" class="fieldset" id="smscModifyUserAgentString"> -->
							<!-- Prepaid Attributes -->
							<label class="label1">Prepaid<span style="color: red;">*</span></label>
							<select name="prepaid" id="smscModifyPrepaid"
								class="fieldsetSelect" onchange="showPrepaidDiv(this.val);">
								<option value="0">Select</option>
								<option value="yes">Yes</option>
								<option value="no">No</option>
							</select>
							<div id="prepaidModifyDiv" style="display: inline;">
								<label class="label1">TPLACODE<span style="color: red;">*</span></label>
								<select name="smscModifyTplaCodeSelect"
									id="smscModifyTplaCodeSelect" class="fieldsetSelect"
									onChange="addSupplierUserAgentProf('smscModifyTplaCodeSelect','smscModifyTplaCode');">
									<option value="TPLA0">TPLA0</option>
									<option value="TPLA1" selected="selected">TPLA1</option>
									<option value="TPLA2">TPLA2</option>
									<option value="OTH">Other</option>
								</select> <input type="text" class="fieldset" id="smscModifyTplaCode"
									name="smscModifyTplaCode"
									style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
								<!-- <input type="text" class="fieldset" id="smscModifyTplaCode"> -->
								<label class="label1">Smsc Sub Prepaid</label> <input
									type="text" class="fieldset" id="smscModifysubprepaid">
								<label class="label1">PSA</label> <input type="text"
									class="fieldset" id="smscModifyPAS" value="PSA1"> <label
									class="label1">PSA INPCODE</label> <input type="text"
									class="fieldset" id="smscModifyInpCode" value="TMO"> <label
									class="label1">PSA Services</label> <input type="text"
									class="fieldset" id="smscModifyPSAServices"> <label
									class="label1">PSA Force Flag </label> <select name="prepaid"
									id="modifypsaforceflag" class="fieldsetSelect">
									<option value="0">Select</option>
									<option value="yes" selected="selected">Yes</option>
									<option value="no">No</option>
								</select> <label class="label1">PSA Action</label> <input type="text"
									class="fieldset" id="modifypsaaction"> <label
									class="label1">PSA MSISDN</label> <input type="text"
									class="fieldset" id="modifypsamsisdn">
							</div>
							<!-- End -->

							<label class="label1">Language<span style="color: red;">*</span></label>
							<select name="smscModifyLanguageSelect"
								id="smscModifyLanguageSelect" class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscModifyLanguageSelect','smscModifyLanguage');">
								<option value="">Select</option>
								<option value="En" selected="selected">En</option>
								<option value="Sp">Sp</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscModifyLanguage"
								name="smscModifyLanguage"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="smscModifyLanguage" class="fieldset"> -->
							<label class="label1">Mobile Type<span
								style="color: red;">*</span></label> <select
								name="smscModifyMobileTypeSelect"
								id="smscModifyMobileTypeSelect" class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscModifyMobileTypeSelect','smscModifyMobileType');">
								<option value="Nokia 5180i">Select</option>
								<option value="Nokia 5180i" selected="selected">Nokia
									5180i</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscModifyMobileType"
								name="smscModifyMobileType"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" id="smscModifyMobileType" class="fieldset"> -->
							<label class="label1">Password</label> <input type="text"
								id="smscModifyPassword" class="fieldset">
							<button id="modifySMSCSubscriberBtn" class="btn btn-primary"">Modify
								SMSC Subscriber</button>

						</div>
					</div>
				</div>
			</div>

			<!--  Add SMSC Subscriber  -->
			<div id="addSNSCSubscriberDiv" style="display: none;">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForAddSMSCSubscriber">Add
						SMSC Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group" style="width: 800px;">

							<label class="label1">Telephone Number<span
								style="color: red;">*</span></label> <input type="text" class="fieldset"
								id="smscTelephone"> <label class="label1">Subscribe
								Profile Number</label> <input type="text" id="smscProfileNo"
								class="fieldset"> <label class="label1">Subscriber
								Address <span style="color: red;">*</span>
							</label> <input type="text" id="smscSubAddress" class="fieldset">
							<!-- <label class="label1">UID</label> -->
							<input type="hidden" class="fieldset" id="smscUID"> <label
								class="label1">Gender</label> <select name="gender"
								id="smscGender" class="fieldsetSelect">
								<option value="0">Select</option>
								<option value="Unlimited" selected="selected">Unlimited</option>
								<option value="Limited">Limited</option>
							</select> <label class="label1">CN</label> <select name="smscCNSelect"
								id="smscCNSelect" class="fieldsetSelect"
								onChange="addSupplierCNdisplay()">
								<option value="LYCA">LYCA</option>
								<option value="M2M">M2M</option>
								<option value="METRO">METRO</option>
								<option value="MVNE">MVNE</option>
								<option value="MVNE2">MVNE2</option>
								<option value="NOVA">NOVA</option>
								<option value="PLINTRON">PLINTRON</option>
								<option value="ROMO">ROMO</option>
								<option value="SIMPLE">SIMPLE</option>
								<option value="SOLAVEI">SOLAVEI</option>
								<option value="SOUTHERNLINC">SOUTHERNLINC</option>
								<option value="TARGET">TARGET</option>
								<option value="TFRAME">TFRAME</option>
								<option value="TMOUS" selected="selected">TMOUS</option>
								<option value="TMOUS_EF">TMOUS_EF</option>
								<option value="TMOUS_EF_ON">TMOUS_EF_ON</option>
								<option value="TMOUS_ES">TMOUS_ES</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_FA">TMOUS_FA</option>
								<option value="TMOUS_FA_EF">TMOUS_FA_EF</option>
								<option value="TMOUS_FA_EF_ON">TMOUS_FA_EF_ON</option>
								<option value="TMOUS_FA_ES">TMOUS_FA_ES</option>
								<option value="TMOUS_FA_ES_ON">TMOUS_FA_ES_ON</option>
								<option value="TMOUS_FA_ON">TMOUS_FA_ON</option>
								<option value="TMOUS_MB">TMOUS_MB</option>
								<option value="TMOUS_MB_ON">TMOUS_MB_ON</option>
								<option value="TMOUS_ON">TMOUS_ON</option>
								<option value="TMOUS_TF">TMOUS_TF</option>
								<option value="TMOUS_TF_ON">TMOUS_TF_ON</option>
								<option value="TRACFONE">TRACFONE</option>
								<option value="WALMART">WALMART</option>
								<option value="OTH">Other</option>

							</select> <input type="text" class="fieldset" id="smscCN" name="smscCN"
								value="TMOUS"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" class="fieldset" id="smscCN" value="TMOUS"> -->
							<label class="label1">Mail</label> <input type="text"
								class="fieldset" id="smscMail"> <label class="label1">Given
								Name</label> <input type="text" class="fieldset" id="smscGivenName">
							<label class="label1">SN <span style="color: red;">*</span>
							</label> <select name="smscSNSelect" id="smscSNSelect"
								class="fieldsetSelect" onChange="addSupplierSNdisplay();">
								<option value="TMO" selected="selected">TMO</option>
								<option value="MVE">MVE</option>
								<option value="IDT">IDT</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscSN" name="smscSN"
								value="TMO"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" class="fieldset" id="smscSN" value="TMO"> -->
							<!-- <label class="label1">Unique Identifier</label> -->
							<input type="hidden" class="fieldset" id="smscUniqueIdentifier">
							<label class="label1">User Agent Profile</label> <select
								name="smscUserAgentProfileSelect"
								id="smscUserAgentProfileSelect" class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscUserAgentProfileSelect','smscUserAgentProfile');">
								<option value="Postpaid" selected="selected">Postpaid</option>
								<option value="Controlled">Controlled</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscUserAgentProfile"
								name="smscUserAgentProfile" value="Postpaid"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" class="fieldset" id="smscUserAgentProfile" value="Postpaid"> -->
							<label class="label1">User Agent String</label> <select
								name="smscUserAgentStringSelect" id="smscUserAgentStringSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscUserAgentStringSelect','smscUserAgentString');">
								<option value="Active" selected="selected">Active</option>
								<option value="Hard Suspended">Hard Suspended</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscUserAgentString"
								name="smscUserAgentString" value="Active"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--   <input type="text" class="fieldset" id="smscUserAgentString" value="Active"> -->
							<label class="label1">Prepaid <span style="color: red;">*</span>
							</label> <select name="prepaid" id="smscPrepaid" class="fieldsetSelect"
								onchange="showPrepaidDiv(this);">
								<option value="yes" selected="selected">Yes</option>
								<option value="no">No</option>
							</select>
							<!-- Prepaid Attributes -->
							<div id="prepaidDiv" style="display: inline;">
								<label class="label1">TPLACODE <span style="color: red;">*</span>
								</label> <select class="fieldsetSelect" name="smscTplaCodeSel"
									id="smscTplaCodeSel"
									onChange="addSupplierUserAgentProf('smscTplaCodeSel','smscTplaCode');">
									<option value="TPLA0">TPLA0</option>
									<option value="TPLA1" selected="selected">TPLA1</option>
									<option value="TPLA2">TPLA2</option>
									<option value="OTH">Other</option>
								</select> <input type="text" class="fieldset" id="smscTplaCode"
									name="smscTplaCode" value="TPLA1"
									style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
								<!-- <input type="text" class="fieldset" id="smscTplaCode" value="TPLA1"> -->
								<label class="label1">Smsc Sub Prepaid</label> <input
									type="text" class="fieldset" id="smscsubprepaid"> <label
									class="label1">PSA</label> <input type="text" class="fieldset"
									id="smscPAS" value="PSA1"> <label class="label1">PSA
									INPCODE</label> <input type="text" class="fieldset" id="smscInpCode"
									value="TMO"> <label class="label1">PSA Services</label>
								<input type="text" class="fieldset" id="smscPSAServices">
								<label class="label1">PSA Force Flag </label> <select
									name="prepaid" id="psaforceflag" class="fieldsetSelect">
									<option value="0">Select</option>
									<option value="yes" selected="selected">Yes</option>
									<option value="no">No</option>
								</select> <label class="label1">PSA Action</label> <input type="text"
									class="fieldset" id="psaaction"> <label class="label1">PSA
									MSISDN</label> <input type="text" class="fieldset" id="psamsisdn">
							</div>
							<!-- End -->

							<label class="label1">Language <span style="color: red;">*</span>
							</label> <select name="smscLanguageSelect" id="smscLanguageSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscLanguageSelect','smscLanguage');">
								<option value="">Select</option>
								<option value="En" selected="selected">En</option>
								<option value="SP">Sp</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscLanguage"
								name="smscLanguage" value="En"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="smscLanguage" class="fieldset"> -->
							<label class="label1">Mobile Type <span
								style="color: red;">*</span>
							</label> <select name="smscMobileTypeSelect" id="smscMobileTypeSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('smscMobileTypeSelect','smscMobileType');">
								<option value="Nokia 5180i">Select</option>
								<option value="Nokia 5180i" selected="selected">Nokia
									5180i</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="smscMobileType"
								name="smscMobileType" value="Nokia 5180i"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="smscMobileType" class="fieldset"> -->
							<label class="label1">Password</label> <input type="text"
								id="smscPassword" class="fieldset">
							<button id="addSMSCSubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">Add SMSC Subscriber</button>

						</div>
					</div>
				</div>
			</div>


			<div id="mmscAddSubscriberDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForMMSCAddSubscriber">MMSC
						Add Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="mmscAddSubscriberMsisdn" class="fieldset">
							<label class="label1">SubStatus</label> <select
								name="mmscAddSubStatus" id="mmscAddSubStatus"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="ACT">ACT</option>
								<option value="DAC">DAC</option>
								<option value="RSM">RSM</option>
								<option value="SUS">SUS</option>
								<option value="RST">RST</option>
							</select>


							<!--  <input type="text" id="mmscAddSubStatus" class="fieldset" /> -->
							<label class="label1">COS</label> <select name="mmscAddCOS"
								id="mmscAddCOS" class="fieldsetSelect">
								<option value="">Select</option>
								<option value="Mav_Cos">Mav_Cos</option>
								<option value="Cos_Bocked">Cos_Bocked</option>
								<option value="Sender_Blocked">Sender_Blocked</option>
								<option value="mav_block">mav_block</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="27">27</option>
								<option value="25">25</option>
								<option value="32">32</option>
								<option value="26">26</option>
								<option value="31">31</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="33">33</option>
								<option value="37">37</option>
								<option value="78">78</option>
								<option value="34">34</option>
								<option value="35">35</option>
							</select>
							<!--  <input type="text" id="mmscAddCOS" class="fieldset" /> -->
							<label class="label1">Sub Type</label> <select
								id="mmscAddSubType" class="fieldset" style="width: 161px;">
								<option value="">Select</option>
								<option value="POSTPAID">POSTPAID</option>
								<option value="PREPAID">PREPAID</option>
							</select>
							<!--  <input type="text" id="mmscAddSubType" class="fieldset" /> -->
							<label class="label1">HandSet Type</label> <select
								name="mmscAddHandSetType" id="mmscAddHandSetType"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="other_mms_phone">other_mms_phone</option>
								<option value="LegacyPhone">LegacyPhone</option>
							</select>
							<!--  <input type="text" id="mmscAddHandSetType" class="fieldset" /> -->
							<label class="label1">MMSCapable</label> <select
								name="mmscAddMMSCapable" id="mmscAddMMSCapable"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="true">TRUE</option>
								<option value="false">FALSE</option>
							</select>
							<!--  <input type="text" id="mmscAddMMSCapable" class="fieldset" /> -->
							<label class="label1">Operator Id</label> <select
								name="mmscAddOperatorId" id="mmscAddOperatorId"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="ARGOS">ARGOS</option>
								<option value="CINTEX">CINTEX</option>
								<option value="Default">Default</option>
								<option value="GOSMART">GOSMART</option>
								<option value="LYCA">LYCA</option>
								<option value="M2M">M2M</option>
								<option value="METRO">METRO</option>
								<option value="MVNE">MVNE</option>
								<option value="MVNE2">MVNE2</option>
								<option value="NOVA">NOVA</option>
								<option value="PLINTRON">PLINTRON</option>
								<option value="ROMO">ROMO</option>
								<option value="SIMPLE">SIMPLE</option>
								<option value="SOLAVEI">SOLAVEI</option>
								<option value="SOUTHERNLINC">SOUTHERNLINC</option>
								<option value="TARGET">TARGET</option>
								<option value="TFRAME">TFRAME</option>
								<option value="TMOUS">TMOUS</option>
								<option value="TMOUS_EF">TMOUS_EF</option>
								<option value="TMOUS_EF_ON">TMOUS_EF_ON</option>
								<option value="TMOUS_ES">TMOUS_ES</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_FA">TMOUS_FA</option>
								<option value="TMOUS_FA_EF">TMOUS_FA_EF</option>
								<option value="TMOUS_FA_EF_ON">TMOUS_FA_EF_ON</option>
								<option value="TMOUS_FA_ES">TMOUS_FA_ES</option>
								<option value="TMOUS_FA_ES_ON">TMOUS_FA_ES_ON</option>
								<option value="TMOUS_FA_ON">TMOUS_FA_ON</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_MB">TMOUS_MB</option>
								<option value="TMOUS_MB_ON">TMOUS_MB_ON</option>
								<option value="TMOUS_ON">TMOUS_ON</option>
								<option value="TMOUS_TF">TMOUS_TF</option>
								<option value="TMOUS_TF_ON">TMOUS_TF_ON</option>
								<option value="TRACFONE">TRACFONE</option>
							</select>
							<!-- <input type="text" id="mmscAddOperatorId" class="fieldset"> -->
							<label class="label1">Profile Id</label> <select
								name="mmscAddProfileId" id="mmscAddProfileId"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
							</select>
							<!-- <input type="text" id="mmscAddProfileId" class="fieldset"> -->
							<label class="label1">IMSEnabled</label> <select
								name="mmscAddImsEnabled" id="mmscAddImsEnabled"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="TRUE">TRUE</option>
								<option value="FALSE">FALSE</option>
							</select>
							<!-- <input type="text" id="mmscAddImsEnabled" class="fieldset" /> -->

							<button id="mmscAddSubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">MMSC Add Subscriber</button>
						</div>
					</div>
				</div>
			</div>

			<!--  Put Subscriber  -->
			<div id="putSubscriberDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForPutSubscriber">Add/Modify
						Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">
							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="putSubscriberMsisdn" class="fieldset">
							<label class="label1">SubStatus<span style="color: red;">*</span></label>
							<select name="putSubStatusSelect" id="putSubStatusSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('putSubStatusSelect','putSubStatus');">
								<option value="">Select</option>
								<option value="ACT">ACT</option>
								<option value="DAC">DAC</option>
								<option value="RSM">RSM</option>
								<option value="SUS">SUS</option>
								<option value="RST">RST</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="putSubStatus"
								name="putSubStatus" value=""
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="putSubStatus" class="fieldset" /> -->
							<label class="label1">COS<span style="color: red;">*</span></label>
							<select name="putCOSSelect" id="putCOSSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('putCOSSelect','putCOS');">
								<option value="">Select</option>
								<option value="Mav_Cos">Mav_Cos</option>
								<option value="Cos_Bocked">Cos_Bocked</option>
								<option value="Sender_Blocked">Sender_Blocked</option>
								<option value="mav_block">mav_block</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="27">27</option>
								<option value="25">25</option>
								<option value="32">32</option>
								<option value="26">26</option>
								<option value="31">31</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="33">33</option>
								<option value="37">37</option>
								<option value="78">78</option>
								<option value="34">34</option>
								<option value="35">35</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="putCOS" name="putCOS"
								value=""
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="putCOS" class="fieldset" /> -->
							<label class="label1">Sub Type<span style="color: red;">*</span></label>
							<select id="putSubTypeSelect" class="fieldset"
								style="width: 161px;"
								onChange="addSupplierUserAgentProf('putSubTypeSelect','putSubType');">
								<option value="">Select</option>
								<option value="POSTPAID">POSTPAID</option>
								<option value="PREPAID">PREPAID</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="putSubType"
								name="putSubType" value=""
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" id="putSubType" class="fieldset" /> -->
							<label class="label1">HandSet Type</label> <select
								name="putHandSetTypeSelect" id="putHandSetTypeSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('putHandSetTypeSelect','putHandSetType');">
								<option value="">Select</option>
								<option value="other_mms_phone">other_mms_phone</option>
								<option value="LegacyPhone">LegacyPhone</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="putHandSetType"
								name="putHandSetType" value=""
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="putHandSetType" class="fieldset" /> -->
							<label class="label1">MMSCapable<span style="color: red;">*</span></label>
							<select name="putMMSCapable" id="putMMSCapable"
								class="fieldsetSelect">
								<option value="">Select</option>
								<option value="true">TRUE</option>
								<option value="false">FALSE</option>
							</select>
							<!--  <input type="text" id="putMMSCapable" class="fieldset" /> -->
							<label class="label1">Operator Id</label> <select
								name="putOperatorIdSelect" id="putOperatorIdSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('putOperatorIdSelect','putOperatorId');">
								<option value="">Select</option>
								<option value="ARGOS">ARGOS</option>
								<option value="CINTEX">CINTEX</option>
								<option value="Default">Default</option>
								<option value="GOSMART">GOSMART</option>
								<option value="LYCA">LYCA</option>
								<option value="M2M">M2M</option>
								<option value="METRO">METRO</option>
								<option value="MVNE">MVNE</option>
								<option value="MVNE2">MVNE2</option>
								<option value="NOVA">NOVA</option>
								<option value="PLINTRON">PLINTRON</option>
								<option value="ROMO">ROMO</option>
								<option value="SIMPLE">SIMPLE</option>
								<option value="SOLAVEI">SOLAVEI</option>
								<option value="SOUTHERNLINC">SOUTHERNLINC</option>
								<option value="TARGET">TARGET</option>
								<option value="TFRAME">TFRAME</option>
								<option value="TMOUS">TMOUS</option>
								<option value="TMOUS_EF">TMOUS_EF</option>
								<option value="TMOUS_EF_ON">TMOUS_EF_ON</option>
								<option value="TMOUS_ES">TMOUS_ES</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_FA">TMOUS_FA</option>
								<option value="TMOUS_FA_EF">TMOUS_FA_EF</option>
								<option value="TMOUS_FA_EF_ON">TMOUS_FA_EF_ON</option>
								<option value="TMOUS_FA_ES">TMOUS_FA_ES</option>
								<option value="TMOUS_FA_ES_ON">TMOUS_FA_ES_ON</option>
								<option value="TMOUS_FA_ON">TMOUS_FA_ON</option>
								<option value="TMOUS_ES_ON">TMOUS_ES_ON</option>
								<option value="TMOUS_MB">TMOUS_MB</option>
								<option value="TMOUS_MB_ON">TMOUS_MB_ON</option>
								<option value="TMOUS_ON">TMOUS_ON</option>
								<option value="TMOUS_TF">TMOUS_TF</option>
								<option value="TMOUS_TF_ON">TMOUS_TF_ON</option>
								<option value="TRACFONE">TRACFONE</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="putOperatorId"
								name="putOperatorId" value=""
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!--  <input type="text" id="putOperatorId" class="fieldset"> -->
							<label class="label1">SMTPGWDomain</label> <input type="text"
								id="putProfileId" class="fieldset"
								value="tmo-dev.msg.lab.t-mobile.com">
							<!--  <label class="label1">IMSEnabled</label>
                                <input type="text" id="putImsEnabled" class="fieldset" /> -->

							<button id="putSubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">Put Subscriber</button>
						</div>
					</div>
				</div>
			</div>

			<!--  Modify Subscriber  -->
			<div id="modifySubscriberDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForModifySubscriber">Modify
						Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">

							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="modifySubscriberMsisdn" class="fieldset">
							<label class="label1">Sub Type<span style="color: red;">*</span></label>
							<!--<input type="text" id="modifySubType" class="fieldset"> -->
							<select id="modifySubTypeSelect" class="fieldset"
								style="width: 161px;"
								onChange="addSupplierUserAgentProf('modifySubTypeSelect','modifySubType');">
								<option value="">Select</option>
								<option value="POSTPAID">POSTPAID</option>
								<option value="PREPAID">PREPAID</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="modifySubType"
								name="modifySubType"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<label class="label1">Profile Id</label> <select
								name="profileIdUpdateSelect" id="profileIdUpdateSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('profileIdUpdateSelect','profileIdUpdate');">
								<option value="">Select</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="profileIdUpdate"
								name="profileIdUpdate"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">
							<!-- <input type="text" id="profileIdUpdate" class="fieldset"> -->
							<label class="label1">IMSEnabled</label>
							<!--  <input type="text" id="imsEnabledUpdate" list="IMSEnabledValUpdate" class="fieldset" /> -->
							<!-- <datalist id="IMSEnabledValUpdate">
                                    <option value="TRUE">TRUE </option>
                                    <option value="FALSE">FALSE</option>

                                </datalist> -->
							<select name="imsEnabledUpdateSelect" id="imsEnabledUpdateSelect"
								class="fieldsetSelect"
								onChange="addSupplierUserAgentProf('imsEnabledUpdateSelect','imsEnabledUpdate');">
								<option value="">Select</option>
								<option value="TRUE">TRUE</option>
								<option value="FALSE">FALSE</option>
								<option value="OTH">Other</option>
							</select> <input type="text" class="fieldset" id="imsEnabledUpdate"
								name="imsEnabledUpdate"
								style="margin-left: 368px; margin-top: -44px; visibility: hidden;">

							<button id="modifySubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">Modify Subscriber</button>
						</div>
					</div>
				</div>
			</div>

			<!--  Delete Subscriber  -->
			<div id="deleteSubscriberDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForDeleteSubscriber">Delete
						Subscriber:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">

							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="deleteSubscriberMsisdn"
								class="form-control">
							<button id="deleteSubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">Delete Subscriber</button>
						</div>
					</div>
				</div>
			</div>
			<!--  Delete Subscriber For Service Specific  -->
			<div id="deleteSubscriberServiceDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading"
						id="panelHeadingForDeleteSubscriberService">Delete
						Subscriber Service Specific:</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">

							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="deletespecificSubscriberMsisdn"
								class="form-control">
							<!-- <label class="label1">Service Specific</label> -->
							<!-- <input type="text" id="deleteServiceSubscriber" class="fieldset"> -->
							<button id="deleteSubscriberServiceBtn" class="btn btn-primary"
								onClick="getedatData(this);">Delete Subscriber</button>
						</div>
					</div>
				</div>
			</div>

			<!--  Get Subscriber   -->
			<div id="getSubscriberDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForGetSubscriber">Get
						Subscriber</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">

							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="getsubscriberMsisdn" class="form-control">

							<button id="getSubscriberBtn" class="btn btn-primary"
								onClick="getedatData(this);">Get Subscriber</button>
						</div>
					</div>
				</div>

				<div class="panel panel-primary" style="display: none;"
					id="panelDiv">
					<div class="panel-heading" id="getSubscriberContentHeading"
						style="display: none;"></div>
					<div id="getSubscriberContentBody" class="panel-body"
						style="display: none;"></div>

				</div>
			</div>

			<!--  Get Subscriber For Service Specific  -->
			<div id="getSubscriberServiceDiv" style="display: none">
				<div class="panel panel-primary">
					<div class="panel-heading" id="panelHeadingForGetSerciceSubscriber">Get
						Subscriber For Service Specific</div>
					<div class="panel-body">
						<h5>
							<b><i>Note</i>:Required fields are indicated with the red
								asterisk(<span style="color: red;">*</span>).</b>
						</h5>
						<div id="searchBox" class="input-group">

							<label class="label1">MSISDN<span style="color: red;">*</span>
							</label> <input type="text" id="getServiceSubscriberMsisdn"
								class="form-control">
							<!-- <label class="label1">Service Specific</label> -->
							<!-- <input type="text" id="serviceSpecific" class="fieldset"> -->

							<button id="getSubscriberServiceBtn" class="btn btn-primary"
								onClick="getedatData(this);">Get Subscriber</button>
						</div>
					</div>
				</div>

				<div class="panel panel-primary" style="display: none;"
					id="panelDivService">
					<div class="panel-heading" id="getSubscriberServiceContentHeading"
						style="display: none;"></div>
					<div id="getSubscriberServiceContentBody" class="panel-body"
						style="display: none;"></div>

				</div>
			</div>

			<div class="panel panel-primary" style="display: none;" id="msgInfo">
				<div class="panel-heading" id="msgContentHeading"
					style="display: none;"></div>
				<div id="msgContentBody" class="panel-body" style="display: none;"></div>
			</div>
			<div id="nap-tab" style="padding: 10px;"></div>
		</div>
		<div id="loading" style="padding-top: 158px;"></div>
	</div>
</body>

<jsp:include page="/views/footer.jsp"></jsp:include>

</html>