<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tmobile.filter.store.LinkData"%>
<script type="text/javascript" src="<c:url value="/resources/js/paggination.js" />"></script>
<%
	Map<String,Object>dataMap=new HashMap<String,Object>();
  		List<LinkData> linkData = new ArrayList<LinkData>();
	  boolean showFlag=false;
	  boolean existingUserFlag=false;
	  boolean saveFlag=false;
	  String message="";
	  int currentPage=1;
	  int totalRecords=0;
	  
	  if(request.getAttribute("dataMap")!=null)
	  {
		  dataMap=(Map)request.getAttribute("dataMap");
	  }
	  if(dataMap.get("linkData")!=null){
		  linkData = (List<LinkData>)dataMap.get("linkData");
	  }
	  
	  if(dataMap.get("currentPage")!=null){
		  currentPage = (Integer)dataMap.get("currentPage");
	  }
	  
	  if(dataMap.get("totalRecords")!=null){
		  totalRecords = (Integer)dataMap.get("totalRecords");
	  }
%>
<div id="pageNavPosition"></div>
<div id="showReport"></div>
<style type="text/css">
.pg-normal {
    color: #0000FF;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
}
 
.pg-selected {
    color: #800080;
    font-weight: bold;
    text-decoration: underline;
    cursor: pointer;
}
</style>

<script>
<% 
int counter=1;
boolean finalCall=false;
if(linkData!=null && linkData.size()>0){
for(LinkData l : linkData){
	if(counter==linkData.size()){
		finalCall=true;
	}
%>
showReport('<%=l.getLoginId()%>','<%=l.getMessageData()%>','<%=l.getLogoutTime()%>','<%=finalCall%>','<%=counter%>');
<%
counter++;
}
}%>
</script>

	<script type="text/javascript">
        var pager = new Pager('results', 30); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    </script>