<%@page import="com.tmobile.edat.util.Constants"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tmobile.edat.admin.model.ServerConfig"%>
<meta charset="ISO-8859-1"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>Change Server Setting</title>
	<link rel="stylesheet" href="resources/css/bootstrap/bootstrap-table.css"/>
	<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet"/>
	<link href="resources/css/bootstrap/bootstrap-select.min.css" rel="stylesheet"/>
	<link href="resources/css/bootstrap/glyphicons.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
	<script type="text/javascript" src="resources/js/admin.js"></script>
	<script src="<c:url value='/resources/lib/jquery-ui.js'/>"></script>
	<script type="text/javascript">
	document.title="Configure Server";
	$(document).ready(function(){
		$("ul.sb #server_config").parent().addClass("selected");
	});
	window.history.forward();	
		</script>
		<style>
		.clear {
			height: 20px;;
			}
		</style>
<%
	Map<String,Object>dataMap=new HashMap<String,Object>();
  		List<ServerConfig> serverConfig = new ArrayList<ServerConfig>();
	  boolean showFlag=false;
	  boolean existingUserFlag=false;
	  boolean saveFlag=false;
	  String message="";
	  
	  if(request.getAttribute("dataMap")!=null)
	  {
		  dataMap=(Map)request.getAttribute("dataMap");
	  }
	  if(dataMap.get("serverConfig")!=null){
		  serverConfig = (List<ServerConfig>)dataMap.get("serverConfig");
	  }
	  String activeServer="Default Server Setting is active !!";
	    
%>
<form class="form-inline" action="changeServerConfig" method="post"
	id="server_setting" name="server_setting"
	onsubmit="return isServerSelected();" autocomplete="off">
			<input type="hidden" name="csrfPreventionSalt" value="<c:out value='<%=request.getAttribute("csrfPreventionSalt") %>'/>"/>
				<input type="hidden" name="saveData" value="1"/>		
	<div class="panel panel-primary">
	  <div class="panel-heading">Configure Server:</div>
	  <div class="panel-body">
	  	<div style="color: red; font-weight: bold; margin-left: 100px; font-size: 30px; " id="msg_div">
<h5><%if(dataMap.get("message")!=null && !dataMap.get("message").equals("")){out.print(dataMap.get("message"));} %></h5>
</div>
<div class="Clear"></div>
	    <div id="active_server" style="color: green; font-weight: bold;  font-size: 16px; "></div>
		<div class="Clear"></div><div class="Clear"></div><div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Select Server</label> <select class="fieldset"
				id="server_id" name="server_id" style="width:200px;" onChange="showIP(this.value,'1');">
				<option value="0">--Select--</option>
	
				<%
					for (ServerConfig s : serverConfig) {
				%>
				<option value="<%=s.getId()%>" <% if(s.getId()==Constants.DEFAULT_SERVER){ out.print(" selected ");} %> title="<%=s.getServerName()%>"><%=s.getServerName()%></option>
	
				<%
					}
				%>
			</select>
		<!-- 	<button id="apply" class="btn btn-primary">Apply</button> -->
			<div class="Clear"></div>
			<%
					for (ServerConfig s : serverConfig) {
						if(Constants.DEFAULT_SERVER==s.getId()){
							activeServer = s.getServerName()+" Setting is active !!";
						}
				%>
				<div id="ip_show"></div>
				<div id="ip_<%=s.getId()%>" style="display:none;">
				<table class='table table-striped'><tr><th>Node</th><th>IP Address</th></tr>
				<%

				String[] serverIPDetaildsArr = s.getServerIPDetails().split(";");
				if(serverIPDetaildsArr!=null && serverIPDetaildsArr.length==22){					
				out.print("<tr><td>LDAP_SERVER</td><td>"+serverIPDetaildsArr[0].substring(12, serverIPDetaildsArr[0].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_NAP</td><td>"+serverIPDetaildsArr[1].substring(16, serverIPDetaildsArr[1].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_DUS</td><td>"+serverIPDetaildsArr[2].substring(16, serverIPDetaildsArr[2].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVERL_PCRF</td><td>"+serverIPDetaildsArr[3].substring(18, serverIPDetaildsArr[3].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_UMA</td><td>"+serverIPDetaildsArr[4].substring(16, serverIPDetaildsArr[4].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_HLR</td><td>"+serverIPDetaildsArr[5].substring(16, serverIPDetaildsArr[5].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_EIR</td><td>"+serverIPDetaildsArr[6].substring(16, serverIPDetaildsArr[6].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_HSS</td><td>"+serverIPDetaildsArr[7].substring(16, serverIPDetaildsArr[7].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_PCRF_SESSION</td><td>"+serverIPDetaildsArr[8].substring(25, serverIPDetaildsArr[8].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_CNTB</td><td>"+serverIPDetaildsArr[9].substring(17, serverIPDetaildsArr[9].length())+"</td></tr>");
				out.print("<tr><td>CNTB_StringUrl</td><td>"+serverIPDetaildsArr[10].substring(15, serverIPDetaildsArr[10].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_MSISDN</td><td>"+serverIPDetaildsArr[11].substring(19, serverIPDetaildsArr[11].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_BAN</td><td>"+serverIPDetaildsArr[12].substring(16, serverIPDetaildsArr[12].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_IAM</td><td>"+serverIPDetaildsArr[13].substring(16, serverIPDetaildsArr[13].length())+"</td></tr>");
				out.print("<tr><td>LDAP_SERVER_SMSC</td><td>"+serverIPDetaildsArr[14].substring(17, serverIPDetaildsArr[14].length())+"</td></tr>");
				out.print("<tr><td>SERVER_MMSC</td><td>"+serverIPDetaildsArr[15].substring(12, serverIPDetaildsArr[15].length())+"</td></tr>");
				out.print("<tr><td>SMSC_API_URL</td><td>"+serverIPDetaildsArr[16].substring(13, serverIPDetaildsArr[16].length())+"</td></tr>");
				out.print("<tr><td>OTA_SERVER_URL</td><td>"+serverIPDetaildsArr[17].substring(15, serverIPDetaildsArr[17].length())+"</td></tr>");
				out.print("<tr><td>INSDP_API_URL</td><td>"+serverIPDetaildsArr[18].substring(14, serverIPDetaildsArr[18].length())+"</td></tr>");	
				out.print("<tr><td>INSDP_StringUrl</td><td>"+serverIPDetaildsArr[19].substring(16, serverIPDetaildsArr[19].length())+"</td></tr>");	
				out.print("<tr><td>INSDP_HOST</td><td>"+serverIPDetaildsArr[20].substring(11, serverIPDetaildsArr[20].length())+"</td></tr>");		
				out.print("<tr><td>TELNET_HOST</td><td>"+serverIPDetaildsArr[21].substring(12, serverIPDetaildsArr[21].length())+"</td></tr>");			
				}
				%>
				</table>
				</div>
			<div class="Clear"></div>
				<%
					}
				%>
			
		</div>		
	</div>
	</div>
	<script>	
	showIP('<%=Constants.DEFAULT_SERVER%>','0');
	$('#active_server').html('<%=activeServer%>');
	</script>
</form>