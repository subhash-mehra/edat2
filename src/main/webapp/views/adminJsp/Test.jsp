
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tmobile.edat.admin.model.*"%>
<%
	Map<String, Object> dataMap = new HashMap<String, Object>();
	List<UserAccess> usersList = new ArrayList<UserAccess>();
	;
	boolean showFlag = false;
	boolean existingUserFlag = false;
	boolean saveFlag = false;
	String message = "";

	if (request.getAttribute("dataMap") != null) {
		dataMap = (Map) request.getAttribute("dataMap");
	}

	if (dataMap.get("usersList") != null) {
		usersList = (List) dataMap.get("usersList");

	}

	if (dataMap.get("message") != null) {
		message = (String) dataMap.get("message");

	}
%>

<%
if (dataMap.get("message") != null) {
%>
<form name="message">
	<div
		style="color: red; font-weight: bold; margin-left: 400px; font-size: 30px;">
		<h5><%=message%></h5>
	</div>
</form>
<%
}
%>


<script type="text/javascript">
	$(document).ready(function(){
		$("ul.sb #showUser").parent().addClass("selected");
	});
</script>

<form class="form-inline" role="form" method="post" id="userRequest"
	name="userRequest">
	
	
	
	<%
		if (usersList.size() > 0) {
	%>
	<div id="users" class="panel panel-primary">
	  <div class="panel-heading">Users:</div>
	  <div class="panel-body">
		<table class="table" >
				<thead>

					<tr>
						<th><div style="text-align: center;">User Name</div></th>
						<th><div style="text-align: center;">Login Name</div></th>
						<th><div style="text-align: center;">Role</div></th>
						<th style="width:200px;">Note</th>
						<th><div style="text-align: center;">Created Date</div></th>
						<th><div style="text-align: center;">Edit User</div></th>
						<th><div style="text-align: center;">Delete User</div></th>
					</tr>
				</thead>
				<tbody>
					<%
						String userName = "";

							for (UserAccess userAccess : usersList) {
								if (userAccess.getFirstName() != null) {
									userName = userAccess.getFirstName();
								}
								if (userAccess.getLastName() != null) {
									userName = userName + " " + userAccess.getLastName();
								}
					%>
					<tr>

						<td><div style="text-align: center;"><%=userName%></div></td>
						<td><div style="text-align: center;">
							<%
								if (userAccess.getUserId() != null) {
							%> <%=userAccess.getUserId()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
 						</div>
						</td>
						<td>
						<div style="text-align: center;">
							<%
								if (userAccess.getRoleName() != null) {
							%> <%=userAccess.getRoleName()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
						</div>
						</td>
						<td>
							<%
								if (userAccess.getNote() != null) {
							%> <%=userAccess.getNote()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
 						
						</td>
						<td><div style="text-align: center;">
							<%
								if (userAccess.getCreateDate() != null) {
							%> <%=userAccess.getCreateDate()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
						
						</div>
						</td>

						
						<%if(!userAccess.getRoleName().equalsIgnoreCase("admin")){ %>
						<td><div style="text-align: center;">
							
								<button type="submit" class="gridAddButton"
									onclick="showUpdateUser('edit','<%=userAccess.getUserId()%>')">Edit</button>

							</div>
							
						</td>

						<td>
						<div style="text-align: center;">
							
								<button type="submit" class="gridAddButton"
									onclick="showUpdateUser('delete','<%=userAccess.getUserId()%>')">Delete</button>

							</div>
							
						</td>
						
						<%}else{ %>
						<td>
						<div style="text-align: center;">
						<button type="submit" class="gridAddButton" onclick="showUpdateUser('edit','<%=userAccess.getUserId()%>')" disabled="disabled"">Edit</button>
						</div>
						</td>
						<td>
						<div style="text-align: center;">
						<button type="submit" class="gridAddButton" onclick="showUpdateUser('delete','<%=userAccess.getUserId()%>')" disabled="disabled">Delete</button>
						</div>
						</td>
							
							<%} %>

					</tr>
					<%
						}
					%>
				</tbody>

			</table>
		</div>
	</div>
	<%
		} else {
	%>
	<table id="" name="">
		<thead>
			<tr>
				<td>User Name</td>
				<td>Login Name</td>
				<td>Role</td>
				<td>Note</td>
				<td>Created Date</td>
			</tr>
		</thead>
		<%
			}
		%>
		<input type="hidden" name="csrfPreventionSalt"
			value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
			</table>
			
		</form>
		
		