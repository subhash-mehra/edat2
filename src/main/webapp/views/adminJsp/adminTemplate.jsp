<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn"%>
<%@page import="com.tmobile.edat.admin.dto.request.UserRole"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<c:if test='${!(fn:contains(sessionScope.roleList, "ADMIN"))}'>
<c:redirect url="/accessError"></c:redirect>
</c:if>
<jsp:include page="/views/header.jsp"></jsp:include>

<script type="text/javascript">
document.title="Register New User";
	$(document).ready(function() {
		$("#admin").parent().addClass("active");

	});
</script>

<body>

	<div class="row" style="height: 25px; background-color: #eee"></div>
	<div class="row" id="contentMain">


		<form name="adminTemplate" id="adminTemplate" method="get"
			class="col-lg-2" style="padding: 0 0 0 15px;">
			<ul class="sb" style="width: 100%">
				<li><a id="newUser"
					href="javascript:OnSubmitFormForShowUpdateJsp('save')">Register
						New User</a></li>
				<li><a id="showUser"
					href="javascript:OnSubmitFormForShowUpdateJsp('update')">Show
						Users</a></li>
				<li><a id="report" href="javascript:OnSubmitFormForShowUpdateJsp('report')">Reports</a></li>
				<li><a id="server_config" href="javascript:OnSubmitFormForShowUpdateJsp('server_config')">Configure Server</a></li>
				<!-- <li><a id="executeQuery" href="javascript:OnSubmitFormForShowUpdateJsp('executeQuery')">Execute Query</a></li> -->
				
			</ul>
			<input type="hidden" name="csrfPreventionSalt"
				value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
		</form>

		<div id="contentJspId" class="contentJspDiv col-lg-10">


			<%
				Map<String,Object>dataMap=new HashMap<String,Object>();
					
						 String show="";
						 String contentJsp="";
						
						if(request.getAttribute("dataMap")!=null)
						{
					  dataMap=(Map)request.getAttribute("dataMap");
						}
						
						if(dataMap.get("show")!=null)
						{
					show=(String)dataMap.get("show");
						}
						
						if(show.equalsIgnoreCase("true"))
						{
					List<UserRole>roleList=new ArrayList<UserRole>();
					  if(dataMap.get("roleList")!=null)
					  {
						  roleList=(List)dataMap.get("roleList");
						 
					  }
			%>
			<script type="text/javascript">
				$(document).ready(function() {
					$("ul.sb #newUser").parent().addClass("selected");
				});
			</script>


			<form class="form-inline" role="form" action="saveUser" method="post"
				id="userRequest" name="userRequest"
				onsubmit="return validateUserRegisterForm();">

				<div class="panel panel-primary">
					<div class="panel-heading">Create New User:</div>
					<div class="panel-body">
					<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
						<div class="form-group">
							<label class="label1">First Name<span style="color: red;">*</span></label> <input type="text"
								class="fieldset" id="fname" name="fname" maxlength="45"
								placeholder="Enter First Name">
						</div>
						<div class="Clear"></div>
						<div class="form-group">

							<label class="label1">Last Name<span style="color: red;">*</span></label> <input type="text"
								maxlength="45" class="fieldset" id="lname" name="lname"
								placeholder="Enter Second Name">
						</div>
						<div class="Clear"></div>
						<div class="form-group">

							<label class="label1">Login Name<span style="color: red;">*</span></label> <input type="text"
								maxlength="30" class="fieldset" id="loginname" name="loginname"
								placeholder="Enter Login Name">
						</div>
						<div class="Clear"></div>
						<div class="form-group">

							<label class="label1">Password<span style="color: red;">*</span></label> <input type="password"
								maxlength="30" class="fieldset" id="pwd" name="pwd"
								placeholder="Enter password">
						</div>
						<div class="Clear"></div>
						<div class="form-group">

							<label class="label1">Confirm Password<span style="color: red;">*</span></label> <input
								type="password" maxlength="30" class="fieldset" id="c_pwd"
								name="c_pwd" placeholder="Enter confirm password">
						</div>
						<div class="Clear"></div>
						<div class="form-group">

							<label class="label1">Role<span style="color: red;">*</span></label> <select class="fieldset"
								id="addUserRoleSelector" name="addUserRoleSelector"
								multiple="multiple">
								<option value="0">Select Role</option>

								<%
									for (UserRole userRole : roleList) {
								%>
								<option value="<%=userRole.getRoleName()%>"><%=userRole.getRoleName()%></option>

								<%
									}
								%>
							</select>
						</div>

						<div id='CharCountLabel1'></div>
						<div class="clear"></div>
						<div class="form-group" style="text-align: right;">
							<label class="label1" style="text-align: left;">Note</label>
							<textarea rows="10" cols="50" name="note" id="note"
								class="bigTextArea fieldset" maxlength="60"
								onkeyup="CharacterCount('note', 'CharCountLabel1');"
								onkeydown="CharacterCount('note', 'CharCountLabel1');"></textarea>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>

						<input type="hidden" name="csrfPreventionSalt"
							value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
					</div>
				</div>



			</form>

			<%
				} else {
					if (dataMap.get("contentJsp") != null) {

						contentJsp = (String) dataMap.get("contentJsp");
			%>
			<jsp:include page="<%=contentJsp%>" flush="false"></jsp:include>
			<%
				} else {
			%>

			<%
				}
				}
			%>

		</div>

	</div>
	
	<jsp:include page="/views/footer.jsp"></jsp:include>

</body>
