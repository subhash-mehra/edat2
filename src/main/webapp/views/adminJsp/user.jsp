
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tmobile.edat.admin.dto.request.UserRole"%>
<script type="text/javascript">
document.title="Register New User";
</script>

<%
	Map<String,Object>dataMap=new HashMap<String,Object>();
  		List<UserRole>roleList=new ArrayList<UserRole>();
	  boolean showFlag=false;
	  boolean existingUserFlag=false;
	  boolean saveFlag=false;
	  String message="";
	  
	  if(request.getAttribute("dataMap")!=null)
	  {
		  dataMap=(Map)request.getAttribute("dataMap");
	  }
	  
	  
	  if(dataMap.get("roleList")!=null)
	  {
		  roleList=(List)dataMap.get("roleList");
		 
	  }
	  
	  if(dataMap.get("showFlag")!=null)
	  {
		  showFlag=(Boolean)dataMap.get("showFlag");
		 
	  }
	  
	  if(dataMap.get("saveFlag")!=null)
	  {
		  saveFlag=(Boolean)dataMap.get("saveFlag");
		 
	  }
	
	  if(!saveFlag)
	  {
	  if(dataMap.get("existingUserFlag")!=null)
	  {
		  existingUserFlag=(Boolean)dataMap.get("existingUserFlag");
		 
	  }
	  
	  if(existingUserFlag)
	  {
		  message="User Already Exist!!";
	  }else
	  {
		  message="Can't create User Please Try again!!";
	  }
	  }else
	  {
		  message="User has been Created Successfully!!";
	  }
%>
<%
		if (!showFlag) {
	%>
	<form name="message">
<div style="color: red; font-weight: bold; margin-left: 100px; font-size: 30px; ">
<h5><%=message %></h5>
</div>
</form>
	<%
		}
	%>
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("ul.sb #newUser").parent().addClass("selected");
	});
	window.history.forward();
	
</script>

<form class="form-inline" role="form" action="saveUser" method="post"
	id="userRequest" name="userRequest"
	onsubmit="return validateUserRegisterForm();" autocomplete="off">
	
	<div class="panel panel-primary">
	  <div class="panel-heading">Create New User:</div>
	  <div class="panel-body">
	  <h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
	    <div class="form-group">
			<label class="label1">First Name<span style="color: red;">*</span></label> 
			<input type="text" class="fieldset" id="fname" name="fname" maxlength="45"
				placeholder="Enter First Name" style="float:left;">
		</div>
		<div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Last Name<span style="color: red;">*</span></label> <input type="text" maxlength="45"
				class="fieldset" id="lname" name="lname"
				placeholder="Enter Second Name" style="float:left;">
		</div>
		<div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Login Name<span style="color: red;">*</span></label> <input type="text" maxlength="30"   value=""
				class="fieldset" id="loginname" name="loginname"
				placeholder="Enter Login Name" style="float:left;">
		</div>
		<div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Password<span style="color: red;">*</span></label> <input type="password" maxlength="30"
				class="fieldset" id="pwd" name="pwd"
				placeholder="Enter password" style="float:left;">
		</div>
		<div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Confirm Password<span style="color: red;">*</span></label> <input type="password" maxlength="30"
				class="fieldset" id="c_pwd" name="c_pwd"
				placeholder="Enter confirm password" style="float:left;">
		</div>
		<div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Role<span style="color: red;">*</span></label> <select class="fieldset"
				id="addUserRoleSelector" name="addUserRoleSelector" multiple="multiple" style="float:left;">
				<option value="0" selected="selected">Select Role</option>
	
				<%
					for (UserRole userRole : roleList) {
				%>
				<option value="<%=userRole.getRoleName()%>"><%=userRole.getRoleName()%></option>
	
				<%
					}
				%>
			</select>
		</div>
		
		<div id='CharCountLabel1'></div>
		<div class="clear"></div>
		<div class="form-group" style="text-align: right;">
			<label class="label1" style="text-align: left;">Note</label>
			<textarea rows="10" cols="50" name="note" id="note" class="bigTextArea fieldset"  maxlength="60" onkeyup="CharacterCount('note', 'CharCountLabel1');" onkeydown="CharacterCount('note', 'CharCountLabel1');" style="float:left;"></textarea>
			<div style="margin-left: 300px;">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	
		<input type="hidden" name="csrfPreventionSalt" value="<c:out value='<%=request.getAttribute("csrfPreventionSalt") %>'/>"/>
	  </div>
	</div>
	
	

</form>

