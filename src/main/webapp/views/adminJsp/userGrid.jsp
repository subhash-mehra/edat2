<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="fn" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tmobile.edat.admin.model.*"%>
<script type="text/javascript" src="<c:url value="/resources/js/dynamicPagignation.js" />"></script>

<script type="text/javascript">
document.title="Show Users";
</script>
<%
	Map<String, Object> dataMap = new HashMap<String, Object>();
	List<UserAccess> usersList = new ArrayList<UserAccess>();
	;
	boolean showFlag = false;
	boolean existingUserFlag = false;
	boolean saveFlag = false;
	String message = "";

	if (request.getAttribute("dataMap") != null) {
		dataMap = (Map) request.getAttribute("dataMap");
	}

	if (dataMap.get("usersList") != null) {
		usersList = (List) dataMap.get("usersList");

	}

	if (dataMap.get("message") != null) {
		message = (String) dataMap.get("message");

	}
%>


<%
if (dataMap.get("message") != null) {
%>
<form name="message">
	<div
		style="color: red; font-weight: bold; margin-left: 400px; font-size: 30px;">
		<h5><%=message%></h5>
	</div>
</form>
<%
}
%>
<script type="text/javascript">
	$(document).ready(function(){
		$("ul.sb #showUser").parent().addClass("selected");
	});

	window.history.forward();
	
	</script>


<form class="form-inline" role="form" method="post" id="userRequest"
	name="userRequest">
	
	
	
	<%
		if (usersList.size() > 0) {
	%>
	<div id="pageNavPosition"></div>
	<div id="users" class="panel panel-primary">
	  <div class="panel-heading">Users:</div>
	  <div class="panel-body">
	  
		<table class="table" id="results">
				<thead>

					<tr>
						<th><div style="text-align: center;">User Name</div></th>
						<th><div style="text-align: center;">Login Name</div></th>
						<th><div style="text-align: center;">Role</div></th>
						<th style="width:200px;">Note</th>
						<th><div style="text-align: center;">Created Date</div></th>
						<th><div style="text-align: center;">Edit User</div></th>
						<th><div style="text-align: center;">Delete User</div></th>
					</tr>
				</thead>
				<tbody>
					<%
						String userName = "";

							for (UserAccess userAccess : usersList) {
								if (userAccess.getFirstName() != null) {
									userName = userAccess.getFirstName();
								}
								if (userAccess.getLastName() != null) {
									userName = userName + " " + userAccess.getLastName();
								}
					%>
					<tr>

						<td><div style="text-align: center;"><%=userName%></div></td>
						<td><div style="text-align: center;">
							<%
								if (userAccess.getUserId() != null) {
							%> <%=userAccess.getUserId()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
 						</div>
						</td>
						<td>
						<div style="text-align: center;">
							<%
								if (userAccess.getRoleName() != null) {
							%> <%=userAccess.getRoleName()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
						</div>
						</td>
						<td>
							<%
								if (userAccess.getNote() != null) {
							%> <%=userAccess.getNote()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
 						
						</td>
						<td><div style="text-align: center;">
							<%
								if (userAccess.getCreateDate() != null) {
							%> <%=userAccess.getCreateDate()%> <%
 	} else {
 %> <%="-"%> <%
 	}
 %>
						
						</div>
						</td>

						
						<%if(!userAccess.getRoleName().equalsIgnoreCase("admin")){ %>
						<td><div style="text-align: center;">
							
								<button type="submit" class="btn btn-primary btn-sm"
									onclick="showUpdateUser('edit','<%=userAccess.getUserId()%>')">Edit</button>

							</div>
							
						</td>

						<td>
						<div style="text-align: center;">
							
								<button type="submit" class="btn btn-primary btn-sm"
									onclick="showUpdateUser('delete','<%=userAccess.getUserId()%>')">Delete</button>

							</div>
							
						</td>
						
						<%}else{ %>
						<td>
						<div style="text-align: center;">
						<button type="submit" class="btn btn-primary btn-sm" onclick="showUpdateUser('edit','<%=userAccess.getUserId()%>')" disabled="disabled"">Edit</button>
						</div>
						</td>
						<td>
						<div style="text-align: center;">
						<button type="submit" class="btn btn-primary btn-sm" onclick="showUpdateUser('delete','<%=userAccess.getUserId()%>')" disabled="disabled">Delete</button>
						</div>
						</td>
							
							<%} %>

					</tr>
					<%
						}
					%>
				</tbody>

			</table>
		</div>
	</div>
	<%
		} else {
	%>
	<table id="" name="">
		<thead>
			<tr>
				<td>User Name</td>
				<td>Login Name</td>
				<td>Role</td>
				<td>Note</td>
				<td>Created Date</td>
			</tr>
		</thead>
		<%
			}
		%>
		
		<input type="hidden" name="csrfPreventionSalt"
			value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
			</table>
			
			
	
		</form>
		
		<style type="text/css">
.pg-normal {
    color: #0000FF;
    font-weight: normal;
    text-decoration: none;
    cursor: pointer;
}
 
.pg-selected {
    color: #800080;
    font-weight: bold;
    text-decoration: underline;
    cursor: pointer;
}
</style>
		
		<script type="text/javascript">
        var pager = new Pager1('results', 20); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    </script>
		