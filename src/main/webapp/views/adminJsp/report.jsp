<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>

<meta charset="ISO-8859-1"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>Report</title>
	<link rel="stylesheet" href="resources/css/bootstrap/bootstrap-table.css"/>
	<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet"/>
	<link href="resources/css/bootstrap/bootstrap-select.min.css" rel="stylesheet"/>
	<link href="resources/css/bootstrap/glyphicons.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
	<script type="text/javascript" src="resources/js/admin.js"></script>
	<script src="<c:url value='/resources/lib/jquery-ui.js'/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/core/jquery.1.10.2.min.js" />"></script>
	<script type="text/javascript">
	document.title="Reports";
	$(document).ready(function(){
		$("ul.sb #report").parent().addClass("selected");
		$("#user_wise_report").trigger("click");
	});
	window.history.forward();	
		</script>
<%
String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December" };
String[] monthId = { "01", "02", "03", "04", "05", "06", "07","08", "09", "10", "11", "12" };
Calendar cal = Calendar.getInstance();
String month = monthName[cal.get(Calendar.MONTH)];
	List<Integer>yearList=new ArrayList<Integer>();
	Map<String,Object>dataMap=new HashMap<String,Object>();
	  boolean showFlag=false;
	  boolean existingUserFlag=false;
	  boolean saveFlag=false;
	  int year=2014;
	  String message="";
	  
	  if(request.getAttribute("dataMap")!=null)
	  {
		  dataMap=(Map)request.getAttribute("dataMap");
	  }
	  
	  if(dataMap.get("yearList")!=null)
	  {
		  yearList=(List)dataMap.get("yearList");
		  if(yearList.size()>0)
		  {
			  year=yearList.get(0);
		  }
	  }
%>
<div id="users" class="panel panel-primary">
	  <div class="panel-heading">Reports:</div>
	  <div id="totalReport" class="row" style="text-align: right; background-color: #eee;padding: 5px 10px;" ></div>
	
	  <div>
				<ul class="tab-menu nav-justified"  style="width: 100%" id="napDropDown" >
					<li id="user_wise_report" class="nap-sub-menu-head selected">User Report</li>
					<li id="day_wise_report" class="nap-sub-menu-head">Day Report</li>
					 <li id="month_wise_report" class="nap-sub-menu-head">Monthly Report</li>
					<li id="module_wise_report" class="nap-sub-menu-head">Module Report</li>
					
				</ul>
		</div>
		<div style="display:none;" id="showDate">
		<label class="label2">Date</label><input type="text"
								id="reportDate" class="fieldset">
								</div>
								
		<div style="display:none; margin-left: 50px; height: 130px; padding: 10px;" id="shoowMonth">
		
		<label class="label1">From</label>
		<label class="label1">Year</label>
		<input type="text" name="year" id="year" value="<%=year-1%>" class="fieldset" style="width: 150px;float:left;margin-left:-150px;margin-top:-5px;">
		<%-- <select id="year" class="fieldset" style="width: 150px;">
		<%for(int i=0;i<yearList.size();i++){%>
		<option value=<%=yearList.get(i)%>><%=yearList.get(i)%></option>
		<%} %>
		</select> --%>
		<label class="label1" style="padding-left:50px;margin-top:0px;">Month</label>
		<select id="fromMonth" class="fieldset" style="width: 150px;"> 
		<%for(int j=0;j<monthName.length;j++){
			if(month.equalsIgnoreCase(monthName[j])){
			%>
		<option value="<%=monthId[j]%>" selected="selected"><%=monthName[j]%></option>
		<%}else{ %>
		<option value="<%=monthId[j]%>"><%=monthName[j]%></option>
		<%}} %>
		</select>
		
		<label class="label1">To</label>
		<label class="label1">Year</label>
		<input type="text" name="toyear" id="toyear" value="<%=year%>" class="fieldset" style="width: 150px;float:left;margin-left:-150px;">
		<label class="label1" style="padding-left: -2px;margin-top:5px;margin-left: 50px;">Month</label>
		<select id="toMonth" class="fieldset" style="width: 150px;float:left;margin-left: -50px;">
		<%for(int j=0;j<monthName.length;j++){
			if(month.equalsIgnoreCase(monthName[j])){
			%>
		<option value="<%=monthId[j]%>" selected="selected"><%=monthName[j]%></option>
		<%}else{ %>
		<option value="<%=monthId[j]%>"><%=monthName[j]%></option>
		<%}} %>
		</select>
		
	
		</div>
</div>
 <div class="row" style="height: 25px; background-color: #eee"></div>
 <div id="loading"></div>
<div id="users" class="panel panel-primary">
		<div id="showAllReport"></div>
</div>