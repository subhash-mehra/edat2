<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<meta charset="ISO-8859-1"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>Demo Application</title>
	<link rel="stylesheet" href="resources/css/bootstrap/bootstrap-table.css"/>
	<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet"/>
	<link href="resources/css/bootstrap/bootstrap-select.min.css" rel="stylesheet"/>
	<link href="resources/css/bootstrap/glyphicons.css" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
	<script type="text/javascript" src="resources/js/admin.js"></script>
	<script type="text/javascript">
	window.history.forward();
		</script>
<%
	Map<String,Object>dataMap=new HashMap<String,Object>();
  		
	  boolean showFlag=false;
	  boolean existingUserFlag=false;
	  boolean saveFlag=false;
	  String message="";
	  
	  if(request.getAttribute("dataMap")!=null)
	  {
		  dataMap=(Map)request.getAttribute("dataMap");
	  }
	  
	  
	 
	  
	  if(dataMap.get("message")!=null)
	  {
		  message=(String)dataMap.get("message");
		 
	  }
	  if(session.getAttribute("userAccess")==null){
		  out.print("<h6 style=\"text-align: center; font-size: 20px; float:left;\">Oops!! Session Expired .</h6>");
	  }else{
	  %>
<h3 style="margin: 10px;">Change Password:</h3>		
		<h5><%=message %></h5>		
			<form class="changePassword" role="form" action="changePassword" method="post"
				id="changePasswordRequest" onsubmit="return validatePasswordChange();" onload="setFocus();" >
				<div class="panel panel-primary">
				<div class="panel-heading">Change Password</div>
				<div class="panel-body">
				<div class="form-group">
						<label class="label1">Old Password<span style="color: red;">*</span></label>
					<input type="password"  id="oldPassword" class="passwordFieldset"
						name="oldPassword" placeholder="Enter old password">
				</div>			
				<div class="Clear"></div>	
				<div class="form-group">
						<label class="label1">New Password<span style="color: red;">*</span></label>
				
					<input type="password"  id="newPassword" class="passwordFieldset"
						name="newPassword" placeholder="Enter New password">
			</div>
			<div class="Clear"></div>	
			<div class="form-group">			
						<label class="label1">Confirm New Password<span style="color: red;">*</span></label>					
					<input type="password"  id="newPasswordConfirm" class="passwordFieldset"
						name="confirmNewPassword" placeholder="Enter New Confirm password">
				</div>
				<div style="margin: 5px;">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-primary" onclick="window.close()">Close</button>
				</div>
				<input type="hidden" name="csrfPreventionSalt" value="<c:out value='<%=request.getAttribute("csrfPreventionSalt") %>'/>"/>
			</div>
			</div>
			</form>
			<%}%>