
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tmobile.edat.admin.model.*"%>
<%@page import="com.tmobile.edat.admin.dto.request.UserRole"%>
<script type="text/javascript" src="resources/js/admin.js"></script>
<script type="text/javascript">
document.title="Edit Existing User";
	window.history.forward();
</script>
<%
	Map<String, Object> dataMap = new HashMap<String, Object>();
	List<UserRole> roleList = new ArrayList<UserRole>();
	;
	UserAccess userAccess = null;
	;
	boolean showFlag = false;
	boolean existingUserFlag = false;
	boolean saveFlag = false;
	String message = "";

	if (request.getAttribute("dataMap") != null) {
		dataMap = (Map) request.getAttribute("dataMap");
	}

	if (dataMap.get("roleList") != null) {
		roleList = (List) dataMap.get("roleList");

	}

	if (dataMap.get("userAccess") != null) {
		userAccess = (UserAccess) dataMap.get("userAccess");

	}

	if (dataMap.get("showFlag") != null) {
		showFlag = (Boolean) dataMap.get("showFlag");

	}

	if (dataMap.get("saveFlag") != null) {
		saveFlag = (Boolean) dataMap.get("saveFlag");

	}

	if (!saveFlag) {
		if (dataMap.get("existingUserFlag") != null) {
			existingUserFlag = (Boolean) dataMap
					.get("existingUserFlag");

		}

		if (existingUserFlag) {
			message = "User already exists!!";
		} else {
			message = "Can't create user, Please try again!!";
		}
	} else {
		message = "User created successfully!!";
	}
%>


<%
	if (!showFlag) {
%>
<form name="message">
	<div
		style="color: red; font-weight: bold; margin-left: 100px; font-size: 30px;">
		<h5><%=message%></h5>
	</div>
</form>
<%
	}
%>

<form class="form-inline" role="form" action="updateUser" method="post"
	id="userRequest" name="userRequest"
	onsubmit="return validateUserUpdateForm();">

			<div class="panel panel-primary">
				<div class="panel-heading">Edit User Details:</div>

		<%
			if (userAccess != null) {
		%>
		<div class="panel-body">
		<h5><b><i>Note</i>:Required fields are indicated with the red asterisk(<span style="color: red;">*</span>).</b></h5>
		<div class="form-group">

			<label class="label1">First Name<span style="color: red;">*</span></label> <input type="text"
				maxlength="45" class="fieldset" id="fname" name="fname"
				value="<%=userAccess.getFirstName()%>">
		</div>
		<div class="Clear"></div>
		<div class="form-group">

			<label class="label1">Last Name<span style="color: red;">*</span></label> <input type="text"
				maxlength="45" class="fieldset" id="lname" name="lname"
				value="<%=userAccess.getLastName()%>">
		</div>
		<div class="Clear"></div>
		<div class="form-group">

			<label class="label1">Login Name<span style="color: red;">*</span></label> <input type="text"
				maxlength="30" class="fieldset" id="loginname" name="loginname"
				readonly="readonly" value="<%=userAccess.getUserId()%>"> <input
				type="hidden" maxlength="30" class="fieldset" id="userid"
				name="userid" value="<%=userAccess.getUserId()%>">
		</div>
		<div class="Clear"></div>
		<div class="form-group">

			<label class="label1">Password<span style="color: red;">*</span></label> <input type="password"
				maxlength="30" class="fieldset" id="pwd" name="pwd"
				value="<%=userAccess.getPassword()%>">
		</div>
		<div class="Clear"></div>
		<div class="form-group">
	
			<label class="label1">Confirm Password<span style="color: red;">*</span></label> <input type="password" maxlength="30"
				class="fieldset" id="c_pwd" name="c_pwd" value="<%=userAccess.getPassword()%>"
				placeholder="Enter confirm password">
		</div>
		<div class="Clear"></div>
		<div class="form-group">

			<label class="label1">Role<span style="color: red;">*</span></label> <select class="fieldset"
				id="addUserRoleSelector" name="addUserRoleSelector" multiple="multiple">
				<option value="0" >-Select role-</option>

				<%
					for (UserRole userRole : roleList) {
							if (userAccess.getRoleName() != null) {
								String [] userRoleArr = null;
							List<String> userRole1  = new ArrayList<String>();
							userRoleArr = userAccess.getRoleName().split(",");
							for(String role : userRoleArr){
								userRole1.add(role.toUpperCase());
							}
						
						if (userRole1!=null && userRole1.contains(userRole.getRoleName())) {
				%>
				<option value="<%=userRole.getRoleName()%>" selected="selected"><%=userRole.getRoleName()%></option>
				<%
					} else {
				%>
				<option value="<%=userRole.getRoleName()%>"><%=userRole.getRoleName()%></option>

				<%
					}
							} else {
				%>

				<option value="<%=userRole.getRoleName()%>"><%=userRole.getRoleName()%></option>
				<%
					}
						}
				%>
			</select>


		</div>
		<div id='CharCountLabel1'></div>
		<div class="Clear"></div>
		<div class="form-group">
			<label class="label1">Note</label>
			<textarea rows="10" cols="50" id="note" name="note"
				class="bigTextArea" maxlength="60" onkeyup="CharacterCount("
				note", "CharCountLabel1");" onkeydown="CharacterCount("note", "CharCountLabel1");"><%=userAccess.getNote()%></textarea>

		</div>


		<div style="margin-left: 300px;">
			<button type="submit" class="btn btn-primary">Update</button>

		</div>
		</div>
		</div>

		<%
			}
		%>

		<input type="hidden" name="csrfPreventionSalt"
			value="<c:out value='<%=request.getAttribute("csrfPreventionSalt")%>'/>" />
</form>

