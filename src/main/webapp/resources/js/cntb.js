var uid;
var jsonDusRes;

$(document).ready(function() {
	
	$("#msisdn").keypress(function(event) {
		if (event.keyCode == 13) {
			$("#search").click();
		}
	});
	
	$("#imsi").keypress(function(event) {
		if (event.keyCode == 13) {
			$("#search").click();
		}
	});
	 $("#cntb").parent().addClass("active");
    var file;
    $('#batchDelete').click(function() {
        window.open("showBatchDelete", "formresult", "toolbar=no, scrollbars=yes, resizable=no, top=100, left=700, width=400, height=250");

    });

    $('#batchAdd').click(function() {
        window.open("showBatchAdd", "formresult", "toolbar=no, scrollbars=yes, resizable=no, top=100, left=700, width=400, height=250");

    });

    $('#imsi').blur(function(event) {
        var searchVal = document.getElementById("searchTextVal").value;
        if (validateInput()) {
            var data = {
                "imsi": document.getElementById("imsi").value,
            };
            if (searchVal == 'add') {
                urlAction = 'existAdd';
                sendDataRequestForCheckExistRecord(urlAction, data);
            }
            if (searchVal == 'delete') {
                urlAction = 'existDelete';
                if ($.trim($("#msisdn").val()) != '') {
                    sendDataRequestForCheckExistRecordForDelete(urlAction, data);
                }
            }
        }
    });
    $('#msisdn').blur(function(event) {
        var searchVal = document.getElementById("searchTextVal").value;
        if (validateInput()) {
            var data = {
                "msisdn": '1' + document.getElementById("msisdn").value,
            };
            if (searchVal == 'add') {
                urlAction = 'existAdd';
                sendDataRequestForCheckExistRecord(urlAction, data);
            }
            if (searchVal == 'delete') {
                urlAction = 'existDelete';
                if ($.trim($("#msisdn").val()) != '') {
                    sendDataRequestForCheckExistRecordForDelete(urlAction, data);
                }
            }
        }
    });
    $('#search').click(function(event) {
        if (!empltyCheck()) {
            return;
        }
        if (validateInput()) {
            $("ul.sb #cntbSearch").parent().addClass("selected");
            var urlAction;

            urlAction = 'msisdn';
            dataProcessStart();
            var data = {
                "msisdn": '1' + document.getElementById("msisdn").value,
                "imsi": document.getElementById("imsi").value,
            };
            sendDataRequest(urlAction, data);
        }
    });
    $('#add').click(function(event) {
        if (!empltyCheckForAdd()) {
            return;
        }
        urlAction = 'save';
        dataProcessStart();
        if (validateInput()) {
            var data = {
                "msisdn": '1' + document.getElementById("msisdn").value,
                "imsi": document.getElementById("imsi").value,
                "encKey": document.getElementById("encKey").value,
                "accId": document.getElementById("accId").value,
                "alogId": document.getElementById("alogId").value,
                "kdbId": document.getElementById("kdbId").value,
                "custId": document.getElementById("custId").value,
                "acsub": document.getElementById("acsub").value
            };
            sendDataRequestForAdd(urlAction, data);
        }
    });
    $('#delete').click(function(event) {
        if (!empltyCheck()) {
            return;
        }
        if (validateInput()) {
            urlAction = 'delete';
            dataProcessStart();
            var data = {
                "msisdn": '1' + document.getElementById("msisdn").value,
                "imsi": document.getElementById("imsi").value,
            };
            sendDataRequestForDelete(urlAction, data);
        }
    });
});

function sendDataRequestForCheckExistRecord(urlAction, data) {

    $.ajax({
        url: '/edat/' + urlAction,
        data: JSON.stringify(data),
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(result) {
            jsonDusRes = result;
            putMessageForExistRecord(result);

            uid = jsonDusRes.uid;
        }
    });
}

function sendDataRequestForCheckExistRecordForDelete(urlAction, data) {

    $.ajax({
        url: '/edat/' + urlAction,
        data: JSON.stringify(data),
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(result) {
            jsonDusRes = result;
            putMessageForExistRecordForDelete(result);

            uid = jsonDusRes.uid;
        }
    });
}

function sendDataRequestForAdd(urlAction, data) {

    $.ajax({
        url: '/edat/' + urlAction,
        data: JSON.stringify(data),
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(result) {
            jsonDusRes = result;
            putMessage(result);


            uid = jsonDusRes.uid;
            clearTextForAdd();
        }
    });
}

function sendDataRequestForDelete(urlAction, data) {

    $.ajax({
        url: '/edat/' + urlAction,
        data: JSON.stringify(data),
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(result) {
            jsonDusRes = result;
            putMessage(result);
            uid = jsonDusRes.uid;
            clearTextForDelete();
        }
    });
}




function sendDataRequest(urlAction, data) {
    $.ajax({
        url: '/edat/' + urlAction,
        data: JSON.stringify(data),
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(result) {
            jsonDusRes = result;
            var missdniList = result.missdniList;
            var imsiiList = result.imsiiList;

            	genrateUI(result);
                genrateUIForImsi(result);
                
            

            uid = jsonDusRes.uid;
            if (urlAction == "msisdn") {
                $('#cnbtContentHeading').show();
                $('#cnbtContentBody').show();
                $('#cnbtIMSIContentHeading').show();
                $('#cnbtIMSIContentBody').show();
                $('#panelDiv').show();
                $('#panelDivForBatchSearch').hide();
                $('#cnbtContentHeadingForBatchSearch').hide();
                $('#cnbtContentBodyMSISDN').hide();
                $('#panelDivForIMSIBatchSearch').hide();
                $('#cnbtContentHeadingForIMSIBatchSearch').hide();
                $('#cnbtContentBodyForIMSI').hide();
                clearText();
            }
        }
    });
}

function clearText() {
    document.getElementById('msisdn').value = '';
    document.getElementById('imsi').value = '';
}

function clearTextForAdd() {
    document.getElementById('msisdn').value = '';
    document.getElementById('imsi').value = '';
    document.getElementById("custId").value = '';
    /*document.getElementById("encKey").value = '';
    document.getElementById("accId").value = '';
    document.getElementById("alogId").value = '';
    document.getElementById("kdbId").value = '';
    */
}

function clearTextForDelete() {
    document.getElementById('msisdn').value = '';
    document.getElementById('imsi').value = '';
}

function empltyCheck() {
    var msisdn = document.getElementById('msisdn').value;
    var imsi = document.getElementById('imsi').value;
    if (msisdn.trim().length == 0 && imsi.trim().length == 0) {
        alert('Please enter either MSISDN or IMSI or both');
        return false;
    } else {
        return true;
    }
}

function empltyCheckForAdd() {
    var msisdn = $.trim($('#msisdn').val());
    var imsi = $.trim($('#imsi').val());
    if (msisdn != '') {
        if (imsi === '') {
            alert('IMSI is empty.');
            return false;
        } else {
            return true;
        }
    } else if (imsi === '') {
        alert('MSISDN and IMSI is empty.');
        return false;
    } else {
        alert('MSISDN is empty.');
        return false;
    }
    return true;
}

function dataProcessStart() {
    $('#loading').html('<img src="/edat/resources/images/process.gif" >');
    $('#extraInfo, #extrainfo2').hide();
}

function dataProcessEnd() {
    $('#loading').empty();
}

function putMessage(result) {
    $('#messageHeading').text(result.message);
    dataProcessEnd();
    /*
     * for ( var key in result) { if(key=='message') {
     * $('#messageHeading').text(result[key].message); } }
     */
}

function putMessageForExistRecord(result) {
    if (result.message == 'exist') {
        $('#messageHeading').text("Record already exist.");
        if (result.msisdn != '') {
            document.getElementById('msisdn').value = '';
        }
        if (result.msisdn != '') {
            document.getElementById('imsi').value = '';
        }
        return false;
    } else {
        $('#messageHeading').text("");
        return true;
    }
    return true;
}

function putMessageForExistRecordForDelete(result) {
    if (result.message == 'exist') {
        $('#messageHeading').text("");
        return true;
    } else {
        $('#messageHeading').text("Record does not exist.");
        if (result.msisdn != '') {
            document.getElementById('msisdn').value = '';
        }
        if (result.msisdn != '') {
            document.getElementById('imsi').value = '';
        }
        return false;
    }
    return true;
}

function genrateUI(result) {
    console.log(result);
	if (result.applicationError){
		displayInternalErroGridForMsisdn(result.applicationError.errorCode);
		$("#imsiResultDiv").css('visibility','hidden');
		$("#msisdnResultDiv").css('visibility','hidden');
		return ;
	}else
		{
		displayInternalErroGridForMsisdn(null);
		}
	if(result.missdniList.length!=0)
	{
	var myTable = null;
		myTable = "<table data-toggle='table' class= 'display'  id='insdpTable'><thead><tr><th>Attribute</th>";

	myTable += "<th> Value </th>";
	myTable += "</tr> </thead>  <tbody>";

	for (var key in result) {
        if (key == 'missdniList' && result.hasOwnProperty('missdniList')) {
            var str_array = result.missdniList;
            myTable += "<tr class='success'><td>" + "UID" + "</td>";
            if (str_array.length > 2) {
            	myTable += "<td>" + (str_array[0]) + "</td>";
            } else {
            	myTable += "<td>" + '-' + "</td>";
            }
            myTable += "<tr class='success'><td>" + "MSISDN" + "</td>";
            if (str_array.length > 0) {
            	myTable += "<td>" + (str_array[2]) + "</td>";
            } else {
            	myTable += "<td>" + '-' + "</td>";
            }
            myTable += "<tr class='success'><td>" + "IMSI" + "</td>";
            if (str_array.length > 1) {
            	myTable += "<td>" + (str_array[1]) + "</td>";
            } else {
            	myTable += "<td>" + '-' + "</td>";
            }
        }
    }
	myTable += "</tbody></table>";
		$('#msisdnResultDivContentHeading').text("Contents for CNTB MSISDN Search");
		document.getElementById('msisdnResultDivContentBody').innerHTML = myTable;

		$('#insdpTable').dataTable({
			"scrollY" : "300px",
			"toolbar" : true,
			"scrollCollapse" : true,
			"paging" : false,

			"columns" : [ null, null

			],
			"jQueryUI" : true,
		});
		
		$("#msisdnResultDiv").css('visibility','visible');
		$("#msisdnBatchSearch").hide();
		$("#imsiBatchSearch").hide();
		$("imsiDiv").hide();
		$('#messageHeading').hide();
	}
	if(result.imsiiList.length!=0)
	{
		$('#messageHeading').hide();
	}
		dataProcessEnd();
		
  
}

function genrateUIForImsi(result) {
    console.log(result);
	if (result.applicationError){
		displayInternalErroGridForIMSI(result.applicationError.errorCode);
		$("#imsiResultDiv").css('visibility','hidden');
		$("#msisdnResultDiv").css('visibility','hidden');
		 
		return ;
	}else
		{
		displayInternalErroGridForIMSI(null);
		}
	if(result.imsiiList.length!=0)
	{
	var myTable = null;
		myTable = "<table data-toggle='table' class= 'display' data-height='180' id='imsiTable'><thead><tr><th>Attribute</th>";

	myTable += "<th> Value </th>";
	myTable += "</tr> </thead>  <tbody>";

	 for (var key in result) {
	        if (key == 'imsiiList' && result.hasOwnProperty('imsiiList')) {
	            var str_array = result.imsiiList;
	            myTable += "<tr><td>" + "UID" + "</td>";
	            if (str_array.length > 2) {
	            	myTable += "<td>" + (str_array[0]) + "</td>";
	            } else {
	            	myTable += "<td>" + '-' + "</td>";
	            }
	            myTable += "<tr><td>" + "MSISDN" + "</td>";
	            if (str_array.length > 0) {
	            	myTable += "<td>" + (str_array[2]) + "</td>";
	            } else {
	            	myTable += "<td>" + '-' + "</td>";
	            }
	            myTable += "<tr><td>" + "IMSI" + "</td>";
	            if (str_array.length > 1) {
	            	myTable += "<td>" + (str_array[1]) + "</td>";
	            } else {
	            	myTable += "<td>" + '-' + "</td>";
	            }
	        }
	    }
	myTable += "</tbody></table>";
	$('#imsiResultDivContentHeading').text("Contents for CNTB IMSI Search");
	document.getElementById('imsiResultDivContentBody').innerHTML = myTable;
		$('#panelHeading').text("IMSI Search Result");

		$('#imsiTable').dataTable({
			"scrollY" : "300px",
			"toolbar" : true,
			"scrollCollapse" : true,
			"paging" : false,

			"columns" : [ null, null

			],
			"jQueryUI" : true,
		});
		$("#imsiResultDiv").css('visibility','visible');
		$('#messageHeading').hide();
	}
	
	if(result.missdniList.length!=0)
	{
		$('#messageHeading').hide();
	}
	
		dataProcessEnd();
		
		
		
}

function cntbShow(val) {
    if (val == 'search') {
        /*
         * $('#panelHeading').text("CNTB DB Search:");
         *
         * $("ul.sb #cntbSearch").parent().addClass("selected"); $("ul.sb
         * #cntbAdd").parent().removeClass("selected"); $("ul.sb
         * #cntbDelete").parent().removeClass("selected");
         */
        document.buttonForm.method = "get";
        document.buttonForm.action = "/edat/cntbDBSearch?csrfPreventionSalt=" + csrf;
        document.forms["buttonForm"].submit();
    }
    if (val == 'add') {
        /*
         * $('#panelHeading').text("CNTB DB Add:"); $("ul.sb
         * #cntbAdd").parent().addClass("selected"); $("ul.sb
         * #cntbSearch").parent().removeClass("selected"); $("ul.sb
         * #cntbDelete").parent().removeClass("selected");
         */
        document.buttonForm.method = "get";
        document.buttonForm.action = "/edat/cntbDBAdd?csrfPreventionSalt=" + csrf;
        document.forms["buttonForm"].submit();
    }
    if (val == 'delete') {
        var csrf = null;
        document.buttonForm.method = "get";
        document.buttonForm.action = "/edat/cntbDBDelete?csrfPreventionSalt=" + csrf;
        document.forms["buttonForm"].submit();
    }
}

function validateInput() {
    if ($("#msisdn").val().length != 10 && $("#msisdn").val().length != 14 && $("#msisdn").val().length != 15 && $("#msisdn").val().length != 0) {
        alert('Invalid MSISDN');
        $("#msisdn").focus();
        return false;
    } else if ($("#imsi").val().length != 15 && $("#imsi").val().length) {
        alert('Invalid IMSI');
        $("#imsi").focus();
        return false;
    }
    return true;
}

function Checkfiles() {
    var fup = document.getElementById('fileselect');
    var fileName = fup.value;
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    if (ext == "csv") {
        dataProcessStart();
        return true;
    } else {
        alert("Upload CSV file only");
        fup.focus();
        return false;
    }
}



function displayInternalErroGridForMsisdn(error) {
	$('#messageHeading').show();
	if(error!=null)
    {
    	$('#messageHeading').text("Customer not found !");	
    }else
    	{
    	$('#messageHeading').text("Node Not Reachable !");
    	}
	 
   dataProcessEnd();
}

function displayInternalErroGridForIMSI(error) {
    // $("#nap-tab").show();
    //$('#napDataTable').hide();
    $('#messageHeading').show();
    if(error!=null)
    {
    	$('#messageHeading').text("Customer not found !");	
    }else
    	{
    	$('#messageHeading').text("Node Not Reachable !");
    	}

    dataProcessEnd();
}



function geberateTableForMSISDNBatch(msisdnAttributeList)
{
	var counter=0;
	var nextLineCounterForMSISDN=0;
	
	myTable = "<table data-toggle='table' class= 'display' data-height='180' id='msisdnBatchTable'><thead><tr><th>Attribute</th>";

	myTable += "<th> Value </th>";
	myTable += "</tr> </thead>  <tbody>";


	var str_array = msisdnAttributeList;
	var indexFirst=str_array.indexOf("[");
	var indexLast=str_array.indexOf("]");
	var subString=str_array.substring(indexFirst+1,indexLast);
	var res = subString.split(",");
		var j=0;
		var sepratorCounter=1;
		for(var i=0;i<res.length;i++)
		{
		nextLineCounterForMSISDN=0;
		if(counter>2){counter=0;}
		switch(j)
		{
		case 0:myTable += "<tr class='success'><td>" + "UID" + "</td>";
        				myTable += "<td>" + (res[i]) + "</td>";
        				j=j+1;
        				sepratorCounter=sepratorCounter+1;
        				break;
		case 1: myTable += "<tr class='success'><td>" + "IMSI" + "</td>";
	       myTable += "<td>" + (res[i]) + "</td>";
	       j=j+1;
	       sepratorCounter=sepratorCounter+1;
	       break;
	 	  
		case 2: myTable += "<tr class='success'><td>" + "MSISDN" + "</td>";
	       myTable += "<td>" + (res[i]) + "</td>";
	       sepratorCounter=sepratorCounter+1;
	       j=0;
	       
	   	myTable += "<tr class='success'><td>" +"<hr>" + "</td>";
	       myTable += "<td>" + "<hr>"+ "</td>";
	       break;
	       
		
		}
      
		}
	myTable += "</tbody></table>";
	$('#msisdnResultDivContentHeading').text("Contents for CNTB MSISDN Search");
	document.getElementById('msisdnResultDivContentBody').innerHTML = myTable;


	$('#msisdnBatchTable').dataTable({
		"scrollY" : "300px",
		
		"toolbar" : true,
		"scrollCollapse" : true,
		"paging" : false,
		 "bSort": false,

		"aoColumns": [{ "bSortable": false },{ "bSortable": true }],
		//"Sortable": false,
		"jQueryUI" : true,
	});
	

	$("#msisdnResultDiv").css('visibility','visible');
	dataProcessEnd();
}

function geberateTableForIMSIBatch(msisdnAttributeList)
{
	var counter=0;
	var nextLineCounterForMSISDN=0;
	
	myTable = "<table data-toggle='table' class= 'display' data-height='180' id='imsiBatchTable'><thead><tr><th>Attribute</th>";

	myTable += "<th> Value </th>";
	myTable += "</tr> </thead>  <tbody>";


	var str_array = msisdnAttributeList;
	var indexFirst=str_array.indexOf("[");
	var indexLast=str_array.indexOf("]");
	var subString=str_array.substring(indexFirst+1,indexLast);
	var res = subString.split(",");
		var j=0;
		var sepratorCounter=1;
		for(var i=0;i<res.length;i++)
		{
		nextLineCounterForMSISDN=0;
		if(counter>2){counter=0;}
		switch(j)
		{
		case 0:myTable += "<tr class='success'><td>" + "UID" + "</td>";
        				myTable += "<td>" + (res[i]) + "</td>";
        				j=j+1;
        				sepratorCounter=sepratorCounter+1;
        				break;
		case 1: myTable += "<tr class='success'><td>" + "IMSI" + "</td>";
	       myTable += "<td>" + (res[i]) + "</td>";
	       j=j+1;
	       sepratorCounter=sepratorCounter+1;
	       break;
	 	  
		case 2: myTable += "<tr class='success'><td>" + "MSISDN" + "</td>";
	       myTable += "<td>" + (res[i]) + "</td>";
	       sepratorCounter=sepratorCounter+1;
	       j=0;
	       
	   	myTable += "<tr class='success'><td>" +"<hr>" + "</td>";
	       myTable += "<td>" + "<hr>"+ "</td>";
	       break;
	       
		
		}
      
		}
	myTable += "</tbody></table>";
	$('#imsiResultDivContentHeading').text("Contents for CNTB IMSI Search");
	document.getElementById('imsiResultDivContentBody').innerHTML = myTable;

	$('#imsiBatchTable').dataTable({
		"scrollY" : "300px",
		
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		 "bSort": false,

		"aoColumns": [{ "bSortable": false },{ "bSortable": true }],
		//"Sortable": false,
		"jQueryUI" : true,
	});
	$("#imsiResultDiv").css('visibility','visible');
	dataProcessEnd();
}


