$(document).ready(function(){
    var activeTabIndex = -1;
    var tabNames = ["nap","dus","pcrf","uma","hss","hlr","eir"];
    var messagingTabNames = ["SMCS","MMSC","IAM"];
    var selectValues = { "1": "test 1", "2": "test 2" };
    
    var dropDownOptions = [[{label:"Customer Id", value:"customerId"},{label:"MSISDN(1)", value:"msisdn"},{label:"IMSI", value:"imsi"}],
                           [{label:"Customer Id", value:"customerId"},{label:"MSISDN(1)", value:"msisdn"},{label:"IMSI", value:"imsi"}],
                           [{label:"MSISDN(1)", value:"msisdn"},{label:"IMSI", value:"imsi"},{label:"User Id", value:"userId"},{label:"Email", value:"email"}],
                           [{label:"MSISDN(1)", value:"msisdn"},{label:"BAN", value:"ban"}],
                           [{label:"Customer Id", value:"customerId"},{label:"MSISDN(1)", value:"msisdn"},{label:"IMSI", value:"imsi"}]];
    
    function populateDropDownForTab(index){
    	$("#searchSelector option").remove();
    	var dd = $("#searchSelector");
    	var options = dropDownOptions[index];
    	for(var i = 0; i < options.length; i++){
    		var opt = $("<option value='"+ options[i].value +"'>"+ options[i].label +"</option>");
    		dd.append(opt);
    	}
    }
    
    $(".tab-menu > li").click(function(e){
    	if(e.target.id == "nap"){
    		$('.tab-dropdown').toggleClass("dsply");

    		$('#usdContainer ul li').each(function(){
    			$(this).removeClass('activeUSD');
    		});
    		$("#nap").addClass("activeUSD");
    	}else{
    		for(var i=0;i<tabNames.length;i++) {
    			if(e.target.id == tabNames[i]) {
    				activeTabIndex = i;
    			} else {
    				$("#"+tabNames[i]).removeClass("activeUSD");
    				$("#"+tabNames[i]+"-tab").removeClass("activeUSD");
    			}
    		}
    		$("#"+tabNames[activeTabIndex]+"-tab").addClass("activeUSD");
    		$("#"+tabNames[activeTabIndex]).addClass("activeUSD");
    	}
    	return false;
    });

    $('#nap').hover(
    		function(){
    			if($( this ).hasClass('activeUSD')){
    				$('.tab-dropdown').addClass("dsply");
    			}
    		},
    		function(){
    			if($( this ).hasClass('activeUSD')){
    				$('.tab-dropdown').removeClass("dsply");
    			}
    		});

    $('.tab-dropdown').hover(function(){
    	$(this).toggleClass("dsply");
    });
    $( '.tab-dropdown > div').click(function(e) {
    	if(e.target.id==""){
    		return false;
    	}
    	removeActiveTab();
    	$('#nap-tab p').text("Content for NAP | "+e.target.id );
    	return false;
    });

    function removeActiveTab(){
    	if(!$("#nap-tab").hasClass("active")){
    		$('#usdContainer div.tab-content').each(function(){
    			$(this).removeClass('active');
    		});
    		$("#nap-tab").addClass("active");
    	}
    }
    
    /*$("#mainNav li").click(function(){
    	var tabIndex = $(this).index();
    	populateDropDownForTab(tabIndex);
    	$("#tabContainer > div.active").removeClass("active");
    	$("#tabContainer > div:nth-child("+ (tabIndex + 1) +")").addClass("active");
    	
    	$("#asd option").each(function() {
    	    $(this).remove();
    	});
    	
    	if($(this).has("a#messagingData").length > 0){
    		$("#messagingRadioGroup").show();
    	}else{
    		$("#messagingRadioGroup").hide();
    	}
    });*/
    
    $('#admin').click(function(){
    	
    	$('.top-container').each(function(){
    		$(this).removeClass('active');
    	});
    	$('.relative-tab-container').each(function(){
    		$(this).removeClass('active');
    	});
    	$('#adminContainer').addClass('active');
    });


    $('#messagingListGroup > a').click(function(e){
    	$('#messagingListGroup > a').each(function(){
    		$(this).removeClass('active');
    	});
    	$(this).addClass('active');

    	for(var i=0;i<messagingTabNames.length;i++) {
    		if(e.target.id == messagingTabNames[i]){
    			$('#messagingTopContainer > div > div.table-header').text("Search "+messagingTabNames[i]);
    			$('#messagingContainer ul li').text('Search Result '+messagingTabNames[i]); 
    		}
    	}
    });
    
    $('input[name="addUserOptCheck"]').change(function(){
    	  if (this.checked) {
    	      if(this.id == "checkDUS"){
    	    	  $('#radioDUS').addClass('active');
    	      }
    	      if(this.id == "checkUSD"){
    	    	  $('#radioUSD').addClass('active');
    	      }
    	      if(this.id == "checkCNDT"){
    	    	  $('#radioCNDT').addClass('active');
    	      }
    	  }else{
    		  if(this.id == "checkDUS"){
    	    	  $('#radioDUS').removeClass('active');
    	      }
    	      if(this.id == "checkUSD"){
    	    	  $('#radioUSD').removeClass('active');
    	      }
    	      if(this.id == "checkCNDT"){
    	    	  $('#radioCNDT').removeClass('active');
    	      }
    	  }
    });
    
    $(document).on('change', '#addUserRoleSelector', function(e) {
    	  var roleSelected = $('#addUserRoleSelector').val();
    	  if(roleSelected == "normalUser"){
    		  $('#addUsePermission').addClass('active');
    	  }else{
    		  $('#addUsePermission').removeClass('active');
    	  }
    });
    
    $('.form-control').change(function(){
    	if(this.value != ""){
    		$(this).css('background','rgb(250,255,189)');
    	}else{
    		$(this).css('background','none');
    	}
    });
    
    /*$('#mainNav a').click(function (e) {
    	  e.preventDefault()
    	  $(this).tab('show')
    });*/
});

