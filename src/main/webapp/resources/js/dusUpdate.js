function clearText(){
	document.getElementById('searchText').value='';
	 if($("#searchType").val() == "MSISDN"){
		 $("#banResult").hide();
		 $("#result").show();
		 $("#msg").hide();
	 }else{
		 $("#result").hide();
		 $("#banResult").show();
		 $("#msg").hide();
	 }
	
	
}

function empltyCheck()
{
	var searchBox=document.getElementById('searchText').value;
	if(searchBox.trim().length == 0){
		alert('Please enter the value');
		return false;
	}else{
		return true;
	}			
}
	
var uid;
var jsonDusRes;

$(document).ready(function() {
	
	
	$("#dusUpdate").parent().addClass("active");
	
	$("#searchText").keypress(function(event) {
		if (event.keyCode == 13) {
			$("#search").click();
		}
	});
	 $('#search').click(function(event) {
		
		 if ($.trim($("#searchText").val()) == '') {
				alert("Please specify search cretria...");
				dataProcessEnd()
				return;
		} else if($("#searchText").val().length>10){
	    	alert('Invalid MSISDN');
	    	dataProcessEnd()
	    	return false;
		}else{
			 var searchText = $("#searchText").val();
		     var urlAction;
		     var searchJson = {};
		     dataProcessStart();
			 if($("#searchType").val() == "MSISDN"){
				 urlAction='msisdn';
				 searchJson.msisdn = '1'+searchText;
				 searchJson.ban = "";
		     }else{
		    	 urlAction='ban';
				 searchJson.ban = searchText;
				 searchJson.msisdn = "";
		     }			      
		      
		      $.ajax({
		    	url: '/edat/dusupdate/'+urlAction,
		    	data: JSON.stringify(searchJson),
		    	type: "POST",
	
		    	beforeSend: function(xhr) {				    		
		    		xhr.setRequestHeader("Accept", "application/json");
		    		xhr.setRequestHeader("Content-Type", "application/json");
		    	},
		    	success: function(result) {			    	    
		    		jsonDusRes = result;
		    		var counters = jsonDusRes.counters;
		    		uid = jsonDusRes.uid;
		    		if(urlAction == "msisdn"){
						$('#dusMsisdnResult tbody tr').remove();
			    		$("#banResult").hide();
			    		$("#msg").hide();
			    		//alert(counters.length);
			    		if(counters.length > 0){
			    			$("#dusMsisdnResult").show();
				    		for(var i=0;i<counters.length;i++){
				    			DusCountersResponse(counters[i],i);
				    			//document.getElementById("save"+i).style.visibility = 'hidden';
				    			//document.getElementById("cancel"+i).style.visibility = 'hidden';
			    			}
				    		generateDUSUI(result);
				    		dataProcessEnd();
						}else{
							$('#dusMsisdnResult').hide();
							$('#extraInfo').hide();
							
							$("#banResult").hide();
							$("#msg").show();
						}
			    		dataProcessEnd();
					}else{
						if(jsonDusRes.length > 0){
						$("#dusMsisdnResult").hide();
						$("#extraInfo").hide();
						$("#banResult").show();
			    		$("#result").show();
			    		$("#msg").hide();
			    		$("#banResult ul").empty();
						$("#counters tr:gt(0)").remove();
						$("#offers tr:gt(0)").remove();
						for(var i=0;i<jsonDusRes.length;i++){
		    				DusBanCountersResponse(jsonDusRes[i],i);			    				
	    				};
	    				$("#banResult ul li:first-child a").click();
	    				dataProcessEnd();
	    		 	 }else{
		    		 		$("#banResult ul").empty();
							$("#counters tr:gt(0)").remove();
							$("#offers tr:gt(0)").remove();
	    		 		 	$('#dusMsisdnResult').hide();
	    		 		 	$('#extraInfo').hide();
							$("#banResult").hide();
							$("#msg").show();
							dataProcessEnd();
						}
				  }	
		    	}
		    });
		 }
  });
	 
	 function generateDUSUI(result) {
			if (result.applicationError) {
				displayInternalErroGrid(result.applicationError.errorCode);
				return;
			}

			var subProfileHtml = "<table id ='dusData' data-toggle='table' class= 'display' data-height='180'><thead><tr><th>Attribute</th>";
			subProfileHtml += "<th> Value </th>";
			subProfileHtml += "</tr>  </thead>  <tbody>";
			var counterHtml = "<table data-toggle='table' class= 'display' data-height='180'  id ='counterHTable'><thead><tr><th>amfCounter</th>";
			var itration = -1;
			for ( var key in result) {
				if (key == 'uid' && result.hasOwnProperty('uid')) {
					subProfileHtml += "<tr><td>" + key + "</td>";
					subProfileHtml += "<td>"
							+ (result.uid ? result.uid : " ") + "</td>";
				} else if (key == 'attributes' && result.hasOwnProperty('attributes')) {
					for ( var attributes in result.attributes) {
						if (attributes == "objectClass")
							continue;
						subProfileHtml += "<tr><td>" + attributes + "</td>";
						subProfileHtml += "<td>"
								+ result.attributes[attributes] + "</td>";
					}
				} else if (key == 'counters' && result.hasOwnProperty('counters')) {
					for ( var counters in result.counters) {
						var counterObj = result.counters[counters];
						for ( var prop in counterObj) {
							if (prop == 'counterId'
									&& counterObj.hasOwnProperty('counterId')) {
								counterHtml += "<th > id = " + counterObj.counterId
										+ "</th>";
								itration++;
							}
						}
					}
					counterHtml += "</tr> </thead> <tbody>";
				}
			}
			for ( var set in result.counterSuperset) {
				if (set == "objectClass")
					continue;
				counterHtml += "<tr><td>" + set + "</td>";
				for ( var counter in result.counters) {
					if (result.counters[counter].attributes[set]) {
						counterHtml += "<td>"
								+ result.counters[counter].attributes[set] + "</td>";
					} else {
						if (set == "amfCounterValue") {
							counterHtml += "<td>"
									+ result.counters[counter].counterValue + "</td>";
						} else {
							counterHtml += "<td>-</td>";
						}
					}
				}
				counterHtml += "</tr>";
			}
			subProfileHtml += " </tbody></table>";
			counterHtml += "</tr> </tbody></table>";
			
			$('#extraInfoBody').empty();
			$('#extraInfoHeading').text("DUS : amfCounters");
			$('#extraInfoBody').html(counterHtml);
			$('#extraInfo').show();

			$('#counterHTable').dataTable({
				"scrollY" : "300px",
				"toolbar" : false,
				"scrollCollapse" : true,
				"paging" : false,
				"bAutoWidth": false, 
				//"columns" : [ null, null, null],
				"jQueryUI" : true,
			});

			dataProcessEnd();

		}

function dataProcessStart()
{
$('#loading').html('<img src="/edat/resources/images/process.gif"> Loading...');
			
}
function dataProcessEnd() {
	$('#loading').empty();
}
function DusCountersResponse(dusCountersData, i){
	var row = $("<tr/>");
    $("#dusMsisdnResult table").append(row); 
    //row.attr("id", ""+dusCountersData.counterId+"_"+ i);
    row.append($("<td><span>"+ dusCountersData.counterId +"</span></td>"));
    row.append($("<td><span>" + dusCountersData.counterValue + "</span></td>"));
    row.append($("<td><button class='btn btn-primary btn-xs' onClick='editDusCounter(this);'>Edit</button><button class='btn btn-primary btn-xs'  onClick='saveDusCounter(this);'>Save</button><button class='btn btn-primary btn-xs' onClick='cancelDusCounter(this);'>Cancel</button></td>"));
}

function DusBanCountersResponse(dusCountersData,i){
	var ul = $("#banResult ul");
	var a = $("<a href='#'>" + dusCountersData.msisdn + " ></a>");
	$("<li></li>").append(a).appendTo(ul);
	
	a.click(function(){
		$(this).parent().addClass("selected").siblings().removeClass("selected");
		
		var details = jsonDusRes[$(this).parent().index()];
		var counters = details["counters"];
		var offers = details["activeOffers"];
		$("#counters tr:gt(0)").remove();
		$("#offers tr:gt(0)").remove();
		for(var i=0;i<counters.length;i++)
		{
			var counter = counters[i];
			var row = $("<tr/>");
			$("#counters").append(row);
			 row.append($("<td>"+counter.counterId+"</td>"));
		     row.append($("<td>"+counter.counterValue+"</td>"));
		}
		
		for(var i=0;i<offers.length;i++)
		{
			var offer = offers[i];
			var row = $("<tr/>");
			$("#offers").append(row);
			 row.append($("<td>"+offer+"</td>"));
		}
		
	});
}
	 				
});

var cIdDDTemp = $('<select><option value="1000">1000</option><option value="1001">1001</option><option value="1002">1002</option><option value="1003">1003</option><option value="1004">1004</option><option value="1005">1005</option><option value="1006">1006</option><option  value="1007">1007</option><option value="1008">1008</option><option value="1009">1009</option><option value="2000">2000</option><option value="2001">2001</option><option value="3000">3000</option><option value="4000">4000+</option></select>');
var cValInTemp = $('<input type="text" class="form-control" style="width: 120px;" value=""></input>');
var counterIdTemp = $('<input type="text" value="" disabled="disabled"></input>');



function editDusCounter(editBtn){
	
	var row = $(editBtn).closest("tr");
	var cIdCell = row.find("td:first-child");
	var cValCell = row.find("td:nth-child(2)");
	var cIdDD = cIdDDTemp.clone();
	var cValIn = cValInTemp.clone();
	var counterDD = counterIdTemp.clone();
		
	/*cIdDD.val(cIdCell.text());
	cIdCell.append(cIdDD);
	cIdCell.find("span").hide();
	*/
	counterDD.val(cIdCell.text());
	cIdCell.append(counterDD);
	cIdCell.find("span").hide();
	
	
	cValIn.val(cValCell.text());
	cValCell.append(cValIn);
	cValCell.find("span").hide();
	
	$(editBtn).hide().siblings().show();
}

function saveDusCounter(saveBtn){
	
	var row = $(saveBtn).closest("tr");
	var cIdCell = row.find("td:first-child");
	var cValCell = row.find("td:nth-child(2)");
	//var cIdDD = cIdCell.find("select");
	var cValIn = cValCell.find("input");
	var counterId = cIdCell.find("input");
	
	 var editMsisdnJson = { "uid" :uid , "counterId" : counterId.val(), "counterValue" : cValIn.val()} ;
	 var json = JSON.stringify(editMsisdnJson);

	 $.ajax({
    	url: '/edat/dusupdate/editMSiSDN',
    	data: json,
    	type: "POST",
    	beforeSend: function(xhr) {				    		
    		xhr.setRequestHeader("Accept", "application/json");
    		xhr.setRequestHeader("Content-Type", "application/json");
    	},
	    success: function (data) {
	    	//cIdCell.find("span").show().text(cIdDD.val());
			cValCell.find("span").show().text(cValIn.val());
			cIdCell.find("span").show().text(counterId.val());
			
	    	//cIdDD.remove();
			cValIn.remove();
			counterId.remove();
			
			$(saveBtn).prev().show().siblings().hide();
	    },
	    error: function () {
	        alert("Error in saving data to server!");
	    }
	});
}

function cancelDusCounter(cancelBtn){
	var row = $(cancelBtn).closest("tr");
	
	var cIdCell = row.find("td:first-child");
	var cValCell = row.find("td:nth-child(2)");
	
	//cIdCell.find("select").remove();
	cIdCell.find("input").remove();
	cValCell.find("input").remove();
	
	cIdCell.find("span").show();
	cValCell.find("span").show();
	
	$(cancelBtn).hide().prev().hide().prev().show();
} 
