/**
 * This is tree structure to create tree on UI.
 * Pass the JSON format data as parameter into createTree
 * 
 * @author Rahul Prakash
 */

var nodeChildren = [];		
function createTree(jsonData){
	var treeData = [];
	var child = null;
	for(var key in jsonData){
		var node = {};
		node.label = key;
		child = jsonData[key];
		if(isArray(jsonData[key]))
			createChildrenOfArrayType(key, child);
		else
			createChildrenOfObjectType(key, child);
		node.children = nodeChildren;
		treeData.push(node);
	}
	return treeData;
}

function createChildrenOfArrayType(key, array){
	nodeChildren = null;
	nodeChildren = [];
	var len = array.length;
	for(var i=0; i<len; i++){
		var obj = array[i];
		for(var itemkey in obj){
			if(isArray(obj[itemkey])){
				var node = {};
				var children = [];
				for(var j=0; j<obj[itemkey].length; j++){
					children.push(createChildrenOfObjectType(itemkey, obj[itemkey][j]));
				}
				console.log(itemkey, children);
				if(itemkey == children[children.length-1].label){					
					node.label = children[children.length-1].label;
					node.children = children[children.length-1].children;
				}
				else{
					node.label = itemkey;
					node.children = children;
				}
				nodeChildren.push(node);
			}
			else
				nodeChildren.push(createChildrenOfObjectType(itemkey, obj[itemkey]));
		}
	}
}

function createChildrenOfObjectType(key, value){
	var node = {};
	var subChildren = [];
	var t = [];
	var temp = [];
	if(typeof value === "object")
	{
		node.label = key;
		for(var itemkey in value){
			var child = {};
			if(isArray(value[itemkey])){
				for(var i=0; i<value[itemkey].length; i++){
					child = createChildrenOfObjectType(itemkey, value[itemkey][i]);
					t.push(child);
					var c = t[i].children;
					temp.push(c);
				}
				var concatChildren = [];
				var grandChildren = [];
				
				// First
//				for(var j=0; j<temp.length; j++){
//					var children = [];
//					children = temp[j];
//
//					for(var k=0; k<children.length; k++){
//						concatChildren.push(children[k]);
//					}
//				}
				
				// Second
				for(var k=0; k<t.length; k++){
					concatChildren = [];
					var ch = t[k].children;
					for(var m=0; m<ch.length; m++){
						concatChildren.push(ch[m]);
					}
					var subnode = {};
					subnode.label = itemkey + "-" + (k+1);
					subnode.children = concatChildren;
					grandChildren.push(subnode);
				}

				var subnode = {};
				subnode.label = itemkey;
				//subnode.children = concatChildren;
				subnode.children = grandChildren;
				subChildren.push(subnode);
			}
			else if(typeof value[itemkey] === "string"){
				child.label = itemkey + " : " + value[itemkey];
				subChildren.push(child);
			}
			else{
				child = createChildrenOfObjectType(itemkey, value[itemkey]);
				subChildren.push(child);
			}					
			node.children = subChildren;
		}
	}
	else if(typeof value === "string")
	{
		node.label = key + " : " + value;
	}
	
	else
	{
		node.label = key + " : " + value;
	}
	return node;
}

function isArray(object)
{
	if(!object) return false;
	if (object.constructor === Array) return true;
	else return false;
}
