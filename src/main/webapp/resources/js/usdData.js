var operationId = "";
var usdCustomerID = null;
var usdUID = null;
var usdMSISDN = null;
var usdIMSI = null;
var searchCretria = {};
var napDataObject = null;
var searchDropDownMenu = {
	"msisdn" : "MSISDN(1)",
	"imsi" : "IMSI",
	"customerId" : "Customer ID"
};
var customerInfo = null;
$(document)
		.ready(
				function() {
					
					$("#usdData").parent().addClass("active");
					$("#" + Constants.USD_SELECTED_TAB).addClass("activeUSD")
							.siblings().removeClass("activeUSD");
					resetUI1();
					createSearchDropDown(searchDropDownMenu);
					$("#searchText").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#search").click();
						}
					});
					 $("#enableLockImsiImei").hide();
					$("#search")
							.click(
									function() {
										searchCretria = {};
										if ($.trim($("#searchText").val()) == '') {
											alert(error_msg);
											$("#searchText").focus();
											return;
										}
										else {
											var element = $("#searchType")
													.val();
											var lockimsi = $("#lockImsiImei")
											.val();
											
											var flag = validateInput(element);
											if (flag) {
												searchCretria[$("#searchType")
														.val()] = $(
														"#searchText").val();
												if (element == "msisdn") {
													searchCretria = {};
													searchCretria[$(
															"#searchType")
															.val()] = '1'
															+ $("#searchText")
																	.val();
												}
												
												if(Constants.USD_SELECTED_TAB == Constants.EIR_UNLOCK)
													{
													
													   searchCretria["lockImsiImei"]= lockimsi;
													}
												dataProcessStart();
												getUsdData(searchCretria);
												$("#"+Constants.USD_SELECTED_TAB)
														.addClass("activeUSD")
														.siblings().removeClass("activeUSD");
												$('#usdContentHeading, #usdContentBody').show();
											}
										}
									});
					
					$("#update")
					.click(
							function() {
																		
											   searchCretria["lockImsiImei"]= $("#lockImsiImei").val();;
										dataProcessStart();
										updateEirUnlock(searchCretria);
										$("#"+Constants.USD_SELECTED_TAB)
												.addClass("activeUSD")
												.siblings().removeClass("activeUSD");
										$('#usdContentHeading, #usdContentBody').show();
									
								
							});
					$("#napDropDown li").click(
							function() {
								$(this).addClass("selected").siblings()
										.removeClass("selected");
								$("#" + Constants.USD_SELECTED_TAB).addClass(
										"activeUSD").siblings().removeClass(
										"activeUSD");
								switch (this.id) {
								case Constants.NAP_INFO.SOCS:
									generateUsdNapSOCsDatatable();
									break;
								case Constants.NAP_INFO.PARTNERS:
									generateUsdNapPartnersDatatable();
									break;
								case Constants.NAP_INFO.PROFILE:
									generateUsdNapProfileDatatable();
								default:
									break;
								}
							});

					$("#usdTabList li").click(
							function() {
								$(this).addClass("activeUSD").siblings()
										.removeClass("activeUSD");

								Constants.USD_SELECTED_TAB = this.id;

								$('#panelHeading').html(
										(this.id).toUpperCase() + "  Data");
								 searchDropDownMenu = {
											"msisdn" : "MSISDN(1)",
											"imsi" : "IMSI",
											"customerId" : "Customer ID"
										};
								 $("#enableLockImsiImei").hide();
								 $("#tacHint").css('visibility','hidden');
								// $("#usdSearchDropDown").show();
								 $("#searchLbl").text("Search");
								 $("#searchType").show();
								 $('#search').show();
								 $('#searchText').show();
								 $('#searchLbl').show();
								 document.getElementById("search").innerText = 'Search';
								 
								switch (this.id) {
								
								case Constants.DUS:
									resetUI1();
									$("#napDropDown").hide();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="DUS";
									sendRequestForSearch();
									break;
								case Constants.PCRF:
									resetUI1();
									$("#napDropDown").hide();
									sendRequestForSearch();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="PCRF";
									break;
								case Constants.UMA:
									resetUI1();
									$("#napDropDown").hide();
									sendRequestForSearch();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="UMA";
									break;
								case Constants.HSS:
									resetUI1();
									$("#napDropDown").hide();
									sendRequestForSearch();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="HSS";
									break;
								case Constants.HLR:
									resetUI1();
									$("#napDropDown").hide();
									sendRequestForSearch();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="HLR";
									break;
								case Constants.EIR:
									resetUI1();
									$("#napDropDown").hide();
									 searchDropDownMenu = {
												"msisdn" : "MSISDN(1)",
												"imsi" : "IMSI",
												"customerId" : "Customer ID"

											};
									sendRequestForSearch();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="EIR";
									break;
								case Constants.EIR_SEARCH:
									resetUI1();
									$("#napDropDown").hide();
									$('#panelHeading').html(
											Constants.EIR.toUpperCase()
													+ "  Data");
									 searchDropDownMenu = {
											 "msisdn" : "MSISDN(1)",
											 "imsi" : "IMSI",
											 "customerId" : "Customer ID"
											};
									sendRequestForSearch();
									$("#searchLbl").html('Search<span style="color: red;">*</span>');
									document.title="EIR Search";
									break;
								searchDropDownMenu = {
									"msisdn" : "MSISDN(1)",
									"imsi" : "IMSI",
									"customerId" : "Customer ID"
								};
								$("#napDropDown").hide();
								if (this.id == Constants.EIR) {
									$("ul#searchEirTabs").slideToggle(
											500,
											function() {
												$("#searchEir").addClass(
														"activeUSD");
											});
								}
								if (this.id == Constants.EIR_SEARCH) {
									$('#panelHeading').html(
											Constants.EIR.toUpperCase()
													+ "  Data");
								}

								break;
							case Constants.EIR_ADD:
								resetUI();
								$("#eirInput").show();
								$("#searchLbl").html('IMSI<span style="color: red;">*</span>');
								searchDropDownMenu = {
									"imsi" : "IMSI"
								};
								//$("#usdSearchDropDown").show();
								$("#searchType").hide();
								$("#searchType").val("imsi");
								document.getElementById("search").innerText = 'Save';
								$("#inputTAC").hide();
								$("#labelTAC").hide();
								document.title="EIR Add";
								
								break;
							case Constants.EIR_UNLOCK:
								resetUI();
								$("#eirInput, #addTacInput")
										.hide();
								searchDropDownMenu = {
									"imsi" : "IMSI"
								};
								$("#enableLockImsiImei").hide();
								$("#napDropDown").hide();
								$("#searchLbl").html('Search<span style="color: red;">*</span>');
								document.title="EIR Unlock";
								break;
							case Constants.EIR_SEARCH_TAC:
								resetUI();
								$('#panelHeading').text("Search Tac Data");
								$("#searchType").hide();
								$("#searchLbl").html('TAC<span style="color: red;">*</span>');
								$("#eirInput, #addTacInput")
										.hide();
								searchDropDownMenu = {
									"tac" : "TAC"
								};
								$("#napDropDown").hide();
								document.title="EIR Search TAC";
								break;
							case Constants.EIR_ADD_TAC:
								resetUI();
								$("#eirInput, #napDropDown")
										.hide();
								searchDropDownMenu = {
										"tac" : "TAC"
								};
								$("#addTacInput").show();
								$("#tacHint").css('visibility','visible');
								$("#searchType").hide();
								$("#searchLbl").html('TAC<span style="color: red;">*</span>');
								document.getElementById("search").innerText = 'Update';
								document.title="EIR Add TAC";
								break;
							case Constants.EIR_ENABLE_MMS:
								resetUI();
								$("#eirInput, #napDropDown, #addTacInput")
										.hide();
								searchDropDownMenu = {
										"tac" : "TAC"
								};
								$("#tacHint").css('visibility','visible');
								$("#searchType").hide();
								$("#searchLbl").html('TAC<span style="color: red;">*</span>');
								document.title="EIR Enable MMS";
								break;
							case Constants.NAP:
								resetUI1();
								//$("#enableMMSDiv").show();
								sendRequestForSearch();
								$("#searchLbl").html('Search<span style="color: red;">*</span>');
								document.title="NAP";

							default:
								searchDropDownMenu = {
									"msisdn" : "MSISDN(1)",
									"imsi" : "IMSI",
									"customerId" : "Customer ID"
								};
								$("#napDropDown").show();
								$("#" + Constants.NAP_INFO.PROFILE).addClass(
										"selected").siblings().removeClass(
										"selected");
								$("#searchLbl").html('Search<span style="color: red;">*</span>');
								break;
							}
							if(this.id!=Constants.EIR_ADD)
							{
							createSearchDropDown(searchDropDownMenu);
							}
						});
				});

function sendRequestForSearch() {

	if ($("#searchText").val() != "") {
		var element = $("#searchType").val();
		if (element == "msisdn") {
			searchCretria = {};
			searchCretria[$("#searchType").val()] = '1'
					+ $("#searchText").val();
		} else {
			searchCretria[$("#searchType").val()] = $("#searchText").val();
		}

		dataProcessStart();
		getUsdData(searchCretria);
	}
}

function createSearchDropDown(options) {
	var optionMenu = $('#searchType');
	var selectedvalue=$( "#searchType option:selected" ).val();
	
	optionMenu.empty();
	
	for ( var val in options) {
		$("<option />", {
			value : val,
			text : options[val]
		}).appendTo(optionMenu);
	}
	
	// for making selected value 
	$('[name=searchType] option').filter(function() { 
	    return ($(this).val() == selectedvalue); //To select the previous value
	}).prop('selected', true);
	//optionMenu.appendTo($('#dropDownSpan'));
}

function resetUI() {

	$("#searchText").val("");
	$("#inputTAC").val("");
	$("#inputIMEI").val("");
	$("#deviceManufacturer").val("");
	$("#deviceModel").val("");
	$('#enableMMSOption option[value="yes"]').attr("selected", "selected");
	$("#nap-tab").hide();
	$("#eirInput, #addTacInput").hide();
	$('#usdContentHeading').text("");
	$('#usdContentBody').empty();
	$('#usdContentBody').html("");
	$('#searchText').attr('readonly', false);
	$('#deviceManufacturer').attr('readonly', false);
	$('#deviceModel').attr('readonly', false);
	$('#deviceBands').attr('readonly', false);
	$('#deviceScreenResolution').attr('readonly', false);
	$('#enableMMSFlag').val("false");
	
	$("#inputFastBlackList").val("FALSE");
	$("#inputLockImsiImei").val("FALSE");
	$("#inputDualImsi").val("FALSE");

}

function resetUI1() {
	$("#inputTAC").val("");
	$("#inputIMEI").val("");
	$("#deviceManufacturer").val("");
	$("#deviceModel").val("");
	$('#enableMMSOption option[value="yes"]').attr("selected", "selected");
	$("#nap-tab").hide();
	$("#eirInput, #addTacInput").hide();
	$('#usdContentHeading').text("");
	$('#usdContentBody').empty();
	$('#usdContentBody').html("");
	$('#searchText').attr('readonly', false);
	$('#deviceManufacturer').attr('readonly', false);
	$('#deviceModel').attr('readonly', false);
	$('#deviceBands').attr('readonly', false);
	$('#deviceScreenResolution').attr('readonly', false);
	$('#enableMMSFlag').val("false");

}

function validateInput(elementObj) {
	if (elementObj == "emailId") {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test($("#searchText").val())) {
			return true;
		} else {
			alert('Invalid Email Address');
			$("#searchText").focus();
			return false;
		}
	} else if (elementObj == "msisdn" && $("#searchText").val().length != 10) {
		alert('Invalid MSISDN');
		$("#searchText").focus();
		return false;
	} else if (elementObj == "imsi" && $("#searchText").val().length != 15) {
		alert('Invalid IMSI');
		$("#searchText").focus();
		return false;
	} else if (elementObj == "imei" && $("#searchText").val().length != 8) {
		alert('Invalid IMEI');
		$("#searchText").focus();
		return false;
	} else if (elementObj == "tac" && $("#searchText").val().length != 8) {
		alert('Invalid TAC');
		$("#searchText").focus();
		return false;
	} 
	else if(Constants.USD_SELECTED_TAB == Constants.EIR_ADD)
	{
	  
	  if ($.trim($("#inputIMEI").val()) == '') {
		alert(error_msg);
		$("#inputIMEI").focus();
		return false;
	  }else if($("#inputIMEI").val().length!=14)
		   {
			    alert('Invalid IMEI');
				$("#inputIMEI").focus();
				return false;
		   }else
			{
		  return true;
	  }
	 
	}else if(Constants.USD_SELECTED_TAB == Constants.EIR_ADD_TAC)
	{
		  if ($.trim($("#deviceManufacturer").val()) == '' && $.trim($("#deviceModel").val()) == '') {
			   alert(error_msg);
			   $("#deviceManufacturer").focus();
			return false;
		  }else{
			  return true;
		  }
		 
	}
	else {
		return true;
	}
}

function dataProcessStart() {
	$('#loading').html(
			'<img src="' + Constants.CONTEXT_PATH
					+ '/resources/images/process.gif"> Loading...');
	$('#extraInfo, #extrainfo2').hide();
	$('#extraInfo3').hide();
	$('#extraInfoimpi').hide();
	$('#extraInfo5').hide();
	$('#extraInfonoti').hide();
	// $("#nap-tab").show();
}

function dataProcessEnd() {
	$('#loading').empty();
}

function displayErroGrid() {
	$('#napDataTable').hide();
	$('#usdContentHeading').text("");
	$('#usdContentBody').empty();
	$('#usdContentBody').html("Node Not Reachable !");
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function displayInternalErroGrid(key) {
	$('#napDataTable').hide();
	$('#usdContentHeading').text("");
	$('#usdContentBody').empty();
	$('#usdContentBody').html("Customer Not Found !");
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function noDataFoundGrid() {
	$('#napDataTable').hide();
	$('#usdContentHeading').text("");
	$('#usdContentBody').empty();
	$('#usdContentBody').html("No data found in " + Constants.USD_SELECTED_TAB);
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function updateEirUnlock(data) {
		data["uid"]=$("#eirImsiUnlockUID").val();;
		data["lockImsiImei"]= $("#inputLockImsiImei").val();
		sendAddEIRDataRequest(data);
       }

function getUsdData(searchCretria) {
	var searchUrl = Constants.CONTEXT_PATH + Constants.USD
			+ Constants.USD_SELECTED_TAB;

	switch (Constants.USD_SELECTED_TAB) {
	case Constants.NAP:
		sendDataRequest(searchUrl, searchCretria);
		break;
	case Constants.DUS:
		sendDUSDataRequest(searchUrl, searchCretria);
		break;
	case Constants.PCRF:
		sendPCRFDataRequest(searchUrl, searchCretria);
		break;
	case Constants.UMA:
	case Constants.HSS:
	case Constants.HLR:
	case Constants.EIR:
	case Constants.EIR_SEARCH:
	case Constants.EIR_ADD:
	case Constants.EIR_UNLOCK:
		getUIDFromDUS(Constants.CONTEXT_PATH + Constants.USD + "getUid",
				searchCretria, Constants.USD_SELECTED_TAB);
		break;
	case Constants.EIR_SEARCH_TAC:
		getTacData(searchCretria);
		break;
	case Constants.EIR_ADD_TAC:
		//searchCretria["tac"]= $('#searchText').val();
		addTacData(searchCretria);
		break;
	case Constants.EIR_ENABLE_MMS:
		enableTacData(searchCretria);
		break;
	default:
		sendDataRequest(searchUrl, searchCretria);
		break;
	}
	$("#nap-tab").show();
}

function sendDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$
			.ajax({
				url : searchUrl,
				data : jsonData,
				type : "POST",
				beforeSend : function(xhr) {
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				error : function(xhr, ajaxOptions, thrownError) {
					$("#searchText").val("");
					usdCustomerID = null;
					usdUID = null;
					usdMSISDN = null;
					usdIMSI = null;
					displayErroGrid();
					napDataObject = null;
				},
				success : function(result) {
					napDataObject = result;
					if (result.applicationError) {
						displayInternalErroGrid(result.applicationError.errorCode);
						return;
					}
					if (result) {
						console.log(result.profile.customerId + " - "
								+ result.profile.uid + " - "
								+ result.profile.msisdn + " - "
								+ result.profile.imsi);
						usdCustomerID = result.profile.customerId;
						usdMSISDN = '1' + result.profile.msisdn;
						usdIMSI = result.profile.imsi;
						customerInfo = "";
						var customerDetails = "<div id='cusInfo'><table class='table table-striped' style='table-layout: fixed;' border='1px'>";
						customerDetails += "<tr><td>Customer Name : "
								+ napDataObject.profile.attributes.firstName
								+ " "
								+ napDataObject.profile.attributes.lastName
								+ "</td><td> Customer MSISDN : " + usdMSISDN
								+ " </td></tr>";
						
						customerDetails += "<tr><td>Operator Id (Subscriber Name) : " + operationId
								+ "</td><td> Customer Id : " + usdCustomerID
								+ " </td></tr>";
						customerDetails += "</table></div>";
						customerInfo = customerInfo + "<br>" + customerDetails
								+ "<br>";
					}
					generateUsdNapProfileDatatable();
				}
			});
}
// NAP Profile Data
function generateUsdNapProfileDatatable() {
	var result = napDataObject;
	if (result == null) {
		displayErroGrid();
		return;
	} else if (result == "") {
		noDataFoundGrid();
		return;
	} else if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}

	var profileTable = "<table id='usdprofile' data-toggle='table' class= 'display' data-height='180'><thead><tr><th>Attribute</th>";
	profileTable += "<th> Value </th>";
	profileTable += "</tr> </thead>  <tbody>";
	for ( var key in result) {
		if (key == 'profile' && result.hasOwnProperty('profile')) {
			for ( var profileKey in result.profile) {
				if (result.profile[profileKey] && profileKey != 'attributes'
						&& profileKey != 'customerId') {
					profileTable += "<tr><td>" + profileKey + "</td>";
					profileTable += "<td>"
							+ result.profile[profileKey] + "</td>";
					profileTable += "</tr> ";
				}
				if (profileKey == 'attributes') {
					for ( var keys in result.profile.attributes) {
						if (keys == 'objectClass') {
							continue;
						}
						profileTable += "<tr><td>" + keys + "</td>";
						profileTable += "<td>"
								+ result.profile.attributes[keys] + "</td>";
						profileTable += "</tr> ";
					}
				}
			}
		}
	}
	profileTable += "</tbody></table>";
	$('#usdContentHeading').text("Contents for NAP : Profile");
	$('#usdContentBody').empty();
	document.getElementById('usdContentBody').innerHTML = profileTable;

	$('#usdprofile').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,

		"columns" : [ null, null ],
		"jQueryUI" : true,
	});
	
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
	
}

function generateUsdNapSOCsDatatable() {
	var result = napDataObject;
	if (result == null) {
		displayErroGrid();
		return;
	} else if (result == "") {
		noDataFoundGrid();
		return;
	} else if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	
	var htmlTable = customerInfo;
	htmlTable += "<table data-toggle='table' class= 'display' data-height='180' id='napSocdatatable'><thead><tr><th></th>";
	
	for ( var counters in result.socsList) {
		var partnerObj = result.socsList[counters];
		for ( var prop in partnerObj) {
			if (prop == 'socCode' && partnerObj.hasOwnProperty('socCode')) {
				htmlTable += "<th> " + partnerObj.socCode + "</th>";
			}
		}
	}
	
	htmlTable += "</tr></thead> <tbody>";

	for ( var set in result.socAttributesSuperset) {
		if (set == "objectClass")
			continue;
		htmlTable += "<tr><td>" + set + "</td>";
		for ( var soc in result.socsList) {
			if (result.socsList[soc].attributes[set]) {
				htmlTable += "<td style='overflow:hidden; word-wrap: break-word;'>"
						+ result.socsList[soc].attributes[set]
						+ "</td>";
			} else {
				htmlTable += "<td>-</td>";
			}
		}
		htmlTable += "</tr>";
	}
	
	htmlTable += "</tbody></table>";
	$('#usdContentHeading').text("Contents for NAP : SOCs");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(htmlTable);
	
	$('#napSocdatatable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});
	
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function generateUsdNapPartnersDatatable() {
	var result = napDataObject;
	if (result == null) {
		displayErroGrid();
		return;
	} else if (result == "") {
		noDataFoundGrid();
		return;
	} else if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	
	
	var partnersHtml = customerInfo;
	//partnersHtml += "<span id='uid-span-info' style='font-size: 14px;'>Partners List</span>";
	partnersHtml +="<table data-toggle='table' class= 'display' data-height='180'  id ='partnersHtml'><thead><tr><th></th>";
	for ( var counters in result.partnersList) {
		var partnerObj = result.partnersList[counters];
		for ( var prop in partnerObj) {
			if (prop == 'partnerId' && partnerObj.hasOwnProperty('partnerId')) {
				partnersHtml += "<th> id = " + partnerObj.partnerId + "</th>";
			}
		}
	}
	partnersHtml += "</tr> </thead>  <tbody>";
	for ( var set in result.partnersrAttributesSuperset) {
		if (set == "objectClass")
			continue;
		partnersHtml += "<tr><td>" + set + "</td>";
		for ( var partners in result.partnersList) {
			if (result.partnersList[partners].attributes[set]) {
				partnersHtml += "<td style='overflow:hidden; word-wrap: break-word;'>"
						+ result.partnersList[partners].attributes[set]
						+ "</td>";
			} else {
				partnersHtml += "<td>-</td>";
			}
		}
		partnersHtml += "</tr>";
	}
	partnersHtml += "</tr><tbody></table>";
	$('#usdContentHeading').text("Contents for NAP : Partners");
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(partnersHtml);
	
	$('#partnersHtml').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"bAutoWidth": false, 
		"jQueryUI" : true,
	});
		
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function sendDUSDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			usdUID = result.uid;
			generateDUSUI(result);
		}
	});
}

function generateDUSUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}

	var subProfileHtml = "<table id ='dusData' data-toggle='table' class= 'display' data-height='180'><thead><tr><th>Attribute</th>";
	subProfileHtml += "<th> Value </th>";
	subProfileHtml += "</tr>  </thead>  <tbody>";
	var counterHtml = "<table data-toggle='table' class= 'display' data-height='180'  id ='counterHTable'><thead><tr><th>amfCounter</th>";
	var itration = -1;
	for ( var key in result) {
		if (key == 'uid' && result.hasOwnProperty('uid')) {
			subProfileHtml += "<tr><td>" + key + "</td>";
			subProfileHtml += "<td>"
					+ (result.uid ? result.uid : " ") + "</td>";
		} else if (key == 'attributes' && result.hasOwnProperty('attributes')) {
			for ( var attributes in result.attributes) {
				if (attributes == "objectClass")
					continue;
				subProfileHtml += "<tr><td>" + attributes + "</td>";
				subProfileHtml += "<td>"
						+ result.attributes[attributes] + "</td>";
			}
		} else if (key == 'counters' && result.hasOwnProperty('counters')) {
			for ( var counters in result.counters) {
				var counterObj = result.counters[counters];
				for ( var prop in counterObj) {
					if (prop == 'counterId'
							&& counterObj.hasOwnProperty('counterId')) {
						counterHtml += "<th > id = " + counterObj.counterId
								+ "</th>";
						itration++;
					}
				}
			}
			counterHtml += "</tr> </thead> <tbody>";
		}
	}
	for ( var set in result.counterSuperset) {
		if (set == "objectClass")
			continue;
		counterHtml += "<tr><td>" + set + "</td>";
		for ( var counter in result.counters) {
			if (result.counters[counter].attributes[set]) {
				counterHtml += "<td>"
						+ result.counters[counter].attributes[set] + "</td>";
			} else {
				if (set == "amfCounterValue") {
					counterHtml += "<td>"
							+ result.counters[counter].counterValue + "</td>";
				} else {
					counterHtml += "<td>-</td>";
				}
			}
		}
		counterHtml += "</tr>";
	}
	subProfileHtml += " </tbody></table>";
	counterHtml += "</tr> </tbody></table>";
	$('#usdContentHeading').text("Content for DUS : Subscriber Profile");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	$('#extraInfoBody').empty();
	$('#extraInfoHeading').text("DUS : amfCounters");
	$('#extraInfoBody').html(counterHtml);
	$('#extraInfo').show();

	$('#dusData').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"bAutoWidth": false, 
		"jQueryUI" : true,
	});

	$('#counterHTable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"bAutoWidth": false, 
		"jQueryUI" : true,
	});

	$("#resultPanel").css('visibility','visible');dataProcessEnd();

}

function sendPCRFDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generatePCRFUI(result);
		}
	});
}

function generatePCRFUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	
	var subProfileHtml = "<table  data-toggle='table' class= 'display' data-height='180' id='subpcrfProfile'><thead><tr><th>Attribute</th>";
	subProfileHtml += "<th> Value </th>";
	subProfileHtml += "</tr></thead><tbody>";
	var activeSessionHtml = "<table data-toggle='table' class= 'display' data-height='180' id='sessionpcrfProfile'><thead><tr><th>Attribute</th>";
	activeSessionHtml += "<th> Value </th>";
	activeSessionHtml += "</tr></thead><tbody>";
	for ( var key in result) {
		if (key == 'attributes' && result.hasOwnProperty('attributes')) {
			for ( var attributes in result.attributes) {
				if (attributes == 'objectClass') {
					continue;
				}
				subProfileHtml += "<tr><td>" + attributes + "</td>";
				subProfileHtml += "<td >"
						+ result.attributes[attributes] + "</td>";
			}
		} else if (key == 'activeSession'
				&& result.hasOwnProperty('activeSession')) {
			for ( var asKey in result.activeSession) {
				activeSessionHtml += "<tr><td>" + asKey + "</td>";
				activeSessionHtml += "<td >"
						+ result.activeSession[asKey] + "</td>";
			}
		}
	}
	subProfileHtml += "</tbody></table>";
	activeSessionHtml += "</tbody></table>";
	$('#usdContentHeading').text("Content for PCRF : Subscriber Profile");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	$('#extraInfoBody').empty();
	$('#extraInfoHeading').text("Content for PCRF : Active Session Info");
	$('#extraInfoBody').html(activeSessionHtml);
	$('#extraInfo').show();
	
	$('#subpcrfProfile').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
         "jQueryUI" : true,
	});
	$('#sessionpcrfProfile').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
         "jQueryUI" : true,
	});
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function sendUMADataRequest(searchUrl, searchCretria) {
	var jsonData = JSON.stringify(searchCretria);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateUMAUI(result);
		}
	});
}

function generateUMAUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}

	var subProfileHtml = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	subProfileHtml += "<table id = 'umaDataTable' data-toggle='table' class= 'display' data-height='180' id='insdpTable'><thead><tr><th>Attribute</th><th>  Value</th>";
	subProfileHtml += "</tr></thead><tbody>";
	for ( var key in result) {
		if (key == 'attributes' && result.hasOwnProperty('attributes')) {
			for ( var attributes in result.attributes) {
				if (attributes == 'objectClass') {
					continue;
				}
				
				if(attributes == "subscriberName")
					{
					   operationId = result.attributes[attributes];
					}
				subProfileHtml += "<tr><td>" + attributes + "</td>";
				subProfileHtml += "<td>"
						+ result.attributes[attributes] + "</td>";
			}
		}
	}
	subProfileHtml += "</tbody></table>";
	$('#usdContentHeading').text("UMA Info");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);

	$('#umaDataTable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
         "jQueryUI" : true,
	});

	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function sendHSSDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateHSSUI(result);
		}
	});
}


function generateHSSUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}

	//hssServiceProfile
	var hssServiceProfile = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	hssServiceProfile += "<table data-toggle='table' class= 'display' data-height='180' id='hssServiceProfile'><thead><tr><th>Attribute</th><th>  Value</th>";
	hssServiceProfile += "</tr></thead><tbody>";
	
	//hssUserFilterCriteria
	var hssUserFilterCriteria = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	hssUserFilterCriteria += "<table data-toggle='table' class= 'display' data-height='180' id='hssUserFilterCriteria'><thead><tr><th>Attribute</th><th>  Value</th>";
	hssUserFilterCriteria += "</tr></thead><tbody>";
	
	//hssIrs
	var hssIrs = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	hssIrs += "<table data-toggle='table' class= 'display' data-height='180' id='hssIrs'><thead><tr><th>Attribute</th><th>  Value</th>";
	hssIrs += "</tr></thead><tbody>";
	
	//hssImpi
	var hssImpi = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	hssImpi += "<table data-toggle='table' class= 'display' data-height='180' id='hssImpi'><thead><tr><th>Private Id</th>";
	hssImpi += "</tr></thead><tbody>";
	
	//hssImpu
	var hssImpu = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	hssImpu += "<table data-toggle='table' class= 'display' data-height='180' id='hssImpu'><thead><tr><th>Service Profile Name</th><th>Public Id</th>";
	hssImpu += "</tr></thead><tbody>";
	
	//hssNotification
	var hssNotification = "<span id='uid-span-info'> uid: " + usdUID + "</span>";
	hssNotification += "<table data-toggle='table' class= 'display' data-height='180' id='hssNotification'><thead><tr><th>Attribute</th><th>  Value</th>";
	hssNotification += "</tr></thead><tbody>";
	
	
	for ( var key in result) {
		if (key == 'hssServiceProfile' && result.hasOwnProperty('hssServiceProfile')) {
			for ( var attributes in result.hssServiceProfile) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(result.hssServiceProfile[attributes]!=null)
				{
				var array = result.hssServiceProfile[attributes].split(',')
				var index=array[0].indexOf(":");
				if(index!=0)
				{
					var firstIndexString=array[0].substring(index+1,array[0].length);
					hssServiceProfile += "<tr><td>" + "globalFilterIds" + "</td>";
					hssServiceProfile += "<td>" + firstIndexString+ "</td>";
				}
				for(var i=1;i<array.length;i++)
				{
				
				hssServiceProfile += "<tr><td>" + "globalFilterIds" + "</td>";
				hssServiceProfile += "<td>" + array[i]+ "</td>";
				}
				}
				
			}
		}
		
		if (key == 'hssUserFilterCriteria' && result.hasOwnProperty('hssUserFilterCriteria')) {
			for ( var attributes in result.hssUserFilterCriteria) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="filterName")
				{
				hssUserFilterCriteria += "<tr><td>" + attributes + "</td>";
				hssUserFilterCriteria += "<td>" + result.hssUserFilterCriteria[attributes]
						+ "</td>";
				}
			}
		}
		
		if (key == 'hssIrs' && result.hasOwnProperty('hssIrs')) {
			for ( var attributes in result.hssIrs) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="registrationStatus")
				{
				hssIrs += "<tr><td>" + attributes + "</td>";
				hssIrs += "<td>" + result.hssIrs[attributes]
						+ "</td>";
				}
			}
		}
		
		if (key == 'hssImpi' && result.hasOwnProperty('hssImpi')) {
			for ( var attributes in result.hssImpi) {
				if (attributes == 'objectClass') {
					continue;
				}
				var myArray = new Array();
				myArray=result.hssImpi.hssImpiList.split('{');
				for(var i=2;i<myArray.length;i++)
				{
					hssImpi += "<tr>";
					 var lastattr = null;
					var attrArray  = myArray[i].split(',');
					for(j =0;j<attrArray.length;j++)
					{

						var attValue = attrArray[j].split('=');
						  		if(attValue.length >1 && attValue[0].trim() == "privateId")  
						  			hssImpi += "<td>" + attValue[1] + "</td>";
						  		  
							   
					}
				}
			}
		}
		
		if (key == 'hssImpu' && result.hasOwnProperty('hssImpu')) {
			/*for ( var attributes in result.hssImpu) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="publicId" || attributes=="serviceProfileName")
				{
				hssImpu += "<tr><td>" + attributes + "</td>";
				hssImpu += "<td>" + result.hssImpu[attributes]
						+ "</td>";
				}
			}*/
			var myArray = new Array();
			myArray=result.hssImpu.hssImpuList.split('{');
			for(var i=2;i<myArray.length;i++)
			{
				hssImpu += "<tr>";
				 var lastattr = null;
				var attrArray  = myArray[i].split(',');
				for(j =0;j<attrArray.length;j++)
				{

					var attValue = attrArray[j].split('=');
					  		if(attValue.length >1 && attValue[0].trim() == "serviceProfileName")  
					  		   hssImpu += "<td>" + attValue[1] + "</td>";
					  		if(attValue.length >1 && attValue[0].trim() == "publicId")  
								hssImpu += "<td>" + attValue[1]  + "</td>";	   
						   
				}
			}
		
		}
		
		if (key == 'hssNotification' && result.hasOwnProperty('hssNotification')) {
			for ( var attributes in result.hssNotification) {
				if (attributes == 'objectClass') {
					continue;
				}
				hssNotification += "<tr><td>" + attributes + "</td>";
				hssNotification += "<td>" + result.hssNotification[attributes]
						+ "</td>";
			}
		}
		
	}
	hssServiceProfile += "</tbody</table>";
	hssUserFilterCriteria += "</tbody</table>";
	hssIrs += "</tbody</table>";
	hssImpi += "</tbody</table>";
	hssImpu += "</tbody</table>";
	hssNotification += "</tbody</table>";
	
	$('#usdContentHeading').text("HSS Service Profile");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(hssServiceProfile);
	
	//extraInfo 
	$('#extraInfoBody').empty();
	$('#extraInfoHeading').text("Hss User Filter");
	$('#extraInfoBody').html(hssUserFilterCriteria);
	$('#extraInfo').show();
	
	//extraInfo3
	$('#extraInfoBody3').empty();
	$('#extraInfoHeading3').text("Hss IRS");
	$('#extraInfoBody3').html(hssIrs);
	$('#extraInfo3').show();
	
	//extraInfo4
	$('#extraInfoBody4').empty();
	$('#extraInfoHeading4').text("Hss IMPI");
	$('#extraInfoBody4').html(hssImpi);
	$('#extraInfoimpi').show();
	
	//extraInfo5
	$('#extraInfoBody5').empty();
	$('#extraInfoHeading5').text("Hss IMPU");
	$('#extraInfoBody5').html(hssImpu);
	$('#extraInfo5').show();
	
	//extraInfo6
	$('#extraInfoBody6').empty();
	$('#extraInfoHeading6').text("Hss Notification");
	$('#extraInfoBody6').html(hssNotification);
	$('#extraInfonoti').show();
	
	
	$('#hssServiceProfile').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
         "jQueryUI" : true,
	});

	$('#hssUserFilterCriteria').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});

	$('#hssIrs').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});

	$('#hssImpi').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});

	$('#hssImpu').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});

	$('#hssNotification').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});
	
	dataProcessEnd();

	$("#resultPanel").css('visibility','visible');dataProcessEnd();

}


function sendHLRDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateHLRUI(result);
		}
	});
}

function generateHLRUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}

	var subinnss = "<table data-toggle='table' class= 'display' data-height='180' id='subinnss'><thead><tr><th>Attribute</th><th>  Value</th>";
	subinnss += "</tr></thead>  <tbody>";
	
	
	var epsdata = "<table  data-toggle='table' class= 'display' data-height='180' id='epsdata'><thead><tr><th>Attribute</th><th>  Value</th>";
	epsdata += "</tr></thead>  <tbody>";
	
	var non3gppdata = "<table data-toggle='table' class= 'display' data-height='180' id='non3gppdata'><thead><tr><th>Attribute</th><th>  Value</th>";
	non3gppdata += "</tr></thead>  <tbody>";
	
	
	var gprsdata = "<table data-toggle='table' class= 'display' data-height='180' id='gprsdata'><thead><tr><tr><th>PdpContextId</th><th>  PdpType</th><th>AccPointName</th><th>RefqOfServName</th>";
	gprsdata += "</tr></thead>  <tbody>";	
	
	var pdncontext = "<table data-toggle='table' class= 'display' data-height='180' id='pdncontext'><thead><tr><th> PdnContextId </th><th>Pdn Acc Point Name</th><th>pdnType</th><th>RefLteQOfServName</th>";
	pdncontext += "</tr></thead>  <tbody>";
	//<th>PdnGwIPv4</th>
	for ( var key in result) {
		
		if (key == 'subinnss' && result.hasOwnProperty('subinnss')) {
			for ( var attributes in result.subinnss) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="sgsnNumber" || attributes=="vlrNumber" ||attributes=="sgsnAddress"||attributes=="mscNumber" ||attributes=="odbBaoc" ||attributes=="odbBaic" ||attributes=="odbHplmn" ||attributes=="isActiveIMSI" ||attributes=="actIMSIGprs" ||attributes=="clipOverr" ||attributes=="clirRestrOpt" ||attributes=="refroamAreaName" ||attributes=="refinmtcName" ||attributes=="refinmocName" ||attributes=="cfdFTNOforCFU" ||attributes=="cfdFTNOforCFB" ||attributes=="cfdFTNOforCFNRY" ||attributes=="cfdFTNOforCFNRC" ||attributes=="IMEISV")
				{
				subinnss += "<tr><td>" + attributes + "</td>";
				subinnss += "<td >" + result.subinnss[attributes]
						+ "</td>";
				}
			}
		} if (key == 'epsdata' && result.hasOwnProperty('epsdata')) {
			for ( var attributes in result.epsdata) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="sessionTransNum" || attributes=="mmeIdentity")
				{
				epsdata += "<tr><td>" + attributes + "</td>";
				epsdata += "<td >" + result.epsdata[attributes]
						+ "</td>";
				}
			}
		} if (key == 'non3gppdata' && result.hasOwnProperty('non3gppdata')) {
			for ( var attributes in result.non3gppdata) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="aaaServerAddress")
				{
				non3gppdata += "<tr><td>" + attributes + "</td>";
				non3gppdata += "<td >" + result.non3gppdata[attributes]
						+ "</td>";
				}
			}
		} if (key == 'gprsdata' && result.hasOwnProperty('gprsdata')) {
			var myArray = new Array();
			
			myArray=result.gprsdata.gprsList.split('{');
			
			for(var i=2;i<myArray.length;i++)
			{
				 gprsdata += "<tr>";
				 var lastattr = null;
				var attrArray  = myArray[i].split(',');
				for(j =0;j<attrArray.length;j++)
				{

					var attValue = attrArray[j].split('=');
					   if(attValue.length > 1 && attValue[0].trim() == "pdpContextId")
						   {
						    var  value = attValue[1].replace('}','');
						    value = value.replace(']','');
						   
						     gprsdata +="<td>"+value+"</td>";
						   }
					   if(attValue.length > 1 && attValue[0].trim() == "pdpType")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					     gprsdata +="<td>"+value+"</td>";
					   }
					   if(attValue.length > 1 && attValue[0].trim() == "accPointName")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					     gprsdata +="<td>"+value+"</td>";
					   }
					   if(attValue.length > 1 && attValue[0].trim() == "refqOfServName")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					     gprsdata +="<td>"+value+"</td>";
					   }
				}
			}
			
			
			/*for ( var attributes in result.gprsdata.gprsList) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="pdpContextId" || attributes=="pdpType" || attributes=="accPointName" || attributes=="refqOfServName")
				{
				gprsdata += "<tr><td>" + attributes + "</td>";
				gprsdata += "<td >" + result.gprsdata[attributes]
						+ "</td>";
				}
			}*/
		} if (key == 'pdncontext' && result.hasOwnProperty('pdncontext')) {
			/*for ( var attributes in result.pdncontext) {
				if (attributes == 'objectClass') {
					continue;
				}
				if(attributes=="pdnAccPointName" || attributes=="pdnContextId" || attributes=="pdnType" || attributes=="refLteQOfServName" || attributes=="pdnGwIPv4")
				{
				pdncontext += "<tr><td>" + attributes + "</td>";
				pdncontext += "<td >" + result.pdncontext[attributes]
						+ "</td>";
				}
			}*/
			
			

			var myArray = new Array();
			
			myArray=result.pdncontext.pdnContextList.split('{');
			
			for(var i=2;i<myArray.length;i++)
			{
				pdncontext += "<tr>";
				 var lastattr = null;
				var attrArray  = myArray[i].split(',');
				for(j =0;j<attrArray.length;j++)
				{

					var attValue = attrArray[j].split('=');
					   if(attValue.length > 1 && attValue[0].trim() == "pdnAccPointName")
						   {
						    var  value = attValue[1].replace('}','');
						    value = value.replace(']','');
						   
						    pdncontext +="<td>"+value+"</td>";
						   }
					   if(attValue.length > 1 && attValue[0].trim() == "pdnContextId")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					    pdncontext +="<td>"+value+"</td>";
					   }
					   if(attValue.length > 1 && attValue[0].trim() == "pdnType")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					    pdncontext +="<td>"+value+"</td>";
					   }
					   if(attValue.length > 1 && attValue[0].trim() == "refLteQOfServName")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					    pdncontext +="<td>"+value+"</td>";
					   }
					   
					  /* if(attValue.length > 1 && attValue[0].trim() == "pdnGwIPv4")
					   {
					    var  value = attValue[1].replace('}','');
					    value = value.replace(']','');
					   
					    pdncontext +="<td>"+value+"</td>";
					   }
						  */
						   

				}
			}
		
		}
	}
	subinnss += "</tbody></table>";
	epsdata += "</tbody></table>";
	non3gppdata += "</tbody></table></div>";
	gprsdata += "</tbody></table>";
	pdncontext += "</tbody></table>";
	
	$('#usdContentHeading').text("HLR Info");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subinnss);
	
	//extraInfo 
	$('#extraInfoBody').empty();
	$('#extraInfoHeading').text("EPS Data");
	$('#extraInfoBody').html(epsdata);
	$('#extraInfo').show();
	
	//extraInfo3
	$('#extraInfoBody3').empty();
	$('#extraInfoHeading3').text("Non GPRS Data");
	$('#extraInfoBody3').html(non3gppdata);
	$('#extraInfo3').show();
	
	//extraInfo4
	$('#extraInfoBody4').empty();
	$('#extraInfoHeading4').text("GPRS Data(APNS)");
	$('#extraInfoBody4').html(gprsdata);
	$('#extraInfoimpi').show();
	
	//extraInfo5
	$('#extraInfoBody5').empty();
	$('#extraInfoHeading5').text("LTE APNs");
	$('#extraInfoBody5').html(pdncontext);
	$('#extraInfo5').show();
	
	$('#subinnss').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});
	
	$('#epsdata').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});
	
	$('#non3gppdata').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
        "jQueryUI" : true,
	});
	
	$('#gprsdata').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
       "jQueryUI" : true,
	});
	
	$('#pdncontext').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
        "jQueryUI" : true,
	});
	$("div.toolbar").html('<b>HLR Info</b>');

	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function sendEIRDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateEIRUI(result);
		}
	});
}

function sendEIRIMSIUnlockDataRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateEIRIMSIUnlockUI(result);
		}
	});
}

function generateEIRIMSIUnlockUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}

		$('#eirImsiUnlockUID').val(usdUID);
		$("#searchType").val("UID");
		$('#eirUnlockImeiHistory').val(result.imeiHistory);
		$('#enableLockImsiImei').show();
	
		$('#search').hide();
		$('#searchType').hide();
		$('#searchText').hide();
		$('#searchLbl').hide();
		$('#searchLbl').hide();
		$("#resultPanel").css('visibility','hidden');
		dataProcessEnd();
}

function generateEIRUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}


	
	var eirTabHTML = "<table data-toggle='table' class= 'display' data-height='180' id='eirprofileTable'><thead><tr><th>Attribute</th><th>  Value</th>";
	eirTabHTML += "</tr></thead>  <tbody>";
	
	for ( var key in result) {
	
		if (key == 'imeiHistory' && result.hasOwnProperty('imeiHistory')) {
			eirTabHTML += "<tr><td>" + key + "</td>";
			var imeiVal=" ";
			for ( var n in result.imeiHistory) {
				var imei = result.imeiHistory[n].trim();
				if(imei!="")
				{
				imeiVal=imeiVal+" "+imei
				}
			}
			
			eirTabHTML += "<td >" + imeiVal
			+ "</td>";
		}else if (key == 'eirFastBlackList' && result.hasOwnProperty('eirFastBlackList')) {
			eirTabHTML += "<tr><td>" + key + "</td>";
			if(result.eirFastBlackList!=null)
			{
			eirTabHTML += "<td >" +  result.eirFastBlackList
			+ "</td>";
			}else
				{
				eirTabHTML += "<td >  -</td>";
				}
		} else if (key == 'eirLockImsiImei' && result.hasOwnProperty('eirLockImsiImei')) {
			eirTabHTML += "<tr><td>" + key + "</td>";
			if(result.eirLockImsiImei!=null)
			{
			eirTabHTML += "<td >" +  result.eirLockImsiImei
			+ "</td>";
			}else
				{
				eirTabHTML += "<td >  -</td>";
				}
			
		}
		
		else if (key == 'eirDualImsi' && result.hasOwnProperty('eirDualImsi')) {
			eirTabHTML += "<tr><td>" + key + "</td>";
			if(result.eirDualImsi!=null)
			{
			eirTabHTML += "<td >" +  result.eirDualImsi
			+ "</td>";
			}else
				{
				eirTabHTML += "<td >  -</td>";
				}
			
			
 		}
		
	}
	eirTabHTML += "</tbody></table>";
	$('#usdContentHeading').text("IMEI History");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(eirTabHTML);

	$('#eirprofileTable').dataTable({
		"scrollY" : "150px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"bSort": false,
		"jQueryUI" : true,
	});

	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function getUIDFromDUS(searchUrl, data, selectedTab) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			if (Constants.USD_SELECTED_TAB == Constants.EIR_ADD) {
				if (result.uid != null) {
					usdUID = result.uid;
					 searchCretria["uid"]=usdUID;
					 searchCretria["tac"]=$("#inputTAC").val();
					 searchCretria["imei"]= $("#inputIMEI").val();
					 searchCretria["imsi"]= $("#searchText").val();
					 searchCretria["lockImsiImei"]= $("#inputLockImsiImei").val();
					 searchCretria["fastBlackList"]= $("#inputFastBlackList").val();
					 searchCretria["dualImsi"]= $("#inputDualImsi").val();
					
					sendAddEIRDataRequest(searchCretria);
				} else {
					$('#usdContentBody').empty();
					$('#usdContentBody').html(
							"uid is not available, unable to process!!!");
					$("#resultPanel").css('visibility','visible');dataProcessEnd();
				}
			} else if(Constants.USD_SELECTED_TAB == Constants.EIR_UNLOCK)
			{
			var searchUrl = Constants.CONTEXT_PATH + Constants.USD + Constants.EIR;
			
			if (result.uid != null) {
				usdUID = result.uid;
				 searchCretria["uid"]=usdUID;
				 sendEIRIMSIUnlockDataRequest(searchUrl, searchCretria);
			}else
				{
				$('#usdContentBody').empty();
				$('#usdContentBody').html(
				"uid is not available, unable to process!!!");
				$("#resultPanel").css('visibility','visible');dataProcessEnd();
				}
			}else {
				if (result.uid != null) {
					usdUID = result.uid;
					var data = {
						"customerId" : null,
						"msisdn" : null,
						"imsi" : null,
						"uid" : usdUID,
						"ban" : null
					};
					renderTabDetails(data);
				} else {
					noDataFoundGrid();
				}
			}
		}
	});
}

function renderTabDetails(searchCretria) {
	var searchUrl = Constants.CONTEXT_PATH + Constants.USD
			+ Constants.USD_SELECTED_TAB;
	switch (Constants.USD_SELECTED_TAB) {
	case Constants.UMA:
		sendUMADataRequest(searchUrl, searchCretria);
		break;
	case Constants.HSS:
		sendHSSDataRequest(searchUrl, searchCretria);
		break;
	case Constants.HLR:
		sendHLRDataRequest(searchUrl, searchCretria);
		break;
	case Constants.EIR_SEARCH:
	case Constants.EIR:
		sendEIRDataRequest(searchUrl, searchCretria);
		break;
	default:
		break;
	}
}

function sendAddEIRDataRequest(data) {
	var searchUrl = Constants.CONTEXT_PATH + Constants.USD
			+ Constants.USD_SELECTED_TAB;
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			if (Constants.USD_SELECTED_TAB == Constants.EIR_ADD) {
				generateAddEIRUI(result);
			}
			if (Constants.USD_SELECTED_TAB == Constants.EIR_UNLOCK) {
				//generateUnlockIMEIIMSIUI(result);
				generateUnlockIMSIUI(result);
			}
			if (Constants.USD_SELECTED_TAB == Constants.EIR_SEARCH_TAC) {
				generateSearchTacUI(result);
			}
		}
	});
}

function generateAddEIRUI(result) {
	
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	
	var subProfileHtml = "";
	subProfileHtml += "<table  data-toggle='table' class= 'display' data-height='180' id='addEirImeiResponse'><thead><tr><th>Attributs</th><th>Value</th>";
	subProfileHtml += "</tr></thead><tbody>";
	for ( var key in result) {
		if (key == 'attributes' && result.hasOwnProperty('attributes')) {
			for ( var attribute in result.attributes) {
				//var imei = result.attributes[0].split('|');
				subProfileHtml += "<tr><td>" + attribute + "</td>";
				subProfileHtml += "<td>" + result.attributes[attribute] + "</td>";
			}
		}
	}
	subProfileHtml += "</table>";
	$('#usdContentHeading').text(
			Constants.USD_SELECTED_TAB.toUpperCase() + " Result");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	
	$('#addEirImeiResponse').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
        "jQueryUI" : true,
	});
	$('#searchText').val("");
	$('#inputIMEI').val("");
	$("#inputFastBlackList").val("FALSE");
	$("#inputLockImsiImei").val("FALSE");
	$("#inputDualImsi").val("FALSE");
	
	
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function generateUnlockIMSIUI(result) {
	
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	
	var subProfileHtml = "<table data-toggle='table' class= 'display' data-height='180' id='unlockimsi'><thead><tr><td>"
			+ usdUID + " " + result.attributes[0] + "</td></tr></table>";
	$('#usdContentHeading').text(
			Constants.USD_SELECTED_TAB.toUpperCase() + " Result");
	// Clean Div
	$('#usdContentBody').empty();
	
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	dataProcessEnd();
	$("#resultPanel").css('visibility','visible');
}

function getTacData(data) {
	var searchUrl = Constants.CONTEXT_PATH + Constants.USD
			+ Constants.USD_SELECTED_TAB;
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateSearchTacUI(result);
		}
	});
}

function generateSearchTacUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
		
	var subProfileHtml = "";
	subProfileHtml += "<table data-toggle='table' class= 'display' data-height='180' id='tacdatatable'><thead><tr><th>Attributes</th><th>Value</th>";
	subProfileHtml += "</tr></thead><tbody>";
	for ( var attributes in result.attributes) {
		subProfileHtml += "<tr><td>" + attributes + "</td>";
		subProfileHtml += "<td>" + result.attributes[attributes]
				+ "</td>";
	}
	subProfileHtml += "</tbody></table>";
	$('#usdContentHeading').text(
			Constants.USD_SELECTED_TAB.toUpperCase() + " Result");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	
	$('#tacdatatable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
        "jQueryUI" : true,
	});
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}

function addTacData(searchCretria) {
	dataProcessStart();
	var searchUrl = Constants.CONTEXT_PATH + Constants.USD
			+ Constants.USD_SELECTED_TAB;

	if (!vaidateTacInput()) {
		dataProcessEnd();
		return;
	}
	var data = fillInput();
	
	var jsonData = JSON.stringify(data);

	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			generateAddTacUI(result);
		}
	});
}

function vaidateTacInput() {

	var deviceManuVal = $("#deviceManufacturer").val();
	var deviceModval = $("#deviceModel").val();

	if ($.trim(deviceManuVal) == '') {
		alert("Mandatory Field is Not Empty !.");
		$("#deviceManufacturer").focus();
		return false;
	} else if ($.trim(deviceModval) == '') {
		alert("Mandatory Field is Not Empty !.");
		$("#deviceModel").focus();
		return false;
	}

	return true;
}

function fillInput() {

	var deviceManuVal = $("#deviceManufacturer").val();
	var deviceModval = $("#deviceModel").val();
	var brandVal = $("#deviceBands").val();
	var deviceScreenVal = $("#deviceScreenResolution").val();
	var deviceTechnVal = $("#deviceTechnologies").val();

	if ($.trim(brandVal) == '') {
		brandVal = "900/1800/1900";
	}
	if ($.trim(deviceScreenVal) == '') {
		deviceScreenVal = "500x400";
	}

	if ($.trim(deviceTechnVal) == '') {
		deviceTechnVal = "TAC2=" + $.trim($("#searchText").val())
				+ ",Capability=MMS-access=IP,SUPL=2,remunl=no,mmsblank=yes";
	}

	var data = {
		"imsi" : null,
		"uid" : usdUID,
		"tac" : searchCretria.tac,
		"imei" : searchCretria.imei,
		"deviceManufacturer" : deviceManuVal,
		"deviceModel" : deviceModval,
		"deviceBands" : brandVal,
		"deviceScreenResolution" : deviceScreenVal,
		"deviceTechnologies" : deviceTechnVal,
		"enableMMSOption" : null
	};

	return data;
}

function generateAddTacUI(result) {
	
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	var subProfileHtml = "";
	subProfileHtml += "<table data-toggle='table' class= 'display' data-height='180' id='addTacResult'><thead><tr><th>Attributes</th><th>Value</th>";
	subProfileHtml += "</tr></thead><tbody>";
	for ( var attributes in result.attributes) {
		subProfileHtml += "<tr><td>" + attributes + "</td>";
		subProfileHtml += "<td>" + result.attributes[attributes]
				+ "</td>";
	}
	subProfileHtml += "</table>";
	$('#usdContentHeading').text(
			Constants.USD_SELECTED_TAB.toUpperCase() + " Result");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	
	$('#addTacResult').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,
		"jQueryUI" : true,
	});
	$("#resultPanel").css('visibility','visible');
	dataProcessEnd();
}

function enableTacData(searchCretria) {
	var searchUrl = Constants.CONTEXT_PATH + Constants.USD
			+ Constants.USD_SELECTED_TAB;
	var enableMMSOptionValue = $("#enableMMSOption").find('option:selected')
			.val();
	var flag="";
	
	if($('#enableMMSFlag').val()=="false")
	{
		flag="false";
	}else
	{
		flag="true";
	}
	
	var data = {
		"imsi" : null,
		"uid" : null,
		"tac" : null,
		"imei" : searchCretria.tac,
		"mmsUpdate" : flag,
		"deviceManufacturer" : null,
		"deviceModel" : null,
		"deviceBands" : null,
		"deviceScreenResolution" : null,
		"deviceTechnologies" : null,
		"enableMMSOption" : enableMMSOptionValue
	};
	
	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",
		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			if(flag=="true")
			{
			enableTacDataUI(result);
			}else
			{
			generateSearchEnableMMSUI(result);
			}
		}
	});
}

function generateSearchEnableMMSUI(result) {
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	$('#searchText').val(result.attributes.tac);	
	$('#deviceManufacturer').val(result.attributes.deviceManufacturer);
	$('#deviceModel').val(result.attributes.deviceModel);
	$('#deviceBands').val(result.attributes.deviceBands);
	$('#deviceScreenResolution').val(result.attributes.deviceScreenResolution);
	$('#deviceTechnologies').val(result.attributes.deviceTechnologies);
	$('#searchText').attr('readonly', true);
	$('#deviceManufacturer').attr('readonly', true);
	$('#deviceModel').attr('readonly', true);
	$('#deviceBands').attr('readonly', true);
	$('#deviceScreenResolution').attr('readonly', true);
	document.getElementById("search").innerText = 'Update';
	$('#enableMMSFlag').val("true");
	
	$('#addTacInput').show();
	$("#resultPanel").css('visibility','hidden');dataProcessEnd();
}

function enableTacDataUI(result) {
	
	if (result.applicationError) {
		displayInternalErroGrid(result.applicationError.errorCode);
		return;
	}
	
	var subProfileHtml = "<table class='table table-striped' style='table-layout: fixed;'><tr><td>"
			+ searchCretria.tac
			+ " MMS Blank Status: "
			+ result.attributes[0]
			+ "</td></tr></table>";
	$('#usdContentHeading').text(
			Constants.USD_SELECTED_TAB.toUpperCase() + " Result");
	// Clean Div
	$('#usdContentBody').empty();
	// Insert Table into DIV
	$('#usdContentBody').html(subProfileHtml);
	$("#resultPanel").css('visibility','visible');dataProcessEnd();
}


$(function() {
    var availableTags = [
		"TRUE",
		"FALSE"
    ];
    $( "#inputLockImsiImei" ).autocomplete({
      source: availableTags
    });
  });


$(function() {
    var availableTags = [
      "TRUE",
      "FALSE"
    ];
    $( "#inputFastBlackList" ).autocomplete({
      source: availableTags
    });
  });



$(function() {
    var availableTags = [
      "TRUE",
      "FALSE"
    ];
    $( "#inputDualImsi" ).autocomplete({
      source: availableTags
    });
  });