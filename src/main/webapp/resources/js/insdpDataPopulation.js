var resultObject = null;
resultObject = {
	acctab: null,
	balDattab: null,
	serCaltab: null,
	createsubscrtab: null,
	updateBaltab: null,
	updateAccumtab: null,
	updateSubSegtab: null,
	activeSubtab: null,
	deleteSubtab: null,
}
$(document).ready(function() {
	$("#searchText").keypress(function(event) {
		if (event.keyCode == 13) {
			getedatData("frm");
		}
	});
});

function getedatData(frm) {
	var activeTab = $("#insdpTabs li.activeUSD");
	activeTab = activeTab[0].id;
	if (activeTab == "acc") {
		if (validateInput($("#searchText").val(), "searchText")) {
			getinsdpAccData();
		}
	}
	if (activeTab == "balDat") {
		if (validateInput($("#searchText").val(), "searchText")) {
			getinsdpBalData();
		}
	}
	if (activeTab == "getAccumulators") {
		if (validateInput($("#getAccumulatorsMsisdn").val(), "getAccumulatorsMsisdn")) {
			getAccumulatorsData();
		}
	}
	
	if (activeTab == "getOffer") {
		if (validateInput($("#getOfferMsisdn").val(), "getOfferMsisdn")) {
			getOfferData();
		}
	}
	
	if (activeTab == "updateserCal") {
		if (validateInput($("#servicemsisdn").val(), "servicemsisdn")) {
			updateServiceClass();
		}
	}
	if (activeTab == "updateBal") {
		if (validateInput($("#UpdateBalanceMsisdn").val(), "UpdateBalanceMsisdn")  && validateDateCreateSubscriberFields() && validateDate() && validateDateForFeeExpiry()) {
			updateBalanceAndDate();
		}
	}
	// need to be write new defination
	if (activeTab == "createsubscr") {
		if (validateInput($("#newmsisdn").val(), "newmsisdn") && validateDateCreateSubscriberFields()) {
			createSubscriber();
		}
	}
	if (activeTab == "activeSub") {
		if (validateInput($("#activeSubscriberMsisdn").val(), "activeSubscriberMsisdn") && validateDateActiveFields() && validateDateForActiveSub() && validateDateForFeeExpiryActiveSub()) {
			activeSubscriber();
		}
	}
	if (activeTab == "deleteSub") {
		if (validateInput($("#deletedmsisdn").val(), "deletedmsisdn")) {
			deleteSubscriber();
		}
	}
	if (activeTab == "updateSubSegment") {
		if (validateInput($("#updatedmsisdn").val(), "updatedmsisdn")) {
			updateSubscriberSeg();
		}
	}
	if (activeTab == "updateAccum") {
		if (validateInput($("#Accmsisdn").val(), "Accmsisdn") && validateInputFields()) {
			UpdateAccumulators();
		}
	}
	if (activeTab == "updateDABal") {
		if (validateInput($("#UpdateDABalmsisdn").val(), "UpdateDABalmsisdn") && validateInputForUpdateDABalFields()) {
			UpdateDedicatedDABal();
		}
	}
	if (activeTab == "updateOffer") {
		if (validateInput($("#offerMsisdn").val(), "offerMsisdn") && validateInputForUpdateOfferFields()) {
			updateOffer();
		}
	}
}

function validateServiceOffering() {
	var rowFlag = $("#rowFlag").val();
	if (rowFlag != "0") {
		return true;
	} else {
		alert("Please enter atleast one row of service offering.")
		return false;
	}
}

function validateCreateSubscriber() {
	var flag = "false";
	var rowCount = $('#sdpContentBody >tbody >tr').length;
	var serviceName = "";
	var activeFlag = "";
	if (rowCount > 1) {
		for (var i = 1; i < rowCount; i++) {
			serviceName = document.getElementById("value" + i).value;
			activeFlag = document.getElementById("ActiveFlag" + i).value;
			if (serviceName != "" && activeFlag != "") {
				flag = "true";
			} else {
				flag = "false";
				break;
			}
		}
	} else {
		flag = "false";
	}
	if (flag == "false") {
		alert("Please check  new Service Offerings")
		return false;
	} else {
		return true;
	}
}

function getinsdpAccData() {
	dataProcessStart();
	var searchBox = $('#searchBox :selected').val();
	//var searchText = '1'+$("#searchText").val();
	var searchText = $("#searchText").val();
	var data = null;
	if (searchBox == "MSISDN") {
		data = {
			"customerId": null,
			"msisdn": searchText,
			"imsi": null
		};
	}
	var searchUrl = null;
	var testdata = JSON.stringify(data);
	searchUrl = '/edat/insdp/AccountDetails';
	$.ajax({
		url: searchUrl,
		data: testdata,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(result) {
			$('#insdp').css('display', 'inline');
			resultObject.acctab = result;
			generateMessagingDatatableForAcc(result);
		}
	});
}

function clearTextForAcc() {
	$("#datepicker").val("");
	$("#searchText").val("");
	$("#servicemsisdn").val("");
	$("#serviceClass").val("");
	$("#expDatedatepicker").val("");
	$("#feeExpDatedatepicker").val("");
	$("#AmountRelativeForUpdateBalance").val("");
	$("#newmsisdn").val("");
	$("#newServiceClass").val("");
	$("#newLanguage").val("");
	$("#actSupexpDatedatepicker").val("");
	$("#actSerfeeExpDatedatepicker").val("");
	$("#activeSubscriberMsisdn").val("");
	$("#actOrignTimedatepicker").val("");
	$("#adjustmentAmountRelative").val("");
	$("#deleteOrgdatepicker").val("");
	$("#deletedmsisdn").val("");
	$("#updatedmsisdn").val("");
	$("#Accmsisdn").val("");
	$("#AccId").val("");
	$("#AccVal").val("");
	$("#UpdateBalanceMsisdn").val("");
}

function getinsdpBalData() {
		var searchBox = $('#searchBox :selected').val();
		dataProcessStart();
		var searchText = $("#searchText").val();
		var data = null;
		if (searchBox == "MSISDN") {
			data = {
				"customerId": null,
				//"msisdn" : '1'+searchText,
				"msisdn": searchText,
				"imsi": null
			};
		}
		var searchUrl = null;
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/BalanceAndDate';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				displayErroGrid();
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.balDattab = result;
				generateMessagingDatatable(result);
			}
		});
	}
	//for Get Accumulators
function getAccumulatorsData() {
	dataProcessStart();
	var searchText = $("#getAccumulatorsMsisdn").val();
	var data = null;
	data = {
		"customerId": null,
		//"msisdn" : '1'+searchText,
		"msisdn": searchText,
		"imsi": null
	};
	var searchUrl = null;
	var testdata = JSON.stringify(data);
	searchUrl = '/edat/insdp/getAccumulators';
	$.ajax({
		url: searchUrl,
		data: testdata,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error: function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success: function(result) {
			$('#insdp').css('display', 'inline');
			//resultObject.balDattab = result;
			//generateMessagingDatatable(result);
			generateAccumulatorDataTable(result);
		}
	});
}




//for Get Offer
function getOfferData() {
	dataProcessStart();
	var searchText = $("#getOfferMsisdn").val();
	var data = null;
	data = {
		"customerId": null,
		//"msisdn" : '1'+searchText,
		"msisdn": searchText,
		"imsi": null
	};
	var searchUrl = null;
	var testdata = JSON.stringify(data);
	searchUrl = '/edat/insdp/getOffer';
	$.ajax({
		url: searchUrl,
		data: testdata,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error: function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success: function(result) {
			$('#insdp').css('display', 'inline');
			//resultObject.balDattab = result;
			//generateMessagingDatatable(result);
			generateOfferDataTable(result);
		}
	});
}

function generateAccumulatorDataTable(result) {
		var value;
		if (result.applicationError) {
			displayInternalErroGrid(result.applicationError.errorCode);
			return;
		}
		/*var gprsdata = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable'><thead><tr><th>Accumulator ID</th><th>Accumulator Start Date</th><th>Accumulator Value</th>";
		gprsdata += "</tr></thead>  <tbody>";*/
		
		var myTable = null;
		myTable = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable1'><thead><tr><th>AccumulatorID</th>";
		myTable += "<th> AccumulatorValue</th>";
	
	
		myTable += "</tr> </thead>  <tbody>";
		var i = 0;
	
			
		
		for (var key in result.attributes) {
			//gprsdata += "<tr>";
			
				var myArray = new Array();
				var accumulatorIdArray = new Array();
				var accumulatorDateArray = new Array();
				var accumulatorVal;
				var idVal;
				var dateVal;
				myArray = result.attributes.accumulatorIDList.split(",");
				accumulatorIdArray = result.attributes.accumulatorValList.split(",");
				accumulatorDateArray = result.attributes.accumulatorDateList.split(",");
				if(myArray.length>1)
				{
				if(key=="accumulatorIDList")
				{
				for (var i = 0; i < myArray.length; i++) {
					myTable += "<tr>";
					var length = myArray.length;
					var accumulatorIdArrayLength = accumulatorIdArray.length;
					var accumulatorDateArrayLength = accumulatorDateArray.length;
				
				if (i == 0) {
						accumulatorVal = myArray[i].replace('[', '');
						myTable += "<td>" + accumulatorVal + "</td>";
					} else if (i == length - 1) {
						accumulatorVal = myArray[i].replace(']', '');
						myTable += "<td>" + accumulatorVal + "</td>";
					} else {
						accumulatorVal = myArray[i].replace(' ', '');
						myTable += "<td>" + accumulatorVal + "</td>";
					}
					
					
					
					if (i == 0) {
						idVal = accumulatorIdArray[i].replace('[', '');
						myTable += "<td>" + idVal + "</td>";
					} else if (i == accumulatorIdArrayLength - 1) {
						idVal = accumulatorIdArray[i].replace(']', '');
						myTable += "<td>" + idVal + "</td>";
					} else {
						idVal = accumulatorIdArray[i].replace(' ', '');
						myTable += "<td>" + idVal + "</td>";
					}
					
				
					
					
				}	
				
				myTable += "</tr>";
				}
		}
			}
			
				
				
		
		myTable += "</tbody></table>";
		document.getElementById('insdp').innerHTML = myTable;
		$('#insdpTable1').dataTable({
			"scrollY": "300px",
			"toolbar": false,
			"scrollCollapse": true,
			"paging": false,
			"columns": [null, null],
			"jQueryUI": true,
		});
		dataProcessEnd();
	}






function generateOfferDataTable(result) {
		var value;
		if (result.applicationError) {
			displayInternalErroGrid(result.applicationError.errorCode);
			return;
		}
		/*var gprsdata = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable'><thead><tr><th>Accumulator ID</th><th>Accumulator Start Date</th><th>Accumulator Value</th>";
		gprsdata += "</tr></thead>  <tbody>";*/
		
		var myTable = null;
		myTable = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable1'><thead><tr><th>ProductId</th><th>OfferId</th><th>OfferType</th>";
	
		myTable += "<th> OfferState</th>";
		myTable += "<th> StartDate</th>";
		myTable += "<th> ExpiryDate</th>";
	
	
		myTable += "</tr> </thead>  <tbody>";
		var i = 0;
	
			
		
		for (var key in result.attributes) {
			//gprsdata += "<tr>";
			
				
				var expiryDateTimeArray = new Array();
				var offerIDArray = new Array();
				var offerStateArray = new Array();
				var offerTypeArray = new Array();
				var startDateTimeArray = new Array();
				var productIdArray = new Array();
				
				var expiryDateVal;
				var offerIdVal;
				var offerStateVal;
				var offerTypeVal;
				var startDateTimeVal;
				var productIdVal;
				expiryDateTimeArray = result.attributes.expiryDateTimeList.split(",");
				offerIDArray = result.attributes.offerIDList.split(",");
				offerStateArray = result.attributes.offerStateList.split(",");
				
				offerTypeArray = result.attributes.offerTypeList.split(",");
				startDateTimeArray = result.attributes.startDateTimeList.split(",");
				productIdArray = result.attributes.productIdList.split(",");
				if(expiryDateTimeArray.length>1)
				{
				if(key=="expiryDateTimeList")
				{
				for (var i = 0; i < expiryDateTimeArray.length; i++) {
					myTable += "<tr>";
					var length = expiryDateTimeArray.length;
					var offerIDArrayLength = offerIDArray.length;
					var offerStateArrayLength = offerStateArray.length;
					
					var offerTypeArrayLength = offerTypeArray.length;
					var startDateTimeArrayLength = startDateTimeArray.length;
					var productIdArrayLength = productIdArray.length;
				
					//
					if (i == 0) {
						productIdVal = productIdArray[i].replace('[', '');
						myTable += "<td>" + productIdVal + "</td>";
					} else if (i == productIdArrayLength - 1) {
						productIdVal = productIdArray[i].replace(']', '');
						myTable += "<td>" + productIdVal + "</td>";
					} else {
						productIdVal = productIdArray[i].replace(' ', '');
						myTable += "<td>" + productIdVal + "</td>";
					}
					
					//
					
					if (i == 0) {
						offerIdVal = offerIDArray[i].replace('[', '');
						myTable += "<td>" + offerIdVal + "</td>";
					} else if (i == offerIDArrayLength - 1) {
						offerIdVal = offerIDArray[i].replace(']', '');
						myTable += "<td>" + offerIdVal + "</td>";
					} else {
						offerIdVal = offerIDArray[i].replace(' ', '');
						myTable += "<td>" + offerIdVal + "</td>";
					}
					
					
					
					
					//
				
					if (i == 0) {
						offerTypeVal = offerTypeArray[i].replace('[', '');
						myTable += "<td>" + offerTypeVal + "</td>";
					} else if (i == offerTypeArrayLength - 1) {
						offerTypeVal = offerTypeArray[i].replace(']', '');
						myTable += "<td>" + offerTypeVal + "</td>";
					} else {
						offerTypeVal = offerTypeArray[i].replace(' ', '');
						myTable += "<td>" + offerTypeVal + "</td>";
					}
					
					
					//				
						
					if (i == 0) {
						offerStateVal = offerStateArray[i].replace('[', '');
						myTable += "<td>" + offerStateVal + "</td>";
					} else if (i == offerStateArrayLength - 1) {
						offerStateVal = offerStateArray[i].replace(']', '');
						myTable += "<td>" + offerStateVal + "</td>";
					} else {
						offerStateVal = offerStateArray[i].replace(' ', '');
						myTable += "<td>" + offerStateVal + "</td>";
					}
					
					//
					
					if (i == 0) {
						startDateTimeVal = startDateTimeArray[i].replace('[', '');
						myTable += "<td>" + startDateTimeVal + "</td>";
					} else if (i == startDateTimeArrayLength - 1) {
						startDateTimeVal = startDateTimeArray[i].replace(']', '');
						myTable += "<td>" + startDateTimeVal + "</td>";
					} else {
						startDateTimeVal = startDateTimeArray[i].replace(' ', '');
						myTable += "<td>" + startDateTimeVal + "</td>";
					}
					
					
					//
					if (i == 0) {
					expiryDateVal = expiryDateTimeArray[i].replace('[', '');
						myTable += "<td>" + expiryDateVal + "</td>";
					} else if (i == length - 1) {
						expiryDateVal = expiryDateTimeArray[i].replace(']', '');
						myTable += "<td>" + expiryDateVal + "</td>";
					} else {
						expiryDateVal = expiryDateTimeArray[i].replace(' ', '');
						myTable += "<td>" + expiryDateVal + "</td>";
					}
					
					
					
				
					
					
					
					
				
					
				
					
					myTable += "</tr>";
				}	
				
				
				}
		}
			}
			
				
				
		
		myTable += "</tbody></table>";
		document.getElementById('insdp').innerHTML = "<table data-toggle='table' class= 'display' data-height='500' id='insdpTable1'><thead><tr><th>ProductId</th><th>OfferId</th><th>OfferType</th><th> OfferState</th><th> StartDate</th><th> ExpiryDate</th></tr> </thead>  <tbody><tr><td>1111</td><td>50001</td><td>2</td><td>0</td><td>2015-01-28</td><td>9999-12-30</td></tr><tr><td>1</td><td>60001</td><td>2</td><td>0</td><td>2015-01-28</td><td>9999-12-30</td></tr><tr><td>2</td><td>70001</td><td>2</td><td>0</td><td>2015-01-28</td><td>9999-12-30</td></tr><tr><td>3</td><td>80001</td><td>2</td><td>0</td><td>2015-01-28</td><td>9999-12-30</td></tr><tr><td>4</td><td>90001</td><td>2</td><td> 0</td><td>2015-01-28</td><td>9999-12-30</td></tr>";
		$('#insdpTable1').dataTable({
			"scrollY": "300px",
			"toolbar": false,
			"scrollCollapse": true,
			"paging": false,
			"columns": [null, null,null,null,null,null],
			"jQueryUI": true,
		});
		dataProcessEnd();
	}
	// for Update service Class
function updateServiceClass(frm) {
		dataProcessStart();
		var originTime = $("#datepicker").val();
		var msisdn = $("#servicemsisdn").val();
		var serviceClass = $("#serviceClass").val();
		data = {
			"customerId": null,
			//"msisdn" : '1'+msisdn,
			"msisdn": msisdn,
			"imsi": null,
			"serviceClassNew": serviceClass,
			"originTimeStamp": originTime
		};
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/UpdateService';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				displayErroGrid();
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.serCaltab = result;
				generateMessagingDatatable(result);
			}
		});
	}
	//for Update service Class
function updateBalanceAndDate(frm) {
		dataProcessStart();
		var superExpDate = $("#expDatedatepicker").val();
		var feeExpDate = $("#feeExpDatedatepicker").val();
		var adjAmountRelative = $("#AmountRelativeForUpdateBalance").val();
		var msisdn = $("#UpdateBalanceMsisdn").val();
		data = {
			"customerId": null,
			//"msisdn" : '1'+msisdn,
			"msisdn": msisdn,
			"imsi": null,
			"serviceFeeExpiryDate": feeExpDate,
			"supervisionExpiryDate": superExpDate,
			"adjustmentAmountRelative": adjAmountRelative
		};
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/UpdateBalanceAndDate';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				displayErroGrid();
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.updateBaltab = result;
				generateMessagingDatatable(result);
			}
		});
	}
	//for Activate Subscriber 
function activeSubscriber(frm) {
		dataProcessStart();
		var superExpDate = $("#actSupexpDatedatepicker").val();
		var feeExpDate = $("#actSerfeeExpDatedatepicker").val();
		var msisdn = $("#activeSubscriberMsisdn").val();
		var actOrignTimedatepicker = $("#actOrignTimedatepicker").val();
		var adjAmountRelative = $("#adjustmentAmountRelative").val();
		data = {
			"customerId": null,
			//"msisdn" : '1'+msisdn,
			"msisdn": msisdn,
			"imsi": null,
			"serviceFeeExpiryDate": feeExpDate,
			"supervisionExpiryDate": superExpDate,
			"originTimeStamp": actOrignTimedatepicker,
			"adjustmentAmountRelative": adjAmountRelative
		};
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/ActivateSubscriber';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				displayErroGrid();
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.activeSubtab = result;
				generateMessagingDatatable(result);
			}
		});
	}
	//for Create/Install  New Subscriber
function createSubscriber(frm) {
		dataProcessStart();
		var msisdn = $("#newmsisdn").val();
		var serviceClass = $("#newServiceClass").val();
		var newLanguage = $("#newLanguage").val();
		var ussdEndOfCallNotificationID = $("#ussdEndOfCallNotificationID").val();
		if (ussdEndOfCallNotificationID == '') {
			ussdEndOfCallNotificationID = 6;
		}
		var data = {
			"customerId": null,
			//"msisdn" : '1'+msisdn,
			"msisdn": msisdn,
			"imsi": null,
			//"originTimeStamp" : Orgdatepicker,
			"languageIDNew": newLanguage,
			"serviceClassNew": serviceClass,
			"ussdEndOfCallNotificationID": ussdEndOfCallNotificationID,
			"serviceOffering": []
		};
		var i = 0;
		$('#sdpContentBody tr').each(function() {
			i++;
			var atrValue, attrFlag;
			if ($("#value" + i).val() != undefined) {
				atrValue = $("#value" + i).val();
				attrFlag = $("#ActiveFlag" + i).find('option:selected').val();
				console.log(atrValue + " " + attrFlag);
			}
			if (atrValue == undefined) {} else {
				data.serviceOffering.push({
					"serviceOfferingName": "",
					"serviceOfferingID": atrValue,
					"serviceOfferingActiveFlag": attrFlag
				});
			}
		});
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/InstallSubscriber';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			error: function(xhr, ajaxOptions, thrownError) {
				displayErroGrid();
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.createsubscrtab = result;
				generateMessagingDatatable(result);
			}
		});
		$("#dynaTable").hide();
		$("#newmsisdn").val("");
		$("#newLanguage").val("");
		$("#newServiceClass").val("");
	}
	//for Update Subscriber Segment
function updateSubscriberSeg(frm) {
		dataProcessStart();
		var msisdn = $("#updatedmsisdn").val();
		data = {
			"customerId": null,
			//"msisdn" : '1'+msisdn,
			"msisdn": msisdn,
			"imsi": null,
			//"originTimeStamp" : Orgdatepicker,
			"serviceOffering": []
		};
		var i = 0;
		$('#sdpContentBody1 tr').each(function() {
			/* var atributes = $(this).find("td:nth-child(1)").html();    */
			i++;
			var atrValue, attrFlag;
			if ($("#value" + i).val() != undefined) {
				atrValue = $("#value" + i).val();
				attrFlag = $("#ActiveFlag2" + i).find('option:selected').val();
				console.log(atrValue + " " + attrFlag);
			}
			if (typeof atrValue === "undefined") {} else {
				data.serviceOffering.push({
					"serviceOfferingName": "",
					"serviceOfferingID": atrValue,
					"serviceOfferingActiveFlag": attrFlag
				});
			}
		});
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/UpdateSubscriber';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.updateSubSegtab = result;
				generateMessagingDatatable(result);
			}
		});
	}
	//for Update Accumulators
function UpdateAccumulators(frm) {
		dataProcessStart();
		var msisdn = $("#Accmsisdn").val();
		var accId = $("#AccId").val();
		var accVal = $("#AccVal").val();
		data = {
			//"customerId" : null,
			//"msisdn" : '1'+msisdn,
			"msisdn": msisdn,
			"accId": accId,
			"accVal": accVal
				//"imsi" : null,
				//"originTimeStamp" : null,
				//"accumulatorInfo": []
		};
		/*$('#sdpContentBody2 tr').each(function() {
			 var atributes = $(this).find("td:nth-child(1)").html();    
			 var atrValue = $(this).find("td:nth-child(2)").html();  
			 var attrFlag = $(this).find("td:nth-child(3)").html();  
			 data.serviceOffering.push({
				 "accumulatorValueAbsoluteName":atributes,
				 "accumulatorValueAbsolute":atrValue,
				 "accumulatorID":attrFlag
				
			 });
		 });*/
		var testdata = JSON.stringify(data);
		searchUrl = '/edat/insdp/UpdateAccumulators';
		$.ajax({
			url: searchUrl,
			data: testdata,
			type: "POST",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Accept", "application/json");
				xhr.setRequestHeader("Content-Type", "application/json");
			},
			success: function(result) {
				$('#insdp').css('display', 'inline');
				resultObject.updateAccumtab = result;
				generateMessagingDatatable(result);
			}
		});
	}
	//for Update Dedicated DABal
function updateOffer(frm) {
	dataProcessStart();
	var msisdn = $("#offerMsisdn").val();
	var offerId = $("#offerId").val();
	data = {
		//"msisdn" : '1'+msisdn,
		"msisdn": msisdn,
		"accId": offerId,
		"accVal": null
	};
	var testdata = JSON.stringify(data);
	searchUrl = '/edat/insdp/updateOffer';
	$.ajax({
		url: searchUrl,
		data: testdata,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(result) {
			$('#insdp').css('display', 'inline');
			resultObject.updateAccumtab = result;
			generateTableForDedicatedDABal(result);
			clearTextForUpdateOffer();
		}
	});
}

function clearTextForUpdateOffer() {
		$("#offerMsisdn").val("");
		$("#offerId").val("");
	}
	//for Update Dedicated DABal
function UpdateDedicatedDABal(frm) {
	dataProcessStart();
	var msisdn = $("#UpdateDABalmsisdn").val();
	var accId = $("#dedicatedAccountId").val();
	var accVal = $("#dedicatedAccountValue").val();
	data = {
		//"msisdn" : '1'+msisdn,
		"msisdn": msisdn,
		"accId": accId,
		"accVal": accVal
	};
	var testdata = JSON.stringify(data);
	searchUrl = '/edat/insdp/updateDedicatedDABal';
	$.ajax({
		url: searchUrl,
		data: testdata,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(result) {
			$('#insdp').css('display', 'inline');
			resultObject.updateAccumtab = result;
			generateTableForDedicatedDABal(result);
			clearTextForUpdateDABal();
		}
	});
}

function clearTextForUpdateDABal() {
		$("#UpdateDABalmsisdn").val("");
		$("#dedicatedAccountId").val("");
		$("#dedicatedAccountValue").val("");
	}
	//for Delete Subscriber
function deleteSubscriber(frm) {
	dataProcessStart();
	var Orgdatepicker = $("#deleteOrgdatepicker").val();
	var msisdn = $("#deletedmsisdn").val();
	data = {
		"customerId": null,
		//"msisdn" : '1'+msisdn,
		"msisdn": msisdn,
		"imsi": null,
		"originTimeStamp": Orgdatepicker
	};
	var testdata = JSON.stringify(data);
	searchUrl = '/edat/insdp/DeleteSubscriber';
	$.ajax({
		url: searchUrl,
		data: testdata,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success: function(result) {
			$('#insdp').css('display', 'inline');
			resultObject.deleteSubtab = result;
			generateMessagingDatatable(result);
		}
	});
}

function generateTableForDedicatedDABal(result) {
	if (result.applicationError) {
		html = "<table class='table'><tr><td style='font-size: 15px;'><b> Node not reachable !! </b></td>";
		html += "<td></td><td></td>";
		document.getElementById('insdp').innerHTML = html;
		dataProcessEnd();
		return;
	}
	var myTable = null;
	myTable = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable'><thead><tr><th>Attribute</th>";
	myTable += "<th> Value </th>";
	myTable += "</tr> </thead>  <tbody>";
	for (var key in result.attributes) {
		if (key == "dedicatedAccountValue1") {
			var str_array = result.attributes[key];
			var res = str_array.slice(1, str_array.length - 1);
			var splitted = res.split(", ");
			var str_array1 = result.attributes["dedicatedAccountID"];
			var res1 = str_array1.slice(1, str_array1.length - 1);
			var splitted1 = res1.split(", ");
			for (var i = 0; i < splitted.length; i++) {
				myTable += "<tr class='success'><td>DedicatedAccountID -  Balance" + "</td>";
				myTable += "<td>" + splitted1[i] + "-" + splitted[i] + "</td>";
			}
		} else {
			if (key != "dedicatedAccountID") {
				myTable += "<tr class='success'><td>" + key + "</td>";
				myTable += "<td>" + result.attributes[key] + "</td>";
				myTable += "</tr> ";
			}
		}
	}
	myTable += "</tbody></table>";
	document.getElementById('insdp').innerHTML = myTable;
	$('#insdpTable').dataTable({
		"scrollY": "300px",
		"toolbar": false,
		"scrollCollapse": true,
		"paging": false,
		"columns": [null, null],
		"jQueryUI": true,
	});
	dataProcessEnd();
}

function generateMessagingDatatable(result) {
	if (result.applicationError) {
		html = "<table class='table'><tr><td style='font-size: 15px;'><b> Node not reachable !! </b></td>";
		html += "<td></td><td></td>";
		document.getElementById('insdp').innerHTML = html;
		dataProcessEnd();
		return;
	}
	var myTable = null;
	myTable = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable'><thead><tr><th>Attribute</th>";
	myTable += "<th> Value </th>";
	myTable += "</tr> </thead>  <tbody>";
	for (var key in result.attributes) {
		if (key == "dedicatedAccountValue1") {
			var str_array = result.attributes[key];
			var res = str_array.slice(1, str_array.length - 1);
			var splitted = res.split(", ");
			var str_array1 = result.attributes["dedicatedAccountID"];
			var res1 = str_array1.slice(1, str_array1.length - 1);
			var splitted1 = res1.split(", ");
			for (var i = 0; i < splitted.length; i++) {
				myTable += "<tr class='success'><td>DedicatedAccountID -  Balance" + "</td>";
				myTable += "<td>" + splitted1[i] + "-" + splitted[i] + "</td>";
			}
		} else {
			if (key != "dedicatedAccountID") {
				myTable += "<tr class='success'><td>" + key + "</td>";
				myTable += "<td>" + result.attributes[key] + "</td>";
				myTable += "</tr> ";
			}
		}
	}
	myTable += "</tbody></table>";
	document.getElementById('insdp').innerHTML = myTable;
	$('#insdpTable').dataTable({
		"scrollY": "300px",
		"toolbar": false,
		"scrollCollapse": true,
		"paging": false,
		"columns": [null, null],
		"jQueryUI": true,
	});
	dataProcessEnd();
}

function generateMessagingDatatableForAcc(result) {
		if (result.applicationError) {
			html = "<table class='table'><tr><td style='font-size: 15px;'><b> Node not reachable !! </b></td>";
			html += "<td></td><td></td>";
			document.getElementById('insdp').innerHTML = html;
			dataProcessEnd();
			return;
		}
		var myTable = null;
		myTable = "<table data-toggle='table' class= 'display' data-height='180' id='insdpTable'><thead><tr><th>Attribute</th>";
		myTable += "<th> Value </th>";
		myTable += "</tr> </thead>  <tbody>";
		for (var key in result.attributes) {
			if (key != "serviceOfferingID") {
				if (key == "dedicatedAccountValue1") {
					var str_array = result.attributes[key];
					var res = str_array.slice(1, str_array.length - 1);
					var splitted = res.split(", ");
					var str_array1 = result.attributes["dedicatedAccountID"];
					var res1 = str_array1.slice(1, str_array1.length - 1);
					var splitted1 = res1.split(", ");
					for (var i = 0; i < splitted.length; i++) {
						myTable += "<tr class='success'><td>DedicatedAccountID -  Balance" + "</td>";
						myTable += "<td>" + splitted1[i] + "-" + splitted[i] + "</td>";
					}
				} else {
					if (key != "dedicatedAccountID") {
						myTable += "<tr class='success'><td>" + key + "</td>";
						myTable += "<td>" + result.attributes[key] + "</td>";
						myTable += "</tr> ";
					}
				}
			}
		}
		myTable += "</tbody></table>";
		document.getElementById('insdp').innerHTML = myTable;
		$('#insdpTable').dataTable({
			"scrollY": "300px",
			"toolbar": false,
			"scrollCollapse": true,
			"paging": false,
			"columns": [null, null],
			"jQueryUI": true,
		});
		dataProcessEnd();
	}
	//For clear div data
	/*function deletePoll(){
		$("#inSdp").parent().addClass("active");
		$("ul.sb #cntbAdd").parent().addClass("selected");
		
	}*/
$(document).ready(function() {
	$("#inSdp").parent().addClass("active");
	$("#insdpTabs li").click(function() {
		$(this).addClass("activeUSD").siblings().removeClass("activeUSD");
		switch (this.id) {
			case "acc":
				// clear div
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'block');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#searchBoxHeading').text("Accounts & Accumulators Search");
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				dataProcessEnd();
				$('#insdp').css('display', 'none');
				clearTextForAcc();
				document.title = "GetAccount";
				if (resultObject.acctab != null) generateMessagingDatatable(resultObject.acctab);
				break;
			case "balDat":
				// clear div
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'acc');
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'block');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#searchBoxHeading').text("Balance & Date");
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Get Balance and Date ";
				dataProcessEnd();
				if (resultObject.balDattab != null) generateMessagingDatatable(resultObject.balDattab);
				break;
			case "getAccumulators":
				// clear div
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'block');
				$('#updateServiceClass').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#searchBoxHeading').text("Accounts & Accumulators Search");
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				
				dataProcessEnd();
				$('#insdp').css('display', 'none');
				clearTextForAcc();
				document.title = "Get Accumulators";
				if (resultObject.acctab != null) generateMessagingDatatable(resultObject.acctab);
				break;
				
				
			case "getOffer":
				// clear div
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#getOffersearchBox').css('display', 'block');
				$('#updateServiceClass').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#searchBoxHeading').text("Accounts & Accumulators Search");
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				
				dataProcessEnd();
				$('#insdp').css('display', 'none');
				clearTextForAcc();
				document.title = "Get Offer";
				if (resultObject.acctab != null) generateMessagingDatatable(resultObject.acctab);
				break;
				
				
			case "updateserCal":
				// clear div
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#updateServiceClass').css('display', 'block');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				$('#insdp').css('display', 'none');
				clearTextForAcc();
				document.title = "Update Service Class";
				dataProcessEnd();
				if (resultObject.serCaltab != null) generateMessagingDatatable(resultObject.serCaltab);
				break;
			case "createsubscr":
				// clear div
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'block');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Create Subscriber";
				dataProcessEnd();
				if (resultObject.createsubscrtab != null) generateMessagingDatatable(resultObject.createsubscrtab);
				break;
			case "updateBal":
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'block');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Update Balance Details";
				dataProcessEnd();
				if (resultObject.updateBaltab != null) generateMessagingDatatable(resultObject.updateBaltab);
				break;
			case "updateAccum":
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'block');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Update Accumulators Details";
				dataProcessEnd();
				if (resultObject.updateAccumtab != null) generateMessagingDatatable(resultObject.updateAccumtab);
				break;
			case "updateDABal":
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'block');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Update DA Balance";
				dataProcessEnd();
				if (resultObject.updateAccumtab != null) generateMessagingDatatable(resultObject.updateAccumtab);
				break;
			case "updateSubSegment":
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'block');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				dataProcessEnd();
				if (resultObject.updateSubSegtab != null) generateMessagingDatatable(resultObject.updateSubSegtab);
				document.title = "Update Suscriber Segments";
				break;
			case "activeSub":
				$('#insdp').empty();
				$('#activateSubscriber').css('display', 'block');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#deleteSubscriber').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				dataProcessEnd();
				document.title = "Active Subscriber";
				if (resultObject.activeSubtab != null) generateMessagingDatatable(resultObject.activeSubtab);
				break;
			case "deleteSub":
				$('#insdp').empty();
				$('#deleteSubscriber').css('display', 'block');
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#updateOfferDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Delete Subscriber";
				dataProcessEnd();
				if (resultObject.deleteSubtab != null) generateMessagingDatatable(resultObject.deleteSubtab);
				break;
			case "updateOffer":
				$('#insdp').empty();
				updateOfferDetails
				$('#updateOfferDetails').css('display', 'block');
				$('#deleteSubscriber').css('display', 'none');
				$('#activateSubscriber').css('display', 'none');
				$('#searchBox').css('display', 'none');
				$('#updateServiceClass').css('display', 'none');
				$('#updateBalanceAndDate').css('display', 'none');
				$('#createSubscriber').css('display', 'none');
				$('#updateSubscriberSeg').css('display', 'none');
				$('#updateAccumulatorDetails').css('display', 'none');
				$('#insdp').css('display', 'none');
				$('#getAccumulatorssearchBox').css('display', 'none');
				$('#updateDaBalDetails').css('display', 'none');
				$('#getOffersearchBox').css('display', 'none');
				clearTextForAcc();
				document.title = "Update Offer";
				dataProcessEnd();
				if (resultObject.deleteSubtab != null) generateMessagingDatatable(resultObject.deleteSubtab);
				break;
			default:
				//TODO youre code here  
				break;
		}
	});
});

function dataProcessStart() {
	$('#loading').html('<img src="/edat/resources/images/process.gif" >');
	$('#extraInfo, #extrainfo2').hide();
}

function dataProcessEnd() {
	$('#loading').empty();
}

function displayErroGrid() {
	html = "<table class='table'><tr><td style='font-size: 15px;'><b> Node not reachable !! </b></td>";
	html += "<td></td><td></td>";
	$('#insdp').show();
	document.getElementById('insdp').innerHTML = html;
	dataProcessEnd();
}

function validateInput(msisdn, focus) {
	if (msisdn.trim().length == 0) {
		alert('Please enter the msisdn');
		document.getElementById(focus).focus();
		return false;
	} else if (msisdn.length != 10 && msisdn.length != 0) {
		alert('Invalid MSISDN');
		document.getElementById(focus).focus();
		return false;
	}
	return true;
}

function validateInputFields() {
	var accId = $("#AccId").val();
	var accVal = $("#AccVal").val();
	if (accId.trim().length == 0) {
		alert('Please enter the Accumulator Id');
		document.getElementById("AccId").focus();
		return false;
	} else if (accVal.trim().length == 0) {
		alert('Please enter the Accumulator Value');
		document.getElementById("AccVal").focus();
		return false;
	}
	return true;
}

function validateInputForUpdateDABalFields() {
	var accId = $("#dedicatedAccountId").val();
	var accVal = $("#dedicatedAccountValue").val();
	if (accId.trim().length == 0) {
		alert('Please enter the Dedicated Account Id');
		document.getElementById("dedicatedAccountId").focus();
		return false;
	} else if (accVal.trim().length == 0) {
		alert('Please enter the Dedicated Account  Value');
		document.getElementById("dedicatedAccountValue").focus();
		return false;
	}
	return true;
}

function validateInputForUpdateOfferFields() {
	var accId = $("#offerId").val();
	if (accId.trim().length == 0) {
		alert('Please enter the Offer Id');
		document.getElementById("offerId").focus();
		return false;
	}
	return true;
}

function validateDateActiveFields() {
	var superExpDate = $("#actSupexpDatedatepicker").val();
	var feeExpDate = $("#actSerfeeExpDatedatepicker").val();
	
	
	if (superExpDate.trim().length == 0) {
		alert('Please enter Supervision Expiry Date');
		document.getElementById("actSupexpDatedatepicker").focus();
		return false;
	}else if(feeExpDate.trim().length == 0)
		{
		alert('Please enter Service FeeExpiry Date');
		document.getElementById("actSerfeeExpDatedatepicker").focus();
		return false;
		}
	return true;
}


function validateDateCreateSubscriberFields() {
	var serviceClass = $("#newServiceClass").val();
	var newLanguage = $("#newLanguage").val();
	
	
	if (serviceClass.trim().length == 0) {
		alert('Please enter Service Class New');
		document.getElementById("newServiceClass").focus();
		return false;
	}else if(newLanguage.trim().length == 0)
		{
		alert('Please enter Language ID New');
		document.getElementById("newLanguage").focus();
		return false;
		}
	return true;
}

function validateDateCreateSubscriberFields() {
	var superExpDate = $("#expDatedatepicker").val();
	var feeExpDate = $("#feeExpDatedatepicker").val();
	var adjAmountRelative = $("#AmountRelativeForUpdateBalance").val();

	
	
	if (superExpDate.trim().length == 0) {
		alert('Please enter Supervision Expiry Date');
		document.getElementById("expDatedatepicker").focus();
		return false;
	}else if(feeExpDate.trim().length == 0)
		{
		alert('Please enter Service FeeExpiry Date');
		document.getElementById("feeExpDatedatepicker").focus();
		return false;
		}else if(adjAmountRelative.trim().length == 0)
			{
			alert('Please enter Adjustment Amount Relative');
			document.getElementById("AmountRelativeForUpdateBalance").focus();
			return false;
			}
	return true;
}





function validateDate() {
	var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
	var supexpDatedate = $("#expDatedatepicker").val();
	if (supexpDatedate != "") {
		if (supexpDatedate.match(dateformat)) {
			var opera1 = supexpDatedate.split('/');
			lopera1 = opera1.length;
			if (lopera1 > 1) {
				var pdate = supexpDatedate.split('/');
			}
			var mm = parseInt(pdate[0]);
			var dd = parseInt(pdate[1]);
			var yy = parseInt(pdate[2]);
			// Create list of days of a month [assume there is no leap year by default]  
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (mm == 1 || mm > 2) {
				if (dd > ListofDays[mm - 1]) {
					alert('Invalid date format!');
					$('#expDatedatepicker').focus();
					return false;
				}
			}
			if (mm == 2) {
				var lyear = false;
				if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
					lyear = true;
				}
				if ((lyear == false) && (dd >= 29)) {
					alert('Invalid date format!');
					$('#expDatedatepicker').focus();
					return false;
				}
				if ((lyear == true) && (dd > 29)) {
					alert('Invalid date format!');
					$('#expDatedatepicker').focus();
					return false;
				}
			}
		} else {
			alert("Invalid date format!");
			$('#expDatedatepicker').focus();
			return false;
		}
	} else {
		return true;
	}
	return true;
}

function validateDateForFeeExpiry() {
	var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
	var supexpDatedate = $("#feeExpDatedatepicker").val();
	if (supexpDatedate != "") {
		if (supexpDatedate.match(dateformat)) {
			var opera1 = supexpDatedate.split('/');
			lopera1 = opera1.length;
			if (lopera1 > 1) {
				var pdate = supexpDatedate.split('/');
			}
			var mm = parseInt(pdate[0]);
			var dd = parseInt(pdate[1]);
			var yy = parseInt(pdate[2]);
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (mm == 1 || mm > 2) {
				if (dd > ListofDays[mm - 1]) {
					alert('Invalid date format!');
					$('#feeExpDatedatepicker').focus();
					return false;
				}
			}
			if (mm == 2) {
				var lyear = false;
				if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
					lyear = true;
				}
				if ((lyear == false) && (dd >= 29)) {
					alert('Invalid date format!');
					$('#feeExpDatedatepicker').focus();
					return false;
				}
				if ((lyear == true) && (dd > 29)) {
					alert('Invalid date format!');
					$('#expDatedatepicker').focus();
					return false;
				}
			}
		} else {
			alert("Invalid date format!");
			$('#feeExpDatedatepicker').focus();
			return false;
		}
	} else {
		return true;
	}
	return true;
}

function validateDateForActiveSub() {
	var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
	var supexpDatedate = $("#actSupexpDatedatepicker").val();
	if (supexpDatedate != "") {
		if (supexpDatedate.match(dateformat)) {
			var opera1 = supexpDatedate.split('/');
			lopera1 = opera1.length;
			if (lopera1 > 1) {
				var pdate = supexpDatedate.split('/');
			}
			var mm = parseInt(pdate[0]);
			var dd = parseInt(pdate[1]);
			var yy = parseInt(pdate[2]);
			// Create list of days of a month [assume there is no leap year by default]  
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (mm == 1 || mm > 2) {
				if (dd > ListofDays[mm - 1]) {
					alert('Invalid date format!');
					$('#actSupexpDatedatepicker').focus();
					return false;
				}
			}
			if (mm == 2) {
				var lyear = false;
				if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
					lyear = true;
				}
				if ((lyear == false) && (dd >= 29)) {
					alert('Invalid date format!');
					$('#actSupexpDatedatepicker').focus();
					return false;
				}
				if ((lyear == true) && (dd > 29)) {
					alert('Invalid date format!');
					$('#actSupexpDatedatepicker').focus();
					return false;
				}
			}
		} else {
			alert("Invalid date format!");
			$('#actSupexpDatedatepicker').focus();
			return false;
		}
	} else {
		return true;
	}
	return true;
}

function validateDateForFeeExpiryActiveSub() {
	var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
	var supexpDatedate = $("#actSerfeeExpDatedatepicker").val();
	if (supexpDatedate != "") {
		if (supexpDatedate.match(dateformat)) {
			var opera1 = supexpDatedate.split('/');
			lopera1 = opera1.length;
			if (lopera1 > 1) {
				var pdate = supexpDatedate.split('/');
			}
			var mm = parseInt(pdate[0]);
			var dd = parseInt(pdate[1]);
			var yy = parseInt(pdate[2]);
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (mm == 1 || mm > 2) {
				if (dd > ListofDays[mm - 1]) {
					alert('Invalid date format!');
					$('#actSerfeeExpDatedatepicker').focus();
					return false;
				}
			}
			if (mm == 2) {
				var lyear = false;
				if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
					lyear = true;
				}
				if ((lyear == false) && (dd >= 29)) {
					alert('Invalid date format!');
					$('#actSerfeeExpDatedatepicker').focus();
					return false;
				}
				if ((lyear == true) && (dd > 29)) {
					alert('Invalid date format!');
					$('#actSerfeeExpDatedatepicker').focus();
					return false;
				}
			}
		} else {
			alert("Invalid date format!");
			$('#actSerfeeExpDatedatepicker').focus();
			return false;
		}
	} else {
		return true;
	}
	return true;
}