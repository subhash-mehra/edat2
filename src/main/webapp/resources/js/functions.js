
$(document).ready(function() {
	$("#dynaTable").hide();
	$("#dynaTable1").hide();
	$("#dynaTable2").hide();
	$("#btnAddd").click(function() {
		$("#dynaTable").show();
		addRow();
	});
	$("#btnAddd1").click(function() {
		$("#dynaTable1").show();
		addRow1();
	});
	$("#btnAddd2").click(function() {
		$("#dynaTable2").show();
		addRow2();
	});

	$(".btnEdit").click(function() {
		Edit();
	});
	$(".btnDelete").click(function() {
		deleteRow();
	});
	
	$(".btnEdit1").click(function() {
		Edit1();
	});
	$(".btnDelete1").click(function() {
		deleteRow1();
	});
	
	$(".btnEdit2").click(function() {
		Edit2();
	});
	$(".btnDelete2").click(function() {
		deleteRow2();
	});
});

function addRow() {

	var rowCount = $('#sdpContentBody >tbody >tr').length;
	var html ="<tr>";
	html += /*"<td><input type='text' class='fieldset'/></td>" +
			"<td><select class='fieldset' name='value"+rowCount+"' ><option value='0'>0</option><option value='1'>1</option></select></td>" +*/
		"<td><input type='text' class='fieldset' onblur=validateNumForINSDP(this.id); name='value"+rowCount+"'id='value"+rowCount+"'/></td>" +
		
			"<td><select class='fieldset' name='ActiveFlag"+rowCount+"' id='ActiveFlag"+rowCount+"'><option value='0'>0</option><option value='1'>1</option></select></td>"
			+"<td><img src='resources/images/delete.png' class='btnDelete' alt='Delete'/></td>";
			//+"<td><img src='resources/images/save.png' class='btnSave'>
	html += "</tr>";
	
	$('#sdpContentBody').append(html);
	//$(".btnSave").bind("click", saveTable);
	$(".btnDelete").bind("click", deleteRow); };
	
	function addRow1() {
		
		
		var rowCount = $('#sdpContentBody1 >tbody >tr').length;
		var html ="<tr>";
		html += /*"<td><input type='text' class='fieldset'/></td>" +*/
				/*"<td><select class='fieldset' name='value2"+rowCount+"' ><option value='0'>0</option><option value='1'>1</option></select></td>" +*/
				"<td><input type='text' class='fieldset'  onblur=validateNumForINSDP(this.id); name='value"+rowCount+"' id='value"+rowCount+"'/></td>" +
				"<td><select class='fieldset' name='ActiveFlag2"+rowCount+"' id='ActiveFlag2"+rowCount+"' ><option value='0'>0</option><option value='1'>1</option></select></td>"+
				"<td><img src='resources/images/delete.png' class='btnDelete1'/></td>";
				//"<td><img src='resources/images/save.png' class='btnSave1'><img src='resources/images/delete.png' class='btnDelete1'/></td>";
		html += "</tr>";
		
		$('#sdpContentBody1').append(html);
		//$(".btnSave1").bind("click", saveTable1);
		$(".btnDelete1").bind("click", deleteRow1); };
		
		function addRow2() {
			
			var rowCount = $('#sdpContentBody2 >tbody >tr').length;
			var html ="<tr>";
					
			html += "<td><input type='text' class='fieldset'/></td>" +
					"<td><select class='fieldset' name='value4"+rowCount+"' ><option value='0'>0</option><option value='1'>1</option></select></td>" +
					"<td><select class='fieldset' name='ActiveFlag4"+rowCount+"' ><option value='0'>0</option><option value='1'>1</option></select></td>"+
				
					"<td><img src='resources/images/save.png' class='btnSave2'><img src='resources/images/delete.png' class='btnDelete2'/></td>";
				
			html += "</tr>";
			
			$('#sdpContentBody2').append(html);
			$(".btnSave2").bind("click", saveTable2);
			$(".btnDelete2").bind("click", deleteRow2); };

//Create Subscriber
function saveTable() {
/*	var length=$('#sdpContentBody >tbody >tr').length;
	
	var flag="flase";
	var i;
	for(i=1;i<length;i++)
	{
		if($("#value"+i).val()!="")
		{
			flag="true";
			break;
		}else
			{
			flag="flase";
			break;
			}
	}
	if(flag=="true")
	{
		$("#rowFlag").val("1");
		
	}*/
	
	
	var par = $(this).parent().parent();
	var attElement = par.children("td:nth-child(1)");
	var valueKey = par.children("td:nth-child(2)");
	var activeFlags = par.children("td:nth-child(3)");
	var tdButtons = par.children("td:nth-child(4)");
	attElement.html(attElement.children("input[type=text]").val());
	
	activeFlags.html(activeFlags.find('select option:selected').val());
	//valueKey.html(valueKey.children("input[type=text]").val());
	valueKey.html(valueKey.find('select option:selected').val());
	
	tdButtons.html("<img src='resources/images/delete.png' class='btnDelete'/><img src='resources/images/pencil.png' class='btnEdit'/>");
	$(".btnEdit").bind("click", Edit);
	$(".btnDelete").bind("click", deleteRow);
	
	};

//Create Subscriber
function Edit() {
	var par = $(this).parent().parent();
	var attElement = par.children("td:nth-child(1)");

	var valueKey = par.children("td:nth-child(2)");
	var activeFlags = par.children("td:nth-child(3)");
	var tdButtons = par.children("td:nth-child(4)");
	
	attElement.html("<input type='text' id='txtName' value='" + attElement.html()
			+ "'/>");
	//console.log(valueKey.html());
	if(valueKey.html() == '0'){
		valueKey.html("<select class='fieldset' ><option value='0' selected >0</option><option value='1'>1</option></select>");
	}
	if(valueKey.html() == '1'){
		valueKey.html("<select class='fieldset' ><option value='0'>0</option><option value='1' selected >1</option></select>");
	}
	
	if(activeFlags.html() == '0'){
		activeFlags.html("<select class='fieldset' ><option value='0' selected >0</option><option value='1'>1</option></select>");
	}
	if(activeFlags.html() == '1'){
		activeFlags.html("<select class='fieldset' ><option value='0'>0</option><option value='1' selected >1</option></select>");
	}
	
	
	tdButtons.html("<img src='resources/images/save.png' class='btnSave'/>");
	$(".btnSave").bind("click", saveTable);
	$(".btnEdit").bind("click", Edit);
	$(".btnDelete").bind("click", deleteRow); };
	
//Create Subscriber
function deleteRow() {
	
/*var length=$('#sdpContentBody >tbody >tr').length;
	
	var flag="false";
	var i;
	for(i=1;i<length;i++)
	{
		if($("#value"+i).val()!="" && $("#value"+i).val()!=undefined)
		{
			flag="true";
			break;
		}else
			{
			flag="flase";
			
			}
	}
	if(flag=="true")
	{
		$("#rowFlag").val("1");
		
	}else
		{
		$("#rowFlag").val("1");
		}*/
	
	var par = $(this).parent().parent();
	par.remove();

	var rowCount = $('#sdpContentBody >tbody >tr').length;
	if(rowCount == 1){
		$("#dynaTable").hide();	
	}
};

// Update Subscriber Segment
function saveTable1() {
	var par = $(this).parent().parent();
	var attElement = par.children("td:nth-child(1)");
	var valueKey = par.children("td:nth-child(2)");
	var activeFlags = par.children("td:nth-child(3)");
	var tdButtons = par.children("td:nth-child(4)");
	attElement.html(attElement.children("input[type=text]").val());
	
	activeFlags.html(activeFlags.find('select option:selected').val());
	//valueKey.html(valueKey.children("input[type=text]").val());
	valueKey.html(valueKey.find('select option:selected').val());
	tdButtons.html("<img src='resources/images/delete.png' class='btnDelete1'/><img src='resources/images/pencil.png' class='btnEdit1'/>");
	$(".btnEdit1").bind("click", Edit1);
	$(".btnDelete1").bind("click", deleteRow1); };

	//Update Subscriber Segment
function Edit1() {
	var par = $(this).parent().parent();
	var attElement = par.children("td:nth-child(1)");

	var valueKey = par.children("td:nth-child(2)");
	var activeFlags = par.children("td:nth-child(3)");
	var tdButtons = par.children("td:nth-child(4)");
	attElement.html("<input type='text' id='txtName' value='" + attElement.html()
			+ "'/>");
	//console.log(valueKey.html());
	if(valueKey.html() == '0'){
		valueKey.html("<select class='fieldset' ><option value='0' selected >0</option><option value='1'>1</option></select>");
	}
	if(valueKey.html() == '1'){
		valueKey.html("<select class='fieldset' ><option value='0'>0</option><option value='1' selected >1</option></select>");
	}
	
	if(activeFlags.html() == '0'){
		activeFlags.html("<select class='fieldset' ><option value='0' selected >0</option><option value='1'>1</option></select>");
	}
	if(activeFlags.html() == '1'){
		activeFlags.html("<select class='fieldset' ><option value='0'>0</option><option value='1' selected >1</option></select>");
	}
	
	
	tdButtons.html("<img src='resources/images/save.png' class='btnSave1'/>");
	$(".btnSave1").bind("click", saveTable1);
	$(".btnEdit1").bind("click", Edit1);
	$(".btnDelete1").bind("click", deleteRow1); };

	//Update Subscriber Segment
function deleteRow1() {
	
	var par = $(this).parent().parent();
	par.remove();
	var rowCount = $('#sdpContentBody1 >tbody >tr').length;
	if(rowCount == 1){
		$("#dynaTable1").hide();	
	}
};

//Update Accumulators Details
function saveTable2() {
	var par = $(this).parent().parent();
	var attElement = par.children("td:nth-child(1)");
	var valueKey = par.children("td:nth-child(2)");
	var activeFlags = par.children("td:nth-child(3)");
	var tdButtons = par.children("td:nth-child(4)");
	attElement.html(attElement.children("input[type=text]").val());
	
	activeFlags.html(activeFlags.find('select option:selected').val());
	//valueKey.html(valueKey.children("input[type=text]").val());
	valueKey.html(valueKey.find('select option:selected').val());
	tdButtons.html("<img src='resources/images/delete.png' class='btnDelete2'/><img src='resources/images/pencil.png' class='btnEdit2'/>");
	$(".btnEdit2").bind("click", Edit2);
	$(".btnDelete2").bind("click", deleteRow2); };

	//Update Accumulators Details
function Edit2() {
	var par = $(this).parent().parent();
	var attElement = par.children("td:nth-child(1)");

	var valueKey = par.children("td:nth-child(2)");
	var activeFlags = par.children("td:nth-child(3)");
	var tdButtons = par.children("td:nth-child(4)");
	attElement.html("<input type='text' id='txtName' value='" + attElement.html()
			+ "'/>");
	//console.log(valueKey.html());
	if(valueKey.html() == '0'){
		valueKey.html("<select class='fieldset' ><option value='0' selected >0</option><option value='1'>1</option></select>");
	}
	if(valueKey.html() == '1'){
		valueKey.html("<select class='fieldset' ><option value='0'>0</option><option value='1' selected >1</option></select>");
	}
	
	if(activeFlags.html() == '0'){
		activeFlags.html("<select class='fieldset' ><option value='0' selected >0</option><option value='1'>1</option></select>");
	}
	if(activeFlags.html() == '1'){
		activeFlags.html("<select class='fieldset' ><option value='0'>0</option><option value='1' selected >1</option></select>");
	}
	
	tdButtons.html("<img src='resources/images/save.png' class='btnSave2'/>");
	$(".btnSave2").bind("click", saveTable2);
	$(".btnEdit2").bind("click", Edit2);
	$(".btnDelete2").bind("click", deleteRow2); };

	//Update Accumulators Details
function deleteRow2() {
	var par = $(this).parent().parent();
	par.remove();
};

