var Constants = {
	CONTEXT_PATH : "/edat",
	NAP : "nap",
	NAP_INFO : {
		SOCS : "socs",
		PROFILE : "profile",
		PARTNERS : "partners"
	},
	DUS : "dus",
	PCRF : "pcrf",
	UMA : "uma",
	HSS : "hss",
	HLR : "hlr",
	EIR : "eir",
	USD_SELECTED_TAB : "nap",
	GLFEX : "/gflex",
	USD : "/usd/",
	GLFEX_SEARCH_MSISDN : "/search",
	GLFEX_ADD_MSISDN : "/add",
	GLFEX_UPDATE_MSISDN : "/update",
	GLFEX_DELETE_MSISDN : "/delete",
	GLFEX_REPORT : "/gFlexReports",
	GLFEX_SELECTED_TAB : "/search",
	EIR_SEARCH : "searchEir",
	EIR_ADD : "addEir",
	EIR_UNLOCK : "unlockEir",
	EIR_SEARCH_TAC : "searchTac",
	EIR_ADD_TAC : "addtac",
	EIR_ENABLE_MMS : "enableMMS"
	
		
};

var Method = {
		GET : "GET",
		POST : "POST",
		UPDATE : "UPDATE",
		DELETE : "DELETE"
}

var error_msg = "Mandatory Field is Empty !";

