function getData() {  
		
		 var checkedValue = $("input[name='messagingRadio']:checked").val();
		 
		var searchValue = $("#searchTextBox").val();
		
		var selectValue = $('#searchSelector :selected').val();
		
		var data = null;
		
		if(selectValue=="msisdn")
		 {
			
			 data = { "customerId" : null, "msisdn" : searchValue, "imsi": null} ;
		 }
		if(selectValue=="imsi")
		{
			 data = { "customerId" : null, "msisdn" : null, "imsi": searchValue} ;
		}
	
		if(selectValue=="userId")
		{
			 data = { "customerId" : searchValue, "msisdn" : null, "imsi": null} ;
		}
	
		// data = { "customerId" : null, "msisdn" : msisdn, "imsi": imsi} ;
		
		var searchUrl =null;
		var testdata = JSON.stringify(data);
		if(checkedValue =='smcs')
			{
			  searchUrl ='/edat/home/mdt/smsc';
			}
		if(checkedValue =='mmsc')
		    {
			 searchUrl ='/edat/home/mdt/mmsc';
				
			}
	    if(checkedValue =='iam')
		   {
	    	 searchUrl ='/edat/home/mdt/iam';
		   }
		
		    $.ajax({
		    	url: searchUrl,
		    	data: testdata,
		    	type: "POST",

		    	beforeSend: function(xhr) {				    		
		    		xhr.setRequestHeader("Accept", "application/json");
		    		xhr.setRequestHeader("Content-Type", "application/json");
		    	},
		    	success: function(result) {
		    		var respContent = "";	
		    		generateDatatable(result);
					//alert(JSON.stringify(result));
		    		$("#responseJson").val(JSON.stringify(result));		
		    	}
		    });

		 //   event.preventDefault();
		  }


function generateDatatable(result)
   {
	    var myTable = "<table data-toggle='table' class= 'display' data-height='180' id='iamTable'><thead><tr><th>Attribute</th>"; 
		myTable += "<th> Value </th>"; 
		myTable +="</tr> </thead>  <tbody>"; 
		
		
		for (var key in result.attributes) {
			   
		   myTable +="<tr class='success'><td>" +key +"</td>"; 
           myTable +="<td>" +result.attributes[key] + "</td>"; 
		   myTable +="</tr> "; 
		}

	myTable +="</tbody></table>"; 
    document.getElementById('myTable').innerHTML = myTable;
    $("#tabs").tabs( {
        "activate": function(event, ui) {
            $( $.fn.dataTable.tables( true ) ).DataTable().columns.adjust();
           }
        } );   
		  $('#iamTable').dataTable( {
            "scrollY":        "300px",
            "toolbar":		false,
             "scrollCollapse": true,
             "paging":         false,
         			 
			  "columns": [
                    null,
					null
					
                  ],
				   "jQueryUI": true,
         } ); 
	}
