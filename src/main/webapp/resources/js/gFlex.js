var searchMsisdnValue = null;

$(document).ready(function() {
	$("#gflex").parent().addClass("active");
	$(function() {
		$("#deleteDate").datepicker();
	});

	$("#gflexContents").hide();
	$("#gflContentHeading").text("MSISDN Query Result ");
	
	$("#searchText").keypress(function(event) {
		if (event.keyCode == 13) {
			$("#search").click();
		}
	});
	
	$("#search").click(function() {
		searchCretria = {};
		if ($.trim($("#searchText").val()) == '') {
			alert("Please specify search criteria...");
			return;
		} else if ($("#searchText").val().length != 10) {
			alert('Invalid MSISDN');
			$("#searchText").focus();
			return false;
		} else {
			getSearchData();
		}
	});

	$("#cleanUpBtn").click(function() {
		if(validateDateForCleanupFromDate() && validateDateForCleanupToDate() && fromDateValidate())
		{
			
			cleanUpData();
		}
	});
	$("#saveBtn").click(function() {
		var type = $("#gFlexTabList li.selected")[0].id;

		if(type =="addMsisdn"){
			if(validateDateForUpdateMsisdn())
			{
			addMsisdnSave();
			}
		}else{
			if(validateDateForUpdateMsisdn())
			{
			UpdateMsisdn();
			}
		}
		

	});

	$("#canBtn").click(function() {
		cancelAddMsisdn();

	});

	$("#gFlexTabList li").click(function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		$("#gflContentHeading").text("MSISDN Query Result ");
		$("#search").text("Search");
		switch (this.id) {

			case "addMsisdn" :
				$("#errorMainPannel").hide();
				$("#searchDiv, #cleanupDiv, #messageHeading, #gflexResponse").hide();
				$("#gflexContents, #gflexBody").show();
				$("#msisdnID").val("");
				$("#deleteDate").val("");
				$("#gflexResponse").hide();
				dataProcessEnd();
				document.title="GFlex Add Msisdn";
				break;

			case "updateMsisdn" :
				$("#errorMainPannel").hide();
				$("#searchDiv").show();
				$("#gflexContents").hide();
				$("#cleanupDiv").hide();
				$('#messageHeading').hide();
				$("#gflexResponse").hide();
				dataProcessEnd();
				document.title="GFlex Update Msisdn";
				break;

			case "deleteMsisdn" :
				$("#errorMainPannel").hide();
				$("#search").text("Delete");
				$("#gflexContents").hide();
				$("#cleanupDiv").hide();
				$('#messageHeading').hide();
				$("#searchDiv").show();
				$("#gflexResponse").hide();
				dataProcessEnd();
				document.title="GFlex Delete Msisdn";
				break;

			case "cleanUp" :
				$("#errorMainPannel").hide();
				$("#cleanupDiv").show();
				$('#messageHeading').hide();
				$("#gflexContents").hide();
				$("#searchDiv").hide();
				$("#gflexResponse").hide();
				dataProcessEnd();
				document.title="GFlex CleanUp";
				break;

			case "reports" :
				$("#errorMainPannel").hide();
				$("#gflContentHeading").text("Gflex Report");
				$("#gflexContents").show();
				$("#messageHeading, #cleanupDiv, #searchDiv,#gflexBody").hide();
				getGflexReports();
				$("#gflexResponse").hide();
				dataProcessEnd();
				document.title="GFlex Reports";
				break;
			case "searchMsisdn" :
			default :
				$("#errorMainPannel").hide();
				$("#searchDiv").show();
				$("#gflexContents").hide();
				$("#cleanupDiv").hide();
				$('#messageHeading').hide();
				$("#gflexResponse").hide();
				document.title="GFlex Search Msisdn";
				dataProcessEnd();

				break;
		}
		$("#searchText").val("");

	});

});

function fromDateValidate()
{
    var startDate = document.getElementById("fromDate").value;
    var endDate = document.getElementById("toDate").value;
 
    if ((Date.parse(startDate) >= Date.parse(endDate))) {
        alert("To date should be greater than From date");
        document.getElementById("toDate").value = "";
        document.getElementById("toDate").focus();
        return false;
    }
    
    return true;

}

function getSearchData() {
	dataProcessStart();
	var searchJson = {};
	searchMsisdnValue = '1' + $("#searchText").val();
	searchJson.msisdn = '1' + $("#searchText").val();

	var searchUrl = Constants.CONTEXT_PATH + Constants.GLFEX
			+ Constants.GLFEX_SEARCH_MSISDN;
	
	$.ajax({
		url : searchUrl,
		data : JSON.stringify(searchJson),
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			// $("#gflexResponseBody").html(xhr.status + ":" + thrownError);
			displayErroGrid();

		},
		success : function(result) {
			showResult(result);

		}
	});

}

function showResult(result) {
	
	if (result.applicationError){
		displayInternalErroGrid(result.applicationError.errorCode);
		return ;
	}

	var type = $("#gFlexTabList li.selected")[0].id;
	var html = null;
	
	if(result.response !=""){
		var el1 = document.getElementById("updateMsisdnValue");
		for(var i=0; i<el1.options.length; i++) {
		  if ( el1.options[i].value == result.selVal) {
		    el1.selectedIndex = i;
		    break;
		  }
		}
	     html = "<table class='table'><tr><td>" + result.response + "</td>";
	

	
	switch (type) {

		case "updateMsisdn" :
			var index=result.response.indexOf("rc");
			index=index+3;
			var status=result.response.substring(index, index+4);
			if(status=="0, d")
			{
				status="0";
			}
			var msisdn =  result.msisdn;
			if(result.msisdn!="")
			{
				msisdn=msisdn.substring(1, msisdn.length);
			}
			$("#searchDiv, #gflexResponse").hide();
			$("#gflexBody").show();
			if(status=="0")
			{
				$("#msisdnID").val(msisdn);
				$("#saveBtn").html("Save");
				$("#deleteDate").val(result.deleteDate);
			}else
			{
			$("#msisdnID").val("");
			$("#saveBtn").html("Save");
			$("#deleteDate").val("");
			}
			break;

		case "deleteMsisdn" :
			html += "<td></td><td></td>";
			DeleteMsisdn();
			break;

		case "searchMsisdn" :
		default :
			html += "<td></td><td></td>";
			$('#gflexBody').hide();
			$('#gflexResponseBody').empty();
			$('#gflexResponseBody').html(html);
			$("#searchDiv, #gflexResponse").show();
			break;
	}

	html += "</tr></table>";
	// $('#gflexBody').empty();
	// $('#gflexBody').html(html);
	
	$("#gflexContents").show();

	$(".btnUpdateMsisdn").bind("click", UpdateMsisdn);
	
}else{
		displayInternalErroResPonseGrid();
	}
	dataProcessEnd();
}

function displayInternalErroGrid(key) {
	
	$("#errorMainPannel").show();
	$('#ErrorResponseHeading').show();
	
	//$('#ErrorResponseHeading').empty();
	$('#ErrorResponseBody').show();
	$('#ErrorResponseHeading').empty();
	$('#ErrorResponseBody').html("Node Not Found!");
	dataProcessEnd();
}

function displayInternalErroResPonseGrid(key) {
	$("#errorMainPannel").show();
	$('#ErrorResponseHeading').show();
	//$('#ErrorResponseHeading').empty();
	$('#ErrorResponseBody').show();
	$('#ErrorResponseHeading').empty();
	$('#ErrorResponseBody').html("Customer Not Found!!!");
	dataProcessEnd();
}


function addMsisdnSave() {

	
	var msisdnID = $("#msisdnID").val();
	var updateMsisdnValue = $("#updateMsisdnValue").find('option:selected')
			.val();
	var deleteDate = $("#deleteDate").val();
	
	if ($.trim(msisdnID) == '') {
		alert("Please enter MSISDN.");
		$("#msisdnID").focus();
		return false
	} else if (msisdnID.length != 10) {
		alert('Invalid MSISDN');
		$("#msisdnID").focus();
		return false;
	} else if (updateMsisdnValue == '-1') {
		alert("Please select value.");
		return false;
	} else if ($.trim(deleteDate) == '') {
		alert("Please enter Deleted date.");
		$("#deleteDate").focus();
		return;
	}

	dataProcessStart();
	var searchJson = {};
	searchJson.msisdn = '1'+msisdnID;
	searchJson.selValue = updateMsisdnValue;
	searchJson.deleteDate = deleteDate;
	var searchUrl = Constants.CONTEXT_PATH + Constants.GLFEX
			+ Constants.GLFEX_ADD_MSISDN;
	$.ajax({
		url : searchUrl,
		data : JSON.stringify(searchJson),
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$("#gflexResponseBody").html(xhr.status + ":" + thrownError);
		},
		success : function(result) {
			switch (result.status)
			{

			case "0" :
				$("#gflexResponseBody").html("Success... Responce code : "+ result.status);
				break;
			case "1001" :
				$("#gflexResponseBody").html("Internal error... Responce code : "+ result.status);
				break;
			case "1002" :
				$("#gflexResponseBody").html("Not connceted... Responce code : "+ result.status);
				break;
			case "1003" :
				$("#gflexResponseBody").html("Already connected... Responce code : "+ result.status);
				break;
			case "1004" :
				$("#gflexResponseBody").html("Parse failed... Responce code : "+ result.status);
				break;
			case "1005" :
				$("#gflexResponseBody").html("Write Unavail... Responce code : "+ result.status);
				break;
			case "1006" :
				$("#gflexResponseBody").html("No Write Permission... Responce code : "+ result.status);
				break;
			case "1007" :
				$("#gflexResponseBody").html("No Mate... Responce code : "+ result.status);
				break;
			case "1008" :
				$("#gflexResponseBody").html("Standby Side... Responce code : "+ result.status);
				break;
			case "1009" :
				$("#gflexResponseBody").html("No Active Txn... Responce code : "+ result.status);
				break;
			case "1010" :
				$("#gflexResponseBody").html("Active Txn... Responce code : "+ result.status);
				break;
			case "1011" :
				$("#gflexResponseBody").html("Write In Read Txn... Responce code : "+ result.status);
				break;
			case "1012" :
				$("#gflexResponseBody").html("Invalid Value... Responce code : "+ result.status);
				break;
			case "1013" :
				$("#gflexResponseBody").html("Not Found... Responce code : "+ result.status);
				break;
			case "1014" :
				$("#gflexResponseBody").html("Conflict Found... Responce code : "+ result.status);
				break;
			case "1015" :
				$("#gflexResponseBody").html("Item Exists... Responce code : "+ result.status);
				break;
			case "1016" :
				$("#gflexResponseBody").html("Partial Success... Responce code : "+ result.status);
				break;
			case "1017" :
				$("#gflexResponseBody").html("No Updates... Responce code : "+ result.status);
				break;
			case "1018" :
				$("#gflexResponseBody").html("Interrupted... Responce code : "+ result.status);
				break;
			case "1019" :
				$("#gflexResponseBody").html("Bad Args... Responce code : "+ result.status);
				break;
			case "1020" :
				$("#gflexResponseBody").html("Too Many Connections... Responce code : "+ result.status);
				break;
			case "1021" :
				$("#gflexResponseBody").html("Ne Not Found... Responce code : "+ result.status);
				break;
			case "1022" :
				$("#gflexResponseBody").html("Contains Subs... Responce code : "+ result.status);
				break;
			case "1023" :
				$("#gflexResponseBody").html("Unknown Version... Responce code : "+ result.status);
				break;
			case "1024" :
				$("#gflexResponseBody").html("Unused2... Responce code : "+ result.status);
				break;
			case "1025" :
				$("#gflexResponseBody").html("Unimplemented... Responce code : "+ result.status);
				break;
			case "1026" :
				$("#gflexResponseBody").html("Mate Busy... Responce code : "+ result.status);
				break;
			case "1027" :
				$("#gflexResponseBody").html("Imsi Dn Limit... Responce code : "+ result.status);
				break;
			case "1028" :
				$("#gflexResponseBody").html("Bad Import Cmd... Responce code : "+ result.status);
				break;
			case "1029" :
				$("#gflexResponseBody").html("Txn Too Big... Responce code : "+ result.status);
				break;
			case "1030" :
				$("#gflexResponseBody").html("Db Maint Reqd... Responce code : "+ result.status);
				break;
			case "1031" :
				$("#gflexResponseBody").html("Db Exception... Responce code : "+ result.status);
				break;
			case "1032" :
				$("#gflexResponseBody").html("Max Imsi Limit... Responce code : "+ result.status);
				break;
			case "1033" :
				$("#gflexResponseBody").html("Max Dn Limit... Responce code : "+ result.status);
				break;
			case "1034" :
				$("#gflexResponseBody").html("Max Dnblk Limit... Responce code : "+ result.status);
				break;
			case "1035" :
				$("#gflexResponseBody").html("Max Ne Limit... Responce code : "+ result.status);
				break;
			case "1036" :
				$("#gflexResponseBody").html("Replicating... Responce code : "+ result.status);
				break;
			case "1037" :
				$("#gflexResponseBody").html("Check Digit Error... Responce code : "+ result.status);
				break;
			case "1038" :
				$("#gflexResponseBody").html("Imsi Not Found... Responce code : "+ result.status);
				break;
			case "1039" :
				$("#gflexResponseBody").html("Imei Imsi Limit... Responce code : "+ result.status);
				break;
			case "1040" :
				$("#gflexResponseBody").html("Max Imei Limit... Responce code : "+ result.status);
				break;
			case "1041" :
				$("#gflexResponseBody").html("Max Imei Blk Limit... Responce code : "+ result.status);
				break;
			case "1042" :
				$("#gflexResponseBody").html("No List For Imei... Responce code : "+ result.status);
				break;
			case "1043" :
				$("#gflexResponseBody").html("Bad Switch In Abp... Responce code : "+ result.status);
				break;
			case "1044" :
				$("#gflexResponseBody").html("Sub Ne Limit... Responce code : "+ result.status);
				break;
			default :
				
				$("#gflexResponseBody").html("Node not reachable :");
			}
			
			$("#searchDiv, #gflexBody").hide();
			$('#gflexResponse').show();
			$('#gflexBody').hide();
			dataProcessEnd();
		}
	});

};


function UpdateMsisdn() {

	
	var msisdnID = $("#msisdnID").val();
	
	var updateMsisdnValue = $("#updateMsisdnValue").find('option:selected')
			.val();
	var deleteDate = $("#deleteDate").val();

	if ($.trim(msisdnID) == '') {
		alert(error_msg);
		return;
	} else if (msisdnID.length != 10) {
		alert('Invalid MSISDN');
	} else if (updateMsisdnValue == '-1') {
		alert("Please select value.");
		return;
	} else if ($.trim(deleteDate) == '') {
		alert("Please enter Delete Date.");
		return;
	}
	dataProcessStart();
	var searchJson = {};
	searchJson.msisdn = '1'+msisdnID;
	searchJson.selValue = updateMsisdnValue;
	searchJson.deleteDate = deleteDate;
	
	var searchUrl = Constants.CONTEXT_PATH + Constants.GLFEX
			+ Constants.GLFEX_UPDATE_MSISDN;
	$.ajax({
		url : searchUrl,
		data : JSON.stringify(searchJson),
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$("#gflexResponseBody").html(xhr.status + ":" + thrownError);
		},
		success : function(result) {
			switch (result.status)
			{

			case "0" :
				$("#gflexResponseBody").html("Success... Responce code : "+ result.status);
				break;
			case "1001" :
				$("#gflexResponseBody").html("Internal error... Responce code : "+ result.status);
				break;
			case "1002" :
				$("#gflexResponseBody").html("Not connceted... Responce code : "+ result.status);
				break;
			case "1003" :
				$("#gflexResponseBody").html("Already connected... Responce code : "+ result.status);
				break;
			case "1004" :
				$("#gflexResponseBody").html("Parse failed... Responce code : "+ result.status);
				break;
			case "1005" :
				$("#gflexResponseBody").html("Write Unavail... Responce code : "+ result.status);
				break;
			case "1006" :
				$("#gflexResponseBody").html("No Write Permission... Responce code : "+ result.status);
				break;
			case "1007" :
				$("#gflexResponseBody").html("No Mate... Responce code : "+ result.status);
				break;
			case "1008" :
				$("#gflexResponseBody").html("Standby Side... Responce code : "+ result.status);
				break;
			case "1009" :
				$("#gflexResponseBody").html("No Active Txn... Responce code : "+ result.status);
				break;
			case "1010" :
				$("#gflexResponseBody").html("Active Txn... Responce code : "+ result.status);
				break;
			case "1011" :
				$("#gflexResponseBody").html("Write In Read Txn... Responce code : "+ result.status);
				break;
			case "1012" :
				$("#gflexResponseBody").html("Invalid Value... Responce code : "+ result.status);
				break;
			case "1013" :
				$("#gflexResponseBody").html("Not Found... Responce code : "+ result.status);
				break;
			case "1014" :
				$("#gflexResponseBody").html("Conflict Found... Responce code : "+ result.status);
				break;
			case "1015" :
				$("#gflexResponseBody").html("Item Exists... Responce code : "+ result.status);
				break;
			case "1016" :
				$("#gflexResponseBody").html("Partial Success... Responce code : "+ result.status);
				break;
			case "1017" :
				$("#gflexResponseBody").html("No Updates... Responce code : "+ result.status);
				break;
			case "1018" :
				$("#gflexResponseBody").html("Interrupted... Responce code : "+ result.status);
				break;
			case "1019" :
				$("#gflexResponseBody").html("Bad Args... Responce code : "+ result.status);
				break;
			case "1020" :
				$("#gflexResponseBody").html("Too Many Connections... Responce code : "+ result.status);
				break;
			case "1021" :
				$("#gflexResponseBody").html("Ne Not Found... Responce code : "+ result.status);
				break;
			case "1022" :
				$("#gflexResponseBody").html("Contains Subs... Responce code : "+ result.status);
				break;
			case "1023" :
				$("#gflexResponseBody").html("Unknown Version... Responce code : "+ result.status);
				break;
			case "1024" :
				$("#gflexResponseBody").html("Unused2... Responce code : "+ result.status);
				break;
			case "1025" :
				$("#gflexResponseBody").html("Unimplemented... Responce code : "+ result.status);
				break;
			case "1026" :
				$("#gflexResponseBody").html("Mate Busy... Responce code : "+ result.status);
				break;
			case "1027" :
				$("#gflexResponseBody").html("Imsi Dn Limit... Responce code : "+ result.status);
				break;
			case "1028" :
				$("#gflexResponseBody").html("Bad Import Cmd... Responce code : "+ result.status);
				break;
			case "1029" :
				$("#gflexResponseBody").html("Txn Too Big... Responce code : "+ result.status);
				break;
			case "1030" :
				$("#gflexResponseBody").html("Db Maint Reqd... Responce code : "+ result.status);
				break;
			case "1031" :
				$("#gflexResponseBody").html("Db Exception... Responce code : "+ result.status);
				break;
			case "1032" :
				$("#gflexResponseBody").html("Max Imsi Limit... Responce code : "+ result.status);
				break;
			case "1033" :
				$("#gflexResponseBody").html("Max Dn Limit... Responce code : "+ result.status);
				break;
			case "1034" :
				$("#gflexResponseBody").html("Max Dnblk Limit... Responce code : "+ result.status);
				break;
			case "1035" :
				$("#gflexResponseBody").html("Max Ne Limit... Responce code : "+ result.status);
				break;
			case "1036" :
				$("#gflexResponseBody").html("Replicating... Responce code : "+ result.status);
				break;
			case "1037" :
				$("#gflexResponseBody").html("Check Digit Error... Responce code : "+ result.status);
				break;
			case "1038" :
				$("#gflexResponseBody").html("Imsi Not Found... Responce code : "+ result.status);
				break;
			case "1039" :
				$("#gflexResponseBody").html("Imei Imsi Limit... Responce code : "+ result.status);
				break;
			case "1040" :
				$("#gflexResponseBody").html("Max Imei Limit... Responce code : "+ result.status);
				break;
			case "1041" :
				$("#gflexResponseBody").html("Max Imei Blk Limit... Responce code : "+ result.status);
				break;
			case "1042" :
				$("#gflexResponseBody").html("No List For Imei... Responce code : "+ result.status);
				break;
			case "1043" :
				$("#gflexResponseBody").html("Bad Switch In Abp... Responce code : "+ result.status);
				break;
			case "1044" :
				$("#gflexResponseBody").html("Sub Ne Limit... Responce code : "+ result.status);
				break;
			default :
				
				$("#gflexResponseBody").html("Node not reachable :");
			}
		
			
			
			$("#searchDiv, #gflexBody").hide();
			$('#gflexResponse').show();
			$('#gflexBody').hide();
			dataProcessEnd();
		}
	});

};

function DeleteMsisdn() {
	dataProcessStart();
	var searchJson = {};
	searchJson.msisdn = searchMsisdnValue;
	searchJson.deleteDate = new Date();

	var searchUrl = Constants.CONTEXT_PATH + Constants.GLFEX
			+ Constants.GLFEX_DELETE_MSISDN;

	$.ajax({
		url : searchUrl,
		data : JSON.stringify(searchJson),
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$("#gflexResponseBody").html(xhr.status + ":" + thrownError);

		},
		success : function(result) {
			if (result.status != "1013") {
				// $('#gflexBody').empty();
				$("#gflexResponseBody").html(
						"Record deleted sucessfully... Responce code : "
								+ result.status);
			} else {
				$("#gflexResponseBody").html(
						"Record cannot be deleted. Error code : "
								+ result.status);
			}
			$("#searchDiv, #gflexBody").hide();
			$('#gflexResponse').show();
			$('#gflexBody').hide();
			dataProcessEnd();

		}
	});
};

function cancelAddMsisdn() {

	$("#searchDiv").show();
	// $('#gflexBody').empty();
	$('#gflexContents').hide();
	$("#searchMsisdn").click();
};

function getGflexReports() {
	dataProcessStart();
	var searchUrl = Constants.CONTEXT_PATH + Constants.GLFEX
			+ Constants.GLFEX_REPORT;

	$.ajax({
		url : searchUrl,
		data : "showData",
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			// $("#gflexResponseBody").html(xhr.status + ":" + thrownError);
			displayErroGrid();

		},
		success : function(result) {
			showReport(result);

		}
	});

}

function showReport(result) {
	
	if (result.applicationError){
		displayInternalErroGrid(result.applicationError.errorCode);
		return ;
	}

	var html = "<table  data-toggle='table' class= 'display' data-height='180' id='gflexReport'><thead><tr><th>MSISDN</th><th>LAB</th><th>OPERATION</th><th>Operation Date</th><th>Status</th><th>Delete Date</th></tr></thead> <tbody>";

	for ( var key in result) {
		if (result.hasOwnProperty('gflexDetailsList')
				&& key == 'gflexDetailsList') {
			for ( var prop in result.gflexDetailsList) {
				html += "<tr><td>" + result.gflexDetailsList[prop].msisdn
						+ "</td>";
				html += "<td>" + result.gflexDetailsList[prop].selValue
						+ "</td>";
				html += "<td>" + result.gflexDetailsList[prop].operartion
						+ "</td>";
				html += "<td>" + result.gflexDetailsList[prop].dateOfOperation
						+ "</td>";
				html += "<td>" + result.gflexDetailsList[prop].status + "</td>";
				html += "<td>" + result.gflexDetailsList[prop].deleteDate
						+ "</td>";
				html += "</tr>";
			}
		}

	}

	html += "</tbody></table>";
	/*
	 * var prevHtml = $("#gflexContents").html(); $('#gflexContents').empty();
	 * $("#gflexContents").append( "<div class='panel-heading'
	 * id='usdContentHeading'>Gflex Report</div>" + prevHtml);
	 */
	$('#gflexResponseBody').empty();
	$('#gflexResponseBody').html(html);
	$("#searchDiv").hide();
	$("#gflexResponse").show();
	$("#gflexContents").show();
	
	$('#gflexReport').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,

		//"columns" : [ null, null],
		"jQueryUI" : true,
	});
	dataProcessEnd();
};

function cleanUpData() {
	
	var searchJson = {};
	var fromDate = $("#fromDate").val();
	var toDate = $("#toDate").val();
	searchJson.fromDate = fromDate;
	searchJson.toDate = toDate;
// need to validate by bharat
	
	dataProcessStart();
	$.ajax({
		url : '/edat/gflex/cleanUp',
		data : JSON.stringify(searchJson),
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$("#gflexResponseBody").html(xhr.status + ":" + thrownError);

		},
		success : function(result) {
			//showResult(result);
			switch (result.status)
			{

			case "0" :
				$("#gflexResponseBody").html("Success... Responce code : "+ result.status);
				break;
			case "1001" :
				$("#gflexResponseBody").html("Internal error... Responce code : "+ result.status);
				break;
			case "1002" :
				$("#gflexResponseBody").html("Not connceted... Responce code : "+ result.status);
				break;
			case "1003" :
				$("#gflexResponseBody").html("Already connected... Responce code : "+ result.status);
				break;
			case "1004" :
				$("#gflexResponseBody").html("Parse failed... Responce code : "+ result.status);
				break;
			case "1005" :
				$("#gflexResponseBody").html("Write Unavail... Responce code : "+ result.status);
				break;
			case "1006" :
				$("#gflexResponseBody").html("No Write Permission... Responce code : "+ result.status);
				break;
			case "1007" :
				$("#gflexResponseBody").html("No Mate... Responce code : "+ result.status);
				break;
			case "1008" :
				$("#gflexResponseBody").html("Standby Side... Responce code : "+ result.status);
				break;
			case "1009" :
				$("#gflexResponseBody").html("No Active Txn... Responce code : "+ result.status);
				break;
			case "1010" :
				$("#gflexResponseBody").html("Active Txn... Responce code : "+ result.status);
				break;
			case "1011" :
				$("#gflexResponseBody").html("Write In Read Txn... Responce code : "+ result.status);
				break;
			case "1012" :
				$("#gflexResponseBody").html("Invalid Value... Responce code : "+ result.status);
				break;
			case "1013" :
				$("#gflexResponseBody").html("Not Found... Responce code : "+ result.status);
				break;
			case "1014" :
				$("#gflexResponseBody").html("Conflict Found... Responce code : "+ result.status);
				break;
			case "1015" :
				$("#gflexResponseBody").html("Item Exists... Responce code : "+ result.status);
				break;
			case "1016" :
				$("#gflexResponseBody").html("Partial Success... Responce code : "+ result.status);
				break;
			case "1017" :
				$("#gflexResponseBody").html("No Updates... Responce code : "+ result.status);
				break;
			case "1018" :
				$("#gflexResponseBody").html("Interrupted... Responce code : "+ result.status);
				break;
			case "1019" :
				$("#gflexResponseBody").html("Bad Args... Responce code : "+ result.status);
				break;
			case "1020" :
				$("#gflexResponseBody").html("Too Many Connections... Responce code : "+ result.status);
				break;
			case "1021" :
				$("#gflexResponseBody").html("Ne Not Found... Responce code : "+ result.status);
				break;
			case "1022" :
				$("#gflexResponseBody").html("Contains Subs... Responce code : "+ result.status);
				break;
			case "1023" :
				$("#gflexResponseBody").html("Unknown Version... Responce code : "+ result.status);
				break;
			case "1024" :
				$("#gflexResponseBody").html("Unused2... Responce code : "+ result.status);
				break;
			case "1025" :
				$("#gflexResponseBody").html("Unimplemented... Responce code : "+ result.status);
				break;
			case "1026" :
				$("#gflexResponseBody").html("Mate Busy... Responce code : "+ result.status);
				break;
			case "1027" :
				$("#gflexResponseBody").html("Imsi Dn Limit... Responce code : "+ result.status);
				break;
			case "1028" :
				$("#gflexResponseBody").html("Bad Import Cmd... Responce code : "+ result.status);
				break;
			case "1029" :
				$("#gflexResponseBody").html("Txn Too Big... Responce code : "+ result.status);
				break;
			case "1030" :
				$("#gflexResponseBody").html("Db Maint Reqd... Responce code : "+ result.status);
				break;
			case "1031" :
				$("#gflexResponseBody").html("Db Exception... Responce code : "+ result.status);
				break;
			case "1032" :
				$("#gflexResponseBody").html("Max Imsi Limit... Responce code : "+ result.status);
				break;
			case "1033" :
				$("#gflexResponseBody").html("Max Dn Limit... Responce code : "+ result.status);
				break;
			case "1034" :
				$("#gflexResponseBody").html("Max Dnblk Limit... Responce code : "+ result.status);
				break;
			case "1035" :
				$("#gflexResponseBody").html("Max Ne Limit... Responce code : "+ result.status);
				break;
			case "1036" :
				$("#gflexResponseBody").html("Replicating... Responce code : "+ result.status);
				break;
			case "1037" :
				$("#gflexResponseBody").html("Check Digit Error... Responce code : "+ result.status);
				break;
			case "1038" :
				$("#gflexResponseBody").html("Imsi Not Found... Responce code : "+ result.status);
				break;
			case "1039" :
				$("#gflexResponseBody").html("Imei Imsi Limit... Responce code : "+ result.status);
				break;
			case "1040" :
				$("#gflexResponseBody").html("Max Imei Limit... Responce code : "+ result.status);
				break;
			case "1041" :
				$("#gflexResponseBody").html("Max Imei Blk Limit... Responce code : "+ result.status);
				break;
			case "1042" :
				$("#gflexResponseBody").html("No List For Imei... Responce code : "+ result.status);
				break;
			case "1043" :
				$("#gflexResponseBody").html("Bad Switch In Abp... Responce code : "+ result.status);
				break;
			case "1044" :
				$("#gflexResponseBody").html("Sub Ne Limit... Responce code : "+ result.status);
				break;
			default :
				
				$("#gflexResponseBody").html("Node not reachable :");
			}
			
			$("#gflexContents, #cleanupDiv").show();
			$("#gflexResponse").show();
			
			$("#searchDiv").hide();
			$("#gflexBody").hide();
			
			dataProcessEnd();
		}
	});

}

function displayErroGrid(){
	
	$("#errorMainPannel").show();
	$('#ErrorResponseHeading').show();
	//$('#ErrorResponseHeading').empty();
	$('#ErrorResponseBody').show();
	
	$('#ErrorResponseBody').html("Node Not Found!!!");
	dataProcessEnd();
}


function dataProcessStart() {
	$('#loading').html('<img src="/edat/resources/images/process.gif"> Loading...');
}

function dataProcessEnd() {
	$('#loading').empty();
}


function validateDateForUpdateMsisdn()  
{  
var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
var supexpDatedate = $("#deleteDate").val();
if(supexpDatedate!="")
{
if(supexpDatedate.match(dateformat))  
{  
var opera1 = supexpDatedate.split('/');  
lopera1 = opera1.length;  
if (lopera1>1)  
{  
var pdate = supexpDatedate.split('/');  
}  

var mm  = parseInt(pdate[0]);  
var dd = parseInt(pdate[1]);  
var yy = parseInt(pdate[2]);  
// Create list of days of a month [assume there is no leap year by default]  
var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  
if (mm==1 || mm>2)  
{  
if (dd>ListofDays[mm-1])  
{  
alert('Invalid date format!');  
$('#deleteDate').focus(); 
return false;  
}  
}  
if (mm==2)  
{  
var lyear = false;  
if ( (!(yy % 4) && yy % 100) || !(yy % 400))   
{  
lyear = true;  
}  
if ((lyear==false) && (dd>=29))  
{  
alert('Invalid date format!'); 
$('#deleteDate').focus(); 
return false;  
}  
if ((lyear==true) && (dd>29))  
{  
alert('Invalid date format!');
$('#deleteDate').focus(); 
return false;  
}  
}  
}  
else  
{  
alert("Invalid date format!");  
$('#deleteDate').focus(); 
return false;  
}
}else
	{
	return true;
	}
return true;
} 

function validateDateForCleanupFromDate()  
{  
var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
var supexpDatedate = $("#fromDate").val();
if(supexpDatedate!="")
{
if(supexpDatedate.match(dateformat))  
{  
var opera1 = supexpDatedate.split('/');  
lopera1 = opera1.length;  
if (lopera1>1)  
{  
var pdate = supexpDatedate.split('/');  
}  

var mm  = parseInt(pdate[0]);  
var dd = parseInt(pdate[1]);  
var yy = parseInt(pdate[2]);  
// Create list of days of a month [assume there is no leap year by default]  
var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  
if (mm==1 || mm>2)  
{  
if (dd>ListofDays[mm-1])  
{  
alert('Invalid date format!');  
$('#fromDate').focus(); 
return false;  
}  
}  
if (mm==2)  
{  
var lyear = false;  
if ( (!(yy % 4) && yy % 100) || !(yy % 400))   
{  
lyear = true;  
}  
if ((lyear==false) && (dd>=29))  
{  
alert('Invalid date format!'); 
$('#fromDate').focus(); 
return false;  
}  
if ((lyear==true) && (dd>29))  
{  
alert('Invalid date format!');
$('#fromDate').focus(); 
return false;  
}  
}  
}  
else  
{  
alert("Invalid date format!");  
$('#fromDate').focus(); 
return false;  
}
}else
	{
	alert("Please enter From Date.!");  
	$('#fromDate').focus(); 
	return false;
	}
return true;
}  

function validateDateForCleanupToDate()  
{  
var dateformat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
var supexpDatedate = $("#toDate").val();
if(supexpDatedate!="")
{
if(supexpDatedate.match(dateformat))  
{  
var opera1 = supexpDatedate.split('/');  
lopera1 = opera1.length;  
if (lopera1>1)  
{  
var pdate = supexpDatedate.split('/');  
}  

var mm  = parseInt(pdate[0]);  
var dd = parseInt(pdate[1]);  
var yy = parseInt(pdate[2]);  
// Create list of days of a month [assume there is no leap year by default]  
var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  
if (mm==1 || mm>2)  
{  
if (dd>ListofDays[mm-1])  
{  
alert('Invalid date format!');  
$('#toDate').focus(); 
return false;  
}  
}  
if (mm==2)  
{  
var lyear = false;  
if ( (!(yy % 4) && yy % 100) || !(yy % 400))   
{  
lyear = true;  
}  
if ((lyear==false) && (dd>=29))  
{  
alert('Invalid date format!'); 
$('#toDate').focus(); 
return false;  
}  
if ((lyear==true) && (dd>29))  
{  
alert('Invalid date format!');
$('#toDate').focus(); 
return false;  
}  
}  
}  
else  
{  
alert("Invalid date format!");  
$('#toDate').focus(); 
return false;  
}
}else
	{
	
	alert("Please enter To Date.!");  
	$('#toDate').focus(); 
	return false;
	}
return true;
}  

