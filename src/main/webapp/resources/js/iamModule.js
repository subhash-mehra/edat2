var iam_scopes = null;
var iam_grantType = null;
var clientIDs_Data = [];

$(document).ready(function(){
	$("#clientid_lbl").hide();
	
	$("#lock_type_value").keypress(function(event) {
        if (event.keyCode == 13) {
              $("#lock_account").click();
        }
  });
  
  $("#unlock_type_value").keypress(function(event) {
        if (event.keyCode == 13) {
              $("#unlock_account").click();
        }
  });

  $("#lock_account").click(
		function() {
			if ($.trim($("#lock_type_value").val()) == '') {
				alert("Lock Type value should not be null!");
				return;
			} else {
				var element = $("#searchTypeLock").val().toLowerCase();
				var flag = validateInput(element, "lock_type_value");
				if (flag) {
					var value = $("#lock_type_value").val();
					lockUnlockAccount(element, value, 'lockType');
				}
			}
		});
  

  $("#unlock_account").click(
		function() {
			if ($.trim($("#unlock_type_value").val()) == '') {
				alert("Unlock Type value should not be null!");
				return;
			} else {
				var element = $("#searchTypeUnLock").val().toLowerCase();
				var flag = validateInput(element, "unlock_type_value");
				if (flag) {
					var value = $("#unlock_type_value").val();
					var isAccountLocked = $("#is_account_locked").val();
					lockUnlockAccount(element, value, 'unlockType', isAccountLocked);
				}
			}
		});
});

/**
 *   get SecretKey onChange on clientID selectInput type
 *  * @param dropBox
 */
function onSelectClientID(selectInput){
	var value = selectInput.value;
	var index = selectInput.selectedIndex;
	$(".iam_search_otherTxt").addClass("hideOthers");
	$(".iamSecretKeyLbl").addClass("hideOthers");
	$(".iamSecretKey").addClass("hideOthers");
	
	if(value == "Others"){
		$(".iam_search_otherTxt").removeClass("hideOthers");
		$(".iamSecretKeyLbl").removeClass("hideOthers");
		$(".iamSecretKey").removeClass("hideOthers");
		$(".iamSecretKey").val("");
	}
	else{
		$(".iamSecretKey").val(clientIDs_Data[index].secretKey);
	}
}

function getClientIDs(){
	var iamClientIdURL = "/edat/mdt/getClientIds";
	sendRequest(iamClientIdURL, Method.GET, null, getClientIDResultHandler, getClientIDErrorHandler, null);
}

function getClientIDResultHandler(result){
	clientIDs_Data = result;
	insertClientIDs(clientIDs_Data);
	$(".iamSecretKey").val(clientIDs_Data[0].secretKey);
}

function getClientIDErrorHandler(xhr, ajaxOptions, thrownError, callbackObj){
	errorMessage("Problem to get 'Client IDs'!");
}

function insertClientIDs(data){
	$(".searchTypeClientId:visible").empty();
	var len = data.length;
	for(var i=0; i<len; i++){
		$(".searchTypeClientId:visible").append("<option>"+data[i].clientId+"</option>");
	}
	$(".searchTypeClientId:visible").append("<option>Others</option>");
}

function onSelectScopes(element){
	iam_scopes = $(element).val();
}

function onSelectGrantType(element){
	iam_grantType = $(element).val();
}

function generateToken(){
	var tokenParams = getTokenParameters();
	if(tokenParams.error){
		errorMessage(tokenParams.error);
		return;
	}	
	getGeneratedToken(tokenParams);
}

function showAccessTokenData(data){
	var html = "<tr>";
	html += "<td>"+data.access_token+"</td>";
	html += "<td>"+data.token_type+"</td>";
	html += "<td>"+data.scope+"</td>";
	html += "<td>"+data.expires_in+"</td>";
	html += "</tr>";
	$("#accessToken_table").append(html);
}

function getGeneratedToken(tokenParams){
	var iamTokenGenerateURL = "/edat/token/tokenGenerator";
	var controllerBody = {
		"tokenParams" : tokenParams,
	};
	sendRequest(iamTokenGenerateURL, Method.POST, controllerBody, tokenGenResultHandler, tokenGenErrorHandler, null);
}

function tokenGenResultHandler(data){
	var active_tab = $("#iam-sub-menu li.selected")[0].id;
	switch(active_tab){
		case "iam_search":
			break;
		case "iam_token":
			showAccessTokenData(data);
			break;
		case "iam_link":
			break;
		case "iam_unlink":
			break;
	}
}

function tokenGenErrorHandler(xhr, ajaxOptions, thrownError){

	errorMessage("Problem to get 'Token Generation'!");
}

function lockUnlockAccount(element, value, type, isAccountLocked){
      var url = null;
      var urlParams = null;
      var profile = "";
      
      if(element == "email")
    	  profile = "mailto:";     
      else if(element == "username")
    	  profile = "uname:";
      else if(element == "uid")
    	  profile = "uid:";
      else if(element == "msisdn")
    	  profile = "tel:";
      
      var tokenParams = getTokenParameters();
  	  if(tokenParams.error){
  		  errorMessage(tokenParams.error);
  		  return;
  	  }
  	  switch(type){
  	  	case "lockType":
              url = "/edat/mdt/lockAccount";
              urlParams = {"user_id" : profile + value};
              break;
        case "unlockType":
              url = "/edat/mdt/unLockAccount";                  
              urlParams = {"user_id" : profile + value, "isAccountLocked" : isAccountLocked};
              break;
      }
  	  var callbackObj = {"type" : type};
  	  getLockUnlockTypeData(url, urlParams, tokenParams, callbackObj);
}

function getLockUnlockTypeData(url, urlParams, tokenParams, callbackObj){
	var iamLockUnlockTypeURL = url;
	var controllerBody = {
			"urlParams" : urlParams,
			"tokenParams" : tokenParams
	}

	sendRequest(iamLockUnlockTypeURL, Method.POST, controllerBody, lockUnlockResultHandler, lockUnlockErrorHandler, callbackObj);
}

function lockUnlockResultHandler(result, callbackObj){
	if(callbackObj.type == "lockType")
		showLockTypeData(result);
	else
		errorMessage(result.response);
}

function lockUnlockErrorHandler(xhr, ajaxOptions, thrownError, callbackObj){
	if(callbackObj.type == "locktype")
		errorMessage("Problem to get 'Lock Type' data!");
	else
		errorMessage("Problem to get 'UnLock Type' data!");
}


function lockUnlockResultHandler(result, callbackObj){
	if(callbackObj.type == 'lockType')
		showLockTypeData(result);
	else
		alert(result.statusMessage);
}

function lockUnlockErrorHandler(xhr, ajaxOptions, thrownError, callbackObj){
	alert("Problem to get Lock Type Data!");
}

function showLockTypeData(data){
      var html = "<tr>";
      html += "<td>"+data.profile_type+"</td>";
      html += "</tr>";
      $("#accessLock_table").append(html);
}

/**
 * to show/hide the iam Private Vault offer id input box
 * 
 * @param dropBox
 */
function getPrivateVaultOfferId(dropBox) {
	var value = dropBox.value;
	switch (value) {
	case "alloffers":
		$("#privacy_vault_mdt_search").addClass("hideOthers");
		break;
	case "offerIds":
		$("#privacy_vault_mdt_search").removeClass("hideOthers");
		break;
	default:
		break;
	}
}

/**
 * iam privacy vault offer search
 */
function offerSearchPrivacyVault() {
	var iamPrivacyVaultControllerUrl = "/edat/mdt/iamPrivacyVault";
	var urlParams = {};
	var tokenParams = getTokenParameters();
	if(tokenParams.error){
		errorMessage(tokenParams.error);
		return;
	}
	var searchTypeVal = ($("#privacy_vault_search_type").val()).trim();
	switch (searchTypeVal) {
	case "alloffers":
		break;
	case "offerIds":
		var offerID = $("#privacy_vault_mdt_search").val();
		if (offerID.trim().length == 0) {
			alert('Please enter the Offer ID');
			return;
		}
		urlParams = {
			"offerIds" : offerID
		};
		break;
	default:
		break;
	}
	var controllerBody = {
		"urlParams" : urlParams,
		"tokenParams" : tokenParams,
	};

	sendRequest(iamPrivacyVaultControllerUrl, Method.POST, controllerBody, offerResultHandler, offerErrorHandler, null);
}

/*
 * iam profile link
 */
function linkProfile() {
	var iamLinkProfileControllerUrl = "/edat/mdt/linkmsisdn";
	var tokenParams = getTokenParameters();
	var search_type = $("#searchTypeLink").val();
	var is_valid_input = validateInput(search_type, "msisdnLinkInput");
	if (is_valid_input) {
		var msisdn = $("#msisdnLinkInput").val();
		var smsPinParams = {
			"msisdn" : msisdn
		};
		var useid_type = $("#scopeList").val();
		var userid = $("#userIdInput").val();
		if (userid.trim().length == 0) {
			alert('Please enter the User ID');
			return;
		}
		switch (useid_type) {
		case "Email":
			userid = "mailto:" + userid;
			break;
		case "UID":
			userid = "uid:" + userid;
			break;
		case "User":
			userid = "uname:" + userid;
			break;
		default:
			break;
		}
		var urlParams = {
			"msisdn" : msisdn,
			"user_id" : userid,
			"sms_pin" : ""
		};

		var controllerBody = {
			"urlParams" : urlParams,
			"tokenParams" : tokenParams,
			"smsPinParams" : smsPinParams
		};
		sendRequest(iamLinkProfileControllerUrl, controllerBody, function(
				result) {
			console.log(result);
			if (result.statusCode == "200") {
				resetFieldInputs();
				alert('Profile linked successfully');
			} else {
				showErrorMessage(result);
			}

		});
	}
}

/*
 * iam profile unlink
 */

function unLinkProfile() {
	var iamUnlinkProfileControllerUrl = "/edat/mdt/unlinkmsisdn";
	var tokenParams = getTokenParameters();
	var controllerBody = {};
	var userIdUrlParams = {};
	var search_type = $("#searchTypeUnlink").val();
	var is_valid_input = validateInput(search_type, "msisdnUnlinkInput");
	if (is_valid_input) {
		var urlParams = {};
		var msisdn = $("#msisdnUnlinkInput").val();
		var userid_val = $("#userIdInputUnlink").val();
		var user_id_param = null;
		var user_id = null;
		if (userid_val.trim().length == 0) {
			alert('Please enter the User ID');
			return;
		}
		var useid_type = $("#scopeListUnlink").val();
		switch (useid_type) {
		case "Email":
			userIdUrlParams = {
				"UID" : "email:" + userid_val
			};
			break;
		case "UID":
			user_id = "uid:" + userid_val;
			break;
		case "User":
			userIdUrlParams = {
				"UID" : "userName:" + userid_val
			};
			break;
		default:
			break;
		}
		var revoke_permisson_value = $("#revokePermissonValue").val();
		urlParams = {
			"msisdn" : msisdn,
			"user_id" : user_id,
			"revokePermission" : revoke_permisson_value
		};

		controllerBody = {
			"urlParams" : urlParams,
			"tokenParams" : tokenParams,
			"userIdUrlParams" : userIdUrlParams
		};

		sendRequest(iamUnlinkProfileControllerUrl, controllerBody, function(
				result) {
			console.log(result);
			if (result.statusCode == "200") {
				resetFieldInputs();
				alert('Profile linked successfully');
			} else {
				showErrorMessage(result);
			}
		});
	}
}

function offerResultHandler(result){
	// TODO offerResultHandler
}

function offerErrorHandler(xhr, ajaxOptions, thrownError, callbackObj){
	errorMessage("Problem to get 'Privacy Vault' data!");
}