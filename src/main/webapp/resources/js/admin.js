
function loginForValidUser()
{
	
	
		document.validUser.method = "get";
		document.validUser.action = "logout";
		document.forms["validUser"].submit();

}

function OnSubmitFormForShowUpdateJsp(val) {
		
		if (val == 'save') {
			document.adminTemplate.method = "post";
			document.adminTemplate.action = "showUser";
			document.forms["adminTemplate"].submit();
			
		} else if (val == 'update') {
			
			document.adminTemplate.method = "post";
			document.adminTemplate.action = "showUpdateUser";
			document.forms["adminTemplate"].submit();
					}
		else if (val == 'report') {
			document.adminTemplate.method = "post";
			document.adminTemplate.action = "showReports";
			document.forms["adminTemplate"].submit();
					}
		else if (val == 'server_config') {
			document.adminTemplate.method = "post";
			document.adminTemplate.action = "changeServerConfig";
			document.forms["adminTemplate"].submit();
					}
		else if (val == 'executeQuery') {
			document.adminTemplate.method = "post";
			document.adminTemplate.action = "executeQuery";
			document.forms["adminTemplate"].submit();
					}
		 
	}


function showUpdateUser(val,id) {
	
	
	if (val == 'edit') {
	
		document.userRequest.action = "updateExistUser?userId="+id;
		
	} else if (val == 'delete') {
		var r = confirm("Do you want to delete user");
		if (r == true) {
			document.userRequest.action = "deleteUser?userId="+id;
		} else {
		    return false;
		}
		
				}
	return true; 
}


function goToModule(val)
{
	var csrf=document.getElementById("csrfPreventionSalt1").value;
	if(val=='admin')
	{
		document.header.method = "get";
		document.header.action = "showAdminTemplate?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();
		
	}
	
	if(val=='logout')
	{
		
		document.header.method = "get";
		document.header.action = "logout?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();
		
	}
	
	if(val=='dus')
	{		
		document.header.method = "get";
		document.header.action = "dusupdate?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
	
	if(val=='cntb')
	{		
		document.header.method = "get";
		document.header.action = "cntb?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
	
	if(val=='msg')
	{		
		document.header.method = "get";
		document.header.action = "mdt?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
	
	if(val=='usd')
	{		
		document.header.method = "get";
		document.header.action = "usd?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
	
	if(val=='gflex')
	{		
		document.header.method = "get";
		document.header.action = "gflex?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
	
	if(val=='inSdp')
	{		
		document.header.method = "get";
		document.header.action = "insdp?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
		
	if(val=='otaTool')
	{		
		document.header.method = "get";
		document.header.action = "ota?csrfPreventionSalt="+csrf;
		document.forms["header"].submit();		
	}
	
		
	return false;
}

function showPasswordWindow(val)
{
	var csrf=document.getElementById("csrfPreventionSalt1").value;
	if (val == 'password')
	{
		document.header.method = "POST";		
		window.open("showPasswordPopup?csrfPreventionSalt="+csrf, "formresult", "toolbar=no, scrollbars=yes, top=100, left=200, width=700, height=400");
	}
	
	return;
}

function showLoginHelp()
{
		
		window.open("resources/helpDoc/loginHelp.jsp", "formresult", "titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no, top=100, left=200, width=800, height=420");
	
	
	
}

function validatePassword()
{
var newPassword=document.getElementById('newPassword').value;
var confirmNewPwd=document.getElementById('newPasswordConfirm').value;
if(newPassword==confirmNewPwd)
{
	document.userRequest.action = "changePassword";
	return true; 
}else
	{
	alert("You have incorrect New Confirm Password");
	document.getElementById('newPasswordConfirm').focus;
	return false;
	}
}



function validateUserRegisterForm() { 
      var errorMessage = "Please complete the following fields:"; 
      var errorElements = new Array(); 
      var node; 
      var result; 
      var node_focus = null; 
      var pwd=null;
      var c_pwd=null;
      // check required fields 
       
      // Check first name 
      var node = document.getElementById("fname"); 
      if( node && node.value == "" ) { 
          errorElements.push("You must enter your first name"); 
       
          node_focus = node; 
       } 

      // Check last name 
      node = document.getElementById("lname"); 
      if( node && node.value == "" ) { 
          errorElements.push("You must enter your last name"); 
           
          if( node_focus == null ) 
             node_focus = node; 
       } 
       
      // Check Login Name 
      node = document.getElementById("loginname"); 
      if( node  && node.value == "") { 

            errorElements.push("You must enter a login name"); 
             
           if( node_focus == null ) 
               node_focus = node; 
               
      }
      
      //check Password
      node = document.getElementById("pwd"); 
      if( node  && node.value == "") { 

            errorElements.push("You must enter a password"); 
             
           if( node_focus == null ) 
               node_focus = node; 
               
      }else{
    	  pwd = node.value;
      }
      node = document.getElementById("c_pwd"); 
      if( node  && node.value == "") { 

            errorElements.push("You must enter a confirm password"); 
             
           if( node_focus == null ) 
               node_focus = node; 
               
      }else{
    	  c_pwd = node.value;
      }
      
    //check Password
      node = document.getElementById("addUserRoleSelector"); 
      if( node  &&  node.options.selectedIndex == "0") { 

            errorElements.push("You must select role"); 
             
           if( node_focus == null ) 
               node_focus = node; 
               
      }
      
      // endif 

     
   first_name_error = true; 
   last_name_error = true;   
   phone_error = true;   

   if( errorElements.length > 0 ) { 
   
        for(i=0; i < errorElements.length; i++ ) { 
         
           errorMessage += "\n" + errorElements[i]; 
           
        }  // end for 
   
        alert(errorMessage); 
   
         if( node_focus ) 
            node_focus.focus(); 
             
        return false; 
   } else { 
        // supress submit for test; for a real form, return true 
	   if(pwd!='' && c_pwd!='' && pwd!=c_pwd){
	    	  alert("Password not match !!");  
	    	  document.getElementById("c_pwd").focus();
	    	  return false;
	      }else{
	   return true;
	      }
   } 
   
return true;
} 


function validateUserUpdateForm() { 
    var errorMessage = "Please complete the following fields:"; 
    var errorElements = new Array(); 
    var node; 
    var result; 
    var node_focus = null; 
    var pwd = null;
    var c_pwd = null;
    // check required fields 
     
    // Check first name 
    var node = document.getElementById("fname"); 
    if( node && node.value == "" ) { 
        errorElements.push("You must enter your first name"); 
     
        node_focus = node; 
     } 

    // Check last name 
    node = document.getElementById("lname"); 
    if( node && node.value == "" ) { 
        errorElements.push("You must enter your last name"); 
         
        if( node_focus == null ) 
           node_focus = node; 
     } 
     
    // Check Login Name 
    node = document.getElementById("loginname"); 
    if( node  && node.value == "") { 

          errorElements.push("You must enter a login name"); 
           
         if( node_focus == null ) 
             node_focus = node; 
             
    }
    
    /*//check Password
    node = document.getElementById("pwd"); 
    if( node  && node.value == "") { 

          errorElements.push("You must enter a password"); 
           
         if( node_focus == null ) 
             node_focus = node; 
             
    }
    */
    
    //check Password
    node = document.getElementById("pwd"); 
    if( node  && node.value == "") { 

          errorElements.push("You must enter a password"); 
           
         if( node_focus == null ) 
             node_focus = node; 
             
    }else{
  	  pwd = node.value;
    }
    node = document.getElementById("c_pwd"); 
    if( node  && node.value == "") { 

          errorElements.push("You must enter a confirm password"); 
           
         if( node_focus == null ) 
             node_focus = node; 
             
    }else{
  	  c_pwd = node.value;
    }
    node = document.getElementById("addUserRoleSelector"); 
    if( node   && node.options.selectedIndex == "0") { 

          errorElements.push("You must select role"); 
           
         if( node_focus == null ) 
             node_focus = node; 
             
    }
    
    // endif 


 first_name_error = true; 
 last_name_error = true;   
 phone_error = true;   

 if( errorElements.length > 0 ) { 
 
      for(i=0; i < errorElements.length; i++ ) { 
       
         errorMessage += "\n" + errorElements[i]; 
         
      }  // end for 
 
      alert(errorMessage); 
 
       if( node_focus ) 
          node_focus.focus(); 
           
      return false; 
 } else { 
      // supress submit for test; for a real form, return true 
	  if(pwd!='' && c_pwd!='' && pwd!=c_pwd){
    	  alert("Password not match !!");  
    	  document.getElementById("c_pwd").focus();
    	  return false;
      }else{
   return true;
      }
 } 
 
return true;
}



function validatePasswordChange() { 
    var errorMessage = "Please complete the following fields:"; 
    var errorElements = new Array(); 
    var node; 
    var result; 
    var node_focus = null; 
    // check required fields 
     
    // Check first name 
    var node = document.getElementById("oldPassword"); 
    if( node && node.value == "" ) { 
        errorElements.push("You must enter old password"); 
     
        node_focus = node; 
     } 

    // Check last name 
    node = document.getElementById("newPassword"); 
    if( node && node.value == "" ) { 
        errorElements.push("You must enter new password"); 
         
        if( node_focus == null ) 
           node_focus = node; 
     } 
     
    // Check Login Name 
    node = document.getElementById("newPasswordConfirm"); 
    if( node  && node.value == "") { 

          errorElements.push("You must enter cinfirm password"); 
           
         if( node_focus == null ) 
             node_focus = node; 
             
    }
    
  
 first_name_error = true; 
 last_name_error = true;   
 phone_error = true;   

 if( errorElements.length > 0 ) { 
 
      for(i=0; i < errorElements.length; i++ ) { 
       
         errorMessage += "\n" + errorElements[i]; 
         
      }  // end for 
 
      alert(errorMessage); 
 
       if( node_focus ) 
          node_focus.focus(); 
           
      return false; 
 } else { 
      // supress submit for test; for a real form, return true 
	   return true;
 } 
 
return true;
}


function CharacterCount(TextArea,FieldToCount){
	var myField = document.getElementById(TextArea);
	var myLabel = document.getElementById(FieldToCount); 
	if(!myField || !myLabel){return false}; // catches errors
	var MaxChars =  myField.maxLengh;
	if(!MaxChars){MaxChars =  myField.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};
	var remainingChars =   MaxChars - myField.value.length
	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars
}
var htmlData="";
var total=0;
var loginIdTemp="";
var counter =0;
var tableRowsLength=0;
var subResult = "";
function showReport(loginId,result,time,finalCall,id) {	
	var result1 = result;
	result = '{"ModuleLink" : ['+result+']}';
	var obj = JSON.parse(result);
	var	addEir;
	var	unlockEir;
	var	searchTac;
	var	addtac;
	var	enableMMS;
	
	if(obj.ModuleLink[0].addEir==undefined || obj.ModuleLink[0].addEir=="NaN")
	{
		addEir=0;
	}else
	{
		addEir=obj.ModuleLink[0].addEir;
	}
	if(obj.ModuleLink[0].unlockEir==undefined || obj.ModuleLink[0].unlockEir=="NaN")
	{
		unlockEir=0;
	}else
	{
		unlockEir=obj.ModuleLink[0].unlockEir;
	}
	if(obj.ModuleLink[0].searchTac==undefined || obj.ModuleLink[0].searchTac=="NaN")
	{
		searchTac=0;
	}else
	{
		searchTac=obj.ModuleLink[0].searchTac;
	}
	if(obj.ModuleLink[0].addtac==undefined || obj.ModuleLink[0].addtac=="NaN")
	{
		addtac=0;
	}else
	{
		addtac=obj.ModuleLink[0].addtac;
	}
	if(obj.ModuleLink[0].enableMMS==undefined || obj.ModuleLink[0].enableMMS=="NaN")
	{
		enableMMS=0;
	}else
	{
		enableMMS=obj.ModuleLink[0].enableMMS;
	}
	
	total=total+obj.ModuleLink[0].nap + obj.ModuleLink[0].dus + obj.ModuleLink[0].pcrf + obj.ModuleLink[0].uma + obj.ModuleLink[0].hss + obj.ModuleLink[0].hlr + obj.ModuleLink[0].eir + addEir + unlockEir + searchTac + addtac + enableMMS + obj.ModuleLink[0].cntb + obj.ModuleLink[0].cntbDBSearch + obj.ModuleLink[0].msisdn + obj.ModuleLink[0].cntbDBAdd + obj.ModuleLink[0].existAdd + obj.ModuleLink[0].cntbDBDelete + obj.ModuleLink[0].existDelete + obj.ModuleLink[0].mdt + obj.ModuleLink[0].smsc + obj.ModuleLink[0].iam + obj.ModuleLink[0].dusupdate + obj.ModuleLink[0].gflex + obj.ModuleLink[0].search + obj.ModuleLink[0].add + obj.ModuleLink[0].update + obj.ModuleLink[0].delete_ + obj.ModuleLink[0].gFlexReports + obj.ModuleLink[0].insdp + obj.ModuleLink[0].BalanceAndDate + obj.ModuleLink[0].UpdateService + obj.ModuleLink[0].UpdateBalanceAndDate + obj.ModuleLink[0].InstallSubscriber + obj.ModuleLink[0].UpdateAccumulators + obj.ModuleLink[0].UpdateSubscriber + obj.ModuleLink[0].ActivateSubscriber + obj.ModuleLink[0].DeleteSubscriber + obj.ModuleLink[0].showAdminTemplate + obj.ModuleLink[0].saveUser + obj.ModuleLink[0].showUpdateUser  +  obj.ModuleLink[0].updateUser +  obj.ModuleLink[0].otaSearch +  obj.ModuleLink[0].cleanUp;
	if(loginIdTemp!=loginId){
		if(loginIdTemp==""){
			counter = (obj.ModuleLink[0].nap + obj.ModuleLink[0].dus + obj.ModuleLink[0].pcrf + obj.ModuleLink[0].uma + obj.ModuleLink[0].hss + obj.ModuleLink[0].hlr + obj.ModuleLink[0].eir + addEir + unlockEir + searchTac + addtac + enableMMS + obj.ModuleLink[0].cntb + obj.ModuleLink[0].cntbDBSearch + obj.ModuleLink[0].msisdn + obj.ModuleLink[0].cntbDBAdd + obj.ModuleLink[0].existAdd + obj.ModuleLink[0].cntbDBDelete + obj.ModuleLink[0].existDelete + obj.ModuleLink[0].mdt + obj.ModuleLink[0].smsc + obj.ModuleLink[0].iam + obj.ModuleLink[0].dusupdate + obj.ModuleLink[0].gflex + obj.ModuleLink[0].search + obj.ModuleLink[0].add + obj.ModuleLink[0].update + obj.ModuleLink[0].delete_ + obj.ModuleLink[0].gFlexReports + obj.ModuleLink[0].insdp + obj.ModuleLink[0].BalanceAndDate + obj.ModuleLink[0].UpdateService + obj.ModuleLink[0].UpdateBalanceAndDate + obj.ModuleLink[0].InstallSubscriber + obj.ModuleLink[0].UpdateAccumulators + obj.ModuleLink[0].UpdateSubscriber + obj.ModuleLink[0].ActivateSubscriber + obj.ModuleLink[0].DeleteSubscriber + obj.ModuleLink[0].showAdminTemplate + obj.ModuleLink[0].saveUser + obj.ModuleLink[0].showUpdateUser  +  obj.ModuleLink[0].updateUser +  obj.ModuleLink[0].otaSearch +  obj.ModuleLink[0].cleanUp);
			subResult = result1;
		}
		if(loginIdTemp!=""){
			subResult =	'{"ModuleLink" : ['+subResult+']}';
			tableRowsLength=tableRowsLength+1;
	htmlData += "<tr><td><a href='javascript:showReportDetails("+id+","+subResult+");'>" + loginIdTemp
						+ "</a></td>";
	htmlData += "<td>" + counter
						+ "</td>";
	htmlData += "</tr>";			
	htmlData+="<tr style='display:none;' id=\"showReportDetails_"+id+"\"><td colspan=3 id=\"reportDetails_"+id+"\"></td></tr>";
		}
		loginIdTemp = loginId;
		subResult = result1;
		counter = (obj.ModuleLink[0].nap + obj.ModuleLink[0].dus + obj.ModuleLink[0].pcrf + obj.ModuleLink[0].uma + obj.ModuleLink[0].hss + obj.ModuleLink[0].hlr + obj.ModuleLink[0].eir + addEir + unlockEir + searchTac + addtac + enableMMS + obj.ModuleLink[0].cntb + obj.ModuleLink[0].cntbDBSearch + obj.ModuleLink[0].msisdn + obj.ModuleLink[0].cntbDBAdd + obj.ModuleLink[0].existAdd + obj.ModuleLink[0].cntbDBDelete + obj.ModuleLink[0].existDelete + obj.ModuleLink[0].mdt + obj.ModuleLink[0].smsc + obj.ModuleLink[0].iam + obj.ModuleLink[0].dusupdate + obj.ModuleLink[0].gflex + obj.ModuleLink[0].search + obj.ModuleLink[0].add + obj.ModuleLink[0].update + obj.ModuleLink[0].delete_ + obj.ModuleLink[0].gFlexReports + obj.ModuleLink[0].insdp + obj.ModuleLink[0].BalanceAndDate + obj.ModuleLink[0].UpdateService + obj.ModuleLink[0].UpdateBalanceAndDate + obj.ModuleLink[0].InstallSubscriber + obj.ModuleLink[0].UpdateAccumulators + obj.ModuleLink[0].UpdateSubscriber + obj.ModuleLink[0].ActivateSubscriber + obj.ModuleLink[0].DeleteSubscriber + obj.ModuleLink[0].showAdminTemplate + obj.ModuleLink[0].saveUser + obj.ModuleLink[0].showUpdateUser  +  obj.ModuleLink[0].updateUser +  obj.ModuleLink[0].otaSearch +  obj.ModuleLink[0].cleanUp);
	}else{
		if(subResult==""){
			subResult = result1;
			}else{
				subResult = subResult +", "+ result1;
			}
		counter = counter +  (obj.ModuleLink[0].nap + obj.ModuleLink[0].dus + obj.ModuleLink[0].pcrf + obj.ModuleLink[0].uma + obj.ModuleLink[0].hss + obj.ModuleLink[0].hlr + obj.ModuleLink[0].eir + addEir + unlockEir + searchTac + addtac + enableMMS + obj.ModuleLink[0].cntb + obj.ModuleLink[0].cntbDBSearch + obj.ModuleLink[0].msisdn + obj.ModuleLink[0].cntbDBAdd + obj.ModuleLink[0].existAdd + obj.ModuleLink[0].cntbDBDelete + obj.ModuleLink[0].existDelete + obj.ModuleLink[0].mdt + obj.ModuleLink[0].smsc + obj.ModuleLink[0].iam + obj.ModuleLink[0].dusupdate + obj.ModuleLink[0].gflex + obj.ModuleLink[0].search + obj.ModuleLink[0].add + obj.ModuleLink[0].update + obj.ModuleLink[0].delete_ + obj.ModuleLink[0].gFlexReports + obj.ModuleLink[0].insdp + obj.ModuleLink[0].BalanceAndDate + obj.ModuleLink[0].UpdateService + obj.ModuleLink[0].UpdateBalanceAndDate + obj.ModuleLink[0].InstallSubscriber + obj.ModuleLink[0].UpdateAccumulators + obj.ModuleLink[0].UpdateSubscriber + obj.ModuleLink[0].ActivateSubscriber + obj.ModuleLink[0].DeleteSubscriber + obj.ModuleLink[0].showAdminTemplate + obj.ModuleLink[0].saveUser + obj.ModuleLink[0].showUpdateUser  +  obj.ModuleLink[0].updateUser +  obj.ModuleLink[0].otaSearch +  obj.ModuleLink[0].cleanUp);
	}
	
	if(finalCall=='true'){
		$("#tableRowsLength").val(tableRowsLength);
		//if(htmlData==""){
			subResult =	'{"ModuleLink" : ['+subResult+']}';
			htmlData += "<tr><td><a href='javascript:showReportDetails("+id+","+subResult+");'>" + loginIdTemp
								+ "</a></td>";
			htmlData += "<td>" + counter
								+ "</td>";
			htmlData += "</tr>";			
			htmlData+="<tr style='display:none;' id=\"showReportDetails_"+id+"\"><td colspan=3 id=\"reportDetails_"+id+"\"></td></tr>";
		//		}
		htmlData = "<table id=results class='table table-striped '><tr><th>Login Id</th><th>Total Hit</th></tr>"+htmlData+"</table>";
		
		$('#showReport').empty();
		$('#showReport').html(htmlData);
		
		htmlData="";
		$('#totalReport').text("Total Hits:"+total);
		total=0;
		counter = 0;
		loginIdTemp="";
		tableRowsLength=0;
		subResult = "";
	}	

}
function showReportDetails(id,obj) {	
	var objId="showReportDetails_"+id;
	var totalUsd="";
	var html ="";
	var objId1="reportDetails_"+id;
	if($('#'+objId).is(':hidden')) {
	for(i = 0; i < obj.ModuleLink.length; i++) {
	totalUsd=obj.ModuleLink[i].nap + obj.ModuleLink[i].dus + obj.ModuleLink[i].pcrf + obj.ModuleLink[i].uma + obj.ModuleLink[i].hss + obj.ModuleLink[i].hlr + obj.ModuleLink[i].eir;
	html += "<tr><td>USD</td>";
	html += "<td>"+totalUsd+"</td></tr>";
	html += "<tr><td>Add EIR</td>";
	if(obj.ModuleLink[i].addEir==undefined || obj.ModuleLink[i].addEir=="NaN")
	{
	html += "<td>"+0+"</td></tr>";
	} else
	{
	html += "<td>"+obj.ModuleLink[i].addEir+"</td></tr>";
	}

	html += "<tr><td>Unlock EIR</td>";
	if(obj.ModuleLink[i].unlockEir==undefined || obj.ModuleLink[i].unlockEir=="NaN")
	{
	html += "<td>"+0+"</td></tr>";
	} else
	{
	html += "<td>"+obj.ModuleLink[i].unlockEir+"</td></tr>";
	}
	
	html += "<tr><td>Search TAC</td>";
	if(obj.ModuleLink[i].searchTac==undefined || obj.ModuleLink[i].searchTac=="NaN")
	{
	html += "<td>"+0+"</td></tr>";
	} else
	{
	html += "<td>"+obj.ModuleLink[i].searchTac+"</td></tr>";
	}
	
	html += "<tr><td>Add TAC</td>";
	if(obj.ModuleLink[i].addtac==undefined || obj.ModuleLink[i].addtac=="NaN")
	{
	html += "<td>"+0+"</td></tr>";
	} else
	{
	html += "<td>"+obj.ModuleLink[i].addtac+"</td></tr>";
	}
	
	html += "<tr><td>Enable MMS</td>";
	if(obj.ModuleLink[i].enableMMS==undefined || obj.ModuleLink[i].enableMMS=="NaN")
	{
	html += "<td>"+0+"</td></tr>";
	} else
	{
	html += "<td>"+obj.ModuleLink[i].enableMMS+"</td></tr>";
	}
		
	html += "<tr><td>CNTDB</td>";
	html += "<td>"+obj.ModuleLink[i].cntb+"</td></tr>";	
	html += "<tr><td>CNTDB Search</td>";
	html += "<td>"+(obj.ModuleLink[i].cntbDBSearch + obj.ModuleLink[i].msisdn)+"</td></tr>";
	html += "<tr><td>CNTDB Add</td>";
	html += "<td>"+(obj.ModuleLink[i].cntbDBAdd + obj.ModuleLink[i].existAdd)+"</td></tr>";
	html += "<tr><td>CNTDB Delete</td>";
	html += "<td>"+(obj.ModuleLink[i].cntbDBDelete + obj.ModuleLink[i].existDelete)+"</td></tr>";
	
	
	html += "<tr><td>Messaging Data</td>";
	html += "<td>"+obj.ModuleLink[i].mdt+"</td></tr>";	
	html += "<tr><td>SMSC-Aci / MMSC</td>";
	html += "<td>"+obj.ModuleLink[i].smsc+"</td></tr>";
	html += "<tr><td>IAM</td>";
	html += "<td>"+obj.ModuleLink[i].iam+"</td></tr>";
	
	
	html += "<tr><td>Update DUS</td>";
	html += "<td>"+obj.ModuleLink[i].dusupdate+"</td></tr>";
	
	
	html += "<tr><td>GFlex Tools</td>";
	html += "<td>"+obj.ModuleLink[i].gflex+"</td></tr>";
	html += "<tr><td>Search MSISDN</td>";
	html += "<td>"+obj.ModuleLink[i].search+"</td></tr>";
	html += "<tr><td>Add MSISDN</td>";
	html += "<td>"+obj.ModuleLink[i].add+"</td></tr>";
	html += "<tr><td>Update MSISDN</td>";
	html += "<td>"+obj.ModuleLink[i].update+"</td></tr>";
	html += "<tr><td>Delete MSISDN</td>";
	html += "<td>"+obj.ModuleLink[i].delete_+"</td></tr>";
	html += "<tr><td>Clean-Up</td>";
	html += "<td>"+obj.ModuleLink[i].cleanUp+"</td></tr>";
	html += "<tr><td>Reports</td>";
	html += "<td>"+obj.ModuleLink[i].gFlexReports+"</td></tr>";	
	
	html += "<tr><td>In/SDP Data</td>";
	html += "<td>"+obj.ModuleLink[i].insdp+"</td></tr>";
	html += "<tr><td>Balance & Date</td>";
	html += "<td>"+obj.ModuleLink[i].BalanceAndDate+"</td></tr>";
	html += "<tr><td>Update Service</td>";
	html += "<td>"+obj.ModuleLink[i].UpdateService+"</td></tr>";
	html += "<tr><td>Update Balance</td>";
	html += "<td>"+obj.ModuleLink[i].UpdateBalanceAndDate+"</td></tr>";
	html += "<tr><td>Create Subscriber</td>";
	html += "<td>"+obj.ModuleLink[i].InstallSubscriber+"</td></tr>";
	html += "<tr><td>Update Accumulator</td>";
	html += "<td>"+obj.ModuleLink[i].UpdateAccumulators+"</td></tr>";
	html += "<tr><td>Update Subscriber</td>";
	html += "<td>"+obj.ModuleLink[i].UpdateSubscriber+"</td></tr>";
	html += "<tr><td>Activate Subscriber</td>";
	html += "<td>"+obj.ModuleLink[i].ActivateSubscriber+"</td></tr>";
	html += "<tr><td>Delete Subscriber</td>";
	html += "<td>"+obj.ModuleLink[i].DeleteSubscriber+"</td></tr>";
	html += "<tr><td>OTA Tools</td>";
	html += "<td>"+obj.ModuleLink[i].otaSearch+"</td></tr>";
	html += "<tr><td>Admin</td>";
	html += "<td>"+obj.ModuleLink[i].showAdminTemplate+"</td></tr>";
	html += "<tr><td>Create New User</td>";
	html += "<td>"+obj.ModuleLink[i].saveUser+"</td></tr>";
	html += "<tr><td>Show Update User</td>";
	html += "<td>"+( obj.ModuleLink[i].showUpdateUser + obj.ModuleLink[i].updateUser  )+"</td></tr>";
	}
	
	html = "<table class='table table-striped'><tr><th>Link Name</th><th>Total Hit</th></tr>"+html+"</table>";
	$('#'+objId1).empty();
	$('#'+objId1).html(html);	
	}else{
		$('#'+objId1).empty();
		$('#'+objId1).html('');	
	}
	$('#'+objId).slideToggle(500);
	
}

function generateDayWiseReport(result) {	
	var htmlData="";
	result = '{"ModuleLink" : ['+result+']}';
	var obj = JSON.parse(result);
	var hitArray = new Array();
	var linkArray = new Array();
	var	addEir;
	var	unlockEir;
	var	searchTac;
	var	addtac;
	var	enableMMS;
	var totalCount=0;
	linkArray[0]='USD Data';	
	linkArray[1]='CNTDB';	
	linkArray[2]='Messaging Data';	
	linkArray[3]='Update DUS';	
	linkArray[4]='GFlex Tools';	
	linkArray[5]='IN/SDP Data';	
	linkArray[6]='OTA Tools';
	linkArray[7]='Admin';
	
	
	for(var k=0;k<linkArray.length;k++){
		hitArray[linkArray[k]]=0;
	}

	for(var i in obj.ModuleLink)
	{

		if(obj.ModuleLink[i].addEir==undefined || obj.ModuleLink[i].addEir=="NaN")
		{
			addEir=0;
		}else
		{
			addEir=obj.ModuleLink[i].addEir;
		}
		if(obj.ModuleLink[i].unlockEir==undefined || obj.ModuleLink[i].unlockEir=="NaN")
		{
			unlockEir=0;
		}else
		{
			unlockEir=obj.ModuleLink[i].unlockEir;
		}
		if(obj.ModuleLink[i].searchTac==undefined || obj.ModuleLink[i].searchTac=="NaN")
		{
			searchTac=0;
		}else
		{
			searchTac=obj.ModuleLink[i].searchTac;
		}
		if(obj.ModuleLink[i].addtac==undefined || obj.ModuleLink[i].addtac=="NaN")
		{
			addtac=0;
		}else
		{
			addtac=obj.ModuleLink[i].addtac;
		}
		if(obj.ModuleLink[i].enableMMS==undefined || obj.ModuleLink[i].enableMMS=="NaN")
		{
			enableMMS=0;
		}else
		{
			enableMMS=obj.ModuleLink[i].enableMMS;
		}
		
		//totalCount=obj.ModuleLink[i].nap + obj.ModuleLink[i].dus + obj.ModuleLink[i].pcrf + obj.ModuleLink[i].uma + obj.ModuleLink[i].hss + obj.ModuleLink[i].hlr + obj.ModuleLink[i].eir + addEir + unlockEir + searchTac + addtac + enableMMS;
		
		hitArray[linkArray[0]] = hitArray[linkArray[0]]+ obj.ModuleLink[i].nap + obj.ModuleLink[i].dus + obj.ModuleLink[i].pcrf + obj.ModuleLink[i].uma + obj.ModuleLink[i].hss + obj.ModuleLink[i].hlr + obj.ModuleLink[i].eir + addEir + unlockEir + searchTac + addtac + enableMMS;		
		hitArray[linkArray[1]] = hitArray[linkArray[1]]+ obj.ModuleLink[i].cntb + (obj.ModuleLink[i].cntbDBSearch + obj.ModuleLink[i].msisdn) + (obj.ModuleLink[i].cntbDBAdd + obj.ModuleLink[i].existAdd) + (obj.ModuleLink[i].cntbDBDelete + obj.ModuleLink[i].existDelete);		
		hitArray[linkArray[2]] = hitArray[linkArray[2]]+ obj.ModuleLink[i].mdt + obj.ModuleLink[i].smsc + obj.ModuleLink[i].iam ;				
		hitArray[linkArray[3]] = hitArray[linkArray[3]]+ obj.ModuleLink[i].dusupdate;			
		hitArray[linkArray[4]] = hitArray[linkArray[4]]+ obj.ModuleLink[i].gflex + obj.ModuleLink[i].search + obj.ModuleLink[i].add + obj.ModuleLink[i].update + obj.ModuleLink[i].delete_ + obj.ModuleLink[i].gFlexReports + obj.ModuleLink[i].cleanUp;		
		hitArray[linkArray[5]] = hitArray[linkArray[5]]+ obj.ModuleLink[i].insdp + obj.ModuleLink[i].BalanceAndDate + obj.ModuleLink[i].UpdateService + obj.ModuleLink[i].UpdateBalanceAndDate + obj.ModuleLink[i].InstallSubscriber + obj.ModuleLink[i].UpdateAccumulators	+ obj.ModuleLink[i].UpdateSubscriber + obj.ModuleLink[i].ActivateSubscriber + obj.ModuleLink[i].DeleteSubscriber;
		hitArray[linkArray[6]] = hitArray[linkArray[6]]+ obj.ModuleLink[i].otaSearch;
		hitArray[linkArray[7]] = hitArray[linkArray[7]]+ obj.ModuleLink[i].showAdminTemplate + obj.ModuleLink[i].saveUser + (obj.ModuleLink[i].showUpdateUser +  obj.ModuleLink[i].updateUser);
	}	
	
	for(var k=0;k<linkArray.length;k++){
		htmlData += "<tr><td>" + linkArray[k]+ "</td>";
		htmlData += "<td>" + hitArray[linkArray[k]]	+ "</td>";
		htmlData += "</tr>";	
		totalCount=totalCount+hitArray[linkArray[k]];
			
	}
		htmlData = "<table class='table table-striped'><tr><th>Link Name</th><th>Total Hit</th></tr>"+htmlData+"</table>";
	
		$('#showDayWiseReport').empty();
		$('#showDayWiseReport').html(htmlData);
		$('#totalReport').text("Total Hits:"+totalCount);
	

}
function isServerSelected(){
	if(document.getElementById('server_id').value=='' || document.getElementById('server_id').value=='0'){
		alert('Kindly Select Server !!');
		document.getElementById('server_id').focus();
		return false;
	}
	return true;
}
function showIP(id,id1){
	if(id1==1){
		$('#msg_div').empty();
		$('#msg_div').html('');	
	}	
	$('#ip_show').html('');		
	$('#ip_show').html($('#ip_'+id).html());	
	
}
$(document).ready(function() {
	function dataProcessStart() {
		$('#loading').html('<img src="' + Constants.CONTEXT_PATH + '/resources/images/process.gif"> Loading...');
		}

	function dataProcessEnd() {
		$('#loading').empty();
	}
	$("#hdr_server_config").change(function() {	
		searchCretria = {};	
		changeServerConfigFromHdr('/edat/changeServerConfig?saveData=1&callFrom=ajax&server_id='+$("#hdr_server_config").val(),searchCretria);
	});
	
$("#user_wise_report").click(function() {
	$('#user_wise_report').addClass("selected").siblings().removeClass("selected");
	$('#showDate').hide();	
	$('#showAllReport').empty();
	$('#showAllReport').html('');	
	searchCretria = {};
	searchCretria.link='user';
	dataProcessStart();
  sendUserWiseReportRequest('/edat/showUserWiseReports',searchCretria);
});
$("#day_wise_report").click(function() {
	$('#day_wise_report').addClass("selected").siblings().removeClass("selected");
	$('#showDate').show();
	$('#shoowMonth').hide();
	$('#showAllReport').empty();
	$('#showAllReport').html('');	
	searchCretria = {};
	searchCretria['reportDate']=$('#reportDate').val();
	searchCretria['link']='dayWise';
	dataProcessStart();
	sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
});

$("#month_wise_report").click(function() {
	$('#month_wise_report').addClass("selected").siblings().removeClass("selected");
	$('#shoowMonth').show();
	$('#showDate').hide();
	$('#showAllReport').empty();
	$('#showAllReport').html('');	
	searchCretria = {};
	searchCretria['year']=$('#year').val();
	searchCretria['toyear']=$('#toyear').val();
	searchCretria['fromMonth']=$('#fromMonth').val();
	searchCretria['toMonth']=$('#toMonth').val();
	searchCretria['link']='monthWise';
	dataProcessStart();
	sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
});

$("#year").blur(
		function(){
			$('#shoowMonth').show();
			$('#showDate').hide();
			$('#showAllReport').empty();
			$('#showAllReport').html('');	
			searchCretria = {};
			searchCretria['year']=$('#year').val();
			searchCretria['toyear']=$('#toyear').val();
			searchCretria['fromMonth']=$('#fromMonth').val();
			searchCretria['toMonth']=$('#toMonth').val();
			searchCretria['link']='monthWise';
			dataProcessStart();
			sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
		}		
		);

$("#toyear").blur(
		function(){
			$('#shoowMonth').show();
			$('#showDate').hide();
			$('#showAllReport').empty();
			$('#showAllReport').html('');	
			searchCretria = {};
			searchCretria['year']=$('#year').val();
			searchCretria['toyear']=$('#toyear').val();
			searchCretria['fromMonth']=$('#fromMonth').val();
			searchCretria['toMonth']=$('#toMonth').val();
			searchCretria['link']='monthWise';
			dataProcessStart();
			sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
		}		
		);

$("#toMonth").change(
		function(){
			$('#shoowMonth').show();
			$('#showDate').hide();
			$('#showAllReport').empty();
			$('#showAllReport').html('');	
			searchCretria = {};
			searchCretria['year']=$('#year').val();
			searchCretria['toyear']=$('#toyear').val();
			searchCretria['fromMonth']=$('#fromMonth').val();
			searchCretria['toMonth']=$('#toMonth').val();
			searchCretria['link']='monthWise';
			dataProcessStart();
			sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
		}		
		);


$("#fromMonth").change(
		function(){
			$('#shoowMonth').show();
			$('#showDate').hide();
			$('#showAllReport').empty();
			$('#showAllReport').html('');	
			searchCretria = {};
			searchCretria['year']=$('#year').val();
			searchCretria['toyear']=$('#toyear').val();
			searchCretria['fromMonth']=$('#fromMonth').val();
			searchCretria['toMonth']=$('#toMonth').val();
			searchCretria['link']='monthWise';
			dataProcessStart();
			sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
		}		
		);

$("#module_wise_report").click(function() {
	$('#module_wise_report').addClass("selected").siblings().removeClass("selected");
	$('#showDate').hide();	
	$('#showAllReport').empty();
	$('#showAllReport').html('');	
	searchCretria = {};
	searchCretria['link']='allHit';
	dataProcessStart();
	sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
});
$(function() {
	$("#reportDate").datepicker();
});
$("#reportDate").change(
function(){
	$('#showAllReport').empty();
	$('#showAllReport').html('');	
	searchCretria = {};
	searchCretria['reportDate']=$('#reportDate').val();
	searchCretria['link']='dayWise';
	sendDayWiseReportRequest('/edat/showDayWiseReports',searchCretria);
}		
);
function sendUserWiseReportRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url: searchUrl,
		data: jsonData,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Content-Type", "text/plain;");
			},
		error: function(xhr, ajaxOptions, thrownError) {
			dataProcessEnd();
			
		},
		success: function(result) {
			if (result) {				
				$('#showAllReport').empty();
				$('#showAllReport').html(result);	
			}
			dataProcessEnd();
		}
	});
}
function sendDayWiseReportRequest(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url: searchUrl,
		data: jsonData,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error: function(xhr, ajaxOptions, thrownError) {
			dataProcessEnd();
		},
		success: function(result) {
			if (result) {				
				$('#showAllReport').empty();
				$('#showAllReport').html(result);	
			}
			dataProcessEnd();
		}
	});
}
function changeServerConfigFromHdr(searchUrl, data) {
	var jsonData = JSON.stringify(data);
	$.ajax({
		url: searchUrl,
		data: jsonData,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Content-Type", "text/plain;");
			},
		error: function(xhr, ajaxOptions, thrownError) {
			
			
		},
		success: function(result) {
			if (result) {				
				alert(result);
			}
			
		}
	});
}
});




