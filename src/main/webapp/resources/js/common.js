$(document).ready(function() {
	$('[id^=msisdn]').keypress(validateNumber);
	$('[id^=msisdn]').blur(validateNum);
	$('[id^=imsi]').keypress(validateNumber);
	$('[id^=imsi]').blur(validateNum);

	$('[id^=alogId]').keypress(validateNumber);
	$('[id^=alogId]').blur(validateNum);
	$('[id^=kdbId]').keypress(validateNumber);
	$('[id^=kdbId]').blur(validateNum);
	$('[id^=custId]').keypress(validateNumber);
	$('[id^=custId]').blur(validateNum);
	$('[id^=accId]').keypress(validateNumber);
	$('[id^=accId]').blur(validateNum);

	// for common search Criteria
	$('[id^=searchText]').keypress(validateNumber);
	$('[id^=searchText]').blur(validateNum);

	// for INSDP Tool

	$('[id^=servicemsisdn]').keypress(validateNumber);
	$('[id^=servicemsisdn]').blur(validateNum);

	$('[id^=UpdateBalanceMsisdn]').keypress(validateNumber);
	$('[id^=UpdateBalanceMsisdn]').blur(validateNum);

	$('[id^=newmsisdn]').keypress(validateNumber);
	$('[id^=newmsisdn]').blur(validateNum);

	$('[id^=activeSubscriberMsisdn]').keypress(validateNumber);
	$('[id^=activeSubscriberMsisdn]').blur(validateNum);

	$('[id^=deletedmsisdn]').keypress(validateNumber);
	$('[id^=deletedmsisdn]').blur(validateNum);

	$('[id^=updatedmsisdn]').keypress(validateNumber);
	$('[id^=updatedmsisdn]').blur(validateNum);

	$('[id^=Accmsisdn]').keypress(validateNumber);
	$('[id^=Accmsisdn]').blur(validateNum);

	$('[id^=inputIMEI]').keypress(validateNumber);
	$('[id^=inputIMEI]').blur(validateNum);

});

function validateNum(field) {

	var charsAllowed = "0123456789";
	var allowed;
	for (var i = 0; i < this.value.length; i++) {
		allowed = false;
		for (var j = 0; j < charsAllowed.length; j++) {
			if (this.value.charAt(i) == charsAllowed.charAt(j)) {
				allowed = true;
			}
		}
		if (allowed == false) {
			alert("Invalid input enter only digit.");
			this.value = "";
			i--;
			this.focus();
		}
	}
	return true;
}

function validateNumForINSDP(id) {

	var charsAllowed = "0123456789";
	var allowed;
	for (var i = 0; i < document.getElementById(id).value.length; i++) {
		allowed = false;
		for (var j = 0; j < charsAllowed.length; j++) {
			if (document.getElementById(id).value.charAt(i) == charsAllowed
					.charAt(j)) {
				allowed = true;
			}
		}
		if (allowed == false) {
			alert("Invalid input enter only digit.");
			document.getElementById(id).value = "";
			i--;
			document.getElementById(id).focus();
		}
	}
	return true;
}

function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;

	if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37
			|| event.keyCode == 39 || event.keyCode == 67
			|| event.keyCode == 86 || event.keyCode == 88) {
		return true;
	} else if (key < 48 || key > 57) {
		return true;
	} else
		return true;
};

/**
 * This method get token parameters from UI and return it as an Object(Map)
 * 
 * @returns {tokenParams}
 */
function getTokenParameters() {
	var tokenParams = {};
	var clientId = $(".searchTypeClientId:visible").val();
	var secretKey = $(".iamSecretKey").val();
	
	if(clientId == "Others"){
		clientId = $(".iam_search_otherTxt:visible").val();
		if(!clientId || clientId == null || clientId == ""){
			tokenParams["error"] = "Please enter clientID!";
			return tokenParams;
		}		
	}	
	if(!secretKey || secretKey == "" || secretKey == null){
		secretKey = $(".iamSecretKey:visible").val();
		if(secretKey == ""){
			tokenParams["error"] = "Please enter secretKey!";
			return tokenParams;
		}
	}	
	if(iam_scopes == null || iam_scopes.length == 0){
		tokenParams["error"] = "Please select atleast one scope!";
		return tokenParams;
	}
	if(iam_grantType == null || iam_grantType.length == 0){
		tokenParams["error"] = "Please select atleast one grantType!";
		return tokenParams;
	}
	tokenParams["clientId"] = clientId;
	tokenParams["secretKey"] = secretKey;
	tokenParams["scope"] = iam_scopes;
	tokenParams["grantType"] = iam_grantType;
	
	return tokenParams;
}

function errorMessage(msg){
	alert(msg);
}

/*
 * This method will clear all the selection and inputs of iamCommonElmnt.
 */
function resetFieldInputs() {
	$("input:text").val("");
	$(".iam_search_otherTxt").addClass("hideOthers");
	$(".iamSecretKeyLbl").addClass("hideOthers");
	$(".iamSecretKey").addClass("hideOthers");
	$("select option:selected").removeAttr("selected");
}

/**
 * This method is responsible for sending orl and data to controller and receive
 * result back to UI
 * 
 * @param conrollerUrl
 * @param controllerBody
 * @param callbackResult
 */

function sendRequest(conrollerUrl, methodType, controllerBody, callbackResult, callbackError, callbackObj) {
	dataProcessStart();
	$('#msgInfo').hide();

	var jsonData = JSON.stringify(controllerBody);
	$.ajax({
		url : conrollerUrl,
		data : jsonData,
		type : methodType,

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			callbackResult(result, callbackObj);
			setTimeout(function() {
				$('#loading').empty();
			});
		},
		error : function(xhr, ajaxOptions, thrownError) {
			callbackError(xhr, ajaxOptions, thrownError, callbackObj);
			setTimeout(function() {
				$('#loading').empty();
			});
		}
	});

}
/**
 * This method will display the error message as dialog.
 * 
 * @param result
 */
function showErrorMessage(result) {
	if (result.statusCode == "400") {
		alert(result.error + '\n' + result.errorDescription);
	}
}

function displayCreatedTree(jsonData){
	$('.tree').tree({
		data: createTree(jsonData),
		selectable: false,
		closedIcon: $('<i class="glyphicon glyphicon-plus-sign"></i>'),
		openedIcon: $('<i class="glyphicon glyphicon-minus-sign"></i>')
	});
}



