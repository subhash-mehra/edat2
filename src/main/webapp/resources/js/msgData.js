var customerId = null;
var emailId = null;
var usdMSISDN = null;
var usdIMSI = null;
var searchCretria = {};
var msgUrl = "smcs";
var msgUrl1 = "smscMavenier";
var searchDropDownMenu = {
	"msisdn" : "MSISDN"
};

$(document)
		.ready(
				function() {
					$("#searchBox").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#search").click();
						}
					});

					$("#modifySearchText").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#modifySearch").click();
						}
					});

					$("#deleteSearchText").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#deleteSearch").click();
						}
					});

					$("#mmscDeleteMsisdn").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#mmscDeleteBtn").click();
						}
					});

					$("#deleteSubscriberMsisdn").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#deleteSubscriberBtn").click();
						}
					});

					$("#deletespecificSubscriberMsisdn").keypress(
							function(event) {
								if (event.keyCode == 13) {
									$("#deleteSubscriberServiceBtn").click();
								}
							});

					$("#getsubscriberMsisdn").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#getSubscriberBtn").click();
						}
					});

					$("#getServiceSubscriberMsisdn").keypress(function(event) {
						if (event.keyCode == 13) {
							$("#getSubscriberServiceBtn").click();
						}
					});

					$('#smscPrepaid').on('change', function() {

						if (this.value == "yes") {
							$('#prepaidDiv').show();
							$('#smscUserAgentProfile').val("Control");

						} else {
							$('#prepaidDiv').hide();
						}

					});

					$('#smscModifyPrepaid').on('change', function() {

						if (this.value == "yes") {
							$('#prepaidModifyDiv').show();
							$('#smscModifyUserAgentProfile').val("Control");

						} else {
							$('#prepaidModifyDiv').hide();
						}

					});

					$("#messagingData").parent().addClass("active");

					/* $(this).addClass("activeUSD").siblings().removeClass("activeUSD"); */
					/* $("#smcs").addClass("selected").siblings().removeClass("selected"); */
					$('#msgInfo').hide();
					$("#iam_privacy_vault_panel").hide();
					$('#iam_lock_panel').hide();
					$('#iam_unlock_panel').hide();

					createSearchDropDown("searchType", searchDropDownMenu);
					$("#search").click(
							function() {
								searchCretria = {};
								if ($.trim($("#mdtSearch").val()) == '') {
									alert(error_msg);
									return;
								} else {
									var element = $("#searchType").val();
									var flag = validateInput(element, "mdtSearch");
									if (flag) {
										var val = $("#searchType").val();
										searchCretria[val] = $("#mdtSearch")
												.val();
										if (element == "msisdn") {
											searchCretria = {};
											searchCretria[val] = '1'
													+ $("#mdtSearch").val();
											/*
											 * searchCretria[$("#searchType").val()] =
											 * $("#mdtSearch").val();
											 */
										}

										getMsgData();

										if (msgUrl != "smcs"
												&& msgUrl != "iam_search"
												&& msgUrl != "iam_token"
												&& msgUrl != "mmsc"
												&& msgUrl != "smscMavenier") {
											getedatData();
										}
									}
								}
							});

					$("#modifySearch")
							.click(
									function() {
										searchCretria = {};
										if ($
												.trim($("#modifySearchText")
														.val()) == '') {
											alert("Please specify search cretria...");
											return;
										} else {
											var element = $("#searchType")
													.val();
											var flag = validateInput(element, "mdtSearch");
											if (flag) {
												searchCretria[$("#searchType")
														.val()] = $(
														"#mdtSearch").val();
												if (element == "msisdn") {
													searchCretria = {};
													searchCretria[$(
															"#searchType")
															.val()] = '1'
															+ $("#mdtSearch")
																	.val();

													/*
													 * searchCretria[$("#searchType").val()] =
													 * $("#mdtSearch").val();
													 */
												}

												getMsgData();

												if (msgUrl != "smcs"
														&& msgUrl != "iam_search"
														&& msgUrl != "iam_token"
														&& msgUrl != "mmsc"
														&& msgUrl != "smscMavenier") {
													getedatData();
												}
											}
										}
									});

					$("#modifySMSCSubscriberBtn").click(function() {
						if (validateSmscModifySub()) {
							smscModifySubscriber();
						}
					});

					$("#deleteSearch")
							.click(
									function() {
										searchCretria = {};
										if ($
												.trim($("#deleteSearchText")
														.val()) == '') {
											alert("Please specify search cretria...");
											return;
										} else {
											var element = $("#searchType")
													.val();
										var flag = validateInput(element, "mdtSearch");
											if (flag) {
												searchCretria[$("#searchType")
														.val()] = $(
														"#mdtSearch").val();
												if (element == "msisdn") {
													searchCretria = {};
													searchCretria[$(
															"#searchType")
															.val()] = '1'
															+ $("#mdtSearch")
																	.val();
													/*
													 * searchCretria[$("#searchType").val()] =
													 * $("#mdtSearch").val();
													 */
												}

												getMsgData();

												if (msgUrl != "smcs"
														&& msgUrl != "iam_search"
														&& msgUrl != "iam_token"
														&& msgUrl != "mmsc"
														&& msgUrl != "smscMavenier") {
													getedatData();
												}
											}
										}
									});

					$("#mmscDeleteBtn")
							.click(
									function() {
										searchCretria = {};
										if ($
												.trim($("#mmscDeleteMsisdn")
														.val()) == '') {
											alert("Please specify search cretria...");
											return;
										} else {
											var element = $("#searchType")
													.val();
											var flag = validateInput(element, "mdtSearch");
											if (flag) {
												searchCretria[$("#searchType")
														.val()] = $(
														"#mdtSearch").val();
												if (element == "msisdn") {
													searchCretria = {};
													searchCretria[$(
															"#searchType")
															.val()] = '1'
															+ $("#mdtSearch")
																	.val();
													/*
													 * searchCretria[$("#searchType").val()] =
													 * $("#mdtSearch").val();
													 */
												}

												getMsgData();

												if (msgUrl != "smcs"
														&& msgUrl != "iam_search"
														&& msgUrl != "iam_token"
														&& msgUrl != "mmsc"
														&& msgUrl != "smscMavenier") {
													getedatData();
												}
											}
										}
									});
		
			
			$("#msgTabList li").click(
					function() {
						resetAllElments();
						$(this).addClass("selected").siblings().removeClass("selected");
						switch (this.id) {

						case "iam":
						case "iam_search":
							$("#clientid_lbl").show();
							$('#SMSCDiv').show();
							$('#panelHeading').html('IAM Search :');
							searchDropDownMenu = {
								"msisdn" : "MSISDN",
								"imsi" : "IMSI",
								"customerId" : "User ID",
								"emailId" : "Email"
							};
							createSearchDropDown("searchType", searchDropDownMenu);
							msgUrl = "iam_search";
							$(".iam-main-panel").removeClass("hideOthers");
							$("#iam").removeClass("selected");
							$(".iamSection").removeClass("hideOthers");
							document.title="IAM Search";
							getClientIDs();
							break;
							
						case "iam_token":
							$("#iam_token_panel").show();
							msgUrl = "iam_token";
							$("#iam").removeClass("selected");
							$(".iam-main-panel").removeClass("hideOthers");
							$("#iam_token_panel").removeClass("hideTokenPanel");
							$(".iamSection").removeClass("hideOthers");
							document.title="IAM Generate Token";
							getClientIDs();
							break;
							
						case "iam_lock":
							$('#iam_lock_panel').show();
							$("#searchTypeClientId").addClass('show_Client_DD');
							msgUrl = "iam_lock";
							searchDropDownMenu = {
									"msisdn" : "MSISDN",
									"imsi" : "IMSI",
									"customerId" : "User ID",
									"emailId" : "Email"
							};
							createSearchDropDown("searchTypeLock", searchDropDownMenu);
							$("#iam_lock_panel").removeClass("hideLockPanel");
							$("#iam").removeClass("selected");
							$(".iam-main-panel").removeClass("hideOthers");
							$(".iamSection").removeClass("hideOthers");
							document.title="IAM Lock";
							getClientIDs();
							break;
							
						case "iam_unlock":
							$('#iam_unlock_panel').show();
							$("#searchTypeClientId").addClass('show_Client_DD');
							msgUrl = "iam_unLock";
							searchDropDownMenu = {
									"msisdn" : "MSISDN",
									"imsi" : "IMSI",
									"customerId" : "User ID",
									"emailId" : "Email"
							};
							createSearchDropDown("searchTypeUnLock", searchDropDownMenu);
							$("#iam").removeClass("selected");
							$(".iam-main-panel").removeClass("hideOthers");
							$(".iamSection").removeClass("hideOthers");
							$("#iam_unlock_panel").removeClass("hideUnLockPanel");
							document.title="IAM UnLock";
							getClientIDs();
							break;

						case "iam_link":
							$('#iam_link_panel').show();
							searchDropDownMenu = {
								"msisdn" : "MSISDN",
								"imsi" : "IMSI",
								"customerId" : "User ID",
								"emailId" : "Email"
							};
							createSearchDropDown("searchTypeLink", searchDropDownMenu);
							$("#iam").removeClass("selected");
							$(".iam-main-panel").removeClass("hideOthers");
							$(".iamSection").removeClass("hideOthers");
							document.title="IAM Link";
							getClientIDs();
							break;
							
						case "iam_unlink":
							$('#iam_unlink_panel').show();
							searchDropDownMenu = {
									"msisdn" : "MSISDN",
									"imsi" : "IMSI",
									"customerId" : "User ID",
									"emailId" : "Email"
							};
							createSearchDropDown("searchTypeUnLink", searchDropDownMenu);
							$("#iam").removeClass("selected");
							$(".iam-main-panel").removeClass("hideOthers");
							$(".iamSection").removeClass("hideOthers");
							document.title="IAM Unlink";
							getClientIDs();
							break;
							
						case "iam_privacy_vault":
							$("#clientid_lbl").show();
							$("#iam_privacy_vault_panel").show();
							$('#panelHeading').html('Privacy Vault :');
							searchDropDownMenu = {
								"alloffers" : "All Offers",
								"offerIds" : "OfferID"								
							};
							createSearchDropDown("privacy_vault_search_type", searchDropDownMenu);
							$(".iam-main-panel").removeClass("hideOthers");
							$("#iam").removeClass("selected");
							$("#iamSection").removeClass("hideOthers");
							document.title="Privacy Vault";
							getClientIDs();
							break;
						
						case "smcsAci":
						case "smcs":
							$('#SMSCDiv').show();
							$('#panelHeading').html('SMSC-Aci Search :');
							msgUrl = "smcs";
							searchDropDownMenu = {
								"msisdn" : "MSISDN"
							};
							createSearchDropDown("searchType", searchDropDownMenu);	
							document.title="SMSC Search";
							break;
						
						case "mmscMain":
						case "mmsc":
							$('#SMSCDiv').show();
							msgUrl = "mmsc";
							$('#panelHeading').html('MMSC Search :');
							searchDropDownMenu = {
								"msisdn" : "MSISDN"
							};
							createSearchDropDown("searchType", searchDropDownMenu);
							$("#iam").removeClass("selected");
							$("#iam_search").removeClass("selected");
							$("#iam_token").removeClass("selected");
							$("#iam_link").removeClass("selected");
							$("#iam_unlink").removeClass("selected");
							document.title="MMSC Search";
							break;
						
						case "addSubscriber":
							$('#addSubscriberDiv').show();
							msgUrl = "addSubscriber";
							document.title="Add Subscriber";
							break;

						case "putSubscriber":
							$('#putSubscriberDiv').show();
							msgUrl = "putSubscriber";
							document.title="MMSC Add/Modify";
							break;
							
							
						case "mmscDelete":
							$('#mmscDeleteDiv').show();
							msgUrl = "putSubscriber";
							document.title="MMSC Delete";
							break;

						case "mmscAdd":
							$('#mmscAddSubscriberDiv').show();
							msgUrl = "putSubscriber";
							document.title="MMSC Add Subscriber";
							break;

						case "modifySubscriber":
							$('#modifySubscriberDiv').show();
							msgUrl = "modifySubscriber";
							document.title="SMSC-Mav ModifySubscriber";
							break;

						case "deleteSubscriberForSMCS":
							$('#deleteSubscriberDiv').show();
							msgUrl = "deleteSubscriber";
							document.title="SMSC-Mav Delete Suscriber";
							break;

						case "deleteSubscriberService":
							$('#deleteSubscriberServiceDiv').show();
							msgUrl = "deleteSubscriberService";
							document.title="SMSC-Mav Delete Suscriber By Service";
							break;

						case "smscMavenier":
						case "getSubscriber":
							$('#getSubscriberDiv').show();
							msgUrl = "getSubscriber";
							document.title="SMSC-Mav Get Suscriber";
							break;

						case "getSubscriberService":
							$('#getSubscriberServiceDiv').show();
							msgUrl = "getSubscriberService";
							document.title="SMSC-Mav Get Suscriber By Service";
							break;

						case "smscAddSubscriber":
							$('#addSNSCSubscriberDiv').show();
							msgUrl = "addSubscriber";
							document.title="SMSC Add Subscriber";							
							break;

						case "smscModifySubscriber":
							$('#SMSCSearchModifyDiv').show();
							msgUrl = "addSubscriber";
							document.title="SMSC Modify Subscriber";
							break;

						case "smscDeleteSubscriber":
							$('#SMSCSearchdeleteDiv').show();
							msgUrl = "addSubscriber";
							document.title="SMSC Delete Subscriber";
							break;
						}
						
						$("#mdtSearch").val("");
						$('#msgContentBody').empty();
					});
		});

/**
 * By default, all elements should be hidden and 
 * removed the selected class on mdt.jsp page
 */
function resetAllElments(){
	$(".iam-main-panel").addClass("hideOthers");
	$("#iam_token_panel").addClass("hideTokenPanel");
	$(".iam_search_otherTxt").addClass("hideOthers");
	$(".iamSecretKeyLbl").addClass("hideOthers");
	$(".iamSecretKey").addClass("hideOthers");
	$('#iam_token_panel').hide();
	$("#msgInfo").hide();
	$("#clientid_lbl").hide();
	$("#iam_privacy_vault_panel").hide();
	$('#iam_lock_panel').hide();
	$('#iam_unlock_panel').hide();
	$("#clientid_lbl").hide();
	$('#iam_lock_panel').hide();
	$('#iam_unlock_panel').hide();
	$('#iam_link_panel').hide();
	$('#iam_unlink_panel').hide();
	$('#SMSCDiv').hide();
	$('#addSubscriberDiv').hide();
	$('#modifySubscriberDiv').hide();
	$('#modifySMSCSubscriberDiv').hide();
	$('#SMSCSearchdeleteDiv').hide();
	$('#deleteSubscriberDiv').hide();
	$('#deleteSubscriberServiceDiv').hide();
	$('#getSubscriberDiv').hide();
	$('#getSubscriberServiceDiv').hide();
	$('#messageHeading').hide();
	$('#getSubscriberContentHeading').hide();
	$('#getSubscriberContentBody').hide();
	$('#panelDiv').hide();
	$('#getSubscriberServiceContentHeading').hide();
	$('#getSubscriberServiceContentBody').hide();
	$('#panelDivService').hide();
	$('#putSubscriberDiv').hide();
	$('#mmscAddSubscriberDiv').hide();
	$('#addSNSCSubscriberDiv').hide();
	$('#SMSCSearchModifyDiv').hide();
	$("#msgInfo").hide();
	$("#mmscDeleteDiv").hide();
	
	$("#deleteSubscriberService").removeClass("selected");
	$("#getSubscriberService").removeClass("selected");
	$("#smscMavenier").removeClass("selected");
	
	$("#mmscAdd").removeClass("selected");
	$("#mmscMain").removeClass("selected");
	
	$("#smcs").removeClass("selected");
	$("#smscAddSubscriber").removeClass("selected");
	$("#smscModifySubscriber").removeClass("selected");
	$("#smscDeleteSubscriber").removeClass("selected");
	
	$("#mmsc").removeClass("selected");
	$("#putSubscriber").removeClass("selected");
	$("#mmscDelete").removeClass("selected");
	
	$("#iam_search").removeClass("selected");
	$("#iam_token").removeClass("selected");
	$("#iam_link").removeClass("selected");
	$("#iam_unlink").removeClass("selected");
	$("#iam_lock").removeClass("selected");
	$("#iam_unlock").removeClass("selected");
	$("#iam_privacy_vault").removeClass("selected");

	$("#getSubscriber").removeClass("selected");
	$("#addSubscriber").removeClass("selected");
	$("#modifySubscriber").removeClass("selected");
	$("#deleteSubscriberForSMCS").removeClass("selected");	
	clearText();
}

function clearText() {
	$("#addSubscriberMsisdn").val("");
	$("#imsiAdd").val("");
	$("#subStatus").val("");
	$("#language").val("");
	$("#subType").val("");
	$("#operatorId").val("");
	$("#modifySubscriberMsisdn").val("");
	$("#modifySubType").val("");
	$("#deleteSubscriberMsisdn").val("");
	$("#deletespecificSubscriberMsisdn").val("");
	$("#getsubscriberMsisdn").val("");
	$("#getServiceSubscriberMsisdn").val("");
	iam_scopes = null;
	iam_grantType = null;
}
function getedatData() {

	var activeTab = $("#msgTabList li.selected");
	activeTab = activeTab[0].id;

	if (activeTab == "smscDeleteSubscriber") {

		$('#messageHeading').hide();
		if (smscDeleteValidate()) {
			smscSearchDeleteSubscriber();
		}
	}

	if (activeTab == "smscModifySubscriber") {

		$('#messageHeading').hide();

		smscSearchModifySubscriber();

	}

	if (activeTab == "smscAddSubscriber") {

		$('#messageHeading').hide();
		if (validateSmscAddSub()) {
			smscAddSubscriber();
		}
	}

	if (activeTab == "addSubscriber") {

		$('#messageHeading').hide();
		if (validateFieldForAddSubscriber()) {
			addSubscriber();
		}
	}

	if (activeTab == "putSubscriber") {
		$('#messageHeading').hide();

		if (validateFieldForMMSCAddSubscriber())
			putSubscriber();

	}

	if (activeTab == "mmscAdd") {
		$('#messageHeading').hide();
		mmscAddSubscriber();

	}

	if (activeTab == "modifySubscriber") {
		$('#messageHeading').hide();
		if (validateFieldForModifySubscriber()) {
			modifySubscriber();
		}
	}
	if (activeTab == "deleteSubscriberForSMCS") {
		$('#messageHeading').hide();
		if (validateFieldForDeleteSubscriber()) {

			deleteSubscriber();
		}
	}

	if (activeTab == "mmscDelete") {
		$('#messageHeading').hide();
		if (validateFieldForMmscDelete()) {

			deleteMmscSubscriber();
		}
	}

	if (activeTab == "deleteSubscriberService") {
		$('#messageHeading').hide();
		if (validateFieldForDeleteServiceSubscriber()) {
			deleteSubscriberService();
		}
	}
	// need to be write new defination
	if (activeTab == "getSubscriber") {
		$('#messageHeading').hide();
		if (validateFieldForGetSubscriber()) {
			getSubscriber();
		}
	}

	if (activeTab == "getSubscriberService") {
		$('#messageHeading').hide();
		if (validateFieldForGetServiceSubscriber()) {
			getSubscriberService();
		}
	}
}

function validateFieldForMMSCAddSubscriber() {
	var msisdn = $("#putSubscriberMsisdn").val();
	var subStatus = $("#putSubStatus").val();
	var putCOS = $("#putCOS").val();
	var putSubType = $("#putSubType").val();
	var putMMSCapable = $("#putMMSCapable").val();
	/*
	 * var putCOS = $("#putCOS").val(); var putSubType = $("#putSubType").val();
	 * var putHandSetType = $("#putHandSetType").val(); var putMMSCapable =
	 * $("#putMMSCapable").val();
	 */

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#putSubscriberMsisdn").focus();
			return false;
		} else if (subStatus != "") {
			if (putCOS != "") {
				if (putSubType != "") {
					if (putMMSCapable != "") {
						return true;
					} else {
						alert("Please enter MMSCapable");
						$("#putMMSCapable").focus();
						return false;
					}
				} else {
					alert("Please enter sub type");
					$("#putSubType").focus();
					return false;
				}

			} else {
				alert("Please enter COS");
				$("#putCOS").focus();
				return false;
			}

		} else {
			alert("Please enter sub Status");
			$("#putSubStatus").focus();
			return false;

		}

	} else {
		alert("Please enter MSISDN");
		$("#putSubscriberMsisdn").focus();
		return false;
	}
}

function validateFieldForGetSubscriber() {
	var msisdn = $("#getsubscriberMsisdn").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#getsubscriberMsisdn").focus();
			return false;
		} else {
			return true;
		}

	} else {
		alert("Please enter MSISDN");
		$("#getsubscriberMsisdn").focus();
		return false;
	}

	return false;
}

function validateFieldForGetServiceSubscriber() {
	var msisdn = $("#getServiceSubscriberMsisdn").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#getServiceSubscriberMsisdn").focus();
			return false;
		} else {
			return true;
		}

	} else {
		alert("Please enter MSISDN");
		$("#getServiceSubscriberMsisdn").focus();
		return false;
	}

	return false;
}
function validateFieldForDeleteSubscriber() {
	var msisdn = $("#deleteSubscriberMsisdn").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#deleteSubscriberMsisdn").focus();
			return false;
		} else {
			return true;
		}

	} else {
		alert("Please enter MSISDN");
		$("#deleteSubscriberMsisdn").focus();
		return false;
	}

	return false;
}

function validateFieldForMmscDelete() {
	var msisdn = $("#mmscDeleteMsisdn").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#mmscDeleteMsisdn").focus();
			return false;
		} else {
			return true;
		}

	} else {
		alert("Please enter MSISDN");
		$("#mmscDeleteMsisdn").focus();
		return false;
	}

	return false;
}

function smscDeleteValidate() {
	var msisdn = $("#deleteSearchText").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#deleteSearchText").focus();
			return false;
		} else {
			return true;
		}

	} else {
		alert("Please enter MSISDN");
		$("#deleteSearchText").focus();
		return false;
	}

	return false;
}

function validateFieldForDeleteServiceSubscriber() {
	var msisdn = $("#deletespecificSubscriberMsisdn").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#deletespecificSubscriberMsisdn").focus();
			return false;
		} else {
			return true;
		}

	} else {
		alert("Please enter MSISDN");
		$("#deletespecificSubscriberMsisdn").focus();
		return false;
	}

	return false;
}

function validateFieldForModifySubscriber() {
	var msisdn = $("#modifySubscriberMsisdn").val();
	var subType = $("#modifySubType").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#modifySubscriberMsisdn").focus();
			return false;
		} else if (subType != "") {
			return true;
		} else {
			alert("Please enter sub type");
			$("#modifySubType").focus();
			return false;
		}
	} else {
		alert("Please enter MSISDN");
		$("#modifySubscriberMsisdn").focus();
		return false;
	}

	return false;
}

function validateFieldForAddSubscriber() {
	var msisdn = $("#addSubscriberMsisdn").val();
	var imsi = $("#imsiAdd").val();

	var subStatus = $("#subStatus").val();
	var language = $("#language").val();
	var subType = $("#subType").val();
	var operatorId = $("#operatorId").val();
	var profileId = $("#profileId").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {

			alert('Invalid MSISDN');
			$("#addSubscriberMsisdn").focus();
			return false;
		} else if (imsi != "") {
			if (imsi.length != 15 && imsi.length != 0) {
				alert("Invalid IMSI");
				$("#imsiAdd").focus();
			} else if (subStatus != "") {
				if (language != "") {
					if (subType != "") {
						if (operatorId != "") {
							if (profileId != "") {
								return true;
							} else {
								alert("Please enter profile Id");
								$("#profileId").focus();
								return false;
							}

						} else {
							alert("Please enter operatorId");
							$("#operatorId").focus();
							return false;
						}
					} else {
						alert("Please enter sub type");
						$("#subType").focus();
						return false;
					}

				} else {
					alert("Please enter language");
					$("#language").focus();
					return false;
				}

			} else {
				alert("Please enter sub Status");
				$("#subStatus").focus();
				return false;

			}
		} else {
			alert("Please enter IMSI");
			$("#imsiAdd").focus();
		}
	} else {
		alert("Please enter MSISDN");
		$("#addSubscriberMsisdn").focus();
		return false;
	}

	return false;
}

function smscAddSubscriber() {
	var telePhoneNo = $("#smscTelephone").val();
	var smscsubprofilenumber = $("#smscProfileNo").val();
	var subAddress = $("#smscSubAddress").val();
	var uid = $("#smscUID").val();
	var gender = $("#smscGender").val();
	var prepaid = $("#smscPrepaid").val();
	var cn = $("#smscCN").val();
	var emailId = $("#smscMail").val();
	var givenName = $("#smscGivenName").val();
	var sn = $("#smscSN").val();
	var uniqIdentifier = $("#smscUniqueIdentifier").val();
	var userAgentProfile = $("#smscUserAgentProfile").val();
	var userAgentString = $("#smscUserAgentString").val();
	var tplaCode = $("#smscTplaCode").val();
	var smscsubprepaid = $("#smscsubprepaid").val();
	var psa = $("#smscPAS").val();
	var inpCode = $("#smscInpCode").val();
	var psaServices = $("#smscPSAServices").val();
	var psaforceflag = $("#psaforceflag").val();
	var psaaction = $("#psaaction").val();
	var psamsisdn = $("#psamsisdn").val();
	var language = $("#smscLanguage").val();
	var mobileType = $("#smscMobileType").val();
	var password = $("#smscPassword").val();

	var data = null;
	dataProcessStart1();

	data = {
		"telePhoneNo" : telePhoneNo,
		"smscsubprofilenumber" : smscsubprofilenumber,
		"subAddress" : subAddress,
		"uid" : uid,
		"gender" : gender,
		"prepaid" : prepaid,
		"cn" : cn,
		"emailId" : emailId,
		"givenName" : givenName,
		"sn" : sn,
		"uniqIdentifier" : uniqIdentifier,
		"userAgentProfile" : userAgentProfile,
		"userAgentString" : userAgentString,
		"tplaCode" : tplaCode,
		"smscsubprepaid" : smscsubprepaid,
		"psa" : psa,
		"inpCode" : inpCode,
		"psaServices" : psaServices,
		"psaforceflag" : psaforceflag,
		"psaaction" : psaaction,
		"psamsisdn" : psamsisdn,
		"language" : language,
		"mobileType" : mobileType,
		"password" : password,

	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/addSMSCSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			generatesmscAddSubscriber(result);
			$('#messageHeading').show();

		}
	});

}

function generatesmscAddSubscriber(result) {

	if (result.applicationError != null) {
		$('#messageHeading').text(result.applicationError.errorCode);
		$('#messageHeading').show();
		clearTextFieldForAddSMSCSubscriber();

	} else {
		$('#messageHeading').text(result.message);
		$('#messageHeading').show();
		clearTextFieldForAddSMSCSubscriber();

	}
	dataProcessEnd();

}

function clearTextFieldForAddSMSCSubscriber() {
	$("#smscTelephone").val("");
	$("#smscProfileNo").val("");
	$("#smscSubAddress").val("");
	$("#smscUID").val("");
	// $("#smscGender").val("");

	var el1 = document.getElementById("smscPrepaid");
	for (var i = 0; i < el1.options.length; i++) {
		if (el1.options[i].text == "Yes") {
			el1.selectedIndex = i;
			break;
		}
	}

	$("#smscCN").val("TMOUS");
	$("#smscMail").val("");
	$("#smscGivenName").val("");
	$("#smscSN").val("TMO");
	$("#smscUniqueIdentifier").val("");
	$("#smscUserAgentProfile").val("Postpaid");
	$("#smscUserAgentString").val("Active");
	$("#smscTplaCode").val("TPLA1");
	$("#smscsubprepaid").val("");
	$("#smscPAS").val("PSA1");
	$("#smscInpCode").val("TMO");
	$("#smscPSAServices").val("");
	var el1 = document.getElementById("psaforceflag");
	for (var i = 0; i < el1.options.length; i++) {
		if (el1.options[i].text == "Yes") {
			el1.selectedIndex = i;
			break;
		}

	}
	$("#psaaction").val("");
	$("#psamsisdn").val("");
	$("#smscLanguage").val("");
	$("#smscMobileType").val("");
	$("#smscPassword").val("");

}

function addSubscriber() {

	dataProcessStart();
	var msisdn = '1' + $("#addSubscriberMsisdn").val();
	// var msisdn = $("#addSubscriberMsisdn").val();
	var imsiAdd = $("#imsiAdd").val();
	var subStatus = $("#subStatus").val();
	var language = $("#language").val();
	var subType = $("#subType").val();
	var operatorId = $("#operatorId").val();
	var profileId = $("#profileId").val();
	var imsEnabled;

	if ($("#imsEnabled").val() == "TRUE") {
		imsEnabled = 1;
	} else {
		imsEnabled = 0;
	}

	// var searchText = $("#searchText").val();
	var data = null;

	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"imsi" : imsiAdd,
		"subType" : subType,
		"subStatus" : subStatus,
		"language" : language,
		"operatorId" : operatorId,
		"profileId" : profileId,
		"imsEnabled" : imsEnabled
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/addSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			putMessageForAddSubscriber(result);
			ClearTextForAddSubscriber();
			$('#messageHeading').show();

		}
	});

}

function mmscAddSubscriber() {

	var msisdn = '1' + $("#mmscAddSubscriberMsisdn").val();
	/* var msisdn = $("#mmscAddSubscriberMsisdn").val(); */
	var putSubStatus = $("#mmscAddSubStatus").val();
	var putCOS = $("#mmscAddCOS").val();
	var putSubType = $("#mmscAddSubType").val();
	var putHandSetType = $("#mmscAddHandSetType").val();
	var putOperatorId = $("#mmscAddOperatorId").val();
	var putMMSCapable = $("#mmscAddMMSCapable").val();
	var putProfileId = $("#mmscAddProfileId").val();
	var putImsEnabled = $("#mmscAddImsEnabled").val();

	if ($("#mmscAddImsEnabled").val() == "TRUE") {
		imsEnabled = 1;
	} else {
		imsEnabled = 0;
	}

	// var searchText = $("#searchText").val();
	var data = null;

	data = {
		"msisdn" : msisdn,
		"subStatus" : putSubStatus,
		"cos" : putCOS,
		"subType" : putSubType,
		"handsetType" : putHandSetType,
		"operatorId" : putOperatorId,
		"mmsCapable" : putMMSCapable,
		"profileId" : putProfileId,
		"imsEnabled" : putImsEnabled
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/putSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			putMessageForAddSubscriber(result);
			ClearTextForAddMMSC();
			$('#messageHeading').show();

		}
	});

}

function putSubscriber() {
	dataProcessStart();
	var msisdn = '1' + $("#putSubscriberMsisdn").val();
	/* var msisdn = $("#putSubscriberMsisdn").val(); */
	var putSubStatus = $("#putSubStatus").val();
	var putCOS = $("#putCOS").val();
	var putSubType = $("#putSubType").val();
	var putHandSetType = $("#putHandSetType").val();
	var putOperatorId = $("#putOperatorId").val();
	var putMMSCapable = $("#putMMSCapable").val();
	var putProfileId = $("#putProfileId").val();
	var putImsEnabled = $("#putImsEnabled").val();

	if ($("#putImsEnabled").val() == "TRUE") {
		imsEnabled = 1;
	} else {
		imsEnabled = 0;
	}

	var data = null;

	data = {
		"msisdn" : msisdn,
		"subStatus" : putSubStatus,
		"cos" : putCOS,
		"subType" : putSubType,
		"handsetType" : putHandSetType,
		"operatorId" : putOperatorId,
		"mmsCapable" : putMMSCapable,
		"profileId" : putProfileId,
		"imsEnabled" : putImsEnabled
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/putSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			putMessageForAddSubscriber(result);
			ClearTextForPutSubscriber();
			$('#messageHeading').show();

		}
	});

}

function ClearTextForAddSubscriber() {
	$("#addSubscriberMsisdn").val("");
	$("#imsiAdd").val("");
	$("#subStatus").val("");
	$("#language").val("");
	$("#subType").val("");
	$("#operatorId").val("");
	$("#profileId").val("");
	$("#imsEnabled").val("");
}

function ClearTextForPutSubscriber() {
	$("#putSubscriberMsisdn").val("");
	$("#putSubStatus").val("");
	$("#putCOS").val("");
	$("#putSubType").val("");
	$("#putHandSetType").val("");
	$("#putOperatorId").val("");
	$("#putMMSCapable").val("");
	// $("#putProfileId").val("");
	$("#putImsEnabled").val("");
}

function ClearTextForAddMMSC() {
	$("#mmscAddSubscriberMsisdn").val("");
	$("#mmscAddSubStatus").val("");
	$("#mmscAddCOS").val("");
	$("#mmscAddSubType").val("");
	$("#mmscAddHandSetType").val("");
	$("#mmscAddMMSCapable").val("");
	$("#mmscAddOperatorId").val("");
	$("#mmscAddProfileId").val("");
	$("#mmscAddImsEnabled").val("");
}
function modifySubscriber() {

	dataProcessStart();
	var msisdn = '1' + $("#modifySubscriberMsisdn").val();
	/* var msisdn = $("#modifySubscriberMsisdn").val(); */
	var subType = $("#modifySubType").val();
	var profileId = $("#profileIdUpdate").val();
	var imsEnabled;

	if ($("#imsEnabledUpdate").val() == "TRUE") {
		imsEnabled = 1;
	} else {
		imsEnabled = 0;
	}
	var data = null;

	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"imsi" : null,
		"subType" : subType,
		"subStatus" : null,
		"language" : null,
		"operatorId" : null,
		"profileId" : profileId,
		"imsEnabled" : imsEnabled
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/modifySubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			putMessageForAddSubscriber(result);
			ClearTextForModifySubscriber();
			$('#messageHeading').show();
		}
	});

}

function ClearTextForModifySubscriber() {
	$("#modifySubscriberMsisdn").val("");
	$("#modifySubType").val("");
	$("#profileIdUpdate").val("");
	$("#imsEnabledUpdate").val("");

}

function deleteSubscriber() {

	dataProcessStart();
	var msisdn = '1' + $("#deleteSubscriberMsisdn").val();
	/* var msisdn = $("#deleteSubscriberMsisdn").val(); */

	var data = null;

	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"imsi" : null,
		"subType" : null,
		"subStatus" : null,
		"language" : null,
		"operatorId" : null
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/deleteSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			putMessageForAddSubscriber(result);
			$('#messageHeading').show();

		}
	});

}

function deleteMmscSubscriber() {

	dataProcessStart();
	var msisdn = '1' + $("#mmscDeleteMsisdn").val();
	/* var msisdn = $("#mmscDeleteMsisdn").val(); */

	var data = null;

	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"imsi" : null,
		"subType" : null,
		"subStatus" : null,
		"language" : null,
		"operatorId" : null
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/deleteMmscSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			putMessageForAddSubscriber(result);
			$('#messageHeading').show();
			$('#msgInfo').hide();

		}
	});

}

function deleteSubscriberService() {

	dataProcessStart();
	var msisdn = '1' + $("#deletespecificSubscriberMsisdn").val();
	/* var msisdn = $("#deletespecificSubscriberMsisdn").val(); */
	var service = "SMSC";

	var data = null;

	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"service" : service,
		"imsi" : null,
		"subType" : null,
		"subStatus" : null,
		"language" : null,
		"operatorId" : null
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/deleteSubscriberServiceSpecific";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {

			putMessageForAddSubscriber(result);
			$('#messageHeading').show();

		}
	});

}

function getSubscriber() {

	var msisdn = '1' + $("#getsubscriberMsisdn").val();
	/* var msisdn = $("#getsubscriberMsisdn").val(); */

	var data = null;

	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"service" : null,
		"imsi" : null,
		"subType" : null,
		"subStatus" : null,
		"language" : null,
		"operatorId" : null
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/getSubscriber";
	dataProcessStart1();
	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {
			genrateUI(result);
			$('#getSubscriberContentHeading').show();
			$('#getSubscriberContentBody').show();
			$('#panelDiv').show();

		}
	});

}

function getSubscriberService() {

	var msisdn = '1' + $("#getServiceSubscriberMsisdn").val();
	/* var msisdn = $("#getServiceSubscriberMsisdn").val(); */
	var service = "SMSC";

	var data = null;

	dataProcessStart1();
	data = {
		"customerId" : null,
		"msisdn" : msisdn,
		"service" : service,
		"imsi" : null,
		"subType" : null,
		"subStatus" : null,
		"language" : null,
		"operatorId" : null
	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/getSubscriberServiceSpecific";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			displayErroGrid();
		},
		success : function(result) {

			genrateUIForService(result);
			$('#getSubscriberServiceContentHeading').show();
			$('#getSubscriberServiceContentBody').show();
			$('#panelDivService').show();

		}
	});

}
function genrateUIForService(result) {

	console.log(result);
	if (result.applicationError) {
		displayErroGridInternalError(result.applicationError.errorCode);
		return;
	}
	var subProfileHtml = "<table  data-toggle='table' class= 'display' data-height='180' id='servicedatatable'><thead><tr><th>Attribute</th>";
	subProfileHtml += "<th> Value </th>";
	subProfileHtml += "</tr></thead> <tbody>";

	if (result.MSISDN != undefined && result.IMSI != undefined) {

		// var subCounterHtml = "<table
		// class='table'><thead><tr><th>amfCounterLastModificationDate</th><th>
		// amfCounterUpperThr1</th><th> amfCounterUpperLimit</th><th>
		// amfCounterRegistrationDate</th><th> amfCounterPassValidity</th><th>
		// objectClass</th><th> amfCounterLowerThr1</th><th>
		// amfCounterFirstModificationDate</th>";
		// subCounterHtml += "</tr>";
		subProfileHtml += "<tr><td>" + "MSISDN" + "</td>";
		subProfileHtml += "<td>" + (result.MSISDN) + "</td>";
		subProfileHtml += "<tr><td>" + "IMSI" + "</td>";
		subProfileHtml += "<td>" + result.IMSI + "</td>";

		subProfileHtml += "<tr><td>" + "Sub Status" + "</td>";
		subProfileHtml += "<td>" + (result.SubStatus) + "</td>";
		subProfileHtml += "<tr><td>" + "Languahe" + "</td>";
		subProfileHtml += "<td>" + result.Language + "</td>";

		subProfileHtml += "<tr><td>" + "Sub Type" + "</td>";
		subProfileHtml += "<td>" + (result.SubType) + "</td>";
		subProfileHtml += "<tr><td>" + "Operator Id" + "</td>";
		subProfileHtml += "<td>" + result.OperatorId + "</td>";
	} else {
		// var subProfileHtml = "<table class='table'>";
		var subCounterHtml = "<table class='table'><thead><tr><th>amfCounterLastModificationDate</th><th> amfCounterUpperThr1</th><th> amfCounterUpperLimit</th><th> amfCounterRegistrationDate</th><th> amfCounterPassValidity</th><th> objectClass</th><th> amfCounterLowerThr1</th><th> amfCounterFirstModificationDate</th>";

		subProfileHtml += "<tr><td>" + "No Record Found" + "</td>";
		subCounterHtml += "</tr>";
	}

	subProfileHtml += "<tbody></table>";
	subCounterHtml += "</table>";

	$('#getSubscriberServiceContentHeading').text(
			"Content for Get Subsciber By Service Specific:");
	$('#getSubscriberServiceContentBody').empty();
	$('#getSubscriberServiceContentBody').html(subProfileHtml);

	$('#servicedatatable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,

		// "columns" : [ null, null],
		"jQueryUI" : true,
	});

	/*
	 * $('#extraInfoHeading').text("Content for DUS : Counters");
	 * $('#extraInfoBody').html(subCounterHtml); $('#extraInfo').show();
	 */
	// $("#searchText").empty();
	dataProcessEnd();

}

function putMessageForAddSubscriber(result) {

	if (result.applicationError) {
		displayErroGridInternalError(result.applicationError.errorCode);
		return;
	}
	if (result.success != null) {
		$('#messageHeading').text(result.success);
	} else {
		$('#messageHeading').text(result.faultstring);
	}
	$('#msgContentBody').hide();
	$('#msgInfo').hide();
	dataProcessEnd();

}

function validateInput(elementObj, id) {

	if (elementObj == "emailId") {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

		if (filter.test($("#"+id).val())) {
			return true;
		} else {
			alert('Invalid Email Address');
			$("#"+id).focus();
			return false;
		}
	} else if (elementObj == "msisdn" && $("#"+id).val().length != 10 && $("#"+id).val().length != 0) {
		alert('Invalid MSISDN');
		 $("#"+id).focus();
		return false;
	} else if (elementObj == "imsi" && $("#"+id).val().length > 15) {
		alert('Invalid IMSI');
		$("#"+id).focus();

		return false;
	} else {
		return true;
	}
}

/**
 * this method create dropdown list provided id is <Select> tag id.
 * 
 * @param id
 * @param options
 */

function createSearchDropDown(id, options) {
	var optionMenu = $("#" + id).empty();

	for ( var val in options) {
		$("<option />", {
			value : val,
			text : options[val]
		}).appendTo(optionMenu);
	}
	dataProcessEnd();
}

function dataProcessStart() {
	$('#loading').html(
			'<img src="/edat/resources/images/process.gif"> Loading...');
	$('#msgInfo').show();
}

function dataProcessStart1() {
	$('#loading').html(
			'<img src="/edat/resources/images/process.gif"> Loading...');

}

function dataProcessEnd() {
	$('#loading').empty();
}

function displayErroGrid() {
	$('#msgInfo').show();
	$('#msgContentHeading').show();
	$('#msgContentBody').show();
	$('#msgContentHeading').text("");
	$('#msgContentBody').empty();
	$('#msgContentBody').html("Node Not Reachable !");
	dataProcessEnd();
}

function displayErroGridInternalError(error) {
	$('#msgInfo').show();
	$('#msgContentHeading').show();
	$('#msgContentBody').show();
	$('#msgContentHeading').text("");
	$('#msgContentBody').empty();
	$('#msgContentBody').html("Customer not found!");
	dataProcessEnd();
}

function displayNoResultDiv() {
	$('#msgInfo').show();
	$('#msgContentHeading').show();
	$('#msgContentBody').show();
	$('#msgContentHeading').text("");
	$('#msgContentBody').empty();
	$('#msgContentBody').html("Customer not found!");
	dataProcessEnd();
}

function getMsgData() {
	var isIAMSearch = false;
	var searchUrl = "/edat/mdt/smsc";
	var searchCretria = null;
	var tokenParams = {};
	var urlParams = {};
	switch (msgUrl) {
	case "iam_search":
		tokenParams = getTokenParameters();
		if(tokenParams.error){
			errorMessage(tokenParams.error);
			return;
		}
		urlParams[$("#searchType").val()] = $("#mdtSearch").val();
		searchCretria = {
			"urlParams" : urlParams,
			"tokenParams" : tokenParams
		}
		searchUrl = "/edat/mdt/iamsearch";
		isIAMSearch = true;
		break;
	case "mmsc":
		searchUrl = "/edat/mdt/mmsc";
		break;

	case "imsi":
	case "customerId":
	case "emailId":
	default:
		searchUrl = "/edat/mdt/smsc";
		break;
	}

	dataProcessStart();
	$('#msgInfo').hide();
	
	sendDataRequest(searchUrl, searchCretria, isIAMSearch);

}

function sendDataRequest(searchUrl, data, isIAMSearch) {

	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log(xhr);
			console.log(ajaxOptions);
			console.log(thrownError);

			displayErroGrid();
		},
		success : function(result) {
			//generateSMCSDatatable(result);
			//console.log(result);
			if(isIAMSearch == true) displayCreatedTree(result);
			setTimeout(function() {
				$('#loading').empty();
			});
		}
	});
}

function genrateUI(result) {

	console.log(result);
	if (result.applicationError) {
		displayErroGridInternalError(result.applicationError.errorCode);
		return;
	}
	var subProfileHtml = "<table data-toggle='table' class= 'display' data-height='180' id ='messagedatatable'><thead><tr><th>Attribute</th>";
	subProfileHtml += "<th> Value </th>";
	subProfileHtml += "</tr></thead>  <tbody>";
	if (result.MSISDN != undefined && result.IMSI != undefined) {
		// var subProfileHtml = "<table data-toggle='table' class= 'display'
		// data-height='180' id
		// ='messagedatatable'><thead><tr><th>Attribute</th>";
		// subProfileHtml += "<th> Value </th>";
		// subProfileHtml += "</tr></thead> <tbody>";

		var subCounterHtml = "<table data-toggle='table' class= 'display' data-height='180' id='messagCounterdatatable'><thead><tr><th>amfCounterLastModificationDate</th><th> amfCounterUpperThr1</th><th> amfCounterUpperLimit</th><th> amfCounterRegistrationDate</th><th> amfCounterPassValidity</th><th> objectClass</th><th> amfCounterLowerThr1</th><th> amfCounterFirstModificationDate</th>";
		subCounterHtml += "</tr></thead>  <tbody>";

		subProfileHtml += "<tr><td>" + "MSISDN" + "</td>";
		subProfileHtml += "<td>" + (result.MSISDN) + "</td>";
		subProfileHtml += "<tr><td>" + "IMSI" + "</td>";
		subProfileHtml += "<td>" + result.IMSI + "</td>";

		subProfileHtml += "<tr><td>" + "Sub Status" + "</td>";
		subProfileHtml += "<td>" + (result.SubStatus) + "</td>";
		subProfileHtml += "<tr><td>" + "Languahe" + "</td>";
		subProfileHtml += "<td>" + result.Language + "</td>";

		subProfileHtml += "<tr><td>" + "Sub Type" + "</td>";
		subProfileHtml += "<td>" + (result.SubType) + "</td>";
		subProfileHtml += "<tr><td>" + "Operator Id" + "</td>";
		subProfileHtml += "<td>" + result.OperatorId + "</td>";
	}/*
		 * else { var subProfileHtml = "<table class='table'>"; var
		 * subCounterHtml = "<table class='table'><thead><tr><th>amfCounterLastModificationDate</th><th>
		 * amfCounterUpperThr1</th><th> amfCounterUpperLimit</th><th>
		 * amfCounterRegistrationDate</th><th> amfCounterPassValidity</th><th>
		 * objectClass</th><th> amfCounterLowerThr1</th><th>
		 * amfCounterFirstModificationDate</th>";
		 * 
		 * subProfileHtml += "<tr><td>" + "No Record Found" + "</td>";
		 * subCounterHtml += "</tr>"; }
		 */

	subProfileHtml += "</tbody></table>";
	// subCounterHtml += "</table>";

	$('#getSubscriberContentHeading').text("Content for Get Subscriber");
	$('#getSubscriberContentBody').empty();
	$('#getSubscriberContentBody').html(subProfileHtml);

	/*
	 * $('#extraInfoHeading').text("Content for DUS : Counters");
	 * $('#extraInfoBody').html(subCounterHtml); $('#extraInfo').show();
	 */
	// $("#searchText").empty();
	$('#messagedatatable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,

		"columns" : [ null, null

		],
		"jQueryUI" : true,
	});
	dataProcessEnd();

}

function generateSMCSDatatable(result) {

	if (result.applicationError) {
		displayErroGridInternalError(result.applicationError.errorCode);
		return;
	}

	var htmlTable = "<table data-toggle='table' class= 'display' data-height='180' id='smcsdatatable'><thead><tr><th>Attribute</th>";
	htmlTable += "<th> Value </th>";
	htmlTable += "</tr></thead><tbody>";
	var count = 0;
	for ( var key in result) {
		if (key == 'attributes' && result.hasOwnProperty('attributes')) {
			count = Object.keys(result.attributes).length;
			count = parseInt(count, 10);
			for ( var attributes in result.attributes) {
				var value = (result.attributes[attributes] ? result.attributes[attributes]
						: " ");
				if (value == "" || value == "null" || value == null) {
					value = "";
				}
				htmlTable += "<tr><td>" + attributes + "</td>";
				htmlTable += "<td>" + value + "</td>";
			}
		}
	}

	htmlTable += "</tbody></table>";
	$('#msgContentHeading').text("Search Result");

	// Clean Div
	$('#msgContentBody').empty();
	// Insert Table into DIV
	$('#msgContentBody').html(htmlTable);

	if (count == 0) {
		displayNoResultDiv();

	}

	$('#smcsdatatable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,

		// "columns" : [ null, null],
		"jQueryUI" : true,
	});

	$('#msgInfo').show();
	$('#msgContentHeading').show();
	$('#msgContentBody').show();

	dataProcessEnd();
}

function changeImage() {
	var image = document.getElementById('arrowdown');

	if (image.src.match("arrow-down")) {
		image.src = "/edat/resources/images/arrow-up-Icon.jpg";
	} else {
		image.src = "/edat/resources/images/arrow-down-Icon.jpg";
	}
}

function changeImageForMMSC() {
	var image = document.getElementById('arrowdownForMMSC');

	if (image.src.match("arrow-down")) {
		image.src = "/edat/resources/images/arrow-up-Icon.jpg";
	} else {
		image.src = "/edat/resources/images/arrow-down-Icon.jpg";
	}
}

function changeImageForSMSCAci() {
	var image = document.getElementById('arrowdownForSMSCAci');

	if (image.src.match("arrow-down")) {
		image.src = "/edat/resources/images/arrow-up-Icon.jpg";
	} else {
		image.src = "/edat/resources/images/arrow-down-Icon.jpg";
	}
}

function smscSearchModifySubscriber() {

	var searchUrl = "/edat/mdt/smsc";
	var msisdn = '1' + $("#modifySearchText").val();
	/* var msisdn = $("#modifySearchText").val(); */
	dataProcessStart();
	$('#msgInfo').hide();
	var data = null;
	data = {

		"msisdn" : msisdn

	};

	sendDataRequestForSearchModify(searchUrl, data);

}

function sendDataRequestForSearchModify(searchUrl, data) {

	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log(xhr);
			console.log(ajaxOptions);
			console.log(thrownError);

			displayErroGrid();
		},
		success : function(result) {
			generateSMCSModifyDiv(result);
			setTimeout(function() {
				$('#loading').empty();
			});
		}
	});
}

function generateSMCSModifyDiv(result) {

	if (result.attributes.telephoneNumber != "") {
		var telNo = result.attributes.telephoneNumber.slice(1);
		$("#smscModifyTelephone").val(telNo);
		$("#smscModifyProfileNo").val(result.attributes.smscsubprofilenumber);
		$("#smscModifySubAddress").val(result.attributes.smscsubaddress);
		$("#smscModifyUID").val(result.attributes.uid);
		var el = document.getElementById("smscModifyGender");
		for (var i = 0; i < el.options.length; i++) {
			if (el.options[i].text == result.attributes.gender) {
				el.selectedIndex = i;
				break;
			}
		}

		var el1 = document.getElementById("smscModifyPrepaid");
		for (var i = 0; i < el1.options.length; i++) {
			if (el1.options[i].text == result.attributes.prepaid) {
				el1.selectedIndex = i;
				break;
			}
		}

		// $("#smscModifyCN").val(result.attributes.cn);

		var eCn = document.getElementById("smscModifyCNSelect");
		for (var i = 0; i < eCn.options.length; i++) {
			if (eCn.options[i].text == result.attributes.cn) {
				eCn.selectedIndex = i;
				$("#smscModifyCN").css('visibility', 'hidden');
				break;
			} else {
				$("#smscModifyCN").val(result.attributes.cn);
				eCn.selectedIndex = eCn.options.length - 1;
				$("#smscModifyCN").css('visibility', 'visible');

			}
		}

		$("#smscModifyMail").val(result.attributes.mail);
		$("#smscModifyGivenName").val(result.attributes.givenName);
		// $("#smscModifySN").val(result.attributes.sn);
		var eSn = document.getElementById("smscModifySNSelect");
		for (var i = 0; i < eSn.options.length; i++) {
			if (eSn.options[i].text == result.attributes.sn) {
				eSn.selectedIndex = i;
				$("#smscModifySN").css('visibility', 'hidden');
				break;
			} else {
				$("#smscModifySN").val(result.attributes.sn);
				eSn.selectedIndex = eSn.options.length - 1;
				$("#smscModifySN").css('visibility', 'visible');

			}
		}

		$("#smscModifyUniqueIdentifier")
				.val(result.attributes.uniqueIdentifier);

		// $("#smscModifyUserAgentProfile").val(result.attributes.useragentprofile);
		var eUserAgentProfile = document
				.getElementById("smscModifyUserAgentProfileSelect");
		for (var i = 0; i < eUserAgentProfile.options.length; i++) {
			if (eUserAgentProfile.options[i].text == result.attributes.useragentprofile) {
				eUserAgentProfile.selectedIndex = i;
				$("#smscModifyUserAgentProfile").css('visibility', 'hidden');
				break;
			} else {
				$("#smscModifyUserAgentProfile").val(
						result.attributes.useragentprofile);
				eUserAgentProfile.selectedIndex = eUserAgentProfile.options.length - 1;
				$("#smscModifyUserAgentProfile").css('visibility', 'visible');

			}
		}

		// $("#smscModifyUserAgentString").val(result.attributes.useragentstring);

		var eUserAgentString = document
				.getElementById("smscModifyUserAgentStringSelect");
		for (var i = 0; i < eUserAgentString.options.length; i++) {
			if (eUserAgentString.options[i].text == result.attributes.useragentstring) {
				eUserAgentString.selectedIndex = i;
				$("#smscModifyUserAgentString").css('visibility', 'hidden');
				break;
			} else {
				$("#smscModifyUserAgentString").val(
						result.attributes.useragentstring);
				eUserAgentString.selectedIndex = eUserAgentString.options.length - 1;
				$("#smscModifyUserAgentString").css('visibility', 'visible');

			}
		}

		if (result.attributes.prepaid == "yes") {
			$("#smscModifyPAS").val(result.attributes.psa);

			// $("#smscModifyTplaCode").val(result.attributes.psatplacode);
			var eTplaCode = document.getElementById("smscModifyTplaCodeSelect");
			for (var i = 0; i < eTplaCode.options.length; i++) {
				if (eTplaCode.options[i].text == result.attributes.psatplacode) {
					eTplaCode.selectedIndex = i;
					$("#smscModifyTplaCode").css('visibility', 'hidden');
					break;
				} else {
					$("#smscModifyTplaCode").val(result.attributes.psatplacode);
					eTplaCode.selectedIndex = eTplaCode.options.length - 1;
					$("#smscModifyTplaCode").css('visibility', 'visible');

				}
			}

			$("#smscModifysubprepaid").val(result.attributes.smscsubprepaid);

			$("#smscModifyInpCode").val(result.attributes.psainpcode);
			$("#smscModifyPSAServices").val(result.attributes.psaservices);
			// $("#modifypsaforceflag").val(result.attributes.psaforceflag);

			var eForceFlag = document.getElementById("modifypsaforceflag");
			for (var i = 0; i < eForceFlag.options.length; i++) {
				if (eForceFlag.options[i].text == result.attributes.psaforceflag) {
					eForceFlag.selectedIndex = i;
					break;
				}
			}

			$("#modifypsaaction").val(result.attributes.psaforceflag);
			$("#modifypsamsisdn").val(result.attributes.psamsisdn);
			$('#prepaidModifyDiv').show();

		} else {
			$('#prepaidModifyDiv').hide();
		}

		// $("#smscModifyLanguage").val(result.attributes.language);

		var eLanguage = document.getElementById("smscModifyLanguageSelect");
		for (var i = 0; i < eLanguage.options.length; i++) {
			if (eLanguage.options[i].text == result.attributes.language) {
				eLanguage.selectedIndex = i;
				$("#smscModifyLanguage").css('visibility', 'hidden');
				break;
			} else {
				$("smscModifyLanguage").val(result.attributes.language);
				eLanguage.selectedIndex = eLanguage.options.length - 1;
				$("#smscModifyLanguage").css('visibility', 'visible');

			}
		}

		// $("#smscModifyMobileType").val(result.attributes.mobiletype);
		var eMobileType = document.getElementById("smscModifyMobileTypeSelect");
		for (var i = 0; i < eMobileType.options.length; i++) {
			if (eMobileType.options[i].text == result.attributes.mobiletype) {
				eMobileType.selectedIndex = i;
				$("#smscModifyMobileType").css('visibility', 'hidden');
				break;
			} else {
				$("smscModifyMobileType").val(result.attributes.mobiletype);
				eMobileType.selectedIndex = eMobileType.options.length - 1;
				$("#smscModifyMobileType").css('visibility', 'visible');

			}
		}

		$("#smscModifyPassword").val(result.attributes.password);
		$("#smscModifyInpCode").val(result.attributes.psainpcode);

		$('#modifySMSCSubscriberDiv').show();
		$('#SMSCSearchModifyDiv').hide();
		$('#msgInfo').hide();

	} else {
		$('#messageHeading').text("record not exist!!");
		$('#SMSCSearchModifyDiv').show();
		$('#modifySMSCSubscriberDiv').hide();
		$('#modifySearchText').val("");
		dataProcessEnd();
	}
}

function smscSearchDeleteSubscriber() {

	var searchUrl = "/edat/mdt/deleteSMSC";
	var msisdn = '1' + $("#deleteSearchText").val();
	/* var msisdn = $("#deleteSearchText").val(); */
	dataProcessStart();
	$('#msgInfo').hide();
	var data = null;
	data = {

		"msisdn" : msisdn

	};

	sendDataRequestForSearchDelete(searchUrl, data);

}

function sendDataRequestForSearchDelete(searchUrl, data) {

	var jsonData = JSON.stringify(data);
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log(xhr);
			console.log(ajaxOptions);
			console.log(thrownError);

			displayErroGrid();
		},
		success : function(result) {
			generateSMCSDeleteDiv(result);
			setTimeout(function() {
				$('#loading').empty();
			});
		}
	});
}

function generateSMCSDeleteDiv(result) {

	if (result.applicationError != null) {
		$('#messageHeading').text(result.applicationError.errorCode);
		$('#messageHeading').show();
		$('#deleteSearchText').val("");
	} else {
		$('#messageHeading').text(result.message);
		$('#messageHeading').show();
		$('#deleteSearchText').val("");
	}
	dataProcessEnd();
	dataProcessEnd();
}

function showPrepaidDiv(val) {

	if (val == "yes") {
		$('#prepaidDiv').hide();
		$('#smscUserAgentProfile').val("Control");

		var preElement = document.getElementById("smscPrepaid");

		for (var i = 0; i < preElement.options.length; i++) {
			if (preElement.options[i].text == "yes") {
				preElement.selectedIndex = i;
				break;
			}
		}

	}

}

function smscModifySubscriber() {
	var telePhoneNo = $("#smscModifyTelephone").val();
	var smscsubprofilenumber = $("#smscModifyProfileNo").val();
	var subAddress = $("#smscModifySubAddress").val();
	var uid = $("#smscModifyUID").val();
	var gender = $("#smscModifyGender").val();
	var prepaid = $("#smscModifyPrepaid").val();
	var cn = $("#smscModifyCN").val();
	var emailId = $("#smscModifyMail").val();
	var givenName = $("#smscModifyGivenName").val();
	var sn = $("#smscModifySN").val();
	var uniqIdentifier = $("#smscModifyUniqueIdentifier").val();
	var userAgentProfile = $("#smscModifyUserAgentProfile").val();
	var userAgentString = $("#smscModifyUserAgentString").val();
	var tplaCode = $("#smscModifyTplaCode").val();
	var smscsubprepaid = $("#smscModifysubprepaid").val();
	var psa = $("#smscModifyPAS").val();
	var inpCode = $("#smscModifyInpCode").val();
	var psaServices = $("#smscModifyPSAServices").val();
	var psaforceflag = $("#modifypsaforceflag").val();
	var psaaction = $("#modifypsaaction").val();
	var psamsisdn = $("#modifypsamsisdn").val();
	var language = $("#smscModifyLanguage").val();
	var mobileType = $("#smscModifyMobileType").val();
	var password = $("#smscModifyPassword").val();

	var data = null;
	dataProcessStart1();

	data = {
		"telePhoneNo" : "1" + telePhoneNo,
		"smscsubprofilenumber" : smscsubprofilenumber,
		"subAddress" : subAddress,
		"uid" : uid,
		"gender" : gender,
		"prepaid" : prepaid,
		"cn" : cn,
		"emailId" : emailId,
		"givenName" : givenName,
		"sn" : sn,
		"uniqIdentifier" : uniqIdentifier,
		"userAgentProfile" : userAgentProfile,
		"userAgentString" : userAgentString,
		"tplaCode" : tplaCode,
		"smscsubprepaid" : smscsubprepaid,
		"psa" : psa,
		"inpCode" : inpCode,
		"psaServices" : psaServices,
		"psaforceflag" : psaforceflag,
		"psaaction" : psaaction,
		"psamsisdn" : psamsisdn,
		"language" : language,
		"mobileType" : mobileType,
		"password" : password,

	};

	var searchUrl = null;
	var testdata = JSON.stringify(data);

	searchUrl = "/edat/mdt/modifySMSCSubscriber";

	$.ajax({
		url : searchUrl,
		data : testdata,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(result) {
			generatesmscModifySubscriber(result);
			$('#messageHeading').show();

		}
	});

}

function generatesmscModifySubscriber(result) {
	if (result.applicationError != null) {
		$('#messageHeading').text(result.applicationError.errorCode);
		$('#messageHeading').show();
		$('#SMSCSearchModifyDiv').show();
		$('#modifySMSCSubscriberDiv').hide();
		$('#modifySearchText').val("");
	} else {
		$('#messageHeading').text(result.message);
		$('#SMSCSearchModifyDiv').show();
		$('#messageHeading').show();
		$('#modifySMSCSubscriberDiv').hide();
		$('#modifySearchText').val("");
	}
	dataProcessEnd();
}

function validateSmscAddSub() {
	var msisdn = $("#smscTelephone").val();
	var language = $("#smscLanguage").val();
	var tplaCode = $("#smscTplaCode").val();
	var address = $("#smscSubAddress").val();
	var mobileType = $("#smscMobileType").val();
	var sn = $("#smscSN").val();
	var prepaid = $("#smscPrepaid").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {
			alert('Invalid Telephone Number');
			$("#smscTelephone").focus();
			return false;
		} else if (language == "") {
			alert("Please enter Language!");
			$("#smscLanguage").focus();
			return false;
		} else if (address == "") {
			alert("Please enter Subscriber Address!");
			$("#smscSubAddress").focus();
			return false;
		} else if (mobileType == "") {
			alert("Please enter Mobile Type!");
			$("#smscMobileType").focus();
			return false;
		} else if (sn == "") {
			alert("Please enter SN!");
			$("#smscSN").focus();
			return false;
		} else if (prepaid == "yes") {
			if (tplaCode == "") {
				alert("Please enter TPLACODE!");
				$("#smscTplaCode").focus();
				return false;
			} else {
				return true;
			}

		} else {
			return true;
		}
	} else {
		alert("Please enter Telephone Number");
		$("#smscTelephone").focus();
		return false;
	}
	return false;
}

function validateSmscModifySub() {
	var msisdn = $("#smscModifyTelephone").val();

	if (msisdn != "") {
		if (msisdn.length != 10 && msisdn.length != 0) {
			alert('Invalid Telephone Number');
			$("#smscModifyTelephone").focus();
			return false;
		} else {
			return true;
		}
		return true;
	} else {
		alert("Please enter Telephone Number");
		$("#smscModifyTelephone").focus();
		return false;
	}
	return false;
}

function validateInputForMsg(msisdn, focus) {

	if (msisdn.trim().length == 0) {
		alert('Please enter the msisdn');
		focus;
		return false;
	} else if (msisdn.length != 10 && msisdn.length != 0) {
		alert('Invalid MSISDN');
		focus;
		return false;
	}
	return true;
}

/*
 * function displayInternalErroGrid(key) { // $("#nap-tab").show();
 * //$('#napDataTable').hide(); $('#msgContentHeading').text("");
 * $('#msgContentBody').empty(); $('#msgContentBody').html("Customer Not Found
 * !!"); $("#msgInfo").show(); dataProcessEnd(); }
 * 
 * function displayErroGrid() { // $("#nap-tab").show();
 * $('#msgContentHeading').text(""); $('#msgContentBody').empty();
 * $('#msgContentBody').html("Node Not Reachable!!!"); $("#msgInfo").show();
 * dataProcessEnd(); }
 */

function addSupplierCNdisplay() {
	var dpt = document.getElementById("smscCNSelect");

	if (dpt.options[dpt.selectedIndex].value == "OTH") {
		$("#smscCN").css('visibility', 'visible');
		document.getElementById("smscCN").value = "";
	} else {
		$("#smscCN").css('visibility', 'hidden');
		document.getElementById("smscCN").value = dpt.options[dpt.selectedIndex].value;
	}

}

function addSupplierSNdisplay() {
	var dpt = document.getElementById("smscSNSelect");

	if (dpt.options[dpt.selectedIndex].value == "OTH") {
		$("#smscSN").css('visibility', 'visible');
		document.getElementById("smscSN").value = "";
	} else {
		$("#smscSN").css('visibility', 'hidden');
		document.getElementById("smscSN").value = dpt.options[dpt.selectedIndex].value;
	}

}

function addSupplierUserAgentProf(selectId, textFieldId) {
	var dpt = document.getElementById(selectId);

	if (dpt.options[dpt.selectedIndex].value == "OTH") {
		$("#" + textFieldId).css('visibility', 'visible');
		document.getElementById(textFieldId).value = "";
	} else {
		$("#" + textFieldId).css('visibility', 'hidden');
		document.getElementById(textFieldId).value = dpt.options[dpt.selectedIndex].value;
	}

}
