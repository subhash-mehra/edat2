var searchDropDownMenu = {"msisdn":"MSISDN(1)","imsi":"IMSI","iccid":"iccid"};
var searchCretria = {};
$(document).ready(function() {
	
	$("#otaTool").parent().addClass("active");
	createSearchDropDown(searchDropDownMenu);
	$("#otaInfo").hide();
	$("#subscription").addClass("selected");
	$("#searchText").keypress(function( event ) {
		if (event.keyCode == 13) {
			$("#search").click();
		}
	});

	$('#usdContentHeading, #usdContentBody').hide();

	$("#search").click(function() {
		
		if ($.trim($("#searchText").val()) == '') {
			alert("Please specify search criteria...");
			return;
		} else {
			var element = $("#searchType").val();
			var flag = validateInput(element);
			if (flag) {
				searchCretria = createRequestJson(element);
				sendSubscriptionRequest(searchCretria);
				
				$('#otaInfo, #usdContentHeading, #usdContentBody').show();
				//TODO - Remove this when fully implemented
				$("#subscription").addClass("selected").siblings().removeClass("selected");
			}
		}
	});


	$("#otaMenuItems li").click(function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		dataProcessStart();

		$('#usdContentHeading, #usdContentBody').show();
		switch (this.id) {

			case "activate" :
				resetUI();
				break;

			case "deacivate" :
				resetUI();
				break;

			case "update" :
				resetUI();
				break;
				
			case "subscription" :
			default :
				resetUI();
				break;
		}

	});

});

function createRequestJson(element){
	dataProcessStart();
	if (element == "msisdn") {
		searchCretria = {"msisdn":'1'+ $("#searchText").val(),"imsi":null,"iccid":null};
	}else if (element == "imsi") {
		searchCretria = {"msisdn":null,"imsi":$("#searchText").val(),"iccid":null};
	}else if (element == "iccid") {
		searchCretria = {"msisdn":null,"imsi":null,"iccid":$("#searchText").val()};
	} 
	
	return searchCretria;
}

function createSearchDropDown(options){

	var optionMenu = $("#searchType");
	optionMenu.empty();

	for(var val in options) {
	    $("<option />", {value: val, text: options[val]}).appendTo(optionMenu);
	}
}


function validateInput(elementObj) {

	if (elementObj == "emailId") {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test($("#searchText").val())) {
			return true;
		} else {
			alert('Invalid Email Address');
			return false;
		}
	} else if (elementObj == "msisdn" && $("#searchText").val().length != 10 ) {
		alert('Invalid MSISDN');
		return false;
	} else if (elementObj == "imsi" && $("#searchText").val().length > 15) {
		alert('Invalid IMSI');
		return false;
	} else {
		return true;
	}

}

function resetUI() {
	dataProcessStart();
	$("#searchText").val("");
	$("#otaInfo").hide();
	$('#otaContentHeading').text("");
	$('#otaContentBody').empty();
	$('#otaContentBody').html("");
	setTimeout(function(){dataProcessEnd();}, 500);
	

}
function dataProcessStart() {
	$('#loading').html('<img src="/edat/resources/images/process.gif"> Loading...');
}

function dataProcessEnd() {
	$('#loading').empty();
}

function displayErroGrid(result) {

	$('#otaContentHeading').text("OTA Subscription Details");
	$('#otaContentBody').empty();
	$('#otaContentBody').html("Customer Not Found !!");
	
	dataProcessEnd();

}

function sendSubscriptionRequest(searchCretria) {

	var searchUrl = "/edat/ota/otaSearch";
	/*var data = {
		"customerId" : null,
		"msisdn" : usdMSISDN,
		"imsi" : null,
		"ban" : null
	};*/

	var jsonData = JSON.stringify(searchCretria);
	
	$.ajax({
		url : searchUrl,
		data : jsonData,
		type : "POST",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$("#searchText").val("");
			displayErroGrid(thrownError);
		},
		success : function(result) {
			if(result.status =="ERROR"){
				displayErroGrid(result.status);
			}else{
			getSubscriptionDetails(result);
			}

		}
	});

}

function getSubscriptionDetails(result) {
	if (result =="" || result == null) {
		displayErroGrid(result);
		return;
	}

	var html = "<table data-toggle='table' class= 'display' data-height='180' id='otasubdatatable'><thead><tr><th>Attribute</th>";
	html += "<th> Value </th>";
	html += "</tr></thead> <tbody>";

	
	for ( var key in result) {
		if(key == "statusCode" || key == "applicationError") continue;
			html += "<tr><td>"+key + "</td><td>"+result[key]+"</td></tr>";
	}
	
	html += "</tbody></table>";
	
	$('#otaContentHeading').text("OTA Subscription Details");

	// Clean Div
	$('#otaContentBody').empty();
	// Insert Table into DIV
	$('#otaContentBody').html(html);
	
	
	$('#otasubdatatable').dataTable({
		"scrollY" : "300px",
		"toolbar" : false,
		"scrollCollapse" : true,
		"paging" : false,

		"columns" : [ null, null

		],
		"jQueryUI" : true,
	});
	dataProcessEnd();
}
