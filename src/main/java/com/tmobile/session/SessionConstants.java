package com.tmobile.session;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class SessionConstants implements Serializable{
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(SessionConstants.class);
	private String LDAP_SERVER;
	
	/* For USD Module */
	private String LDAP_SERVER_NAP;
	private String LDAP_SERVER_DUS;
	private String LDAP_SERVERL_PCRF;
	private String LDAP_SERVER_UMA;
	private String LDAP_SERVER_HLR;
	private String LDAP_SERVER_EIR;
	private String LDAP_SERVER_HSS;
	private String LDAP_SERVER_PCRF_SESSION;
	
	private String LDAP_SERVER_EIR_ADD_IMSI;
	private String LDAP_SERVER_UNLOCK_IMSI_IMEI; 
	private String LDAP_SERVER_TAC_SEARCH;
	private String LDAP_SERVER_ADD_TAC;
	private String LDAP_SERVER_ENABLE_MMS;	
	/* For CNTDB module */
	private String LDAP_SERVER_CNTB;
	private String CNTB_StringUrl; // For WebService / HOST	
	/* For DUS Update */
	private String LDAP_SERVER_MSISDN;
	private String LDAP_SERVER_BAN;
	/* For Messaging Module */
	private String LDAP_SERVER_IAM;
	private String LDAP_SERVER_SMSC;
	private String SERVER_MMSC;
	private String SMSC_API_URL;	
	/* For OTA module */
	private String OTA_SERVER_URL;	
	/* For INSDP module */
	private String INSDP_API_URL;
	private String INSDP_USER_PASS;
	private String INSDP_StringUrl;
	private String INSDP_HOST;	
	/* for Gflex Module */
	private String TELNET_HOST;	
	private int DEFAULT_SERVER = 0;
	public String getLDAP_SERVER() {
		return LDAP_SERVER;
	}
	public void setLDAP_SERVER(String lDAP_SERVER) {
		LDAP_SERVER = lDAP_SERVER;
	}
	public String getLDAP_SERVER_NAP() {
		return LDAP_SERVER_NAP;
	}
	public void setLDAP_SERVER_NAP(String lDAP_SERVER_NAP) {
		LDAP_SERVER_NAP = lDAP_SERVER_NAP;
	}
	public String getLDAP_SERVER_DUS() {
		return LDAP_SERVER_DUS;
	}
	public void setLDAP_SERVER_DUS(String lDAP_SERVER_DUS) {
		LDAP_SERVER_DUS = lDAP_SERVER_DUS;
	}
	public String getLDAP_SERVERL_PCRF() {
		return LDAP_SERVERL_PCRF;
	}
	public void setLDAP_SERVERL_PCRF(String lDAP_SERVERL_PCRF) {
		LDAP_SERVERL_PCRF = lDAP_SERVERL_PCRF;
	}
	public String getLDAP_SERVER_UMA() {
		return LDAP_SERVER_UMA;
	}
	public void setLDAP_SERVER_UMA(String lDAP_SERVER_UMA) {
		LDAP_SERVER_UMA = lDAP_SERVER_UMA;
	}
	public String getLDAP_SERVER_HLR() {
		return LDAP_SERVER_HLR;
	}
	public void setLDAP_SERVER_HLR(String lDAP_SERVER_HLR) {
		LDAP_SERVER_HLR = lDAP_SERVER_HLR;
	}
	public String getLDAP_SERVER_EIR() {
		return LDAP_SERVER_EIR;
	}
	public void setLDAP_SERVER_EIR(String lDAP_SERVER_EIR) {
		LDAP_SERVER_EIR = lDAP_SERVER_EIR;
	}
	public String getLDAP_SERVER_HSS() {
		return LDAP_SERVER_HSS;
	}
	public void setLDAP_SERVER_HSS(String lDAP_SERVER_HSS) {
		LDAP_SERVER_HSS = lDAP_SERVER_HSS;
	}
	public String getLDAP_SERVER_PCRF_SESSION() {
		return LDAP_SERVER_PCRF_SESSION;
	}
	public void setLDAP_SERVER_PCRF_SESSION(String lDAP_SERVER_PCRF_SESSION) {
		LDAP_SERVER_PCRF_SESSION = lDAP_SERVER_PCRF_SESSION;
	}
	public String getLDAP_SERVER_EIR_ADD_IMSI() {
		return LDAP_SERVER_EIR_ADD_IMSI;
	}
	public void setLDAP_SERVER_EIR_ADD_IMSI(String lDAP_SERVER_EIR_ADD_IMSI) {
		LDAP_SERVER_EIR_ADD_IMSI = lDAP_SERVER_EIR_ADD_IMSI;
	}
	public String getLDAP_SERVER_UNLOCK_IMSI_IMEI() {
		return LDAP_SERVER_UNLOCK_IMSI_IMEI;
	}
	public void setLDAP_SERVER_UNLOCK_IMSI_IMEI(String lDAP_SERVER_UNLOCK_IMSI_IMEI) {
		LDAP_SERVER_UNLOCK_IMSI_IMEI = lDAP_SERVER_UNLOCK_IMSI_IMEI;
	}
	public String getLDAP_SERVER_TAC_SEARCH() {
		return LDAP_SERVER_TAC_SEARCH;
	}
	public void setLDAP_SERVER_TAC_SEARCH(String lDAP_SERVER_TAC_SEARCH) {
		LDAP_SERVER_TAC_SEARCH = lDAP_SERVER_TAC_SEARCH;
	}
	public String getLDAP_SERVER_ADD_TAC() {
		return LDAP_SERVER_ADD_TAC;
	}
	public void setLDAP_SERVER_ADD_TAC(String lDAP_SERVER_ADD_TAC) {
		LDAP_SERVER_ADD_TAC = lDAP_SERVER_ADD_TAC;
	}
	public String getLDAP_SERVER_ENABLE_MMS() {
		return LDAP_SERVER_ENABLE_MMS;
	}
	public void setLDAP_SERVER_ENABLE_MMS(String lDAP_SERVER_ENABLE_MMS) {
		LDAP_SERVER_ENABLE_MMS = lDAP_SERVER_ENABLE_MMS;
	}
	public String getLDAP_SERVER_CNTB() {
		return LDAP_SERVER_CNTB;
	}
	public void setLDAP_SERVER_CNTB(String lDAP_SERVER_CNTB) {
		LDAP_SERVER_CNTB = lDAP_SERVER_CNTB;
	}
	public String getCNTB_StringUrl() {
		return CNTB_StringUrl;
	}
	public void setCNTB_StringUrl(String cNTB_StringUrl) {
		CNTB_StringUrl = cNTB_StringUrl;
	}
	public String getLDAP_SERVER_MSISDN() {
		return LDAP_SERVER_MSISDN;
	}
	public void setLDAP_SERVER_MSISDN(String lDAP_SERVER_MSISDN) {
		LDAP_SERVER_MSISDN = lDAP_SERVER_MSISDN;
	}
	public String getLDAP_SERVER_BAN() {
		return LDAP_SERVER_BAN;
	}
	public void setLDAP_SERVER_BAN(String lDAP_SERVER_BAN) {
		LDAP_SERVER_BAN = lDAP_SERVER_BAN;
	}
	public String getLDAP_SERVER_IAM() {
		return LDAP_SERVER_IAM;
	}
	public void setLDAP_SERVER_IAM(String lDAP_SERVER_IAM) {
		LDAP_SERVER_IAM = lDAP_SERVER_IAM;
	}
	public String getLDAP_SERVER_SMSC() {
		return LDAP_SERVER_SMSC;
	}
	public void setLDAP_SERVER_SMSC(String lDAP_SERVER_SMSC) {
		LDAP_SERVER_SMSC = lDAP_SERVER_SMSC;
	}
	public String getSERVER_MMSC() {
		return SERVER_MMSC;
	}
	public void setSERVER_MMSC(String sERVER_MMSC) {
		SERVER_MMSC = sERVER_MMSC;
	}
	public String getSMSC_API_URL() {
		return SMSC_API_URL;
	}
	public void setSMSC_API_URL(String sMSC_API_URL) {
		SMSC_API_URL = sMSC_API_URL;
	}
	public String getOTA_SERVER_URL() {
		return OTA_SERVER_URL;
	}
	public void setOTA_SERVER_URL(String oTA_SERVER_URL) {
		OTA_SERVER_URL = oTA_SERVER_URL;
	}
	public String getINSDP_API_URL() {
		return INSDP_API_URL;
	}
	public void setINSDP_API_URL(String iNSDP_API_URL) {
		INSDP_API_URL = iNSDP_API_URL;
	}
	public String getINSDP_USER_PASS() {
		return INSDP_USER_PASS;
	}
	public void setINSDP_USER_PASS(String iNSDP_USER_PASS) {
		INSDP_USER_PASS = iNSDP_USER_PASS;
	}
	public String getINSDP_StringUrl() {
		return INSDP_StringUrl;
	}
	public void setINSDP_StringUrl(String iNSDP_StringUrl) {
		INSDP_StringUrl = iNSDP_StringUrl;
	}
	public String getINSDP_HOST() {
		return INSDP_HOST;
	}
	public void setINSDP_HOST(String iNSDP_HOST) {
		INSDP_HOST = iNSDP_HOST;
	}
	public String getTELNET_HOST() {
		return TELNET_HOST;
	}
	public void setTELNET_HOST(String tELNET_HOST) {
		TELNET_HOST = tELNET_HOST;
	}
	public int getDEFAULT_SERVER() {
		return DEFAULT_SERVER;
	}
	public void setDEFAULT_SERVER(int dEFAULT_SERVER) {
		DEFAULT_SERVER = dEFAULT_SERVER;
	}
}
