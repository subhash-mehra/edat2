package com.tmobile.session;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.tmobile.edat.util.Constants;

public class SessionConstantsFactory implements Serializable{
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(SessionConstantsFactory.class);
	
	public static String getSessionConstant(String val,SessionConstants sessionConstants){
		if(sessionConstants!=null){
		if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_NAP")){
			return sessionConstants.getLDAP_SERVER_NAP()!=null?sessionConstants.getLDAP_SERVER_NAP() : Constants.LDAP_SERVER_NAP;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_DUS")){
			return sessionConstants.getLDAP_SERVER_DUS()!=null?sessionConstants.getLDAP_SERVER_DUS() : Constants.LDAP_SERVER_DUS;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVERL_PCRF")){
			return sessionConstants.getLDAP_SERVERL_PCRF()!=null?sessionConstants.getLDAP_SERVERL_PCRF() : Constants.LDAP_SERVERL_PCRF;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_UMA")){
			return sessionConstants.getLDAP_SERVER_UMA()!=null?sessionConstants.getLDAP_SERVER_UMA() : Constants.LDAP_SERVER_UMA;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_HLR")){
			return sessionConstants.getLDAP_SERVER_HLR()!=null?sessionConstants.getLDAP_SERVER_HLR() : Constants.LDAP_SERVER_HLR;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_EIR")){
			return sessionConstants.getLDAP_SERVER_EIR()!=null?sessionConstants.getLDAP_SERVER_EIR() : Constants.LDAP_SERVER_EIR;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_HSS")){
			return sessionConstants.getLDAP_SERVER_HSS()!=null?sessionConstants.getLDAP_SERVER_HSS() : Constants.LDAP_SERVER_HSS;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_PCRF_SESSION")){
			return sessionConstants.getLDAP_SERVER_PCRF_SESSION()!=null?sessionConstants.getLDAP_SERVER_PCRF_SESSION() : Constants.LDAP_SERVER_PCRF_SESSION;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_EIR_ADD_IMSI")){
			return sessionConstants.getLDAP_SERVER_EIR_ADD_IMSI()!=null?sessionConstants.getLDAP_SERVER_EIR_ADD_IMSI() : Constants.LDAP_SERVER_EIR_ADD_IMSI;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_UNLOCK_IMSI_IMEI")){
			return sessionConstants.getLDAP_SERVER_UNLOCK_IMSI_IMEI()!=null?sessionConstants.getLDAP_SERVER_UNLOCK_IMSI_IMEI() : Constants.LDAP_SERVER_UNLOCK_IMSI_IMEI;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_TAC_SEARCH")){
			return sessionConstants.getLDAP_SERVER_TAC_SEARCH()!=null?sessionConstants.getLDAP_SERVER_TAC_SEARCH() : Constants.LDAP_SERVER_TAC_SEARCH;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_ADD_TAC")){
			return sessionConstants.getLDAP_SERVER_ADD_TAC()!=null?sessionConstants.getLDAP_SERVER_ADD_TAC() : Constants.LDAP_SERVER_ADD_TAC;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_ENABLE_MMS")){
			return sessionConstants.getLDAP_SERVER_ENABLE_MMS()!=null?sessionConstants.getLDAP_SERVER_ENABLE_MMS() : Constants.LDAP_SERVER_ENABLE_MMS;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_CNTB")){
			return sessionConstants.getLDAP_SERVER_CNTB()!=null?sessionConstants.getLDAP_SERVER_CNTB() : Constants.LDAP_SERVER_CNTB;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_IAM")){
			return sessionConstants.getLDAP_SERVER_IAM()!=null?sessionConstants.getLDAP_SERVER_IAM() : Constants.LDAP_SERVER_IAM;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_SMSC")){
			return sessionConstants.getLDAP_SERVER_SMSC()!=null?sessionConstants.getLDAP_SERVER_SMSC() : Constants.LDAP_SERVER_SMSC;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_MSISDN")){
			return sessionConstants.getLDAP_SERVER_MSISDN()!=null?sessionConstants.getLDAP_SERVER_MSISDN() : Constants.LDAP_SERVER_MSISDN;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_BAN")){
			return sessionConstants.getLDAP_SERVER_BAN()!=null?sessionConstants.getLDAP_SERVER_BAN() : Constants.LDAP_SERVER_BAN;
		}else if(val!=null && val.equalsIgnoreCase("CNTB_StringUrl")){
			return sessionConstants.getCNTB_StringUrl()!=null?sessionConstants.getCNTB_StringUrl() : Constants.CNTB_StringUrl;
		}else if(val!=null && val.equalsIgnoreCase("OTA_SERVER_URL")){
			return sessionConstants.getOTA_SERVER_URL()!=null?sessionConstants.getOTA_SERVER_URL() : Constants.OTA_SERVER_URL;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_API_URL")){
			return sessionConstants.getINSDP_API_URL()!=null?sessionConstants.getINSDP_API_URL() : Constants.INSDP_API_URL;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_HOST")){
			return sessionConstants.getINSDP_HOST()!=null?sessionConstants.getINSDP_HOST() : Constants.INSDP_HOST;
		}else if(val!=null && val.equalsIgnoreCase("TELNET_HOST")){
			return sessionConstants.getTELNET_HOST()!=null?sessionConstants.getTELNET_HOST() : Constants.TELNET_HOST;
		}else if(val!=null && val.equalsIgnoreCase("SERVER_MMSC")){
			return sessionConstants.getSERVER_MMSC()!=null?sessionConstants.getSERVER_MMSC() : Constants.SERVER_MMSC;
		}else if(val!=null && val.equalsIgnoreCase("SMSC_API_URL")){
			return sessionConstants.getSMSC_API_URL()!=null?sessionConstants.getSMSC_API_URL() : Constants.SMSC_API_URL;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_USER_PASS")){
			return sessionConstants.getINSDP_USER_PASS()!=null?sessionConstants.getINSDP_USER_PASS() : Constants.INSDP_USER_PASS;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_StringUrl")){
			return sessionConstants.getINSDP_StringUrl()!=null?sessionConstants.getINSDP_StringUrl() : Constants.INSDP_StringUrl;
		}
		}else{
			getConstant(val);
		}
		return null;
	}
	
	public static String getConstant(String val){

		if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_NAP")){
			return Constants.LDAP_SERVER_NAP;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_DUS")){
			return Constants.LDAP_SERVER_DUS;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVERL_PCRF")){
			return Constants.LDAP_SERVERL_PCRF;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_UMA")){
			return Constants.LDAP_SERVER_UMA;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_HLR")){
			return Constants.LDAP_SERVER_HLR;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_EIR")){
			return Constants.LDAP_SERVER_EIR;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_HSS")){
			return Constants.LDAP_SERVER_HSS;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_PCRF_SESSION")){
			return Constants.LDAP_SERVER_PCRF_SESSION;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_EIR_ADD_IMSI")){
			return Constants.LDAP_SERVER_EIR_ADD_IMSI;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_UNLOCK_IMSI_IMEI")){
			return Constants.LDAP_SERVER_UNLOCK_IMSI_IMEI;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_TAC_SEARCH")){
			return Constants.LDAP_SERVER_TAC_SEARCH;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_ADD_TAC")){
			return Constants.LDAP_SERVER_ADD_TAC;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_ENABLE_MMS")){
			return Constants.LDAP_SERVER_ENABLE_MMS;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_CNTB")){
			return Constants.LDAP_SERVER_CNTB;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_IAM")){
			return Constants.LDAP_SERVER_IAM;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_SMSC")){
			return Constants.LDAP_SERVER_SMSC;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_MSISDN")){
			return Constants.LDAP_SERVER_MSISDN;
		}else if(val!=null && val.equalsIgnoreCase("LDAP_SERVER_BAN")){
			return Constants.LDAP_SERVER_BAN;
		}else if(val!=null && val.equalsIgnoreCase("CNTB_StringUrl")){
			return Constants.CNTB_StringUrl;
		}else if(val!=null && val.equalsIgnoreCase("OTA_SERVER_URL")){
			return Constants.OTA_SERVER_URL;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_API_URL")){
			return Constants.INSDP_API_URL;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_HOST")){
			return Constants.INSDP_HOST;
		}else if(val!=null && val.equalsIgnoreCase("TELNET_HOST")){
			return Constants.TELNET_HOST;
		}else if(val!=null && val.equalsIgnoreCase("SERVER_MMSC")){
			return Constants.SERVER_MMSC;
		}else if(val!=null && val.equalsIgnoreCase("SMSC_API_URL")){
			return Constants.SMSC_API_URL;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_USER_PASS")){
			return Constants.INSDP_USER_PASS;
		}else if(val!=null && val.equalsIgnoreCase("INSDP_StringUrl")){
			return Constants.INSDP_StringUrl;
		}else{		
		return null;
		}
	}
}
