package com.tmobile.session;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.tmobile.edat.admin.model.ServerConfig;
import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.filter.store.LinkData;

public class SessionEventTrack implements HttpSessionListener{
	private static Logger log = Logger.getLogger(SessionEventTrack.class);
	
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		// TODO Auto-generated method stub
		log.info("sessionCreated method of SessionEventTrack");
		 HttpSession session = sessionEvent.getSession();
		 SessionConstants sessionConstants = new SessionConstants();
		 session.setAttribute("SessionConstants", sessionConstants);
		 try{
		 log.info("Loading ApplicationContex...");
         ApplicationContext ctx = 
               WebApplicationContextUtils.
                     getWebApplicationContext(session.getServletContext());
         log.info("Loading DataSource Bean...");
         DataSource dataSource =  (DataSource) ctx.getBean("dataSource");
         JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
         log.info("Loading server_config from database...");
 		List<ServerConfig>  serverConfig = jdbcTemplate.query("select * from server_config where status='y'",
 				new RowMapper<ServerConfig>() {
 					@Override
 					public ServerConfig mapRow(ResultSet rs, int rownumber)
 							throws SQLException {
 						ServerConfig s = new ServerConfig();
 						s.setId(rs.getInt(1));
 						s.setServerName(rs.getString(2));
 						s.setServerIPDetails(rs.getString(3));
 						return s;
 					}
 				});
 		 session.setAttribute("serverConfigList", serverConfig); 		
		 }catch(Exception e){}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		// TODO Auto-generated method stub
		log.info("sessionDestroyed method of SessionEventTrack");
		 HttpSession session = sessionEvent.getSession();
		 log.info("Loading ApplicationContex...");
         ApplicationContext ctx = 
               WebApplicationContextUtils.
                     getWebApplicationContext(session.getServletContext());
         log.info("Loading DataSource Bean...");
         DataSource dataSource = 
                     (DataSource) ctx.getBean("dataSource");
         if(session.getAttribute("linkData")!=null && session.getAttribute("userAccess")!=null){
        	 log.info("Saving Session Data in DB...");
 			if(saveSessionData((LinkData)session.getAttribute("linkData"), (UserAccess)session.getAttribute("userAccess"),dataSource)){
 			log.info("Session Data Saved");
 			}else{
 				log.info("Session Data Not Saved");
 			}
 		}	
         session.removeAttribute("SessionConstants");
         session.removeAttribute("userAccess");
         session.removeAttribute("linkData");
	}
	
	public boolean saveSessionData(LinkData linkData,UserAccess userAccess,DataSource dataSource){
		boolean flag=false;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();		
		String currentDate = dateFormat.format(date);
		
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);	
			//int nextId = jdbcTemplate.queryForInt("select manage_session_data_seq.nextval from dual");
			int nextId = jdbcTemplate.queryForInt("select manage_session_data_seq.nextval from dual");
			log.info("saveSessionData NextSequence "+nextId);
			String sql = "INSERT INTO manage_session_data (sess_id ,login_id,link_hit_counter_detail,logout_time) VALUES (?,?, ?, ?)";
			log.info("saveSessionData Successfully !! ");
			Object[] params = new Object[] {nextId, userAccess.getUserId(), linkData, date };
			int[] types = new int[] {Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.DATE };
			int row = jdbcTemplate.update(sql, params, types);
			flag =true;
		} catch (Exception e) {
			log.info("saveSessionData Problem !! "+e.getMessage());
			// TODO: handle exception
		}
		return flag;
	}  

}
