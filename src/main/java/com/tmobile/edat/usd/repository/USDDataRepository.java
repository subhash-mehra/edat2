package com.tmobile.edat.usd.repository;

import java.util.List;

import com.tmobile.edat.usd.dto.request.ImeiTacSearchRequest;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EirDataResponse;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.HssDataResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.PcrfDataResponse;
import com.tmobile.edat.usd.dto.response.UmaDataResponse;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;

public interface USDDataRepository {

	public NapDataResponse doNAPSearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public DusDataResponse doDUSSearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public PcrfDataResponse doPCRFSearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public UmaDataResponse doUMASearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public HssDataResponse doHSSSearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public HlrDataResponse doHLRSearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public EirDataResponse doEIRSearch(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public ImeiTacDataResponse doAddEIR(int search_type, String key1,
			String key2, String key3,String key4,String key5,String key6, SessionConstants sessionConstants) throws UserException;;

	//public ImeiTacDataResponse doImsiUnlock(int search_type, String key, SessionConstants sessionConstants) throws UserException;;

	public ImeiTacDataResponse doImsiUnlock(int search_type, ImeiTacSearchRequest imeiTacSearchRequest) throws UserException;

	public ImeiTacDataResponse doGetTac(int search_type, String key, SessionConstants sessionConstants) throws UserException;

	public ImeiTacDataResponse doAddTac(int search_type, String key,
			String deviceManufacturer, String deviceModel, String deviceBands,
			String deviceScreenResolution, String deviceTechnologies, SessionConstants sessionConstants) throws UserException;

	public ImeiTacDataResponse doEnableMMS(int search_type, String key,
			String enableOption,SessionConstants sessionConstants) throws UserException;
	
	public DusDataResponse getUid(int search_type, String key, SessionConstants sessionConstants) throws UserException;
	
	

}
