package com.tmobile.edat.usd.repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.LdapName;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import sun.nio.cs.ext.MSISO2022JP;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.cntb.repository.CNTBDBDataRepositoryHelper;
import com.tmobile.edat.usd.dto.request.ImeiTacSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EirDataResponse;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.HssDataResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.PcrfDataResponse;
import com.tmobile.edat.usd.dto.response.UmaDataResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

@Repository("USDDataRepository")
public class USDDataHandler implements USDDataRepository {

	private static final Logger logger = Logger.getLogger(USDDataHandler.class);

	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";

	private static String SECURITY_PRINCIPAL_NAP = "cn=napAppUser";
	private static String SECURITY_CREDENTIALS_NAP = "Cir0Hygu";
	private static String SECURITY_PRINCIPAL_DEFAULT = "cn=napprovuser";
	private static String SECURITY_CREDENTIALS_DEFAULT = "siemens";
	private static String PROVIDER_URL_NAP = "ldap://"
			+ Constants.LDAP_SERVER_NAP + "/dc=%s,dc=C-NTDB";

	private String PROVIDER_URL_NAP_WITH_UID;

	private String PROVIDER_URL_DUS_WITH_UID;

	private String PROVIDER_URL_PCRF_WITH_UID;

	private String PROVIDER_URL_DEFAULT;

	private static String SECURITY_PRINCIPAL_DUS = "cn=amfUser";
	private static String SECURITY_CREDENTIALS_DUS = "siemens";
	private static String PROVIDER_URL_DUS = "ldap://"
			+ Constants.LDAP_SERVER_DUS + "/dc=%s,dc=C-NTDB";

	private static String SECURITY_PRINCIPAL_PCRF_SESSION = "iam";
	private static String SECURITY_CREDENTIALS_PCRF_SESSION = "aaa";
	private String PROVIDER_URL_PCRF_SESSION;// "ldap://10.169.56.3:389/";

	private static String SECURITY_PRINCIPAL_PCRF = "cn=sdcUser";
	private static String SECURITY_CREDENTIALS_PCRF = "siemens";
	private static String PROVIDER_URL_PCRF = "ldap://"
			+ Constants.LDAP_SERVERL_PCRF + "/dc=%s,dc=C-NTDB";

	private static String SECURITY_PRINCIPAL_UMA = "cn=napprovuser";
	private static String SECURITY_CREDENTIALS_UMA = "siemens";
	private String PROVIDER_URL_UMA;

	private static String SECURITY_PRINCIPAL_HLR = "cn=napprovuser";
	private static String SECURITY_CREDENTIALS_HLR = "siemens";
	private String PROVIDER_URL_HLR;

	private static String SECURITY_PRINCIPAL_EIR = "cn=napprovuser";
	private static String SECURITY_CREDENTIALS_EIR = "siemens";
	private String PROVIDER_URL_EIR;

	private String PROVIDER_URL_ADD_IMSI_EIR;

	private String PROVIDER_URL_UNLOCK_IMSI_IMEI;

	private String PROVIDER_URL_TAC_SEARCH;

	private String PROVIDER_URL_ADD_TAC;

	private String PROVIDER_URL_ENABLE_MMS;

	private static String SECURITY_PRINCIPAL_HSS = "cn=napprovuser";
	private static String SECURITY_CREDENTIALS_HSS = "siemens";
	private String PROVIDER_URL_HSS;

	String SEARCH_QUERY_PCRF_SESSION = "anything callingstationid=%s";
	String SEARCH_FILTER_PCRF_SESSION = "(callingstationid=%s)";// 14254444604

	private static String SECURITY_PRINCIPAL_SEARCH_TAC = "cn=napprovuser";

	private static String SECURITY_CREDENTIALS_SEARCH_TAC = "siemens";

	String SEARCH_QUERY_ADD_EIR = "subdata=policing,ds=eir,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";

	String SEARCH_QUERY_UNLOCK_IMSI_IMEI = "subdata=policing,ds=eir,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";

	String SEARCH_QUERY_SEARCH_TAC = "tac=%s,ds=device,o=DEFAULT,dc=C-NTDB";
	String SEARCH_QUERY_ADD_TAC = "tac=%s,ds=device,o=DEFAULT,dc=C-NTDB";
	String SEARCH_QUERY_ENABLE_MMS = "tac=%s,ds=device,o=DEFAULT,dc=C-NTDB";

	String SEARCH_QUERY_EIR = "ds=eir,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";
	String SEARCH_QUERY_HLR = "ds=hlr,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";
	String SEARCH_QUERY_HLR_2 = "subdata=non3gpp,o=hlr,imsi=310310990005142,ds=hlr,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT,DC=C-NTDB";
	String SEARCH_QUERY_HSS = "ds=hss,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";
	String SEARCH_QUERY_UMA = "ds=uma,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";
	String SEARCH_QUERY_DUS = "ds=amf,subdata=services, %s=%s";
	String SEARCH_QUERY_DEFAULT = "%s=%s";

	String SEARCH_QUERY_DEFAULT_FOR_NAP = "ds=nap,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";

	String SEARCH_QUERY_DEFAULT_FOR_DUS = "ds=amf,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";
	String SEARCH_QUERY_DEFAULT_FOR_PCRF = "ds=sdc,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";

	String SEARCH_FILTER_DEFAULT = "(objectclass=%s)";

	String SEARCH_QUERY_DEFAULT_FOR_ALL_INPUT = "%s=%s,dc=%s";
	// By Tirath

	private String PROVIDER_URL_CNTB_UID;
	String SEARCH_QUERY_UID = "uid=%s,ds=SUBSCRIBER,o=DEFAULT";
	private static String SECURITY_PRINCIPAL_CNTB_UID = "cn=napprovuser";
	private static String SECURITY_CREDENTIALS_CNTB = "siemens";

	public String getUidFromDefaultInput(int search_type, String key,
			SessionConstants sessionConstants) {

		System.out
				.println("*************** Starts: getUidFromDefaultInput **************** ");

		String uid = "";
		String providerURL = "";
		String query = "";
		String filter = "";

		LdapContext ctx = null;
		SearchControls sc = new SearchControls();

		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			query = String.format(SEARCH_QUERY_DEFAULT_FOR_ALL_INPUT, "msisdn",
					key, "msisdn");
			break;

		case Constants.SEARCH_TYPE_IMSI:
			query = String.format(SEARCH_QUERY_DEFAULT_FOR_ALL_INPUT, "imsi",
					key, "imsi");
			break;

		case Constants.SEARCH_TYPE_CUSTID:
			query = String.format(SEARCH_QUERY_DEFAULT_FOR_ALL_INPUT,
					"CustomerId", key, "CustomerId");
			break;

		default:
			break;

		}

		System.out.println("query====> " + query);
		PROVIDER_URL_DEFAULT = "ldap://"
				+ SessionConstantsFactory.getSessionConstant("LDAP_SERVER_NAP",
						sessionConstants) + "/DC=C-NTDB";
		providerURL = String.format(PROVIDER_URL_DEFAULT);
		System.out.println("providerURL :" + providerURL);
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		filter = String.format(SEARCH_FILTER_DEFAULT, "*");

		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_DEFAULT, SECURITY_CREDENTIALS_DEFAULT,
					providerURL));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);
				SearchResult rs = result.next();

				Attributes attrs = rs.getAttributes();
				System.out.println("attrs=====> " + attrs);
				NamingEnumeration<String> atyrs = attrs.getIDs();
				while (atyrs != null && atyrs.hasMoreElements()) {
					try {
						String id = atyrs.next().toString();
						if ("uid".equals(id)) {
							uid = attrs.get(id).get().toString();
							System.out.println("uid ::"
									+ attrs.get(id).get().toString());
						}

					} catch (Exception e) {
						e.getMessage();
					}
				}

			} catch (Exception e) {
				e.getMessage();
			}
		}
		System.out
				.println("*************** Ends: getUidFromDefaultInput **************** "
						+ uid);
		return uid;
	}

	@Override
	public NapDataResponse doNAPSearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {
		NapDataResponse napDataResponse = null;
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";
		System.out
				.println("##################### Starts: NAP Search ########################");

		/*
		 * switch (search_type) { case Constants.SEARCH_TYPE_MSISDN :
		 * providerURL = String.format(PROVIDER_URL_NAP, "msisdn");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DEFAULT, "msisdn", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, "*"); break; case
		 * Constants.SEARCH_TYPE_IMSI : providerURL =
		 * String.format(PROVIDER_URL_NAP, "imsi");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DEFAULT, "imsi", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, "*"); break; case
		 * Constants.SEARCH_TYPE_CUSTID : providerURL =
		 * String.format(PROVIDER_URL_NAP, "CustomerId");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DEFAULT, "CustomerId", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, "*"); break; case
		 * Constants.SEARCH_TYPE_UID : providerURL =
		 * String.format(PROVIDER_URL_NAP, "SUBSCRIBER");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DEFAULT, "uid", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, "*"); break;
		 * 
		 * default : break; }
		 */
	 try {
		uid = getUidFromDefaultInput(search_type, key, sessionConstants);
		napDataResponse = new NapDataResponse();
		//if (uid != null && !"".equalsIgnoreCase(uid)) {
			PROVIDER_URL_NAP_WITH_UID = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_NAP", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_NAP_WITH_UID);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			query = String.format(SEARCH_QUERY_DEFAULT_FOR_NAP, uid);

			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_DEFAULT, SECURITY_CREDENTIALS_DEFAULT,
						providerURL));
			 
			if (ctx != null) {
				try {

					// System.out.println("NAP search query :" + query +
					// "\nfilter :"+ filter);

					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);

					// System.out.println("NAP search result :"+
					// result.hasMoreElements());
					// System.err.println("----------------------------------------");
					// Create Fresh Object of NapDataResponse to fill in data

					while (result != null && result.hasMoreElements()) {

						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						// System.out.println("DN has " + dn.size() +
						// " RDNs: ");
						/*
						 * for (int i = 0; i < dn.size(); i++) {
						 * System.out.println("RDNs : " + dn.get(i)); }
						 */
						Attributes attrs = rs.getAttributes();
						Attribute attribute = attrs.get("objectClass");
						// 1. Filter Profile Data
						if (attribute.contains("napAccount")) {
							USDDataRepositoryHelper.fillInNapProfileData(
									napDataResponse, dn, attrs);
						}
						// 2. Filter Partners Data
						else if (attribute.contains("napPartner")) {
							USDDataRepositoryHelper.fillInNapPartnersData(
									napDataResponse, dn, attrs);
						}
						// 3. Filter SOCs Data
						else if (attribute.contains("napSubscriberSoc")) {
							USDDataRepositoryHelper.fillInNapSOCData(
									napDataResponse, dn, attrs);
						}
						/*
						 * System.err.println(" attribute : " +
						 * attribute.get());
						 * 
						 * NamingEnumeration<String> atyrs = attrs.getIDs();
						 * while (atyrs != null && atyrs.hasMoreElements()) {
						 * try { String id = atyrs.next().toString();
						 * System.out.println("Attr : " + id + " - " +
						 * attrs.get(id)); } catch (Exception e) {
						 * System.err.println("error : " + e.toString()); }
						 * 
						 * } System.err
						 * .println("----------------------------------------");
						 */
					}

				} catch (NamingException e) {
					napDataResponse.setApplicationError(new ApplicationError(e
							.getMessage()));
					//e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						napDataResponse.setApplicationError(new ApplicationError(e
								.getMessage()));
					}
				}
			}
		}catch (UserException e1) {
			throw new UserException(e1.getMessage());
		}
		System.out
				.println("##################### Ends: NAP Search ########################");
		return napDataResponse;
	}

	@Override
	public DusDataResponse doDUSSearch(int search_type, String key,
			SessionConstants sessionConstants) {
		DusDataResponse dusDataResponse = null;
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";
		System.out
				.println("##################### Starts: DUS Search ########################");
		/*
		 * 
		 * switch (search_type) { case Constants.SEARCH_TYPE_MSISDN: providerURL
		 * = String.format(PROVIDER_URL_DUS, "msisdn");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DUS, "msisdn", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, "*");//
		 * "(|(objectclass=cntdbDsElement)(objectclass=amfCounter)(objectclass=amfIntAccount)(objectclass=v-amfAccount))"
		 * ; break; case Constants.SEARCH_TYPE_IMSI: providerURL =
		 * String.format(PROVIDER_URL_DUS, "imsi");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DUS, "imsi", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, "*"); break; case
		 * Constants.SEARCH_TYPE_CUSTID:
		 * 
		 * break; case Constants.SEARCH_TYPE_UID:
		 * 
		 * break;
		 * 
		 * default: break; }
		 */
		try {
		uid = getUidFromDefaultInput(search_type, key, sessionConstants);
		dusDataResponse = new DusDataResponse();
		
			PROVIDER_URL_DUS_WITH_UID = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_DUS", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_DUS_WITH_UID);
			System.out.println("providerURL :::::::" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			System.out.println("uid  for Dus Search ::::: " + uid);
			query = String.format(SEARCH_QUERY_DEFAULT_FOR_DUS, uid);

			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_DUS, SECURITY_CREDENTIALS_DUS,
						providerURL));
			
			if (ctx != null) {
				try {

					System.out.println("DUS search query :" + query
							+ "\nfilter :" + filter);
					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);
					System.out.println("DUS search result :"
							+ result.hasMoreElements());
					System.err
							.println("----------------------------------------");
					while (result != null && result.hasMoreElements()) {

						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());

						/*
						 * System.out.println("DN has " + dn.size() +
						 * " RDNs: "); for (int i = 0; i < dn.size(); i++) {
						 * System.out.println("RDNs : " + dn.get(i)); }
						 */
						Attributes attrs = rs.getAttributes();

						Attribute attribute = attrs.get("objectClass");
						// 1. Filter Profile Data
						if (attribute.contains("v-amfAccount")) {

							USDDataRepositoryHelper.fillInDUSData(
									dusDataResponse, dn, attrs);
						}
						// 2. Filter Counters
						else if (attribute.contains("amfCounter")) {
							USDDataRepositoryHelper.fillInDUSCounterData(
									dusDataResponse, dn, attrs);
						}

						/*
						 * NamingEnumeration<String> atyrs = attrs.getIDs();
						 * while (atyrs != null && atyrs.hasMoreElements()) {
						 * try { String id = atyrs.next().toString();
						 * System.out.println("Attr : " + id + " - " +
						 * attrs.get(id)); } catch (Exception e) {
						 * System.err.println("error : " + e.toString()); } }
						 */

					}
					System.out.println("DUS response :"
							+ dusDataResponse.toString());
				} catch (NamingException e) {

					dusDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
					//e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						dusDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
					}
				}
			}
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			dusDataResponse.setApplicationError(new ApplicationError(e1.getMessage()));
		}
		System.out
				.println("##################### Ends: DUS Search ########################");
		return dusDataResponse;
	}
	
	
	@Override
	public DusDataResponse getUid(int search_type, String key,
			SessionConstants sessionConstants) {
		DusDataResponse dusDataResponse = null;
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";
		System.out
				.println("##################### Starts: Get UID Search ########################");
		
		try {
		uid = getUidFromDefaultInput(search_type, key, sessionConstants);
		dusDataResponse = new DusDataResponse();
		dusDataResponse.setUid(uid);
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			dusDataResponse.setApplicationError(new ApplicationError(e1.getMessage()));
		}
		System.out
				.println("##################### Ends:  Get UID Search ########################");
		return dusDataResponse;
	}

	@Override
	public PcrfDataResponse doPCRFSearch(int search_type, String key,
			SessionConstants sessionConstants) {
		PcrfDataResponse pcrfdata = null;
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";
		String msisdn = null;
		System.out
				.println("##################### Starts: PCRF Search ########################");

		/*
		 * switch (search_type) { case Constants.SEARCH_TYPE_MSISDN :
		 * providerURL = String.format(PROVIDER_URL_PCRF, "msisdn");
		 * System.out.println("providerURL :" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		 * 
		 * query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", key); filter =
		 * String.format(SEARCH_FILTER_DEFAULT, ""); break; case
		 * Constants.SEARCH_TYPE_IMSI : break; case Constants.SEARCH_TYPE_CUSTID
		 * : break; case Constants.SEARCH_TYPE_UID :
		 * 
		 * uid = getUidFromDefaultInput(search_type, key); providerURL =
		 * String.format(PROVIDER_URL_PCRF_WITH_UID);
		 * System.out.println("providerURL :::::::" + providerURL);
		 * sc.setSearchScope(SearchControls.SUBTREE_SCOPE); query =
		 * String.format(SEARCH_QUERY_DEFAULT_FOR_PCRF, uid);
		 * 
		 * filter = String.format(SEARCH_FILTER_DEFAULT, "");
		 * 
		 * break; default : break; }
		 */
	try {
		uid = getUidFromDefaultInput(search_type, key, sessionConstants);
		pcrfdata = new PcrfDataResponse();
		
			msisdn = getMsisdn(uid, sessionConstants);
			
			PROVIDER_URL_PCRF_WITH_UID = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_DUS", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_PCRF_WITH_UID);
			System.out.println("providerURL :::::::" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT_FOR_PCRF, uid);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

		
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_PCRF, SECURITY_CREDENTIALS_PCRF,
						providerURL));
		
			if (ctx != null) {

				try {
					System.out.println("PCRF search query :" + query
							+ "\nfilter :" + filter);
					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);
					System.out.println("PCRF search result :"
							+ result.hasMoreElements());
					System.err
							.println("----------------------------------------");
					Map<String, String> attributes = new HashMap<String, String>();
					while (result != null && result.hasMoreElements()) {
						// FIXME read the result and return the response
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						System.out.println("DN has " + dn.size() + " RDNs: ");
						for (int i = 0; i < dn.size(); i++) {

							String dnstr = dn.get(i);
							System.out.println("DN has " + dnstr);
							if (dnstr.contains("uid")) {
								pcrfdata.setUid(dnstr.substring(
										dnstr.indexOf("=") + 1, dnstr.length()));

							}
						}
						Attributes attrs = rs.getAttributes();
						NamingEnumeration<String> atyrs = attrs.getIDs();
						while (atyrs != null && atyrs.hasMoreElements()) {
							try {
								String id = atyrs.next().toString();
								if (id.equalsIgnoreCase("napCustomerId")) {
									pcrfdata.setCustomerId(attrs.get(id).get()
											.toString());
								} else {
									attributes.put(id, attrs.get(id).get()
											.toString());
								}
							} catch (Exception e) {
								System.err.println("error : " + e.toString());
							}
						}

					}
					pcrfdata.setAttributes(attributes);

					try
					{
					pcrfdata.setActiveSession(getPCRFSessionInfo(msisdn,
							sessionConstants));
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				} catch (NamingException e) {
					//e.printStackTrace();
					pcrfdata.setApplicationError(new ApplicationError(e.getMessage()));
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						pcrfdata.setApplicationError(new ApplicationError(e.getMessage()));
					}
				}
			}
		} 
		catch (UserException e1) {
			pcrfdata.setApplicationError(new ApplicationError(e1.getMessage()));
		}

		System.out.println("PCRF response :" + pcrfdata.toString());
		System.out
				.println("##################### Ends: PCRF Search ########################");
		return pcrfdata;
	}

	public String getMsisdn(String uid, SessionConstants sessionConstants) throws UserException {

		String providerURL = "";
		String msisdn = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List<String> attributeList = new ArrayList<String>();
		try {
			PROVIDER_URL_CNTB_UID = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_CNTB", sessionConstants)
					+ "/dc=C-NTDB";
			providerURL = PROVIDER_URL_CNTB_UID;

			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_UID, uid);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
		

			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_CNTB_UID, SECURITY_CREDENTIALS_CNTB,
					providerURL));
		

		if (ctx != null) {
			try {

				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);

				while (result != null && result.hasMoreElements()) {

					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());

					Attributes attrs = rs.getAttributes();

					Attribute attribute = attrs.get("objectClass");

					if (attribute.contains("MSISDNINNSS")) {
						msisdn = USDDataRepositoryHelper.getMsisdn(dn, attrs,
								attribute);
					}
				}
			} catch (Exception e) {
				throw new UserException(e.getMessage());
			}
		}
		}catch (UserException e1) {
		   throw new UserException(e1.getMessage());
		}

		return msisdn;

	}

	@Override
	public UmaDataResponse doUMASearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {

		UmaDataResponse umaData = null;
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		
	try {
		System.out
				.println("##################### Starts: UMA Search ########################");
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			break;
		case Constants.SEARCH_TYPE_IMSI:
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			break;
		case Constants.SEARCH_TYPE_UID:
			PROVIDER_URL_UMA = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_UMA", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_UMA, "SUBSCRIBER");
			System.out.println("ProviderURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			query = String.format(SEARCH_QUERY_UMA, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "umaSubscriberData");
			break;
		default:
			break;
		}
		umaData = new UmaDataResponse();
		
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_UMA, SECURITY_CREDENTIALS_UMA, providerURL));
		
		if (ctx != null) {
			
			try {
				System.out.println("UMA search query :" + query + "\nfilter :"
						+ filter + "\n" + ctx.getNameInNamespace());
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);

				System.out.println("UMA search result :"
						+ result.hasMoreElements());
				System.err.println("----------------------------------------");
				Map<String, String> attributes = new HashMap<String, String>();
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					System.out.println("output  L" + rs.toString());
					LdapName dn = new LdapName(rs.getNameInNamespace());
					System.out.println("DN has " + dn.size() + " RDNs: ");
					for (int i = 0; i < dn.size(); i++) {
						String dnstr = dn.get(i);
						if (dnstr.contains("uid")) {
							umaData.setUid(dnstr.substring(
									dnstr.indexOf("=") + 1, dnstr.length()));
						}
					}
					Attributes attrs = rs.getAttributes();
					NamingEnumeration<String> atyrs = attrs.getIDs();
					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							attributes.put(id, attrs.get(id).get().toString());
							// System.out.println("Attr : " + id + " - "
							// + attrs.get(id));
						} catch (Exception e) {
							System.err.println("error : " + e.toString());
						}
					}

				}
				umaData.setAttributes(attributes);

			} catch (NamingException e) {
				//e.printStackTrace();
				umaData.setApplicationError(new ApplicationError(e.getMessage()));
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					umaData.setApplicationError(new ApplicationError(e.getMessage()));
				}
			}
		}
		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
		}
		//System.out.println("UMA response :" + umaData.toString());
		System.out
				.println("##################### Ends: UMA Search ########################");
		return umaData;
	}

	@Override
	public HssDataResponse doHSSSearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {
		HssDataResponse hssData = null;
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String[] attrIDs = { "globalFilterIds", "publicId", "privateId",
				"filterName", "imsi" };
		List<String>hssImpiList =new ArrayList<String>();
		List<String>hssImpuList =new ArrayList<String>();
		//sc.setReturningAttributes(attrIDs);
		String query = "((objectClass=hssServiceProfile)||(objectClass=hssUserFilterCriteria)||(objectClass=hssIrs)||(objectClass=hssImpi )||(objectClass=hssImpu))";
		
		String filter = "";
	try {
		System.out
				.println("##################### Starts: HSS Search ########################");
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			break;
		case Constants.SEARCH_TYPE_IMSI:
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			break;
		case Constants.SEARCH_TYPE_UID:
			PROVIDER_URL_HSS = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_HSS", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_HSS, "SUBSCRIBER");
			System.out.println("ProviderURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			query = String.format(SEARCH_QUERY_HSS, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
			break;
		default:
			break;
		}
		hssData = new HssDataResponse();
		
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_HSS, SECURITY_CREDENTIALS_HSS, providerURL));
		
		if (ctx != null) {
			
			try {
				System.out.println("HSS search query :" + query + "\nfilter :"
						+ filter + "\n" + ctx.getNameInNamespace());
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);

				System.out.println("HSS search result :"
						+ result.hasMoreElements());
				System.err.println("----------------------------------------");
				Map<String, String> attributes =  null;
				
				Map<String, String> hssServiceProfile =  new HashMap<String, String>();
				Map<String, String> hssUserFilterCriteria =  new HashMap<String, String>();
				Map<String, String> hssIrs =  new HashMap<String, String>();
				Map<String, String> hssImpi =  new HashMap<String, String>();
				Map<String, String> hssImpu =  new HashMap<String, String>();
				Map<String, String> hssNotification =  new HashMap<String, String>();
				while (result != null && result.hasMoreElements()) {
					
				
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					// System.out.println("output  L" + rs.toString());
					LdapName dn = new LdapName(rs.getNameInNamespace());
					// System.out.println("DN has " + dn.size() + " RDNs: ");
					for (int i = 0; i < dn.size(); i++) {
						String dnstr = dn.get(i);
						if (dnstr.contains("uid")) {
							hssData.setUid(dnstr.substring(
									dnstr.indexOf("=") + 1, dnstr.length()));
						}
					}
					Attributes attrs = rs.getAttributes();
					Attribute attribute = attrs.get("objectClass");
					
					
					NamingEnumeration<String> atyrs = null;
					if (attribute.contains("hssServiceProfile")) {
						
						hssServiceProfile.put("hssServiceProfile", ""+attrs.get("globalFilterIds"));
						//attributes = hssServiceProfile;
					}
					if (attribute.contains("hssUserFilterCriteria")) {
						atyrs = attrs.getIDs();
						attributes = hssUserFilterCriteria;
					}
					if (attribute.contains("hssIrs")) {
						atyrs = attrs.getIDs();
						attributes = hssIrs;
					}
					if (attribute.contains("hssImpi")) {
						atyrs = attrs.getIDs();
						attributes = hssImpi;
						hssImpiList.add(""+hssImpi);
					}
					if (attribute.contains("hssImpu")) {
						atyrs = attrs.getIDs();
						attributes = hssImpu;
						hssImpuList.add(""+hssImpu);
					}
					if (attribute.contains("hssNotification")) {
						atyrs = attrs.getIDs();
						attributes = hssNotification;
						
                   }
					
					//NamingEnumeration<String> atyrs = attrs.getIDs();
					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							//System.out.println("id===================="+id+"value========================"+attrs.get(id).get().toString());
							attributes.put(id, attrs.get(id).get().toString());
						} catch (Exception e) {
							System.err.println("error : " + e.toString());
						}
					}
					
					
					
				}
				
				System.out.println("hssImpiList=================="+hssImpiList);
				System.out.println("hssImpuList=================="+hssImpuList);
				hssImpi.put("hssImpiList", ""+hssImpiList);
				hssImpu.put("hssImpuList", ""+hssImpuList);
				hssData.setHssImpi(hssImpi);
				hssData.setHssImpu(hssImpu);
				hssData.setHssIrs(hssIrs);
				hssData.setHssNotification(hssNotification);
				hssData.setHssServiceProfile(hssServiceProfile);
				hssData.setHssUserFilterCriteria(hssUserFilterCriteria);
			} catch (NamingException e) {
				//e.printStackTrace();
				hssData.setApplicationError(new ApplicationError(e.getMessage()));
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
				hssData.setApplicationError(new ApplicationError(e.getMessage()));
				}
			}
		}
		} catch (UserException e1) {
			  throw new UserException(e1.getMessage());
		}
		System.out.println("HSS data Response :" + hssData.toString());
		System.out
				.println("##################### Ends: HSS Search ########################");
		return hssData;
	}

	@Override
	public HlrDataResponse doHLRSearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {
		HlrDataResponse hlrdata = new HlrDataResponse();
		List<String>gprsList=new ArrayList<String>();
		List<String>pdnContextList=new ArrayList<String>();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String[] attrIDs = { "sgsnNumber ", "vlrNumber", "sgsnAddress",
				"mscNumber", "odbBaoc", "odbHplmn", "isActiveIMSI",
				"actIMSIGprs", "clipOverr", "clirRestrOpt", "refroamAreaName",
				"IMEISV", "cfdFTNOforCFU", "cfdFTNOforCFB", "cfdFTNOforCFNRY",
				"cfdFTNOforCFNRC", "pdpContextId", "objectClass", "pdpType",
				"accPointName", "refqOfServName", "mmeIdentity",
				"pdnAccPointName", "refLteQOfServName", "vplmnAddressAllowed",
				"aaaServerAddress" };
		//sc.setReturningAttributes(attrIDs);
		String query = "((objectClass=SUBINNSS)||(objectClass=EPSDATA)||(objectClass=NON3GPPDATA )||(objectClass=NON3GPPDATA )||(objectClass=GPRSDATA)||(objectClass=PDNCONTEXT  ))";
		String filter = "";
	try {	
		System.out
				.println("##################### Starts: HLR Search ########################");
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			break;
		case Constants.SEARCH_TYPE_IMSI:
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			break;
		case Constants.SEARCH_TYPE_UID:
			PROVIDER_URL_HLR = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_HLR", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_HLR, "SUBSCRIBER");
			System.out.println("ProviderURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			query = String.format(SEARCH_QUERY_HLR, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
			break;
		default:
			break;
		}
		
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_HLR, SECURITY_CREDENTIALS_HLR, providerURL));
		
		if (ctx != null) {
			try {
				System.out.println("HLR search query :" + query + "\nfilter :"
						+ filter + "\n" + ctx.getNameInNamespace());
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);

				System.out.println("HLR search result :"
						+ result.hasMoreElements());
				System.out.println("----------------------------------------");
				
				Map<String, String> attributes =  null;
				Map<String, String> subinnss =  new HashMap<String, String>();
				Map<String, String> epsdata =  new HashMap<String, String>();
				Map<String, String> non3gppdata =  new HashMap<String, String>();
				Map<String, String> gprsdata =  new HashMap<String, String>();
				Map<String, String> pdncontext =  new HashMap<String, String>();
				
				
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					// System.out.println("output  :" + rs.toString());
					LdapName dn = new LdapName(rs.getNameInNamespace());
					// System.out.println("DN has " + dn.size() + " RDNs: ");
					for (int i = 0; i < dn.size(); i++) {
						String dnstr = dn.get(i);
						if (dnstr.contains("uid")) {
							hlrdata.setUid(dnstr.substring(
									dnstr.indexOf("=") + 1, dnstr.length()));
						}
					}
					Attributes attrs = rs.getAttributes();
					Attribute attribute = attrs.get("objectClass");
					System.out.println("========================"+attrs);
					
					NamingEnumeration<String> atyrs = null;
					if (attribute.contains("SUBINNSS")) {
						atyrs = attrs.getIDs();
						attributes = subinnss;
					}
					if (attribute.contains("EPSDATA")) {
						
						atyrs = attrs.getIDs();
						
						attributes = epsdata;
					}
					if (attribute.contains("NON3GPPDATA ")) {
						atyrs = attrs.getIDs();
						attributes = non3gppdata;
					}
					if (attribute.contains("GPRSDATA")) {
						atyrs = attrs.getIDs();
						attributes = gprsdata;
						gprsList.add(""+gprsdata);
					}
					if (attribute.contains("PDNCONTEXT")) {
						atyrs = attrs.getIDs();
						attributes = pdncontext;
						pdnContextList.add(""+pdncontext);
					}
					
					
					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							attributes.put(id, attrs.get(id).get().toString());
						} catch (Exception e) {
							System.err.println("error : " + e.toString());
						}
					}

					// 1. Filter Partners Data
				/*	if (attribute.contains("GPRSDATA")) {
						USDDataRepositoryHelper
								.fillGprsData(hlrdata, dn, attrs);
					}
					// 2. Filter EPSDATA Data
					else if (attribute.contains("EPSDATA")) {
						USDDataRepositoryHelper.fillEpsData(hlrdata, dn, attrs);
					}
					// 3. Filter Profile Data
					else {
						USDDataRepositoryHelper.fillInHLRData(hlrdata, dn,
								attrs);
					} */

					/*
					 * NamingEnumeration<String> atyrs = attrs.getIDs(); while
					 * (atyrs != null && atyrs.hasMoreElements()) { try { String
					 * id = atyrs.next().toString();
					 * System.out.println("Attr : " + id + " - " +
					 * attrs.get(id)); } catch (Exception e) {
					 * System.err.println("error : " + e.toString()); } }
					 */

				}
				System.out.println("gprsList============="+gprsList);
				System.out.println("pdnContextList============="+pdnContextList);
				gprsdata.put("gprsList", ""+gprsList);
				pdncontext.put("pdnContextList", ""+pdnContextList);
				
				
				hlrdata.setEpsdata(epsdata);
				hlrdata.setGprsdata(gprsdata);
				hlrdata.setNon3gppdata(non3gppdata);
				hlrdata.setPdncontext(pdncontext);
				hlrdata.setSubinnss(subinnss);

			} catch (NamingException e) {
				hlrdata.setApplicationError(new ApplicationError(e.getMessage()));
				e.printStackTrace();
				
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					hlrdata.setApplicationError(new ApplicationError(e.getMessage()));
				}
			}
		}
	  } catch (UserException e1) {
		  throw new UserException(e1.getMessage());
	}
		//System.out.println("HLR response data :" + hlrdata.toString());
		System.out
				.println("##################### Ends: HLR Search ########################");
		System.out.println("GPRSList==========="+gprsList);
		return hlrdata;
	}

	@Override
	public EirDataResponse doEIRSearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {
		EirDataResponse eirData = new EirDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String[] attrIDs = { "eirImeiHistory" };
		sc.setReturningAttributes(attrIDs);
		String query = "";
		String filter = "";
	try {
		System.out
				.println("##################### Starts: EIR Search ########################");
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			break;
		case Constants.SEARCH_TYPE_IMSI:
			PROVIDER_URL_EIR = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_EIR", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_EIR, "SUBSCRIBER");
			System.out.println("ProviderURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			query = String.format(SEARCH_QUERY_EIR, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			break;
		case Constants.SEARCH_TYPE_UID:
			PROVIDER_URL_EIR = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_EIR", sessionConstants) + "/DC=C-NTDB";
			providerURL = String.format(PROVIDER_URL_EIR, "SUBSCRIBER");
			System.out.println("ProviderURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			query = String.format(SEARCH_QUERY_EIR, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
			break;
		default:
			break;
		}
		
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_EIR, SECURITY_CREDENTIALS_EIR, providerURL));
		
		if (ctx != null) {
			try {
				System.out.println("EIR search query :" + query + "\nfilter :"
						+ filter + "\n" + ctx.getNameInNamespace().toString());
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, null);
				
				System.out.println("=========================================="+result.hasMoreElements());
				System.out.println("EIR search result :"
						+ result.hasMoreElements());
				System.err.println("----------------------------------------");
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					//LdapName dn = new LdapName(rs.getNameInNamespace());
					Attributes attrs1 = rs.getAttributes();
					Attribute attribute = attrs1.get("subPolicing");
					
					
					System.out.println("111111111111111111111111111111111111111111111---"+attrs1.get("objectClass"));
					System.out.println("attribute---"+attribute);
					System.out.println("output  L" + rs.toString());
					LdapName dn = new LdapName(rs.getNameInNamespace());
					System.out.println("DN has " + dn.size() + " RDNs: ");
					for (int i = 0; i < dn.size(); i++) {
						String dnstr = dn.get(i);
						if (dnstr.contains("uid")) {
							eirData.setUid(dnstr.substring(
									dnstr.indexOf("=") + 1, dnstr.length()));
						}
					}
					
					eirData.setUid(key);
					Attributes attrs = rs.getAttributes();
					System.out.println("============================="+attrs);
					// Attribute = attrs.get("eirImeiHistory");

					NamingEnumeration<String> atyrs = attrs.getIDs();
					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							if (id.equalsIgnoreCase("eirImeiHistory")) {
								String value = attrs.get(id).toString();
								value = value.substring(value.indexOf(":") + 1,
										value.length());
								String imei[] = value.split(",");
								if (imei != null) {
									for (int i = 0; i < imei.length; i++) {
										eirData.addImeiHistory(imei[i]);
									}
								}
							}
							
							if (id.equalsIgnoreCase("eirFastBlackList")) {
								String value = attrs.get(id).toString();
								value = value.substring(value.indexOf(":") + 1,
										value.length());
								String imei[] = value.split(",");
								if (imei != null) {
									for (int i = 0; i < imei.length; i++) {
										eirData.setEirFastBlackList(imei[i]);
									}
								}
							}
							
							if (id.equalsIgnoreCase("eirLockImsiImei")) {
								String value = attrs.get(id).toString();
								value = value.substring(value.indexOf(":") + 1,
										value.length());
								String imei[] = value.split(",");
								if (imei != null) {
									for (int i = 0; i < imei.length; i++) {
										eirData.setEirLockImsiImei(imei[i]);
									}
								}
							}
							
							if (id.equalsIgnoreCase("eirDualImsi")) {
								String value = attrs.get(id).toString();
								value = value.substring(value.indexOf(":") + 1,
										value.length());
								String imei[] = value.split(",");
								if (imei != null) {
									for (int i = 0; i < imei.length; i++) {
										eirData.setEirDualImsi(imei[i]);
									}
								}
							}
						} catch (Exception e) {
							System.err.println("error : " + e.toString());
						}
					}

				}

			} catch (NamingException e) {
				eirData.setApplicationError(new ApplicationError(e.getMessage()));
			//	e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					eirData.setApplicationError(new ApplicationError(e.getMessage()));
				}
			}
		}
	   } catch (UserException e1) {
		 throw new UserException(e1.getMessage());
	}
		System.out.println("EIR data response :" + eirData.toString());
		System.out
				.println("##################### Ends: EIR Search ########################");
		return eirData;
	}

	private Map<String, String> getPCRFSessionInfo(String msisdn,
			SessionConstants sessionConstants) throws UserException {
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		Map<String, String> attributes = new HashMap<String, String>();
		String query = "";
		String filter = "";
	try {
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		query = String.format(SEARCH_QUERY_PCRF_SESSION, msisdn);
		filter = String.format(SEARCH_FILTER_PCRF_SESSION, msisdn);

		PROVIDER_URL_PCRF_SESSION = "ldap://"
				+ SessionConstantsFactory.getSessionConstant(
						"LDAP_SERVER_PCRF_SESSION", sessionConstants) + "/";
		
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_PCRF_SESSION,
					SECURITY_CREDENTIALS_PCRF_SESSION, PROVIDER_URL_PCRF_SESSION));
		

		if (ctx != null) {
			try {

				
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);
				
				
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());

					Attributes attrs = rs.getAttributes();
					NamingEnumeration<String> atyrs = attrs.getIDs();

					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							attributes.put(id, attrs.get(id).get().toString());

							// System.out.println("ATTR : " + id
							// +" : "+attrs.get(id));
						} catch (Exception e) {
							
						}

					}

					
				}

			} catch (NamingException e) {

			//	e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
				//	e.printStackTrace();
				}
			}
		}
		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
			
		}

		logger.info("#### Session Info=================########################>>"
				+ attributes);
		return attributes;
	}

	@Override
	public ImeiTacDataResponse doAddEIR(int search_type, String uid,
			String imsi, String imei,String lockImsiImei,String fastBlackList,String dualImsi, SessionConstants sessionConstants) throws UserException {

		logger.info("##################### Starts: Add EIR ########################");
		ImeiTacDataResponse respData = new ImeiTacDataResponse();

		try {

			String providerURL = "";
			LdapContext ctx = null;
			SearchControls sc = new SearchControls();
			String[] attrIDs = { "eirImeiHistory" };
			sc.setReturningAttributes(attrIDs);
			String query = "";
			String epochTime=getEpochTime();
			switch (search_type) {
			case Constants.SEARCH_TYPE_MSISDN:
				break;
			case Constants.SEARCH_TYPE_IMSI:
				break;
			case Constants.SEARCH_TYPE_CUSTID:
				break;
			case Constants.SEARCH_TYPE_UID:
				PROVIDER_URL_ADD_IMSI_EIR = "ldap://"
						+ SessionConstantsFactory.getSessionConstant(
								"LDAP_SERVER_EIR_ADD_IMSI", sessionConstants)
						+ "/DC=C-NTDB";
				providerURL = String.format(PROVIDER_URL_ADD_IMSI_EIR,
						"SUBSCRIBER");
				logger.info("ProviderURL :" + providerURL);
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

				query = String.format(SEARCH_QUERY_ADD_EIR, uid);
				break;
			default:
				break;
			}
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_EIR, SECURITY_CREDENTIALS_EIR,
					providerURL));
			if (ctx != null) {
				try {

					Attribute attribute = new BasicAttribute("eirImeiHistory",
							epochTime + "|" + imei);
					Attribute attribute2 = new BasicAttribute(
							"eirFastBlackList", fastBlackList);
					Attribute attribute3 = new BasicAttribute(
							"eirLockImsiImei", lockImsiImei);
					Attribute attribute4 = new BasicAttribute("eirDualImsi",
							dualImsi);
					ModificationItem[] item = new ModificationItem[4];
					item[0] = new ModificationItem(
							DirContext.REPLACE_ATTRIBUTE, attribute);
					item[1] = new ModificationItem(
							DirContext.REPLACE_ATTRIBUTE, attribute2);
					item[2] = new ModificationItem(
							DirContext.REPLACE_ATTRIBUTE, attribute3);
					item[3] = new ModificationItem(
							DirContext.REPLACE_ATTRIBUTE, attribute4);
					logger.info("query:::::::: " + query);

					ctx.modifyAttributes(query, item);
					Map<String, String> map = new HashMap<String, String>();
					//map.put("" + 0, tac + "|" + imei);
					map.put("eirImeiHistory", epochTime + "|" + imei);

					respData.setAttributes(map);
				} catch (Exception e) {
					respData.setApplicationError(new ApplicationError(e
							.getMessage()));
					//e.printStackTrace();
					
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						respData.setApplicationError(new ApplicationError(e
								.getMessage()));
					}
				}
			}
		} catch (Exception e) {
			throw new UserException(e.getMessage());
		}
		logger.info("##################### Ends: Add EIR ########################");
		logger.info("EIR data response :" + respData.toString());

		return respData;
	}

	@Override
	public ImeiTacDataResponse doImsiUnlock(int search_type,ImeiTacSearchRequest imeiTacSearchRequest) throws UserException {

		logger.info("##################### Starts: Unlock Imsi ########################");
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();

		try {

			String providerURL = "";
			LdapContext ctx = null;
			SearchControls sc = new SearchControls();
			String[] attrIDs = { "eirImeiHistory" };
			sc.setReturningAttributes(attrIDs);
			String query = "";

			switch (search_type) {
			case Constants.SEARCH_TYPE_MSISDN:
				break;
			case Constants.SEARCH_TYPE_IMSI:
				break;
			case Constants.SEARCH_TYPE_CUSTID:
				break;
			case Constants.SEARCH_TYPE_UID:
				PROVIDER_URL_UNLOCK_IMSI_IMEI = "ldap://"
						+ SessionConstantsFactory.getSessionConstant(
								"LDAP_SERVER_UNLOCK_IMSI_IMEI",
								imeiTacSearchRequest.getSessionConstants()) + "/DC=C-NTDB";
				providerURL = String.format(PROVIDER_URL_UNLOCK_IMSI_IMEI,
						"SUBSCRIBER");
				logger.info("ProviderURL :" + providerURL);
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

				query = String.format(SEARCH_QUERY_UNLOCK_IMSI_IMEI, imeiTacSearchRequest.getUid());
				break;
			default:
				break;
			}
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_EIR, SECURITY_CREDENTIALS_EIR,
					providerURL));
			if (ctx != null) {
				try {

					Attribute attribute = new BasicAttribute("eirLockImsiImei",
							imeiTacSearchRequest.getLockImsiImei());
					ModificationItem[] item = new ModificationItem[1];
					item[0] = new ModificationItem(
							DirContext.REPLACE_ATTRIBUTE, attribute);
					logger.info("query:::::::: " + query);

					ctx.modifyAttributes(query, item);
					Map<String, String> map = new HashMap<String, String>();
					map.put("" + 0, "Unlocked");
					imeiTacDataResponse.setAttributes(map);
				} catch (Exception e) {
					imeiTacDataResponse
							.setApplicationError(new ApplicationError(e
									.getMessage()));
				//	e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
					//	e.getMessage();
						imeiTacDataResponse
						.setApplicationError(new ApplicationError(e
								.getMessage()));
					}
				}
			}
		} catch (Exception e) {
			throw new UserException(e.getMessage());
		}
		logger.info("##################### Ends: Unlock Imsi ########################");
		logger.info("EIR data response :" + imeiTacDataResponse.toString());

		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse doGetTac(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {
		logger.info("##################### Starts: Search TAC ########################");
		ImeiTacDataResponse imeiTacDataResponse = null;

		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
	try {
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			break;
		case Constants.SEARCH_TYPE_IMSI:
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			break;
		case Constants.SEARCH_TYPE_UID:
			break;
		case Constants.SEARCH_TYPE_IMEI:
			PROVIDER_URL_TAC_SEARCH = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_TAC_SEARCH", sessionConstants);
			providerURL = String.format(PROVIDER_URL_TAC_SEARCH, "device");
			logger.info("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_SEARCH_TAC, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
			break;

		default:
			break;
		}
		imeiTacDataResponse = new ImeiTacDataResponse();
		
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SEARCH_TAC, SECURITY_CREDENTIALS_SEARCH_TAC,
					providerURL));
		
		if (ctx != null) {
			try {
				
				logger.info("TAC search query :" + query + "\nfilter :"
						+ filter);
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);
				logger.info("TAC search result :" + result.hasMoreElements());
				logger.info("----------------------------------------");
				while (result != null && result.hasMoreElements()) {

					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());

					Attributes attribute = rs.getAttributes();

					USDDataRepositoryHelper.fillTacData(imeiTacDataResponse,
							attribute);

				}
				logger.info("Search TAC response :"
						+ imeiTacDataResponse.toString());
			} catch (NamingException e) {
				imeiTacDataResponse.setApplicationError(new ApplicationError(e
						.getMessage()));
				//e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					imeiTacDataResponse.setApplicationError(new ApplicationError(e
							.getMessage()));
				}
			}
		}
	} catch (UserException e1) {
		throw new UserException(e1.getMessage());
	}

		logger.info("##################### Ends: Search TAC ########################");

		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse doAddTac(int search_type, String key,
			String deviceManufacturer, String deviceModel, String deviceBands,
			String deviceScreenResolution, String deviceTechnologies,
			SessionConstants sessionConstants) throws UserException {
		logger.info("##################### Starts: Add TAC ########################");
		logger.info("Key:: " + key + " tacAttributes:: " + search_type);
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
	try {	
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			break;
		case Constants.SEARCH_TYPE_IMSI:
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			break;
		case Constants.SEARCH_TYPE_UID:
			break;
		case Constants.SEARCH_TYPE_IMEI:
			PROVIDER_URL_ADD_TAC = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_ADD_TAC", sessionConstants);
			providerURL = String.format(PROVIDER_URL_ADD_TAC, "device");
			logger.info("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_ADD_TAC, key);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
			break;

		default:
			break;
		}
	
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SEARCH_TAC, SECURITY_CREDENTIALS_SEARCH_TAC,
					providerURL));
		
		if (ctx != null) {
			try {

				Attribute attribute1 = new BasicAttribute("deviceManufacturer",
						deviceManufacturer);
				Attribute attribute2 = new BasicAttribute("deviceModel",
						deviceModel);
				Attribute attribute3 = new BasicAttribute("deviceBands",
						deviceBands);
				Attribute attribute4 = new BasicAttribute(
						"deviceScreenResolution", deviceScreenResolution);
				Attribute attribute5 = new BasicAttribute("deviceTechnologies",
						deviceTechnologies);

				ModificationItem[] item = new ModificationItem[5];

				item[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						attribute1);
				item[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						attribute2);

				item[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						attribute3);

				item[3] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						attribute4);

				item[4] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						attribute5);

				logger.info("query:::::::: " + attribute1);

				ctx.modifyAttributes(query, item);
				Map<String, String> map = new HashMap<String, String>();
				map.put("IMEI", key);
				map.put("deviceManufacturer", deviceManufacturer);
				map.put("deviceModel", deviceModel);
				map.put("deviceBands", deviceBands);
				map.put("deviceScreenResolution", deviceScreenResolution);
				map.put("deviceTechnologies", deviceTechnologies);

				imeiTacDataResponse.setAttributes(map);
			} catch (NamingException e) {
				imeiTacDataResponse.setApplicationError(new ApplicationError(e
						.getMessage()));
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
			}
		logger.info("##################### Ends: Add TAC ########################");

		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse doEnableMMS(int search_type, String key,
			String enableOption, SessionConstants sessionConstants) throws UserException {
		logger.info("##################### Starts: Enable MMS ########################");
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();

		try {

			String providerURL = "";
			LdapContext ctx = null;
			SearchControls sc = new SearchControls();
			String[] attrIDs = { "eirImeiHistory" };
			sc.setReturningAttributes(attrIDs);
			String query = "";

			switch (search_type) {
			case Constants.SEARCH_TYPE_MSISDN:
				break;
			case Constants.SEARCH_TYPE_IMSI:
				break;
			case Constants.SEARCH_TYPE_CUSTID:
				break;
			case Constants.SEARCH_TYPE_UID:
				break;
			case Constants.SEARCH_TYPE_IMEI:
				PROVIDER_URL_ENABLE_MMS = "ldap://"
						+ SessionConstantsFactory.getSessionConstant(
								"LDAP_SERVER_ENABLE_MMS", sessionConstants);
				providerURL = String.format(PROVIDER_URL_ENABLE_MMS,
						"SUBSCRIBER");
				logger.info("ProviderURL :" + providerURL);
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

				query = String.format(SEARCH_QUERY_ENABLE_MMS, key);
				break;
			default:
				break;
			}
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_EIR, SECURITY_CREDENTIALS_EIR,
					providerURL));
			if (ctx != null) {
				try {

					Attribute attribute = new BasicAttribute(
							"deviceTechnologies",
							"TAC2="
									+ key
									+ ",Capability=MMS-access=IP,SUPL=2,remunl=no,mmsblank="
									+ enableOption);
					ModificationItem[] item = new ModificationItem[1];
					item[0] = new ModificationItem(
							DirContext.REPLACE_ATTRIBUTE, attribute);
					logger.info("query:::::::: " + query);

					ctx.modifyAttributes(query, item);
					Map<String, String> map = new HashMap<String, String>();

					if (enableOption != null && !"".equals(enableOption)
							&& "yes".equals(enableOption)) {
						map.put("" + 0, "Enabled");
					} else {
						map.put("" + 0, "Disabled");
					}

					imeiTacDataResponse.setAttributes(map);
				} catch (Exception e) {
					imeiTacDataResponse
							.setApplicationError(new ApplicationError(e
									.getMessage()));
					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						e.getMessage();
					}
				}
			}
		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
			}

		logger.info("##################### Ends: Enable MMS ########################");

		return imeiTacDataResponse;
	}

	/**
	 * @param args
	 */
	
	public String getEpochTime()
	{
		String epochTime="";
		String currentDateTime="";
		DateFormat dateFormat = new SimpleDateFormat("MMddyyyyHHmmss");
		Date date = new Date();
		
		try
		{
		currentDateTime=dateFormat.format(date);
		long epoch = System.currentTimeMillis();
		int epoch_t=(int) (epoch/1000);
		epochTime=""+epoch_t;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
			
		return epochTime;
		
	}
	public static void main(String[] args) {
		USDDataHandler handler = new USDDataHandler();
		handler.getEpochTime();
		// UID retrieval either by customer_id or msisdn or imsi search

		// handler.getUidFromDefaultInput(Constants.SEARCH_TYPE_CUSTID,"4254444604");
		// handler.getUidFromDefaultInput(Constants.SEARCH_TYPE_IMSI,"310260540571787");
		// handler.getUidFromDefaultInput(Constants.SEARCH_TYPE_MSISDN,"14254444604");
		// handler.getUidFromDefaultInput(Constants.SEARCH_TYPE_IMSI,"310310990005129");

		// NAP
		// handler.doNAPSearch(Constants.SEARCH_TYPE_CUSTID, "4254444604");
		// handler.doNAPSearch(Constants.SEARCH_TYPE_IMSI, "310260540571787");
		// handler.doNAPSearch(Constants.SEARCH_TYPE_MSISDN, "14254444604");
		// handler.doNAPSearch(Constants.SEARCH_TYPE_UID,"fecabaf3-7eee-4e9a-8bec-aa4a019c");//no

		// DUS
		// handler.doDUSSearch(Constants.SEARCH_TYPE_MSISDN, "14254444604");
		// handler.doDUSSearch(Constants.SEARCH_TYPE_IMSI, "310260540571787");

		// handler.doAddEIR(Constants.SEARCH_TYPE_UID,"8ceff4aa-90e8-48ce-a2e3-53f640c7","1377203501","01293700481501");
		// handler.doGetTac(Constants.SEARCH_TYPE_IMEI, "35588005");

		// handler.doAddTac(Constants.SEARCH_TYPE_IMEI,"35588005","Samsung","SGH-T729","900/1800/1900","500x400","deviceTechnologies: TAC2=35588005,Capability=MMS-access=IP,SUPL=2,remunl=no,mmsblank=yes");

		// handler.doEnableMMS(Constants.SEARCH_TYPE_IMEI, "35588005", "yes");
		// handler.doImsiUnlock(Constants.SEARCH_TYPE_UID,
		// "8ceff4aa-90e8-48ce-a2e3-53f640c7");
		// PCRF
		// handler.doPCRFSearch(Constants.SEARCH_TYPE_MSISDN, "14254444604");

		// PCRF SESSION
		SessionConstants sessionConstants=new SessionConstants();
		try
		{ 
		handler.getPCRFSessionInfo("14254444604",sessionConstants); // 14254444604
		}catch(Exception e){}
		// UMA
		// handler.doUMASearch(Constants.SEARCH_TYPE_UID,"d23513b0-74f1-42f4-b763-8d91ff27");

		//SessionConstants sessionConstants = new SessionConstants();
		try
		{
		//handler.doHSSSearch(Constants.SEARCH_TYPE_UID,"235e6951-f201-4f7f-a552-a8d99ed4",sessionConstants);
		}catch(Exception e){}
		// HLR
		// handler.doHLRSearch(Constants.SEARCH_TYPE_UID,"235e6951-f201-4f7f-a552-a8d99ed4");

		// EIR
		
		 /*try {
			handler.doEIRSearch(Constants.SEARCH_TYPE_UID,"2418136f-dfa4-4c42-a3c5-f33b85a3",sessionConstants);
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
		// (this can be tested after deployment )

	}

}
