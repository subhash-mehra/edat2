package com.tmobile.edat.usd.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmobile.edat.usd.component.USDDataComponent;
import com.tmobile.edat.usd.dto.request.ImeiTacSearchRequest;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EirDataResponse;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.HssDataResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.PcrfDataResponse;
import com.tmobile.edat.usd.dto.response.UmaDataResponse;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;

@Controller
@RequestMapping("usd")
public class USDDataController {

	@Autowired
	@Qualifier("USDDataComponent")
	USDDataComponent usdDataComponent = null;

	// 1. USD data landing page
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String usd() {
		return "usd/usd";
	}

	// 2. NAP data search request
	@RequestMapping(value = "/nap", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public NapDataResponse nap(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getNapData(usdSearchRequest);
	}

	// 3. DUS data search request
	@RequestMapping(value = "/dus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public DusDataResponse dus(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getDUSData(usdSearchRequest);
	}

	// 4. PCRF data search request
	@RequestMapping(value = "/pcrf", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PcrfDataResponse pcrf(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getPCRFData(usdSearchRequest);
	}

	// 5. UMA data search request
	@RequestMapping(value = "/uma", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UmaDataResponse uma(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getUMAData(usdSearchRequest);
	}

	// 6. HSS data search request
	@RequestMapping(value = "/hss", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HssDataResponse hss(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getHSSData(usdSearchRequest);
	}

	// 7. HLR data search request
	@RequestMapping(value = "/hlr", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HlrDataResponse hlr(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getHLRData(usdSearchRequest);
	}

	// 8. EIR data search request
	@RequestMapping(value = { "/eir", "/searchEir" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public EirDataResponse eir(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
		usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getEIR(usdSearchRequest);
	}

	@RequestMapping(value = { "/addEir" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ImeiTacDataResponse addEIR(
			@RequestBody ImeiTacSearchRequest imeiTacSearchRequest,HttpSession session) throws UserException {
		imeiTacSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.addEIR(imeiTacSearchRequest);
	}

	@RequestMapping(value = { "/unlockEir" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ImeiTacDataResponse imsiUnlock(
			@RequestBody ImeiTacSearchRequest imeiTacSearchRequest,HttpSession session) throws UserException {
		imeiTacSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.imsiUnlock(imeiTacSearchRequest);
	}

	@RequestMapping(value = { "/searchTac" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ImeiTacDataResponse getTac(
			@RequestBody ImeiTacSearchRequest imeiTacSearchRequest,HttpSession session) throws UserException {
		imeiTacSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.getTac(imeiTacSearchRequest);
	}

	@RequestMapping(value = { "/addtac" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ImeiTacDataResponse addTac(
			@RequestBody ImeiTacSearchRequest imeiTacSearchRequest,HttpSession session) throws UserException {
		imeiTacSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.addTac(imeiTacSearchRequest);
	}

	@RequestMapping(value = { "/enableMMS" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ImeiTacDataResponse enableMMS(
			@RequestBody ImeiTacSearchRequest imeiTacSearchRequest,HttpSession session) throws UserException {
		imeiTacSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.usdDataComponent.enableMMS(imeiTacSearchRequest);
	}
	
	
	// 3.getUID
		@RequestMapping(value = "/getUid", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		public DusDataResponse getUid(@RequestBody UsdSearchRequest usdSearchRequest,HttpSession session) throws UserException {
			usdSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
			return this.usdDataComponent.getUid(usdSearchRequest);
		}

}
