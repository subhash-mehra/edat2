package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;

public class HssDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
	protected String uid;

	// Map store all attributes and values;
	protected Map<String, String> attributes;
	
	protected Map<String, String> hssServiceProfile;
	
	protected Map<String, String> hssUserFilterCriteria;
	
	protected Map<String, String> hssIrs;
	
	protected Map<String, String> hssImpi;
	
	protected Map<String, String> hssImpu;
	
	protected Map<String, String> hssNotification;
	

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public Map<String, String> getHssServiceProfile() {
		return hssServiceProfile;
	}

	public void setHssServiceProfile(Map<String, String> hssServiceProfile) {
		this.hssServiceProfile = hssServiceProfile;
	}

	public Map<String, String> getHssUserFilterCriteria() {
		return hssUserFilterCriteria;
	}

	public void setHssUserFilterCriteria(Map<String, String> hssUserFilterCriteria) {
		this.hssUserFilterCriteria = hssUserFilterCriteria;
	}

	public Map<String, String> getHssIrs() {
		return hssIrs;
	}

	public void setHssIrs(Map<String, String> hssIrs) {
		this.hssIrs = hssIrs;
	}

	public Map<String, String> getHssImpi() {
		return hssImpi;
	}

	public void setHssImpi(Map<String, String> hssImpi) {
		this.hssImpi = hssImpi;
	}

	public Map<String, String> getHssImpu() {
		return hssImpu;
	}

	public void setHssImpu(Map<String, String> hssImpu) {
		this.hssImpu = hssImpu;
	}

	public Map<String, String> getHssNotification() {
		return hssNotification;
	}

	public void setHssNotification(Map<String, String> hssNotification) {
		this.hssNotification = hssNotification;
	}

	@Override
	public String toString() {
		return "HssDataResponse [uid=" + uid + ", attributes=" + attributes
				+ "]";
	}
	
	
	
}
