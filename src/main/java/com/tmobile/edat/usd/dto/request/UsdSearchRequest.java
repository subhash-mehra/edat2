package com.tmobile.edat.usd.dto.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.session.SessionConstants;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class UsdSearchRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String customerId;
    protected String msisdn;
    protected String imsi;
    protected String ban;
    protected String uid;
    protected SessionConstants sessionConstants;
    public UsdSearchRequest() {
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getBan() {
        return ban;
    }

    public void setBan(String ban) {
        this.ban = ban;
    }

	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}

	@Override
	public String toString() {
		return "UsdSearchRequest [customerId=" + customerId + ", msisdn=" + msisdn + ", imsi=" + imsi + ", ban=" + ban
				+ ", uid=" + uid + "]";
	}

    
}
