package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;

public class PcrfDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
	protected String uid;
	protected String customerId;
	// Map store all attributes and values;
	protected Map<String, String> attributes;
	protected Map<String, String> activeSession;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	
	public Map<String, String> getActiveSession() {
		return activeSession;
	}

	public void setActiveSession(Map<String, String> activeSession) {
		this.activeSession = activeSession;
	}

	@Override
	public String toString() {
		return "PcrfDataResponse [uid=" + uid + ", customerId=" + customerId
				+ ", attributes=" + attributes + ", activeSession="
				+ activeSession + "]";
	}

	

}
