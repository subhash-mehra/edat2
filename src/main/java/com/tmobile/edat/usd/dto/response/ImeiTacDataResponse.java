package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;

public class ImeiTacDataResponse extends BaseResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	protected String uid;
	protected Map<String, String> attributes;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "EirDataResponse [uid=" + uid + ", attributes=" + attributes
				+ "]";
	}

}
