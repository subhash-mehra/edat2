package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tmobile.edat.base.BaseResponse;

public class EirDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    protected String uid;
    private String eirFastBlackList;
    private String eirLockImsiImei;
    private String eirDualImsi;
    
    protected List<String> imeiHistory= new ArrayList<String>();
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public List<String> getImeiHistory() {
		return imeiHistory;
	}
	public void addImeiHistory(String imeiHistory) {
		this.imeiHistory.add(imeiHistory);
	}
	
	
	public String getEirFastBlackList() {
		return eirFastBlackList;
	}
	public void setEirFastBlackList(String eirFastBlackList) {
		this.eirFastBlackList = eirFastBlackList;
	}
	public String getEirLockImsiImei() {
		return eirLockImsiImei;
	}
	public void setEirLockImsiImei(String eirLockImsiImei) {
		this.eirLockImsiImei = eirLockImsiImei;
	}
	public String getEirDualImsi() {
		return eirDualImsi;
	}
	public void setEirDualImsi(String eirDualImsi) {
		this.eirDualImsi = eirDualImsi;
	}
	@Override
	public String toString() {
		return "EirDataResponse [uid=" + uid + ", imeiHistory=" + imeiHistory
				+ "]";
	}
    
    

}
