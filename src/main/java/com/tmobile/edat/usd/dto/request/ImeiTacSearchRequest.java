package com.tmobile.edat.usd.dto.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.dusupdate.dto.Counter;
import com.tmobile.session.SessionConstants;

public class ImeiTacSearchRequest extends BaseRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	protected String imsi;
	protected String uid;
	protected String tac;
	protected String imei;
	protected String lockImsiImei;
	protected String fastBlackList;
	protected String dualImsi;
	protected String deviceManufacturer;
	protected String deviceModel;
	protected String deviceBands;
	protected String deviceScreenResolution;
	protected String deviceTechnologies;
	protected String enableMMSOption;
	protected String mmsUpdate;
	//protected String lockImsiImei;
	protected SessionConstants sessionConstants;
	public ImeiTacSearchRequest() {

	}

	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * @param imsi
	 *            the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return the tac
	 */
	public String getTac() {
		return tac;
	}

	/**
	 * @param tac
	 *            the tac to set
	 */
	public void setTac(String tac) {
		this.tac = tac;
	}

	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * @param imei
	 *            the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}

	/**
	 * @return the deviceManufacturer
	 */
	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}

	/**
	 * @param deviceManufacturer
	 *            the deviceManufacturer to set
	 */
	public void setDeviceManufacturer(String deviceManufacturer) {
		this.deviceManufacturer = deviceManufacturer;
	}

	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}

	/**
	 * @param deviceModel
	 *            the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	/**
	 * @return the deviceBands
	 */
	public String getDeviceBands() {
		return deviceBands;
	}

	/**
	 * @param deviceBands
	 *            the deviceBands to set
	 */
	public void setDeviceBands(String deviceBands) {
		this.deviceBands = deviceBands;
	}

	/**
	 * @return the deviceScreenResolution
	 */
	public String getDeviceScreenResolution() {
		return deviceScreenResolution;
	}

	/**
	 * @param deviceScreenResolution
	 *            the deviceScreenResolution to set
	 */
	public void setDeviceScreenResolution(String deviceScreenResolution) {
		this.deviceScreenResolution = deviceScreenResolution;
	}

	/**
	 * @return the deviceTechnologies
	 */
	public String getDeviceTechnologies() {
		return deviceTechnologies;
	}

	/**
	 * @param deviceTechnologies
	 *            the deviceTechnologies to set
	 */
	public void setDeviceTechnologies(String deviceTechnologies) {
		this.deviceTechnologies = deviceTechnologies;
	}

	/**
	 * @return the enableMMSOption
	 */
	public String getEnableMMSOption() {
		return enableMMSOption;
	}

	/**
	 * @param enableMMSOption
	 *            the enableMMSOption to set
	 */
	public void setEnableMMSOption(String enableMMSOption) {
		this.enableMMSOption = enableMMSOption;
	}

	public String getLockImsiImei() {
		return lockImsiImei;
	}

	public void setLockImsiImei(String lockImsiImei) {
		this.lockImsiImei = lockImsiImei;
	}

	public String getFastBlackList() {
		return fastBlackList;
	}

	public void setFastBlackList(String fastBlackList) {
		this.fastBlackList = fastBlackList;
	}

	public String getDualImsi() {
		return dualImsi;
	}

	public void setDualImsi(String dualImsi) {
		this.dualImsi = dualImsi;
	}

	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}
	
	

	public String getMmsUpdate() {
		return mmsUpdate;
	}

	public void setMmsUpdate(String mmsUpdate) {
		this.mmsUpdate = mmsUpdate;
	}

	@Override
	public String toString() {
		return "UsdSearchRequest [imsi=" + imsi + ", uid=" + uid + ", tac="
				+ tac + ", imei=" + imei + "deviceManufacturer="
				+ deviceManufacturer + "deviceModel=" + deviceModel
				+ "deviceBands=" + deviceBands + "deviceScreenResolution="
				+ deviceScreenResolution + "deviceTechnologies="
				+ deviceTechnologies + "enableMMSOption=" + enableMMSOption
				+ "]";
	}
}
