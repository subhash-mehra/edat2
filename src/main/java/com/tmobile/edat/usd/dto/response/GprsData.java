package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;

public class GprsData implements Serializable {
    private static final long serialVersionUID = 1L;
    
	private String pdpContextId;
	private String pdpType;
	private String accPointName;
	private String refqOfServName;
	public String getPdpContextId() {
		return pdpContextId;
	}
	public void setPdpContextId(String pdpContextId) {
		this.pdpContextId = pdpContextId;
	}
	public String getPdpType() {
		return pdpType;
	}
	public void setPdpType(String pdpType) {
		this.pdpType = pdpType;
	}
	public String getAccPointName() {
		return accPointName;
	}
	public void setAccPointName(String accPointName) {
		this.accPointName = accPointName;
	}
	public String getRefqOfServName() {
		return refqOfServName;
	}
	public void setRefqOfServName(String refqOfServName) {
		this.refqOfServName = refqOfServName;
	}
	@Override
	public String toString() {
		return "GprsData [pdpContextId=" + pdpContextId + ", pdpType="
				+ pdpType + ", accPointName=" + accPointName
				+ ", refqOfServName=" + refqOfServName + "]";
	}
	
	
}
