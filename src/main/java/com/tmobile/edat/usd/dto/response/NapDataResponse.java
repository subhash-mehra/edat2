package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;

/**
 * Created with IntelliJ IDEA. User: sameer.c Date: 10/10/14 Time: 1:03 PM To
 * change this template use File | Settings | File Templates.
 */
public class NapDataResponse extends BaseResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private Profile profile;

	private List<Partners> partnersList = new ArrayList<Partners>();

	private List<Socs> socsList = new ArrayList<Socs>();

	private Map<String, String> partnersrAttributesSuperset = new HashMap<String, String>();
	
	private Map<String, String> socAttributesSuperset = new HashMap<String, String>();

	

	
	// Default Constructor
	public NapDataResponse() {
	}

	//
	public NapDataResponse(Profile profile, List<Partners> partnersList,
			List<Socs> socsList) {
		super();
		this.profile = profile;
		this.partnersList = partnersList;
		this.socsList = socsList;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Partners> getPartnersList() {
		return partnersList;
	}

	public void setPartnersList(List<Partners> partnersList) {
		this.partnersList = partnersList;
	}

	public List<Socs> getSocsList() {
		return socsList;
	}

	public void setSocsList(List<Socs> socsList) {
		this.socsList = socsList;
	}

	public Map<String, String> getPartnersrAttributesSuperset() {
		return partnersrAttributesSuperset;
	}

	public void addCounterSuperset(String key) {
		partnersrAttributesSuperset.put(key, "");
	}
	
	public Map<String, String> getSocAttributesSuperset() {
		return socAttributesSuperset;
	}
	
	public void addsocSuperset(String key) {
		socAttributesSuperset.put(key, "");
	}

	@Override
	public String toString() {
		return "NapDataResponse [profile=" + profile + ", partnersList="
				+ partnersList + ", socsList=" + socsList + "]";
	}

}
