package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Partners implements Serializable {
    private static final long serialVersionUID = 178782738239L;
    
    private String partnerId;
    
    /*private String serviceLevel;
    private String createDTTM;
    private String param;
    private String modDTTM;
    private String partnerAssociated;
    private String createUser;
    private String priority;    
    private String service;
    private String modUser;*/
    
    // Map store all attributes and values;
 	private Map<String, String> attributes;
    
	public Partners() {
		super();		
	}
	
	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	@Override
	public String toString() {
		return "Partners [partnerId=" + partnerId + ", attributes="
				+ attributes + "]";
	}
    
	
}
