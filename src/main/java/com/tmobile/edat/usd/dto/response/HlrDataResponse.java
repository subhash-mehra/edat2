package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;

public class HlrDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    protected String uid;

	// Map store all attributes and values;
	protected Map<String, String> attributes=  new HashMap<String, String>();
	//protected List<GprsData> gprsData= new ArrayList<GprsData>();
	//protected List<EpsData> epsData= new ArrayList<EpsData>();
	
	
    protected Map<String, String> subinnss;
	
	protected Map<String, String> epsdata;
	
	protected Map<String, String> non3gppdata;
	
	protected Map<String, String> gprsdata;
	
	protected Map<String, String> pdncontext;
	

	
	
	public String getUid() {
		return uid;
	}



	public void setUid(String uid) {
		this.uid = uid;
	}



	public Map<String, String> getAttributes() {
		return attributes;
	}



	public Map<String, String> getSubinnss() {
		return subinnss;
	}



	public void setSubinnss(Map<String, String> subinnss) {
		this.subinnss = subinnss;
	}



	public Map<String, String> getEpsdata() {
		return epsdata;
	}



	public void setEpsdata(Map<String, String> epsdata) {
		this.epsdata = epsdata;
	}



	public Map<String, String> getNon3gppdata() {
		return non3gppdata;
	}



	public void setNon3gppdata(Map<String, String> non3gppdata) {
		this.non3gppdata = non3gppdata;
	}



	public Map<String, String> getGprsdata() {
		return gprsdata;
	}



	public void setGprsdata(Map<String, String> gprsdata) {
		this.gprsdata = gprsdata;
	}



	public Map<String, String> getPdncontext() {
		return pdncontext;
	}



	public void setPdncontext(Map<String, String> pdncontext) {
		this.pdncontext = pdncontext;
	}



	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}



	/*public List<GprsData> getGprsData() {
		return gprsData;
	}



	public void addGprsData(GprsData gprsData) {
		this.gprsData.add(gprsData);
	}



	public List<EpsData> getEpsData() {
		return epsData;
	}



	public void addEpsData(EpsData epsData) {
		this.epsData.add(epsData);
	}



	@Override
	public String toString() {
		return "HlrDataResponse [uid=" + uid + ", attributes=" + attributes
				+ ", gprsData=" + gprsData + ", epsData=" + epsData + "]";
	}*/
	
	
    
}
