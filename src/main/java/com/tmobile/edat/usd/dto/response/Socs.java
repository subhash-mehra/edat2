package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Socs implements Serializable {
    private static final long serialVersionUID = 178782738239L;
    
    private String socCode;
    
    // Map store all attributes and values;
    private Map<String, String> attributes;

	public Socs() {
		super();		
	}

	public String getSocCode() {
		return socCode;
	}

	public void setSocCode(String socCode) {
		this.socCode = socCode;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "Socs [socCode=" + socCode + ", attributes=" + attributes + "]";
	}    
 
    
}
