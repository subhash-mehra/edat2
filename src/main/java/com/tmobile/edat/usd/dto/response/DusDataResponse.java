package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.dusupdate.dto.Counter;

public class DusDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    
	private String uid;
	 // Map store all attributes and values;
    protected Map<String, String> attributes;
    
    protected List<Counter> counters= new ArrayList<Counter>();
    protected Map<String, String> counterSuperset = new HashMap<String, String>();
    
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Map<String, String> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	public List<Counter> getCounters() {
		return counters;
	}
	public void addCounters(Counter counter) {
		this.counters.add(counter);
	}
	
	
	public Map<String, String> getCounterSuperset() {
		return counterSuperset;
	}
	public void addCounterSuperset(String key) {
		 counterSuperset.put( key, "");
	}
	@Override
	public String toString() {
		return "DusDataResponse [uid=" + uid + ", attributes=" + attributes
				+ ", counters=" + counters + "]";
	}
	

}
