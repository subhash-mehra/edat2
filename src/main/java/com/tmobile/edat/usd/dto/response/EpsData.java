package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;

public class EpsData implements Serializable {
    private static final long serialVersionUID = 1L;

	protected String pdnAccPointName;
	protected String  pdnContextId;
	protected String pdnType;
	protected String refLteQOfServName;
	protected String vplmnAddressAllowed;
	protected String mmeIdentity;
	public String getPdnAccPointName() {
		return pdnAccPointName;
	}
	public void setPdnAccPointName(String pdnAccPointName) {
		this.pdnAccPointName = pdnAccPointName;
	}
	public String getPdnContextId() {
		return pdnContextId;
	}
	public void setPdnContextId(String pdnContextId) {
		this.pdnContextId = pdnContextId;
	}
	public String getPdnType() {
		return pdnType;
	}
	public void setPdnType(String pdnType) {
		this.pdnType = pdnType;
	}
	public String getRefLteQOfServName() {
		return refLteQOfServName;
	}
	public void setRefLteQOfServName(String refLteQOfServName) {
		this.refLteQOfServName = refLteQOfServName;
	}
	public String getVplmnAddressAllowed() {
		return vplmnAddressAllowed;
	}
	public void setVplmnAddressAllowed(String vplmnAddressAllowed) {
		this.vplmnAddressAllowed = vplmnAddressAllowed;
	}
	public String getMmeIdentity() {
		return mmeIdentity;
	}
	public void setMmeIdentity(String mmeIdentity) {
		this.mmeIdentity = mmeIdentity;
	}
	@Override
	public String toString() {
		return "EpsData [pdnAccPointName=" + pdnAccPointName
				+ ", pdnContextId=" + pdnContextId + ", pdnType=" + pdnType
				+ ", refLteQOfServName=" + refLteQOfServName
				+ ", vplmnAddressAllowed=" + vplmnAddressAllowed
				+ ", mmeIdentity=" + mmeIdentity + "]";
	}
	
	
}
