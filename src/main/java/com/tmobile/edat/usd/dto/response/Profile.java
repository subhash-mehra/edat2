package com.tmobile.edat.usd.dto.response;

import java.io.Serializable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Profile implements Serializable {
    private static final long serialVersionUID = 178782738239L;

    protected String msisdn;
    protected String imsi;
    // UID will not come in NAP search, this will come when you do the DUS search,we need to do the dus search to and update the UID here.
    protected String uid;
    protected String customerId;
    
    // Map store all attributes and values;
    protected Map<String, String> attributes;

    //Default Constructor
    public Profile() {
    }
    
	public Profile(String msisdn, String imsi, String uid, String customerId,
			Map<String, String> attributes) {
		super();
		this.msisdn = msisdn;
		this.imsi = imsi;
		this.uid = uid;
		this.customerId = customerId;
		this.attributes = attributes;
	}



	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "Profile [msisdn=" + msisdn + ", imsi=" + imsi + ", uid=" + uid
				+ ", customerId=" + customerId + ", attributes=" + attributes
				+ "]";
	}

    

    

   
}
