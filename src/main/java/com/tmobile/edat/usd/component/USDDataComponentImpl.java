package com.tmobile.edat.usd.component;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.usd.dto.request.ImeiTacSearchRequest;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EirDataResponse;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.HssDataResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.PcrfDataResponse;
import com.tmobile.edat.usd.dto.response.UmaDataResponse;
import com.tmobile.edat.usd.repository.USDDataRepository;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.UserException;

@Component("USDDataComponent")
public class USDDataComponentImpl implements USDDataComponent {

	private static final Logger logger = Logger
			.getLogger(USDDataComponentImpl.class);

	@Autowired
	@Qualifier("USDDataRepository")
	USDDataRepository usdDataRepository = null;

	public USDDataComponentImpl() {
	}

	@Override
	public boolean validateRequest(BaseRequest baseRequest,
			BaseResponse baseResponse, int key) {

		// Applicable to all methods
		if (baseRequest == null) {
			baseResponse = new NapDataResponse();
			baseResponse.setApplicationError(new ApplicationError(
					ApplicationConstants.NULL_REQUEST));
			baseResponse.setStatusCode(ApplicationConstants.FAILURE_CODE);
			return false;
		}

		return true;
	}

	@Override
	public NapDataResponse getNapData(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getNapData() with := " + usdSearchRequest);
		NapDataResponse napDataResponse = null;

		if (this.validateRequest(usdSearchRequest, napDataResponse, 0)) {
			// By CustomerId
			if (usdSearchRequest.getCustomerId() != null
					&& usdSearchRequest.getMsisdn() == null
					&& usdSearchRequest.getImsi() == null) {
				napDataResponse = this.usdDataRepository.doNAPSearch(
						Constants.SEARCH_TYPE_CUSTID, usdSearchRequest
								.getCustomerId().trim(), usdSearchRequest.getSessionConstants());
			}
			// By Msisdn
			else if (usdSearchRequest.getMsisdn() != null
					&& usdSearchRequest.getCustomerId() == null
					&& usdSearchRequest.getImsi() == null) {

				// Append 1 before MSISDN, if does not exist
				/*
				 * if(usdSearchRequest.getMsisdn().charAt(0)!='1'){
				 * usdSearchRequest.setMsisdn("1"+usdSearchRequest.getMsisdn());
				 * }
				 */

				napDataResponse = this.usdDataRepository.doNAPSearch(
						Constants.SEARCH_TYPE_MSISDN, usdSearchRequest
								.getMsisdn().trim(), usdSearchRequest.getSessionConstants());
			}
			// By imsi
			else if (usdSearchRequest.getImsi() != null
					&& usdSearchRequest.getMsisdn() == null
					&& usdSearchRequest.getCustomerId() == null) {
				napDataResponse = this.usdDataRepository.doNAPSearch(
						Constants.SEARCH_TYPE_IMSI, usdSearchRequest.getImsi()
								.trim(), usdSearchRequest.getSessionConstants());
			}

			// To Get UID from DUS
			if (napDataResponse != null && napDataResponse.getProfile() != null
					&& napDataResponse.getProfile().getMsisdn() != null) {
				DusDataResponse dusDataResponse = this.usdDataRepository
						.doDUSSearch(Constants.SEARCH_TYPE_MSISDN,
								napDataResponse.getProfile().getMsisdn().trim(), usdSearchRequest.getSessionConstants());
				// Check Dus response and set the UID in profile
				if (dusDataResponse != null)
					napDataResponse.getProfile().setUid(
							dusDataResponse.getUid());
			}
		}

		return napDataResponse;
	}

	@Override
	public DusDataResponse getDUSData(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getDUSData() with := " + usdSearchRequest);

		DusDataResponse dusDataResponse = null;

		if (this.validateRequest(usdSearchRequest, dusDataResponse, 0)) {
			if (usdSearchRequest.getCustomerId() != null) {
				dusDataResponse = this.usdDataRepository.doDUSSearch(
						Constants.SEARCH_TYPE_CUSTID, usdSearchRequest
								.getCustomerId().trim(), usdSearchRequest.getSessionConstants());
			}else if (usdSearchRequest.getMsisdn() != null) {
				dusDataResponse = this.usdDataRepository.doDUSSearch(
						Constants.SEARCH_TYPE_MSISDN, usdSearchRequest
								.getMsisdn().trim(), usdSearchRequest.getSessionConstants());
			} else if (usdSearchRequest.getImsi() != null) {
				dusDataResponse = this.usdDataRepository.doDUSSearch(
						Constants.SEARCH_TYPE_IMSI, usdSearchRequest.getImsi()
								.trim(), usdSearchRequest.getSessionConstants());
			}
		}
		return dusDataResponse;
	}
	
	@Override
	public DusDataResponse getUid(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getDUSData() with := " + usdSearchRequest);

		DusDataResponse dusDataResponse = null;

		if (this.validateRequest(usdSearchRequest, dusDataResponse, 0)) {
			if (usdSearchRequest.getCustomerId() != null) {
				dusDataResponse = this.usdDataRepository.getUid(
						Constants.SEARCH_TYPE_CUSTID, usdSearchRequest
								.getCustomerId().trim(), usdSearchRequest.getSessionConstants());
			}else if (usdSearchRequest.getMsisdn() != null) {
				dusDataResponse = this.usdDataRepository.getUid(
						Constants.SEARCH_TYPE_MSISDN, usdSearchRequest
								.getMsisdn().trim(), usdSearchRequest.getSessionConstants());
			} else if (usdSearchRequest.getImsi() != null) {
				dusDataResponse = this.usdDataRepository.getUid(
						Constants.SEARCH_TYPE_IMSI, usdSearchRequest.getImsi()
								.trim(), usdSearchRequest.getSessionConstants());
			}
		}
		return dusDataResponse;
	}

	
	
	@Override
	public PcrfDataResponse getPCRFData(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getPCRFData() with := " + usdSearchRequest);

		PcrfDataResponse pcrfDataResponse = null;

		if (this.validateRequest(usdSearchRequest, pcrfDataResponse, 0)) {
			
			if (usdSearchRequest.getCustomerId() != null) {
				pcrfDataResponse = this.usdDataRepository.doPCRFSearch(
						Constants.SEARCH_TYPE_CUSTID, usdSearchRequest
								.getCustomerId().trim(), usdSearchRequest.getSessionConstants());
			}else if (usdSearchRequest.getMsisdn() != null) {
				pcrfDataResponse = this.usdDataRepository.doPCRFSearch(
						Constants.SEARCH_TYPE_MSISDN, usdSearchRequest
								.getMsisdn().trim(), usdSearchRequest.getSessionConstants());
			} else if (usdSearchRequest.getImsi() != null) {
				pcrfDataResponse = this.usdDataRepository.doPCRFSearch(
						Constants.SEARCH_TYPE_IMSI, usdSearchRequest.getImsi()
								.trim(), usdSearchRequest.getSessionConstants());
			}
		}
		return pcrfDataResponse;
	}

	@Override
	public UmaDataResponse getUMAData(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getUMAData() with := " + usdSearchRequest);

		UmaDataResponse umaDataResponse = null;

		if (this.validateRequest(usdSearchRequest, umaDataResponse, 0)) {
			if (usdSearchRequest.getUid() != null) {
				umaDataResponse = this.usdDataRepository.doUMASearch(
						Constants.SEARCH_TYPE_UID, usdSearchRequest.getUid()
								.trim(),usdSearchRequest.getSessionConstants());
			}
		}
		return umaDataResponse;
	}

	@Override
	public HssDataResponse getHSSData(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getHSSData() with := " + usdSearchRequest);

		HssDataResponse hssDataResponse = null;

		if (this.validateRequest(usdSearchRequest, hssDataResponse, 0)) {
			if (usdSearchRequest.getUid() != null) {
				hssDataResponse = this.usdDataRepository.doHSSSearch(
						Constants.SEARCH_TYPE_UID, usdSearchRequest.getUid()
								.trim(),usdSearchRequest.getSessionConstants());
			}
		}
		return hssDataResponse;
	}

	@Override
	public HlrDataResponse getHLRData(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getHLRData() with := " + usdSearchRequest);

		HlrDataResponse hlrDataResponse = null;

		if (this.validateRequest(usdSearchRequest, hlrDataResponse, 0)) {
			if (usdSearchRequest.getUid() != null) {
				hlrDataResponse = this.usdDataRepository.doHLRSearch(
						Constants.SEARCH_TYPE_UID, usdSearchRequest.getUid()
								.trim(), usdSearchRequest.getSessionConstants());
			}
		}
		return hlrDataResponse;
	}

	@Override
	public EirDataResponse getEIR(UsdSearchRequest usdSearchRequest) throws UserException {
		logger.info("Checking in getEIR() with := " + usdSearchRequest);

		EirDataResponse eirDataResponse = null;

		if (this.validateRequest(usdSearchRequest, eirDataResponse, 0)) {
			if (usdSearchRequest.getUid() != null) {
				eirDataResponse = this.usdDataRepository.doEIRSearch(
						Constants.SEARCH_TYPE_UID, usdSearchRequest.getUid()
								.trim(), usdSearchRequest.getSessionConstants());
			}
		}
		return eirDataResponse;
	}

	@Override
	public ImeiTacDataResponse addEIR(ImeiTacSearchRequest imeiTacSearchRequest) throws UserException {
		logger.info("Checking in getEIR() with := " + imeiTacSearchRequest);
		System.out.println("Checking in **********************() with := "
				+ imeiTacSearchRequest);
		ImeiTacDataResponse imeiTacDataResponse = null;

		if (this.validateRequest(imeiTacSearchRequest, imeiTacDataResponse, 0)) {
			if (imeiTacSearchRequest.getUid() != null) {
				imeiTacDataResponse = this.usdDataRepository.doAddEIR(
						Constants.SEARCH_TYPE_UID, imeiTacSearchRequest
								.getUid().trim(), imeiTacSearchRequest.getImsi()
								.trim(), imeiTacSearchRequest.getImei().trim(),imeiTacSearchRequest.getLockImsiImei().trim(),imeiTacSearchRequest.getFastBlackList().trim(),imeiTacSearchRequest.getDualImsi().trim(),imeiTacSearchRequest.getSessionConstants());
			}
		}
		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse imsiUnlock(
			ImeiTacSearchRequest imeiTacSearchRequest) throws UserException {
		ImeiTacDataResponse imeiTacDataResponse = null;
		if (this.validateRequest(imeiTacSearchRequest, imeiTacDataResponse, 0)) {
			
			if (imeiTacSearchRequest.getImsi() != null && !"".equals(imeiTacSearchRequest.getImsi())) {
				imeiTacDataResponse = this.usdDataRepository.doImsiUnlock(
						Constants.SEARCH_TYPE_UID, imeiTacSearchRequest);
			}
		}
		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse getTac(ImeiTacSearchRequest imeiTacSearchRequest) throws UserException {
		ImeiTacDataResponse imeiTacDataResponse = null;
		if (this.validateRequest(imeiTacSearchRequest, imeiTacDataResponse, 0)) {
			if (imeiTacSearchRequest.getTac() != null) {
				imeiTacDataResponse = this.usdDataRepository.doGetTac(
						Constants.SEARCH_TYPE_IMEI, imeiTacSearchRequest
								.getTac().trim(),imeiTacSearchRequest.getSessionConstants());
			}
		}
		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse addTac(ImeiTacSearchRequest imeiTacSearchRequest) throws UserException {
		ImeiTacDataResponse imeiTacDataResponse = null;
		if (this.validateRequest(imeiTacSearchRequest, imeiTacDataResponse, 0)) {
			if (imeiTacSearchRequest.getTac() != null) {
				imeiTacDataResponse = this.usdDataRepository.doAddTac(
						Constants.SEARCH_TYPE_IMEI, imeiTacSearchRequest
								.getTac().trim(), imeiTacSearchRequest
								.getDeviceManufacturer(), imeiTacSearchRequest
								.getDeviceModel(), imeiTacSearchRequest
								.getDeviceBands(), imeiTacSearchRequest
								.getDeviceScreenResolution(),
						imeiTacSearchRequest.getDeviceTechnologies(),imeiTacSearchRequest.getSessionConstants());
			}
		}
		return imeiTacDataResponse;
	}

	@Override
	public ImeiTacDataResponse enableMMS(
			ImeiTacSearchRequest imeiTacSearchRequest) throws UserException {
		ImeiTacDataResponse imeiTacDataResponse = null;
		if (this.validateRequest(imeiTacSearchRequest, imeiTacDataResponse, 0)) {
			if (imeiTacSearchRequest.getImei() != null) {
				if(imeiTacSearchRequest.getMmsUpdate()!=null && imeiTacSearchRequest.getMmsUpdate().equalsIgnoreCase("true"))
				{
				imeiTacDataResponse = this.usdDataRepository.doEnableMMS(
						Constants.SEARCH_TYPE_IMEI, imeiTacSearchRequest
								.getImei().trim(), imeiTacSearchRequest
								.getEnableMMSOption(),imeiTacSearchRequest.getSessionConstants());
				}else
				{
					imeiTacDataResponse = this.usdDataRepository.doGetTac(
							Constants.SEARCH_TYPE_IMEI, imeiTacSearchRequest
									.getImei().trim(),imeiTacSearchRequest.getSessionConstants());
				}
			}
		}
		return imeiTacDataResponse;
	}

}
