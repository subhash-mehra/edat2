package com.tmobile.edat.usd.component;

import com.tmobile.edat.base.BaseComponent;
import com.tmobile.edat.usd.dto.request.ImeiTacSearchRequest;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EirDataResponse;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.HssDataResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.PcrfDataResponse;
import com.tmobile.edat.usd.dto.response.UmaDataResponse;
import com.tmobile.edat.util.UserException;

public interface USDDataComponent extends BaseComponent {

	public NapDataResponse getNapData(UsdSearchRequest usdSearchRequest)  throws UserException;

	public DusDataResponse getDUSData(UsdSearchRequest usdSearchRequest)  throws UserException;

	public PcrfDataResponse getPCRFData(UsdSearchRequest usdSearchRequest)  throws UserException;

	public UmaDataResponse getUMAData(UsdSearchRequest usdSearchRequest)  throws UserException;

	public HssDataResponse getHSSData(UsdSearchRequest usdSearchRequest)  throws UserException;

	public HlrDataResponse getHLRData(UsdSearchRequest usdSearchRequest)  throws UserException;

	public EirDataResponse getEIR(UsdSearchRequest usdSearchRequest)  throws UserException;

	public ImeiTacDataResponse addEIR(ImeiTacSearchRequest usdSearchRequest) throws UserException ;

	public ImeiTacDataResponse imsiUnlock(ImeiTacSearchRequest usdSearchRequest) throws UserException ;

	public ImeiTacDataResponse getTac(ImeiTacSearchRequest usdSearchRequest) throws UserException ;

	public ImeiTacDataResponse addTac(ImeiTacSearchRequest usdSearchRequest) throws UserException ;

	public ImeiTacDataResponse enableMMS(ImeiTacSearchRequest usdSearchRequest) throws UserException ;
	
	public DusDataResponse getUid(UsdSearchRequest usdSearchRequest)  throws UserException;
	
	

}
