package com.tmobile.edat.dusupdate.component;

import java.util.List;

import com.tmobile.edat.base.BaseComponent;
import com.tmobile.edat.dusupdate.dto.BanSearchResponse;
import com.tmobile.edat.dusupdate.dto.DusSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;

public interface DUSUpdateComponent extends BaseComponent {

	public DusDataResponse getDUSData(DusSearchRequest dusSearchRequest);
	
	public List<BanSearchResponse> getBanSearchResponse(DusSearchRequest dusSearchRequest);
	
	public  Boolean editMsisdn(DusSearchRequest dusSearchRequest);
	
}
