package com.tmobile.edat.dusupdate.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.dusupdate.dto.BanSearchResponse;
import com.tmobile.edat.dusupdate.dto.DusSearchRequest;
import com.tmobile.edat.dusupdate.repository.DUSUpdateRepository;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.repository.USDDataRepository;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.UserException;

@Service("DUSUpdateComponent")
public class DUSUpdateComponentImpl implements DUSUpdateComponent {

	@Autowired
	@Qualifier("DUSUpdateRepository")
	private DUSUpdateRepository dusUpdateRepository;

	@Autowired
	@Qualifier("USDDataRepository")
	USDDataRepository usdDataRepository;

	@Override
	public boolean validateRequest(BaseRequest baseRequest, BaseResponse baseResponse, int key) {

		// Applicable to all methods
		if (baseRequest == null) {
			baseResponse = new DusDataResponse();
			baseResponse.setApplicationError(new ApplicationError(ApplicationConstants.NULL_REQUEST));
			baseResponse.setStatusCode(ApplicationConstants.FAILURE_CODE);
			return false;
		}

		return true;
	}

	@Override
	public DusDataResponse getDUSData(DusSearchRequest dusSearchRequest) {

		DusDataResponse dusDataResponse = null;

		if (this.validateRequest(dusSearchRequest, dusDataResponse, 0)) {
			if (dusSearchRequest.getMsisdn() != null) {
				
				// Append 1 before MSISDN, if does not exist
	        	/*if(dusSearchRequest.getMsisdn().charAt(0)!='1'){
	        		dusSearchRequest.setMsisdn("1"+dusSearchRequest.getMsisdn());
	        	}*/
				
				try {
					dusDataResponse = this.usdDataRepository.doDUSSearch(Constants.SEARCH_TYPE_MSISDN, dusSearchRequest
							.getMsisdn().trim(), dusSearchRequest.getSessionConstants());
				} catch (UserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		
		
		// on UI we only needs to show Counter Id and Counter Value 
		// that,s why we can set null to attributes property of DusDataResponse
		
		dusDataResponse.setAttributes(null);
		return dusDataResponse;
	}

	public List<BanSearchResponse> getBanSearchResponse(DusSearchRequest dusSearchRequest) {
		List<BanSearchResponse> banSearchResponse = null;
		if (dusSearchRequest.getBan() != null && !dusSearchRequest.getBan().equals("")) {
			banSearchResponse= new ArrayList<BanSearchResponse>();
			banSearchResponse = this.dusUpdateRepository.doBANSearch(dusSearchRequest.getBan(), dusSearchRequest.getSessionConstants());
		}
		return banSearchResponse;

	}

	@Override
	public Boolean editMsisdn(DusSearchRequest dusSearchRequest) {
		
		Boolean dusDataResponse = null;
		if(dusSearchRequest.getCounterId()!=null && dusSearchRequest.getCounterValue()!=null){
			dusDataResponse=(Boolean) this.dusUpdateRepository.modifyDUSCounter(dusSearchRequest.getUid(), dusSearchRequest.getCounterId(), dusSearchRequest.getCounterValue(), dusSearchRequest.getSessionConstants());
		}
		return dusDataResponse;
	}

}
