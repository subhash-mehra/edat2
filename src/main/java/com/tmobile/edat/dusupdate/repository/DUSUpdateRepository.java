package com.tmobile.edat.dusupdate.repository;

import java.util.List;

import com.tmobile.edat.dusupdate.dto.BanSearchResponse;
import com.tmobile.session.SessionConstants;



public interface DUSUpdateRepository {

	public Object modifyDUSCounter(String uid, String counterid,
			String counterValue, SessionConstants sessionConstants);

	public List<BanSearchResponse>  doBANSearch(String ban, SessionConstants sessionConstants) ;
	
	//public List<String>  searchBAN(String ban) ;

	//public Object  getCounterInfo(String misdn);
	
}
