package com.tmobile.edat.dusupdate.repository;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapName;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.tmobile.edat.dusupdate.dto.BanSearchResponse;
import com.tmobile.edat.dusupdate.dto.Counter;
import com.tmobile.edat.usd.repository.USDDataHandler;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

@Repository("DUSUpdateRepository")
public class DUSUpdateRepositoryImpl implements DUSUpdateRepository {
	private static final Logger logger = Logger
			.getLogger(DUSUpdateRepositoryImpl.class);

	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";

	private static String SECURITY_PRINCIPAL = "cn=amfUser,o=DEFAULT";
	private static String SECURITY_CREDENTIALS = "siemens";
	private String PROVIDER_URL;

	private static String SECURITY_PRINCIPAL_BAN = "cn=amfUser";
	private static String SECURITY_CREDENTIALS_BAN = "siemens";
	private String PROVIDER_URL_BAN ;

	//
	// ldapmodify -x -h 10.25.72.247 -p 16800 -D "cn=amfUser" -w siemens -f
	// try1.ldif

	// dn:
	// amfCounterId=1001,ds=amf,subdata=services,uid=d23513b0-74f1-42f4-b763-8d91ff27,ds=SUBSCRIBER,o=DEFAULT,DC=C-NTDB
	// changetype: modify
	// replace: amfCounterValue
	// amfCounterValue: 12345
	@Override
	public Object modifyDUSCounter(String uid, String counterid,
			String counterValue, SessionConstants sessionConstants) {
		 PROVIDER_URL = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_MSISDN", sessionConstants)  + "/DC=C-NTDB";
		DirContext ctx = null ;
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL, SECURITY_CREDENTIALS, PROVIDER_URL));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Boolean rtn = new Boolean(false);
		if (ctx != null) {

			String query = "amfCounterId=%s,ds=amf,subdata=services,uid=%s,ds=SUBSCRIBER,o=DEFAULT";
			try {
				logger.info("counterid:: " + counterid + " uid===> " + uid
						+ " counterValue: " + counterValue);
				Attributes attrs = ctx.getAttributes(String.format(query,
						counterid, uid));
				logger.info("--------Before update---------");

				NamingEnumeration<String> atyrs = attrs.getIDs();
				while (atyrs != null && atyrs.hasMoreElements()) {
					try {
						String id = atyrs.next().toString();
						logger.info("Attr : " + id + " - " + attrs.get(id));
					} catch (Exception e) {
						System.err.println("error : " + e.toString());
					}

				}
				System.err.println("----------------------------------------");
				System.err.println("Updating... amfCounterValue to:"
						+ counterValue);
				System.err.println("----------------------------------------");
				Attribute attribute = new BasicAttribute("amfCounterValue",
						counterValue);
				ModificationItem[] item = new ModificationItem[1];
				item[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
						attribute);
				ctx.modifyAttributes(String.format(query, counterid, uid), item);

				logger.info("---------After update ---------");
				Attributes attrs1 = ctx.getAttributes(String.format(query,
						counterid, uid));
				NamingEnumeration<String> atyrs1 = attrs1.getIDs();

				while (atyrs1 != null && atyrs1.hasMoreElements()) {
					try {
						String id = atyrs1.next().toString();

						if (id.equalsIgnoreCase("amfCounterValue")) {
							String updatedValue = attrs1.get(id).get()
									.toString();
							logger.info("updatedValue:::: " + updatedValue
									+ " : " + counterValue);
							if (counterValue.equalsIgnoreCase(updatedValue)) {
								rtn = true;
							} else {
								rtn = false;
							}

						}
					} catch (Exception e) {
						System.err.println("error : " + e.toString());
					}

				}
				System.err.println("----------------------------------------");

			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return rtn;
	}

	// ldapsearch -h 10.25.72.247 -p 16800 -x -D cn=amfUser -w siemens -b
	// ban=1234501,ds=account,o=DEFAULT,dc=C-NTDB -a always -LLL
	// "objectclass=MSISDNINNSS"

	private List<BanSearchResponse> searchBAN(String ban, SessionConstants sessionConstants) {
		List<BanSearchResponse> list = new ArrayList<BanSearchResponse>();
		 PROVIDER_URL = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_MSISDN", sessionConstants)  + "/DC=C-NTDB";
		DirContext ctx = null;
		try {
			ctx = LDAPConnectionManager
					.getLdapContext(new LDAPInfo(LDAP_CONTEXT_FACTORY,
							SECURITY_AUTHENTICATION, SECURITY_PRINCIPAL_BAN,
							SECURITY_CREDENTIALS_BAN, PROVIDER_URL));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attr = { "msisdn" };
			sc.setReturningAttributes(attr);
			sc.setCountLimit(10);
			String query = "ban=%s,ds=account,o=DEFAULT";
			query = String.format(query, ban);
			String filter = "(objectclass=MSISDNINNSS)";
			System.out.println("bannnnnnnnnnnnnnn"+query);
			System.out.println("query"+ban);
			try {

				logger.info("----------------------------------------");
				logger.info("BAN search query :" + query + "\nfilter :"
						+ filter);
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);
				logger.info("BAN search result :" + result.hasMoreElements());
				logger.info("----------------------------------------");
				System.out.println("=================================");
				while (result != null && result.hasMoreElements()) {
					BanSearchResponse info = new BanSearchResponse();
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());
					// System.out.println("DN has " + dn.size() + " RDNs: ");
					for (int i = 0; i < dn.size(); i++) {
						String dnstr = dn.get(i);
						if (dnstr.contains("uid")) {
							info.setUid(dnstr.substring(dnstr.indexOf("=") + 1,
									dnstr.length()));
						}
						if (dnstr.contains("imsi")) {
							info.setImsi(dnstr.substring(
									dnstr.indexOf("=") + 1, dnstr.length()));
						}
						if (dnstr.contains("msisdn")) {
							info.setMsisdn(dnstr.substring(
									dnstr.indexOf("=") + 1, dnstr.length()));
						}
						 System.out.println("RDNs : " + dn.get(i));
					}
					// System.out.println("Response  : " + info.toString());
					list.add(info);
					/*
					 * Attributes attrs = rs.getAttributes();
					 * NamingEnumeration<String> atyrs = attrs.getIDs(); while
					 * (atyrs != null && atyrs.hasMoreElements()) { try { String
					 * id = atyrs.next().toString();
					 * System.out.println("Attr : " + id + " - " +
					 * attrs.get(id)); } catch (Exception e) {
					 * System.err.println("error : " + e.toString()); } }
					 */

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return list;
	}

	// ldapsearch -h 10.25.72.247 -p 16800 -x -D cn=amfUser -w siemens -b
	// ds=amf,subdata=services,msisdn=14254436068,dc=MSISDN,DC=C-NTDB -a always
	// "objectclass=*"

	private Counter getCounterInfo(String misdn,
			BanSearchResponse banSearchResponse, SessionConstants sessionConstants) {
		PROVIDER_URL_BAN = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_BAN", sessionConstants) + "/dc=%s,dc=C-NTDB";
		DirContext ctx = null;
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_BAN, SECURITY_CREDENTIALS_BAN, String
							.format(PROVIDER_URL_BAN, "MSISDN")));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			String[] attr = { "amfActiveOffers", "amfCounterId",
					"amfCounterValue", "amfCounterRegistrationDate",
					"amfCounterConsumptionDirection",
					"amfCounterLastModificationDate", "amfCounterUpperThr2" };
			sc.setReturningAttributes(attr);
			sc.setCountLimit(5);
			String query = "ds=amf,subdata=services,msisdn=%s";
			query = String.format(query, misdn);
			String filter = "(objectclass=*)";
			try {

				logger.info("----------------------------------------");
				logger.info("GET COUNTERS search query :" + query
						+ "\nfilter :" + filter);
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);
				logger.info("GET COUNTERS search result :"
						+ result.hasMoreElements());
				logger.info("----------------------------------------");
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());
					// System.out.println("DN has " + dn.size() + " RDNs: ");
					/*
					 * for (int i = 0; i < dn.size(); i++) {
					 * System.out.println("RDNs : " + dn.get(i)); }
					 */
					Attributes attrs = rs.getAttributes();

					// 1. Filter Counter Data
					DUSCounterHelper.fillCounterData(banSearchResponse, dn,
							attrs);
					/*
					 * NamingEnumeration<String> atyrs = attrs.getIDs(); while
					 * (atyrs != null && atyrs.hasMoreElements()) { try { String
					 * id = atyrs.next().toString();
					 * System.out.println("Attr : " + id + " - " +
					 * attrs.get(id)); } catch (Exception e) {
					 * System.err.println("error : " + e.toString()); } }
					 */

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		return null;
	}

	@Override
	public List<BanSearchResponse> doBANSearch(String ban, SessionConstants sessionConstants) {
		DUSUpdateRepositoryImpl dusHandler = new DUSUpdateRepositoryImpl();
		List<BanSearchResponse> list = dusHandler.searchBAN(ban,sessionConstants);
		for (BanSearchResponse banSearchResponse : list) {
			String msisdn = banSearchResponse.getMsisdn();
			if (msisdn != null) {
				dusHandler.getCounterInfo(msisdn, banSearchResponse, sessionConstants);
			}
		}
		return list;
	}

	public static void main(String[] args) {
		// TODO Do DUS search based on MSIDN show the all counter information
		// first
		USDDataHandler handler = new USDDataHandler();
	//	 handler.doDUSSearch(Constants.SEARCH_TYPE_MSISDN, "14259519027");
		DUSUpdateRepositoryImpl dusHandler = new DUSUpdateRepositoryImpl();
		//dusHandler.modifyDUSCounter("8ceff4aa-90e8-48ce-a2e3-53f640c7", "1001",
			//	"1234");

		// TODO first to the search based on BAN will get get the top f MSISDN
		// TODO then get the counter detail for each MSISDN and display it in
		// tab format (for each MSISDN)
		// dusHandler.doBANSearch("1234501");
		// dusHandler.getCounterInfo("14253833044");

	}

}
