package com.tmobile.edat.dusupdate.repository;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.ldap.LdapName;

import com.tmobile.edat.dusupdate.dto.BanSearchResponse;
import com.tmobile.edat.dusupdate.dto.Counter;

public class DUSCounterHelper {

	
	 public static void fillCounterData(BanSearchResponse banDataResponse,LdapName dn, Attributes attrs) throws NamingException {

	        Counter counter = new Counter();

	        if(attrs!=null){
	        	
				NamingEnumeration<String> atyrs = attrs.getIDs();
				Map<String ,String> attributes= new HashMap<String, String>();
				while (atyrs != null && atyrs.hasMoreElements()) {
					try {
						String id = atyrs.next().toString();
						if(id.equalsIgnoreCase("amfActiveOffers")){
							banDataResponse.addActiveOffers(attrs.get(id).get().toString());
						}else if(id.equalsIgnoreCase("amfCounterId")){
							counter.setCounterId(attrs.get(id).get().toString());
						}else if(id.equalsIgnoreCase("amfCounterValue")){
							counter.setCounterValue(attrs.get(id).get().toString());
						}else{
							attributes.put(id, attrs.get(id).get().toString());
						 
						}
					} catch (Exception e) {
						System.out.println("outor : " + e.toString());
					}
				}
				counter.setAttributes(attributes);
	            // Finally set Profile into napDataResponse
			//	System.out.println("Counter :"+counter.toString());
				if(counter.getCounterId()!=null){
					banDataResponse.addCounters(counter);
				}
	        }

	    }
	
}

