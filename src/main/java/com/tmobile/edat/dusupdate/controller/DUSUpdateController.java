package com.tmobile.edat.dusupdate.controller;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmobile.edat.dusupdate.component.DUSUpdateComponent;
import com.tmobile.edat.dusupdate.dto.BanSearchResponse;
import com.tmobile.edat.dusupdate.dto.DusSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.session.SessionConstants;

import org.springframework.http.MediaType;

@Controller
@RequestMapping("dusupdate")

/** Description of DUSUpdateController 
*
* DUSUpdateController class is used for Update Dus tool.
* 
* 
*/
public class DUSUpdateController {

	@Autowired
	@Qualifier("DUSUpdateComponent")
	private DUSUpdateComponent dusUpdateComponent;

	/**
	*
	* dusUpdate() method is used for show Update Dus Page.
	* 
	*/
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String dusUpdate() {

		return "/dus/dusUpdate";

	}

	
	
	/**
	*
	* dusMsisdnResponse() method is used for get msisdn search response.
	* 
	*/
	
	@RequestMapping(value = "/msisdn", method = RequestMethod.POST, 
					produces = MediaType.APPLICATION_JSON_VALUE, 
					consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public DusDataResponse dusMsisdnResponse(@RequestBody DusSearchRequest dusSearchRequest,HttpSession session) {	
		dusSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.dusUpdateComponent.getDUSData(dusSearchRequest);
	}
	
	
	
	
	/**
	*
	* dusBanResponse() method is used for get ban search response.
	* 
	*/

	@RequestMapping(value = "/ban", method = RequestMethod.POST ,produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<BanSearchResponse> dusBanResponse(@RequestBody DusSearchRequest dusSearchRequest,HttpSession session) {
		System.out.println("calling BAN action");
		dusSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.dusUpdateComponent.getBanSearchResponse(dusSearchRequest);

	}
	
	/**
	*
	* cntbDB() method is used for edit msisdn.
	* 
	*/
	
	@RequestMapping(value = "/editMSiSDN", method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Boolean editMSiSDN(@RequestBody DusSearchRequest dusSearchRequest,HttpSession session) {
		dusSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.dusUpdateComponent.editMsisdn(dusSearchRequest);	
	}

}
