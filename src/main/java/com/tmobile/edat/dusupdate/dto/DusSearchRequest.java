package com.tmobile.edat.dusupdate.dto;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.session.SessionConstants;

public class DusSearchRequest extends BaseRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	
	public DusSearchRequest() {
		super();
	}

	protected String msisdn;

	protected String ban;
	
	protected String counterId;
	
	protected String counterValue;
	
	protected String uid;
	
	protected SessionConstants sessionConstants;
	
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getBan() {
		return ban;
	}

	public void setBan(String ban) {
		this.ban = ban;
	}

	public String getCounterId() {
		return counterId;
	}

	public void setCounterId(String counterId) {
		this.counterId = counterId;
	}

	public String getCounterValue() {
		return counterValue;
	}

	public void setCounterValue(String counterValue) {
		this.counterValue = counterValue;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}

}
