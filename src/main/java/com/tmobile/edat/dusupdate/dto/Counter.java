package com.tmobile.edat.dusupdate.dto;

import java.io.Serializable;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;

public class Counter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6995310951347227961L;
	protected String counterId;
	protected String counterValue;

	 // Map store all attributes and values;
    protected Map<String, String> attributes;

	public String getCounterId() {
		return counterId;
	}

	public void setCounterId(String counterId) {
		this.counterId = counterId;
	}

	
	
	public String getCounterValue() {
		return counterValue;
	}

	public void setCounterValue(String counterValue) {
		this.counterValue = counterValue;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return "Counter [counterId=" + counterId + ", counterValue="
				+ counterValue + ", attributes=" + attributes + "]";
	}

	

}
