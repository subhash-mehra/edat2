package com.tmobile.edat.dusupdate.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tmobile.edat.base.BaseResponse;

public class BanSearchResponse extends BaseResponse implements Serializable {
	

    
	/**
	 * 
	 */
	private static final long serialVersionUID = 8584517173789567570L;
	protected String msisdn;
    protected String imsi;
    protected String uid;
    protected List<String> activeOffers  = new ArrayList<String>();;
    protected List<Counter> counters = new ArrayList<Counter>();
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public List<Counter> getCounters() {
		return counters;
	}
	public void addCounters(Counter counter) {
		this.counters.add(counter);
	}
	
	public List<String> getActiveOffers() {
		return activeOffers;
	}
	public void addActiveOffers(String activeOffer) {
		this.activeOffers.add(activeOffer);
	}
	@Override
	public String toString() {
		return "BanSearchResponse [msisdn=" + msisdn + ", imsi=" + imsi
				+ ", uid=" + uid + ", activeOffers=" + activeOffers
				+ ", counters=" + counters + "]";
	}
	
 

}
