/**
 * 
 */
package com.tmobile.edat.soaprequestxml;

/**
 * @author bharatbhushan.m
 *
 */
public class INSDPDataSoapRequest {
	
	public static String GetAccountDetails ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<methodCall>"
			+ "<methodName>GetAccountDetails</methodName> "
			+ "    <params> "
			+ "        <param> "
			+ "            <value> "
			+ "                <struct> "
			+ "                    <member> "
			+ "                        <name>originTransactionID</name> "
			+ "                        <value> "
			+ "                            <string>613504</string> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>originTimeStamp</name> "
			+ "                        <value> "
			+ "                            <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>subscriberNumber</name> "
			+ "                        <value> "
			+ "                            <string>%s</string> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>originNodeType</name> "
			+ "                        <value> "
			+ "                            <string>ESP</string> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>originHostName</name> "
			+ "                        <value> "
			+ "                            <string>ESP</string> "
			+ "                        </value> "
			+ "                    </member> "
		
			+ "                </struct> "
			+ "            </value> "
			+ "        </param> "
			+ "    </params> "
			+ "</methodCall> ";

	
	// user input MSISDN , Ammount, DATE
	public static String UpdateBalanceAndDate ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
			+ "<methodCall> "
			+ "<methodName>UpdateBalanceAndDate</methodName> "
			+ " <params> "
			+ "  <param> "
			+ "   <value> "
			+ "    <struct> "
			+ "     <member> "
			+ "      <name>originNodeType</name> "
			+ "       <value> "
			+ "        <string>PAG</string> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>originHostName</name> "
			+ "       <value> "
			+ "        <string>PAG</string> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>originTransactionID</name> "
			+ "       <value> "
			+ "        <string>12345</string> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>originTimeStamp</name> "
			+ "       <value> "
			+ "        <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>subscriberNumber</name> "
			+ "       <value> "
			+ "        <string>%s</string> "// 7874357106 4047516050
			+ "       </value> "
			+ "     </member> "
			+ "# transactionCurrency : Optional but Mandatory if adjust change any account # "
			+ "     <member> "
			+ "      <name>transactionCurrency</name> "
			+ "      <value> "
			+ "       <string>USD</string> "
			+ "      </value> "
			+ "     </member> "
			+ "# adjustmentAmountRelative : Optional # "
			+ "     <member> "
			+ "      <name>adjustmentAmountRelative</name> "//dynamic
			+ "      <value><string>%s</string></value> "
			+ "     </member> "
			+ "# supervisionExpiryDate : Optional #      "
			+ "     <member> "
			+ "      <name>supervisionExpiryDate</name> "// dynamic
			+ "       <value> "
			+ "        <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "       </value> "
			+ "     </member> "
			+ "# serviceFeeExpiryDate : Optional #      "
			+ "     <member> "
			+ "      <name>serviceFeeExpiryDate</name> "// dynamic
			+ "       <value> "
			+ "         <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "       </value> " + "     </member> "
			+ "# creditClearancePeriod : Optional # " 
			+ "     <member> "
			+ "      <name>creditClearancePeriod</name> "
			+ "      <value><int>0</int></value> " 
			+ "     </member> "
			+ "# serviceRemovalPeriod : Optional # " 
			+ "     <member> "
			+ "      <name>serviceRemovalPeriod</name> "
			+ "      <value><int>0</int></value> "
			+ "     </member> "
			+ "    </struct> " 
			+ "   </value> " 
			+ "  </param> " 
			+ " </params> "
			+ "</methodCall> ";

	public static String GetBalanceAndDate = "<?xml version=\"1.0\"?>"
			+ "<methodCall>"
			+ "<methodName>GetBalanceAndDate</methodName>"
			+ "<params>"
			+ "	<param>"
			+ "		<value>"
			+ "			<struct>"
			+ "				<member>"
			+ "					<name>originTransactionID</name>"
			+ "					<value>"
			+ "						<string>208100234618394636</string>"
			+ "					</value>"
			+ "				</member>"
			+ "				<member>"
			+ "					<name>originTimeStamp</name>" // dynamic current time
			+ "					<value>"
			+ "						<dateTime.iso8601>%S</dateTime.iso8601>"
			+ "					</value>" 
			+ "				</member>" 
			+ "				<member>"
			+ "					<name>originHostName</name>" 
			+ "					<value>"
			+ "						<string>epsi</string>" 
			+ "					</value>" 
			+ "				</member>"
			+ "				<member>" 
			+ "					<name>originNodeType</name>"
			+ "					<value><string>ADM</string></value>" 
			+ "				</member>"
			+ "				<member>" 
			+ "					<name>subscriberNumber</name>" // dynamic
			+ "					<value><string>%s</string></value>" 
			+ "				</member>"
			+ "			</struct>" 
			+ "		</value>" 
			+ "	</param>" 
			+ "</params>"
			+ "</methodCall>";
	
	
	public static String GetAccumulators = "<?xml version=\"1.0\"?> " + 
			 "<methodCall> " + 
			 "  <methodName>GetAccumulators</methodName> " + 
			 "  <params> " + 
			 "    <param> " + 
			 "      <value> " + 
			 "        <struct> " + 
			 "          <member> " + 
			 "            <name>originNodeType</name> " + 
			 "            <value> " + 
			 "              <string>EXT</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originHostName</name> " + 
			 "            <value> " + 
			 "              <string>10767714</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originTransactionID</name> " + 
			 "            <value> " + 
			 "              <string>100010000010001236</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originTimeStamp</name> " + 
			 "            <value> " + 
			 "              <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>subscriberNumber</name> " + 
			 "            <value> " + 
			 "              <string>%s</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "        </struct> " + 
			 "      </value> " + 
			 "    </param> " + 
			 "  </params> " + 
			 "</methodCall> ";
	
	public static String GetOffer="<?xml version=\"1.0\" encoding=\"utf-8\"?> " + 
 "<methodCall> " + 
 "<methodName>GetOffers</methodName> " + 
 "<params> " + 
 "<param> " + 
 "<value> " + 
 "<struct> " + 
 "<member> " + 
 "<name>originHostName</name> " + 
 "<value> " + 
 "<string>TEST</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>originNodeType</name> " + 
 "<value> " + 
 "<string>AIR</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>originTimeStamp</name> " + 
 "<value> " + 
 "<dateTime.iso8601>%s</dateTime.iso8601> " + //dynamic 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>originTransactionID</name> " + 
 "<value> " + 
 "<string>000006</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>requestInactiveOffersFlag</name> " + 
 "<value> " + 
 "<boolean>1</boolean> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>offerRequestedTypeFlag</name> " + 
 "<value> " + 
 "<string>11111111</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>subscriberNumber</name> " + 
 "<value> " + 
 "<string>%s</string> " + //dynamic 3138981502
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>subscriberNumberNAI</name> " + 
 "<value> " + 
 "<i4>2</i4> " + 
 "</value> " + 
 "</member> " + 
 "</struct> " + 
 "</value> " + 
 "</param> " + 
 "</params> " + 
 "</methodCall> ";
	
	public static String InstallSubscriber = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<methodCall> "
			+ "<methodName>InstallSubscriber</methodName> "
			+ "<params> "
			+ "<param> "
			+ "<value> "
			+ "<struct> "
			+ "<member> "
			+ "<name>originHostName</name> "
			+ "<value> "
			+ "<string>epsi</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originNodeType</name> "
			+ "                  <value> "
			+ "                     <string>ADM</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originTransactionID</name> "
			+ "                  <value> "
			+ "                     <string>156295616207844130</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originTimeStamp</name> " // dynamic current time
			+ "                  <value> "
			+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>serviceClassNew</name> "
			+ "                  <value> "
			+ "                     <int>%s</int> "// dynamic
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originOperatorID</name> "
			+ "                  <value> "
			+ "                     <string>add</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>subscriberNumber</name> "// dynamic
			+ "                  <value> "
			+ "                     <string>%s</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>languageIDNew</name> "// dynamic
			+ "                  <value> "
			+ "                     <int>%s</int> "
			+ "                  </value> "
			+ "               </member> "
			+ "  				<member> "
				+ "                  <name>ussdEndOfCallNotificationID</name> "// dynamic
				+ "                  <value> "
				+ "                     <int>%s</int> "
				+ "                  </value> " 
				+ "               </member> "
			/*+ "               <member> "
			+ "                  <name>serviceOfferings</name> "
			+ "                  <value> "
			+ "                     <array> "
			+ "                        <data> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> " // dynamic upto 20
			+ "                                    <value> "
			+ "                                       <int>1</int> "// dynamic
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "// dynamic
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>2</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>3</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>4</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>5</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>6</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>7</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>8</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>9</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>10</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>11</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>12</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>13</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>14</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>15</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>16</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>17</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>18</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>19</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>20</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                        </data> "
			+ "                     </array> " 
			+ "                  </value> "
			+ "               </member> "
			         
			+ "            </struct> " + "         </value> "
			+ "      </param> " + "   </params> " + "</methodCall> "*/; 

	
	public static String UpdateSubscriberSegmentation = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
			+ "<methodCall> "
			+ "   <methodName>UpdateSubscriberSegmentation</methodName> "
			+ "   <params> "
			+ "      <param> "
			+ "         <value> "
			+ "            <struct> "
			+ "               <member> "
			+ "                  <name>originTransactionID</name> "
			+ "                  <value> "
			+ "                     <string>056123662849772791</string> "
			+ "                  </value> "
			+ "               </member> ";
		/*	+ "               <member> "
			+ "                  <name>serviceOfferings</name> "
			+ "                  <value> "
			+ "                     <array> "
			+ "                        <data> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "// dynamic its upto 20
			+ "                                    <value> "
			+ "                                       <int>1</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "// dynamic
			+ "                                    <value> "
			+ "                                       <boolean>1</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>4</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>5</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>6</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>10</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>27</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>1</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>30</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>31</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                        </data> "
			+ "                     </array> "
			+ "                  </value> "
			+ "               </member> "*/
			
			public static String remainSubscriberSegmentation ="  <member> "
			+ "                  <name>originTimeStamp</name> " // dynamic current time
			+ "                  <value> "
			+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originHostName</name> "
			+ "                  <value> "
			+ "                     <string>epsi</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originNodeType</name> "
			+ "                  <value> "
			+ "                     <string>ADM</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originOperatorID</name> "
			+ "                  <value> "
			+ "                     <string>seattle</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>subscriberNumber</name> "// dynamic
			+ "                  <value> "
			+ "                     <string>%s</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "            </struct> " 
			+ "         </value> "
			+ "      </param> " + "   </params> " + "</methodCall> ";

	
			public static String UpdateAccumulators = "<?xml version=\"1.0\"?> "
					+ "<methodCall> "
					+ "   <methodName>UpdateAccumulators</methodName> "
					+ "   <params> "
					+ "      <param> "
					+ "         <value> "
					+ "            <struct> "
					+ "               <member> "
					+ "                  <name>originTransactionID</name> "
					+ "                  <value> "
					+ "                     <string>156295616207844130</string> "
					+ "                  </value> "
					+ "               </member> "
					+ "               <member> "
					+ "                  <name>accumulatorInformation</name> "
					+ "                  <value> "
					+ "                     <array> "
					+ "                        <data> "
					+ "                           <value> "
					+ "                              <struct> "
					+ "                                 <member> "
					+ "                                    <name>accumulatorValueAbsolute</name> "// dynamic
					+ "                                    <value> "
					+ "                                       <int>%s</int> "
					+ "                                    </value> "
					+ "                                 </member> "
					+ "                                 <member> "
					+ "                                    <name>accumulatorID</name> " // dynamic
					+ "                                    <value> "
					+ "                                       <int>%s</int> "
					+ "                                    </value> "
					+ "                                 </member> "
					+ "                              </struct> "
					+ "                           </value> "
					+ "                        </data> "
					+ "                     </array> "
					+ "                  </value> "
					+ "               </member> "
					+ "               <member> "
					+ "                  <name>originTimeStamp</name> "// dynamic current time
					+ "                  <value> "
					+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
					+ "                  </value> " + "               </member> "
					+ "               <member> "
					+ "                  <name>originHostName</name> "
					+ "                  <value> "
					+ "                     <string>epsi</string> "
					+ "                  </value> " + "               </member> "
					+ "               <member> "
					+ "                  <name>originNodeType</name> "
					+ "                  <value> "
					+ "                     <string>ADM</string> "
					+ "                  </value> " + "               </member> "
					+ "               <member> "
					+ "                  <name>originOperatorID</name> "
					+ "                  <value> "
					+ "                     <string>add</string> "
					+ "                  </value> " + "               </member> "
					+ "               <member> "
					+ "                  <name>subscriberNumber</name> "// dynamic
					+ "                  <value> "
					+ "                     <string>%s</string> "
					+ "                  </value> " 
					+ "               </member> "
					+ "            </struct> " 
					+ "         </value> "
					+ "      </param> " + "   </params> " + "</methodCall> ";
			
			
			public static String UpdateDABal ="<methodCall> " + 
					 "   <methodName>UpdateBalanceAndDate</methodName> " + 
					 "   <params> " + 
					 "      <param> " + 
					 "         <value> " + 
					 "            <struct> " + 
					 "               <member> " + 
					 "                  <name>dedicatedAccountUpdateInformation</name> " + 
					 "                  <value> " + 
					 "                     <array> " + 
					 "                        <data> " + 
					 "                           <value> " + 
					 "                              <struct> " + 
					 "                                 <member> " + 
					 "                                    <name>dedicatedAccountID</name> " + //dynamic
					 "                                    <value> " + 
					 "                                       <int>%s</int> " + 
					 "                                    </value> " + 
					 "                                 </member> " + 
					 "                                 <member> " + 
					 "                                    <name>dedicatedAccountValueNew</name> " + //dynamic
					 "                                    <value> " + 
					 "                                       <string>%s</string> " + 
					 "                                    </value> " + 
					 "                                 </member> " + 
					 "                              </struct> " + 
					 "                           </value> " + 
					 "                        </data> " + 
					 "                     </array> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>originHostName</name> " + 
					 "                  <value> " + 
					 "                     <string>epsi</string> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>originNodeType</name> " + 
					 "                  <value> " + 
					 "                     <string>ADM</string> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>originTransactionID</name> " + 
					 "                  <value> " + 
					 "                     <string>15612958563484735</string> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>originTimeStamp</name> " + //dynamic currentDate
					 "                  <value> " + 
					 "                     <dateTime.iso8601>%s</dateTime.iso8601> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>transactionCurrency</name> " + 
					 "                  <value> " + 
					 "                     <string>USD</string> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>originOperatorID</name> " + 
					 "                  <value> " + 
					 "                     <string>add</string> " + 
					 "                  </value> " + 
					 "               </member> " + 
					 "               <member> " + 
					 "                  <name>subscriberNumber</name> " + //dynamic
					 "                  <value> " + 
					 "                     <string>%s</string> " + //4256496831
					 "                  </value> " + 
					 "               </member> " + 
					 "            </struct> " + 
					 "         </value> " + 
					 "      </param> " + 
					 "   </params> " + 
					 "</methodCall> ";
			
			
			
			public static String UpdateOffer="<methodCall> " + 
					 "  <methodName>UpdateOffer</methodName> " + 
					 "  <params> " + 
					 "    <param> " + 
					 "      <value> " + 
					 "        <struct> " + 
					 "          <member> " + 
					 "            <name>originHostName</name> " + 
					 "            <value> " + 
					 "              <string>epsi</string> " + 
					 "            </value> " + 
					 "          </member> " + 
					 "          <member> " + 
					 "            <name>originNodeType</name> " + 
					 "            <value> " + 
					 "              <string>ADM</string> " + 
					 "            </value> " + 
					 "          </member> " + 
					 "          <member> " + 
					 "            <name>originTransactionID</name> " + 
					 "            <value> " + 
					 "              <string>159316052100191158</string> " + 
					 "            </value> " + 
					 "          </member> " + 
					 "          <member> " + 
					 "            <name>originTimeStamp</name> " + //dynamic currentDate
					 "            <value> " + 
					 "              <dateTime.iso8601>%s</dateTime.iso8601> " + 
					 "            </value> " + 
					 "          </member> " + 
					 "          <member> " + 
					 "            <name>originOperatorID</name> " + 
					 "            <value> " + 
					 "              <string>add</string> " + 
					 "            </value> " + 
					 "          </member> " + 
					 "          <member> " + 
					 "            <name>subscriberNumber</name> " + //dynamic
					 "            <value> " + 
					 "              <string>%s</string> " + //3138981514
					 "            </value> " + 
					 "          </member> " + 
					 "          <member> " + 
					 "            <name>offerID</name> " + //dynamic
					 "            <value> " + 
					 "              <int></int> " + //50001
					 "            </value> " + 
					 "          </member> " + 
					 "        </struct> " + 
					 "      </value> " + 
					 "    </param> " + 
					 "  </params> " + 
					 "</methodCall> ";
			
			public static String accumulatorsRemains = "<name>originTimeStamp</name> "// dynamic current time
			+ "                  <value> "
			+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originHostName</name> "
			+ "                  <value> "
			+ "                     <string>epsi</string> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originNodeType</name> "
			+ "                  <value> "
			+ "                     <string>ADM</string> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originOperatorID</name> "
			+ "                  <value> "
			+ "                     <string>add</string> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>subscriberNumber</name> "// dynamic
			+ "                  <value> "
			+ "                     <string>%s</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "            </struct> " 
			+ "         </value> "
			+ "      </param> " + "   </params> " + "</methodCall> ";

	
	public static	String UpdateServiceClass = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
			+ "<methodCall> "
			+ "<methodName>UpdateServiceClass</methodName> "
			+ "	<params> "
			+ "		<param> "
			+ "			<value> "
			+ "				<struct> "
			+ "					<member> "
			+ "						<name>originNodeType</name> "
			+ "							<value> "
			+ "								<string>PAG</string> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>originHostName</name> "
			+ "							<value> "
			+ "								<string>PAG</string> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>originTransactionID</name> "
			+ "							<value> "
			+ "								<string>12345</string> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>originTimeStamp</name> "// dynamic
			+ "							<value> "
			+ "								<dateTime.iso8601>%s</dateTime.iso8601> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>subscriberNumber</name> "// dynamic
			+ "							<value> "
			+ "								<string>%s</string> " 
			+ "							</value> "
			+ "					</member> " 
			+ "					<member> "
			+ "						<name>serviceClassAction</name> " 
			+ "							<value> "
			+ "								<string>SetOriginal</string> " 
			+ "							</value> "
			+ "					</member> " 
			+ "						<name>serviceClassNew</name> "// dynamic
			+ "							<value> " 
			+ "								<int>%s</int> " 
			+ "							</value> "
			+ "					</member> "
			+ "				</struct> " 
			+ "			</value> "
			+ "		</param> " + "	</params> " + "</methodCall> ";

	/*public static String activateAccount  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + 
			 "<methodCall> " + 
			 "<methodName>UpdateBalanceAndDate</methodName> " + 
			 " <params> " + 
			 "  <param> " + 
			 "   <value> " + 
			 "    <struct> " + 
			 "     <member> " + 
			 "      <name>originNodeType</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originHostName</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTransactionID</name> " + 
			 "       <value> " + 
			 "        <string>12345</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTimeStamp</name> " + // dynamic current time
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>subscriberNumber</name> " + // dynamic
			 "       <value> " + 
			 "        <string>%s</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# transactionCurrency : Optional but Mandatory if adjust change any account # " + 
			 "     <member> " + 
			 "      <name>transactionCurrency</name> " + 
			 "      <value> " + 
			 "       <string>USD</string> " + 
			 "      </value> " + 
			 "     </member> " + 
			 "# adjustmentAmountRelative : Optional # " + 
			 "     <member> " + 
			 "      <name>adjustmentAmountRelative</name> " + // dynamic
			 "      <value><string>10000</string></value> " + 
			 "     </member> " + 
			 "# supervisionExpiryDate : Optional #      " + 
			 "     <member> " + 
			 "      <name>supervisionExpiryDate</name> " + // dynamic date
			 "       <value> " + 
			 "        <dateTime.iso8601>20151231T11:15:21-0500</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# serviceFeeExpiryDate : Optional #      " + 
			 "     <member> " + 
			 "      <name>serviceFeeExpiryDate</name> " + // dynamic date
			 "       <value> " + 
			 "        <dateTime.iso8601>20151231T11:15:21-0500</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# creditClearancePeriod : Optional # " + 
			 "     <member> " + 
			 "      <name>creditClearancePeriod</name> " + 
			 "      <value><int>0</int></value> " + 
			 "     </member> " + 
			 "# serviceRemovalPeriod : Optional # " + 
			 "     <member> " + 
			 "      <name>serviceRemovalPeriod</name> " + 
			 "      <value><int>0</int></value> " + 
			 "     </member> " + 
			 "    </struct> " + 
			 "   </value> " + 
			 "  </param> " + 
			 " </params> " + 
			 "</methodCall> "; */
	
	
	public static String activateAccount  ="<?xml version=\"1.0\"?> " + 
			 "<methodCall> " + 
			 "<methodName>UpdateBalanceAndDate</methodName> " + 
			 " <params> " + 
			 "  <param> " + 
			 "   <value> " + 
			 "    <struct> " + 
			 "     <member> " + 
			 "      <name>originNodeType</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originHostName</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTransactionID</name> " + 
			 "       <value> " + 
			 "        <string>12345</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTimeStamp</name> " + // dynamic current time
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>subscriberNumber</name> " + // dynamic
			 "       <value> " + 
			 "        <string>%s</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# transactionCurrency : Optional but Mandatory if adjust change any account # " + 
			 "     <member> " + 
			 "      <name>transactionCurrency</name> " + 
			 "      <value> " + 
			 "       <string>USD</string> " + 
			 "      </value> " + 
			 "     </member> " + 
			 "# adjustmentAmountRelative : Optional # " + 
			 "     <member> " + 
			 "      <name>adjustmentAmountRelative</name> " + // dynamic
			 "      <value><string>%s</string></value> " + 
			 "     </member> " + 
			 "# supervisionExpiryDate : Optional #      " + 
			 "     <member> " + 
			 "      <name>supervisionExpiryDate</name> " + // dynamic date
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# serviceFeeExpiryDate : Optional #      " + 
			 "     <member> " + 
			 "      <name>serviceFeeExpiryDate</name> " + // dynamic date
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# creditClearancePeriod : Optional # " + 
			 "     <member> " + 
			 "      <name>creditClearancePeriod</name> " + 
			 "      <value><int>0</int></value> " + 
			 "     </member> " + 
			 "# serviceRemovalPeriod : Optional # " + 
			 "     <member> " + 
			 "      <name>serviceRemovalPeriod</name> " + 
			 "      <value><int>0</int></value> " + 
			 "     </member> " + 
			 "    </struct> " + 
			 "   </value> " + 
			 "  </param> " + 
			 " </params> " + 
			 "</methodCall> "; 
	
	public static String deleteSubscriber = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + 
			 "<methodCall> " + 
			 "<methodName>DeleteSubscriber</methodName> " + 
			 " <params> " + 
			 "  <param> " + 
			 "   <value> " + 
			 "    <struct> " + 
			 "     <member> " + 
			 "      <name>originNodeType</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originHostName</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTransactionID</name> " + 
			 "       <value> " + 
			 "        <string>12345</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTimeStamp</name> " + // dynamic Current time
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>subscriberNumber</name> " + // dynamic
			 "       <value> " + 
			 "        <string>%s</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originOperatorID</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "    </struct> " + 
			 "   </value> " + 
			 "  </param> " + 
			 " </params> " + 
			 "</methodCall> ";
	
}
