package com.tmobile.edat.ota.response;

import java.io.Serializable;

import com.tmobile.edat.base.BaseResponse;

public class OTADataResponse extends BaseResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String Iccid;
	private String msisdn;
	private String imsi;
	private String status;
	private String requestStatus;
	private String requestId;
	private String simCardProfileName;
	private String subscriptionStatus;
	

	public String getIccid() {
		return Iccid;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getSimCardProfileName() {
		return simCardProfileName;
	}

	public void setSimCardProfileName(String simCardProfileName) {
		this.simCardProfileName = simCardProfileName;
	}

	public String getSubscriptionStatus() {
		return subscriptionStatus;
	}

	public void setSubscriptionStatus(String subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}

	public void setIccid(String iccid) {
		Iccid = iccid;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String toString() {
		return "CNTBDBResponse [Iccid=" + Iccid + "imsi=" + imsi + "msisdn="
				+ msisdn + "status=" + status + "requestStatus="
				+ requestStatus + "]";

	}

}
