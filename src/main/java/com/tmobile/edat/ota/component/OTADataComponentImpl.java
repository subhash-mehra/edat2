package com.tmobile.edat.ota.component;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.ota.repository.OTADataRepository;
import com.tmobile.edat.ota.request.OTASearchRequest;
import com.tmobile.edat.ota.response.OTADataResponse;

@Component("OTADataComponent")
public class OTADataComponentImpl implements OTADataComponent {

	private static final Logger logger = Logger
			.getLogger(OTADataComponentImpl.class);

	@Autowired
	@Qualifier("OTADataRepository")
	OTADataRepository otaDataRepository = null;

	public OTADataComponentImpl() {
	}

	@Override
	public boolean validateRequest(BaseRequest baseRequest,
			BaseResponse baseResponse, int key) {

		if (baseRequest == null) {
			baseResponse = new OTADataResponse();
			baseResponse.setApplicationError(new ApplicationError(
					ApplicationConstants.NULL_REQUEST));
			return false;
		}

		return true;
	}

	@Override
	public OTADataResponse getOTASearch(OTASearchRequest otaSearchRequest) {

		logger.info("Searching in getOTASearch with  := " + otaSearchRequest);
		Map<String, String> otaAttributes = new HashMap<String, String>();
		OTADataResponse otaDataResponse = null;

		if (this.validateRequest(otaSearchRequest, otaDataResponse, 0)) {
			otaAttributes = this.otaDataRepository
					.doOTASearch(otaSearchRequest);
			//System.out.println("Siuze ::::::"+otaAttributes.size());
			if (otaAttributes.size() > 0) {
				otaDataResponse = new OTADataResponse();
				otaDataResponse.setIccid(otaAttributes.get("Iccid"));
				otaDataResponse.setMsisdn(otaAttributes.get("msisdn"));
				otaDataResponse.setImsi(otaAttributes.get("imsi"));
				otaDataResponse.setStatus(otaAttributes.get("status"));
				otaDataResponse.setRequestStatus(otaAttributes
						.get("requestStatus"));
				otaDataResponse.setRequestId(otaAttributes
						.get("requestId"));
				
				otaDataResponse.setSubscriptionStatus(otaAttributes
						.get("subscriptionStatus"));
				

				otaDataResponse.setSimCardProfileName(otaAttributes
						.get("simCardProfileName"));
				
				logger.info("Searching in OTADataResponse with  := "
						+ otaDataResponse.toString());
			}

		}
		return otaDataResponse;
	}

}
