package com.tmobile.edat.ota.component;

import com.tmobile.edat.base.BaseComponent;
import com.tmobile.edat.ota.request.OTASearchRequest;
import com.tmobile.edat.ota.response.OTADataResponse;

public interface OTADataComponent extends BaseComponent {

	public OTADataResponse getOTASearch(OTASearchRequest otaSearchRequest);

}
