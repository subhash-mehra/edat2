package com.tmobile.edat.ota.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmobile.edat.ota.component.OTADataComponent;
import com.tmobile.edat.ota.request.OTASearchRequest;
import com.tmobile.edat.ota.response.OTADataResponse;
import com.tmobile.session.SessionConstants;

@Controller
@RequestMapping("ota")
/** Description of OTADataController 
*
* OTADataController class is used for OTA. Data tool.
* 
* 
*/
public class OTADataController {

	@Autowired
	@Qualifier("OTADataComponent")
	private OTADataComponent otaDataComponent;

	 /**
		*
		* ota() method is used for show OTA Data tool main page.
		* 
		*/
	
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String ota() {
		return "/ota/ota";
	}

	
	/**
	*
	* otaMsisdnResponse(@RequestBody OTASearchRequest otaSearchRequest,HttpSession session) method is used for get search  for OTA Data .
	*  otaSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
	* 
	*/
	
	@RequestMapping(value = "/otaSearch", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public OTADataResponse otaMsisdnResponse(
			@RequestBody OTASearchRequest otaSearchRequest, HttpSession session) {
		otaSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.otaDataComponent.getOTASearch(otaSearchRequest);
	}

}
