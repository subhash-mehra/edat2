package com.tmobile.edat.ota.repository;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tmobile.edat.ota.request.OTASearchRequest;
import com.tmobile.edat.ota.response.OTADataResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

@Repository("OTADataRepository")
public class OTADataRepositoryImpl implements OTADataRepository {

	private static final Logger logger = Logger
			.getLogger(OTADataRepositoryImpl.class);

	public static String opmRequestByMsisdn = "<soapenv:Envelope xmlns:ser='http://smarttrust.com/opm/schema/service' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>   <soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-1' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:Username>opiuser</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>opiuser</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header>   <soapenv:Body>      <ser:GetSubscriptionInfo2Request>         <ser:Msisdn>%s</ser:Msisdn>      </ser:GetSubscriptionInfo2Request>   </soapenv:Body></soapenv:Envelope>";
	public static String opmRequestByIccid = "<soapenv:Envelope xmlns:ser='http://smarttrust.com/opm/schema/service' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>   <soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-1' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:Username>opiuser</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>opiuser</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header>   <soapenv:Body>      <ser:GetSubscriptionInfo2Request>         <ser:Iccid>%s</ser:Iccid>      </ser:GetSubscriptionInfo2Request>   </soapenv:Body></soapenv:Envelope>";
	public static String opmRequestByImsi = "<soapenv:Envelope xmlns:ser='http://smarttrust.com/opm/schema/service' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>   <soapenv:Header><wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'><wsse:UsernameToken wsu:Id='UsernameToken-1' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'><wsse:Username>opiuser</wsse:Username><wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>opiuser</wsse:Password></wsse:UsernameToken></wsse:Security></soapenv:Header>   <soapenv:Body>      <ser:GetSubscriptionInfo2Request>         <ser:Imsi>%s</ser:Imsi>      </ser:GetSubscriptionInfo2Request>   </soapenv:Body></soapenv:Envelope>";

	@Override
	public Map<String, String> doOTASearch(OTASearchRequest otaSearchRequest) {

		Map<String, String> attributes = new HashMap<String, String>();
		try {
			URLConnection urlconnection = getRequestHeader(otaSearchRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());

			if (otaSearchRequest.getMsisdn() != null
					&& otaSearchRequest.getIccid() == null
					&& otaSearchRequest.getImsi() == null) {

				wr.writeBytes(String.format(
						OTADataRepositoryImpl.opmRequestByMsisdn,
						otaSearchRequest.getMsisdn()));

			} else if (otaSearchRequest.getIccid() != null
					&& otaSearchRequest.getMsisdn() == null
					&& otaSearchRequest.getImsi() == null) {

				wr.writeBytes(String.format(
						OTADataRepositoryImpl.opmRequestByIccid,
						otaSearchRequest.getIccid()));

			} else if (otaSearchRequest.getImsi() != null
					&& otaSearchRequest.getIccid() == null
					&& otaSearchRequest.getMsisdn() == null) {

				wr.writeBytes(String.format(
						OTADataRepositoryImpl.opmRequestByImsi,
						otaSearchRequest.getImsi()));
			}
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream(), "UTF-8"));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = in.readLine()) != null) {
				sb.append(decodedString);
			}

			System.out.println("Responce::::::::::::: " + sb.toString());
			logger.info("Response XML for doOTASearch Details::"
					+ sb.toString());
			attributes = parseXmltoJava(sb.toString());
			// Map<String, String> attributes = parseXmltoJava("test");
			System.out.println("attributes::::::::::::: " + attributes);
			logger.info("In OTA Response Attributes size ++++++"
					+ attributes.size());

		} catch (Exception e) {
			logger.info("Error " + e.getMessage());
		}
		logger.info("attributeList:::::: " + attributes);
		return attributes;

	}

	public URLConnection getRequestHeader(SessionConstants sessionConstants) {
		URLConnection urlConnection = null;
		URL url;
		try {

			url = new URL("http://"+SessionConstantsFactory.getSessionConstant("OTA_SERVER_URL", sessionConstants) +"/opm");

			urlConnection = url.openConnection();
			urlConnection.addRequestProperty("Accept-Encoding", "gzip,deflate");
			urlConnection.addRequestProperty("Content-Type",
					"text/xml;charset=UTF-8");
			urlConnection.addRequestProperty("SOAPAction", "");
			urlConnection.setDoOutput(true);
		} catch (MalformedURLException e) {
			logger.info("In getRequestHeader +++++" + e.getMessage());

		} catch (IOException e) {
			logger.info("In getRequestHeader +++++" + e.getMessage());

		}
		return urlConnection;
	}

	public Map<String, String> parseXmltoJava(String xml) {

		Map<String, String> dataMap = new HashMap<String, String>();
		String Iccid = "";
		String msisdn = "";
		String imsi = "";
		String status = "";
		String requestStatus = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));

			Document doc = db.parse(is);
			NodeList nodes = doc
					.getElementsByTagName("ns3:GetSubscriptionInfo2Response");

			Element data = null;
			for (int i = 0; i < nodes.getLength(); i++) {
				
				NodeList innerList = nodes.item(i).getChildNodes();

				for (int j = 0; j < innerList.getLength(); j++) {
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:Status"))
				{
					dataMap.put("status", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:Iccid"))
				{
					dataMap.put("Iccid", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:Imsi"))
				{
					dataMap.put("imsi", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:Msisdn"))
				{
					dataMap.put("msisdn", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:SubscriptionStatus"))
				{
					dataMap.put("subscriptionStatus", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:SimCardProfileName"))
				{
					dataMap.put("simCardProfileName", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:RequestId"))
				{
					dataMap.put("requestId", innerList.item(j).getTextContent());
				}
				
				if(innerList.item(j).getNodeName().equalsIgnoreCase("ns3:RequestStatus"))
				{
					dataMap.put("requestStatus", innerList.item(j).getTextContent());
				}
				}
				/*NodeList reqStatus = element.getElementsByTagName("ns3:Status");

				data = (Element) reqStatus.item(0);
				requestStatus = getCharacterDataFromElement(data);

				NodeList statusCode = element
						.getElementsByTagName("ns3:RequestStatus");

				data = (Element) statusCode.item(0);
				status = getCharacterDataFromElement(data);
				System.out.println(requestStatus + "\n " + status);
				if (reqStatus != null && "OK".equals(status)) {
					NodeList IccidNode = element
							.getElementsByTagName("ns3:Iccid");
					if (IccidNode != null) {
						data = (Element) IccidNode.item(0);
						Iccid = getCharacterDataFromElement(data);
					} else {
						Iccid = " - ";
					}

					NodeList imsiNode = element
							.getElementsByTagName("ns3:Imsi");
					if (imsiNode != null) {
						data = (Element) imsiNode.item(0);
						imsi = getCharacterDataFromElement(data);

					} else {
						imsi = " - ";
					}

					NodeList msisdnNode = element
							.getElementsByTagName("ns3:Msisdn");
					if (msisdnNode != null) {
						data = (Element) msisdnNode.item(0);

						msisdn = getCharacterDataFromElement(data);

					} else {
						msisdn = " - ";
					}

				} else {
					dataMap.put("Iccid", Iccid);
					dataMap.put("msisdn", msisdn);
					dataMap.put("imsi", imsi);
					dataMap.put("status", status);
					dataMap.put("requestStatus", requestStatus);
					return dataMap;
				}
*/
			}
			
			

			/*dataMap.put("Iccid", Iccid);
			dataMap.put("msisdn", msisdn);
			dataMap.put("imsi", imsi);
			dataMap.put("status", status);
			dataMap.put("requestStatus", requestStatus);
*/
		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
		}

		return dataMap;
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "?";
	}

}
