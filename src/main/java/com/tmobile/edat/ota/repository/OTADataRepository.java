package com.tmobile.edat.ota.repository;

import java.util.Map;

import com.tmobile.edat.ota.request.OTASearchRequest;

public interface OTADataRepository {

	public Map<String, String> doOTASearch(OTASearchRequest otaSearchRequest);
}
