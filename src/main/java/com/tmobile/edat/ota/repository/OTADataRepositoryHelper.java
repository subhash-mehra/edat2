package com.tmobile.edat.ota.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.ldap.LdapName;

import org.apache.log4j.Logger;

import com.tmobile.edat.dusupdate.dto.Counter;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EpsData;
import com.tmobile.edat.usd.dto.response.GprsData;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.Partners;
import com.tmobile.edat.usd.dto.response.Profile;
import com.tmobile.edat.usd.dto.response.Socs;

public class OTADataRepositoryHelper {
	private static final Logger logger = Logger
			.getLogger(OTADataRepositoryHelper.class);

	public static List<String> fillUIDForOTA(LdapName dn, Attributes attrs,
			List<String> attributeList, Attribute attribute)
			throws NamingException {

		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					String id = atyrs.next().toString();

					if (attribute.contains("subscriber")) {
						if (id.equalsIgnoreCase("uid")) {

							attributeList.add(attrs.get(id).get().toString());
						}
					}
					if (attribute.contains("ACSUBDATA")) {
						if (id.equalsIgnoreCase("imsi")) {

							attributeList.add(attrs.get(id).get().toString());
						}
					}
					if (attribute.contains("MSISDNINNSS")) {
						if (id.equalsIgnoreCase("msisdn")) {

							attributeList.add(attrs.get(id).get().toString());
						}
					}

				} catch (Exception e) {
					logger.info("outor : " + e.toString());
				}

			}

		}

		return attributeList;
	}

	public static String getUID(LdapName dn, Attributes attrs)
			throws NamingException {
		String uid = "";
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					String id = atyrs.next().toString();

					if (id.equalsIgnoreCase("uid")) {

						uid = attrs.get(id).get().toString();
					}

				} catch (Exception e) {
					logger.info("outor : " + e.toString());
				}

			}

		}
		return uid;

	}
}
