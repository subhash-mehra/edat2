package com.tmobile.edat.ota.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.session.SessionConstants;

public class OTASearchRequest extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	public OTASearchRequest() {
		super();
	}

	protected String msisdn;
	protected String Iccid;
	protected String imsi;
	protected SessionConstants sessionConstants;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getIccid() {
		return Iccid;
	}

	public void setIccid(String iccid) {
		Iccid = iccid;
	}

	public String getImsi() {
		return imsi;
	}

	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

}
