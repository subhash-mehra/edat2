package com.tmobile.edat.admin.model;

import java.io.Serializable;
import java.sql.Date;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserAccess{
    private static final long serialVersionUID = 1L;

  
    protected String loginname;
    protected String pwd;
    protected String fname;
    protected String lname;
    protected String addUserRoleSelector;
    protected String note;
    protected Date createDate;    
   

    public UserAccess() {
    }
    
    
    public String getUserId()
    {
    	return loginname;
    }
    
    public void setUserId(String userId)
    {
    	this.loginname=userId;
    }
    
    public String getFirstName() {
        return fname;
    }

    public void setFirstName(String firstName) {
        this.fname = firstName;
    }
    
    
    public String getLastName() {
        return lname;
    }
    
    public void setLastName(String lastName) {
        this.lname = lastName;
    }

    
    
    
    public String getPassword() {
        return pwd;
    }

    public void setPassword(String password) {
        this.pwd = password;
    }

    public String getRoleName() {
        return addUserRoleSelector ;
    }
    
    public void setRoleName(String roleName) {
        this.addUserRoleSelector = roleName;
    }

    
    public String getNote() {
        return note;
    }
    
    public void setNote(String note) {
        this.note = note;
    }
    
    
    public Date getCreateDate() {
        return createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    @Override
    public String toString() {
        return "UserRequest [firstName="+fname+"lastName="+lname+"loginName=" + loginname + ", password="
                + pwd+ "roleName="+addUserRoleSelector+"note="+note+"createDate="+createDate+"]";
    }
}
