package com.tmobile.edat.admin.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: vikas_c
 * Date: 26/11/2014
 * Time: 12:53 PM
 * 
 */
public class ServerConfig implements Serializable{
    private static final long serialVersionUID = 1L;

  
    protected int id;
    protected String serverName;
    protected String serverIPDetails;
    protected String status;
    protected String isActive;
    protected String lastChangedBy;
    protected String lastChangedTime;
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	public String getServerIPDetails() {
		return serverIPDetails;
	}
	public void setServerIPDetails(String serverIPDetails) {
		this.serverIPDetails = serverIPDetails;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getLastChangedBy() {
		return lastChangedBy;
	}
	public void setLastChangedBy(String lastChangedBy) {
		this.lastChangedBy = lastChangedBy;
	}
	public String getLastChangedTime() {
		return lastChangedTime;
	}
	public void setLastChangedTime(String lastChangedTime) {
		this.lastChangedTime = lastChangedTime;
	}
	@Override
    public String toString() {
        return "UserRequest [id="+id+"serverName="+serverName+"serverIPDetails=" + serverIPDetails + ", status="
                + status+ "isActive="+isActive+"lastChangedBy="+lastChangedBy+"lastChangedTime="+lastChangedTime+"]";
    }
}
