package com.tmobile.edat.admin.Filter;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;


//import com.google.common.cache.LoadingCache;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.expression.AccessException;

 
public class LoadSalt implements Filter {
 
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
 
      
        HttpServletRequest httpReq = (HttpServletRequest) request;
       
        String uri = httpReq.getRequestURI();
        if(!uri.contains("resources") && !uri.equalsIgnoreCase("/edat/")&& !uri.equalsIgnoreCase("/edat/doLogin")&& !uri.contains(".js")&& !uri.contains(".css") && !uri.contains("dusupdate"))
        {
        Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)
            httpReq.getSession().getAttribute("csrfPreventionSaltCache");
 
        if (csrfPreventionSaltCache == null){
            csrfPreventionSaltCache = CacheBuilder.newBuilder()
                .maximumSize(5000)
                .expireAfterWrite(20, TimeUnit.MINUTES).build(
                        new CacheLoader<String, Boolean>() {
                          public Boolean load(String Object) throws AccessException {
                            return true;
                          }
                        });
 
            httpReq.getSession().setAttribute("csrfPreventionSaltCache", csrfPreventionSaltCache);
        }
 
        // Generate the salt and store it in the users cache
        String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
       // csrfPreventionSaltCache.put(salt, Boolean.TRUE);
 
        // Add the salt to the current request so it can be used
        // by the page rendered in this request
        httpReq.setAttribute("csrfPreventionSalt", salt);
        chain.doFilter(request, response);
        }else
        {
        String salt = RandomStringUtils.random(20, 0, 0, true, true, null, new SecureRandom());
        httpReq.setAttribute("csrfPreventionSalt", salt);
        chain.doFilter(request, response);
        }
        }
    
 
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
 
    @Override
    public void destroy() {
    }
}