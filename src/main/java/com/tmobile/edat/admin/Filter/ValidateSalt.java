package com.tmobile.edat.admin.Filter;

import com.google.common.cache.Cache;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ValidateSalt implements Filter  {

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
       throws IOException, ServletException {
	  
       // Assume its HTTP
       HttpServletRequest httpReq = (HttpServletRequest) request;
       HttpServletResponse httpRes = (HttpServletResponse) response;
       String uri = httpReq.getRequestURI();
       
       // Get the salt sent with the request
       String salt = (String) httpReq.getParameter("csrfPreventionSalt");

       // Validate that the salt is in the cache
       Cache<String, Boolean> csrfPreventionSaltCache = (Cache<String, Boolean>)
           httpReq.getSession().getAttribute("csrfPreventionSaltCache");
       try {
    	  
    	   if(!uri.contains("resources") && !uri.equalsIgnoreCase("/edat/") && !uri.equalsIgnoreCase("/edat/doLogin")&& !uri.contains(".js")&& !uri.contains(".css")  && !uri.contains("dusupdate"))
    	   {
    	   if (csrfPreventionSaltCache != null &&
		           salt != null &&
		           csrfPreventionSaltCache.get(salt) != null){
		       // If the salt is in the cache, we move on
		       chain.doFilter(request, response);
		   } else {
		       // Otherwise we throw an exception aborting the request flow
		       //throw new ServletException("Potential CSRF detected!! Inform a scary sysadmin ASAP.");
			   String validUser="Not Authorized Session.............";
			   String url=httpReq.getRequestURI();
			   			   httpReq.setAttribute("validUser", validUser);
			   //httpRes.sendRedirect("/edat/checkValidUser");
			   httpReq.getRequestDispatcher("/checkValidUser").forward(httpReq, httpRes);
			 
		   }
    	   }else
    	   {
    		   salt = (String) httpReq.getAttribute("csrfPreventionSalt");
    		   chain.doFilter(request, response);
    	   }
    	 
	} catch (ExecutionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
         }

   @Override
   public void init(FilterConfig filterConfig) throws ServletException {
   }

   @Override
   public void destroy() {
   }
}