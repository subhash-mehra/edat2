package com.tmobile.edat.admin.Filter;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class TimeoutSessionListner
 *
 */
public class TimeoutSessionListner implements HttpSessionListener
{

	HttpServletRequest httpReq;
	 HttpServletResponse httpRes;
    /**
     * Default constructor. 
     */
    public TimeoutSessionListner() {
    	 System.out.println("call---------------------------------------------------------");
    	try {
			httpReq.getRequestDispatcher("/checkValidUser").forward(httpReq, httpRes);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void sessionCreated(HttpSessionEvent event){
    	 System.out.println("session created----------------------------------------------------------");
        event.getSession().setMaxInactiveInterval(1*60); //in seconds
       
      }
      public void sessionDestroyed(HttpSessionEvent event)
      {
    	  HttpSession session = event.getSession();
          session.invalidate();
    	  System.out.println("session destroy----------------------------------------------------------");
    	  try {
			httpReq.getRequestDispatcher("/checkValidUser").forward(httpReq, httpRes);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	  
      }
}
