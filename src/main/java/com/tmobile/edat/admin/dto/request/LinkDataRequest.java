package com.tmobile.edat.admin.dto.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;

/**
 * Created with IntelliJ IDEA.
 * User: vikas_c
 * Date: 28/11/2014
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class LinkDataRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String reportDate;
    protected String link;
    protected String year;
    protected String toyear;
    protected String fromMonth;
    protected String toMonth;
    protected String showFlag;
    public String getShowFlag() {
		return showFlag;
	}

	public void setShowFlag(String showFlag) {
		this.showFlag = showFlag;
	}

	protected String currentPage;
   
   

    public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public LinkDataRequest() {
    }
    
	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public String getToyear() {
		return toyear;
	}

	public void setToyear(String toyear) {
		this.toyear = toyear;
	}

	@Override
    public String toString() {
        return "LinkDataRequest [reportDate="+reportDate+",link="+link+",year="+year+"fromMonth="+fromMonth+"toMonth="+toMonth+"]";
    }
}
