package com.tmobile.edat.admin.dto.request;

import java.io.Serializable;
import java.sql.Date;

import com.tmobile.edat.base.BaseRequest;

public class changePasswordRequest extends BaseRequest  implements Serializable {
	  private static final long serialVersionUID = 1L;

	  
	    protected String oldPassword;
	    protected String newPassword;
	    protected String confirmNewPassword;
	    protected String userId;
	   

	    public changePasswordRequest() {
	    }
	    
	    
	    public String getOldPassword()
	    {
	    	return oldPassword;
	    }
	    
	    public void setOldPassword(String oldPassword)
	    {
	    	this.oldPassword=oldPassword;
	    }
	    
	    public String getNewPassword() {
	        return newPassword;
	    }

	    public void setNewPassword(String newPassword) {
	        this.newPassword = newPassword;
	    }
	    
	    
	    public String getConfirmNewPassword() {
	        return confirmNewPassword;
	    }
	    
	    public void setConfirmNewPassword(String confirmNewPassword) {
	        this.confirmNewPassword = confirmNewPassword;
	    }

	    public String getUserId()
	    {
	    	return userId;
	    }
	    
	    public void setUserId(String userId)
	    {
	    	this.userId=userId;
	    }
	   
	    @Override
	    public String toString() {
	        return "UserRequest [oldPassword="+oldPassword+"newPassword="+newPassword+"confirmNewPassword=" + confirmNewPassword+"userId="+"]";
	    }
	}
