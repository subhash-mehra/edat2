package com.tmobile.edat.admin.dto.request;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String userid;
    protected String loginname;
    protected String pwd;
    protected String fname;
    protected String lname;
    protected List<String> addUserRoleSelector;
    protected String note;
    protected Date createDate;    
   

    public UserRequest() {
    }
    



    public String getUserid() {
		return userid;
	}




	public void setUserid(String userid) {
		this.userid = userid;
	}




	public String getLoginname() {
		return loginname;
	}




	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}




	public String getPwd() {
		return pwd;
	}




	public void setPwd(String pwd) {
		this.pwd = pwd;
	}




	public String getFname() {
		return fname;
	}




	public void setFname(String fname) {
		this.fname = fname;
	}




	public String getLname() {
		return lname;
	}




	public void setLname(String lname) {
		this.lname = lname;
	}




	public List<String> getAddUserRoleSelector() {
		return addUserRoleSelector;
	}




	public void setAddUserRoleSelector(List<String> addUserRoleSelector) {
		this.addUserRoleSelector = addUserRoleSelector;
	}




	public String getNote() {
		return note;
	}




	public void setNote(String note) {
		this.note = note;
	}




	public Date getCreateDate() {
		return createDate;
	}




	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}




	@Override
    public String toString() {
        return "UserRequest [firstName="+fname+"lastName="+lname+"loginName=" + loginname + ", password="
                + pwd+ "roleName="+addUserRoleSelector+"note="+note+"createDate="+createDate+"]";
    }
}
