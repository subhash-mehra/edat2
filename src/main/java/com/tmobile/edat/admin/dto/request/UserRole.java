package com.tmobile.edat.admin.dto.request;

import java.io.Serializable;
import java.sql.Date;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserRole {
   

  
    protected int roleId;
    protected String roleName;
    

    public UserRole() {
    }
    
    
    public int getRoleId()
    {
    	return roleId;
    }
    
    public void setRoleId(int roleId)
    {
    	this.roleId=roleId;
    }
    
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    
    
       @Override
    public String toString() {
        return "UserRole [roleId="+roleId+"roleName="+roleName+"]";
    }
}
