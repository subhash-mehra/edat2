package com.tmobile.edat.admin.component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.edat.admin.repository.AdminDataRepository;
import com.tmobile.edat.admin.dto.request.*;
import com.tmobile.edat.login.model.*;
import com.tmobile.filter.store.LinkData;

@Component("AdminDataComponent")
public class AdminDataComponentImpl implements AdminDataComponent {

	private static final Logger logger = Logger
			.getLogger(AdminDataComponentImpl.class);

	@Autowired
	@Qualifier("AdminDataRepository")
	AdminDataRepository adminDataRepository = null;

	@Override
	public boolean saveNewUser(UserRequest userRequest) {
		boolean saveFlag = false;
		if (userRequest.getLoginname() != null
				&& userRequest.getPwd() != null
				&& userRequest.getFname() != null
				&& userRequest.getLname() != null
				&& userRequest.getAddUserRoleSelector() != null) {
			saveFlag = adminDataRepository.saveNewUser(userRequest);
		} else {

		}

		return saveFlag;
	}

	@Override
	public boolean updateUser(UserRequest userRequest) {
		boolean updateUserFlag = false;
		if (userRequest.getUserid() != null) {
			updateUserFlag = adminDataRepository.updateUser(userRequest);
		} else {

		}

		return updateUserFlag;

	}

	// For Deleting Existing User

	@Override
	public boolean deleteUser(String userId) {
		boolean deleteUserFlag = false;
		if (userId != null) {
			deleteUserFlag = adminDataRepository.deleteUser(userId);
		} else {

		}

		return deleteUserFlag;

	}

	@Override
	public boolean checkExistingUser(UserRequest userRequest) {
		boolean existingUserFlag = false;
		if (userRequest.getLoginname() != null) {
			existingUserFlag = adminDataRepository
					.checkExistingUser(userRequest);
		} else {

		}

		return existingUserFlag;

	}

	// For Get User Role

	@Override
	public List<UserRole> getUserRoleList() {
		List<UserRole> userRoleList = new ArrayList<UserRole>();
		userRoleList = adminDataRepository.getUserRoleList();

		return userRoleList;

	}

	// For Get Users List

	@Override
	public List<UserAccess> getUsersList() {
		List<UserAccess> usersList = new ArrayList<UserAccess>();
		usersList = adminDataRepository.getUsersList();

		return usersList;

	}
	@Override
	public List<LinkData> showReports(){
		return adminDataRepository.showReports();
	}
	@Override
	public String showDayWiseReports(LinkDataRequest linkDataRequest){
		return adminDataRepository.showDayWiseReports(linkDataRequest);
	}
	@Override
	public Map<String,Object> changeServerConfig(Map<String,Object> map){
		return adminDataRepository.changeServerConfig(map);
	}
	@Override
	public List<UserAccess> showUpdateUser(String userId) {
		List<UserAccess> usersList = new ArrayList<UserAccess>();
		usersList = adminDataRepository.showUpdateUser(userId);

		return usersList;

	}

	@Override
	public boolean changePassword(changePasswordRequest changePasswordRequest) {
		boolean passwordUpdateFlag = false;
		if (changePasswordRequest.getOldPassword() != null) {
			passwordUpdateFlag = adminDataRepository
					.changePassword(changePasswordRequest);
		} else {

		}

		return passwordUpdateFlag;

	}
	@Override
	public boolean executeQuery() {
		return adminDataRepository.executeQuery();		
	}

}
