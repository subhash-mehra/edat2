package com.tmobile.edat.admin.repository;

import java.util.List;
import java.util.Map;

import com.tmobile.edat.admin.dto.request.*;
import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.edat.login.model.*;
import com.tmobile.filter.store.LinkData;

//CRUD operations
public interface AdminDataRepository {

	public boolean saveNewUser(UserRequest userRequest);

	public boolean updateUser(UserRequest userRequest);

	public boolean deleteUser(String userId);

	public boolean checkExistingUser(UserRequest userRequest);

	public List<UserRole> getUserRoleList();

	public List<UserAccess> getUsersList();

	public List<UserAccess> showUpdateUser(String userId);

	public boolean changePassword(changePasswordRequest changePasswordRequest);
	public List<LinkData> showReports();
	public String showDayWiseReports(LinkDataRequest linkDataRequest);
	public Map<String,Object> changeServerConfig(Map<String,Object> map);
	public boolean executeQuery();
}