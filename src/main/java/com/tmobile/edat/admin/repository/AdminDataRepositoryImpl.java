package com.tmobile.edat.admin.repository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.tmobile.edat.util.*;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tmobile.edat.admin.dto.request.*;
import com.tmobile.edat.admin.model.ServerConfig;
import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.edat.login.model.*;
import com.tmobile.filter.store.LinkData;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionEventTrack;

import org.springframework.jdbc.core.RowMapper;

@Repository("AdminDataRepository")
public class AdminDataRepositoryImpl implements AdminDataRepository {
	private static Logger log = Logger.getLogger(AdminDataRepositoryImpl.class);
	
	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	// For Create New User

	@Override
	public boolean saveNewUser(UserRequest userRequest) {

		String encodedPassword = "false";
		boolean userSaveFlag = false;
		boolean userExistFlag = false;
		Date currentDate = new Date();
		if (userRequest.getPwd() != null) {
			try {
				encodedPassword = JavaUtil.encodeString(userRequest.getPwd());
			} catch (Exception e) {
				e.printStackTrace();
			}

			userExistFlag = checkExistingUser(userRequest);

			if (!userExistFlag) {
				String sql = "INSERT INTO user_access (first_name,last_name,userid,password,role_name,note,create_dttm) VALUES (?, ?, ?, ?,?,?,?)";

				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String userRole="";
				if(userRequest.getAddUserRoleSelector()!=null && userRequest.getAddUserRoleSelector().size()>0){
					for(String role : userRequest.getAddUserRoleSelector()){
						if(userRole!=null && !userRole.equals("")){
							userRole+=","+role;
						}
						else{
							userRole=role;
						}
					}
				}
				Object[] params = new Object[] { userRequest.getFname(),
						userRequest.getLname(), userRequest.getLoginname(),
						encodedPassword, userRole,
						userRequest.getNote(), currentDate };
				int[] types = new int[] { Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.DATE };
				int row = jdbcTemplate.update(sql, params, types);
				System.out.println(row + " row inserted.");

				if (row > 0) {
					userSaveFlag = true;
				}

			}
		}
		return userSaveFlag;
	}

	// For Update Existing User

	@Override
	public boolean updateUser(UserRequest userRequest) {
		String encodedPassword = "";
		boolean updateUserFlag = false;
		Date currentDate = new Date();
		if (userRequest.getPwd() != null) {
			try {
				encodedPassword = JavaUtil.encodeString(userRequest
						.getPwd());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String updateSql = "UPDATE user_access set userid=?,first_name=?,last_name=?,password=?,role_name=?,note=?,create_dttm=? where userid=?";
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String userRole="";
		if(userRequest.getAddUserRoleSelector()!=null && userRequest.getAddUserRoleSelector().size()>0){
			for(String role : userRequest.getAddUserRoleSelector()){
				if(userRole!=null && !userRole.equals("")){
					userRole+=","+role;
				}
				else{
					userRole=role;
				}
			}
		}
		Object[] params = new Object[] {userRequest.getUserid(),userRequest.getFname(),
				userRequest.getLname(),encodedPassword,
				userRole,
				userRequest.getNote(),currentDate,userRequest.getUserid() };
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR,Types.VARCHAR, Types.VARCHAR,Types.DATE,Types.VARCHAR};
		int row = jdbcTemplate.update(updateSql, params, types);

		if (row > 0) {
			updateUserFlag = true;
		}

		return updateUserFlag;
	}

	// For Deleting Existing User

	@Override
	public boolean deleteUser(String userId) {
		String encodedPassword = "";
		boolean deleteUserFlag = false;

		try {
			String updateSql = "delete from user_access where userid='"
					+ userId + "'";
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			int row = jdbcTemplate.update(updateSql);

			if (row > 0) {
				deleteUserFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return deleteUserFlag;
	}

	// For Check Existing User

	@Override
	public boolean checkExistingUser(UserRequest userRequest) {
		boolean existingUserFlag = false;
		String query = "select count(*) from user_access where lower(userid)='"
				+ userRequest.getLoginname().toLowerCase() + "'";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		int count = jdbcTemplate.queryForInt(query);
		if (count == 1) {
			existingUserFlag = true;
		}

		return existingUserFlag;
	}

	@Override
	public List<UserRole> getUserRoleList() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		return jdbcTemplate.query("select * from user_roles",
				new RowMapper<UserRole>() {
					@Override
					public UserRole mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						UserRole e = new UserRole();
						e.setRoleId(rs.getInt(1));
						e.setRoleName(rs.getString(2));
						return e;
					}
				});

	}

	@Override
	public List<UserAccess> getUsersList() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		return jdbcTemplate.query("select * from user_access",
				new RowMapper<UserAccess>() {
					@Override
					public UserAccess mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						UserAccess e = new UserAccess();
						e.setUserId(rs.getString(1));
						e.setFirstName(rs.getString(2));
						e.setLastName(rs.getString(3));
						e.setPassword(rs.getString(4));
						e.setRoleName(rs.getString(5));
						e.setNote(rs.getString(6));
						e.setCreateDate(rs.getDate(7));

						return e;
					}
				});

	}
	/* @Author Vikas Choudhary
	 * (non-Javadoc)
	 * @see com.tmobile.edat.admin.repository.AdminDataRepository#showReports()
	 */
	@Override
	public List<LinkData> showReports() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		
		return jdbcTemplate.query("select * from manage_session_data order by login_id asc",
				new RowMapper<LinkData>() {
					@Override
					public LinkData mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						LinkData linkData = new LinkData();
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						linkData.setLoginId(rs.getString(2));
						linkData.setMessageData(rs.getString(3));					
						linkData.setLogoutTime(dateFormat.format(rs.getDate(4)));
						return linkData;
					}
				});
		
		
	}
	@Override
	public String showDayWiseReports(LinkDataRequest linkDataRequest) {
		linkDataRequest.getReportDate();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String msg="";
		String whereCondition="";
		if(linkDataRequest!=null && !linkDataRequest.getLink().equals("") && linkDataRequest.getLink().equalsIgnoreCase("dayWise")){
		if(linkDataRequest!=null && linkDataRequest.getReportDate()!=null && !linkDataRequest.getReportDate().equals("")){
			/*For mysql*/
		
		//	whereCondition=" where DATE_FORMAT(logout_time,'%m/%d/%Y')='"+linkDataRequest.getReportDate()+"'";
			/*For Oracle*/
			//whereCondition=" where to_date(logout_time,'%m/%d/%Y')=to_date('"+linkDataRequest.getReportDate()+"','%m/%d/%Y')";
			whereCondition=" where TO_CHAR(logout_time, 'MM/DD/YYYY')='"+linkDataRequest.getReportDate()+"'";
			
			log.info("whereCondition----------------------------- " + whereCondition);
			log.info("Date=====In linkDataRequest====----------------------------- " + linkDataRequest.getReportDate());
			
			
		}else{
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();		
			String currentDate = dateFormat.format(date);
			/*For mysql*/
			//whereCondition=" where DATE_FORMAT(logout_time,'%m/%d/%Y')='"+currentDate+"'";
			/*For oracle*/
			whereCondition=" where TO_CHAR(logout_time,'MM/DD/YYYY')='"+currentDate+"'";
			log.info("whereCondition----------------------------- " + whereCondition);
			log.info("Date====not in linkDataRequest=====----------------------------- " + currentDate);
			
		}
		}
		
		
		if(linkDataRequest!=null && !linkDataRequest.getLink().equals("") && linkDataRequest.getLink().equalsIgnoreCase("monthWise")){
			if(linkDataRequest!=null && linkDataRequest.getFromMonth()!=null && !linkDataRequest.getFromMonth().equals("") && linkDataRequest.getToMonth()!=null && !linkDataRequest.getToMonth().equals("")){
			/*For Oracle*/	// whereCondition=" where TO_CHAR(logout_time, 'MM-YYYY') between '"+linkDataRequest.getFromMonth()+"-"+linkDataRequest.getYear()+"' and '"+linkDataRequest.getToMonth()+"-"+linkDataRequest.getYear()+"'";
				//For mysql
				//whereCondition="where EXTRACT( YEAR_MONTH FROM `logout_time` ) between '"+linkDataRequest.getYear()+""+linkDataRequest.getFromMonth()+"' and '"+linkDataRequest.getYear()+""+linkDataRequest.getToMonth()+"'";
				
				whereCondition=" where TO_CHAR(logout_time, 'MM-YYYY') between '"+linkDataRequest.getFromMonth()+"-"+linkDataRequest.getYear()+"' and '"+linkDataRequest.getToMonth()+"-"+linkDataRequest.getToyear()+"'";
				log.info("whereCondition----------------------------- " + whereCondition);
				log.info("Date=====In linkDataRequest====----------------------------- " + linkDataRequest.getReportDate());
				
				
			}else{
				Calendar now = Calendar.getInstance();
				int year = now.get(Calendar.YEAR);
				int month = now.get(Calendar.MONTH); // Note: zero based!
				
				System.out.println("Current Year---"+year+" ======Current Month---"+month);
				/*For mysql syntax	*/
				//whereCondition="where EXTRACT( YEAR_MONTH FROM `logout_time` ) between '201411' and '201412'"; 
				/*For mysql syntax	*/
				whereCondition=" where TO_CHAR(logout_time, 'MM-YYYY') between '"+month+"-"+year+"' and '"+month+"-"+year+"'";
				log.info("whereCondition----------------------------- " + whereCondition);
				log.info("Date====not in linkDataRequest=====----------------------------- " + month);
				
			}
			}
		 List<LinkData> link = jdbcTemplate.query("select * from manage_session_data "+whereCondition+" order by logout_time desc",
				new RowMapper<LinkData>() {
					@Override
					public LinkData mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						LinkData linkData = new LinkData();
						linkData.setMessageData(rs.getString(3));
						linkData.setLogoutTime(rs.getString(4));
						return linkData;
					}
				});
		 if(link!=null && link.size()>0){
			
			 for(LinkData l : link){
				 if(l.getLogoutTime()!=null && !l.getLogoutTime().equals("") && l.getMessageData()!=null && !l.getMessageData().equals("")){
					if(msg!=null && !msg.equals("")){
						//msg+=",{\"logout\":\""+l.getLogoutTime().substring(0,10)+"\","+l.getMessageData().substring(1, l.getMessageData().length());
						msg+=",{\"logout\":\""+l.getLogoutTime()+"\","+l.getMessageData().substring(1, l.getMessageData().length());
					}else{
						//msg="{\"logout\":\""+l.getLogoutTime().substring(0,10)+"\","+l.getMessageData().substring(1, l.getMessageData().length());
						msg="{\"logout\":\""+l.getLogoutTime()+"\","+l.getMessageData().substring(1, l.getMessageData().length());
					}
				 }
			 }
		 }
		return msg;
	}
	@Override
	public Map<String,Object> changeServerConfig(Map<String,Object> map) {
		Map<String,Object> map1 = new HashMap<String, Object>();
		boolean isSaved=false;
		SessionConstants sessionConstants = (SessionConstants)map.get("sessionConstants");
		if(map.get("saveData")!=null){
			try{
			String saveData = (String)map.get("saveData");
			if(saveData!=null && !saveData.equals("") && saveData.equals("1")){
				/* For Global Config enable below line */
				//isSaved = modifyServerSetting(map);
				
				/* For session based config*/
				try{
					sessionConstants = reloadSessionVariable((Integer) map.get("serverId"),sessionConstants);
					isSaved=true;
				}catch (Exception e) {
					
					try{
						sessionConstants.setLDAP_SERVER(null);
						sessionConstants.setLDAP_SERVER_NAP(null);
						sessionConstants.setLDAP_SERVER_DUS(null);
						sessionConstants.setLDAP_SERVERL_PCRF(null);
						sessionConstants.setLDAP_SERVER_UMA(null);
						sessionConstants.setLDAP_SERVER_HLR(null);
						sessionConstants.setLDAP_SERVER_EIR(null);
						sessionConstants.setLDAP_SERVER_HSS(null);	
						sessionConstants.setLDAP_SERVER_PCRF_SESSION(null);
						sessionConstants.setLDAP_SERVER_CNTB(null);
						sessionConstants.setCNTB_StringUrl(null);
						sessionConstants.setLDAP_SERVER_MSISDN(null);
						sessionConstants.setLDAP_SERVER_BAN(null);
						sessionConstants.setLDAP_SERVER_IAM(null);
						sessionConstants.setLDAP_SERVER_SMSC(null);	
						sessionConstants.setSERVER_MMSC(null);
						sessionConstants.setSMSC_API_URL(null);				
						sessionConstants.setOTA_SERVER_URL(null);
						sessionConstants.setINSDP_API_URL(null);
						sessionConstants.setINSDP_StringUrl(null);
						sessionConstants.setINSDP_HOST(null);	
						sessionConstants.setTELNET_HOST(null);
						sessionConstants.setDEFAULT_SERVER(0);
						}catch(Exception e1){}
				}
				
			}
			}catch(Exception e){ e.printStackTrace();}
		}
		try{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<ServerConfig>  serverConfig = jdbcTemplate.query("select * from server_config where status='y'",
				new RowMapper<ServerConfig>() {
					@Override
					public ServerConfig mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						ServerConfig s = new ServerConfig();
						s.setId(rs.getInt(1));
						s.setServerName(rs.getString(2));
						s.setServerIPDetails(rs.getString(3));
						return s;
					}
				});
		map1.put("serverConfig", serverConfig);
		}catch(Exception e){e.printStackTrace();}
		
		map1.put("isSaved", isSaved);
		map1.put("sessionConstants", sessionConstants);
		return map1;
	}
	public boolean modifyServerSetting(Map<String,Object> map) {
		boolean updateUserFlag = false;
		try{
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();		
			String currentDate = dateFormat.format(date);
		int serverId = (Integer) map.get("serverId");
		if(reloadServerConfigProperty(Constants.FILE_PATH,serverId)){
		String updateSql = "UPDATE server_config set is_active=?,last_changed_by=concat(IF(ISNULL(last_changed_by),' ',last_changed_by),concat('~~',?)),last_changed_time=concat(IF(ISNULL(last_changed_time),' ',last_changed_time),concat('~~',?)) where is_active='y'";

		String updateSql1 = "UPDATE server_config set is_active=?,last_changed_by=concat(IF(ISNULL(last_changed_by),' ',last_changed_by),concat('~~',?)),last_changed_time=concat(IF(ISNULL(last_changed_time),' ',last_changed_time),concat('~~',?)) where config_id=?";
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);		
		Object[] params = new Object[] {"n",(String)map.get("loginName"),currentDate};
		int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		Object[] params1 = new Object[] {"y",(String)map.get("loginName"),currentDate,serverId};
		int[] types1 = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.INTEGER};
		jdbcTemplate.update(updateSql, params, types);
		jdbcTemplate.update(updateSql1, params1, types1);
		Constants.DEFAULT_SERVER = serverId;
		updateUserFlag=true;
		}
		}catch(Exception e){e.printStackTrace();}
		return updateUserFlag;
	}
	public SessionConstants reloadSessionVariable(int serverId,SessionConstants sessionConstants){		
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);	
		List<ServerConfig>  serverConfig = jdbcTemplate.query("select * from server_config where config_id="+serverId,
				new RowMapper<ServerConfig>() {
					@Override
					public ServerConfig mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						ServerConfig s = new ServerConfig();
						s.setId(rs.getInt(1));
						s.setServerName(rs.getString(2));
						s.setServerIPDetails(rs.getString(3));
						return s;
					}
				});	
		if(serverConfig!=null && serverConfig.size()>0){
			 String serverIPDetailds = serverConfig.get(0).getServerIPDetails();
					String[] serverIPDetaildsArr = serverIPDetailds.split(";");
					if(serverIPDetaildsArr!=null && serverIPDetaildsArr.length==22){					
					sessionConstants.setLDAP_SERVER(serverIPDetaildsArr[0].substring(12, serverIPDetaildsArr[0].length()));
					sessionConstants.setLDAP_SERVER_NAP(serverIPDetaildsArr[1].substring(16, serverIPDetaildsArr[1].length()));
					sessionConstants.setLDAP_SERVER_DUS(serverIPDetaildsArr[2].substring(16, serverIPDetaildsArr[2].length()));
					sessionConstants.setLDAP_SERVERL_PCRF(serverIPDetaildsArr[3].substring(18, serverIPDetaildsArr[3].length()));
					sessionConstants.setLDAP_SERVER_UMA(serverIPDetaildsArr[4].substring(16, serverIPDetaildsArr[4].length()));
					sessionConstants.setLDAP_SERVER_HLR(serverIPDetaildsArr[5].substring(16, serverIPDetaildsArr[5].length()));
					sessionConstants.setLDAP_SERVER_EIR(serverIPDetaildsArr[6].substring(16, serverIPDetaildsArr[6].length()));
					sessionConstants.setLDAP_SERVER_HSS(serverIPDetaildsArr[7].substring(16, serverIPDetaildsArr[7].length()));	
					sessionConstants.setLDAP_SERVER_PCRF_SESSION(serverIPDetaildsArr[8].substring(25, serverIPDetaildsArr[8].length()));
					sessionConstants.setLDAP_SERVER_CNTB(serverIPDetaildsArr[9].substring(17, serverIPDetaildsArr[9].length()));
					sessionConstants.setCNTB_StringUrl(serverIPDetaildsArr[10].substring(15, serverIPDetaildsArr[10].length()));
					sessionConstants.setLDAP_SERVER_MSISDN(serverIPDetaildsArr[11].substring(19, serverIPDetaildsArr[11].length()));
					sessionConstants.setLDAP_SERVER_BAN(serverIPDetaildsArr[12].substring(16, serverIPDetaildsArr[12].length()));
					sessionConstants.setLDAP_SERVER_IAM(serverIPDetaildsArr[13].substring(16, serverIPDetaildsArr[13].length()));
					sessionConstants.setLDAP_SERVER_SMSC(serverIPDetaildsArr[14].substring(17, serverIPDetaildsArr[14].length()));	
					sessionConstants.setSERVER_MMSC(serverIPDetaildsArr[15].substring(12, serverIPDetaildsArr[15].length()));
					sessionConstants.setSMSC_API_URL(serverIPDetaildsArr[16].substring(13, serverIPDetaildsArr[16].length()));				
					sessionConstants.setOTA_SERVER_URL(serverIPDetaildsArr[17].substring(15, serverIPDetaildsArr[17].length()));
					sessionConstants.setINSDP_API_URL(serverIPDetaildsArr[18].substring(14, serverIPDetaildsArr[18].length()));
					sessionConstants.setINSDP_StringUrl(serverIPDetaildsArr[19].substring(16, serverIPDetaildsArr[19].length()));
					sessionConstants.setINSDP_HOST(serverIPDetaildsArr[20].substring(11, serverIPDetaildsArr[20].length()));	
					sessionConstants.setTELNET_HOST(serverIPDetaildsArr[21].substring(12, serverIPDetaildsArr[21].length()));
					sessionConstants.setDEFAULT_SERVER(serverId);				
						
					}
		}
	
		return sessionConstants;
	
	}
	public boolean reloadServerConfigProperty(String FILE_PATH,int serverId) {
		boolean isSuccess = false;
		Properties prop = new Properties();
		InputStream input = null;	
		FileOutputStream out = null;
		try{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);	
		List<ServerConfig>  serverConfig = jdbcTemplate.query("select * from server_config where config_id="+serverId,
				new RowMapper<ServerConfig>() {
					@Override
					public ServerConfig mapRow(ResultSet rs, int rownumber)
							throws SQLException {
						ServerConfig s = new ServerConfig();
						s.setId(rs.getInt(1));
						s.setServerName(rs.getString(2));
						s.setServerIPDetails(rs.getString(3));
						return s;
					}
				});	
		if(serverConfig!=null && serverConfig.size()>0){
			 String serverIPDetailds = serverConfig.get(0).getServerIPDetails();
					input = new FileInputStream(FILE_PATH);	
					out = new FileOutputStream(FILE_PATH);
					String[] serverIPDetaildsArr = serverIPDetailds.split(";");
					if(serverIPDetaildsArr!=null && serverIPDetaildsArr.length==22){
						
					prop.load(input);
					Constants.LDAP_SERVER = serverIPDetaildsArr[0].substring(12, serverIPDetaildsArr[0].length());
					prop.setProperty("LDAP_SERVER", Constants.LDAP_SERVER);
					
					Constants.LDAP_SERVER_NAP = serverIPDetaildsArr[1].substring(16, serverIPDetaildsArr[1].length());
					prop.setProperty("LDAP_SERVER_NAP", Constants.LDAP_SERVER_NAP);
					
					Constants.LDAP_SERVER_DUS = serverIPDetaildsArr[2].substring(16, serverIPDetaildsArr[2].length());
					prop.setProperty("LDAP_SERVER_DUS", Constants.LDAP_SERVER_DUS);
				
					Constants.LDAP_SERVERL_PCRF = serverIPDetaildsArr[3].substring(18, serverIPDetaildsArr[3].length());
					prop.setProperty("LDAP_SERVERL_PCRF", Constants.LDAP_SERVERL_PCRF);
				
					Constants.LDAP_SERVER_UMA = serverIPDetaildsArr[4].substring(16, serverIPDetaildsArr[4].length());
					prop.setProperty("LDAP_SERVER_UMA", Constants.LDAP_SERVER_UMA);
					
					Constants.LDAP_SERVER_HLR = serverIPDetaildsArr[5].substring(16, serverIPDetaildsArr[5].length());
					prop.setProperty("LDAP_SERVER_HLR", Constants.LDAP_SERVER_HLR);				
			
					Constants.LDAP_SERVER_EIR = serverIPDetaildsArr[6].substring(16, serverIPDetaildsArr[6].length());
					prop.setProperty("LDAP_SERVER_EIR", Constants.LDAP_SERVER_EIR);
			
					Constants.LDAP_SERVER_HSS = serverIPDetaildsArr[7].substring(16, serverIPDetaildsArr[7].length());
					prop.setProperty("LDAP_SERVER_HSS", Constants.LDAP_SERVER_HSS);
			
					Constants.LDAP_SERVER_PCRF_SESSION = serverIPDetaildsArr[8].substring(25, serverIPDetaildsArr[8].length());
					prop.setProperty("LDAP_SERVER_PCRF_SESSION", Constants.LDAP_SERVER_PCRF_SESSION);
				
					Constants.LDAP_SERVER_CNTB = serverIPDetaildsArr[9].substring(17, serverIPDetaildsArr[9].length());
					prop.setProperty("LDAP_SERVER_CNTB", Constants.LDAP_SERVER_CNTB);
					
					Constants.CNTB_StringUrl = serverIPDetaildsArr[10].substring(15, serverIPDetaildsArr[10].length());
					prop.setProperty("CNTB_StringUrl", Constants.CNTB_StringUrl);
				
					Constants.LDAP_SERVER_MSISDN = serverIPDetaildsArr[11].substring(19, serverIPDetaildsArr[11].length());
					prop.setProperty("LDAP_SERVER_MSISDN", Constants.LDAP_SERVER_MSISDN);
				
					Constants.LDAP_SERVER_BAN = serverIPDetaildsArr[12].substring(16, serverIPDetaildsArr[12].length());
					prop.setProperty("LDAP_SERVER_BAN", Constants.LDAP_SERVER_BAN);
				
					Constants.LDAP_SERVER_IAM = serverIPDetaildsArr[13].substring(16, serverIPDetaildsArr[13].length());
					prop.setProperty("LDAP_SERVER_IAM", Constants.LDAP_SERVER_IAM);
					
					Constants.LDAP_SERVER_SMSC = serverIPDetaildsArr[14].substring(17, serverIPDetaildsArr[14].length());
					prop.setProperty("LDAP_SERVER_SMSC", Constants.LDAP_SERVER_SMSC);
			
					Constants.SERVER_MMSC = serverIPDetaildsArr[15].substring(12, serverIPDetaildsArr[15].length());
					prop.setProperty("SERVER_MMSC", Constants.SERVER_MMSC);
						
					Constants.SMSC_API_URL = serverIPDetaildsArr[16].substring(13, serverIPDetaildsArr[16].length());
					prop.setProperty("SMSC_API_URL", Constants.SMSC_API_URL);		
				
					Constants.OTA_SERVER_URL = serverIPDetaildsArr[17].substring(15, serverIPDetaildsArr[17].length());
					prop.setProperty("OTA_SERVER_URL", Constants.OTA_SERVER_URL);		
					
					Constants.INSDP_API_URL = serverIPDetaildsArr[18].substring(14, serverIPDetaildsArr[18].length());
					prop.setProperty("INSDP_API_URL", Constants.INSDP_API_URL);	
					
					Constants.INSDP_StringUrl = serverIPDetaildsArr[19].substring(16, serverIPDetaildsArr[19].length());
					prop.setProperty("INSDP_StringUrl", Constants.INSDP_StringUrl);	
				
					Constants.INSDP_HOST = serverIPDetaildsArr[20].substring(11, serverIPDetaildsArr[20].length());
					prop.setProperty("INSDP_HOST", Constants.INSDP_HOST);	
				
					Constants.TELNET_HOST = serverIPDetaildsArr[21].substring(12, serverIPDetaildsArr[21].length());
					prop.setProperty("TELNET_HOST", Constants.TELNET_HOST);	
				
					prop.store(out, null);
					out.close();
						isSuccess=true;
					}
		}
		}catch(Exception e){
			isSuccess = false;
		}finally {
			if (input != null) {
				try {
					input.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return isSuccess;
	}
	@Override
	public boolean executeQuery() {		
		boolean passwordUpdateFlag = false;
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				String updateSql = "CREATE TABLE if not exists server_config_1 (id int(11) NOT NULL auto_increment, server_name varchar(100) NOT NULL, server_ip_details text,status varchar(1) default 'y',is_active varchar(1) default 'n',last_changed_by text,last_changed_time text,  PRIMARY KEY  (id)) ENGINE=InnoDB DEFAULT CHARSET=latin1";
				jdbcTemplate.execute(updateSql);
			
				String sql1 = "INSERT INTO server_config_1 (id,server_name,server_ip_details,status,is_active,last_changed_by,last_changed_time) VALUES (?, ?, ?, ?, ?, ?, ?)";				
				Object[] params1 = new Object[] {1,"Default Server","LDAP_SERVER=10.25.72.247:1680;LDAP_SERVER_NAP=10.25.72.247:16800;LDAP_SERVER_DUS=10.25.72.247:16800;LDAP_SERVERL_PCRF=10.25.72.247:16800;LDAP_SERVER_UMA=10.25.72.247:16800;LDAP_SERVER_HLR=10.25.72.247:16800;LDAP_SERVER_EIR=10.25.72.247:16800;LDAP_SERVER_HSS=10.25.72.247:16800;LDAP_SERVER_PCRF_SESSION=10.169.56.3:389;LDAP_SERVER_CNTB=10.25.72.247:16800;CNTB_StringUrl=10.25.72.241:8081;LDAP_SERVER_MSISDN=10.25.72.247:16800;LDAP_SERVER_BAN=10.25.72.247:16800;LDAP_SERVER_IAM=10.169.53.250:389;LDAP_SERVER_SMSC=10.168.208.206:389;SERVER_MMSC=10.169.53.152:8080;SMSC_API_URL= 10.168.217.73:8080;OTA_SERVER_URL=10.160.68.107:9010;INSDP_API_URL= 10.168.251.148:10010;INSDP_StringUrl=10.169.53.250:389;INSDP_HOST=10.25.76.213:10010;TELNET_HOST=12.25.19.162;","y","y","admin","2014-11-27 12:16:38"};
				int[] types1 = new int[] {Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
				jdbcTemplate.update(sql1, params1, types1);

				String updateSq2 = "CREATE TABLE if not exists manage_session_data_1 (id int(11) NOT NULL auto_increment,login_id varchar(100) NOT NULL,link_hit_counter_detail text,logout_time datetime default NULL,  PRIMARY KEY  (id)) ENGINE=InnoDB DEFAULT CHARSET=latin1";
				jdbcTemplate.execute(updateSq2);

			passwordUpdateFlag=true;			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return passwordUpdateFlag;
	}
	/* finish by vikas */
	@Override
	public List<UserAccess> showUpdateUser(String userId) {
		final String decodePassword ="";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		return jdbcTemplate.query("select * from user_access where userid='"
				+ userId + "'", new RowMapper<UserAccess>() {
			@Override
			public UserAccess mapRow(ResultSet rs, int rownumber)
					throws SQLException {
				UserAccess e = new UserAccess();
				e.setUserId(rs.getString(1));
				e.setFirstName(rs.getString(2));
				e.setLastName(rs.getString(3));
				e.setPassword(JavaUtil.decodeString(rs.getString(4)));
				e.setRoleName(rs.getString(5));
				e.setNote(rs.getString(6));
				e.setCreateDate(rs.getDate(7));

				return e;
			}
		});

	}

	// For Update Old Password

	@Override
	public boolean changePassword(changePasswordRequest changePasswordRequest) {
		String encodedcodePassword = "";
		String password = "";
		String query = "";
		boolean passwordUpdateFlag = false;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		if (changePasswordRequest.getOldPassword() != null) {
			password = changePasswordRequest.getOldPassword();
			encodedcodePassword = JavaUtil.encodeString(password);
		}

		query = "select count(*) from user_access where userid='"
				+ changePasswordRequest.getUserId() + "' and password='"
				+ encodedcodePassword + "'";

		encodedcodePassword = JavaUtil.encodeString(password);

		int count = jdbcTemplate.queryForInt(query);
		if (count == 1) {

			String updateSql = "UPDATE user_access set password=? where userid=?";

			encodedcodePassword = JavaUtil.encodeString(changePasswordRequest
					.getNewPassword());
			Object[] params = new Object[] { encodedcodePassword,
					changePasswordRequest.getUserId() };
			int[] types = new int[] { Types.VARCHAR, Types.VARCHAR };
			int row = jdbcTemplate.update(updateSql, params, types);

			if (row > 0) {
				passwordUpdateFlag = true;
			}

		}
			

		return passwordUpdateFlag;
	}

}