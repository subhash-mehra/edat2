package com.tmobile.edat.admin.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tmobile.edat.admin.component.AdminDataComponent;
import com.tmobile.edat.admin.dto.request.LinkDataRequest;
import com.tmobile.edat.admin.dto.request.UserRequest;
import com.tmobile.edat.admin.dto.request.UserRole;
import com.tmobile.edat.admin.dto.request.changePasswordRequest;
import com.tmobile.edat.admin.model.ServerConfig;
import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.filter.store.LinkData;
import com.tmobile.session.SessionConstants;

@Controller
@RequestMapping("/")

/** Description of LoginController 
*
* AdminController class is used for Admin tool(create/update/remove user). And assign read/write permission to user.
* 
* 
*/
public class AdminController {

	@Autowired
	@Qualifier("AdminDataComponent")
	AdminDataComponent adminDataComponent = null;

	private String contentJsp = "";
	
	
	/**
	*
	* showAdminTemplate() method is used for show Admin Template main page.
	* This page includes Register New User,Show User,Report,Configer Server link.
	* 
	*/

	@RequestMapping(value = "showAdminTemplate", method = RequestMethod.GET)
	public ModelAndView showAdminTemplate() {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		List<UserRole> roleList = adminDataComponent.getUserRoleList();
		dataMap.put("roleList", roleList);
		dataMap.put("show", "true");
		return new ModelAndView("/adminJsp/adminTemplate", "dataMap", dataMap);
	}

	

	/** 
	*
	* showCreateUser() method is used for register new user.
	* 
	* 
	*/
	@RequestMapping(value = "showUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView showCreateUser() {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		boolean showFlag = true;
		List<UserRole> roleList = adminDataComponent.getUserRoleList();
		contentJsp = "user.jsp";
		dataMap.put("roleList", roleList);
		dataMap.put("showFlag", showFlag);
		dataMap.put("contentJsp", contentJsp);

		return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
	}

	
	/** 
	*
	* saveNewUser(@ModelAttribute UserRequest userRequest) method is used for save new user.
	*  UserRequest parameter is used for takes input from user.
	* 
	*/
	@ResponseBody
	@RequestMapping(value = "saveUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView saveNewUser(@ModelAttribute UserRequest userRequest) {
		boolean saveFlag = false;
		boolean existingUserFlag = false;
		List<UserRole> roleList = null;
		Map<String, Object> dataMap = new HashMap<String, Object>();
		existingUserFlag = adminDataComponent.checkExistingUser(userRequest);
		
		contentJsp = "/views/adminJsp/user.jsp";

		if (!existingUserFlag) {
			saveFlag = adminDataComponent.saveNewUser(userRequest);
			roleList = adminDataComponent.getUserRoleList();
			dataMap.put("roleList", roleList);
			dataMap.put("saveFlag", saveFlag);
			dataMap.put("contentJsp", contentJsp);
			return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);

		} else {
			roleList = adminDataComponent.getUserRoleList();
			dataMap.put("roleList", roleList);
			dataMap.put("existingUserFlag", existingUserFlag);
			dataMap.put("contentJsp", contentJsp);
			return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
		}

	}

	

	/** 
	*
	* showUpdateUser() method is used for show users list.
	* 
	* 
	*/
	@RequestMapping(value = "showUpdateUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView showUpdateUser() {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		List<UserAccess> usersList = adminDataComponent.getUsersList();
		contentJsp = "/views/adminJsp/userGrid.jsp";
		dataMap.put("usersList", usersList);
		dataMap.put("contentJsp", contentJsp);
		return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
	}

	
	/** Description of LoginController 
	*
	* updateExistUser(HttpServletRequest request,HttpServletResponse response) method is used for show users list for update.
	
	* 
	*/
	@RequestMapping(value = "updateExistUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView updateExistUser(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		List<UserRole> roleList = new ArrayList<UserRole>();
		UserAccess userAccess = null;
		String userId = "";
		boolean showFlag = true;
		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		List<UserAccess> usersList = adminDataComponent.showUpdateUser(userId);
		roleList = adminDataComponent.getUserRoleList();
		if (usersList.size() > 0) {
			userAccess = usersList.get(0);
		}
		contentJsp = "/views/adminJsp/updateUser.jsp";
		dataMap.put("userAccess", userAccess);
		dataMap.put("roleList", roleList);
		dataMap.put("contentJsp", contentJsp);
		dataMap.put("showFlag", showFlag);
		return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
	}

	
	/** 
	*
	* updateUser() method is used for update existing users.
	*  UserRequest parameter is used for takes input from user. 
	* 
	*/
	@ResponseBody
	@RequestMapping(value = "updateUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView updateUser(@ModelAttribute UserRequest userRequest) {
		boolean updateUserFlag = false;
		updateUserFlag = adminDataComponent.updateUser(userRequest);
		String message = "";
		Map<String, Object> dataMap = new HashMap<String, Object>();
		List<UserAccess> usersList = adminDataComponent.getUsersList();
		contentJsp = "/views/adminJsp/userGrid.jsp";
		if (updateUserFlag) {
			message = "User updated Successfully!!";
		}
		dataMap.put("usersList", usersList);
		dataMap.put("contentJsp", contentJsp);
		dataMap.put("message", message);
		return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
	}

	
	/** 
	*
	* deleteUser() method is used for delete existing users.
	* 
	* 
	*/
	@ResponseBody
	@RequestMapping(value = "deleteUser", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView deleteUser(HttpServletRequest request,
			HttpServletResponse response) {
		boolean deleteUserFlag = false;
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String userId = "";
		String message = "";
		if (request.getParameter("userId") != null) {
			userId = request.getParameter("userId");
		}

		deleteUserFlag = adminDataComponent.deleteUser(userId);
		List<UserAccess> usersList = adminDataComponent.getUsersList();
		contentJsp = "/views/adminJsp/userGrid.jsp";
		if (deleteUserFlag) {
			message = "User has been deleted Successfully!!";
		}

		dataMap.put("usersList", usersList);
		dataMap.put("contentJsp", contentJsp);
		dataMap.put("message", message);
		return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
	}

	
	/** 
	*
	* showPasswordPopup() method is used for show change password of user.
	* 
	* 
	*/
	@RequestMapping(value = "showPasswordPopup", method = RequestMethod.GET)
	public ModelAndView showPasswordPopup() {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		return new ModelAndView("adminJsp/changePassword", "dataMap", dataMap);
	}
	
	
	
		
	/** 
	*
	* changePassword(@ModelAttribute changePasswordRequest changePasswordRequest,HttpSession session) method is used for update password of user.
	*  changePasswordRequest parameter is used for takes input from user.
	 	*  session parameter is used for get current session of user.
	*/
				@RequestMapping(value = "changePassword", method = RequestMethod.POST)
				public ModelAndView changePassword(@ModelAttribute changePasswordRequest changePasswordRequest,HttpSession session) {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					String message="";
					if(session.getAttribute("userAccess")!=null)
					{
						UserAccess userAccess=(UserAccess)session.getAttribute("userAccess");
						changePasswordRequest.setUserId(""+userAccess.getUserId());
					}
					if(changePasswordRequest.getNewPassword().equalsIgnoreCase(changePasswordRequest.getConfirmNewPassword()))
					{
					boolean passwordUpdateFlag=adminDataComponent.changePassword(changePasswordRequest);
					if(passwordUpdateFlag)
					{
						message="Password has been updated successfully!!";
					}else
					{
						message="You have enter wrong old password!!";
					}
					}else
					{
						message="You have enter wrong old password!!";
					}
					
					dataMap.put("message", message);
					return new ModelAndView("adminJsp/changePassword", "dataMap", dataMap);
				}

				
				/** 
				*
				* showReports() method is used for show user reports.
				* 
				* 
				*/
				@RequestMapping(value = "showReports", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
				public ModelAndView showReports() {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					List<Integer>yearList=new ArrayList<Integer>();
					Calendar cal=Calendar.getInstance();
					int year=cal.get(Calendar.YEAR);
					int month=cal.get(Calendar.MONTH);
					yearList.add(year);
					yearList.add(year-1);
					  
					contentJsp = "/views/adminJsp/report.jsp";
					dataMap.put("yearList", yearList);
					dataMap.put("contentJsp", contentJsp);
					return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
				}
				
				/** 
				*
				* showUserWiseReports() method is used for show UserWise Reports.
				* 
				* 
				*/
				@RequestMapping(value = "showUserWiseReports",method = RequestMethod.POST)
				
				
				public ModelAndView showUserWiseReports() {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					List<LinkData> linkData = adminDataComponent.showReports();
					String jsp = "adminJsp/user_wise_report";
					dataMap.put("linkData", linkData);				
					return new ModelAndView(jsp, "dataMap", dataMap);
				}
				/** 
				*
				* showDayWiseReports() method is used for show DayWise Reports.
				* 
				* 
				*/
				@RequestMapping(value = "showDayWiseReports",method = RequestMethod.POST,
						produces=MediaType.APPLICATION_JSON_VALUE,
						consumes=MediaType.APPLICATION_JSON_VALUE)
				public ModelAndView showDayWiseReports(@RequestBody LinkDataRequest linkDataRequest) {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					String data = adminDataComponent.showDayWiseReports(linkDataRequest);
					String jsp = "adminJsp/day_wise_report";
					dataMap.put("data", data);				
					return new ModelAndView(jsp, "dataMap", dataMap);
				}
				@RequestMapping(value = "executeQuery",method = RequestMethod.POST)
				public ModelAndView executeQuery() {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					if(adminDataComponent.executeQuery()){
						dataMap.put("message", "Success !!");	
					}else{
						dataMap.put("message", "Try Again");
					}
					contentJsp = "/views/adminJsp/server_config.jsp";
					dataMap.put("contentJsp", contentJsp);
					return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
				}
				
				/** 
				*
				* changeServerConfig() method is used for change server setting.
				* 
				* 
				*/
				@RequestMapping(value = "changeServerConfig",  method = RequestMethod.POST)
				public ModelAndView changeServerConfig(HttpServletRequest req,HttpSession session) {
					Map<String, Object> dataMap = new HashMap<String, Object>();
					Map<String, Object> map = new HashMap<String, Object>();
					String flag =req.getParameter("saveData");
					String callFrom=req.getParameter("callFrom");
					String message="";
					SessionConstants sessionConstants = (SessionConstants)session.getAttribute("SessionConstants");
					try{
					if(session!=null && session.getAttribute("userAccess")!=null){
						map.put("loginName", ((UserAccess)session.getAttribute("userAccess")).getUserId());
					}
					if(flag!=null && flag.equals("1")){
					map.put("serverId", Integer.parseInt(req.getParameter("server_id")));
					}
					map.put("sessionConstants", sessionConstants);
					map.put("saveData", flag);
					map  = adminDataComponent.changeServerConfig(map);
					if(flag!=null && flag.equals("1")){
						if(map.get("isSaved")!=null && !map.get("isSaved").equals("") && (Boolean)map.get("isSaved")){
							sessionConstants = (SessionConstants)map.get("sessionConstants");
							session.setAttribute("sessionConstants", sessionConstants);
							message = "Server Configuration Changed Successfully !!";							
						}else{
							message = "Oops!! Something went wrong. !! kindly Try Again !!";
						}
					}
					}catch(Exception e){
						e.printStackTrace();
					}
					contentJsp = "/views/adminJsp/server_config.jsp";
					dataMap.put("contentJsp", contentJsp);
					dataMap.put("message", message);
					dataMap.put("serverConfig", (List<ServerConfig>)map.get("serverConfig"));
					if(callFrom!=null && !callFrom.equals("") && callFrom.equalsIgnoreCase("ajax")){
						String jsp = "adminJsp/message";
						return new ModelAndView(jsp, "dataMap", dataMap);
					}else{
					return new ModelAndView("adminJsp/adminTemplate", "dataMap", dataMap);
					}
				}		
			
}
