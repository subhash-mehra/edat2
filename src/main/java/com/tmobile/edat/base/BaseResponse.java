package com.tmobile.edat.base;

/* 
 *  This is a base class for all response objects.
 * */
public class BaseResponse {	
	
	private String statusCode;
	
	private ApplicationError applicationError;

	public BaseResponse() {
		super();
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public ApplicationError getApplicationError() {
		return applicationError;
	}

	public void setApplicationError(ApplicationError applicationError) {
		this.applicationError = applicationError;
	}
	
}
