package com.tmobile.edat.base;

public interface BaseComponent {
	
	/* This method will force a Component to validate request object before starting to process for.
	 * Using @key parameter, validate method would perform a method specific validation logic of a Component 
	 */
	public boolean validateRequest(BaseRequest baseRequest, BaseResponse baseResponse, int key);
	
	
}
