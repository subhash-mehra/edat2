package com.tmobile.edat.base;

public interface ApplicationConstants {
	
	String SUCCESS_CODE =  "1";
	String FAILURE_CODE =  "5";
	
	// Error Codes
	String NULL_REQUEST = "101";
	String INVALID_INPUT_IN_REQUEST = "102";	
	
}
