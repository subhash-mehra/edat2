package com.tmobile.edat.cntb.repository;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.LdapName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.cntb.request.CNTBDBSearchRequest;
import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

@Repository("CNTBDBDataRepository")
public class CNTBDBDataRepositoryImpl implements CNTBDBDataRepository {

	private static final Logger logger = Logger.getLogger(CNTBDBDataRepositoryImpl.class);

	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";
	private static String SECURITY_PRINCIPAL_CNTB_UID = "cn=napprovuser";
	private static String SECURITY_PRINCIPAL_CNTB = "cn=napProvUser";
	private static String SECURITY_CREDENTIALS_CNTB = "siemens";
	private String PROVIDER_URL_CNTB_MISSDN;
	private String PROVIDER_URL_CNTB_UID ;

	private String PROVIDER_URL_CNTB_IMSI;

	String SEARCH_QUERY_DEFAULT = "%s=%s";
	String SEARCH_FILTER_DEFAULT = "(objectclass=%s)";
	String SEARCH_QUERY_UID = "uid=%s,ds=SUBSCRIBER,o=DEFAULT";

	@Override
	public CNTBDBDataResponse doMSISDNCSearch(String msisdn, SessionConstants sessionConstants) {

		String providerURL = "";
		String uid = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List < String > attributeList = new ArrayList < String > ();
		CNTBDBDataResponse cntbdbDataMsisdnResponse = new CNTBDBDataResponse();

		if (msisdn != null && !msisdn.equals("")) {
			PROVIDER_URL_CNTB_MISSDN	= "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants) + "/dc=msisdn,dc=c-ntdb";
			providerURL = String.format(PROVIDER_URL_CNTB_MISSDN, "msisdn");
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", msisdn);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
				LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
				SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
				providerURL));
			} catch (UserException e1) {
				cntbdbDataMsisdnResponse.setApplicationError(new ApplicationError(e1.getMessage()));
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {

					// System.out.println("NAP search query :" + query +
					// "\nfilter :"+ filter);

					NamingEnumeration < SearchResult > result = ctx.search(query,
					filter, sc);

					while (result != null && result.hasMoreElements()) {

						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());

						Attributes attrs = rs.getAttributes();

						Attribute attribute = attrs.get("objectClass");

						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}

					attributeList = getAttributeListForMSISDAndIMSI(uid,sessionConstants);
					attributeList.add(uid);

				} catch (NamingException e) {

					cntbdbDataMsisdnResponse.setApplicationError(new ApplicationError(e.getMessage()));
					logger.debug(" In doMSISDNCSearch NamingException :::"+e.getMessage());
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						logger.debug("NamingException  In doMSISDNCSearch:::"+e.getMessage());
					}
				}
			}
		}
		cntbdbDataMsisdnResponse.setMISSDNList(attributeList);
		return cntbdbDataMsisdnResponse;

	}

	@Override
	public CNTBDBDataResponse doIMSICSearch(String imsi, SessionConstants sessionConstants) {
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";
		CNTBDBDataResponse cntbdbDataImsiResponse = new CNTBDBDataResponse();
		List < String > attributeList = new ArrayList < String > ();

		if (imsi != null && !imsi.equals("")) // for imsi
		{
			PROVIDER_URL_CNTB_IMSI = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants)+ "/dc=imsi,dc=c-ntdb";
			providerURL = String.format(PROVIDER_URL_CNTB_IMSI, "imsi");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "imsi", imsi);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
				LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
				SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
				providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {
					NamingEnumeration < SearchResult > result = ctx.search(query,
					filter, sc);

					while (result != null && result.hasMoreElements()) {
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						Attributes attrs = rs.getAttributes();
						Attribute attribute = attrs.get("objectClass");
						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}
					attributeList = getAttributeListForMSISDAndIMSI(uid,sessionConstants);
					attributeList.add(uid);
				} catch (NamingException e) {
					cntbdbDataImsiResponse.setApplicationError(new ApplicationError(e.getMessage()));
					logger.debug("In doIMSICSearch NamingException :::"+e.getMessage());
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						//e.printStackTrace();
						logger.debug(" NamingException  In doIMSICSearch:::"+e.getMessage());
					}
				}
			}
		}
		cntbdbDataImsiResponse.setIMSIList(attributeList);
		return cntbdbDataImsiResponse;

	}

	public List < String > getAttributeListForMSISDAndIMSI(String uid, SessionConstants sessionConstants) {

		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List < String > attributeList = new ArrayList < String > ();
		if (uid != null && !uid.equals("")) {
			PROVIDER_URL_CNTB_UID	 = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants) + "/dc=C-NTDB";
			providerURL = PROVIDER_URL_CNTB_UID;

			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_UID, uid);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
		}

		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
			LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
			SECURITY_PRINCIPAL_CNTB_UID, SECURITY_CREDENTIALS_CNTB,
			providerURL));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (ctx != null) {
			try {

				NamingEnumeration < SearchResult > result = ctx.search(query,
				filter, sc);

				while (result != null && result.hasMoreElements()) {

					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());

					Attributes attrs = rs.getAttributes();
					System.out.println("======="+attrs);

					Attribute attribute = attrs.get("objectClass");

					attributeList = CNTBDBDataRepositoryHelper.fillUIDForCNTBDB(dn, attrs, attributeList,
					attribute);

				}
			} catch (Exception e) {
			//	e.printStackTrace();
				logger.debug("In getAttributeListForMSISDAndIMSI NamingException :::"+e.getMessage());
			}
		}

		System.out.println(attributeList.size());
		for (int i = 0; i < attributeList.size(); i++) {
			System.out.println("---------------" + attributeList.get(i));
		}

		return attributeList;

	}


	@Override
	public CNTBDBDataResponse cntbSave(CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String encKey = "";
		String accId = "";
		String alogId = "";
		String kdbId = "";
		String uid = "";
		String custId = "";
		String message = "";
		String addnew = "";
		String addnewPrefix="";
		String addnewPostfix="";
		String uma="";
		String acsub="";
		String privateUserId="";
	

		if (cntbdbSearchRequest.getMsisdn() != null && cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			uid = getUIDFromMSISDN(msisdn,cntbdbSearchRequest.getSessionConstants());
			
		}

		if (cntbdbSearchRequest.getImsi() != null && cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			uid = getUIDFromIMSI(imsi,cntbdbSearchRequest.getSessionConstants());
			privateUserId=imsi+"@msg.lab.t-mobile.com";
		}
		if (cntbdbSearchRequest.getEncKey() != null && cntbdbSearchRequest.getEncKey() != "") {
			encKey = cntbdbSearchRequest.getEncKey();
		}
		if (cntbdbSearchRequest.getAccId() != null && cntbdbSearchRequest.getAccId() != "") {
			accId = cntbdbSearchRequest.getAccId();
		}
		if (cntbdbSearchRequest.getAlogId() != null && cntbdbSearchRequest.getAlogId() != "") {
			alogId = cntbdbSearchRequest.getAlogId();
		}
		if (cntbdbSearchRequest.getKdbId() != null && cntbdbSearchRequest.getKdbId() != "") {
			kdbId = cntbdbSearchRequest.getKdbId();
		}

		if (cntbdbSearchRequest.getCustId() != null && cntbdbSearchRequest.getCustId() != "") {
			custId = cntbdbSearchRequest.getCustId();
		}
		
		if (cntbdbSearchRequest.getAcsub() != null && cntbdbSearchRequest.getAcsub() != "") {
			acsub = cntbdbSearchRequest.getAcsub();
		}
		//if (uid == "") {

		
		addnewPrefix="<soapenv:Envelope  " + 
		 " " + 
		 "               xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"  " + 
		 " " + 
		 "               xmlns:urn=\"urn:siemens:names:prov:gw:SPML:2:0\"> " + 
		 " " + 
		 "<soapenv:Body> " + 
		 " " + 
		 "<spml:addRequest  " + 
		 " " + 
		 "               xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 pro2v-gw-spml-2.0.xsd\" " + 
		 " " + 
		 "               xmlns:sub=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " + 
		 " " + 
		 "               xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" " + 
		 " " + 
		 "               xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" returnResultingObject=\"full\"> " + 
		 " " + 
		 "               <version>SUBSCRIBER_v20</version> " + 
		 " " + 
		 "               <object xsi:type=\"sub:Subscriber\"> " + 
		 " " + 
		 "               <identifier></identifier> " + 
		 " " + 
		 "               <auc> " + 
		 " " + 
		 "                              <imsi>"+imsi+"</imsi> " + 
		 " " + 
		 "                              <encKey>"+encKey+"</encKey> " + 
		 " " + 
		 "                              <iccId>"+accId+"</iccId> " + 
		 " " + 
		 "                              <algoId>"+alogId+"</algoId> " + 
		 " " + 
		 "                              <kdbId>"+kdbId+"</kdbId> " + 
		 " " + 
		 "                              <acsub>"+acsub+"</acsub> " + 
		 " " + 
		 "                              <amf>0000</amf> " + 
		 " " + 
		 "                              <sqn>000000000000</sqn> " + 
		 " " + 
		 "               </auc> " + 
		 " " + 
		 "      <hlr> " + 
		 " " + 
		 "        <ntype>single</ntype> " + 
		 " " + 
		 "        <osb1>true</osb1> " + 
		 " " + 
		 "        <rr>VOICESTR</rr> " + 
		 " " + 
		 "        <ts11> " + 
		 " " + 
		 "        <msisdn>"+msisdn+"</msisdn> " + 
		 " " + 
		 "        </ts11> " + 
		 " " + 
		 "        <gprs> " + 
		 " " + 
		 "        <msisdn>"+msisdn+"</msisdn> " + 
		 " " + 
		 "        </gprs> " + 
		 " " + 
		 "      </hlr> " +
		 " " + 
		 "<hss> " + 
		 " " + 
		 "            <subscriptionId>1</subscriptionId> " + 
		 " " + 
		 "                <profileType>normal</profileType> " + 
		 " " + 
		 "                <adminBlocked>false</adminBlocked> " + 
		 " " + 
		 "                <defaultScscfRequired>true</defaultScscfRequired> " + 
		 " " + 
		 "                <maximumPublicIds>10</maximumPublicIds> " + 
		 " " + 
		 "                <privateUserId> " + 
		 " " + 
		 "                        <privateUserId>"+imsi+"@msg.lab.t-mobile.com</privateUserId> " + 
		 " " + 
		 "                        <provisionedImsi> " + 
		 " " + 
		 "                        <provisionedImsi>"+imsi+"</provisionedImsi> " + 
		 " " + 
		 "                        </provisionedImsi> " + 
		 " " + 
		 "                        <msisdn>"+msisdn+"</msisdn> " + 
		 " " + 
		 "                        <preferredAuthenticationScheme>aka</preferredAuthenticationScheme> " + 
		 " " + 
		 "                        <preferredDomain>none</preferredDomain> " + 
		 " " + 
		 "                </privateUserId> " + 
		 " " + 
		 "                              <implicitRegisteredSet> " + 
		 " " + 
		 "                  <irsId>"+msisdn+"</irsId> " + 
		 " " + 
		 "                </implicitRegisteredSet> " + 
		 " " + 
		 "                <publicUserId> " + 
		 " " + 
		 "                        <originalPublicUserId>sip:"+msisdn+"@msg.lab.t-mobile.com</originalPublicUserId> " + 
		 " " + 
		 "                        <defaultIndication>true</defaultIndication> " + 
		 " " + 
		 "                        <serviceProfileName>SP01</serviceProfileName> " + 
		 " " + 
		 "                        <irsId>"+msisdn+"</irsId> " + 
		 " " + 
		 "                </publicUserId> " + 
		 " " + 
		 "                <serviceProfile> " + 
		 " " + 
		 "                        <profileName>SP01</profileName> " + 
		 " " + 
		 "                        <mandatoryCapability> " + 
		 " " + 
		 "                                <mandatoryCapability>3</mandatoryCapability> " + 
		 " " + 
		 "                        </mandatoryCapability> " + 
		 " " + 
		 "                        <mandatoryCapability> " + 
		 " " + 
		 "                                <mandatoryCapability>4</mandatoryCapability> " + 
		 " " + 
		 "                        </mandatoryCapability> " + 
		 " " + 
		 "                        <globalFilterId> " + 
		 " " + 
		 "                                <globalFilterId>RLS-BASIC</globalFilterId> " + 
		 " " + 
		 "                        </globalFilterId> " + 
		 " " + 
		 "                        <globalFilterId> " + 
		 " " + 
		 "                                <globalFilterId>PS-BASIC</globalFilterId> " + 
		 " " + 
		 "                        </globalFilterId> " + 
		 " " + 
		 "                        <subscribedMediaProfileID> " + 
		 " " + 
		 "                                <sessionReleasePolicy>deregisterForcedSessionRelease</sessionReleasePolicy> " + 
		 " " + 
		 "                                <forkingPolicy>noForking</forkingPolicy> " + 
		 " " + 
		 "                        </subscribedMediaProfileID> " + 
		 " " + 
		 "                </serviceProfile> " + 
		 " " + 
		 "        </hss> " + 
		 " " + 
		 "    <eir> " + 
		 " " + 
		 "        <fastBlackList>false</fastBlackList> " + 
		 " " + 
		 "        <lockedImsiImei>false</lockedImsiImei> " + 
		 " " + 
		 "        <dualImsi>false</dualImsi> " + 
		 " " + 
		 "    </eir> ";
		  
			if(custId!="")
			{
		 uma="     <uma> " + 
		 " " + 
		 "         <imsi>"+imsi+"</imsi> " + 
		 " " + 
		 "         <msisdn>"+msisdn+"</msisdn> " + 
		 " " + 
		 "         <serviceAllowed>false</serviceAllowed> " + 
		 " " + 
		 "         <roamingAllowed>false</roamingAllowed> " + 
		 " " + 
		 "         <serviceType>serviceTypeId=0,OU=SERVICE,OU=UMA,NE=MOBILE_DATA_SERVER</serviceType> " + 
		 " " + 
		 "         <name>"+custId+"</name> " + 
		 " " + 
		 "      </uma> " ;
			}
		  
		addnewPostfix= "    </object> " + 
		 " " + 
		 "</spml:addRequest> " + 
		 " " + 
		 "   </soapenv:Body> " + 
		 " " + 
		 "</soapenv:Envelope> ";
	
			addnew = addnewPrefix + uma + addnewPostfix;
			
			System.out.println("request xml-----------"+addnew);



			try {
				String stringUrl = "http://" + SessionConstantsFactory.getSessionConstant("CNTB_StringUrl", cntbdbSearchRequest.getSessionConstants()) + "/ProvisioningGateway/services/SPMLSubscriber20Service";
				URL url = new URL(stringUrl);
				URLConnection uc = url.openConnection();

				uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
				uc.addRequestProperty("Connection", " keep-alive");
				uc.addRequestProperty("charset", "utf-8");
				//uc.addRequestProperty("User-Agent", "curl/7.12.1 (x86_64-redhat-linux-gnu) libcurl/7.12.1 OpenSSL/0.9.7a zlib/1.2.1.2 libidn/0.5.6");
				uc.addRequestProperty("Content-Type", "text/xml");
				uc.addRequestProperty("Host", SessionConstantsFactory.getSessionConstant("CNTB_StringUrl", cntbdbSearchRequest.getSessionConstants()));
				uc.addRequestProperty("SOAPAction",
					"urn:siemens:names:prov:gw:SPML:2:0/addRequest");
				uc.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
				Map < String, Object > dataMap = new HashMap < String, Object > ();
				String errorCode = "";
				String errorType = "";
				wr.writeBytes(addnew);

				wr.flush();
				wr.close();
				BufferedReader in = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
				String decodedString;
				StringBuffer sb = new StringBuffer();
				while ((decodedString = in .readLine()) != null) {
					sb.append(decodedString);
				}

				System.out.println("response xml-----------"+sb.toString());
				dataMap = (Map<String, Object>)parseXmltoJava(sb.toString());

				try {
					if (dataMap.get("errorType") != null) {
						errorType = (String) dataMap.get("errorType");
					}

					if (dataMap.get("errorCode") != null) {
						errorCode = (String) dataMap.get("errorCode");
					}

					if (dataMap.get("message") != null) {
						message = (String) dataMap.get("message");
					}


					if (errorType.equalsIgnoreCase("error")) {
						cntbdbDataResponse.setMessage(message + "  Error Code--" + errorCode);
					} else {
						message = "record saved successfully.";
						cntbdbDataResponse.setMessage(message);
					}

				} catch (Exception e) {

					cntbdbDataResponse.setMessage("some problem occured please try again.");
				}



				in .close();
			} catch (Exception e) {
				//e.printStackTrace();
				message="Node not reachable !";
				logger.debug("In cntbSave  :::"+e.getMessage());
			}

		//}
		return cntbdbDataResponse;

	}


	public Map < String, Object > parseXmltoJava(String xml) {
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		String errorCode = "";
		String errorType = "";
		String message = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("errorMessage");
			NodeList addResponse = doc.getElementsByTagName("spml:addResponse");
			logger.info("Information of all member" + nodeLst.getLength());
			for (int i = 0; i < nodeLst.getLength(); i++) {
				Node aNode = nodeLst.item(i);
				Node addResponseNode = addResponse.item(i);


				NamedNodeMap attributes = addResponseNode.getAttributes();

				for (int a = 0; a < attributes.getLength(); a++) {
					Node theAttribute = attributes.item(a);
					if (theAttribute.getNodeName() == "errorCode") {
						errorCode = theAttribute.getNodeValue();
					}
					if (theAttribute.getNodeName() == "errorType") {
						errorType = theAttribute.getNodeValue();
						message = aNode.getFirstChild().getNodeValue();

					}

				}

			}

			dataMap.put("errorCode", errorCode);
			dataMap.put("errorType", errorType);
			dataMap.put("message", message);

		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
		//	e.printStackTrace();
		}
		return dataMap;
	}

	public Map < String, String > parseXmltoJavaForDelete(String xml) {
		Map < String, String > dataMap = new HashMap < String, String > ();
		String errorCode = "";
		String errorType = "";
		String message = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("errorMessage");
			NodeList addResponse = doc.getElementsByTagName("spml:deleteResponse");
			logger.info("Information of all member" + nodeLst.getLength());
			for (int i = 0; i < nodeLst.getLength(); i++) {
				Node aNode = nodeLst.item(i);
				Node addResponseNode = addResponse.item(i);


				NamedNodeMap attributes = addResponseNode.getAttributes();

				for (int a = 0; a < attributes.getLength(); a++) {
					Node theAttribute = attributes.item(a);
					if (theAttribute.getNodeName() == "errorCode") {
						errorCode = theAttribute.getNodeValue();
					}
					if (theAttribute.getNodeName() == "errorType") {
						errorType = theAttribute.getNodeValue();
						message = aNode.getFirstChild().getNodeValue();

					}

				}

			}

			dataMap.put("errorCode", errorCode);
			dataMap.put("errorType", errorType);
			dataMap.put("message", message);

		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
			//e.printStackTrace();
		}
		return dataMap;
	}


	public Map < String, String > parseXmltoJavaForBatchDelete(String xml) {
		Map < String, String > dataMap = new HashMap < String, String > ();
		String errorCode = "";
		String errorType = "";
		String message = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("errorMessage");
			NodeList addResponse = doc.getElementsByTagName("spml:batchResponse");
			logger.info("Information of all member" + nodeLst.getLength());
			for (int i = 0; i < nodeLst.getLength(); i++) {
				Node aNode = nodeLst.item(i);
				Node addResponseNode = addResponse.item(i);


				NamedNodeMap attributes = addResponseNode.getAttributes();

				for (int a = 0; a < attributes.getLength(); a++) {
					Node theAttribute = attributes.item(a);
					if (theAttribute.getNodeName() == "errorCode") {
						errorCode = theAttribute.getNodeValue();
					}
					if (theAttribute.getNodeName() == "errorType") {
						errorType = theAttribute.getNodeValue();
						message = aNode.getFirstChild().getNodeValue();

					}

				}

			}



		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
		//	e.printStackTrace();
		}

		dataMap.put("errorCode", errorCode);
		dataMap.put("errorType", errorType);
		dataMap.put("message", message);
		return dataMap;
	}


	@Override
	public CNTBDBDataResponse cntbDelete(CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String msisdnUid = "";
		String imsiUid = "";
		if (cntbdbSearchRequest.getMsisdn() != null && cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			msisdnUid = getUIDFromMSISDN(msisdn, cntbdbSearchRequest.getSessionConstants());

		}

		if (cntbdbSearchRequest.getImsi() != null && cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			imsiUid = getUIDFromIMSI(imsi, cntbdbSearchRequest.getSessionConstants());
		}


		if (msisdn != "" && imsi != "") {
			if (msisdnUid.equalsIgnoreCase(imsiUid)) {
				deleteReq(cntbdbDataResponse, imsiUid, cntbdbSearchRequest.getSessionConstants());
			} else {


			}
		} else if (msisdn != "") {
			if (msisdnUid != "") {
				deleteReq(cntbdbDataResponse, msisdnUid, cntbdbSearchRequest.getSessionConstants());
			} else {

			}

		} else {
			if (imsiUid != "") {
				deleteReq(cntbdbDataResponse, imsiUid, cntbdbSearchRequest.getSessionConstants());
			} else {

			}
		}



		return cntbdbDataResponse;

	}




	@Override
	public Map < String, Object > cntbBatchDelete(List < String > msisdnList, List < String > imsiList, SessionConstants sessionConstants) {
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		List < String > umiList = new ArrayList < String > ();
		List < String > notMathedMsisdnList = new ArrayList < String > ();
		List < String > notMathedimsiList = new ArrayList < String > ();

		String msisdn = "";
		String imsi = "";
		String msisdnUid = "";
		String imsiUid = "";
		String failInput="";
		int failuerCount=0;
		int successCount=0;
		

		for (int i = 0; i < msisdnList.size(); i++) {

			msisdn = msisdnList.get(i);
			msisdnUid = getUIDFromMSISDN(msisdn,sessionConstants);
			if (i < imsiList.size()) {
				imsi = imsiList.get(i);
				imsiUid = getUIDFromIMSI(imsi,sessionConstants);
			}
			if (msisdnUid.equalsIgnoreCase(imsiUid)) {
				umiList.add(msisdnUid);
				successCount=successCount+1;
			} else {
				notMathedMsisdnList.add(msisdn);
				notMathedimsiList.add(imsi);
				failInput=failInput+msisdn+","+imsi+",";
				failuerCount=failuerCount+1;
			}

		}
		try {
			String message = createSOAPRequestBatchDelete(umiList);
			dataMap.put("notMathedMsisdnList", notMathedMsisdnList);
			dataMap.put("notMathedMsisdnList", notMathedMsisdnList);
			dataMap.put("message", message);
			dataMap.put("failInput", failInput);
			dataMap.put("failuerCount", ""+failuerCount);
			dataMap.put("successCount", ""+successCount);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataMap;

	}


	public void deleteReq(CNTBDBDataResponse cntbdbDataResponse, String uid, SessionConstants sessionConstants) {
		try {
			String errorCode = "";
			String errorType = "";
			String message = "";
			Map < String, String > dataMap = new HashMap < String, String > ();
			String delete = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:siemens:names:prov:gw:SPML:2:0\">" +
				"<soapenv:Header/>" +
				"<soapenv:Body>" +
				"<spml:deleteRequest xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 prov-gw-spml-2.0.xsd\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<version>SUBSCRIBER_v20</version>" +
				"<objectclass>Subscriber</objectclass>" +
				"<identifier>" + uid + "</identifier>" +
				"</spml:deleteRequest>" +
				" </soapenv:Body>" +
				"</soapenv:Envelope>";

			String stringUrl = "http://" +SessionConstantsFactory.getSessionConstant("CNTB_StringUrl", sessionConstants) + "/ProvisioningGateway/services/SPMLSubscriber20Service";
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();
			uc.addRequestProperty("Connection", " keep-alive");
			uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
			uc.addRequestProperty("charset", "utf-8");
			uc.addRequestProperty("Content-Type", "text/xml");
			uc.addRequestProperty("Host", SessionConstantsFactory.getSessionConstant("CNTB_StringUrl", sessionConstants));
			uc.addRequestProperty("SOAPAction",
				"urn:siemens:names:prov:gw:SPML:2:0/deleteRequest");
			uc.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(uc.getOutputStream());

			wr.writeBytes(delete);

			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
			uc.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = in .readLine()) != null) {
				sb.append(decodedString);

			}
			dataMap = (Map<String, String>) parseXmltoJavaForDelete(sb.toString());

			try {

				if (dataMap.get("errorType") != null) {
					errorType = dataMap.get("errorType");
				}

				if (dataMap.get("errorCode") != null) {
					errorCode = dataMap.get("errorCode");
				}

				if (dataMap.get("message") != null) {
					message = dataMap.get("message");
				}


				if (errorType.equalsIgnoreCase("error")) {
					cntbdbDataResponse.setMessage(message + "  Error Code--" + errorCode);
				} else {
					message = "record saved successfully.";
					cntbdbDataResponse.setMessage(message);
				}

			} catch (Exception e) {

				cntbdbDataResponse.setMessage("Node not reachable!");
				cntbdbDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}



			in .close();
		} catch (Exception e) {
			e.printStackTrace();
			cntbdbDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
		}

	}







	public String getUIDFromMSISDN(String msisdn, SessionConstants sessionConstants) {

		String providerURL = "";
		String uid = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		if (msisdn != null && !msisdn.equals("")) {
			PROVIDER_URL_CNTB_MISSDN	= "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants) + "/dc=msisdn,dc=c-ntdb";
			providerURL = String.format(PROVIDER_URL_CNTB_MISSDN, "msisdn");
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", msisdn);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
				LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
				SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
				providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {

					NamingEnumeration < SearchResult > result = ctx.search(query,
					filter, sc);

					while (result != null && result.hasMoreElements()) {

						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());

						Attributes attrs = rs.getAttributes();
						System.out.println("attrs-------" + attrs);

						Attribute attribute = attrs.get("objectClass");

						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}


				} catch (NamingException e) {

					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		return uid;

	}

	public String getUIDFromIMSI(String imsi, SessionConstants sessionConstants) {
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";


		if (imsi != null && !imsi.equals("")) // for imsi
		{
			PROVIDER_URL_CNTB_IMSI = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants)+ "/dc=imsi,dc=c-ntdb";
			providerURL = String.format(PROVIDER_URL_CNTB_IMSI, "imsi");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "imsi", imsi);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
				LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
				SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
				providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {
					NamingEnumeration < SearchResult > result = ctx.search(query,
					filter, sc);

					while (result != null && result.hasMoreElements()) {
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						Attributes attrs = rs.getAttributes();
						Attribute attribute = attrs.get("objectClass");
						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}

				} catch (NamingException e) {
					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return uid;

	}

	@Override
	public CNTBDBDataResponse cnbtExistForAdd(CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String msisdnUid = "";
		String imsiUid = "";
		String message = "";

		if (cntbdbSearchRequest.getMsisdn() != null && cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			msisdnUid = getUIDFromMSISDN(msisdn, cntbdbSearchRequest.getSessionConstants());
			if (msisdnUid != "") {
				message = "exist";
				cntbdbDataResponse.setMessage(message);
			} else {
				message = "not exist";
				cntbdbDataResponse.setMessage(message);
			}

		}

		if (cntbdbSearchRequest.getImsi() != null && cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			imsiUid = getUIDFromIMSI(imsi,cntbdbSearchRequest.getSessionConstants());

			if (imsiUid != "") {
				message = "exist";
				cntbdbDataResponse.setMessage(message);
			} else {
				message = "not exist";
				cntbdbDataResponse.setMessage(message);
			}
		}


		cntbdbDataResponse.setMsisdn(msisdn);
		cntbdbDataResponse.setImsi(imsiUid);

		return cntbdbDataResponse;

	}


	@Override
	public CNTBDBDataResponse cnbtExistForDelete(CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String msisdnUid = "";
		String imsiUid = "";
		String message = "";

		if (cntbdbSearchRequest.getMsisdn() != null && cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			msisdnUid = getUIDFromMSISDN(msisdn,cntbdbSearchRequest.getSessionConstants());
			if (msisdnUid != "") {
				message = "exist";
				cntbdbDataResponse.setMessage(message);
			} else {
				message = "not exist";
				cntbdbDataResponse.setMessage(message);
			}

		}

		if (cntbdbSearchRequest.getImsi() != null && cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			imsiUid = getUIDFromIMSI(imsi,cntbdbSearchRequest.getSessionConstants());
			if (imsiUid != "") {
				message = "exist";
				cntbdbDataResponse.setMessage(message);
			} else {
				message = "not exist";
				cntbdbDataResponse.setMessage(message);
			}
		}





		if (cntbdbSearchRequest.getImsi() != null && cntbdbSearchRequest.getImsi() != "") {
			if (msisdnUid == imsiUid) {
				message = "exist";
				cntbdbDataResponse.setMessage(message);
			} else {
				message = "not exist";
				cntbdbDataResponse.setMessage(message);
			}

		}
		cntbdbDataResponse.setMsisdn(msisdn);
		cntbdbDataResponse.setImsi(imsiUid);

		return cntbdbDataResponse;

	}


	private String createSOAPRequestBatchDelete(List < String > uidList) throws Exception {
		String batchDeleteRequest = "";
		String requestPrefixString = "";
		String requestPostfixString = "";
		String errorCode = "";
		String errorType = "";
		String message = "";
		Map < String, String > dataMap = new HashMap < String, String > ();
		String uid = "";
		batchDeleteRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\"> " +
			"  <soapenv:Header/> " +
			"  <soapenv:Body> " +
			"    <spml:batchRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
			"        xmlns:sub=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " +
			"        xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 pro2v-gw-spml-2.0.xsd\" " +
			"        xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" newGenerated=\"true\" processing=\"parallel\" onError=\"resume\"> " +
			"      <version>SUBSCRIBER_v20</version> ";


		requestPostfixString = "    </spml:batchRequest> " +
			"  </soapenv:Body> " +
			"</soapenv:Envelope> ";

		for (int i = 0; i < uidList.size(); i++) {
			uid = uidList.get(i);
			requestPrefixString = requestPrefixString + " " + "<request xsi:type=\"spml:DeleteRequest\" returnResultingObject=\"identifier\"> " +
				"        <version>SUBSCRIBER_v20</version> " +
				"        <objectclass>Subscriber</objectclass> " +
				"        <identifier>" + uid + "</identifier> " +
				"      </request> ";

		}

		batchDeleteRequest = batchDeleteRequest + requestPrefixString + requestPostfixString;
		System.out.println("request---------" + batchDeleteRequest);

		String stringUrl = "http://10.25.72.241:8081/ProvisioningGateway/services/SPMLSubscriber20Service";
		URL url = new URL(stringUrl);
		URLConnection uc = url.openConnection();
		uc.addRequestProperty("Connection", " keep-alive");
		uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
		uc.addRequestProperty("charset", "utf-8");
		uc.addRequestProperty("Content-Type", "text/xml");
		uc.addRequestProperty("Host", "10.25.72.241:8081");
		uc.addRequestProperty("SOAPAction",
			"urn:siemens:names:prov:gw:SPML:2:0/deleteRequest");
		uc.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(uc.getOutputStream());

		wr.writeBytes(batchDeleteRequest);

		wr.flush();
		wr.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(
		uc.getInputStream()));
		String decodedString;
		StringBuffer sb = new StringBuffer();
		while ((decodedString = in .readLine()) != null) {
			sb.append(decodedString);

		}
		System.out.println("Response---------" + sb.toString());
		dataMap = (Map<String, String>) parseXmltoJavaForBatchDelete(sb.toString());

		try {

			if (dataMap.get("errorType") != null) {
				errorType = dataMap.get("errorType");
			}

			if (dataMap.get("errorCode") != null) {
				errorCode = dataMap.get("errorCode");
			}

			if (dataMap.get("message") != null) {
				message = dataMap.get("message");
			}


			if (errorType.equalsIgnoreCase("error")) {
				message = message + "  Error Code--" + errorCode;
			} else {
				message = "record Deleted successfully.";

			}

		} catch (Exception e) {
			message = "some problem occured pls try again.";

		}



		in .close();
		return message;
	}



	@Override
	public Map < String, Object > cnbtBatchSearch(List < String > msisdnList,
	List < String > imsiList, SessionConstants sessionConstants) {

		String providerURL = "";
		String uid = "";
		String msisdn = "";
		String imsi = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List < String > msisdnAttributeList = new ArrayList < String > ();
		List < String > attributeList = new ArrayList < String > ();
		List < String > imsiAttributeList = new ArrayList < String > ();
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		int failuerCount=0;
		int successCount=0;
		String failMsisdn="";
		String failImsi="";
		String successMsisdn="";
		String successImsi="";
		String failuerMsg="";

		for (int i = 0; i < msisdnList.size(); i++) {
			msisdn = msisdnList.get(i);

			if (msisdn != null && !msisdn.equals("")) {
				PROVIDER_URL_CNTB_MISSDN	= "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants) + "/dc=msisdn,dc=c-ntdb";
				providerURL = String.format(PROVIDER_URL_CNTB_MISSDN, "msisdn");
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", msisdn);
				filter = String.format(SEARCH_FILTER_DEFAULT, "*");

				try {
					ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
					providerURL));
				} catch (UserException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (ctx != null) {
					try {

						// System.out.println("NAP search query :" + query +
						// "\nfilter :"+ filter);

						NamingEnumeration < SearchResult > result = ctx.search(
						query, filter, sc);

						while (result != null && result.hasMoreElements()) {

							SearchResult rs = result.next();
							LdapName dn = new LdapName(rs.getNameInNamespace());

							Attributes attrs = rs.getAttributes();

							Attribute attribute = attrs.get("objectClass");

							if (attribute.contains("subscriber")) {
								uid = CNTBDBDataRepositoryHelper.getUID(dn,
								attrs);
							}
						}
						
						if(uid!="")
						{
							successMsisdn=successMsisdn+msisdn+",";
							successCount=successCount+1;
						}else
						{
							failMsisdn=failMsisdn+msisdn+",";
							failuerCount=failuerCount+1;
						}

						attributeList = getAttributeListForMSISDAndIMSI(uid, sessionConstants);
						//msisdnAttributeList.add(uid);
						msisdnAttributeList.addAll(attributeList);

					} catch (NamingException e) {

						e.printStackTrace();
						failMsisdn=failMsisdn+msisdn+",";
						failuerCount=failuerCount+1;
					} finally {
						try {
							ctx.close();
						} catch (NamingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}






			// For IMSI Batch Search


			imsi = imsiList.get(i);
			if (imsi != null && !imsi.equals("")) // for imsi
			{
				PROVIDER_URL_CNTB_IMSI = "ldap://" + SessionConstantsFactory.getSessionConstant("LDAP_SERVER_CNTB", sessionConstants)+ "/dc=imsi,dc=c-ntdb";
				providerURL = String.format(PROVIDER_URL_CNTB_IMSI, "imsi");
				System.out.println("providerURL :" + providerURL);
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				query = String.format(SEARCH_QUERY_DEFAULT, "imsi", imsi);
				filter = String.format(SEARCH_FILTER_DEFAULT, "*");

				try {
					ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
					providerURL));
				} catch (UserException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (ctx != null) {
					try {
						NamingEnumeration < SearchResult > result = ctx.search(
						query, filter, sc);

						while (result != null && result.hasMoreElements()) {
							SearchResult rs = result.next();
							LdapName dn = new LdapName(rs.getNameInNamespace());
							Attributes attrs = rs.getAttributes();
							Attribute attribute = attrs.get("objectClass");
							if (attribute.contains("subscriber")) {
								uid = CNTBDBDataRepositoryHelper.getUID(dn,
								attrs);
							}
						}
						
						if(uid!="")
						{
							successImsi=successImsi+imsi+",";
							successCount=successCount+1;
						}else
						{
							failMsisdn=failMsisdn+imsi+",";
							failuerCount=failuerCount+1;
						}
						
						attributeList = getAttributeListForMSISDAndIMSI(uid, sessionConstants);
						imsiAttributeList.addAll(attributeList);
					} catch (NamingException e) {
						e.printStackTrace();
						failMsisdn=failMsisdn+imsi+",";
						failuerCount=failuerCount+1;
					} finally {
						try {
							ctx.close();
						} catch (NamingException e) {
							e.printStackTrace();
						}
					}
				}
			}

		}
		
		
		
		dataMap.put("msisdnAttributeList", msisdnAttributeList);
		dataMap.put("imsiAttributeList", imsiAttributeList);
		dataMap.put("successCount", ""+successCount);
		dataMap.put("failuerCount", ""+failuerCount);
		dataMap.put("failMsisdn", failMsisdn);
		return dataMap;

	}


	@Override
	public Map < String, Object > cnbtBatchAdd(Map < String, Object > listDataMap, SessionConstants sessionConstants) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		List < String > msisdnList = new ArrayList < String > ();
		List < String > imsiList = new ArrayList < String > ();
		List < String > encKeyList = new ArrayList < String > ();
		List < String > accIdList = new ArrayList < String > ();
		List < String > alogIdList = new ArrayList < String > ();
		List < String > kdbIdList = new ArrayList < String > ();
		List < String > custIdList = new ArrayList < String > ();
		String msisdn = "";
		String imsi = "";
		String encKey = "";
		String accId = "";
		String alogId = "";
		String kdbId = "";
		String uid = "";
		String custId = "";
		String message = "";
		String requestPreFix = "";
		String requestPostFix = "";
		String requestBody = "";
		String request="";
		String batchAddSoapRequest = "";
		String uma = "";
		int failuerCount=0;
		int successCount=0;
		String failMsisdn="";
		String failImsi="";
		String successMsisdn="";
		String successImsi="";
		String failuerMsg="";
		requestPreFix = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\"> " + 
				 " " + 
				 "   <soapenv:Header/> " + 
				 " " + 
				 "   <soapenv:Body> " + 
				 " " + 
				 "    " + 
				 " " + 
				 "<spml:batchRequest execution=\"synchronous\" " + 
				 " " + 
				 "        processing=\"sequential\" " + 
				 " " + 
				 "        onError=\"exit_commit\" " + 
				 " " + 
				 "        xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" " + 
				 " " + 
				 "        xmlns:subscriber=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " + 
				 " " + 
				 "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> " + 
				 " " + 
				 "      <version>SUBSCRIBER_v20</version> " ;

		requestPostFix =  "</spml:batchRequest> " + 
				 " " + 
				 "   </soapenv:Body> " + 
				 " " + 
				 "</soapenv:Envelope> ";

		if (listDataMap.get("msisdnList") != null) {
			msisdnList = (List<String>) listDataMap.get("msisdnList");
		}

		if (listDataMap.get("imsiList") != null) {
			imsiList = (List<String>) listDataMap.get("imsiList");
		}

		if (listDataMap.get("encKeyList") != null) {
			encKeyList = (List<String>) listDataMap.get("encKeyList");
		}

		if (listDataMap.get("accIdList") != null) {
			accIdList = (List<String>) listDataMap.get("accIdList");
		}
		if (listDataMap.get("alogIdList") != null) {
			alogIdList = (List<String>) listDataMap.get("alogIdList");
		}

		if (listDataMap.get("kdbIdList") != null) {
			kdbIdList = (List<String>) listDataMap.get("kdbIdList");
		}
		if (listDataMap.get("custIdList") != null) {
			custIdList = (List<String>) listDataMap.get("custIdList");
		}

		for (int i = 0; i < msisdnList.size(); i++) {

			if (msisdnList.size() > i) {
				msisdn = msisdnList.get(i);
			}
			

			if (imsiList.size() > i) {
				imsi = imsiList.get(i);
			}
			
			if(msisdn!="")
			{
			
			if (encKeyList.size() > i) {
				encKey = encKeyList.get(i);
			}

			if (accIdList.size() > i) {
				accId = accIdList.get(i);
			}

			if (alogIdList.size() > i) {
				alogId = alogIdList.get(i);
			}

			if (kdbIdList.size() > i) {
				kdbId = kdbIdList.get(i);
			}

			if (custIdList.size() > i) {
				custId = custIdList.get(i);
			}

			uid = getUIDFromMSISDN(msisdn,sessionConstants);
			
			
			if (uid == "") {

				requestBody =  "<request newGenerated=\"true\" xsi:type=\"spml:AddRequest\"> " + 
						 " " + 
						 "<version>SUBSCRIBER_v20</version> " + 
						 " " + 
						 "<object xsi:type=\"subscriber:Subscriber\"> " + 
						 " " + 
						 "<auc> " + 
						 " " + 
						 "               <imsi>"+imsi+"</imsi> " + 
						 " " + 
						 "               <encKey>5E281D8EA8FF8EAED53C27E2B0718687</encKey> " + 
						 " " + 
						 "               <iccId>1</iccId> " + 
						 " " + 
						 "               <algoId>9</algoId> " + 
						 " " + 
						 "               <kdbId>61</kdbId> " + 
						 " " + 
						 "            </auc> " + 
						 " " + 
						 "            <hlr> " + 
						 " " + 
						 "               <ntype>single</ntype> " + 
						 " " + 
						 "               <osb1>true</osb1> " + 
						 " " + 
						 "               <rr>VOICESTR</rr> " + 
						 " " + 
						 "               <ts11> " + 
						 " " + 
						 "                  <msisdn>"+msisdn+"</msisdn> " + 
						 " " + 
						 "               </ts11> " + 
						 " " + 
						 "               <gprs> " + 
						 " " + 
						 "                  <msisdn>"+msisdn+"</msisdn> " + 
						 " " + 
						 "               </gprs> " + 
						 " " + 
						 "            </hlr> ";
						 
						 
						 if(custId!="")
						 {
						 uma= "<uma> " + 
						 " " + 
						 "         <imsi>"+imsi+"</imsi> " + 
						 " " + 
						 "         <msisdn>"+msisdn+"</msisdn> " + 
						 " " + 
						 "         <serviceAllowed>false</serviceAllowed> " + 
						 " " + 
						 "         <roamingAllowed>false</roamingAllowed> " + 
						 " " + 
						 "         <serviceType>serviceTypeId=0,OU=SERVICE,OU=UMA,NE=MOBILE_DATA_SERVER</serviceType> " + 
						 " " + 
						 "         <name>"+custId+"</name> " + 
						 " " + 
						 "</uma> ";
						 }
						 
						 String requestObject=" " + 
						 "</object> " + 
						 " " + 
						 "</request> ";
						 
						 request=request+requestBody+uma+requestObject;
						 successMsisdn=successMsisdn+msisdn+",";
						 successImsi=successImsi+imsi+",";
						 successCount=successCount+1;
			}else
			{
				failMsisdn=failMsisdn+msisdn+",";
				failImsi=failImsi+imsi+",";
				failuerCount=failuerCount+1;
			}
		}
		}

		batchAddSoapRequest = requestPreFix + request + requestPostFix;
		System.out.println("request---------" + batchAddSoapRequest);

		try {
			String stringUrl = "http://"+  SessionConstantsFactory.getSessionConstant("CNTB_StringUrl", sessionConstants) +"/ProvisioningGateway/services/SPMLSubscriber20Service";
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();
		
			uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
			uc.addRequestProperty("Connection", " keep-alive");
			uc.addRequestProperty("charset", "utf-8");
			//uc.addRequestProperty("User-Agent", "curl/7.12.1 (x86_64-redhat-linux-gnu) libcurl/7.12.1 OpenSSL/0.9.7a zlib/1.2.1.2 libidn/0.5.6");
			uc.addRequestProperty("Content-Type", "text/xml");
			uc.addRequestProperty("Host", SessionConstantsFactory.getSessionConstant("CNTB_StringUrl", sessionConstants));
			uc.addRequestProperty("SOAPAction",
					"urn:siemens:names:prov:gw:SPML:2:0/addRequest");
			uc.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
			String errorCode = "";
			String errorType = "";
			wr.writeBytes(batchAddSoapRequest);

			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(
			uc.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = in .readLine()) != null) {
				sb.append(decodedString);
			}


			dataMap = (Map<String, Object>) parseXmltoJavaForBatchAdd(sb.toString());
			
			logger.info("request===============" + batchAddSoapRequest);
			logger.info("response===============" + sb.toString());
			System.out.println("response===============" + sb.toString());
			try {
				if (dataMap.get("errorType") != null) {
					errorType = (String) dataMap.get("errorType");
				}

				if (dataMap.get("errorCode") != null) {
					errorCode = (String) dataMap.get("errorCode");
				}

				if (dataMap.get("message") != null) {
					message = (String) dataMap.get("message");
				}


				if (errorType.equalsIgnoreCase("error")) {
					cntbdbDataResponse.setMessage(message+"  Error Code--" + errorCode);
					message=message+"  Error Code--" + errorCode;
					failuerCount=failuerCount+successCount;
					successCount=0;
					failuerMsg="Failure input are-"+failMsisdn+successMsisdn+failImsi+successImsi;
					
				} else {
					message = " record saved successfully.";
					cntbdbDataResponse.setFailureMessage(""+"Failure input are-"+failMsisdn+failImsi);
					cntbdbDataResponse.setSuccessCount(""+successCount);
					cntbdbDataResponse.setFailurCount(""+failuerCount);

				}

			} catch (Exception e) {

				message="Node not reachable !";
			}



			in .close();
		} catch (Exception e) {
			e.printStackTrace();
			message="Node not reachable !";
		}
		
		System.out.println("Message---------"+message);

		dataMap.put("message", message);
		dataMap.put("failuerCount", failuerCount);
		dataMap.put("successCount", successCount);
		dataMap.put("failuerMsg", failuerMsg);
		return dataMap;

	}



	public Map < String, Object > parseXmltoJavaForBatchAdd(String xml) {
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		String errorCode = "";
		String errorType = "";
		String message = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element " + doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("errorMessage");
			NodeList addResponse = doc.getElementsByTagName("spml:batchResponse");
			logger.info("Information of all member" + nodeLst.getLength());
			for (int i = 0; i < nodeLst.getLength(); i++) {
				Node aNode = nodeLst.item(i);
				Node addResponseNode = addResponse.item(i);


				NamedNodeMap attributes = addResponseNode.getAttributes();

				for (int a = 0; a < attributes.getLength(); a++) {
					Node theAttribute = attributes.item(a);
					if (theAttribute.getNodeName() == "errorCode") {
						errorCode = theAttribute.getNodeValue();
					}
					if (theAttribute.getNodeName() == "errorType") {
						errorType = theAttribute.getNodeValue();
						message = aNode.getFirstChild().getNodeValue();

					}

				}

			}



		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
			e.printStackTrace();
		}

		dataMap.put("errorCode", errorCode);
		dataMap.put("errorType", errorType);
		dataMap.put("message", message);
		return dataMap;
	}


}