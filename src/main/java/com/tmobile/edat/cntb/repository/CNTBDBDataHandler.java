package com.tmobile.edat.cntb.repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.LdapName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tmobile.edat.cntb.request.CNTBDBSearchRequest;
import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.repository.USDDataHandler;
import com.tmobile.edat.usd.repository.USDDataRepositoryHelper;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

public class CNTBDBDataHandler  {
	// ldapsearch -x -h 10.25.72.247 -p 16800 -D cn=napProvUser -w siemens -b
	// imsi=310260666666666,dc=imsi,dc=c-ntdb -s subtree objectclass=*
	private static final Logger logger = Logger
			.getLogger(CNTBDBDataRepositoryImpl.class);
	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";
	private static String SECURITY_PRINCIPAL_CNTB_UID = "cn=napprovuser";
	private static String SECURITY_PRINCIPAL_CNTB = "cn=napProvUser";
	private static String SECURITY_CREDENTIALS_CNTB = "siemens";
	private static String PROVIDER_URL_CNTB_MISSDN = "ldap://"
			+ Constants.LDAP_SERVER_CNTB + "/dc=msisdn,dc=c-ntdb";
	private static String PROVIDER_URL_CNTB_UID = "ldap://"
			+ Constants.LDAP_SERVER_CNTB + "/dc=C-NTDB";

	private static String PROVIDER_URL_CNTB_IMSI = "ldap://"
			+ Constants.LDAP_SERVER_CNTB + "/dc=imsi,dc=c-ntdb";

	String SEARCH_QUERY_DEFAULT = "%s=%s";
	String SEARCH_FILTER_DEFAULT = "(objectclass=%s)";
	String SEARCH_QUERY_UID = "uid=%s,ds=SUBSCRIBER,o=DEFAULT";

	public List<String> doMSISDNCSearch(String msisdn) {

		String providerURL = "";
		String uid = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List<String> attributeList = new ArrayList<String>();

		if (msisdn != null && !msisdn.equals("")) {
			providerURL = String.format(PROVIDER_URL_CNTB_MISSDN, "msisdn");
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", msisdn);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
						providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {

					// System.out.println("NAP search query :" + query +
					// "\nfilter :"+ filter);

					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);

					while (result != null && result.hasMoreElements()) {

						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());

						Attributes attrs = rs.getAttributes();

						Attribute attribute = attrs.get("objectClass");

						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}

					attributeList = getAttributeListForMSISDAndIMSI(uid);
					attributeList.add(uid);

				} catch (NamingException e) {

					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		return attributeList;

	}

	public List<String> doIMSICSearch(String imsi) {
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";

		List<String> attributeList = new ArrayList<String>();

		if (imsi != null && !imsi.equals("")) // for imsi
		{
			providerURL = String.format(PROVIDER_URL_CNTB_IMSI, "imsi");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "imsi", imsi);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
						providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {
					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);

					while (result != null && result.hasMoreElements()) {
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						Attributes attrs = rs.getAttributes();
						Attribute attribute = attrs.get("objectClass");
						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}
					attributeList = getAttributeListForMSISDAndIMSI(uid);
					attributeList.add(uid);
				} catch (NamingException e) {
					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return attributeList;

	}

	public List<String> getAttributeListForMSISDAndIMSI(String uid) {

		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List<String> attributeList = new ArrayList<String>();
		if (uid != null && !uid.equals("")) {
			providerURL = PROVIDER_URL_CNTB_UID;

			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_UID, uid);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");
		}

		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_CNTB_UID, SECURITY_CREDENTIALS_CNTB,
					providerURL));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (ctx != null) {
			try {

				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);

				while (result != null && result.hasMoreElements()) {

					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());

					Attributes attrs = rs.getAttributes();

					Attribute attribute = attrs.get("objectClass");

					attributeList = CNTBDBDataRepositoryHelper
							.fillUIDForCNTBDB(dn, attrs, attributeList,
									attribute);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println(attributeList.size());
		for (int i = 0; i < attributeList.size(); i++) {
			System.out.println("---------------" + attributeList.get(i));
		}

		return attributeList;

	}

	public CNTBDBDataResponse cntbSave(CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();

		return cntbdbDataResponse;
	}

	public CNTBDBDataResponse cntbSave(String msisdn, String imsi) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		// String msisdn="";
		// String imsi="";
		String encKey = "5E281D8EA8FF8EAED53C27E2B0718687";
		String accId = "1";
		String alogId = "9";
		String kdbId = "61";
		String uid = "";
		String acsub="2";
		String custId="8500";
		String message = "";
		String privateUserId="";
		String originalPublicUserId="";
		
		if (msisdn != null && msisdn != "") {

			uid = getUIDFromMSISDN(msisdn);
		}

		if (imsi != null && imsi != "") {

			uid = getUIDFromIMSI(imsi);
			privateUserId=imsi+"@msg.lab.t-mobile.com";
		}
		/*
		 * if(cntbdbSearchRequest.getEncKey()!=null &&
		 * cntbdbSearchRequest.getEncKey()!="") {
		 * encKey=cntbdbSearchRequest.getEncKey(); }
		 * if(cntbdbSearchRequest.getAccId()!=null &&
		 * cntbdbSearchRequest.getAccId()!="") {
		 * accId=cntbdbSearchRequest.getAccId(); }
		 * if(cntbdbSearchRequest.getAlogId()!=null &&
		 * cntbdbSearchRequest.getAlogId()!="") {
		 * alogId=cntbdbSearchRequest.getAlogId(); }
		 * if(cntbdbSearchRequest.getKdbId()!=null &&
		 * cntbdbSearchRequest.getKdbId()!="") {
		 * kdbId=cntbdbSearchRequest.getKdbId(); }
		 * 
		 */
		String addnewPrefix="";
		String addnewPostfix="";
		String uma="";
		String addnew="";
		
		
		
		
		/*	addnewPrefix = "<soapenv:Envelope "
					+ "               xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
					+ "               xmlns:urn=\"urn:siemens:names:prov:gw:SPML:2:0\"> "
					+ "<soapenv:Body> "
					+ "<spml:addRequest "
					+ "               xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 pro2v-gw-spml-2.0.xsd\" "
					+ "               xmlns:sub=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" "
					+ "               xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" "
					+ "               xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" returnResultingObject=\"full\"> "
					+ "               <version>SUBSCRIBER_v20</version> "
					+ "               <object xsi:type=\"sub:Subscriber\"> "
					+ "               <identifier></identifier> "
					+ "               <auc> "
					+ "                              <imsi>"
					+ imsi
					+ "</imsi> "
					+ // dynamic *
					"                              <encKey>"
					+ encKey
					+ "</encKey> "
					+ // dynamic
					"                              <iccId>"
					+ accId
					+ "</iccId> "
					+ // dynamic
					"                              <algoId>"
					+ alogId
					+ "</algoId> "
					+ // dynamic
					"                              <kdbId>"
					+ kdbId
					+ "</kdbId> "
					+ // dynamic
					"                              <acsub>2</acsub> "
					+ "                              <amf>0000</amf> "
					+ "                              <sqn>000000000000</sqn> "
					+ "               </auc> "
					+ "        <hss> "
					+ "            <subscriptionId>1</subscriptionId> "
					+ "                <profileType>normal</profileType> "
					+ "                <adminBlocked>false</adminBlocked> "
					+ "                <defaultScscfRequired>true</defaultScscfRequired> "
					+ "                <maximumPublicIds>10</maximumPublicIds> "
					+ "                <privateUserId> "
					+ "                        <privateUserId>310260666666666@msg.lab.t-mobile.com</privateUserId> "
					+ "                        <provisionedImsi> "
					+ "                        <provisionedImsi>310260666666666</provisionedImsi> "
					+ "                        </provisionedImsi> "
					+ "                        <msisdn>"
					+ msisdn
					+ "</msisdn> "
					+ // dynamic *
					"                        <preferredAuthenticationScheme>aka</preferredAuthenticationScheme> "
					+ "                        <preferredDomain>none</preferredDomain> "
					+ "                </privateUserId> "
					+ "                              <implicitRegisteredSet> "
					+ "                  <irsId>"
					+ msisdn
					+ "</irsId> "
					+ "                </implicitRegisteredSet> "
					+ "                <publicUserId> "
					+ "                        <originalPublicUserId>sip:"
					+ msisdn
					+ "@msg.lab.t-mobile.com</originalPublicUserId> "
					+ "                        <defaultIndication>true</defaultIndication> "
					+ "                        <serviceProfileName>SP01</serviceProfileName> "
					+ "                        <irsId>"
					+ msisdn
					+ "</irsId> "
					+ "                </publicUserId> "
					+ "                <serviceProfile> "
					+ "                        <profileName>SP01</profileName> "
					+ "                        <mandatoryCapability> "
					+ "                                <mandatoryCapability>3</mandatoryCapability> "
					+ "                        </mandatoryCapability> "
					+ "                        <mandatoryCapability> "
					+ "                                <mandatoryCapability>4</mandatoryCapability> "
					+ "                        </mandatoryCapability> "
					+ "                        <globalFilterId> "
					+ "                                <globalFilterId>RLS-BASIC</globalFilterId> "
					+ "                        </globalFilterId> "
					+ "                        <globalFilterId> "
					+ "                                <globalFilterId>PS-BASIC</globalFilterId> "
					+ "                        </globalFilterId> "
					+ "                        <subscribedMediaProfileID> "
					+ "                                <sessionReleasePolicy>deregisterForcedSessionRelease</sessionReleasePolicy> "
					+ "                                <forkingPolicy>noForking</forkingPolicy> "
					+ "                        </subscribedMediaProfileID> "
					+ "                </serviceProfile> "
					+ "        </hss> ";
					if(uma!="")
					{
					uma="<uma> "
					+ "         <imsi>"
					+ imsi
					+ "</imsi> "
					+ "         <msisdn>"
					+ msisdn
					+ "</msisdn> "
					+ "         <serviceAllowed>false</serviceAllowed> "
					+ "         <roamingAllowed>false</roamingAllowed> "
					+ "         <serviceType>serviceTypeId=0,OU=SERVICE,OU=UMA,NE=MOBILE_DATA_SERVER</serviceType> "
					+ "         <name>8500</name> "
					+ "</uma> ";
					}
					addnewPostfix="    </object> "
					+ "</spml:addRequest> "
					+ "   </soapenv:Body> " + "</soapenv:Envelope> ";
*/					
		addnewPrefix= "<soapenv:Envelope  " + 
		 "               xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"  " + 
		 "               xmlns:urn=\"urn:siemens:names:prov:gw:SPML:2:0\"> " + 
		 "<soapenv:Body> " + 
		 "<spml:addRequest  " + 
		 "               xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 pro2v-gw-spml-2.0.xsd\" " + 
		 "               xmlns:sub=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " + 
		 "               xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" " + 
		 "               xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" returnResultingObject=\"full\"> " + 
		 "               <version>SUBSCRIBER_v20</version> " + 
		 "               <object xsi:type=\"sub:Subscriber\"> " + 
		 "               <identifier></identifier> " + 
		 "               <auc> " + 
		 "                              <imsi>"+imsi+"</imsi> " + 
		 "                              <encKey>"+encKey+"</encKey> " + 
		 "                              <iccId>"+accId+"</iccId> " + 
		 "                              <algoId>"+alogId+"</algoId> " + 
		 "                              <kdbId>"+kdbId+"</kdbId> " + 
		 "                              <acsub>"+acsub+"</acsub> " + 
		 "                              <amf>0000</amf> " + 
		 "                              <sqn>000000000000</sqn> " + 
		 "               </auc> " + 
		 "        <hss> " + 
		 "            <subscriptionId>1</subscriptionId> " + 
		 "                <profileType>normal</profileType> " + 
		 "                <adminBlocked>false</adminBlocked> " + 
		 "                <defaultScscfRequired>true</defaultScscfRequired> " + 
		 "                <maximumPublicIds>10</maximumPublicIds> " + 
		 "                <privateUserId> " + 
		 "                        <privateUserId>"+privateUserId+"</privateUserId> " + 
		 "                        <provisionedImsi> " + 
		 "                        <provisionedImsi>"+imsi+"</provisionedImsi> " + 
		 "                        </provisionedImsi> " + 
		 "                        <msisdn>"+msisdn+"</msisdn> " + 
		 "                        <preferredAuthenticationScheme>aka</preferredAuthenticationScheme> " + 
		 "                        <preferredDomain>none</preferredDomain> " + 
		 "                </privateUserId> " + 
		 "                              <implicitRegisteredSet> " + 
		 "                  <irsId>"+msisdn+"</irsId> " + 
		 "                </implicitRegisteredSet> " + 
		 "                <publicUserId> " + 
		 "                        <originalPublicUserId>sip:"+msisdn+"@msg.lab.t-mobile.com</originalPublicUserId> " + 
		 "                        <defaultIndication>true</defaultIndication> " + 
		 "                        <serviceProfileName>SP01</serviceProfileName> " + 
		 "                        <irsId>"+msisdn+"</irsId> " + 
		 "                </publicUserId> " + 
		 "                <serviceProfile> " + 
		 "                        <profileName>SP01</profileName> " + 
		 "                        <mandatoryCapability> " + 
		 "                                <mandatoryCapability>3</mandatoryCapability> " + 
		 "                        </mandatoryCapability> " + 
		 "                        <mandatoryCapability> " + 
		 "                                <mandatoryCapability>4</mandatoryCapability> " + 
		 "                        </mandatoryCapability> " + 
		 "                        <globalFilterId> " + 
		 "                                <globalFilterId>RLS-BASIC</globalFilterId> " + 
		 "                        </globalFilterId> " + 
		 "                        <globalFilterId> " + 
		 "                                <globalFilterId>PS-BASIC</globalFilterId> " + 
		 "                        </globalFilterId> " + 
		 "                        <subscribedMediaProfileID> " + 
		 "                                <sessionReleasePolicy>deregisterForcedSessionRelease</sessionReleasePolicy> " + 
		 "                                <forkingPolicy>noForking</forkingPolicy> " + 
		 "                        </subscribedMediaProfileID> " + 
		 "                </serviceProfile> " + 
		 "        </hss> ";
			if(custId!="")
		{
		 uma= "<uma> " + 
		 "         <imsi>"+imsi+"</imsi> " + 
		 "         <msisdn>"+msisdn+"</msisdn> " + 
		 "         <serviceAllowed>false</serviceAllowed> " + 
		 "         <roamingAllowed>false</roamingAllowed> " + 
		 "         <serviceType>serviceTypeId=0,OU=SERVICE,OU=UMA,NE=MOBILE_DATA_SERVER</serviceType> " + 
		 "         <name>"+custId+"</name> " + 
		 "</uma> ";
		}
		 
		addnewPostfix= "    </object> " + 
		 "</spml:addRequest> " + 
		 "   </soapenv:Body> " + 
		 "</soapenv:Envelope> ";
		
		
		
		addnew=addnewPrefix+uma+addnewPostfix;

			try {
				String stringUrl = "http://" + Constants.CNTB_StringUrl + "/ProvisioningGateway/services/SPMLSubscriber20Service";
				URL url = new URL(stringUrl);
				URLConnection uc = url.openConnection();

				uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
				uc.addRequestProperty("Connection", " keep-alive");
				uc.addRequestProperty("charset", "utf-8");
				//uc.addRequestProperty("User-Agent", "curl/7.12.1 (x86_64-redhat-linux-gnu) libcurl/7.12.1 OpenSSL/0.9.7a zlib/1.2.1.2 libidn/0.5.6");
				uc.addRequestProperty("Content-Type", "text/xml");
				uc.addRequestProperty("Host", Constants.CNTB_StringUrl);
				uc.addRequestProperty("SOAPAction",
					"urn:siemens:names:prov:gw:SPML:2:0/addRequest");
				uc.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
				Map < String, Object > dataMap = new HashMap < String, Object > ();
				String errorCode = "";
				String errorType = "";
				wr.writeBytes(addnew);

				wr.flush();
				wr.close();
				BufferedReader in = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
				String decodedString;
				StringBuffer sb = new StringBuffer();
				while ((decodedString = in .readLine()) != null) {
					sb.append(decodedString);
				}


				dataMap = (Map<String, Object>)parseXmltoJava(sb.toString());

				try {
					if (dataMap.get("errorType") != null) {
						errorType = (String) dataMap.get("errorType");
					}

					if (dataMap.get("errorCode") != null) {
						errorCode = (String) dataMap.get("errorCode");
					}

					if (dataMap.get("message") != null) {
						message = (String) dataMap.get("message");
					}


					if (errorType.equalsIgnoreCase("error")) {
						cntbdbDataResponse.setMessage(message + "  Error Code--" + errorCode);
					} else {
						message = "record saved successfully.";
						cntbdbDataResponse.setMessage(message);
					}

				} catch (Exception e) {

					cntbdbDataResponse.setMessage("some problem occured please try again.");
				}



				in .close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		

		return cntbdbDataResponse;

	}

	public CNTBDBDataResponse cntbDelete(CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String msisdnUid = "";
		String imsiUid = "";

		if (cntbdbSearchRequest.getMsisdn() != null
				&& cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			msisdnUid = getUIDFromMSISDN(msisdn);

		}

		if (cntbdbSearchRequest.getImsi() != null
				&& cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			imsiUid = getUIDFromIMSI(msisdn);
		}

		if (msisdn != "" && imsi != "") {
			if (msisdnUid.equalsIgnoreCase(imsiUid)) {
				deleteReq();
			} else {
			}
		} else if (msisdn != "") {
			deleteReq();

		} else {
			deleteReq();
		}

		return cntbdbDataResponse;

	}

	public void deleteReq() {
		try {

			String delete = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:siemens:names:prov:gw:SPML:2:0\">"
					+ "<soapenv:Header/>"
					+ "<soapenv:Body>"
					+ "<spml:deleteRequest xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 prov-gw-spml-2.0.xsd\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
					+ "<version>SUBSCRIBER_v20</version>"
					+ "<objectclass>Subscriber</objectclass>"
					+ "<identifier>262121111150010_hss_test</identifier>"
					+ "</spml:deleteRequest>"
					+ " </soapenv:Body>"
					+ "</soapenv:Envelope>";

			String stringUrl = "http://" + Constants.CNTB_StringUrl
					+ "/ProvisioningGateway/services/SPMLSubscriber20Service";
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();
			uc.addRequestProperty("Connection", " keep-alive");
			uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
			uc.addRequestProperty("charset", "utf-8");
			uc.addRequestProperty("Content-Type", "text/xml");
			uc.addRequestProperty("Host", Constants.CNTB_StringUrl);
			uc.addRequestProperty("SOAPAction",
					"urn:siemens:names:prov:gw:SPML:2:0/deleteRequest");
			uc.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(uc.getOutputStream());

			wr.writeBytes(delete);

			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					uc.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = in.readLine()) != null) {
				sb.append(decodedString);

			}
			System.out.println("Delete Response:\n" + sb.toString());
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getUIDFromMSISDN(String msisdn) {

		String providerURL = "";
		String uid = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List<String> attributeList = new ArrayList<String>();

		if (msisdn != null && !msisdn.equals("")) {
			providerURL = String.format(PROVIDER_URL_CNTB_MISSDN, "msisdn");
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", msisdn);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
						providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {

					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);

					while (result != null && result.hasMoreElements()) {

						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());

						Attributes attrs = rs.getAttributes();

						Attribute attribute = attrs.get("objectClass");

						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}

				} catch (NamingException e) {

					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		return uid;

	}

	public String getUIDFromIMSI(String imsi) {
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String uid = "";

		if (imsi != null && !imsi.equals("")) // for imsi
		{
			providerURL = String.format(PROVIDER_URL_CNTB_IMSI, "imsi");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			query = String.format(SEARCH_QUERY_DEFAULT, "imsi", imsi);
			filter = String.format(SEARCH_FILTER_DEFAULT, "*");

			try {
				ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
						LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
						SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
						providerURL));
			} catch (UserException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (ctx != null) {
				try {
					NamingEnumeration<SearchResult> result = ctx.search(query,
							filter, sc);

					while (result != null && result.hasMoreElements()) {
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						Attributes attrs = rs.getAttributes();
						Attribute attribute = attrs.get("objectClass");
						if (attribute.contains("subscriber")) {
							uid = CNTBDBDataRepositoryHelper.getUID(dn, attrs);
						}
					}

				} catch (NamingException e) {
					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return uid;

	}

	public void test() {

		DOMParser parser = new DOMParser();
		try {
			parser.parse("D:/my-file.xml");
		} catch (Exception e) {
		}
		Document dom = parser.getDocument();

		NodeList games = dom.getElementsByTagName("errorMessage");

		for (int i = 0; i < games.getLength(); i++) {
			Node aNode = games.item(i);
			System.out.println("==========================="
					+ aNode.getFirstChild().getNodeValue());

			NamedNodeMap attributes = aNode.getAttributes();

			for (int a = 0; a < attributes.getLength(); a++) {
				Node theAttribute = attributes.item(a);
				System.out.println(theAttribute.getNodeName() + "="
						+ theAttribute.getNodeValue());
			}

		}

	}

	public CNTBDBDataResponse cnbtExistForAdd(
			CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String uid = "";
		String message = "";

		if (cntbdbSearchRequest.getMsisdn() != null
				&& cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			uid = getUIDFromMSISDN(msisdn);
		}

		if (cntbdbSearchRequest.getImsi() != null
				&& cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			uid = getUIDFromIMSI(imsi);
		}

		if (msisdn != "" && imsi != "") {
			message = "exist";
			cntbdbDataResponse.setMessage(message);
		} else {
			message = "not exist";
			cntbdbDataResponse.setMessage(message);
		}

		return cntbdbDataResponse;

	}

	public CNTBDBDataResponse cnbtExistForDelete(
			CNTBDBSearchRequest cntbdbSearchRequest) {
		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		String msisdn = "";
		String imsi = "";
		String msisdnUid = "";
		String imsiUid = "";
		String message = "";

		if (cntbdbSearchRequest.getMsisdn() != null
				&& cntbdbSearchRequest.getMsisdn() != "") {
			msisdn = cntbdbSearchRequest.getMsisdn();
			msisdnUid = getUIDFromMSISDN(msisdn);
		}

		if (cntbdbSearchRequest.getImsi() != null
				&& cntbdbSearchRequest.getImsi() != "") {
			imsi = cntbdbSearchRequest.getImsi();
			imsiUid = getUIDFromIMSI(imsi);
		}

		if (msisdnUid == imsiUid) {
			message = "exist";
			cntbdbDataResponse.setMessage(message);
		} else {
			message = "not exist";
			cntbdbDataResponse.setMessage(message);
		}

		return cntbdbDataResponse;

	}

	
	
	public Map<String,Object> cntbBatchDelete(List<String>msisdnList,List<String>imsiList)
	{	
		Map<String, Object>dataMap=new HashMap<String, Object>();
		List<String>umiList=new ArrayList<String>();
		List<String>notMathedMsisdnList=new ArrayList<String>();
		List<String>notMathedimsiList=new ArrayList<String>();
		
		String msisdn="";
		String imsi="";
		String msisdnUid="";
		String imsiUid="";
		
		for(int i=0;i<msisdnList.size();i++)
		{
		
			msisdn=msisdnList.get(i);
			msisdnUid=getUIDFromMSISDN(msisdn);
			if(i<imsiList.size())
			{
			imsi=imsiList.get(i);
			imsiUid=getUIDFromIMSI(imsi);
			}
			if(msisdnUid.equalsIgnoreCase(imsiUid))
			{
				umiList.add(msisdnUid);
			}else
			{
				notMathedMsisdnList.add(msisdn);
				notMathedimsiList.add(imsi);
			}
		
		}
			try
			{
			String message=createSOAPRequestBatchDelete(umiList);
			dataMap.put("notMathedMsisdnList",notMathedMsisdnList);
			dataMap.put("notMathedMsisdnList",notMathedMsisdnList);
			dataMap.put("message",message);
			
			}catch(Exception e){e.printStackTrace();}
			
		return dataMap;

	}
	
	
	private String createSOAPRequestBatchDelete(List<String> uidList) throws Exception {
		String batchDeleteRequest="";
		String requestPrefixString="";
		String requestPostfixString="";
		String request="";
		String errorCode="";
		String errorType="";
		String message="";
		Map<String, String> dataMap = new HashMap<String, String>();
		String uid="";
		batchDeleteRequest=	"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\"> " + 
		 "  <soapenv:Header/> " + 
		 "  <soapenv:Body> " + 
		 "    <spml:batchRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " + 
		 "        xmlns:sub=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " + 
		 "        xsi:schemaLocation=\"urn:siemens:names:prov:gw:SPML:2:0 pro2v-gw-spml-2.0.xsd\" " + 
		 "        xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" newGenerated=\"true\" processing=\"parallel\" onError=\"resume\"> " + 
		 "      <version>SUBSCRIBER_v20</version> ";
		
		
		 requestPostfixString= "    </spml:batchRequest> " + 
				 "  </soapenv:Body> " + 
				 "</soapenv:Envelope> ";
		 
		 for(int i=0;i<uidList.size();i++)
		 {
			 uid=uidList.get(i);
			 requestPrefixString=requestPrefixString+" "+ "<request xsi:type=\"spml:DeleteRequest\" returnResultingObject=\"identifier\"> " + 
					 "        <version>SUBSCRIBER_v20</version> " + 
					 "        <objectclass>Subscriber</objectclass> " + 
					 "        <identifier>"+uid+"</identifier> " + 
					 "      </request> ";
			 
		 }
		
		 batchDeleteRequest=batchDeleteRequest+requestPrefixString+requestPostfixString;
		 System.out.println("request---------"+batchDeleteRequest);
	
		String stringUrl = "http://10.25.72.241:8081/ProvisioningGateway/services/SPMLSubscriber20Service";
		URL url = new URL(stringUrl);
		URLConnection uc = url.openConnection();
		uc.addRequestProperty("Connection", " keep-alive");
		uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
		uc.addRequestProperty("charset", "utf-8");
		uc.addRequestProperty("Content-Type", "text/xml");
		uc.addRequestProperty("Host", "10.25.72.241:8081");
		uc.addRequestProperty("SOAPAction",
				"urn:siemens:names:prov:gw:SPML:2:0/deleteRequest");
		uc.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(uc.getOutputStream());

		wr.writeBytes(batchDeleteRequest);

		wr.flush();
		wr.close();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
		String decodedString;
		StringBuffer sb = new StringBuffer();
		while ((decodedString = in.readLine()) != null) {
			sb.append(decodedString);
			
			}
		System.out.println("Response---------"+sb.toString());
		dataMap=(Map)parseXmltoJavaForBatchDelete(sb.toString());
						
		try
		{
			
			if(dataMap.get("errorType")!=null)
			{
				errorType=(String)dataMap.get("errorType");
			}
			
			if(dataMap.get("errorCode")!=null)
			{
				errorCode=(String)dataMap.get("errorCode");
			}
			
			if(dataMap.get("message")!=null)
			{
				message=(String)dataMap.get("message");
			}
				
			
			if(errorType.equalsIgnoreCase("error"))
			{
				message=message+"  Error Code--"+errorCode;
			}else
			{
				message="record Deleted successfully.";
				
			}
			
		}catch(Exception e)
		{
			message="some problem occured pls try again.";
			
		}
		
		
	
		in.close();
		return message;
	}
	
	
	public Map<String, Object> cnbtBatchSearch(List<String> msisdnList,
			List<String> imsiList) {

		String providerURL = "";
		String uid = "";
		String msisdn = "";
		String imsi = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		List<String> msisdnAttributeList = new ArrayList<String>();
		List<String> attributeList = new ArrayList<String>();
		List<String> imsiAttributeList = new ArrayList<String>();
		Map<String, Object> dataMap = new HashMap<String, Object>();

		for (int i = 0; i < msisdnList.size(); i++) {
			msisdn = msisdnList.get(i);

			if (msisdn != null && !msisdn.equals("")) {
				providerURL = String.format(PROVIDER_URL_CNTB_MISSDN, "msisdn");
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				query = String.format(SEARCH_QUERY_DEFAULT, "msisdn", msisdn);
				filter = String.format(SEARCH_FILTER_DEFAULT, "*");

				try {
					ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
							LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
							SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
							providerURL));
				} catch (UserException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (ctx != null) {
					try {

						// System.out.println("NAP search query :" + query +
						// "\nfilter :"+ filter);

						NamingEnumeration<SearchResult> result = ctx.search(
								query, filter, sc);

						while (result != null && result.hasMoreElements()) {

							SearchResult rs = result.next();
							LdapName dn = new LdapName(rs.getNameInNamespace());

							Attributes attrs = rs.getAttributes();

							Attribute attribute = attrs.get("objectClass");

							if (attribute.contains("subscriber")) {
								uid = CNTBDBDataRepositoryHelper.getUID(dn,
										attrs);
							}
						}

						attributeList = getAttributeListForMSISDAndIMSI(uid);
						//msisdnAttributeList.add(uid);
						msisdnAttributeList.addAll(attributeList);

					} catch (NamingException e) {

						e.printStackTrace();
					} finally {
						try {
							ctx.close();
						} catch (NamingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}

		}

		for (int j = 0; j < imsiList.size(); j++) {
			imsi = imsiList.get(j);
			if (imsi != null && !imsi.equals("")) // for imsi
			{
				providerURL = String.format(PROVIDER_URL_CNTB_IMSI, "imsi");
				System.out.println("providerURL :" + providerURL);
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				query = String.format(SEARCH_QUERY_DEFAULT, "imsi", imsi);
				filter = String.format(SEARCH_FILTER_DEFAULT, "*");

				try {
					ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
							LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
							SECURITY_PRINCIPAL_CNTB, SECURITY_CREDENTIALS_CNTB,
							providerURL));
				} catch (UserException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (ctx != null) {
					try {
						NamingEnumeration<SearchResult> result = ctx.search(
								query, filter, sc);

						while (result != null && result.hasMoreElements()) {
							SearchResult rs = result.next();
							LdapName dn = new LdapName(rs.getNameInNamespace());
							Attributes attrs = rs.getAttributes();
							Attribute attribute = attrs.get("objectClass");
							if (attribute.contains("subscriber")) {
								uid = CNTBDBDataRepositoryHelper.getUID(dn,
										attrs);
							}
						}
						attributeList = getAttributeListForMSISDAndIMSI(uid);
						imsiAttributeList.addAll(attributeList);
					} catch (NamingException e) {
						e.printStackTrace();
					} finally {
						try {
							ctx.close();
						} catch (NamingException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		dataMap.put("msisdnAttributeList", msisdnAttributeList);
		dataMap.put("imsiAttributeList", imsiAttributeList);
		return dataMap;

	}
		
	
	
	public Map<String,Object> cnbtBatchAdd(String msisdn,String imsi) {
		CNTBDBDataResponse cntbdbDataResponse=new CNTBDBDataResponse();
		Map<String, Object>dataMap=new HashMap<String, Object>();
		List<String> msisdnList = new ArrayList<String>();
		List<String> imsiList = new ArrayList<String>();
		List<String> encKeyList = new ArrayList<String>();
		List<String> accIdList = new ArrayList<String>();
		List<String> alogIdList = new ArrayList<String>();
		List<String> kdbIdList = new ArrayList<String>();
		List<String> custIdList = new ArrayList<String>();
	
		String encKey="";
		String accId="";
		String alogId="";
		String kdbId="";
		String uid="";
		String custId="";
		String message="";
		String requestPreFix="";
		String requestPostFix="";
		String requestBody="";
		String batchAddSoapRequest="";
		String uma="";
		requestPreFix="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\"> " + 
		 " " + 
		 "   <soapenv:Header/> " + 
		 " " + 
		 "   <soapenv:Body> " + 
		 " " + 
		 "    " + 
		 " " + 
		 "<spml:batchRequest execution=\"synchronous\" " + 
		 " " + 
		 "        processing=\"sequential\" " + 
		 " " + 
		 "        onError=\"exit_commit\" " + 
		 " " + 
		 "        xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" " + 
		 " " + 
		 "        xmlns:subscriber=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " + 
		 " " + 
		 "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> " + 
		 " " + 
		 "      <version>SUBSCRIBER_v20</version> " + 
		 " " + 
		 "  " + 
		 " " + 
		 "<request newGenerated=\"true\" xsi:type=\"spml:AddRequest\"> " + 
		 " " + 
		 "<version>SUBSCRIBER_v20</version> " + 
		 " " + 
		 "<object xsi:type=\"subscriber:Subscriber\"> " + 
		 " " + 
		 "<auc> " + 
		 " " + 
		 "               <imsi>129220424911111</imsi> " + 
		 " " + 
		 "               <encKey>01234567890123456789012345678901</encKey> " + 
		 " " + 
		 "               <iccId>866</iccId> " + 
		 " " + 
		 "               <algoId>9</algoId> " + 
		 " " + 
		 "               <kdbId>50</kdbId> " + 
		 " " + 
		 "            </auc> " + 
		 " " + 
		 "            <hlr> " + 
		 " " + 
		 "               <ntype>single</ntype> " + 
		 " " + 
		 "               <osb1>true</osb1> " + 
		 " " + 
		 "               <rr>VOICESTR</rr> " + 
		 " " + 
		 "               <ts11> " + 
		 " " + 
		 "                  <msisdn>1292204249</msisdn> " + 
		 " " + 
		 "               </ts11> " + 
		 " " + 
		 "               <gprs> " + 
		 " " + 
		 "                  <msisdn>1292204249</msisdn> " + 
		 " " + 
		 "               </gprs> " + 
		 " " + 
		 "            </hlr> " + 
		 " " + 
		 "<uma> " + 
		 " " + 
		 "         <imsi>129220424911111</imsi> " + 
		 " " + 
		 "         <msisdn>1292204249</msisdn> " + 
		 " " + 
		 "         <serviceAllowed>false</serviceAllowed> " + 
		 " " + 
		 "         <roamingAllowed>false</roamingAllowed> " + 
		 " " + 
		 "         <serviceType>serviceTypeId=0,OU=SERVICE,OU=UMA,NE=MOBILE_DATA_SERVER</serviceType> " + 
		 " " + 
		 "         <name>8500</name> " + 
		 " " + 
		 "</uma> " + 
		 " " + 
		 "</object> " + 
		 " " + 
		 "</request> " + 
		 " " + 
		 "  " + 
		 " " + 
		 "<request newGenerated=\"true\" xsi:type=\"spml:AddRequest\"> " + 
		 " " + 
		 "<version>SUBSCRIBER_v20</version> " + 
		 " " + 
		 "<object xsi:type=\"subscriber:Subscriber\"> " + 
		 " " + 
		 "<auc> " + 
		 " " + 
		 "               <imsi>129220425011111</imsi> " + 
		 " " + 
		 "               <encKey>01234567890123456789012345678901</encKey> " + 
		 " " + 
		 "               <iccId>866</iccId> " + 
		 " " + 
		 "               <algoId>9</algoId> " + 
		 " " + 
		 "               <kdbId>50</kdbId> " + 
		 " " + 
		 "            </auc> " + 
		 " " + 
		 "            <hlr> " + 
		 " " + 
		 "               <ntype>single</ntype> " + 
		 " " + 
		 "               <osb1>true</osb1> " + 
		 " " + 
		 "               <rr>VOICESTR</rr> " + 
		 " " + 
		 "               <ts11> " + 
		 " " + 
		 "                 <msisdn>1292204250</msisdn> " + 
		 " " + 
		 "               </ts11> " + 
		 " " + 
		 "               <gprs> " + 
		 " " + 
		 "                  <msisdn>1292204250</msisdn> " + 
		 " " + 
		 "               </gprs> " + 
		 " " + 
		 "            </hlr> " + 
		 " " + 
		 "<uma> " + 
		 " " + 
		 "         <imsi>129220425011111</imsi> " + 
		 " " + 
		 "         <msisdn>1292204250</msisdn> " + 
		 " " + 
		 "         <serviceAllowed>false</serviceAllowed> " + 
		 " " + 
		 "         <roamingAllowed>false</roamingAllowed> " + 
		 " " + 
		 "         <serviceType>serviceTypeId=0,OU=SERVICE,OU=UMA,NE=MOBILE_DATA_SERVER</serviceType> " + 
		 " " + 
		 "         <name>8500</name> " + 
		 " " + 
		 "</uma> " + 
		 " " + 
		 "</object> " + 
		 " " + 
		 "</request> " + 
		 " " + 
		 "  " + 
		 " " + 
		 "</spml:batchRequest> " + 
		 " " + 
		 "   </soapenv:Body> " + 
		 " " + 
		 "</soapenv:Envelope> ";
		
		/*requestPreFix="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\"> " + 
				 "  <soapenv:Header/> " + 
				 "  <soapenv:Body> " + 
				 "    <spml:batchRequest execution=\"synchronous\" " + 
				 " " + 
				 "        processing=\"sequential\" " + 
				 " " + 
				 "        onError=\"exit_commit\" " + 
				 " " + 
				 "        xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" " + 
				 " " + 
				 "        xmlns:subscriber=\"urn:siemens:names:prov:gw:SUBSCRIBER:2:0\" " + 
				 " " + 
				 "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> " + 
				 "      <version>SUBSCRIBER_v20</version> ";
		
		requestPostFix= "    </spml:batchRequest> " + 
				 "  </soapenv:Body> " + 
				 "</soapenv:Envelope> ";*/
		
			
		
		 System.out.println("request---------"+requestPreFix);
		
		try
		{
		String stringUrl = "http://"+Constants.CNTB_StringUrl+"/ProvisioningGateway/services/SPMLSubscriber20Service";
		URL url = new URL(stringUrl);
		URLConnection uc = url.openConnection();
	
		uc.addRequestProperty("Accept-Encoding", "gzip,deflate");
		uc.addRequestProperty("Connection", " keep-alive");
		uc.addRequestProperty("charset", "utf-8");
		//uc.addRequestProperty("User-Agent", "curl/7.12.1 (x86_64-redhat-linux-gnu) libcurl/7.12.1 OpenSSL/0.9.7a zlib/1.2.1.2 libidn/0.5.6");
		uc.addRequestProperty("Content-Type", "text/xml");
		uc.addRequestProperty("Host", Constants.CNTB_StringUrl);
		uc.addRequestProperty("SOAPAction",
				"urn:siemens:names:prov:gw:SPML:2:0/addRequest");
		uc.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
		String errorCode="";
		String errorType="";
		wr.writeBytes(requestPreFix);

		wr.flush();
		wr.close();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
		String decodedString;
		StringBuffer sb = new StringBuffer();
		while ((decodedString = in.readLine()) != null) {
			sb.append(decodedString);
			}
		
		
		dataMap=(Map)parseXmltoJavaForBatchAdd(sb.toString());
			
		try
		{
			if(dataMap.get("errorType")!=null)
			{
				errorType=(String)dataMap.get("errorType");
			}
			
			if(dataMap.get("errorCode")!=null)
			{
				errorCode=(String)dataMap.get("errorCode");
			}
			
			if(dataMap.get("message")!=null)
			{
				message=(String)dataMap.get("message");
			}
				
		
			if(errorType.equalsIgnoreCase("error"))
			{
				cntbdbDataResponse.setMessage(message+"  Error Code--"+errorCode);
			}else
			{
				message="record saved successfully.";
				
			}
			
		}catch(Exception e)
		{
			
			message="some problem occured pls try again.";
		}
		
		
	
		in.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
		
		dataMap.put("message", message);
		return dataMap;

	}


public Map<String, String> parseXmltoJavaForBatchAdd(String xml)
{
	 Map<String, String> dataMap = new HashMap<String, String>();
	 String errorCode="";
	 String errorType="";
	 String message="";
	 try{
		  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		  logger.info("Enter with xml");
		  DocumentBuilder db = dbf.newDocumentBuilder();
		  xml=xml.substring(xml.indexOf("<?xml"));
		  logger.info("Enter with xml"+xml);
		  InputSource is = new InputSource(new StringReader(xml));
		  Document doc = db.parse(is);
		  doc.getDocumentElement().normalize();
		  logger.info("Root element " + doc.getDocumentElement().getNodeName());
		  NodeList nodeLst = doc.getElementsByTagName("errorMessage");
		  NodeList addResponse = doc.getElementsByTagName("spml:batchResponse");
		  logger.info("Information of all member"+nodeLst.getLength());
		  for (int i = 0; i < nodeLst.getLength(); i++) {
		      Node aNode = nodeLst.item(i);
		      Node addResponseNode=addResponse.item(i);
		    

		      NamedNodeMap attributes = addResponseNode.getAttributes();

		      for (int a = 0; a < attributes.getLength(); a++) {
		        Node theAttribute = attributes.item(a);
		        if(theAttribute.getNodeName()=="errorCode")
		        {
		        	errorCode=theAttribute.getNodeValue();
		        }
		        if(theAttribute.getNodeName()=="errorType")
		        {
		        	errorType=theAttribute.getNodeValue();
		        	message=aNode.getFirstChild().getNodeValue();
		        	
		        }
		       
		      }
		      
		    }
		  
		 
		    
		  } catch (Exception e) {
			  logger.info("parseXmltoJava Exception : "+e.getMessage());
		    e.printStackTrace();
		  }
	 
	 dataMap.put("errorCode",errorCode );
	  dataMap.put("errorType",errorType );
	  dataMap.put("message",message );
	return dataMap;
}


public Map<String, String> parseXmltoJavaForBatchDelete(String xml)
{
	 Map<String, String> dataMap = new HashMap<String, String>();
	 String errorCode="";
	 String errorType="";
	 String message="";
	 try{
		  DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		  logger.info("Enter with xml");
		  DocumentBuilder db = dbf.newDocumentBuilder();
		  xml=xml.substring(xml.indexOf("<?xml"));
		  logger.info("Enter with xml"+xml);
		  InputSource is = new InputSource(new StringReader(xml));
		  Document doc = db.parse(is);
		  doc.getDocumentElement().normalize();
		  logger.info("Root element " + doc.getDocumentElement().getNodeName());
		  NodeList nodeLst = doc.getElementsByTagName("errorMessage");
		  NodeList addResponse = doc.getElementsByTagName("spml:batchResponse");
		  logger.info("Information of all member"+nodeLst.getLength());
		  for (int i = 0; i < nodeLst.getLength(); i++) {
		      Node aNode = nodeLst.item(i);
		      Node addResponseNode=addResponse.item(i);
		    

		      NamedNodeMap attributes = addResponseNode.getAttributes();

		      for (int a = 0; a < attributes.getLength(); a++) {
		        Node theAttribute = attributes.item(a);
		        if(theAttribute.getNodeName()=="errorCode")
		        {
		        	errorCode=theAttribute.getNodeValue();
		        }
		        if(theAttribute.getNodeName()=="errorType")
		        {
		        	errorType=theAttribute.getNodeValue();
		        	message=aNode.getFirstChild().getNodeValue();
		        	
		        }
		       
		      }
		      
		    }
		  
		 
		    
		  } catch (Exception e) {
			  logger.info("parseXmltoJava Exception : "+e.getMessage());
		    e.printStackTrace();
		  }
	 
	 dataMap.put("errorCode",errorCode );
	  dataMap.put("errorType",errorType );
	  dataMap.put("message",message );
	return dataMap;
}


public Map < String, Object > parseXmltoJava(String xml) {
	Map < String, Object > dataMap = new HashMap < String, Object > ();
	String errorCode = "";
	String errorType = "";
	String message = "";
	System.out.println("request xml-----"+xml);
	try {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		logger.info("Enter with xml");
		DocumentBuilder db = dbf.newDocumentBuilder();
		xml = xml.substring(xml.indexOf("<?xml"));
		System.out.println("XML-------------"+xml);
		logger.info("Enter with xml" + xml);
		InputSource is = new InputSource(new StringReader(xml));
		Document doc = db.parse(is);
		System.out.println("doc-------------"+doc);
		doc.getDocumentElement().normalize();
		
		logger.info("Root element " + doc.getDocumentElement().getNodeName());
		NodeList nodeLst = doc.getElementsByTagName("errorMessage");
		NodeList addResponse = doc.getElementsByTagName("spml:addResponse");
		logger.info("Information of all member" + nodeLst.getLength());
		for (int i = 0; i < nodeLst.getLength(); i++) {
			Node aNode = nodeLst.item(i);
			Node addResponseNode = addResponse.item(i);


			NamedNodeMap attributes = addResponseNode.getAttributes();

			for (int a = 0; a < attributes.getLength(); a++) {
				Node theAttribute = attributes.item(a);
				if (theAttribute.getNodeName() == "errorCode") {
					errorCode = theAttribute.getNodeValue();
				}
				if (theAttribute.getNodeName() == "errorType") {
					errorType = theAttribute.getNodeValue();
					message = aNode.getFirstChild().getNodeValue();

				}

			}

		}

		dataMap.put("errorCode", errorCode);
		dataMap.put("errorType", errorType);
		dataMap.put("message", message);

	} catch (Exception e) {
		logger.info("parseXmltoJava Exception : " + e.getMessage());
		e.printStackTrace();
	}
	return dataMap;
}
public static void main(String[] args) {
	// NAP
	CNTBDBDataHandler handler = new CNTBDBDataHandler();
	// handler.doMSISDNCSearch("14254444604");//msisdn
	// handler.doIMSICSearch("310260540571787");// imsi
	// handler.cntbSave("14254444604","");
	// handler.test();
	// handler.cntbSave("","310260540571787");
	//handler.cntbSave("1292204294", "129220429412456");
	handler.cnbtBatchAdd("1292204294", "129220429412456");
	// handler.getAttributeListForMSISDAndIMSI("d23513b0-74f1-42f4-b763-8d91ff27");//UID
	// handler.doNAPSearch(Constants.SEARCH_TYPE_IMSI, "310260540571787");
	// handler.doNAPSearch(Constants.SEARCH_TYPE_MSISDN, "14254444604");
	// handler.doNAPSearch(Constants.SEARCH_TYPE_UID,"d23513b0-74f1-42f4-b763-8d91ff27");//no

	// DUS
	// handler.doDUSSearch(Constants.SEARCH_TYPE_MSISDN, "14254444604");

	// PCRF
	// handler.doPCRFSearch(Constants.SEARCH_TYPE_MSISDN, "14254444604");

	// PCRF SESSION
	// handler.getPCRFSessionInfo("14254444604"); // 14254444604

	// UMA
	// handler.doUMASearch(Constants.SEARCH_TYPE_UID,"d23513b0-74f1-42f4-b763-8d91ff27");

	// HSS
	// handler.doHSSSearch(Constants.SEARCH_TYPE_UID,"2418136f-dfa4-4c42-a3c5-f33b85a3");

	// HLR
	// handler.doHLRSearch(Constants.SEARCH_TYPE_UID,"235e6951-f201-4f7f-a552-a8d99ed4");

	// EIR
	// handler.doEIRSearch(Constants.SEARCH_TYPE_UID,"2418136f-dfa4-4c42-a3c5-f33b85a3");

	// (this can be tested after deployment )

	// ldapsearch -x -h 10.25.72.247 -p 16800 -D "cn=napprovuser" -w siemens
	// -b
	// uid=d17a6ffb-3a9d-4614-a283-d1345382,ds=SUBSCRIBER,o=DEFAULT,dc=C-NTDB
	// -s subtree objectclass=*
	// ldapsearch -x -h 10.25.72.247 -p 16800 -D cn=napProvUser -w siemens
	// -b imsi=310260666666666,dc=imsi,dc=c-ntdb -s subtree objectclass=*
	// ldapsearch -h 10.25.72.247 -p 16800 -x -D "cn=napProvUser" -w siemens
	// -b "msisdn=168405666666666,dc=msisdn,dc=c-ntdb"
}

public Map<String, Object> cnbtBatchAdd(Map<String, Object> dataMap) {
	// TODO Auto-generated method stub
	return null;
}




}
