package com.tmobile.edat.cntb.repository;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;

public class CNTBDataHandler {

	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";
	private static String SECURITY_PRINCIPAL_NAP = "cn=napProvUser";
	private static String SECURITY_CREDENTIALS_NAP = "siemens";
	private static String PROVIDER_URL_CNTB = "ldap://"+Constants.LDAP_SERVER_CNTB+"/dc=%s,dc=C-NTDB";
	String SEARCH_QUERY = "%s=%s";
	String SEARCH_FILTER = "(objectclass=%s)";

	public String doCNTBSearch(int search_type, String key) {
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";

		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:

			break;
		case Constants.SEARCH_TYPE_IMSI:
			providerURL = String.format(PROVIDER_URL_CNTB, "imsi");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = { "uid", "imsi", };
			sc.setReturningAttributes(attrIDs);
			query = String.format(SEARCH_QUERY, "imsi", key);
			filter = String.format(SEARCH_FILTER, "*");
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			providerURL = String.format(PROVIDER_URL_CNTB, "CustomerId");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			
			query = String.format(SEARCH_QUERY, "CustomerId", key);
			filter = String.format(SEARCH_FILTER, "subscriberSubdata");
			break;
		case Constants.SEARCH_TYPE_UID:
			providerURL = String.format(PROVIDER_URL_CNTB, "SUBSCRIBER");
			System.out.println("providerURL :" + providerURL);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			
			query = String.format(SEARCH_QUERY, "uid", key);
			filter = String.format(SEARCH_FILTER, "*");
			break;

		default:
			break;
		}
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_NAP, SECURITY_CREDENTIALS_NAP, providerURL));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {

				System.out.println("CNTB search query :" + query + "\nfilter :"
						+ filter);
				NamingEnumeration<SearchResult> result = ctx.search(query,
						filter, sc);
				System.out.println("CNTB search result :"
						+ result.hasMoreElements());
				while (result != null && result.hasMoreElements()) {
					SearchResult rs = result.next();
					System.out.println("CNTB search data :" + rs.toString());
				}
			} catch (NamingException e) {

				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static void main(String[] args) {
		CNTBDataHandler handler = new CNTBDataHandler();

		handler.doCNTBSearch(Constants.SEARCH_TYPE_CUSTID, "4254444604");
		// handler.doCNTBSearch(Contstants.SEARCH_TYPE_IMSI, "310260540571787");
		// handler.doCNTBSearch(Contstants.SEARCH_TYPE_UID,
		// "d23513b0-74f1-42f4-b763-8d91ff27");
	}
}
