package com.tmobile.edat.cntb.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.ldap.LdapName;

import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.edat.dusupdate.dto.Counter;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.usd.dto.response.EpsData;
import com.tmobile.edat.usd.dto.response.GprsData;
import com.tmobile.edat.usd.dto.response.HlrDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.usd.dto.response.Partners;
import com.tmobile.edat.usd.dto.response.Profile;
import com.tmobile.edat.usd.dto.response.Socs;

public class CNTBDBDataRepositoryHelper {

	// Help Method
	// // UID will not come in NAP search, this will come when you do the DUS
	// search,we need to do the dus search to and update the UID here.
	public static void fillInNapProfileData(NapDataResponse napDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {

		Profile profile = new Profile();

		if (attrs != null) {

			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					String id = atyrs.next().toString();
					if (id.equalsIgnoreCase("msisdn")) {
						profile.setMsisdn(attrs.get(id).get().toString());
					}
					if (id.equalsIgnoreCase("customerId")) {
						profile.setCustomerId(attrs.get(id).get().toString());
					}
					if (id.equalsIgnoreCase("imsi")) {
						profile.setImsi(attrs.get(id).get().toString());
					} else {
						attributes.put(id, attrs.get(id).get().toString());
						/*
						 * System.out.println("Attr : " + id + " - " +
						 * attrs.get(id));
						 */
					}
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}
			profile.setAttributes(attributes);
			// Finally set Profile into napDataResponse
			napDataResponse.setProfile(profile);
		}

	}

	// Fill Partner
	public static void fillInNapPartnersData(NapDataResponse napDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {
		Partners partner = new Partners();
		String id = null;
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					id = atyrs.next().toString();

					if (id.equalsIgnoreCase("partnerId")) {
						partner.setPartnerId(attrs.get(id).get().toString());
					} else {
						attributes.put(id, attrs.get(id).get().toString());
					}
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}
			partner.setAttributes(attributes);
			// Finally add partner into list
			napDataResponse.getPartnersList().add(partner);
		}

	}

	// Fill SOCs
	public static void fillInNapSOCData(NapDataResponse napDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {
		Socs socs = new Socs();
		String id = null;
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					id = atyrs.next().toString();
					if (id.equalsIgnoreCase("socCode")) {
						socs.setSocCode(attrs.get(id).get().toString());
					} else {
						attributes.put(id, attrs.get(id).get().toString());
					}
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}
			socs.setAttributes(attributes);
			// Finally add SOCs into list
			napDataResponse.getSocsList().add(socs);
		}

	}
	
	
	
	public static List<String> fillUIDForCNTBDB(LdapName dn, Attributes attrs,List<String>attributeList,Attribute attribute) throws NamingException {
	
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					String id = atyrs.next().toString();
					
					if(attribute.contains("subscriber"))
					{
					if (id.equalsIgnoreCase("uid")) {
						
						attributeList.add(attrs.get(id).get().toString());
					}
					}
					if(attribute.contains("ACSUBDATA"))
					{
					if (id.equalsIgnoreCase("imsi")) {
						
						attributeList.add(attrs.get(id).get().toString());
					}
					}
					if(attribute.contains("MSISDNINNSS") || attribute.contains("umaSubscriberData"))
					{
					if (id.equalsIgnoreCase("msisdn")) {
						
						attributeList.add(attrs.get(id).get().toString());
					}
					}
					
				
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
				
				
			}
			
			
			
			
		}
		
		
		
		return attributeList;
	}
	// Fill DUSData
	public static String getUID(LdapName dn, Attributes attrs) throws NamingException {
		String uid="";	
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					String id = atyrs.next().toString();
					
					if (id.equalsIgnoreCase("uid")) {
						
						uid=	attrs.get(id).get().toString();
					}
					
				
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
				
				
			}
			
			
			
			
		}
		return uid;

	}

	// Fill DUSCounterData
	public static void fillInDUSCounterData(DusDataResponse dusDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {
		Counter counter = new Counter();
		String id = null;
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = new HashMap<String, String>();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					id = atyrs.next().toString();
					if (id.equalsIgnoreCase("amfCounterId")) {
						counter.setCounterId(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("amfCounterValue")) {
						counter.setCounterValue(attrs.get(id).get().toString());
					} else {
						attributes.put(id, attrs.get(id).get().toString());

					}
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}
			counter.setAttributes(attributes);
			dusDataResponse.addCounters(counter);
			// Finally add Counter into list
		}

	}

	// Fill HLR INFO
	public static void fillInHLRData(HlrDataResponse hlrDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {

		String id = null;
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			Map<String, String> attributes = hlrDataResponse.getAttributes();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					id = atyrs.next().toString();

					attributes.put(id, attrs.get(id).get().toString());

				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}

			hlrDataResponse.setAttributes(attributes);
			// Finally add Counter into list
		}

	}

	// Fill fillGprsData
	/*public static void fillGprsData(HlrDataResponse hlrDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {
		GprsData data = new GprsData();
		String id = null;
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					id = atyrs.next().toString();
					if (id.equalsIgnoreCase("pdpContextId")) {
						data.setPdpContextId(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("pdpType")) {
						data.setPdpType(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("accPointName")) {
						data.setAccPointName(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("refqOfServName")) {
						data.setRefqOfServName(attrs.get(id).get().toString());
					}
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}
			hlrDataResponse.addGprsData(data);
		}
	}
*/
	// Fill fillEPSData
/*	public static void fillEpsData(HlrDataResponse hlrDataResponse,
			LdapName dn, Attributes attrs) throws NamingException {
		EpsData data = new EpsData();
		String id = null;
		if (attrs != null) {
			NamingEnumeration<String> atyrs = attrs.getIDs();
			while (atyrs != null && atyrs.hasMoreElements()) {
				try {
					id = atyrs.next().toString();
					if (id.equalsIgnoreCase("pdnContextId")) {
						data.setPdnContextId(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("pdnType")) {
						data.setPdnType(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("pdnAccPointName")) {
						data.setPdnAccPointName(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("mmeIdentity")) {
						data.setMmeIdentity(attrs.get(id).get().toString());
					} else if (id.equalsIgnoreCase("refLteQOfServName")) {
						data.setRefLteQOfServName(attrs.get(id).get()
								.toString());
					} else if (id.equalsIgnoreCase("vplmnAddressAllowed")) {
						data.setVplmnAddressAllowed(attrs.get(id).get()
								.toString());
					}
				} catch (Exception e) {
					System.out.println("outor : " + e.toString());
				}
			}
			hlrDataResponse.addEpsData(data);
		}

	}*/

}
