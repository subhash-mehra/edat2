package com.tmobile.edat.cntb.repository;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.tmobile.edat.cntb.request.CNTBDBSearchRequest;
import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.session.SessionConstants;

public interface CNTBDBDataRepository {
	
	
	public CNTBDBDataResponse doMSISDNCSearch(String msisdn, SessionConstants sessionConstants);
	public CNTBDBDataResponse doIMSICSearch(String imsi, SessionConstants sessionConstants);
	public CNTBDBDataResponse cntbSave(CNTBDBSearchRequest cntbdbSearchRequest);
	public CNTBDBDataResponse cntbDelete(CNTBDBSearchRequest cntbdbSearchRequest);
	public CNTBDBDataResponse cnbtExistForAdd(CNTBDBSearchRequest cntbdbSearchRequest);
	public CNTBDBDataResponse cnbtExistForDelete(CNTBDBSearchRequest cntbdbSearchRequest);
	public Map<String,Object> cntbBatchDelete(List<String> msisdnList,List<String> imsiList, SessionConstants sessionConstants);
	public Map<String,Object> cnbtBatchSearch(List<String> msisdnList,List<String> imsiList, SessionConstants sessionConstants);
	public Map<String,Object> cnbtBatchAdd(Map<String,Object> dataMap, SessionConstants sessionConstants);
	
	
}
