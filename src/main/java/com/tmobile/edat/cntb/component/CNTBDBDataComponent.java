package com.tmobile.edat.cntb.component;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.tmobile.edat.base.BaseComponent;
import com.tmobile.edat.cntb.request.CNTBDBSearchRequest;
import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.edat.messaging.dto.request.MessagingSearchRequest;
import com.tmobile.edat.messaging.dto.response.MessagingDataResponse;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.session.SessionConstants;

public interface CNTBDBDataComponent extends BaseComponent{
	
	
    public CNTBDBDataResponse getMSISDNSearch(CNTBDBSearchRequest cntbdbSearchRequest);
    public CNTBDBDataResponse cntbSave(CNTBDBSearchRequest cntbdbSearchRequest);
    public CNTBDBDataResponse cntbDelete(CNTBDBSearchRequest cntbdbSearchRequest);
    public CNTBDBDataResponse cnbtExistForAdd(CNTBDBSearchRequest cntbdbSearchRequest);
    public CNTBDBDataResponse cnbtExistForDelete(CNTBDBSearchRequest cntbdbSearchRequest);
    public Map<String,Object> cntbBatchDelete(MultipartFile file,String realPath, SessionConstants sessionConstants);
    public Map<String,Object> cnbtBatchSearch(MultipartFile file,String realPath, SessionConstants sessionConstants);
    public Map<String,Object> cnbtBatchAdd(MultipartFile file,String realPath, SessionConstants sessionConstants);
    
    
    
  
  
}
