package com.tmobile.edat.cntb.component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.csvreader.CsvReader;
import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.cntb.repository.CNTBDBDataRepository;
import com.tmobile.edat.cntb.request.CNTBDBSearchRequest;
import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.session.SessionConstants;

@Component("CNTBDBDataComponent")
public class CNTBDBDataComponentImpl implements CNTBDBDataComponent {

	private static final Logger logger = Logger
			.getLogger(CNTBDBDataComponentImpl.class);

	@Autowired
	@Qualifier("CNTBDBDataRepository")
	CNTBDBDataRepository cntbdbDataRepository = null;

	public CNTBDBDataComponentImpl() {
	}

	@Override
	public boolean validateRequest(BaseRequest baseRequest,
			BaseResponse baseResponse, int key) {

		// Applicable to all methods
		if (baseRequest == null) {
			baseResponse = new NapDataResponse();
			baseResponse.setApplicationError(new ApplicationError(
					ApplicationConstants.NULL_REQUEST));
			return false;
		}

		return true;
	}

	@Override
	public CNTBDBDataResponse getMSISDNSearch(
			CNTBDBSearchRequest cntbdbSearchRequest) {

		logger.info("Searching in getMSISDNearch with  := "
				+ cntbdbSearchRequest.getMsisdn());

		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		CNTBDBDataResponse cntbdbDataMsisdnResponse = new CNTBDBDataResponse();
		CNTBDBDataResponse cntbdbDataImsiResponse = new CNTBDBDataResponse();
		List<String> MSISDNList = new ArrayList<String>();
		List<String> IMSIList = new ArrayList<String>();

		if (this.validateRequest(cntbdbSearchRequest, cntbdbDataResponse, 0)) {
			if (cntbdbSearchRequest.getMsisdn() != null
					&& !cntbdbSearchRequest.getMsisdn().equals("")) {

				cntbdbDataMsisdnResponse = this.cntbdbDataRepository
						.doMSISDNCSearch(cntbdbSearchRequest.getMsisdn().trim(), cntbdbSearchRequest.getSessionConstants());
				cntbdbDataResponse.setMISSDNList(cntbdbDataMsisdnResponse.getMISSDNiList());
				cntbdbDataResponse.setApplicationError(cntbdbDataMsisdnResponse.getApplicationError());
				
			}

			if (cntbdbSearchRequest.getImsi() != null
					&& !cntbdbSearchRequest.getImsi().equals("")) {

				cntbdbDataImsiResponse = this.cntbdbDataRepository
						.doIMSICSearch(cntbdbSearchRequest.getImsi(), cntbdbSearchRequest.getSessionConstants());
				cntbdbDataResponse.setIMSIList(cntbdbDataImsiResponse.getIMSIiList());
				cntbdbDataResponse.setApplicationError(cntbdbDataImsiResponse.getApplicationError());
			}
			
			
			
		}
		return cntbdbDataResponse;
	}

	@Override
	public CNTBDBDataResponse cntbSave(CNTBDBSearchRequest cntbdbSearchRequest) {

		logger.info("Searching in getMSISDNearch with  := "
				+ cntbdbSearchRequest.getMsisdn());

		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
			if (this.validateRequest(cntbdbSearchRequest, cntbdbDataResponse, 0)) {
			cntbdbDataResponse = this.cntbdbDataRepository
					.cntbSave(cntbdbSearchRequest);

		}
		return cntbdbDataResponse;
	}

	@Override
	public CNTBDBDataResponse cntbDelete(CNTBDBSearchRequest cntbdbSearchRequest) {

		logger.info("Searching in getMSISDNearch with  := "
				+ cntbdbSearchRequest.getMsisdn());

		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
			if (this.validateRequest(cntbdbSearchRequest, cntbdbDataResponse, 0)) {
			cntbdbDataResponse = this.cntbdbDataRepository
					.cntbDelete(cntbdbSearchRequest);

		}
		return cntbdbDataResponse;
	}

	@Override
	public CNTBDBDataResponse cnbtExistForAdd(
			CNTBDBSearchRequest cntbdbSearchRequest) {

		logger.info("Searching in getMSISDNearch with  := "
				+ cntbdbSearchRequest.getMsisdn());

		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		
		if (this.validateRequest(cntbdbSearchRequest, cntbdbDataResponse, 0)) {
			cntbdbDataResponse = this.cntbdbDataRepository
					.cnbtExistForAdd(cntbdbSearchRequest);

		}
		return cntbdbDataResponse;
	}

	@Override
	public CNTBDBDataResponse cnbtExistForDelete(
			CNTBDBSearchRequest cntbdbSearchRequest) {

		logger.info("Searching in getMSISDNearch with  := "
				+ cntbdbSearchRequest.getMsisdn());

		CNTBDBDataResponse cntbdbDataResponse = new CNTBDBDataResponse();
		
		if (this.validateRequest(cntbdbSearchRequest, cntbdbDataResponse, 0)) {
			cntbdbDataResponse = this.cntbdbDataRepository
					.cnbtExistForDelete(cntbdbSearchRequest);

		}
		return cntbdbDataResponse;
	}

	@Override
	public Map<String, Object> cntbBatchDelete(MultipartFile file,
			String realPath, SessionConstants sessionConstants) {
		logger.info("cntbBatchDelete calling---------");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String fileName = file.getOriginalFilename();
		BufferedReader br = null;
		List<String> msisdnList = new ArrayList<String>();
		List<String> imsiList = new ArrayList<String>();

		try {
				inputStream = file.getInputStream();
				System.out.println(realPath);
				File newFile = new File(realPath + fileName);
				if (!newFile.exists()) {
					newFile.createNewFile();
				}
				outputStream = new FileOutputStream(newFile);
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = inputStream.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}

			CsvReader products = new CsvReader(realPath + fileName);

			products.readHeaders();

			while (products.readRecord()) {
				String msisdn = products.get("msisdn");
				String imsi = products.get("imsi");
				if(msisdn!="")
				{
					if(msisdn.length()!=10 && msisdn.length()!=14 && msisdn.length()!=15)
					{
						
					}else
					{
						msisdn=1+""+msisdn;
					}
				}
				msisdnList.add(msisdn);
				imsiList.add(imsi);

			}

			products.close();
		} catch (Exception e) {
			//e.printStackTrace();
			logger.info(" cntbBatchDelete Exception :: "+e.getMessage());
		}

		dataMap = cntbdbDataRepository.cntbBatchDelete(msisdnList, imsiList,sessionConstants);

		return dataMap;
	}

	@Override
	public Map<String, Object> cnbtBatchSearch(MultipartFile file,
			String realPath, SessionConstants sessionConstants) {
		logger.info("cntbBatchDelete calling---------");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String fileName = file.getOriginalFilename();
		List<String> msisdnList = new ArrayList<String>();
		List<String> imsiList = new ArrayList<String>();

		try {
			inputStream = file.getInputStream();
			System.out.println(realPath);
			File newFile = new File(realPath + fileName);
			if (!newFile.exists()) {
				newFile.createNewFile();
			}
			outputStream = new FileOutputStream(newFile);
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			CsvReader products = new CsvReader(realPath + fileName);

			products.readHeaders();

			while (products.readRecord()) {
				String msisdn = products.get("msisdn");
				String imsi = products.get("imsi");
				if(msisdn!="")
				{
					if(msisdn.length()!=10 && msisdn.length()!=14 && msisdn.length()!=15)
					{
						
					}else
					{
						msisdn=1+""+msisdn;
					}
				}
				msisdnList.add(msisdn);
				imsiList.add(imsi);

			}

			products.close();

		} catch (Exception e) {
			//e.printStackTrace();
			logger.info(" In cnbtBatchSearch Exception :: "+e.getMessage());
		}

		dataMap = cntbdbDataRepository.cnbtBatchSearch(msisdnList, imsiList, sessionConstants);

		return dataMap;
	}

	@Override
	public Map<String, Object> cnbtBatchAdd(MultipartFile file, String realPath, SessionConstants sessionConstants) {
		logger.info("cntbBatchDelete calling---------");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		Map<String, Object> listDataMap = new HashMap<String, Object>();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String custId="";
		String fileName = file.getOriginalFilename();
		List<String> msisdnList = new ArrayList<String>();
		List<String> imsiList = new ArrayList<String>();
		List<String> encKeyList = new ArrayList<String>();
		List<String> accIdList = new ArrayList<String>();
		List<String> alogIdList = new ArrayList<String>();
		List<String> kdbIdList = new ArrayList<String>();
		List<String> custIdList = new ArrayList<String>();

		try {
			inputStream = file.getInputStream();
			System.out.println(realPath);
			File newFile = new File(realPath + fileName);
			if (!newFile.exists()) {
				newFile.createNewFile();
			}
			outputStream = new FileOutputStream(newFile);
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			CsvReader products = new CsvReader(realPath + fileName);

			products.readHeaders();

			while (products.readRecord()) {
				String msisdn = products.get("msisdn");
				String imsi = products.get("imsi");
				/*String encKey = products.get("encKey");
				String accId = products.get("accId");
				String alogId = products.get("alogId");
				String kdbId = products.get("kdbId");*/
				if(products.get("operatorId")!="" && products.get("operatorId")!=null)
				{
				custId = products.get("operatorId");
				}
				if(msisdn!="")
				{
					if(msisdn.length()!=10 && msisdn.length()!=14 && msisdn.length()!=15)
					{
						
					}else
					{
						msisdn=1+""+msisdn;
					}
				}
				msisdnList.add(msisdn);
				imsiList.add(imsi);
				encKeyList.add("");
				accIdList.add("");
				alogIdList.add("");
				kdbIdList.add("");
				custIdList.add(custId);

			}

			products.close();

		} catch (Exception e) {
			//e.printStackTrace();
			logger.info(" In cnbtBatchAdd Exception :: "+e.getMessage());
		}

		listDataMap.put("msisdnList", msisdnList);
		listDataMap.put("imsiList", imsiList);
		listDataMap.put("encKeyList", encKeyList);
		listDataMap.put("accIdList", accIdList);
		listDataMap.put("alogIdList", alogIdList);
		listDataMap.put("kdbIdList", kdbIdList);
		listDataMap.put("custIdList", custIdList);

		dataMap = cntbdbDataRepository.cnbtBatchAdd(listDataMap,sessionConstants);

		return dataMap;
	}

}