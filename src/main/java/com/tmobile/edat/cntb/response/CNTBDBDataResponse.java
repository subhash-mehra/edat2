package com.tmobile.edat.cntb.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.dusupdate.dto.Counter;
import com.tmobile.edat.usd.dto.response.Profile;
import com.tmobile.edat.usd.dto.response.Socs;

public class CNTBDBDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    
	private String uid;
	private String msisdn;
	private String imsi;
	private String message;
	private String failurCount;
	private String successCount;
	private String failureMessage;
	
	

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}



	// Map store all attributes and values;
    protected Map<String, String> attributes;
    List<String>imsiList=new ArrayList<String>();
    List<String>missdnList=new ArrayList<String>();
    
    
   public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	 public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	
	public List<String> getIMSIiList() {
		return imsiList;
	}

	public void setIMSIList(List<String> imsiList) {
		this.imsiList = imsiList;
	}

	public List<String> getMISSDNiList() {
		return missdnList;
	}

	public void setMISSDNList(List<String> missdnList) {
		this.missdnList = missdnList;
	}


	
	public Map<String, String> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	
	
	
	public String getFailurCount() {
		return failurCount;
	}
	public void setFailurCount(String failurCount) {
		this.failurCount = failurCount;
	}
	public String getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(String successCount) {
		this.successCount = successCount;
	}
	public String getFailureMessage() {
		return failureMessage;
	}
	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}
	public String toString() {
		return "CNTBDBResponse [missdnList="+missdnList+"imsiList="+imsiList+"message="+message
				+ "]";

}
	
}
