package com.tmobile.edat.cntb.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.session.SessionConstants;

public class CNTBDBSearchRequest extends BaseRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	
	public CNTBDBSearchRequest() {
		super();
	}

	protected String msisdn;
	protected String imsi;
	protected String encKey;
	protected String accId;
	protected String alogId;
	protected String kdbId;
	protected String custId;
	protected String acsub;
	protected SessionConstants sessionConstants;
	

	
	
	public String getAcsub() {
		return acsub;
	}

	public void setAcsub(String acsub) {
		this.acsub = acsub;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getEncKey() {
		return encKey;
	}

	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}

	public String getAccId() {
		return accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	public String getAlogId() {
		return alogId;
	}

	public void setAlogId(String alogId) {
		this.alogId = alogId;
	}

	public String getKdbId() {
		return kdbId;
	}

	public void setKdbId(String kdbId) {
		this.kdbId = kdbId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}

	

}
