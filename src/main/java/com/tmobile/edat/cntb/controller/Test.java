package com.tmobile.edat.cntb.controller;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tmobile.edat.util.Constants;

public class Test 
{
	public static String SERVER_MMSC = "10.169.53.152:8080";
	
	/*public static SOAPMessage createSOAPRequest(String msisdn) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://www.mavenir.com/sps/services/mmsc/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("GetSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("userInfo");
		soapBodyUserInfo.addChildElement("userId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("msisdn").addTextNode(msisdn);
		subscriber.addChildElement("sourceSystem").addTextNode("SwitchControl");

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");

		soapMessage.saveChanges();

		 Print the request message 
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println("soapMessage=================> "+soapMessage);

		return soapMessage;
	}
*/
	private Map < String, String > convertToJavaObjectForGetSubscriber(String xmlString) {
	
	Map < String, String > attributes = new HashMap < String, String > ();

	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	DocumentBuilder db;
	try {
		db = dbf.newDocumentBuilder();

		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(xmlString));

		Document doc = null;
		doc = db.parse(is);
		NodeList nodes = doc.getElementsByTagName("SubscriberDetails");
		NodeList subscriberDataNode = doc.getElementsByTagName("SubscriberData");
		// iterate the Subscriber data
		
		if (nodes.getLength() > 0) {
			for (int i = 0; i < nodes.getLength(); i++) {



				NodeList innerList = nodes.item(i).getChildNodes();

				for (int j = 0; j < innerList.getLength(); j++) {
					if (innerList.item(j).getNodeName().equalsIgnoreCase("MSISDN")) {
						attributes.put(innerList.item(j).getNodeName(), innerList.item(j).getTextContent());
					}

					
				}
			}

			for (int i = 0; i < subscriberDataNode.getLength(); i++) {
				NodeList innerList = subscriberDataNode.item(i).getChildNodes();

				for (int j = 0; j < innerList.getLength(); j++) {

					attributes.put(innerList.item(j).getNodeName(), innerList.item(j).getTextContent());
					
				}
			}
		} else {
			NodeList faultstringNode = doc.getElementsByTagName("faultstring");


			if (faultstringNode.getLength() > 0) {
				for (int i = 0; i < faultstringNode.getLength(); i++) {
					attributes.put(faultstringNode.item(i).getNodeName(), faultstringNode.item(i).getTextContent());

					NodeList validationErrorNode = doc.getElementsByTagName("ns3:ValidationError");
					NodeList innerList = validationErrorNode.item(i).getChildNodes();
					for (int j = 0; j < innerList.getLength(); j++) {
						if (innerList.item(j).getNodeName().equalsIgnoreCase("ns3:Code")) {
							attributes.put("Code", innerList.item(j).getTextContent());
						} else if (innerList.item(j).getNodeName().equalsIgnoreCase("ns3:Reason")) {
							attributes.put("Reason", innerList.item(j).getTextContent());
						} else {
							attributes.put(innerList.item(j).getNodeName(), innerList.item(j).getTextContent());
						}



						
					}
				}
			} else {
				attributes.put("success", "success");
			}
		}
		System.out.println("attributes-----" + attributes);

	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		
		e.printStackTrace();
	} catch (SAXException e) {
		
		e.printStackTrace();
	} catch (IOException e) {
		
		e.printStackTrace();
	}
	return attributes;
	}
	
public static void main(String[] args)
{

	try {
		// Create SOAP Connection
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
				.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory
				.createConnection();

		// Send SOAP Message to SOAP Server
		String url = "http://"
				+ Constants.SERVER_MMSC
				+ "/SPSSubscriberProvisioningService/SPSSubscriberProvisioningServiceImpl";
		
		
	
}catch(Exception e){e.printStackTrace();}
}
}
