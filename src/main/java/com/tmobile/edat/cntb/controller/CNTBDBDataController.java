package com.tmobile.edat.cntb.controller;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.tmobile.edat.cntb.component.CNTBDBDataComponent;
import com.tmobile.edat.cntb.request.CNTBDBSearchRequest;
import com.tmobile.edat.cntb.response.CNTBDBDataResponse;
import com.tmobile.session.SessionConstants;


@
Controller@RequestMapping("/")
/** Description of CNTBDBDataController 
*
* CNTBDBDataController class is used for CNTDB tool(CNTDB Search/CNTDB Add/CNTDB Delete).
* 
* 
*/
public class CNTBDBDataController {@
	Autowired@Qualifier("CNTBDBDataComponent")
	private CNTBDBDataComponent cntbdbDataComponent;

/**
*
* cntbDB() method is used for show CNTDB Search Page.
* 
*/
	@
	RequestMapping(value = "cntb", method = RequestMethod.GET)
	public String cntbDB() {
		return "/cntbdb/cntbSearch";
	}



	/**
	*
	* cntbDBAdd() method is used for show CNTDB Add Page.
	* 
	*/
	@
	RequestMapping(value = "cntbDBAdd", method = RequestMethod.GET)
	public String cntbDBAdd() {
		return "/cntbdb/cntbAdd";
	}


	/**
	*
	* cntbDBDelete() method is used for show CNTDB Delete Page.
	* 
	*/
	@
	RequestMapping(value = "cntbDBDelete", method = RequestMethod.GET)
	public String cntbDBDelete() {
		return "/cntbdb/cntbDelete";
	}


	/**
	*
	* cntbDBSearch() method is used for show CNTDB Search Page.
	* 
	*/
	@
	RequestMapping(value = "cntbDBSearch", method = RequestMethod.GET)
	public String cntbDBSearch()
	{
		return "/cntbdb/cntbSearch";
	}




	/**
	*
	* cnbtMsisdnResponse(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) method is used for msisdn exist or not.
	*  cntbdbSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user. 
	*/

	@
	RequestMapping(value = "msisdn", method = RequestMethod.POST,
	produces = MediaType.APPLICATION_JSON_VALUE,
	consumes = MediaType.APPLICATION_JSON_VALUE)@ResponseBody
	public CNTBDBDataResponse cnbtMsisdnResponse(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) {
		HttpServletRequest request;
		
		System.out.println("Inside tge metrhod............");
		cntbdbSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.cntbdbDataComponent.getMSISDNSearch(cntbdbSearchRequest);
	}

	/**
	*
	* cnbtSaveResponse(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) method is used for save msisdn.
	*  cntbdbSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user. 
	*/

	@
	RequestMapping(value = "save", method = RequestMethod.POST,
	produces = MediaType.APPLICATION_JSON_VALUE,
	consumes = MediaType.APPLICATION_JSON_VALUE)@ResponseBody
	public CNTBDBDataResponse cnbtSaveResponse(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		cntbdbSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.cntbdbDataComponent.cntbSave(cntbdbSearchRequest);
	}



	/**
	*
	* cnbtDeleteResponse(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) method is used for delete msisdn.
	*  cntbdbSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user. 
	*/

	@
	RequestMapping(value = "delete", method = RequestMethod.POST,
	produces = MediaType.APPLICATION_JSON_VALUE,
	consumes = MediaType.APPLICATION_JSON_VALUE)@ResponseBody
	public CNTBDBDataResponse cnbtDeleteResponse(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		cntbdbSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.cntbdbDataComponent.cntbDelete(cntbdbSearchRequest);
	}

	/**
	*
	* cnbtExistForAdd(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) method is used for check msisdn or imsi exist before add.
	*  cntbdbSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.  
	*/

	@
	RequestMapping(value = "existAdd", method = RequestMethod.POST,
	produces = MediaType.APPLICATION_JSON_VALUE,
	consumes = MediaType.APPLICATION_JSON_VALUE)@ResponseBody
	public CNTBDBDataResponse cnbtExistForAdd(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		cntbdbSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.cntbdbDataComponent.cnbtExistForAdd(cntbdbSearchRequest);
	}


	/**
	*
	* cnbtExistForDelete(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) method is used for check msisdn or imsi exist before delete.
	*  cntbdbSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.  
	*/

	@
	RequestMapping(value = "existDelete", method = RequestMethod.POST,
	produces = MediaType.APPLICATION_JSON_VALUE,
	consumes = MediaType.APPLICATION_JSON_VALUE)@ResponseBody
	public CNTBDBDataResponse cnbtExistForDelete(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		cntbdbSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
		return this.cntbdbDataComponent.cnbtExistForDelete(cntbdbSearchRequest);
	}

	/**
	*
	* showBatchDelete(@RequestBody CNTBDBSearchRequest cntbdbSearchRequest,HttpSession session) method is used for show BatchDelete.
	*  cntbdbSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user. 
	*/
	@
	RequestMapping(value = "showBatchDelete", method = RequestMethod.GET)
	public ModelAndView showBatchDelete() {
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		return new ModelAndView("cntbdb/batchDelete", "dataMap", dataMap);
	}

	/**
	*
	* cnbtBatchDelete(@RequestParam("file") MultipartFile file, HttpServletRequest request,HttpSession session) method is used for delete batch of msisdn or imsi.
	*  file parameter is used for takes .csv input from user.
   	*  session parameter is used for get current session of user. 
	*/		
	@
	RequestMapping(value = "batchDelete", method = RequestMethod.POST)@ResponseBody
	public ModelAndView cnbtBatchDelete(@RequestParam("file") MultipartFile file, HttpServletRequest request,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		
		String realPath = request.getSession().getServletContext().getRealPath("/");
		dataMap = cntbdbDataComponent.cntbBatchDelete(file, realPath,(SessionConstants)session.getAttribute("SessionConstants"));
		return new ModelAndView("cntbdb/cntbDelete", "dataMap", dataMap);
	}



	/**
	*
	* cnbtBatchSearch(@RequestParam("importData") MultipartFile file, HttpServletRequest request,HttpSession session) method is used for search batch of msisdn or imsi.
	*  file parameter is used for takes .csv input from user.
   	*  session parameter is used for get current session of user. 
	*/		
	@
	RequestMapping(value = "batchSearch", method = RequestMethod.POST)@ResponseBody
	public ModelAndView cnbtBatchSearch(@RequestParam("importData") MultipartFile file, HttpServletRequest request,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		String realPath = request.getSession().getServletContext().getRealPath("/");
		dataMap = cntbdbDataComponent.cnbtBatchSearch(file, realPath,(SessionConstants)session.getAttribute("SessionConstants"));
		dataMap.put("batchSearchFlag", "true");
		return new ModelAndView("cntbdb/cntbSearch", "dataMap", dataMap);
	}

	/**
	*
	* showBatchAdd() method is used for show  CNTDB Batch Add.
	* 
	*/		
	@
	RequestMapping(value = "showBatchAdd", method = RequestMethod.GET)
	public ModelAndView showBatchAdd() {
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		return new ModelAndView("cntbdb/batchAdd", "dataMap", dataMap);
	}



	/**
	*
	* showBatchAdd(@RequestParam("file") MultipartFile file, HttpServletRequest request,HttpSession session) method is used for add batch of msisdn or imsi.
	*  file parameter is used for takes .csv input from user.
   	*  session parameter is used for get current session of user. 
	*/	
	@
	RequestMapping(value = "batchAdd", method = RequestMethod.POST)@ResponseBody
	public ModelAndView cnbtBatchAdd(@RequestParam("file") MultipartFile file, HttpServletRequest request,HttpSession session) {
		System.out.println("Inside tge metrhod............");
		Map < String, Object > dataMap = new HashMap < String, Object > ();
		String realPath = request.getSession().getServletContext().getRealPath("/");
		dataMap = cntbdbDataComponent.cnbtBatchAdd(file, realPath,(SessionConstants)session.getAttribute("SessionConstants"));
		dataMap.put("batchSearchFlag", "true");
		return new ModelAndView("cntbdb/cntbAdd", "dataMap", dataMap);
	}




}