package com.tmobile.edat.login.component;

import java.util.List;
import java.util.Map;

import com.tmobile.edat.login.dto.request.LoginRequest;
import com.tmobile.edat.login.model.*;
 
//CRUD operations
public interface LoginDataComponent {
     
  //Read

  public Map<String,Object> getUserList(LoginRequest loginRequest);
 
}