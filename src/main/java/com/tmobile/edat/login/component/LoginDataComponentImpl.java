package com.tmobile.edat.login.component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
 







import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.tmobile.edat.login.dto.request.LoginRequest;
import com.tmobile.edat.login.model.*;
import com.tmobile.edat.login.repository.LoginDataRepository;
import com.tmobile.edat.usd.component.USDDataComponentImpl;
import com.tmobile.edat.usd.repository.USDDataRepository;
@Component("LoginDataComponent")
public class LoginDataComponentImpl implements LoginDataComponent {
 
	private static final Logger logger = Logger.getLogger(LoginDataComponentImpl.class);

	  @Autowired
	  @Qualifier("LoginDataRepository")
    LoginDataRepository loginDataRepository=null;
 
   
    @Override
    public Map<String,Object> getUserList(LoginRequest loginRequest) {
    	boolean loginFlag=false;
    	Map<String, Object>dataMap=new HashMap<String, Object>();
    	dataMap=loginDataRepository.getUserList(loginRequest.getLoginName(), loginRequest.getPassword());
    	  	
    	return dataMap;
    }
 
 
    
 
}