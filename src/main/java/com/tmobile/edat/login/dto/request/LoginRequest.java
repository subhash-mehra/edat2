package com.tmobile.edat.login.dto.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;

/**
 * Created with IntelliJ IDEA.
 * User: sameer.c
 * Date: 10/10/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoginRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String loginName;
    protected String password;
   

    public LoginRequest() {
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    

    @Override
    public String toString() {
        return "LoginRequest [loginName=" + loginName + ", password="
                + password+ "]";
    }
}
