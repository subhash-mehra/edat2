package com.tmobile.edat.login.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("")
public class CommonController {
	
	private static final Logger logger = Logger
			.getLogger(LoginController.class);
	
	@RequestMapping(value = { "/about" }, method = RequestMethod.GET)
	public String about() {
		return "common/about";
	}
	
	@RequestMapping(value = { "/help" }, method = RequestMethod.GET)
	public String help() {
		return "common/help";
	}
	
	@RequestMapping(value = { "/support" }, method = RequestMethod.GET)
	public String support() {
		return "common/support";
	}

}
