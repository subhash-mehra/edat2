package com.tmobile.edat.login.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.View;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.edat.login.component.LoginDataComponent;
import com.tmobile.edat.login.dto.request.LoginRequest;
import com.tmobile.edat.login.model.User;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.filter.store.LinkData;

@Controller
@RequestMapping("/")
/** Description of LoginController 
*
* Login Controller Class is used for login and validate user.
* 
* 
*/
public class LoginController {

	@Autowired
	@Qualifier("LoginDataComponent")
	LoginDataComponent loginDataComponent = null;

	private final String loginPage = "";

	private static final Logger logger = Logger
			.getLogger(LoginController.class);

	
	/** Description of LoginController 
	*
	* This method is used for show login page.
	* 
	* 
	*/
	// 1. Home Page landing page
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String usd() {
		return "login";
	}

	/** Description of LoginController 
	*
	* doLogin(@ModelAttribute LoginRequest loginRequest,HttpSession session) method is used for validate user.
	*  LoginRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
	*/
	// 2. NAP data search request
	@ResponseBody
	@RequestMapping(value = "doLogin", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, method = RequestMethod.POST)
	public ModelAndView doLogin(@ModelAttribute LoginRequest loginRequest,
			HttpSession session) {
		boolean loginFlag = false;
		UserAccess userAccess = null;
		LinkData linkData = new LinkData();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = loginDataComponent.getUserList(loginRequest);

		if (dataMap.get("loginFlag") != null) {
			loginFlag = (Boolean) dataMap.get("loginFlag");
		}

		if (dataMap.get("userAccess") != null) {
			userAccess = (UserAccess) dataMap.get("userAccess");
		}
		if (loginFlag) {
			session.setAttribute("userAccess", userAccess);
			String [] userRoleArr = null;
			List<String> userRole  = new ArrayList<String>();
			if(userAccess.getRoleName()!=null && !userAccess.getRoleName().equals("")){
				userRoleArr = userAccess.getRoleName().split(",");
				for(String role : userRoleArr){
					userRole.add(role.trim().toUpperCase());
				}
			}			
			System.out.println("user Role----"+userRole);
			session.setAttribute("roleName", userRole);
			session.setAttribute("linkData", linkData);
			return new ModelAndView("home", "dataMap", dataMap);

		} else {
			dataMap.put("message", "Incorrect Login Details");

			return new ModelAndView("login", "dataMap", dataMap);
		}

	}

	/** Description of LoginController 
	*
	* This method is used for show logout page.
	* 
	* 
	*/
	// 3. show Logout Page
	@ResponseBody
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public ModelAndView showLogin(HttpServletRequest request,
			HttpServletResponse response) {
		boolean loginFlag = false;
		Map<String, Object> dataMap = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		session.invalidate();

		return new ModelAndView("login", "dataMap", dataMap);

	}

	/** Description of LoginController 
	*
	* This method is used for check user is authorized for current session or not.
	* 
	* 
	*/
	// 4. show Logout Page
	@ResponseBody
	@RequestMapping(value = "checkValidUser", method = RequestMethod.GET)
	public ModelAndView showValidUser(HttpServletRequest request,
			HttpServletResponse response) {
		boolean loginFlag = false;
		Map<String, Object> dataMap = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		session.removeAttribute("userAccess");
		session.invalidate();
		dataMap.put("validUser", "You are not authorized for current session");
		return new ModelAndView("login", "dataMap", dataMap);
	}
	
	/*Edit by Vikas Choudhary */
	/** Description of LoginController 
	*
	* This method is used for display authorized access message.
	* 
	* 
	*/
	@ResponseBody
	@RequestMapping(value = "accessError", method = RequestMethod.GET)
	public ModelAndView accessDenied(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		session.invalidate();
		return new ModelAndView("access_denied", "dataMap", dataMap);

	}
}
