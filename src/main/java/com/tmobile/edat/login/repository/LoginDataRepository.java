package com.tmobile.edat.login.repository;

import java.util.List;
import java.util.Map;

import com.tmobile.edat.login.model.*;
 
//CRUD operations
public interface LoginDataRepository {
     
  //Read
 	public Map<String,Object> getUserList(String loginName,String password);
 
}