package com.tmobile.edat.login.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmobile.edat.util.*;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.tmobile.edat.admin.model.UserAccess;
import com.tmobile.edat.login.model.*;

@Repository("LoginDataRepository")
public class LoginDataHandler implements LoginDataRepository {

	@Autowired
    @Qualifier("dataSource")
	private DataSource dataSource;
	 
  
   
	@Override
	public Map<String, Object> getUserList(String loginName, String password) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String encodedcodePassword = "";
		boolean loginFlag = false;
		String query = "";
		String userName = "";
		String userRole="";
		UserAccess userAccess=null;
		List<UserAccess>userList=new ArrayList<UserAccess>();
		encodedcodePassword = JavaUtil.encodeString(password);
		System.out.println("paqssw::"+encodedcodePassword);
		userList=getUsersList(loginName, encodedcodePassword);
		query = "select count(*) from user_access where lower(userid)='" + loginName.toLowerCase()
				+ "' and password='" + encodedcodePassword + "'";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		if(userList.size()>0)
		{
			 userAccess=userList.get(0);
		}
		

		try {

			int count = jdbcTemplate.queryForInt(query);
			if(userList.size()>0)
			{
				loginFlag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		dataMap.put("loginFlag", loginFlag);
		dataMap.put("userAccess", userAccess);
		
		return dataMap;
	}
	
	//return user List

		public List<UserAccess> getUsersList(String loginName, String password) {

			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			return jdbcTemplate.query("select * from user_access where userid='" + loginName
				+ "' and password='" + password +"'",
					new RowMapper<UserAccess>() {
						@Override
						public UserAccess mapRow(ResultSet rs, int rownumber)
								throws SQLException {
							UserAccess e = new UserAccess();
							e.setUserId(rs.getString(1));
							e.setFirstName(rs.getString(2));
							e.setLastName(rs.getString(3));
							e.setPassword(rs.getString(4));
							e.setRoleName(rs.getString(5));
							e.setNote(rs.getString(6));
							e.setCreateDate(rs.getDate(7));

							return e;
						}
					});

		}
		
    
 
}