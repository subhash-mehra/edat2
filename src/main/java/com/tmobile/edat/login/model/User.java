package com.tmobile.edat.login.model;
public class User {
 
    private int id;
    private String firstName;
    private String lastName;
    private String loginName;
    private String password;
    private String userType;
    private String status;
    private String username;
    private String userid;
    private String iamemail;
    
    public String getIamemail() {
		return iamemail;
	}
	public void setIamemail(String iamemail) {
		this.iamemail = iamemail;
	}
	public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firName) {
        this.firstName = firName;
    }
    
    public String getLasttName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getLogin() {
        return loginName;
    }
    public void setLogin(String loginName) {
        this.loginName = loginName;
    }
    
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }
    
    
    public String getStatus() {
        return userType;
    }
    public void setStatus(String status) {
    	
    	this.status=status;
    }
    
    public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	@Override
    public String toString(){
        return "{ID="+id+",firstName="+firstName+",lastName="+lastName+",loginName="+loginName+"password="+password+"userType="+userType+"status="+status+"}";
    }
}