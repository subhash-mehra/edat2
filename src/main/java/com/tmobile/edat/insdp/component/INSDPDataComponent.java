package com.tmobile.edat.insdp.component;

import com.tmobile.edat.base.BaseComponent;
import com.tmobile.edat.insdp.dto.request.INSDPDataRequest;
import com.tmobile.edat.insdp.dto.response.INSDPDataResponse;

public interface INSDPDataComponent extends BaseComponent{

	public  INSDPDataResponse GetAccountDetails(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse GetBalanceAndDate(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateBalanceAndDate(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateAccumulators(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateServiceClass(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateSubscriberSegmentation(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse InstallSubscriber(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse ActivateSubscriber(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse DeleteSubscriber(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse getAccumulators(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse updateDedicatedDABal(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse updateOffer(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse getOffer(INSDPDataRequest insdpDataRequest);
	
	
}
