package com.tmobile.edat.insdp.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.insdp.dto.INSDPSearchRequest;
import com.tmobile.edat.insdp.dto.INSDPSearchResponse;
import com.tmobile.edat.insdp.repository.INSDPDataRepository;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.edat.util.Constants;

@Service("INSDPComponent")
public class INSDPComponentImpl implements INSDPComponent {
	
	@Autowired
	@Qualifier("INSDPDataRepository")
	private INSDPDataRepository insdpDataRepository;
	
	@Override
	public boolean validateRequest(BaseRequest baseRequest, BaseResponse baseResponse, int key) {
		if (baseRequest == null) {
			baseResponse = new INSDPSearchResponse();
			baseResponse.setApplicationError(new ApplicationError(ApplicationConstants.NULL_REQUEST));
			baseResponse.setStatusCode(ApplicationConstants.FAILURE_CODE);
			return false;
		}
		return true;
	}

	@Override
	public INSDPSearchResponse getINSDPData(INSDPSearchRequest insdpSearchRequest) {
	INSDPSearchResponse insdpDataResponse = null;
	System.out.println("Before end of method");
	if (this.validateRequest(insdpSearchRequest, insdpDataResponse, 0)) {
			if (insdpSearchRequest.getMsisdn() != null) {
				//insdpDataResponse = this.insdpDataRepository.searMSISDN(Constants.SEARCH_TYPE_MSISDN, insdpSearchRequest.getMsisdn().trim());
			}

	}
	System.out.println("After end of method");
	return insdpDataResponse;
  }
}
