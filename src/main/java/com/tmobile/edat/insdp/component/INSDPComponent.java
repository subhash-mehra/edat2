package com.tmobile.edat.insdp.component;

import com.tmobile.edat.base.BaseComponent;
import com.tmobile.edat.insdp.dto.INSDPSearchRequest;
import com.tmobile.edat.insdp.dto.INSDPSearchResponse;

public interface INSDPComponent extends BaseComponent {


public INSDPSearchResponse getINSDPData(INSDPSearchRequest insdpSearchRequest);

}
