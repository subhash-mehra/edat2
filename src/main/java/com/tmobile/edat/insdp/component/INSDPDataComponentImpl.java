/**
 * 
 */
package com.tmobile.edat.insdp.component;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.insdp.dto.request.INSDPAccumulatorInformation;
import com.tmobile.edat.insdp.dto.request.INSDPDataRequest;
import com.tmobile.edat.insdp.dto.request.INSDPServiceOfferings;
import com.tmobile.edat.insdp.dto.response.INSDPDataResponse;
import com.tmobile.edat.insdp.repository.INSDPDataRepository;
import com.tmobile.edat.messaging.component.MessagingDataComponentImpl;
import com.tmobile.edat.messaging.repository.MessagingDataRepository;
import com.tmobile.edat.soaprequestxml.INSDPDataSoapRequest;
import com.tmobile.edat.usd.dto.response.NapDataResponse;

/**
 * @author bharatbhushan.m
 *
 */
@Component("INSDPDataComponent")
public class INSDPDataComponentImpl implements INSDPDataComponent {
	
	private static final Logger logger = Logger.getLogger(INSDPDataComponentImpl.class);

	Date date = new Date();
	 @Autowired
	  @Qualifier("INSDPDataRepository")
	 INSDPDataRepository insdpDataRepository = null;

	 public INSDPDataComponentImpl() {
			
		}
	 
	 @Override
		public boolean validateRequest(BaseRequest baseRequest, BaseResponse baseResponse, int key) {
			
	    	// Applicable to all methods
	    	if(baseRequest == null){
	    		baseResponse = new NapDataResponse();
	    		baseResponse.setApplicationError(new ApplicationError(ApplicationConstants.NULL_REQUEST));
	    		return false;
	    	}
	    	
			return true;
		} 

	@Override
	public INSDPDataResponse GetAccountDetails(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Get Account Details====================");
	  INSDPDataResponse insdpDataResponse = null;
      if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
    	   insdpDataRequest.setDate(date);
		   insdpDataResponse = this.insdpDataRepository.GetAccountDetails(insdpDataRequest); 
		 }
      return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse GetBalanceAndDate(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Get Balance And Date====================");
		INSDPDataResponse insdpDataResponse = null;
	      if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
	    	  insdpDataRequest.setDate(date);
		   insdpDataResponse = this.insdpDataRepository.GetBalanceAndDate(insdpDataRequest);
	      }
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse getAccumulators(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Get Accumulators ====================");
		INSDPDataResponse insdpDataResponse = null;
	      if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
	    	  insdpDataRequest.setDate(date);
		   insdpDataResponse = this.insdpDataRepository.getAccumulators(insdpDataRequest);
	      }
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse getOffer(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Get Offer ====================");
		INSDPDataResponse insdpDataResponse = null;
	      if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
	    	  insdpDataRequest.setDate(date);
		   insdpDataResponse = this.insdpDataRepository.getOffer(insdpDataRequest);
	      }
		return insdpDataResponse;
	}
	
	
	
	
	

	@Override
	public INSDPDataResponse UpdateBalanceAndDate(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Update Balance And Date====================");
		INSDPDataResponse insdpDataResponse = null;
		  if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
			  insdpDataRequest.setDate(date);
		     insdpDataResponse = this.insdpDataRepository.UpdateBalanceAndDate(insdpDataRequest);
		  }
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse UpdateAccumulators(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Update Accumulators====================");
		
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
				String requestXml = createUpdateAccumulatorsRequest(insdpDataRequest.getAccumulatorInfo());
				logger.debug("UpdateAccumulators::: XML is ::= "+requestXml);
				 insdpDataRequest.setDate(date);
				insdpDataResponse = this.insdpDataRepository.UpdateAccumulators(insdpDataRequest,requestXml);
		 }
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse updateDedicatedDABal(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Update Accumulators====================");
		
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
				String requestXml = createUpdateAccumulatorsRequest(insdpDataRequest.getAccumulatorInfo());
				logger.debug("UpdateAccumulators::: XML is ::= "+requestXml);
				 insdpDataRequest.setDate(date);
				insdpDataResponse = this.insdpDataRepository.updateDedicatedDABal(insdpDataRequest);
		 }
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse updateOffer(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Update Accumulators====================");
		
		INSDPDataResponse insdpDataResponse = null;
		
				 insdpDataRequest.setDate(date);
				insdpDataResponse = this.insdpDataRepository.updateOffer(insdpDataRequest);
		
		return insdpDataResponse;
	}
	
	
	
	

	@Override
	public INSDPDataResponse UpdateServiceClass(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Update Service Class====================");
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
			 insdpDataRequest.setDate(date);
		   insdpDataResponse = this.insdpDataRepository.UpdateServiceClass(insdpDataRequest);
		 }
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse UpdateSubscriberSegmentation(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Update Subscriber Segmentation====================");
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
			 insdpDataRequest.setDate(date);
				String requestXml = createSubscriberSegmentationRequest(insdpDataRequest.getServiceOffering());
		   insdpDataResponse = this.insdpDataRepository.UpdateSubscriberSegmentation(insdpDataRequest,requestXml);
		 }
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse InstallSubscriber(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Install Subscriber====================");
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
			 insdpDataRequest.setDate(date);
			String resuestXml = createInstallSubscriberRequest(insdpDataRequest.getServiceOffering());
		   insdpDataResponse = this.insdpDataRepository.InstallSubscriber(insdpDataRequest,resuestXml);
		 }
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse ActivateSubscriber(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Activate Subscriber====================");
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){
			 insdpDataRequest.setDate(date);
		    insdpDataResponse = this.insdpDataRepository.ActivateSubscriber(insdpDataRequest);
		 }
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse DeleteSubscriber(INSDPDataRequest insdpDataRequest) {
		logger.info("====================Delete Subscriber====================");
		INSDPDataResponse insdpDataResponse = null;
		 if(this.validateRequest(insdpDataRequest, insdpDataResponse, 0)){ 
			 insdpDataRequest.setDate(date);
		    insdpDataResponse = this.insdpDataRepository.DeleteSubscriber(insdpDataRequest);
		 }
		return insdpDataResponse;
	}
	
	
	public String createInstallSubscriberRequest(List<INSDPServiceOfferings> serviceOffers)
	{
		 String xmlOutput =  createServiceOfferingRequest(serviceOffers);
		 String finalRequest = null;
		 
		 String footerXml = "</struct> " + "         </value> "
				 + "      </param> " + "   </params> " + "</methodCall>"; 
		if(serviceOffers.size()>0)
		{
		  finalRequest =  INSDPDataSoapRequest.InstallSubscriber + xmlOutput +footerXml;
		}else
		{
			finalRequest =  INSDPDataSoapRequest.InstallSubscriber +footerXml;
		}
		 
		  
		return finalRequest;
		
	}
	
	public String createSubscriberSegmentationRequest(List<INSDPServiceOfferings> serviceOffers)
	{
		 String xmlOutput =  createServiceOfferingRequest(serviceOffers);
		 String finalRequest = null;
		 
		 if(serviceOffers.size()>0)
		{
		 finalRequest =  INSDPDataSoapRequest.UpdateSubscriberSegmentation + xmlOutput +INSDPDataSoapRequest.remainSubscriberSegmentation;
		  logger.info("SubscriberSegmentation Xml:::: "+finalRequest);	
		}else
		{
			finalRequest =  INSDPDataSoapRequest.UpdateSubscriberSegmentation +INSDPDataSoapRequest.remainSubscriberSegmentation;
		}
		return finalRequest;
		
	}
	
	public String createUpdateAccumulatorsRequest(List<INSDPAccumulatorInformation> accumulatorInformations)
	{
		 String xmlOutput =  createAccumulatorInformationRequest(accumulatorInformations);
		 String finalRequest = null;
		 
		  logger.info("Created AccumulatorsRequest Xml:::: "+xmlOutput);	
		  finalRequest =  INSDPDataSoapRequest.UpdateAccumulators + xmlOutput +INSDPDataSoapRequest.accumulatorsRemains;
		  logger.info("Complete AccumulatorsRequest Xml:::: "+finalRequest);	
		  
		return finalRequest;
		
	}
	
	 // utility method to create text node
	 public  Node createMemberValueElements(Document doc, Element element, String valueTag, String type,int val) {
		 Element node = doc.createElement(valueTag);
		 Element node1 = doc.createElement(type);
		 node1.appendChild(doc.createTextNode(Integer.toString(val)));
		 node.appendChild(node1);
		 return node;
	 }

	 public  Node createMemberIdElements(Document doc, Element element, String name, String value) {
		 Element node = doc.createElement(name);
		 node.appendChild(doc.createTextNode(value));
		 return node;
	 }

	 public String createServiceOfferingRequest(List<INSDPServiceOfferings> serviceOffers)
		{
			logger.info("======IN createInstallSubscriberRequest "+serviceOffers.size());
			 String xmlOutput = null;
			 try {
				 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				 // root elements
				 Document doc = docBuilder.newDocument();
				 Element rootElement = doc.createElement("member");
				 doc.appendChild(rootElement);

				 Element serviceName = doc.createElement("name");
				 serviceName.appendChild(doc.createTextNode("serviceOfferings"));
				 rootElement.appendChild(serviceName);

				 Element value = doc.createElement("value");
				 Element array = doc.createElement("array");
				 Element data = doc.createElement("data");
				 // rootElement.appendChild(data);
				 // need to be change according to form inputs
				 for (INSDPServiceOfferings serviceOffer : serviceOffers) {
					 // child elements
					 Element value1 = doc.createElement("value");
					 //  rootElement.appendChild(value1);
					 Element struct = doc.createElement("struct");
					 // rootElement.appendChild(struct);
					 Element member = doc.createElement("member");

					 member.appendChild(this.createMemberIdElements(doc, rootElement, "name", "serviceOfferingID"));

					 member.appendChild(this.createMemberValueElements(doc, rootElement,"value", "int", serviceOffer.getServiceOfferingID()));

					 member.appendChild(this.createMemberIdElements(doc, rootElement, "name", "serviceOfferingActiveFlag"));

					 member.appendChild(this.createMemberValueElements(doc, rootElement,"value", "boolean", serviceOffer.getServiceOfferingActiveFlag()));

					 struct.appendChild(member);
					 value1.appendChild(struct);
					 data.appendChild(value1);
				 }
				 array.appendChild(data);
				 value.appendChild(array);
				 rootElement.appendChild(value);

				 TransformerFactory transformerFactory = TransformerFactory.newInstance();
				 Transformer transformer = transformerFactory.newTransformer();
				 transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				 DOMSource source = new DOMSource(doc);
				 StreamResult result = new StreamResult(new StringWriter());  
				 transformer.transform(source, result);  

				 xmlOutput = result.getWriter().toString();  

			 } catch (ParserConfigurationException pce) {
				 pce.printStackTrace();
				 logger.info("ParserConfigurationException::"+pce.getMessage());
			 } catch (TransformerException tfe) {
				 logger.info("TransformerException::"+tfe.getMessage());
				 tfe.printStackTrace();
			 }
			
			return xmlOutput;
		}
	 
	 
	 public String createAccumulatorInformationRequest(List<INSDPAccumulatorInformation> accumulatorInformations)
		{
			logger.info("======IN createAccumulatorInformationRequest ");
			 String xmlOutput = null;
			 try {
				 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				 // root elements
				 Document doc = docBuilder.newDocument();
				 Element rootElement = doc.createElement("member");
				 doc.appendChild(rootElement);

				 Element serviceName = doc.createElement("name");
				 serviceName.appendChild(doc.createTextNode("accumulatorInformation"));
				 rootElement.appendChild(serviceName);

				 Element value = doc.createElement("value");
				 Element array = doc.createElement("array");
				 Element data = doc.createElement("data");
				 // rootElement.appendChild(data);
				 // need to be change according to form inputs
				 for (INSDPAccumulatorInformation accumulatorInfo : accumulatorInformations) {
					 // child elements
					 Element value1 = doc.createElement("value");
					 //  rootElement.appendChild(value1);
					 Element struct = doc.createElement("struct");
					 // rootElement.appendChild(struct);
					 Element member = doc.createElement("member");

					 member.appendChild(this.createMemberIdElements(doc, rootElement, "name", accumulatorInfo.getAccumulatorValueAbsoluteName()));

					 member.appendChild(this.createMemberValueElements(doc, rootElement,"value", "int", accumulatorInfo.getAccumulatorValueAbsolute()));

					 member.appendChild(this.createMemberIdElements(doc, rootElement, "name", "accumulatorID"));

					 member.appendChild(this.createMemberValueElements(doc, rootElement,"value", "int", accumulatorInfo.getAccumulatorID()));

					 struct.appendChild(member);
					 value1.appendChild(struct);
					 data.appendChild(value1);
				 }
				 array.appendChild(data);
				 value.appendChild(array);
				 rootElement.appendChild(value);

				 TransformerFactory transformerFactory = TransformerFactory.newInstance();
				 Transformer transformer = transformerFactory.newTransformer();
				 transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				 DOMSource source = new DOMSource(doc);
				 StreamResult result = new StreamResult(new StringWriter());  
				 transformer.transform(source, result);  

				  xmlOutput = result.getWriter().toString();  
				  
			 } catch (ParserConfigurationException pce) {
				 pce.printStackTrace();
				 logger.info("In AccumulatorInformation ParserConfigurationException::"+pce.getMessage());
			 } catch (TransformerException tfe) {
				 logger.info("In AccumulatorInformation TransformerException::"+tfe.getMessage());
				 tfe.printStackTrace();
			 }
			
			return xmlOutput;
		}

}
