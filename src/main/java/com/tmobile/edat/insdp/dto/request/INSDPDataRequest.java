package com.tmobile.edat.insdp.dto.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.session.SessionConstants;


public class INSDPDataRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String customerId;
    protected String msisdn;
    protected String accId;
    protected String accVal;
    protected String imsi;
    protected String ban;
    protected Date date;
    protected String supervisionExpiryDate;
    protected String serviceFeeExpiryDate;
    protected String originTimeStamp;
    protected int serviceClassNew;
    protected int languageIDNew;
    protected int ussdEndOfCallNotificationID;
    protected String adjustmentAmountRelative;
    protected SessionConstants sessionConstants;
    protected List <INSDPServiceOfferings> serviceOffering = new ArrayList<INSDPServiceOfferings>();
    protected List <INSDPAccumulatorInformation> accumulatorInfo = new ArrayList<INSDPAccumulatorInformation>();

    
    public String getAdjustmentAmountRelative() {
		return adjustmentAmountRelative;
	}

	public void setAdjustmentAmountRelative(String adjustmentAmountRelative) {
		this.adjustmentAmountRelative = adjustmentAmountRelative;
	}
	
	

	public int getUssdEndOfCallNotificationID() {
		return ussdEndOfCallNotificationID;
	}

	public void setUssdEndOfCallNotificationID(int ussdEndOfCallNotificationID) {
		this.ussdEndOfCallNotificationID = ussdEndOfCallNotificationID;
	}

	public int getLanguageIDNew() {
		return languageIDNew;
	}

	public void setLanguageIDNew(int languageIDNew) {
		this.languageIDNew = languageIDNew;
	}

	public List<INSDPServiceOfferings> getServiceOffering() {
		return serviceOffering;
	}

	public void setServiceOffering(List<INSDPServiceOfferings> serviceOffering) {
		this.serviceOffering = serviceOffering;
	}

	public String getOriginTimeStamp() {
		return originTimeStamp;
	}

	public void setOriginTimeStamp(String originTimeStamp) {
		this.originTimeStamp = originTimeStamp;
	}

	public int getServiceClassNew() {
		return serviceClassNew;
	}

	public void setServiceClassNew(int serviceClassNew) {
		this.serviceClassNew = serviceClassNew;
	}

	 public List<INSDPAccumulatorInformation> getAccumulatorInfo() {
		return accumulatorInfo;
	}

	public void setAccumulatorInfo(List<INSDPAccumulatorInformation> accumulatorInfo) {
		this.accumulatorInfo = accumulatorInfo;
	}

	public String getSupervisionExpiryDate() {
		return supervisionExpiryDate;
	}

	public void setSupervisionExpiryDate(String supervisionExpiryDate) {
		this.supervisionExpiryDate = supervisionExpiryDate;
	}

	public String getServiceFeeExpiryDate() {
		return serviceFeeExpiryDate;
	}

	public void setServiceFeeExpiryDate(String serviceFeeExpiryDate) {
		this.serviceFeeExpiryDate = serviceFeeExpiryDate;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public INSDPDataRequest() {
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    
    public String getAccId() {
		return accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	public String getAccVal() {
		return accVal;
	}

	public void setAccVal(String accVal) {
		this.accVal = accVal;
	}


    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getBan() {
        return ban;
    }

    public void setBan(String ban) {
        this.ban = ban;
    }

    public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}

	@Override
    public String toString() {
        return "UsdSearchRequest [customerId=" + customerId + ", msisdn="
                + msisdn + ", imsi=" + imsi + ", ban=" + ban +"accId="+accId+"accVal="+accVal+ "ussdEndOfCallNotificationID="+ussdEndOfCallNotificationID+"]";
    }
}
