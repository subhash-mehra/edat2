package com.tmobile.edat.insdp.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Multimap;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.dusupdate.dto.Counter;

public class INSDPDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    
	private String uid;
	 // Map store all attributes and values;
    protected Map<String, String> attributes;
    protected Map<String, Object>attMultimap;
    
  
public Map<String, Object> getAttMultimap() {
		return attMultimap;
	}
	public void setAttMultimap(Map<String, Object> attMultimap) {
		this.attMultimap = attMultimap;
	}
public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Map<String, String> getAttributes() {
		return attributes;
	}
	
	
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
		

}
