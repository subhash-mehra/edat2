/**
 * 
 */
package com.tmobile.edat.insdp.dto.request;

/**
 * @author bharatbhushan.m
 *
 */
public class INSDPAccumulatorInformation {
	
	private String accumulatorValueAbsoluteName;
	
	private int accumulatorValueAbsolute;
	
	public String getAccumulatorValueAbsoluteName() {
		return accumulatorValueAbsoluteName;
	}
	public void setAccumulatorValueAbsoluteName(String accumulatorValueAbsoluteName) {
		this.accumulatorValueAbsoluteName = accumulatorValueAbsoluteName;
	}
	private int accumulatorID;
	
	public int getAccumulatorValueAbsolute() {
		return accumulatorValueAbsolute;
	}
	public void setAccumulatorValueAbsolute(int accumulatorValueAbsolute) {
		this.accumulatorValueAbsolute = accumulatorValueAbsolute;
	}
	public int getAccumulatorID() {
		return accumulatorID;
	}
	public void setAccumulatorID(int accumulatorID) {
		this.accumulatorID = accumulatorID;
	}

}
