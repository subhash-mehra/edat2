package com.tmobile.edat.insdp.dto;

import com.tmobile.edat.base.BaseRequest;

public class INSDPSearchRequest extends BaseRequest {

	private String msisdn;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
}
