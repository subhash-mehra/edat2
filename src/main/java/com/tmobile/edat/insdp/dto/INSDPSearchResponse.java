package com.tmobile.edat.insdp.dto;

import java.io.Serializable;

import com.tmobile.edat.base.BaseResponse;

public class INSDPSearchResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
	
	private String accountsAndAccumulators;
	private String blockStatus;
	private String lifeCycleDates;
	private String annoucements;
	private String familyAndFriends;
	private String refillsAndPromotions;
	private String periodicAdjustment;
	private String offers;
	private String periodicAccountManagement;
	private String usageCountersAndThresholds;
	private String timeRestrictions;
	
	public String getAccountsAndAccumulators() {
		return accountsAndAccumulators;
	}
	
	public void setAccountsAndAccumulators(String accountsAndAccumulators) {
		this.accountsAndAccumulators = accountsAndAccumulators;
	}
	public String getBlockStatus() {
		return blockStatus;
	}
	public void setBlockStatus(String blockStatus) {
		this.blockStatus = blockStatus;
	}
	public String getLifeCycleDates() {
		return lifeCycleDates;
	}
	public void setLifeCycleDates(String lifeCycleDates) {
		this.lifeCycleDates = lifeCycleDates;
	}
	public String getAnnoucements() {
		return annoucements;
	}
	public void setAnnoucements(String annoucements) {
		this.annoucements = annoucements;
	}
	public String getFamilyAndFriends() {
		return familyAndFriends;
	}
	public void setFamilyAndFriends(String familyAndFriends) {
		this.familyAndFriends = familyAndFriends;
	}
	public String getRefillsAndPromotions() {
		return refillsAndPromotions;
	}
	public void setRefillsAndPromotions(String refillsAndPromotions) {
		this.refillsAndPromotions = refillsAndPromotions;
	}
	public String getPeriodicAdjustment() {
		return periodicAdjustment;
	}
	public void setPeriodicAdjustment(String periodicAdjustment) {
		this.periodicAdjustment = periodicAdjustment;
	}
	public String getOffers() {
		return offers;
	}
	public void setOffers(String offers) {
		this.offers = offers;
	}
	public String getPeriodicAccountManagement() {
		return periodicAccountManagement;
	}
	public void setPeriodicAccountManagement(String periodicAccountManagement) {
		this.periodicAccountManagement = periodicAccountManagement;
	}
	public String getUsageCountersAndThresholds() {
		return usageCountersAndThresholds;
	}
	public void setUsageCountersAndThresholds(String usageCountersAndThresholds) {
		this.usageCountersAndThresholds = usageCountersAndThresholds;
	}
	public String getTimeRestrictions() {
		return timeRestrictions;
	}
	public void setTimeRestrictions(String timeRestrictions) {
		this.timeRestrictions = timeRestrictions;
	}
		
	@Override
	public String toString() {
		return "INSDPSearchResponse [accountsAndAccumulators=" + accountsAndAccumulators + ", blockStatus="
				+ blockStatus + ", lifeCycleDates=" + lifeCycleDates + ", annoucements=" + annoucements
				+ ", familyAndFriends=" + familyAndFriends + ", refillsAndPromotions=" + refillsAndPromotions
				+ ", periodicAdjustment=" + periodicAdjustment + ", offers=" + offers + ", periodicAccountManagement="
				+ periodicAccountManagement + ", usageCountersAndThresholds=" + usageCountersAndThresholds
				+ ", timeRestrictions=" + timeRestrictions + "]";
	}

}
