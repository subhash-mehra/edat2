package com.tmobile.edat.insdp.dto.request;

public class INSDPServiceOfferings {
	
	private String serviceOfferingName;
	
	private int serviceOfferingID;
	
	private int serviceOfferingActiveFlag;
	

	public int getServiceOfferingID() {
		return serviceOfferingID;
	}

	public void setServiceOfferingID(int serviceOfferingID) {
		this.serviceOfferingID = serviceOfferingID;
	}

	public String getServiceOfferingName() {
		return serviceOfferingName;
	}

	public void setServiceOfferingName(String serviceOfferingName) {
		this.serviceOfferingName = serviceOfferingName;
	}

	public int getServiceOfferingActiveFlag() {
		return serviceOfferingActiveFlag;
	}

	public void setServiceOfferingActiveFlag(int serviceOfferingActiveFlag) {
		this.serviceOfferingActiveFlag = serviceOfferingActiveFlag;
	}

}
