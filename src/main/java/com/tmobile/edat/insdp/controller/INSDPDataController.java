/**
 * 
 */
package com.tmobile.edat.insdp.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmobile.edat.insdp.component.INSDPDataComponent;
import com.tmobile.edat.insdp.component.INSDPDataComponentImpl;
import com.tmobile.edat.insdp.dto.request.INSDPDataRequest;
import com.tmobile.edat.insdp.dto.response.INSDPDataResponse;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.DusDataResponse;
import com.tmobile.session.SessionConstants;

/** Description of INSDPDataController 
*
* INSDPDataController class is used for INSDP Data tool.
* 
* 
*/
@Controller
@RequestMapping("/insdp")
public class INSDPDataController {
	
	private static final Logger logger = Logger.getLogger(INSDPDataController.class);
	
	@Autowired
	@Qualifier("INSDPDataComponent")
	INSDPDataComponent insdpDataComponent = null;
	
	 /**
		*
		* insdp() method is used for show INSDP main page.
		* 
		*/
    @RequestMapping(value={"","/"}, method=RequestMethod.GET)
    public String insdp() {        
        return "insdp/insdp";
    } 
    
    /**
	*
	* GetAccountDetails() method is used for get Accoubt Details.
	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
	* 
	*/
    @RequestMapping(value="/AccountDetails",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public  INSDPDataResponse GetAccountDetails(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	logger.info("IN controller AccountDetails::");
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.GetAccountDetails(insdpDataRequest);
    }
    
    /**
	*
	* GetBalanceAndDate(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for get Balance and Date.
	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
	* 
	*/
    @RequestMapping(value="/BalanceAndDate",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public  INSDPDataResponse GetBalanceAndDate(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.GetBalanceAndDate(insdpDataRequest);
    }
    
    /**
	*
	* UpdateBalanceAndDate(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for update Balance and Details.
	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
	* 
	*/
    @RequestMapping(value="/UpdateBalanceAndDate",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse UpdateBalanceAndDate(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.UpdateBalanceAndDate(insdpDataRequest);
    }
    
    /**
   	*
   	* UpdateAccumulators(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  Update Accumulators.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	* 
   	*/
    @RequestMapping(value="/UpdateAccumulators",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse UpdateAccumulators(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.UpdateAccumulators(insdpDataRequest);
    }
    
    /**
   	*
   	* UpdateServiceClass(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  Update Service Classes.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	* 
   	*/
    @RequestMapping(value="/UpdateService",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse UpdateServiceClass(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.UpdateServiceClass(insdpDataRequest);
    }
    
    // 2. INSDP Update Subscriber Segment  request
    
    /**
   	*
   	* UpdateSubscriberSegmentation(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  update Subscriber Segment.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	* 
   	*/
    @RequestMapping(value="/UpdateSubscriber",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse UpdateSubscriberSegmentation(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.UpdateSubscriberSegmentation(insdpDataRequest);
    }
    
    /**
   	*
   	* InstallSubscriber(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  create Subscriber.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	* 
   	*/
    @RequestMapping(value="/InstallSubscriber",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse InstallSubscriber(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.InstallSubscriber(insdpDataRequest);
    }
    
    /**
   	*
   	* ActivateSubscriber(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  activate Subscriber.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	* 
   	*/
    @RequestMapping(value="/ActivateSubscriber",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse ActivateSubscriber(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.ActivateSubscriber(insdpDataRequest);
    }
    /**
   	*
   	* DeleteSubscriber(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  delete Subscriber.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	*/
    @RequestMapping(value="/DeleteSubscriber",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
        public  INSDPDataResponse DeleteSubscriber(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.DeleteSubscriber(insdpDataRequest);
    }
    
    
    /**
   	*
   	* getAccumulators(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  Get Accumulators.
   	* insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	*/
    
    @RequestMapping(value="/getAccumulators",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public  INSDPDataResponse getAccumulators(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.getAccumulators(insdpDataRequest);
    }
    
    /**
   	*
   	* updateDedicatedDABal(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  update Relative Balance.
   	*  insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	*/
    
    @RequestMapping(value="/updateDedicatedDABal",
			method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
    	@ResponseBody
    	public  INSDPDataResponse updateDedicatedDABal(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.updateDedicatedDABal(insdpDataRequest);
    	}
    
    /**
   	*
   	* updateOffer(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  Update Offer Details.
   	*  insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	*/
    
    @RequestMapping(value="/updateOffer",
			method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
    	@ResponseBody
    	public  INSDPDataResponse updateOffer(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.updateOffer(insdpDataRequest);
    	}
    
    /**
   	*
   	* getOffer(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session) method is used for  get Offer Details.
   	*  insdpDataRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user.
   	*/
    @RequestMapping(value="/getOffer",
					method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public  INSDPDataResponse getOffer(@RequestBody INSDPDataRequest insdpDataRequest, HttpSession session){
    	insdpDataRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.insdpDataComponent.getOffer(insdpDataRequest);
    }
    

}
