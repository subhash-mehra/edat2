package com.tmobile.edat.insdp.repository;

import com.tmobile.edat.insdp.dto.request.INSDPDataRequest;
import com.tmobile.edat.insdp.dto.response.INSDPDataResponse;

public interface INSDPDataRepository {

	public  INSDPDataResponse GetAccountDetails(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse GetBalanceAndDate(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateBalanceAndDate(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateAccumulators(INSDPDataRequest insdpDataRequest,String requestXml);
	
	public  INSDPDataResponse UpdateServiceClass(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse UpdateSubscriberSegmentation(INSDPDataRequest insdpDataRequest,String requestXml);
	
	public  INSDPDataResponse InstallSubscriber(INSDPDataRequest insdpDataRequest,String requestXml);
	
	public  INSDPDataResponse ActivateSubscriber(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse DeleteSubscriber(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse getAccumulators(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse updateDedicatedDABal(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse updateOffer(INSDPDataRequest insdpDataRequest);
	
	public  INSDPDataResponse getOffer(INSDPDataRequest insdpDataRequest);
}
