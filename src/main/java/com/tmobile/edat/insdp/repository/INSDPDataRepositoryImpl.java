/**
 * 
 */
package com.tmobile.edat.insdp.repository;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.insdp.dto.request.INSDPDataRequest;
import com.tmobile.edat.insdp.dto.response.INSDPDataResponse;
import com.tmobile.edat.soaprequestxml.INSDPDataSoapRequest;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.JavaUtil;
import com.tmobile.edat.util.Util;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

/**
 * @author bharatbhushan.m
 *
 */
@Repository("INSDPDataRepository")
public class INSDPDataRepositoryImpl implements INSDPDataRepository {

	private static final Logger logger = Logger
			.getLogger(INSDPDataRepositoryImpl.class);

	@Override
	public INSDPDataResponse GetAccountDetails(INSDPDataRequest insdpDataRequest) {
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		logger.info("IN Repository GetAccountDetails::"
				+ insdpDataRequest.getMsisdn());
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		try {
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(INSDPDataSoapRequest.GetAccountDetails,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream(), "UTF-8"));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = in.readLine()) != null) {
				sb.append(decodedString);
			}

			logger.info("Response XML for GetAccount Details::" + sb.toString());
			System.out.println("Attributes========================"+sb.toString());
			Map<String, String> attributes = parseXmltoJavaForGetAccountDetails(sb.toString());
			// Map<String, String> attributes = parseXmltoJava("test");
			logger.info("In GetAccountDetails Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			logger.info("Exception In GetAccountDetails::" + ex.getMessage());
			ex.printStackTrace();
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		}
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse GetBalanceAndDate(INSDPDataRequest insdpDataRequest) {
		logger.info("IN Repository GetBalanceAndDate::"
				+ insdpDataRequest.getMsisdn());
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		
		logger.info("MSISDN----------------->::" + insdpDataRequest.getMsisdn());
		logger.info("Date===================>::" +DATE_FORMAT_1.format(date) );
		try {
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			
			wr.writeBytes(String.format(INSDPDataSoapRequest.GetBalanceAndDate,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream(), "UTF-8"));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			logger.info("Response XML for GetAccount Details::" + sb.toString());
			Map<String, String> attributes = parseXmltoJavaForBalanceAndDate(sb.toString());
			// Map<String, String> attributes = parseXmltoJava("test");
			logger.info("In GetBalanceAndDate Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
			ex.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse getAccumulators(INSDPDataRequest insdpDataRequest) {
		logger.info("IN Repository GetBalanceAndDate::"
				+ insdpDataRequest.getMsisdn());
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		System.out.println(DATE_FORMAT_1.format(date) );
		logger.info("MSISDN----------------->::" + insdpDataRequest.getMsisdn());
		logger.info("Date===================>::" +DATE_FORMAT_1.format(date) );
		try {
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			
			wr.writeBytes(String.format(INSDPDataSoapRequest.GetAccumulators,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream(), "UTF-8"));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			logger.info("Response XML for GetAccount Details::" + sb.toString());
			Map<String, String> attributes = parseXmltoJavaForGetAccumulators(sb.toString());
			// Map<String, String> attributes = parseXmltoJava("test");
			logger.info("In GetBalanceAndDate Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
			ex.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse getOffer(INSDPDataRequest insdpDataRequest) {
		
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		/*logger.info("IN Repository GetBalanceAndDate::"
				+ insdpDataRequest.getMsisdn());
		
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		System.out.println(DATE_FORMAT_1.format(date) );
		logger.info("MSISDN----------------->::" + insdpDataRequest.getMsisdn());
		logger.info("Date===================>::" +DATE_FORMAT_1.format(date) );
		try {
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			
			wr.writeBytes(String.format(INSDPDataSoapRequest.GetOffer,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream(), "UTF-8"));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			logger.info("Response XML for GetAccount Details::" + sb.toString());
			Multimap<String, String> attributes = parseXmltoJavaForGetOfferDetails(sb.toString());
			
			*/
			// Map<String, String> attributes = parseXmltoJava("test");
		    Map<String, String> attributes = parseXmltoJavaForGetOfferDetails("");
			logger.info("In GetBalanceAndDate Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

			 /*}catch (Exception ex) {
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
			ex.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}*/
		return insdpDataResponse;
	}
	
	public Map<String, String> parseXmltoJavaForBalanceAndDate(String xml) {
		Map<String, String> attributes = new HashMap<String, String>();
		List<String>accountIdList=new ArrayList<String>();
		List<String>accountValueList=new ArrayList<String>();
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));

			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList nodeLst = doc.getElementsByTagName("member");

			String[] serviceActiveFlagArray = new String[31];
			String[] serviceOfferingIdArray = new String[31];
			String serviceActiveFlag = "";
			String serviceOfferings = "";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String date="";
			for (int s = 0; s < nodeLst.getLength(); s++) {

				Element element = (Element) nodeLst.item(s);
				NodeList name = element.getElementsByTagName("name");
				NodeList value = element.getElementsByTagName("value");
				Element line = (Element) name.item(0);
				Element line1 = (Element) value.item(0).getFirstChild();
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("accountValue1"))
				{
					double val=Double.parseDouble(line1.getFirstChild().getNodeValue());
					double res=roundToDecimals(val/100,2);
					
					attributes.put("Main Account Balance","$"+res);
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("dedicatedAccountValue1"))
				{	
					double val=Double.parseDouble(line1.getFirstChild().getNodeValue());
					double res=roundToDecimals(val/100,2);
					accountValueList.add("$"+res);
					    attributes.put("dedicatedAccountValue1",""+accountValueList);
				}
				
				
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("dedicatedAccountID"))
				{
					accountIdList.add(line1.getFirstChild().getNodeValue());
					
					
					attributes.put("dedicatedAccountID",""+accountIdList);
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("languageIDCurrent"))
				{
					attributes.put("languageIDCurrent",line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("originTransactionID"))
				{
					attributes.put("originTransactionID",line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("responseCode"))
				{
					attributes.put("responseCode",line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceClassCurrent"))
				{
					attributes.put("serviceClassCurrent",line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceFeeExpiryDate"))
				{
					attributes.put("serviceFeeExpiryDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceRemovalDate"))
				{
					attributes.put("serviceRemovalDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("supervisionExpiryDate"))
				{
					attributes.put("supervisionExpiryDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
			
			}
			
			
			
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		logger.info("Attributes================================ ::"+attributes);
		System.out.println("Attributes================="+attributes);
		return attributes;
	}
	
	
	
	public Map<String, String> parseXmltoJavaForGetAccumulators(String xml) {
		logger.info("Response XML================================="+xml);
		Map<String, String> attributes = new HashMap<String, String>();
		List<String>accumulatorIDList=new ArrayList<String>();
		List<String>accumulatorValList=new ArrayList<String>();
		List<String>accumulatorDateList=new ArrayList<String>();
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));

			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList nodeLst = doc.getElementsByTagName("member");
			NodeList nodeLst1 = doc.getElementsByTagName("languageIDCurrent");

		
			//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			System.out.println( nodeLst.getLength());
			String date="";
			for (int s = 0; s < nodeLst.getLength(); s++) {

				Element element = (Element) nodeLst.item(s);
				NodeList name = element.getElementsByTagName("name");
				NodeList value = element.getElementsByTagName("value");
				Element line = (Element) name.item(0);
				Element line1 = (Element) value.item(0).getFirstChild();
				
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("accumulatorID"))
				{
					
					accumulatorIDList.add(line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("accumulatorValue"))
				{
					accumulatorValList.add(line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("accumulatorStartDate"))
				{
					accumulatorDateList.add(line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("languageIDCurrent"))
				{
					
					attributes.put("languageIDCurrent", ""+line1.getFirstChild().getNodeValue());
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceClassCurrent"))
				{
					attributes.put("serviceClassCurrent", ""+line1.getFirstChild().getNodeValue());
				}
			
			}
			
			
			
			
			
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		attributes.put("accumulatorIDList", ""+accumulatorIDList);
		attributes.put("accumulatorValList", ""+accumulatorValList);
		attributes.put("accumulatorDateList", ""+accumulatorDateList);
		logger.info("attributes================================="+attributes);
		return attributes;
	}
	
	
	
	
	public static double roundToDecimals(double d, int c)  
	{   
	   int temp = (int)(d * Math.pow(10 , c));  
	   return ((double)temp)/Math.pow(10 , c);  
	}

	@Override
	public INSDPDataResponse UpdateBalanceAndDate(
			INSDPDataRequest insdpDataRequest) {
		logger.info("In UpdateBalanceAndDate ::");
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		Date supervisionExpiryDate =Util.stringDateFormater(insdpDataRequest.getSupervisionExpiryDate());
		Date serviceFeeExpiryDate =Util.stringDateFormater(insdpDataRequest.getServiceFeeExpiryDate());

		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(
					INSDPDataSoapRequest.UpdateBalanceAndDate,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn(),insdpDataRequest.getAdjustmentAmountRelative(),
					DATE_FORMAT_1.format(supervisionExpiryDate),
					DATE_FORMAT_1.format(serviceFeeExpiryDate)));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("In UpdateBalanceAndDate Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			//ex.printStackTrace();
			logger.info("Exception In UpdateBalanceAndDate ::"
					+ ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				logger.info("Final Exception In UpdateBalanceAndDate ::"
						+ e.getMessage());
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse UpdateAccumulators(
			INSDPDataRequest insdpDataRequest, String requestXml) {
		logger.info("In UpdateAccumulators ::");
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			logger.info("In UpdateAccumulators requestXml::: ::" + requestXml);
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			// wr.writeBytes(String.format(requestXml,insdpDataRequest.getDate(),insdpDataRequest.getMsisdn()));
			wr.writeBytes(String.format(
					INSDPDataSoapRequest.UpdateAccumulators,
					insdpDataRequest.getAccVal(), insdpDataRequest.getAccId(),
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}

			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("In UpdateAccumulators Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("In UpdateAccumulators Exception ::: ::"
					+ ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("In UpdateAccumulators Finally  ::: ::"
						+ e.getMessage());
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}
	
	
	@Override
	public INSDPDataResponse updateDedicatedDABal(INSDPDataRequest insdpDataRequest) {
		logger.info("In UpdateAccumulators ::");
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			// wr.writeBytes(String.format(requestXml,insdpDataRequest.getDate(),insdpDataRequest.getMsisdn()));
			wr.writeBytes(String.format(
					INSDPDataSoapRequest.UpdateDABal,
					 insdpDataRequest.getAccId(),insdpDataRequest.getAccVal(),
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			//
			
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("In UpdateAccumulators Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("In UpdateAccumulators Exception ::: ::"
					+ ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("In UpdateAccumulators Finally  ::: ::"
						+ e.getMessage());
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}
	
	@Override
	public INSDPDataResponse updateOffer(INSDPDataRequest insdpDataRequest) {
		logger.info("In UpdateAccumulators ::");
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			
			/*URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			// wr.writeBytes(String.format(requestXml,insdpDataRequest.getDate(),insdpDataRequest.getMsisdn()));
			wr.writeBytes(String.format(
					INSDPDataSoapRequest.UpdateOffer,DATE_FORMAT_1.format(date),insdpDataRequest.getMsisdn(),insdpDataRequest.getAccId()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			//
*/			String xml="<?xml version=\"1.0\" encoding=\"utf-8\"?><methodResponse><fault><value><struct><member><name>faultCode</name><value><i4>1004</i4></value></member><member><name>faultString</name><value><string>Unknown operation</string></value></member></struct></value></fault></methodResponse> ";
			Map<String, String> attributes = parseXmltoJava(xml);
			logger.info("In UpdateAccumulators Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("In UpdateAccumulators Exception ::: ::"
					+ ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			/*try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("In UpdateAccumulators Finally  ::: ::"
						+ e.getMessage());
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}*/
		}
		return insdpDataResponse;
	}
	
	

	@Override
	public INSDPDataResponse UpdateServiceClass(
			INSDPDataRequest insdpDataRequest) {
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		logger.info("RepositoryImpl In UpdateServiceClass "+insdpDataRequest.getMsisdn());
		logger.info("RepositoryImpl In UpdateServiceClass "+insdpDataRequest.getServiceClassNew());
		logger.info("RepositoryImpl In UpdateServiceClass "+DATE_FORMAT_1.format(date));
		BufferedReader reader = null;
		
		
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(
					INSDPDataSoapRequest.UpdateServiceClass,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn(),
					insdpDataRequest.getServiceClassNew()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);
			}

			logger.info("RepositoryImpl In UpdateServiceClass sb.toString():::: "
					+ sb.toString());
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			// Map<String, String> attributes = parseXmltoJava("updateService");
			logger.info("In UpdateServiceClass Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("Exception In UpdateServiceClass " + ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("Exception In UpdateServiceClass Finally  "
						+ e.getMessage());
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse UpdateSubscriberSegmentation(
			INSDPDataRequest insdpDataRequest, String requestXml) {
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		logger.info("Request XML======================== "+requestXml);
		
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		System.out.println("date====================="+DATE_FORMAT_1.format(date));
		System.out.println("msisdn====================="+insdpDataRequest.getMsisdn());
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(requestXml, DATE_FORMAT_1.format(date),
					insdpDataRequest.getMsisdn()));
			logger.info("Response XMl is ::: " + wr.toString());
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("In UpdateSubscriberSegmentation Attributes size ++++++"
					+ attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("UpdateSubscriberSegmentation  Exception :: "
					+ ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse InstallSubscriber(
			INSDPDataRequest insdpDataRequest, String requestXml) {
		logger.info("RepositoryImpl In InstallSubscriber ");
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(requestXml, DATE_FORMAT_1.format(date),
					insdpDataRequest.getServiceClassNew(),
					insdpDataRequest.getMsisdn(),
					insdpDataRequest.getLanguageIDNew(),insdpDataRequest.getUssdEndOfCallNotificationID()));
			logger.info("Requested XMl is ::: " + wr.toString());
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);
			}
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("InstallSubscriber Size of Map :: " + attributes.size());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			//ex.printStackTrace();
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				logger.info("InstallSubscriber  Exception :: " + e.getMessage());

				//e.printStackTrace();
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse ActivateSubscriber(
			INSDPDataRequest insdpDataRequest) {
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		logger.info("In ActivateSubscriber ++++++");
		BufferedReader reader = null;
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		Date supervisionExpiryDate = Util.stringDateFormater(insdpDataRequest
				.getSupervisionExpiryDate());
		Date serviceFeeExpiryDate = Util.stringDateFormater(insdpDataRequest
				.getServiceFeeExpiryDate());
		logger.info("supervisionExpiryDate=================="
				+ supervisionExpiryDate);
		logger.info("In ActivateSubscriber=================="
				+ serviceFeeExpiryDate);

		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(INSDPDataSoapRequest.activateAccount,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn(),
					insdpDataRequest.getAdjustmentAmountRelative(),
					DATE_FORMAT_1.format(supervisionExpiryDate),
					DATE_FORMAT_1.format(serviceFeeExpiryDate)));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("In ActivateSubscriber Attributes size ++++++"
					+ attributes);
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("ActivateSubscriber  Exception :: " + ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}

	@Override
	public INSDPDataResponse DeleteSubscriber(INSDPDataRequest insdpDataRequest) {
		INSDPDataResponse insdpDataResponse = new INSDPDataResponse();
		BufferedReader reader = null;
		logger.info("In DeleteSubscriber ++++++");
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		try {
			// INSDPDataSoapRequest soapRequest = INSDPDataSoapRequest();
			URLConnection urlconnection = getRequestHeader(insdpDataRequest.getSessionConstants());
			DataOutputStream wr = new DataOutputStream(
					urlconnection.getOutputStream());
			wr.writeBytes(String.format(INSDPDataSoapRequest.deleteSubscriber,
					DATE_FORMAT_1.format(date), insdpDataRequest.getMsisdn()));
			wr.flush();
			wr.close();

			reader = new BufferedReader(new InputStreamReader(
					urlconnection.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = reader.readLine()) != null) {
				sb.append(decodedString);

			}
			Map<String, String> attributes = parseXmltoJava(sb.toString());
			logger.info("response XML==========================="+ sb.toString());
			if (attributes.size() > 0) {
				
				insdpDataResponse.setAttributes(attributes);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("DeleteSubscriber Exception : " + ex.getMessage());
			insdpDataResponse.setApplicationError(new ApplicationError(ex.getMessage()));
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info("DeleteSubscriber IOException : " + e.getMessage());
				insdpDataResponse.setApplicationError(new ApplicationError(e.getMessage()));
			}
		}
		return insdpDataResponse;
	}

	public URLConnection getRequestHeader(SessionConstants sessionConstants) {
		URLConnection urlConnection = null;
		URL url;
		try {

			url = new URL("http://"+ SessionConstantsFactory.getSessionConstant("INSDP_API_URL", sessionConstants) +"/Air");

			urlConnection = url.openConnection();

			urlConnection.addRequestProperty("Authorization",
					Constants.INSDP_BASIC_AUTH + Constants.INSDP_USER_PASS);
			urlConnection.addRequestProperty("User-Agent", "Server/3.1.06/1.0");
			urlConnection.addRequestProperty("Content-Type", "text/xml");
			urlConnection.addRequestProperty("Host", SessionConstantsFactory.getSessionConstant("INSDP_HOST", sessionConstants));
			urlConnection.addRequestProperty("Connection", "keep-alive");
			urlConnection.setDoOutput(true);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("In getRequestHeader +++++" + e.getMessage());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info("In getRequestHeader +++++" + e.getMessage());

		}
		return urlConnection;
	}

	public Map<String, String> parseXmltoJava(String xml) {
		Map<String, String> attributes = new HashMap<String, String>();
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element "
					+ doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("member");
			logger.info("Information of all member" + nodeLst.getLength());
			
			
			for (int s = 0; s < nodeLst.getLength(); s++) {

				Element element = (Element) nodeLst.item(s);
				NodeList name = element.getElementsByTagName("name");
				NodeList value = element.getElementsByTagName("value");
				Element line = (Element) name.item(0);
				Element line1 = (Element) value.item(0).getFirstChild();
				attributes.put(getCharacterDataFromElement(line),
						getCharacterDataFromElement(line1));
				logger.info("line::" + getCharacterDataFromElement(line));
				logger.info("value;::" + getCharacterDataFromElement(line1));
				
				
			}
			

		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
			e.printStackTrace();
		}
		return attributes;
	}
	
	
	public Map<String, String> parseXmltoJavaForGetAccountDetails(String xml) {
		Map<String, String> attributes = new HashMap<String, String>();
		String serviceOfferings = "";
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element "
					+ doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("member");
			logger.info("Information of all member" + nodeLst.getLength());
			
			
			for (int s = 0; s < nodeLst.getLength(); s++) {

				Element element = (Element) nodeLst.item(s);
				NodeList name = element.getElementsByTagName("name");
				NodeList value = element.getElementsByTagName("value");
				Element line = (Element) name.item(0);
				Element line1 = (Element) value.item(0).getFirstChild();
				attributes.put(getCharacterDataFromElement(line),
						getCharacterDataFromElement(line1));
				logger.info("line::" + getCharacterDataFromElement(line));
				logger.info("value;::" + getCharacterDataFromElement(line1));
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceOfferingActiveFlag"))
				{
					
					serviceOfferings=serviceOfferings+line1.getFirstChild().getNodeValue();
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("activationDate"))
				{
					attributes.put("activationDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
					
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("creditClearanceDate"))
				{
					
					attributes.put("creditClearanceDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceFeeExpiryDate"))
				{
					
					attributes.put("serviceFeeExpiryDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("supervisionExpiryDate"))
				{
					
					attributes.put("supervisionExpiryDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
				
				if(line.getFirstChild().getNodeValue().equalsIgnoreCase("serviceRemovalDate"))
				{
					
					attributes.put("serviceRemovalDate",""+JavaUtil.converStringDateToDate(line1.getFirstChild().getNodeValue()));
				}
				
			}
			if(serviceOfferings!="")
			{
			String reverse = new StringBuffer(serviceOfferings).reverse().toString();
			 System.out.println(Integer.parseInt(reverse, 2)+" ("+serviceOfferings+")");
			attributes.put("serviceOfferings", Integer.parseInt(reverse, 2)+"("+reverse+")");
			}

		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
			e.printStackTrace();
		}
		return attributes;
	}

	
	
	
	public Map<String, String> parseXmltoJavaForGetOfferDetails(String xml) {
		Multimap<String, String> attributes =  ArrayListMultimap.create();
		Map<String,String>dataMap=new HashMap<String, String>();
		boolean expiryDateTimeFlag=false;
		boolean offerIDFlag=false;
		boolean offerStateFlag=false;
		boolean offerTypeFlag=false;
		boolean startDateTimeFlag=false;
		boolean productIdFlag=false;
		List<String>expiryDateTimeList=new ArrayList<String>();
		List<String> offerIDList=new ArrayList<String>();
		List<String>offerStateList=new ArrayList<String>();
		List<String>offerTypeList=new ArrayList<String>();
		List<String>startDateTimeList=new ArrayList<String>();
		List<String>productIdList=new ArrayList<String>();
		
		xml="<?xml version=\"1.0\" encoding=\"utf-8\"?> " + 
				 "<methodResponse> " + 
				 "<params> " + 
				 "<param> " + 
				 "<value> " + 
				 "<struct> " + 
				 "<member> " + 
				 "<name>offerInformation</name> " + 
				 "<value> " + 
				 "<array> " + 
				 "<data> " + 
				 "<value> " + 
				 "<struct> " + 
				 "<member> " + 
				 "<name>expiryDateTime</name> " + 
				 "<value><dateTime.iso8601>99991231T00:00:00+1200</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerID</name> " + 
				 "<value><i4>50001</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerState</name> " + 
				 "<value><i4>0</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerType</name> " + 
				 "<value><i4>2</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>startDateTime</name> " + 
				 "<value><dateTime.iso8601>20150127T19:50:35-0500</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "</struct> " + 
				 "</value> " + 
				 "<value> " + 
				 "<struct> " + 
				 "<member> " + 
				 "<name>expiryDateTime</name> " + 
				 "<value><dateTime.iso8601>99991231T00:00:00+1200</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerID</name> " + 
				 "<value><i4>60001</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerState</name> " + 
				 "<value><i4>0</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerType</name> " + 
				 "<value><i4>2</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>productID</name> " + 
				 "<value><i4>1</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>startDateTime</name> " + 
				 "<value><dateTime.iso8601>20150127T19:50:45-0500</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "</struct> " + 
				 "</value> " + 
				 "<value> " + 
				 "<struct> " + 
				 "<member> " + 
				 "<name>expiryDateTime</name> " + 
				 "<value><dateTime.iso8601>99991231T00:00:00+1200</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerID</name> " + 
				 "<value><i4>70001</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerState</name> " + 
				 "<value><i4>0</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerType</name> " + 
				 "<value><i4>2</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>productID</name> " + 
				 "<value><i4>2</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>startDateTime</name> " + 
				 "<value><dateTime.iso8601>20150127T19:50:55-0500</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "</struct> " + 
				 "</value> " + 
				 "<value> " + 
				 "<struct> " + 
				 "<member> " + 
				 "<name>expiryDateTime</name> " + 
				 "<value><dateTime.iso8601>99991231T00:00:00+1200</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerID</name> " + 
				 "<value><i4>80001</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerState</name> " + 
				 "<value><i4>0</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerType</name> " + 
				 "<value><i4>2</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>productID</name> " + 
				 "<value><i4>3</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>startDateTime</name> " + 
				 "<value><dateTime.iso8601>20150127T19:51:03-0500</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "</struct> " + 
				 "</value> " + 
				 "<value> " + 
				 "<struct> " + 
				 "<member> " + 
				 "<name>expiryDateTime</name> " + 
				 "<value><dateTime.iso8601>99991231T00:00:00+1200</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerID</name> " + 
				 "<value><i4>90001</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerState</name> " + 
				 "<value><i4>0</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>offerType</name> " + 
				 "<value><i4>2</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>productID</name> " + 
				 "<value><i4>4</i4></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>startDateTime</name> " + 
				 "<value><dateTime.iso8601>20150127T19:51:13-0500</dateTime.iso8601></value> " + 
				 "</member> " + 
				 "</struct> " + 
				 "</value> " + 
				 "</data> " + 
				 "</array> " + 
				 "</value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>originTransactionID</name> " + 
				 "<value><string>000006</string></value> " + 
				 "</member> " + 
				 "<member> " + 
				 "<name>responseCode</name> " + 
				 "<value><i4>0</i4></value> " + 
				 "</member> " + 
				 "</struct> " + 
				 "</value> " + 
				 "</param> " + 
				 "</params> " + 
				 "</methodResponse> ";
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));

			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList nodeLst = doc.getElementsByTagName("member");
			/*NodeList nodeLst1 = doc.getElementsByTagName("languageIDCurrent");*/

		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			System.out.println( nodeLst.getLength());
			String date="";
			int count=1;
			for (int s = 1; s < nodeLst.getLength(); s++) {
				
				Element element = (Element) nodeLst.item(s);
				NodeList name = element.getElementsByTagName("name");
				NodeList value = element.getElementsByTagName("value");
				NodeList i4 = element.getElementsByTagName("i4");
				NodeList expiryDate = element.getElementsByTagName("dateTime.iso8601");
				
				
				Element line =null;
				Element line1=null;
				
				count=count+1;
				if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("expiryDateTime"))
				{
					
					expiryDateTimeFlag=true;
					expiryDateTimeList.add(JavaUtil.converStringDateToDate(expiryDate.item(0).getFirstChild().getNodeValue()));
				}else if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("offerID"))
				{
					offerIDFlag=true;
					offerIDList.add(i4.item(0).getFirstChild().getNodeValue());
				}else if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("offerState"))
				{
					offerStateFlag=true;
					offerStateList.add(i4.item(0).getFirstChild().getNodeValue());
				}else if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("offerType"))
				{
					offerTypeFlag=true;
					
					offerTypeList.add(i4.item(0).getFirstChild().getNodeValue());
				}else if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("startDateTime"))
				{
					startDateTimeFlag=true;
					startDateTimeList.add(JavaUtil.converStringDateToDate(expiryDate.item(0).getFirstChild().getNodeValue()));
				}else if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("productID"))
				{
					productIdFlag=true;
					productIdList.add(i4.item(0).getFirstChild().getNodeValue());
				}
				
				if(count==6)
				{
				if(name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("productID") || name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("startDateTime") || name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("offerType")||name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("offerState")||name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("offerID")||name.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("expiryDateTime"))
				{
					if(expiryDateTimeFlag==false)
					{
						expiryDateTimeList.add("");
					}
					
					if(offerIDFlag==false)
					{
						offerIDList.add("");
					}
					if(offerStateFlag==false)
					{
						offerStateList.add("");
					}
					if(offerTypeFlag==false)
					{
						offerTypeList.add("");
					}
					if(startDateTimeFlag==false)
					{
						startDateTimeList.add("");
					}
					if(productIdFlag==false)
					{
						productIdList.add("");
					}
				}
				expiryDateTimeFlag=true;offerIDFlag=true;offerStateFlag=true;offerTypeFlag=true;startDateTimeFlag=true;productIdFlag=true;
				}
					
			
			}
			 
			
		
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		dataMap.put("expiryDateTimeList", ""+expiryDateTimeList);
		dataMap.put("offerIDList", ""+offerIDList);
		dataMap.put("offerStateList", ""+offerStateList);
		dataMap.put("offerTypeList", ""+offerTypeList);
		dataMap.put("startDateTimeList", ""+startDateTimeList);
		dataMap.put("productIdList", ""+productIdList);
		System.out.println("Attributes================="+dataMap);
		return dataMap;
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "-";
	}
}
