package com.tmobile.edat.insdp.repository;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tmobile.edat.util.Constants;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

public class INSDPDataHandler {
	// user input MSISDN
	static String GetAccountDetails = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<methodCall>"
			+ "<methodName>GetAccountDetails</methodName> "
			+ "    <params> "
			+ "        <param> "
			+ "            <value> "
			+ "                <struct> "
			+ "                    <member> "
			+ "                        <name>originTransactionID</name> "
			+ "                        <value> "
			+ "                            <string>613504</string> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>originTimeStamp</name> "
			+ "                        <value> "
			+ "                            <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>subscriberNumber</name> "
			+ "                        <value> "
			+ "                            <string>%s</string> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>originNodeType</name> "
			+ "                        <value> "
			+ "                            <string>ESP</string> "
			+ "                        </value> "
			+ "                    </member> "
			+ "                    <member> "
			+ "                        <name>originHostName</name> "
			+ "                        <value> "
			+ "                            <string>ESP</string> "
			+ "                        </value> "
			+ "                    </member> "
		
			+ "                </struct> "
			+ "            </value> "
			+ "        </param> "
			+ "    </params> "
			+ "</methodCall> ";

	// user input MSISDN , Ammount, DATE
	String UpdateBalanceAndDate = "<?xml version=\"1.0\"?> "
			+ "<methodCall> "
			+ "<methodName>UpdateBalanceAndDate</methodName> "
			+ " <params> "
			+ "  <param> "
			+ "   <value> "
			+ "    <struct> "
			+ "     <member> "
			+ "      <name>originNodeType</name> "
			+ "       <value> "
			+ "        <string>PAG</string> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>originHostName</name> "
			+ "       <value> "
			+ "        <string>PAG</string> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>originTransactionID</name> "
			+ "       <value> "
			+ "        <string>12345</string> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>originTimeStamp</name> "
			+ "       <value> "
			+ "        <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "       </value> "
			+ "     </member> "
			+ "     <member> "
			+ "      <name>subscriberNumber</name> "
			+ "       <value> "
			+ "        <string>%s</string> "// 7874357106 4047516050
			+ "       </value> "
			+ "     </member> "
			+ "# transactionCurrency : Optional but Mandatory if adjust change any account # "
			+ "     <member> "
			+ "      <name>transactionCurrency</name> "
			+ "      <value> "
			+ "       <string>USD</string> "
			+ "      </value> "
			+ "     </member> "
			+ "# adjustmentAmountRelative : Optional # "
			+ "     <member> "
			+ "      <name>adjustmentAmountRelative</name> "
			+ "      <value><string>20000</string></value> "
			+ "     </member> "
			+ "# supervisionExpiryDate : Optional #      "
			+ "     <member> "
			+ "      <name>supervisionExpiryDate</name> "// dynamic
			+ "       <value> "
			+ "        <dateTime.iso8601>20151231T11:15:21-0500</dateTime.iso8601> "
			+ "       </value> "
			+ "     </member> "
			+ "# serviceFeeExpiryDate : Optional #      "
			+ "     <member> "
			+ "      <name>serviceFeeExpiryDate</name> "// dynamic
			+ "       <value> "
			+ "        <dateTime.iso8601>20151231T11:15:21-0500</dateTime.iso8601> "
			+ "       </value> " + "     </member> "
			+ "# creditClearancePeriod : Optional # " 
			+ "     <member> "
			+ "      <name>creditClearancePeriod</name> "
			+ "      <value><int>0</int></value> " 
			+ "     </member> "
			+ "# serviceRemovalPeriod : Optional # " 
			+ "     <member> "
			+ "      <name>serviceRemovalPeriod</name> "
			+ "      <value><int>0</int></value> "
			+ "     </member> "
			+ "    </struct> " 
			+ "   </value> " 
			+ "  </param> " 
			+ " </params> "
			+ "</methodCall> ";

	// user input MSISDN , Ammount, DATE
	String GetBalanceAndDate = "<?xml version=\"1.0\"?>"
			+ "<methodCall>"
			+ "<methodName>GetBalanceAndDate</methodName>"
			+ "<params>"
			+ "	<param>"
			+ "		<value>"
			+ "			<struct>"
			+ "				<member>"
			+ "					<name>originTransactionID</name>"
			+ "					<value>"
			+ "						<string>208100234618394636</string>"
			+ "					</value>"
			+ "				</member>"
			+ "				<member>"
			+ "					<name>originTimeStamp</name>" // dynamic current time
			+ "					<value>"
			+ "						<dateTime.iso8601>%S</dateTime.iso8601>"
			+ "					</value>" 
			+ "				</member>" 
			+ "				<member>"
			+ "					<name>originHostName</name>" 
			+ "					<value>"
			+ "						<string>epsi</string>" 
			+ "					</value>" 
			+ "				</member>"
			+ "				<member>" 
			+ "					<name>originNodeType</name>"
			+ "					<value><string>ADM</string></value>" 
			+ "				</member>"
			+ "				<member>" 
			+ "					<name>subscriberNumber</name>" // dynamic
			+ "					<value><string>%s</string></value>" 
			+ "				</member>"
			+ "			</struct>" 
			+ "		</value>" 
			+ "	</param>" 
			+ "</params>"
			+ "</methodCall>";

	String InstallSubscriber = "<?xml version=\"1.0\"?>"
			+ "<methodCall> "
			+ "<methodName>InstallSubscriber</methodName> "
			+ "<params> "
			+ "<param> "
			+ "<value> "
			+ "<struct> "
			+ "<member> "
			+ "<name>originHostName</name> "
			+ "<value> "
			+ "<string>epsi</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originNodeType</name> "
			+ "                  <value> "
			+ "                     <string>ADM</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originTransactionID</name> "
			+ "                  <value> "
			+ "                     <string>156295616207844130</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originTimeStamp</name> " // dynamic current time
			+ "                  <value> "
			+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>serviceClassNew</name> "
			+ "                  <value> "
			+ "                     <int>80</int> "// dynamic
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originOperatorID</name> "
			+ "                  <value> "
			+ "                     <string>add</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>subscriberNumber</name> "// dynamic
			+ "                  <value> "
			+ "                     <string>%s</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>languageIDNew</name> "// dynamic
			+ "                  <value> "
			+ "                     <int>1</int> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>serviceOfferings</name> "
			+ "                  <value> "
			+ "                     <array> "
			+ "                        <data> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> " // dynamic upto 20
			+ "                                    <value> "
			+ "                                       <int>1</int> "// dynamic
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "// dynamic
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>2</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>3</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>4</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>5</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>6</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>7</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>8</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>9</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>10</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>11</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>12</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>13</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>14</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>15</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>16</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>17</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>18</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>19</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>20</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                        </data> "
			+ "                     </array> " 
			+ "                  </value> "
			+ "               </member> " 
			+ "               <member> "
			+ "                  <name>ussdEndOfCallNotificationID</name> "// dynamic
			+ "                  <value> "
			+ "                     <int>6</int> "
			+ "                  </value> " 
			+ "               </member> "
			+ "            </struct> " + "         </value> "
			+ "      </param> " + "   </params> " + "</methodCall> ";

	String UpdateSubscriberSegmentation = "<?xml version=\"1.0\"?> "
			+ "<methodCall> "
			+ "   <methodName>UpdateSubscriberSegmentation</methodName> "
			+ "   <params> "
			+ "      <param> "
			+ "         <value> "
			+ "            <struct> "
			+ "               <member> "
			+ "                  <name>originTransactionID</name> "
			+ "                  <value> "
			+ "                     <string>056123662849772791</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>serviceOfferings</name> "
			+ "                  <value> "
			+ "                     <array> "
			+ "                        <data> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "// dynamic its upto 20
			+ "                                    <value> "
			+ "                                       <int>1</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "// dynamic
			+ "                                    <value> "
			+ "                                       <boolean>1</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>4</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>5</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>6</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>10</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>27</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>1</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>30</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingID</name> "
			+ "                                    <value> "
			+ "                                       <int>31</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>serviceOfferingActiveFlag</name> "
			+ "                                    <value> "
			+ "                                       <boolean>0</boolean> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                        </data> "
			+ "                     </array> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originTimeStamp</name> " // dynamic current time
			+ "                  <value> "
			+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originHostName</name> "
			+ "                  <value> "
			+ "                     <string>epsi</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originNodeType</name> "
			+ "                  <value> "
			+ "                     <string>ADM</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originOperatorID</name> "
			+ "                  <value> "
			+ "                     <string>seattle</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>subscriberNumber</name> "// dynamic
			+ "                  <value> "
			+ "                     <string>%s</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "            </struct> " 
			+ "         </value> "
			+ "      </param> " + "   </params> " + "</methodCall> ";

	String UpdateAccumulators = "<?xml version=\"1.0\"?> "
			+ "<methodCall> "
			+ "   <methodName>UpdateAccumulators</methodName> "
			+ "   <params> "
			+ "      <param> "
			+ "         <value> "
			+ "            <struct> "
			+ "               <member> "
			+ "                  <name>originTransactionID</name> "
			+ "                  <value> "
			+ "                     <string>156295616207844130</string> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>accumulatorInformation</name> "
			+ "                  <value> "
			+ "                     <array> "
			+ "                        <data> "
			+ "                           <value> "
			+ "                              <struct> "
			+ "                                 <member> "
			+ "                                    <name>accumulatorValueAbsolute</name> "// dynamic
			+ "                                    <value> "
			+ "                                       <int>2</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                                 <member> "
			+ "                                    <name>accumulatorID</name> " // dynamic
			+ "                                    <value> "
			+ "                                       <int>4</int> "
			+ "                                    </value> "
			+ "                                 </member> "
			+ "                              </struct> "
			+ "                           </value> "
			+ "                        </data> "
			+ "                     </array> "
			+ "                  </value> "
			+ "               </member> "
			+ "               <member> "
			+ "                  <name>originTimeStamp</name> "// dynamic current time
			+ "                  <value> "
			+ "                     <dateTime.iso8601>%s</dateTime.iso8601> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originHostName</name> "
			+ "                  <value> "
			+ "                     <string>epsi</string> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originNodeType</name> "
			+ "                  <value> "
			+ "                     <string>ADM</string> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>originOperatorID</name> "
			+ "                  <value> "
			+ "                     <string>add</string> "
			+ "                  </value> " + "               </member> "
			+ "               <member> "
			+ "                  <name>subscriberNumber</name> "// dynamic
			+ "                  <value> "
			+ "                     <string>%s</string> "
			+ "                  </value> " 
			+ "               </member> "
			+ "            </struct> " 
			+ "         </value> "
			+ "      </param> " + "   </params> " + "</methodCall> ";

	String UpdateServiceClass = "<?xml version=\"1.0\"?> "
			+ "<methodCall> "
			+ "<methodName>UpdateServiceClass</methodName> "
			+ "	<params> "
			+ "		<param> "
			+ "			<value> "
			+ "				<struct> "
			+ "					<member> "
			+ "						<name>originNodeType</name> "
			+ "							<value> "
			+ "								<string>PAG</string> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>originHostName</name> "
			+ "							<value> "
			+ "								<string>PAG</string> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>originTransactionID</name> "
			+ "							<value> "
			+ "								<string>12345</string> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>originTimeStamp</name> "// dynamic
			+ "							<value> "
			+ "								<dateTime.iso8601>%s</dateTime.iso8601> "
			+ "							</value> "
			+ "					</member> "
			+ "					<member> "
			+ "						<name>subscriberNumber</name> "// dynamic
			+ "							<value> "
			+ "								<string>%s</string> " 
			+ "							</value> "
			+ "					</member> " 
			+ "					<member> "
			+ "						<name>serviceClassAction</name> " 
			+ "							<value> "
			+ "								<string>SetOriginal</string> " 
			+ "							</value> "
			+ "					</member> " 
			+ "						<name>serviceClassNew</name> "// dynamic
			+ "							<value> " 
			+ "								<int>92</int> " 
			+ "							</value> "
			+ "					</member> "
			+ "				</struct> " 
			+ "			</value> "
			+ "		</param> " + "	</params> " + "</methodCall> ";

	String activateAccount  ="<?xml version=\"1.0\"?> " + 
			 "<methodCall> " + 
			 "<methodName>UpdateBalanceAndDate</methodName> " + 
			 " <params> " + 
			 "  <param> " + 
			 "   <value> " + 
			 "    <struct> " + 
			 "     <member> " + 
			 "      <name>originNodeType</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originHostName</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTransactionID</name> " + 
			 "       <value> " + 
			 "        <string>12345</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTimeStamp</name> " + // dynamic current time
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>subscriberNumber</name> " + // dynamic
			 "       <value> " + 
			 "        <string>%s</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# transactionCurrency : Optional but Mandatory if adjust change any account # " + 
			 "     <member> " + 
			 "      <name>transactionCurrency</name> " + 
			 "      <value> " + 
			 "       <string>USD</string> " + 
			 "      </value> " + 
			 "     </member> " + 
			 "# adjustmentAmountRelative : Optional # " + 
			 "     <member> " + 
			 "      <name>adjustmentAmountRelative</name> " + // dynamic
			 "      <value><string>10000</string></value> " + 
			 "     </member> " + 
			 "# supervisionExpiryDate : Optional #      " + 
			 "     <member> " + 
			 "      <name>supervisionExpiryDate</name> " + // dynamic date
			 "       <value> " + 
			 "        <dateTime.iso8601>20151231T11:15:21-0500</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# serviceFeeExpiryDate : Optional #      " + 
			 "     <member> " + 
			 "      <name>serviceFeeExpiryDate</name> " + // dynamic date
			 "       <value> " + 
			 "        <dateTime.iso8601>20151231T11:15:21-0500</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "# creditClearancePeriod : Optional # " + 
			 "     <member> " + 
			 "      <name>creditClearancePeriod</name> " + 
			 "      <value><int>0</int></value> " + 
			 "     </member> " + 
			 "# serviceRemovalPeriod : Optional # " + 
			 "     <member> " + 
			 "      <name>serviceRemovalPeriod</name> " + 
			 "      <value><int>0</int></value> " + 
			 "     </member> " + 
			 "    </struct> " + 
			 "   </value> " + 
			 "  </param> " + 
			 " </params> " + 
			 "</methodCall> "; 
	private String deleteSubscriber="<?xml version=\"1.0\"?> " + 
			 "<methodCall> " + 
			 "<methodName>DeleteSubscriber</methodName> " + 
			 " <params> " + 
			 "  <param> " + 
			 "   <value> " + 
			 "    <struct> " + 
			 "     <member> " + 
			 "      <name>originNodeType</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originHostName</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTransactionID</name> " + 
			 "       <value> " + 
			 "        <string>12345</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originTimeStamp</name> " + // dynamic Current time
			 "       <value> " + 
			 "        <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>subscriberNumber</name> " + // dynamic
			 "       <value> " + 
			 "        <string>%s</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "     <member> " + 
			 "      <name>originOperatorID</name> " + 
			 "       <value> " + 
			 "        <string>PAG</string> " + 
			 "       </value> " + 
			 "     </member> " + 
			 "    </struct> " + 
			 "   </value> " + 
			 "  </param> " + 
			 " </params> " + 
			 "</methodCall> ";
	
	private String getAccumulators="<?xml version=\"1.0\"?> " + 
 "<methodCall> " + 
 "  <methodName>GetAccumulators</methodName> " + 
 "  <params> " + 
 "    <param> " + 
 "      <value> " + 
 "        <struct> " + 
 "          <member> " + 
 "            <name>originNodeType</name> " + 
 "            <value> " + 
 "              <string>EXT</string> " + 
 "            </value> " + 
 "          </member> " + 
 "          <member> " + 
 "            <name>originHostName</name> " + 
 "            <value> " + 
 "              <string>10767714</string> " + 
 "            </value> " + 
 "          </member> " + 
 "          <member> " + 
 "            <name>originTransactionID</name> " + 
 "            <value> " + 
 "              <string>100010000010001236</string> " + 
 "            </value> " + 
 "          </member> " + 
 "          <member> " + 
 "            <name>originTimeStamp</name> " + 
 "            <value> " + 
 "              <dateTime.iso8601>%s</dateTime.iso8601> " + 
 "            </value> " + 
 "          </member> " + 
 "          <member> " + 
 "            <name>subscriberNumber</name> " + 
 "            <value> " + 
 "              <string>%s</string> " + 
 "            </value> " + 
 "          </member> " + 
 "        </struct> " + 
 "      </value> " + 
 "    </param> " + 
 "  </params> " + 
 "</methodCall> ";
	
	
	private String updateDA="<methodCall> " + 
			 "   <methodName>UpdateBalanceAndDate</methodName> " + 
			 "   <params> " + 
			 "      <param> " + 
			 "         <value> " + 
			 "            <struct> " + 
			 "               <member> " + 
			 "                  <name>dedicatedAccountUpdateInformation</name> " + 
			 "                  <value> " + 
			 "                     <array> " + 
			 "                        <data> " + 
			 "                           <value> " + 
			 "                              <struct> " + 
			 "                                 <member> " + 
			 "                                    <name>dedicatedAccountID</name> " + 
			 "                                    <value> " + 
			 "                                       <int>3</int> " + 
			 "                                    </value> " + 
			 "                                 </member> " + 
			 "                                 <member> " + 
			 "                                    <name>dedicatedAccountValueNew</name> " + 
			 "                                    <value> " + 
			 "                                       <string>10000</string> " + 
			 "                                    </value> " + 
			 "                                 </member> " + 
			 "                              </struct> " + 
			 "                           </value> " + 
			 "                        </data> " + 
			 "                     </array> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>originHostName</name> " + 
			 "                  <value> " + 
			 "                     <string>epsi</string> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>originNodeType</name> " + 
			 "                  <value> " + 
			 "                     <string>ADM</string> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>originTransactionID</name> " + 
			 "                  <value> " + 
			 "                     <string>15612958563484735</string> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>originTimeStamp</name> " + 
			 "                  <value> " + 
			 "                     <dateTime.iso8601>20110124T10:05:51+0200</dateTime.iso8601> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>transactionCurrency</name> " + 
			 "                  <value> " + 
			 "                     <string>USD</string> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>originOperatorID</name> " + 
			 "                  <value> " + 
			 "                     <string>add</string> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "               <member> " + 
			 "                  <name>subscriberNumber</name> " + 
			 "                  <value> " + 
			 "                     <string>4256496831</string> " + 
			 "                  </value> " + 
			 "               </member> " + 
			 "            </struct> " + 
			 "         </value> " + 
			 "      </param> " + 
			 "   </params> " + 
			 "</methodCall> ";
	
	private String updateOffer="<methodCall> " + 
			 "  <methodName>UpdateOffer</methodName> " + 
			 "  <params> " + 
			 "    <param> " + 
			 "      <value> " + 
			 "        <struct> " + 
			 "          <member> " + 
			 "            <name>originHostName</name> " + 
			 "            <value> " + 
			 "              <string>epsi</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originNodeType</name> " + 
			 "            <value> " + 
			 "              <string>ADM</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originTransactionID</name> " + 
			 "            <value> " + 
			 "              <string>159316052100191158</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originTimeStamp</name> " + //dynamic currentDate
			 "            <value> " + 
			 "              <dateTime.iso8601>%s</dateTime.iso8601> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>originOperatorID</name> " + 
			 "            <value> " + 
			 "              <string>add</string> " + 
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>subscriberNumber</name> " + //dynamic
			 "            <value> " + 
			 "              <string>%s</string> " + //3138981514
			 "            </value> " + 
			 "          </member> " + 
			 "          <member> " + 
			 "            <name>offerID</name> " + //dynamic
			 "            <value> " + 
			 "              <int></int> " + //50001
			 "            </value> " + 
			 "          </member> " + 
			 "        </struct> " + 
			 "      </value> " + 
			 "    </param> " + 
			 "  </params> " + 
			 "</methodCall> ";
	
	private String getOffer="<?xml version=\"1.0\" encoding=\"utf-8\"?> " + 
 "<methodCall> " + 
 "<methodName>GetOffers</methodName> " + 
 "<params> " + 
 "<param> " + 
 "<value> " + 
 "<struct> " + 
 "<member> " + 
 "<name>originHostName</name> " + 
 "<value> " + 
 "<string>TEST</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>originNodeType</name> " + 
 "<value> " + 
 "<string>AIR</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>originTimeStamp</name> " + 
 "<value> " + 
 "<dateTime.iso8601>20130125T19:21:58+0000</dateTime.iso8601> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>originTransactionID</name> " + 
 "<value> " + 
 "<string>000006</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>requestInactiveOffersFlag</name> " + 
 "<value> " + 
 "<boolean>1</boolean> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>offerRequestedTypeFlag</name> " + 
 "<value> " + 
 "<string>11111111</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>subscriberNumber</name> " + 
 "<value> " + 
 "<string>3138981502</string> " + 
 "</value> " + 
 "</member> " + 
 "<member> " + 
 "<name>subscriberNumberNAI</name> " + 
 "<value> " + 
 "<i4>2</i4> " + 
 "</value> " + 
 "</member> " + 
 "</struct> " + 
 "</value> " + 
 "</param> " + 
 "</params> " + 
 "</methodCall> ";
	
	public void hitServer(int type, String date) {
		try {
			String stringUrl = "http://10.168.251.148:10010/Air";
			URL url = new URL(stringUrl);
			URLConnection uc = url.openConnection();
			//URLConnection uc = null;
			//String userpass = "dXNlcjp1c2Vy";
			String userpass ="dXNlcjp1c2Vy";
			String basicAuth = "Basic ";

			uc.addRequestProperty("Authorization", basicAuth + userpass);
			uc.addRequestProperty("User-Agent", "Server/3.1.06/1.0");
			uc.addRequestProperty("Content-Type", "text/xml");
			uc.addRequestProperty("Host", "10.25.76.213:10010");
			uc.addRequestProperty("Connection", "keep-alive");
			uc.setDoOutput(true);
			//DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
			DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
			switch (type) {
			case 1:
				// System.out.println(String.format(GetAccountDetails,date,
				// "14259089740"));
				wr.writeBytes(String.format(GetAccountDetails, date,
						"8887707580"));
				break;
			case 2:
				wr.writeBytes(String.format(GetBalanceAndDate, date,
						"8887707580"));// 7874357106
				break;
			case 3:
				wr.writeBytes(String.format(UpdateBalanceAndDate, date,
						"8887707580")); // 7874357106
										// 4047516050
				break;
			case 4:
				wr.writeBytes(String.format(UpdateAccumulators, date,
						"8887707580"));//
				break;
			case 5:
				wr.writeBytes(String.format(UpdateServiceClass, date,
						"4257707469"));//
				System.out.println("WC is ::"+wr);
				break;
			case 6:

				wr.writeBytes(String.format(UpdateSubscriberSegmentation, date,
						"8887707580"));//
				break;
			case 7:
				wr.writeBytes(String.format(InstallSubscriber, date,
						"8887707580"));// 5557707580
				break;
			case 8:
				wr.writeBytes(String.format(activateAccount, date,
						"8887707580"));// 5557707580
				break;
			case 9:
				wr.writeBytes(String.format(deleteSubscriber, date,
						"8887707580"));// 5557707580
				break;
				
			case 10:
				wr.writeBytes(String.format(getAccumulators, date,
						"4259089746"));// 5557707580
				break;
			
			case 11:
				wr.writeBytes(String.format(updateDA, date,
						"4259089746"));// 5557707580
				break;
				
			case 12:
				wr.writeBytes(String.format(updateOffer, date,
						"3138981514",50001));// 5557707580
				break;
				
			case 13:
				wr.writeBytes(String.format(getOffer, date,
						"3138981502"));// 5557707580
				break;
			
				
			default:
				break;
			}

			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					uc.getInputStream()));
			String decodedString;
			StringBuffer sb = new StringBuffer();
			while ((decodedString = in.readLine()) != null) {
				sb.append(decodedString);

			}
			System.out.println("Response:\n" + sb.toString());
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String args[]) throws IOException {

		// 20141014T10:05:14:47-0700
		// Instantiate a Date object
		Date date = new Date();
		DateFormat DATE_FORMAT_1 = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
		// display time and date using toString()
		System.out.println(DATE_FORMAT_1.format(date));

		INSDPDataHandler handler = new INSDPDataHandler();
		//System.out.println("--------GetAccountDetails---------");
		//handler.hitServer(1, DATE_FORMAT_1.format(date));// GetAccountDetails
		
		//System.out.println("--------GetBalanceAndDate---------");
		// handler.hitServer(2,DATE_FORMAT_1.format(date));//GetBalanceAndDate
		
		// New requirements  
		//System.out.println("--------UpdateBalanceAndDate---------");
		// handler.hitServer(3,DATE_FORMAT_1.format(date));//UpdateBalanceAndDate
		
		//System.out.println("--------UpdateAccumulators---------");
		// handler.hitServer(4,DATE_FORMAT_1.format(date));//UpdateAccumulators
		
		//System.out.println("--------UpdateServiceClass---------");
		//handler.hitServer(5,DATE_FORMAT_1.format(date));//UpdateServiceClass
		
		//System.out.println("--------UpdateSubscriberSegmentation---------");
		//handler.hitServer(6,DATE_FORMAT_1.format(date));//UpdateSubscriberSegmentation
		
		//System.out.println("--------InstallSubscriber---------");
		// handler.hitServer(7,DATE_FORMAT_1.format(date));// InstallSubscriber
		
		//System.out.println("--------Activate subscriber---------");
		//handler.hitServer(8,DATE_FORMAT_1.format(date));// Activate subscriber
		
		//System.out.println("--------Delete subscriber---------");
		// handler.hitServer(9,DATE_FORMAT_1.format(date));//Delete subscriber
		
		//System.out.println("--------Get Accumlators---------");
		//handler.hitServer(10,DATE_FORMAT_1.format(date));//Delete subscriber
		//System.out.println("--------Get Accumlators---------");
		// handler.hitServer(11,DATE_FORMAT_1.format(date));//Delete subscriber
		 
		// System.out.println("--------Update Offer---------");
		// handler.hitServer(12,DATE_FORMAT_1.format(date));//Delete subscriber
		
		   System.out.println("--------Get Offer---------");
		   handler.hitServer(13,DATE_FORMAT_1.format(date));//Delete subscriber
		 
		 
	}
	// # responseCode 0 = Request succeeded #
	// # responseCode 100 = Other error (verify AF) #
	// # responseCode 102 = Subscriber not found #
	// # responseCode 104 = Temporary blocked #
	// # responseCode 117 = Service Class change not allowed #
	// # responseCode 123 = Max credit limit exceeded #
	// # responseCode 124 = Below minimum balance #
	// # responseCode 126 = Account not active #
	// # responseCode 127 = Accumulator not available #
	// # responseCode 134 = Accumulator overflow #
	// # responseCode 135 = Accumulator underflow #
	// # responseCode 140 = Invalid old Service class #
	// # responseCode 154 = Invalid old Service Class date #
	// # responseCode 155 = Invalid new Service Class #
}

