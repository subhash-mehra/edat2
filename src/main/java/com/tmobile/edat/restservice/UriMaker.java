package com.tmobile.edat.restservice;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.web.util.UriComponentsBuilder;

import com.tmobile.edat.util.Constants;

/**
 * This class is responsible to construct a URI along with it's parameters
 * 
 * @author Subhash-Mehra
 *
 */
public class UriMaker {

	private static final Logger logger = Logger.getLogger(UriMaker.class);

//	private static final String HOST_ADDRESS = "66.94.18.128";
	private static final String URL_IAM_PRIVACY_VAULT = "https://%1$s/offer_management/v1/offers";
	private static final String URL_IAM_TOKEN = "https://%1$s/oauth2-api/p/v1/token";
	private static final String URL_IAM_UNLINK = "https://%1$s/oauth2/v1/unlinkmsisdn";
	private static final String URL_IAM_LINK_MSISDN = "https://%1$s/oauth2/v1/linkmsisdn";
	private static final String URL_IAM_GENERATE_PIN = "https://%1$s/oauth2/v1/sendsmspintomsisdn";
	private static final String URL_IAM_PROFILE = "https://%1$s/iam_provisioning/v1/iam_profile";
	
		private static final String URL_IAM_LOCK = "https://%1$s/oauth2/v1/clearaccountlockcounters";
	private static final String URL_IAM_UNLOCK = "https://%1$s/oauth2/v1/togglehardlockflag";

    private static final String URL_IAM_SEARCH_COMSUMERAPI = "https://%1$s/consumerProfile/v1/userinfo?user_identity=MSISDN%3A8237800078";
	private static final String URL_IAM_SEARCH_IAMPROVISIONINGAPI = "https://%1$s/iam_provisioning/v1/iam_profile?UID=email%3Aabi%40saimail.com";
	private static final String URL_IAM_SEARCH_CONTRACTAPI = "(https://%1$s/iam_provisioning/v1/contract?{query_string}";
	private static final String URL_IAM_SEARCH_PERMISSIONAPI = "https://%1$s/permission-management/v1/permissions?permissionTypes=Linked%2CInherited%2CGranted&tmoid=email%3Araghav405%40tmo.com";
	private static final String URL_IAM_SEARCH_PRIVACYAPI = "https://%1$s/offer_management/v1/user/offers?userId=MSISDN%3A9008007060";
	// private static final String URL_IAM_PROFILE =
	// "https://%1$s/EdatRestTest/edattest/unlinkByEmail";

	/**
	 * This method construct URI based on @param baseUrl and @param parameters
	 * 
	 * @param restAPI
	 *            : code for different REST request
	 * @param baseUrl
	 * @param parameters
	 *            : use LinkedHashMap<String, Object>.
	 * @return
	 */
	public static String makeUri(Constants.RestAPI restAPI,
			Map<String, Object> parameters) {
		String uri = null;
		String baseUrl = null;
		switch (restAPI) {
		case REST_IAM_SEARCH:
			// uri = constructUri(baseUrl, parameters);
			break;

		case REST_IAM_PRIVACY_VAULT:
			// baseUrl = String.format(URL_IAM_PRIVACY_VAULT, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/privacyVaultDefault/";
			break;
		case REST_IAM_TOKEN:
			// baseUrl = String.format(URL_IAM_TOKEN, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/tokenResponse/";
			break;
		case REST_IAM_LINK_MSISDN:
			// baseUrl = String.format(URL_IAM_LINK_MSISDN, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/linkmsisdnByEmail";
			// baseUrl =
			// "http://192.168.1.98:8080/EdatRestTest/edattest/errorResponse";
			break;
		case REST_IAM_UNLINK_MSISDN:
			// baseUrl = String.format(URL_IAM_UNLINK, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/linkmsisdnByEmail";
			break;
		case REST_IAM_GENERATE_PIN:
			// baseUrl = String.format(URL_IAM_GENERATE_PIN, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/generatePin";
			break;
		case REST_IAM_PROFILE:
			// baseUrl = String.format(URL_IAM_PROFILE, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/iamsearchProfileEmail";
			break;

		case REST_IAM_SEARCH_CONSUMERAPI:
		//	baseUrl = String.format(URL_IAM_SEARCH_COMSUMERAPI, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/iamsearchPermission";
			break;
		case REST_IAM_SEARCH_IAMPROVISIONINGAPI:
		//	baseUrl = String.format(URL_IAM_SEARCH_IAMPROVISIONINGAPI, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/iamsearchPermission";
			break;
		case REST_IAM_SEARCH_CONTRACTAPI:
		//	baseUrl = String.format(URL_IAM_SEARCH_CONTRACTAPI, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/iamsearchPermission";
			break;
		case REST_IAM_SEARCH_PERMISSIONAPI:
		//	baseUrl = String.format(URL_IAM_SEARCH_PERMISSIONAPI, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/iamsearchPermission";
			break;
		case REST_IAM_SEARCH_PRIVACYAPI:
		//	baseUrl = String.format(URL_IAM_SEARCH_PRIVACYAPI, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/iamsearchPermission";
			break;

		case REST_IAM_LOCK:
			//baseUrl = String.format(URL_IAM_LOCK, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/lockAccount";
			break;
		
		case REST_IAM_UNLOCK:
			//baseUrl = String.format(URL_IAM_UNLOCK, HOST_ADDRESS);
			baseUrl = "http://192.168.1.98:8080/EdatRestTest/edattest/unlockAccount";
			break;

		default:
			break;
		}

		if (null != baseUrl) {
			uri = constructUri(baseUrl, parameters);
		}
		logger.info("uri = " + uri);
		return uri;
	}

	/**
	 * This is responsible to construct uri
	 * 
	 * @param baseUrl
	 * @param parameters
	 *            : use LinkedHashMap<String, Object>.
	 * @return : uri string
	 */
	private static String constructUri(String baseUrl,
			Map<String, Object> parameters) {
		if (null == parameters) {
			return null;
		}
		// add parameters
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(baseUrl);

		for (Map.Entry<String, Object> entry : parameters.entrySet()) {
			logger.info("parameter Key : " + entry.getKey() + " and Value: "
					+ entry.getValue());
			builder.queryParam(entry.getKey(), entry.getValue());
		}

		return builder.build().toUriString();
	}

}
