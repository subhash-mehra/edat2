package com.tmobile.edat.restservice;

import java.util.Map;

public class RequestObject {

	private Map<String, Object> urlParameters;
	private Map<String, Object> tokenParams;

	public Map<String, Object> getUrlParameters() {
		return urlParameters;
	}

	public void setUrlParameters(Map<String, Object> urlParameters) {
		this.urlParameters = urlParameters;
	}

	public Map<String, Object> getTokenParams() {
		return tokenParams;
	}

	public void setTokenParams(Map<String, Object> tokenParams) {
		this.tokenParams = tokenParams;
	}

}
