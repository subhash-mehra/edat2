package com.tmobile.edat.restservice;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tmobile.edat.messaging.dto.response.AccessTokenResponse;
import com.tmobile.edat.util.Constants.RequestMediaType;
import com.tmobile.edat.util.Constants.RestAPI;

/**
 * This class is wrapper for RestClient class. We will use this class in our
 * code for generating token and sending http request.
 * 
 * @author Subhash-Mehra
 *
 */
public class RequestHandler {

	private static final Logger logger = Logger.getLogger(RequestHandler.class);

	/*
	 * This will have token data
	 */
	private AccessTokenResponse mAccessTokenResponse = null;

	/**
	 * To get the instance of the RequestHandler
	 * 
	 * @param tokenUrlParameters
	 * @return
	 */
	public static RequestHandler getInstance(
			Map<String, Object> tokenUrlParameters) {
		return new RequestHandler(tokenUrlParameters);
	}

	/**
	 * in case token is not needed. i.e. while making token generation request
	 * separately
	 * 
	 * @return
	 */
	public static RequestHandler getInstance() {
		return new RequestHandler();
	}

	private RequestHandler() {
		// in case token is not needed. i.e. while making token generation
		// request separately
	}

	/**
	 * This constructor is responsible to make token request with given
	 * parameters
	 * 
	 * @param tokenUrlParameters
	 */
	private RequestHandler(Map<String, Object> tokenUrlParameters) {
		String tokenUri = UriMaker.makeUri(RestAPI.REST_IAM_TOKEN,
				tokenUrlParameters);
		logger.info("tokenUri = " + tokenUri);
		Map<String, String> tokenUrlHeaders = new LinkedHashMap<String, String>();
		tokenUrlHeaders.put("Accept", "application/json");
		tokenUrlHeaders.put("Content-Type", "application/json");
		mAccessTokenResponse = sendHttpRequest(tokenUri, RequestMediaType.JSON,
				RequestMethod.GET, tokenUrlHeaders, AccessTokenResponse.class);

	}

	/**
	 * This is a generic method to handle the server request for given @param
	 * uri and it returns the @param modelclass object.
	 * 
	 * @param uri
	 *            : server uri (base_url + parameters)
	 * @param requestMediaType
	 *            : JSON, XML . Check in Constants
	 * @param requestType
	 *            : GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
	 * @param headerParams
	 *            : Contains HTTP header key-value pair. use
	 *            LinkedHashMap<String, String>.
	 * @param modelClass
	 *            : .class of the model
	 * @return : modelClass object
	 */
	public <T> T sendHttpRequest(String uri, RequestMediaType requestMediaType,
			RequestMethod requestType, Map<String, String> headerParams,
			Class<T> modelClass) {
		if (null == uri || null == headerParams || null == modelClass) {
			logger.info("uri = " + uri + " headerParams = " + headerParams
					+ " modelClass = " + modelClass + " requestMediaType = "
					+ requestMediaType + " requestType = " + requestType);
			return null;
		}
		if (null != mAccessTokenResponse && null != headerParams) {
			headerParams.put("Authorization",
					mAccessTokenResponse.getAccess_token());
		}
		RestClient restClient = RestClient.getInstance();
		T responseDataObject = restClient.execute(uri, requestMediaType,
				requestType, headerParams, modelClass);
		return responseDataObject;
	}

}
