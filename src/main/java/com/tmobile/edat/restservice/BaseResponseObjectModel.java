package com.tmobile.edat.restservice;

public class BaseResponseObjectModel {

	private int statusCode;
	private String statusMessage;

	private String error;

	private String errorDescription;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String error_description) {
		this.errorDescription = error_description;
	}
}
