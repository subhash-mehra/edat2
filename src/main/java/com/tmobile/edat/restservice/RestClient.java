package com.tmobile.edat.restservice;

import java.util.Arrays;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.tmobile.edat.util.Constants.RequestMediaType;

/**
 * This class is responsible for processing the server request by providing the
 * url and return the response as an instance of modelClass
 * 
 * @author Subhash-Mehra
 *
 */
public class RestClient {

	private static final Logger logger = Logger.getLogger(RestClient.class);

	private RestClient() {
		// it should not be instantiated via new RestClient() outside
	}

	protected static RestClient getInstance() {
		return new RestClient();
	}

	/**
	 * This is a generic method to handle the server request for given @param
	 * uri and it returns the @param modelclass object.
	 * 
	 * @param uri
	 *            : server uri (base_url + parameters)
	 * @param requestMediaType
	 *            : JSON, XML . Check in Constants
	 * @param requestType
	 *            : GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
	 * @param headerParams
	 *            : Contains HTTP header key-value pair. use
	 *            LinkedHashMap<String, String>.
	 * @param modelClass
	 *            : .class of the model
	 * @return : modelClass object
	 */
	protected <T> T execute(String uri, RequestMediaType requestMediaType,
			RequestMethod requestType, Map<String, String> headerParams,
			Class<T> modelClass) {
		T instanceDataModal = null;
		if (null == uri) {
			// TODO: need to return error code
			return null;
		}

		// http request type
		HttpMethod httpMethod;
		switch (requestType) {
		case GET:
			httpMethod = HttpMethod.GET;
			break;
		case POST:
			httpMethod = HttpMethod.POST;
			break;

		default:
			httpMethod = HttpMethod.GET;
			break;
		}

		// media type
		MediaType mediaType;
		switch (requestMediaType) {
		case JSON:
			mediaType = MediaType.APPLICATION_JSON;
			break;
		case XML:
			mediaType = MediaType.APPLICATION_XML;
			break;

		default:
			mediaType = MediaType.APPLICATION_JSON;
			break;
		}

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(mediaType));

		for (Map.Entry<String, String> entry : headerParams.entrySet()) {

			logger.info("header Key : " + entry.getKey() + " and Value: "
					+ entry.getValue());
			headers.add(entry.getKey(), entry.getValue());
		}

		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);

		ResponseEntity<T> responseEntity = restTemplate.exchange(uri,
				httpMethod, requestEntity, modelClass);
		if (null != responseEntity) {
			instanceDataModal = responseEntity.getBody();

			HttpStatus statusCode = responseEntity.getStatusCode();
			if (instanceDataModal instanceof BaseResponseObjectModel) {
				((BaseResponseObjectModel) instanceDataModal).setStatusCode(statusCode.value());
				((BaseResponseObjectModel) instanceDataModal).setStatusMessage(statusCode.getReasonPhrase());;
			}
			
			logger.info("statusCode = " + statusCode.value() + " message = "
					+ statusCode.getReasonPhrase());
		}

		return instanceDataModal;
	}
}
