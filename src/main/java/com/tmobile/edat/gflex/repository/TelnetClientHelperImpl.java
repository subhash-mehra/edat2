package com.tmobile.edat.gflex.repository;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.gflex.dto.GflexSearchResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

@Repository("TelnetClientHelper")
public class TelnetClientHelperImpl implements TelnetClientHelper {
	
	private static final Logger logger = Logger.getLogger(TelnetClientHelperImpl.class);

	private TelnetClient tc = null;
	private String host;
	private int port = 5873;
	private String result = "";
	private String status="";

	public GflexSearchResponse search(String msisdn,SessionConstants sessionConstants) throws UserException {
		GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		try{
			host = SessionConstantsFactory.getSessionConstant("TELNET_HOST", sessionConstants);
			tc = new TelnetClient(host, port);
			String command = "connect(iid 123456,version 1.0,endchar newline,txnmode single,idletimeout 5)";
			result = tc.excuteTelnetCommand(command,flexSearchRes);
			String message = "rtrv_sub(iid 123456, dn " + msisdn + ")";
			
			String message1 = "rtrv_sub(sp " + msisdn + ")";
			result = tc.excuteTelnetCommand(message,flexSearchRes);
			disconnect(sessionConstants,flexSearchRes);
			tc.dispose();
			logger.info("connect Response 1 := "+ message1);
		}catch(Exception ex){
			logger.info("GflexSearchResponse search "+ex.getMessage());
			flexSearchRes.setApplicationError(new ApplicationError(ex.getMessage()));
		}
		logger.info("Final Result "+result);
		flexSearchRes.setResponse(result);
		return flexSearchRes;
	}

	public GflexSearchResponse add(String msisdn, String selValue, SessionConstants sessionConstants) throws UserException {
		GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		try{
			
				host = SessionConstantsFactory.getSessionConstant("TELNET_HOST", sessionConstants);
				tc = new TelnetClient(host, port);
				String command = "connect(iid 123456,version 1.0,endchar newline,txnmode single,idletimeout 5)";
				result = tc.excuteTelnetCommand(command,flexSearchRes);
				String message = "rtrv_sub(iid 123456, dn " + msisdn + ")";
				result = tc.excuteTelnetCommand(message,flexSearchRes);
								
				if(result!="")
				{
				int index=result.indexOf("rc");
				index=index+3;
				status=result.substring(index, index+4);
				if(status.equalsIgnoreCase("0, d"))
				{
					status="0";
				}
				logger.info("Status========================== := "+ status);
				if ( status.equalsIgnoreCase("0") || status.equalsIgnoreCase("1013")) {
					String insertCommand = "ent_sub(iid 123456, dn " + msisdn + ", sp " + selValue + ")";
					result = tc.excuteTelnetCommand(insertCommand,flexSearchRes);
					index=result.indexOf("rc");
					index=index+3;
					status=result.substring(index, index+4);
					if(status.equalsIgnoreCase("0, d"))
					{
						status="0";
					}
					
				}
				}
				
				disconnect(sessionConstants,flexSearchRes);
				tc.dispose();
				logger.info("add connect Response 3 := "+ result);
				logger.info("add connect Response 3 := "+ status);
				flexSearchRes.setStatus(status);
		  }catch (Exception e) {
					flexSearchRes.setApplicationError(new ApplicationError(e.getMessage()));
				}
		return flexSearchRes;
	}

	public GflexSearchResponse update(String msisdn, String selValue, SessionConstants sessionConstants) throws UserException {
		GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		try{
			
		host = SessionConstantsFactory.getSessionConstant("TELNET_HOST", sessionConstants);
		tc = new TelnetClient(host, port);
		String command = "connect(iid 123456,version 1.0,endchar newline,txnmode single,idletimeout 5)";
		result = tc.excuteTelnetCommand(command,flexSearchRes);
		String message = "rtrv_sub(iid 123456, dn " + msisdn + ")";
		result = tc.excuteTelnetCommand(message,flexSearchRes);
		logger.info("After executing excuteTelnetCommand with msisdn :================== "+ result);
		
		if(result!="")
		{
		int index=result.indexOf("rc");
		index=index+3;
		status=result.substring(index, index+4);
		if(status.equalsIgnoreCase("0, d"))
		{
			status="0";
		}
		logger.info("Status========================== := "+ status);
		if (status.equalsIgnoreCase("0") || status.equalsIgnoreCase("1013")) {
			String updateCommand = "ent_sub(iid 123456, dn " + msisdn + ", sp " + selValue + ")";
			result = tc.excuteTelnetCommand(updateCommand,flexSearchRes);
			index=result.indexOf("rc");
			index=index+3;
			status=result.substring(index, index+4);
			if(status.equalsIgnoreCase("0, d"))
			{
				status="0";
			}
			logger.info("After executing excuteTelnetCommand msisdn and selvalue :================== "+ result);
			logger.info("After executing excuteTelnetCommand Status :================== "+ status);
			
		}
		}
		disconnect(sessionConstants,flexSearchRes);
		tc.dispose();
		logger.info("update connect Response 4 := "+ result);
		logger.info("update connect Response 4 := "+ status);
		
		}catch(Exception ex)
		{
			logger.info("GflexSearchResponse update:: "+ex.getMessage());
			flexSearchRes.setApplicationError(new ApplicationError(ex.getMessage()));
		}
		flexSearchRes.setStatus(status);
		return flexSearchRes;
	}

	public GflexSearchResponse delete(String msisdn, SessionConstants sessionConstants) throws UserException {
		
		GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		
		try{
		
				host = SessionConstantsFactory.getSessionConstant("TELNET_HOST", sessionConstants);
				tc = new TelnetClient(host, port);
				String command = "connect(iid 123456,version 1.0,endchar newline,txnmode single,idletimeout 5)";
				result = tc.excuteTelnetCommand(command,flexSearchRes);
				String messgae = "rtrv_sub(iid 123456, dn " + msisdn + ")";
				result = tc.excuteTelnetCommand(messgae,flexSearchRes);
				if (!result.contains("1013")) {
					String deleteCommand = "dlt_sub(iid 123456, dn " + msisdn + ")";
					result = tc.excuteTelnetCommand(deleteCommand,flexSearchRes);
					logger.info("Result================================ := "+ result);
					if(result!="")
					{
					int index=result.indexOf("rc");
					index=index+3;
					status=result.substring(index, index+4);
					logger.info("Status========================== := "+ status);
					}
					
				}else{
					status="1013";
				}
				disconnect(sessionConstants,flexSearchRes);
				tc.dispose();
				
				flexSearchRes.setStatus(status);
		}catch(Exception ex)
		{
			flexSearchRes.setApplicationError(new ApplicationError(ex.getMessage()));
		}
		return flexSearchRes;
	}

	public GflexSearchResponse failure(String msisdn, SessionConstants sessionConstants) throws UserException {
		GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		try{
		
		host = SessionConstantsFactory.getSessionConstant("TELNET_HOST", sessionConstants);
		tc = new TelnetClient(host, port);
		String command = "connect(iid 123456,version 1.0,endchar newline,txnmode single,idletimeout 5)";
		result = tc.excuteTelnetCommand(command,flexSearchRes);
		String message = "rtrv_sub(iid 123456, dn " + msisdn + ")";
		result = tc.excuteTelnetCommand(message,flexSearchRes);
		disconnect(sessionConstants,flexSearchRes);
		tc.dispose();
		logger.info("failure connect Response 3 := "+ result);
		}catch(Exception ex)
		{
			flexSearchRes.setApplicationError(new ApplicationError(ex.getMessage()));
		}
		flexSearchRes.setStatus(status);
		
		return flexSearchRes;
	}

	private void disconnect(SessionConstants sessionConstants , GflexSearchResponse flexSearchRes) throws UserException, IOException {
		//GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		
		host = SessionConstantsFactory.getSessionConstant("TELNET_HOST", sessionConstants);
		tc = new TelnetClient(host, port);
		String message = "disconnect()";
		tc.excuteTelnetCommand(message,flexSearchRes);

	}

	/*public static void main(String[] args) throws IOException {
		TelnetClientHelperImpl obj = new TelnetClientHelperImpl();
		obj.search("12345678941");
		obj.add("12065555556", "211142119");
		obj.update("16667778880","001179002");
		obj.delete("16667778880");
		obj.failure("12065551234)");
		// obj.disconnect();
	}*/

}
