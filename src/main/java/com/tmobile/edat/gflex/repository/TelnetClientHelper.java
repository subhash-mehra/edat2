package com.tmobile.edat.gflex.repository;

import java.io.IOException;

import com.tmobile.edat.gflex.dto.GflexSearchResponse;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;


public interface TelnetClientHelper {
	
	public GflexSearchResponse search(String msisdn, SessionConstants sessionConstants) throws UserException;
	public GflexSearchResponse add(String msisdn,String selValue, SessionConstants sessionConstants) throws UserException;
	public GflexSearchResponse update(String msisdn,String selValue, SessionConstants sessionConstants) throws UserException;
	public GflexSearchResponse delete(String msisdn, SessionConstants sessionConstants) throws UserException;
	public GflexSearchResponse failure(String msisdn, SessionConstants sessionConstants) throws UserException;

}
