package com.tmobile.edat.gflex.repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.log4j.Logger;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.gflex.dto.GflexSearchResponse;

public class TelnetClient {
	private Socket socket = null;
	private PrintWriter s_out = null;
	private BufferedReader s_in = null;
	private static final Logger logger = Logger.getLogger(TelnetClient.class);
	public TelnetClient(String host, int port) {
		try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(host, port));
			s_out = new PrintWriter(socket.getOutputStream(), true);
			s_in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (Exception e) {
			System.err.println("Don't know about host : " + host);
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void dispose() {
		try {
			if (socket != null) {
				socket.close();
			}
			if (s_out != null) {
				s_out.close();
			}
			if (s_in != null) {
				s_in.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String excuteTelnetCommand(String command ,GflexSearchResponse flexSearchRes) throws IOException {
		//GflexSearchResponse flexSearchRes = new GflexSearchResponse();
		StringBuffer response = new StringBuffer();
		try {
			logger.info("in excuteTelnetCommand method socket :================== "+ socket);
			if (socket != null) {
				response = new StringBuffer();
				s_out.println(command);
				String msg;
				logger.info("in if in excuteTelnetCommand method============================ "+ socket);
				while ((msg = s_in.readLine()) != null) {
					response.append(msg);
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			flexSearchRes.setApplicationError(new ApplicationError(ex.getMessage()));
			logger.info("in Exception in excuteTelnetCommand method============================ ");
			
		}
		return response.toString();
	}

	/*public static void main(String[] args) throws IOException {
		TelnetClient tc = new TelnetClient("10.25.19.162", 5873);
		
		//Read/search: Input - msisdn
		String message = "connect(iid 123456,version 1.0,endchar newline,txnmode single,idletimeout 5)\nrtrv_sub(iid 123456, dn 12065555555)";
		String result = tc.excuteTelnetCommand(message);
		System.out.println("connect Response ...  :" + result);

		String message2 = "rtrv_sub(iid 123456, dn 12065555555) ";
		String result2 = tc.excuteTelnetCommand(message2);
		System.out.println("rtrv_sub Response...  :" + result2);
		
		String message3 = "ent_sub(iid 123456, dn 12065555555, sp 211142119)";
		String result3 = tc.excuteTelnetCommand(message3);
		System.out.println("rtrv_sub Response...  :" + result3);
		
		String message4 = "upd_sub(iid 123456, dn 12065555555, sp 211142164)";
		String result4 = tc.excuteTelnetCommand(message4);
		System.out.println("rtrv_sub Response...  :" + result4);
		
		String message5 = "dlt_sub(iid 123456, dn 12065555555)";
		String result5 = tc.excuteTelnetCommand(message5);
		System.out.println("rtrv_sub Response...  :" + result5);
		
		String message6 = "disconnect()";
		String result6 = tc.excuteTelnetCommand(message6);
		System.out.println("disconnect Response ...  :" + result6);
		tc.dispose();
	}*/

}
