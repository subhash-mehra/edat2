/**
 * 
 */
package com.tmobile.edat.gflex.dto;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.session.SessionConstants;

/**
 * @author nilay.s
 *
 */
public class GflexSearchRequest  extends BaseRequest implements Serializable {
	
 private static final long serialVersionUID = 1L;
	
private String msisdn;

private String selValue;

private String deleteDate;

private String fromDate;

private String toDate;



private String operartion;

private String dateOfOperation;

private String status;

protected SessionConstants sessionConstants;


public String getMsisdn() {
	return msisdn;
}
public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}
public String getSelValue() {
	return selValue;
}
public void setSelValue(String selValue) {
	this.selValue = selValue;
}
public String getDeleteDate() {
	return deleteDate;
}
public void setDeleteDate(String deleteDate) {
	this.deleteDate = deleteDate;
}
public String getOperartion() {
	return operartion;
}
public void setOperartion(String operartion) {
	this.operartion = operartion;
}
public String getDateOfOperation() {
	return dateOfOperation;
}
public void setDateOfOperation(String dateOfOperation) {
	this.dateOfOperation = dateOfOperation;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}

public String getToDate() {
	return toDate;
}
public String getFromDate() {
	return fromDate;
}
public void setFromDate(String fromDate) {
	this.fromDate = fromDate;
}
public void setToDate(String toDate) {
	this.toDate = toDate;
}

public SessionConstants getSessionConstants() {
	return sessionConstants;
}
public void setSessionConstants(SessionConstants sessionConstants) {
	this.sessionConstants = sessionConstants;
}
@Override
public String toString() {
	return "GflexSearchRequest [msisdn=" + msisdn + ", selValue=" + selValue + ", deleteDate=" + deleteDate
			+ ", operartion=" + operartion + ", dateOfOperation=" + dateOfOperation + ", status=" + status + "]";
 }

}
