package com.tmobile.edat.gflex.dto;

import java.io.Serializable;

import com.tmobile.edat.base.BaseResponse;

public class GflexSearchResponse extends BaseResponse implements Serializable {


private static final long serialVersionUID = 1L;

private String status;
private String response;
private String msisdn;
private String deleteDate;
private String selVal;



public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getResponse() {
	return response;
}

public void setResponse(String response) {
	this.response = response;
}

public String getMsisdn() {
	return msisdn;
}

public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}

public String getDeleteDate() {
	return deleteDate;
}

public void setDeleteDate(String deleteDate) {
	this.deleteDate = deleteDate;
}

public String getSelVal() {
	return selVal;
}

public void setSelVal(String selVal) {
	this.selVal = selVal;
}


}
