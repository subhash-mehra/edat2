package com.tmobile.edat.gflex.dto;

import java.io.Serializable;
import java.util.List;

import com.tmobile.edat.gflex.model.GflexDetails;

public class GflexReportResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<GflexDetails> gflexDetailsList;

	public GflexReportResponse() {

	}

	public List<GflexDetails> getGflexDetailsList() {
		return gflexDetailsList;
	}

	public void setGflexDetailsList(List<GflexDetails> gflexDetailsList) {
		this.gflexDetailsList = gflexDetailsList;
	}

	public String toString() {
		return "GflexSearchRequest [gflexDetailsList=" + gflexDetailsList + "]";
	}

}
