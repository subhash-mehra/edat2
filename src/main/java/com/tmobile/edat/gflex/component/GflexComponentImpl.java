package com.tmobile.edat.gflex.component;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.gflex.dto.GflexReportResponse;
import com.tmobile.edat.gflex.dto.GflexSearchRequest;
import com.tmobile.edat.gflex.dto.GflexSearchResponse;
import com.tmobile.edat.gflex.model.GflexDetails;
import com.tmobile.edat.gflex.repository.TelnetClientHelper;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.JavaUtil;
import com.tmobile.edat.util.UserException;
import com.tmobile.edat.util.Util;

@Service("GflexComponent")
public class GflexComponentImpl implements GflexComponent {

	private static final Logger logger = Logger
			.getLogger(GflexComponentImpl.class);

	@Autowired
	@Qualifier("TelnetClientHelper")
	private TelnetClientHelper telnetClientHelper;

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	private GflexSearchResponse gflexSearchResponse = null;
	Date currentDate = new Date();

	@Override
	public GflexSearchResponse search(GflexSearchRequest gflexSearchRequest)
			throws UserException {
		Map<String,Object>dataMap=new HashMap<String, Object>();
		String deleteDate="";
		String selVal="";
		logger.info("Calling Search method := ");
		//gflexSearchResponse = new GflexSearchResponse();
	//	dbOperation();
		if (gflexSearchRequest != null) {
			gflexSearchResponse = telnetClientHelper
					.search(gflexSearchRequest.getMsisdn(), gflexSearchRequest.getSessionConstants());
			//gflexSearchResponse.setResponse(searchResponse);
			logger.info("after Search method := " + gflexSearchResponse);
		}
		
				 dataMap = (Map)getDeleteDate(gflexSearchRequest.getMsisdn());
				 if(dataMap.get("deleteDate")!=null)
				 {
					 deleteDate=(String)dataMap.get("deleteDate");
				 }
				 
				 if(dataMap.get("selVal")!=null)
				 {
					 selVal=(String)dataMap.get("selVal");
				 }
				
				
				
				try {
					deleteDate=deleteDate.substring(5,7)+"/"+deleteDate.substring(8,10)+"/"+deleteDate.substring(0,4);
					gflexSearchResponse.setDeleteDate(deleteDate);
				} catch (Exception e) {
				}
				gflexSearchResponse.setMsisdn(gflexSearchRequest.getMsisdn());
				gflexSearchResponse.setSelVal(selVal);
		return gflexSearchResponse;
	}
	
	public Map<String,Object> getDeleteDate(String msisdn) {

		GflexReportResponse response = new GflexReportResponse();
		Map<String,Object>dataMap=new HashMap<String, Object>();
		String deleteDate="";
		String selVal="";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		List<GflexDetails> gflexDetailsList = (List<GflexDetails>) jdbcTemplate
				.query("SELECT MSISDN, LAB, OPERATION, Operation_DTTM, status, Delete_DTTM FROM GFLEX_DATA where MSISDN='"+msisdn+"'",
						new RowMapper<GflexDetails>() {
							public GflexDetails mapRow(ResultSet rs,
									int rownumber) throws SQLException {
								GflexDetails e = new GflexDetails();
								e.setMsisdn(rs.getString(1));
								e.setSelValue(rs.getString(2));
								e.setOperartion(rs.getString(3));
								e.setDateOfOperation(rs.getString(4));
								e.setStatus(rs.getString(5));
								e.setDeleteDate(rs.getString(6));
								return e;
							}
						});
		if(gflexDetailsList.size()>0)
		{
			GflexDetails gflexDetails=gflexDetailsList.get(0);
			
			deleteDate=gflexDetails.getDeleteDate();
			selVal=gflexDetails.getSelValue();
		}
		logger.info("deleteDate======== : " + deleteDate);
		logger.info("GflexReports Final Report Map : " + gflexDetailsList);
		dataMap.put("deleteDate", deleteDate);
		dataMap.put("selVal",selVal );
		return dataMap;
	}

	public void dbOperation() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		int rowCount = jdbcTemplate.queryForObject(
				"ALTER TABLE GFLEX_DATA MODIFY MSISDN VARCHAR(20)",
				new Object[] {}, Integer.class);
		System.out.println(rowCount);

	}

	@Override
	public GflexSearchResponse add(GflexSearchRequest gflexSearchRequest)
			throws UserException {
		//gflexSearchResponse = new GflexSearchResponse();
		logger.info("Calling Add method := ");
		try {
			if (gflexSearchRequest != null) {
				gflexSearchResponse = telnetClientHelper.add(
						gflexSearchRequest.getMsisdn(),
						gflexSearchRequest.getSelValue(), gflexSearchRequest.getSessionConstants());
				if (gflexSearchResponse.getStatus().equalsIgnoreCase("0")) {
					gflexSearchRequest.setStatus("0");
					gflexSearchRequest.setOperartion("Create");
					String date = gflexSearchRequest.getDeleteDate();
					String newDbDate = Util.formateDate(date,
							Constants.MMddyyyy, Constants.yyyyMMdd);
					gflexSearchRequest.setDeleteDate(newDbDate);
					addOperation(gflexSearchRequest);
					logger.info("After Add method := ");
				}
				//gflexSearchResponse.setStatus(addResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			gflexSearchResponse.setApplicationError(new ApplicationError(e.getMessage()));
		}
		return gflexSearchResponse;
	}

	@Override
	public GflexSearchResponse update(GflexSearchRequest gflexSearchRequest)
			throws UserException {

		logger.info("Before calling the update----  ");
		//gflexSearchResponse = new GflexSearchResponse();
		if (gflexSearchRequest != null) {
			gflexSearchResponse = telnetClientHelper.update(
					gflexSearchRequest.getMsisdn(),
					gflexSearchRequest.getSelValue(), gflexSearchRequest.getSessionConstants());
			//gflexSearchResponse.setStatus(updateResponse);
			if (gflexSearchResponse !=null && gflexSearchResponse.getStatus().equalsIgnoreCase("0")  ) {
				gflexSearchRequest.setStatus("0");
				gflexSearchRequest.setOperartion("Update");
				String date = gflexSearchRequest.getDeleteDate();
				String newDbDate = Util.formateDate(date, Constants.MMddyyyy,
						Constants.yyyyMMdd);
				gflexSearchRequest.setDeleteDate(newDbDate);
				addOperation(gflexSearchRequest);
				logger.info("After update method := ");
			}

		}
		return gflexSearchResponse;
	}

	@Override
	public GflexSearchResponse delete(GflexSearchRequest gflexSearchRequest)
			throws UserException {
		logger.info("Before calling the delete----  ");
		//gflexSearchResponse = new GflexSearchResponse();
		if (gflexSearchRequest != null) {
			int count = getMSISDNCountInGflex(gflexSearchRequest.getMsisdn());
			if (count > 0) {
				gflexSearchResponse = telnetClientHelper
						.delete(gflexSearchRequest.getMsisdn(),gflexSearchRequest.getSessionConstants());
			//	gflexSearchResponse.setStatus(deleteeResponse);
				if (!gflexSearchResponse.getStatus().equals("1013")) {
					String selValue = getMSISDNGflexData(
							gflexSearchRequest.getMsisdn(), "LAB");
					gflexSearchRequest.setSelValue(selValue);
					gflexSearchRequest.setOperartion("Delete");
					gflexSearchRequest.setStatus("1");
					String date = gflexSearchRequest.getDeleteDate();
					String newDbDate = Util.formateDate(date,
							Constants.MMddyyyy, Constants.yyyyMMdd);
					gflexSearchRequest.setDeleteDate(newDbDate);
					deleteMsisdn(gflexSearchRequest);
				}
			} else {
				gflexSearchResponse = new GflexSearchResponse();
				gflexSearchResponse.setStatus("1013");
				return gflexSearchResponse;
			}
		}
		return gflexSearchResponse;
	}

	public void deleteMsisdn(GflexSearchRequest gflexSearchRequest) {
		try {
			if (gflexSearchRequest != null) {
				String sql = "INSERT INTO GFLEX_DATA (MSISDN,LAB,OPERATION,Operation_DTTM,status,Delete_DTTM) VALUES (?, ?, ?, ?,?,?)";
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				Object[] params = new Object[] {
						gflexSearchRequest.getMsisdn(),
						gflexSearchRequest.getSelValue(),
						gflexSearchRequest.getOperartion(), currentDate,
						gflexSearchRequest.getStatus(), currentDate };
				int[] types = new int[] { Types.NUMERIC, Types.VARCHAR,
						Types.VARCHAR, Types.DATE, Types.NUMERIC, Types.DATE };

				int row = jdbcTemplate.update(sql, params, types);
				logger.info("Row Status := " + row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addOperation(GflexSearchRequest gflexSearchRequest) {
		try {
			if (gflexSearchRequest != null) {
				String sql = "INSERT INTO GFLEX_DATA (MSISDN,LAB,OPERATION,Operation_DTTM,status,Delete_DTTM) VALUES (?, ?, ?, ?,?,?)";
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				Object[] params = new Object[] {
						gflexSearchRequest.getMsisdn(),
						gflexSearchRequest.getSelValue(),
						gflexSearchRequest.getOperartion(), currentDate,
						gflexSearchRequest.getStatus(),
						gflexSearchRequest.getDeleteDate() };
				int[] types = new int[] { Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.DATE, Types.NUMERIC, Types.DATE };

				int row = jdbcTemplate.update(sql, params, types);
				logger.info("Row Status := " + row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getMSISDNCountInGflex(String msisdn) {

		StringBuffer queryString = new StringBuffer();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		queryString = queryString
				.append("SELECT count(MSISDN) FROM GFLEX_DATA ");
		if (msisdn != null && !"".equals(msisdn)) {
			queryString = queryString
					.append(" WHERE MSISDN = '" + msisdn + "'");
		}

		int rowCount = jdbcTemplate.queryForObject(
				"SELECT count(MSISDN) FROM GFLEX_DATA WHERE MSISDN = ?",
				new Object[] { msisdn }, Integer.class);

		return rowCount;

	}

	public String getMSISDNGflexData(String msisdn, String rowName) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String queryString = "SELECT " + rowName
				+ " FROM GFLEX_DATA WHERE MSISDN = '" + msisdn + "'";

		String value = (String) jdbcTemplate.queryForObject(queryString,
				String.class);

		return value;
	}

	public GflexReportResponse GflexReports() throws UserException {
		logger.info("GflexReports generation starts");

		GflexReportResponse response = new GflexReportResponse();

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		List<GflexDetails> gflexDetailsList = (List<GflexDetails>) jdbcTemplate
				.query("SELECT MSISDN, LAB, OPERATION, Operation_DTTM, status, Delete_DTTM FROM GFLEX_DATA",
						new RowMapper<GflexDetails>() {
							public GflexDetails mapRow(ResultSet rs,
									int rownumber) throws SQLException {
								GflexDetails e = new GflexDetails();
								e.setMsisdn(rs.getString(1));
								e.setSelValue(rs.getString(2));
								e.setOperartion(rs.getString(3));
								e.setDateOfOperation(rs.getString(4));
								e.setStatus(rs.getString(5));
								e.setDeleteDate(rs.getString(6));
								return e;
							}
						});
		logger.info("GflexReports generation Ends : " + gflexDetailsList);
		response.setGflexDetailsList(gflexDetailsList);
		logger.info("GflexReports Final Report Map : " + gflexDetailsList);

		return response;
	}

	@Override
	public GflexSearchResponse cleanUpData(GflexSearchRequest gflexSearchRequest)
			throws UserException {
		String deleteeResponse = "";
		logger.info("cleanUp method calling--------  ");
		DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
		String fromDate = gflexSearchRequest
				.getFromDate();
		String toDate = gflexSearchRequest.getToDate();
		logger.info("fromDate--------  "+fromDate);
		logger.info("toDate--------  "+toDate);
		try
		{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<GflexDetails> gflexDetailsList = (List<GflexDetails>) jdbcTemplate
				.query("SELECT * FROM GFLEX_DATA WHERE TO_CHAR(Delete_DTTM, 'yyyy-MM-dd') BETWEEN '"+JavaUtil.converStringDate(fromDate)+"' AND '"+JavaUtil.converStringDate(toDate)+"' AND  STATUS=0",
						
						new RowMapper<GflexDetails>() {
							public GflexDetails mapRow(ResultSet rs,
									int rownumber) throws SQLException {
								GflexDetails e = new GflexDetails();
								e.setMsisdn(rs.getString(1));
								return e;
							}
						});
		
		logger.info("gflexDetailsList--------  "+gflexDetailsList.size());
		if (gflexDetailsList.size() > 0) {
			for (GflexDetails gflexDetails : gflexDetailsList) {
				gflexSearchResponse = telnetClientHelper.delete(gflexDetails
						.getMsisdn(), gflexSearchRequest.getSessionConstants());
				gflexSearchRequest.setMsisdn(gflexDetails.getMsisdn());
				updateOperation(gflexSearchRequest);
			}
		}

		}catch(Exception e){
			logger.info("Error occured======================================================");
			gflexSearchResponse.setApplicationError(new ApplicationError(e.getMessage()));
		}

		return gflexSearchResponse;
	}

	public void updateOperation(GflexSearchRequest gflexSearchRequest) {
		try {
			if (gflexSearchRequest != null) {
				String sql = "UPDATE TABLE GFLEX_DATA SET STATUS=? WHERE MSISDN=?";
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				Object[] params = new Object[] {
						gflexSearchRequest.getMsisdn(), 1 };
				int[] types = new int[] { Types.NUMERIC, Types.NUMERIC };
				int row = jdbcTemplate.update(sql, params, types);
				logger.info("Row Status := " + row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
