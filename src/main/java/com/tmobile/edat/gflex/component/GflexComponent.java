package com.tmobile.edat.gflex.component;

import java.io.IOException;
import java.util.List;

import com.tmobile.edat.gflex.dto.GflexReportResponse;
import com.tmobile.edat.gflex.dto.GflexSearchRequest;
import com.tmobile.edat.gflex.dto.GflexSearchResponse;
import com.tmobile.edat.gflex.model.GflexDetails;
import com.tmobile.edat.util.UserException;

public interface GflexComponent {
	
	public GflexSearchResponse search(GflexSearchRequest gflexSearchRequest) throws UserException;
	
	public GflexSearchResponse add(GflexSearchRequest gflexSearchRequest) throws UserException;
	
	public GflexSearchResponse update(GflexSearchRequest gflexSearchRequest) throws UserException;
	
	public GflexSearchResponse delete(GflexSearchRequest gflexSearchRequest) throws UserException;
	
	public GflexReportResponse GflexReports() throws UserException;
	
	public GflexSearchResponse cleanUpData(GflexSearchRequest gflexSearchRequest)
			throws UserException;
	

}
