/**
 * 
 */
package com.tmobile.edat.gflex.model;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;

/**
 * @author nilay.s
 * 
 */
public class GflexDetails extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String msisdn;

	private String selValue;

	private String deleteDate;

	private String operartion;

	private String dateOfOperation;

	private String status;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSelValue() {
		return selValue;
	}

	public void setSelValue(String selValue) {
		this.selValue = selValue;
	}

	public String getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}

	public String getOperartion() {
		return operartion;
	}

	public void setOperartion(String operartion) {
		this.operartion = operartion;
	}

	public String getDateOfOperation() {
		return dateOfOperation;
	}

	public void setDateOfOperation(String dateOfOperation) {
		this.dateOfOperation = dateOfOperation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GflexDetails [msisdn=" + msisdn + ", selValue=" + selValue
				+ ", deleteDate=" + deleteDate + ", operartion=" + operartion
				+ ", dateOfOperation=" + dateOfOperation + ", status=" + status
				+ "]";
	}

}
