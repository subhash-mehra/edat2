package com.tmobile.edat.gflex.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmobile.edat.gflex.component.GflexComponent;
import com.tmobile.edat.gflex.dto.GflexReportResponse;
import com.tmobile.edat.gflex.dto.GflexSearchRequest;
import com.tmobile.edat.gflex.dto.GflexSearchResponse;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;


@Controller
@RequestMapping("/gflex")

/** Description of GflexController 
*
* GflexController class is used for GFlex tool.
* 
* 
*/
public class GflexController {
	
private static final Logger logger = Logger.getLogger(GflexController.class);
	
@Autowired
@Qualifier("GflexComponent")
private GflexComponent gflexComponent;

/**
*
* gflex() method is used for show GFlex main page.
* 
*/

@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
public String gflex() {
		return "gflex/gflex";

}

/**
*
* search() method is used for search msisdn.
* 
*/

@RequestMapping(value = "/search", method = RequestMethod.POST, 
produces = MediaType.APPLICATION_JSON_VALUE, 
consumes = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public GflexSearchResponse search(@RequestBody GflexSearchRequest gflexSearchRequest,HttpSession session) throws IOException, UserException {
	logger.info("Inside the search Controller:= "+ gflexSearchRequest.getMsisdn());
	gflexSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
	return this.gflexComponent.search(gflexSearchRequest);
 
}

/**
*
* add() method is used for add msisdn.
* 
*/
@RequestMapping(value = "/add", method = RequestMethod.POST, 
produces = MediaType.APPLICATION_JSON_VALUE, 
consumes = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public GflexSearchResponse add(@RequestBody GflexSearchRequest gflexSearchRequest,HttpSession session) throws IOException, UserException {
	logger.info("Inside the Add Controller:= "+ gflexSearchRequest.toString());
	System.out.println("Inside the Add Controller:= "+ gflexSearchRequest.toString());
	gflexSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
	return this.gflexComponent.add(gflexSearchRequest);
}
	
/**
*
* update() method is used for update msisdn.
* 
*/
@RequestMapping(value = "/update", method = RequestMethod.POST, 
produces = MediaType.APPLICATION_JSON_VALUE, 
consumes = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public GflexSearchResponse update(@RequestBody GflexSearchRequest gflexSearchRequest,HttpSession session) throws IOException, UserException {	
	logger.info("Inside the update Controller:= "+ gflexSearchRequest.getMsisdn());
	gflexSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
	return this.gflexComponent.update(gflexSearchRequest);

}


/**
*
* delete() method is used for delete msisdn.
* 
*/

@RequestMapping(value = "/delete", method = RequestMethod.POST, 
produces = MediaType.APPLICATION_JSON_VALUE, 
consumes = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public GflexSearchResponse delete(@RequestBody GflexSearchRequest gflexSearchRequest,HttpSession session) throws IOException, UserException {
	logger.info("Inside the delet Controller:= "+ gflexSearchRequest.getMsisdn());
	gflexSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
	return this.gflexComponent.delete(gflexSearchRequest);

 }


/**
*
* gFlexReports() method is used for show GFlex Reports.
* 
*/
@RequestMapping(value = "/gFlexReports", method = RequestMethod.POST, 
produces = MediaType.APPLICATION_JSON_VALUE, 
consumes = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public GflexReportResponse gFlexReports() throws IOException, UserException {
	logger.info("Inside the GFlex reports Controller");
	return  this.gflexComponent.GflexReports();

 }

/**
*
* cleanUpData() method is used for clean uo GFlex Data.
* 
*/

@RequestMapping(value = "/cleanUp", method = RequestMethod.POST, 
produces = MediaType.APPLICATION_JSON_VALUE, 
consumes = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public GflexSearchResponse cleanUpData(@RequestBody GflexSearchRequest gflexSearchRequest,HttpSession session) throws IOException, UserException {
	logger.info("Inside the GFlex reports Controller");
	gflexSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
	return  this.gflexComponent.cleanUpData(gflexSearchRequest);

 }

}
