package com.tmobile.edat.messaging.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmobile.edat.messaging.component.MessagingDataComponent;
import com.tmobile.edat.messaging.dto.request.MessagingSearchRequest;
import com.tmobile.edat.messaging.dto.response.AccessTokenResponse;
import com.tmobile.edat.messaging.dto.response.ClientIDResponse;
import com.tmobile.edat.messaging.dto.response.IAMSearchResponse;
import com.tmobile.edat.messaging.dto.response.IamGenerateSMSPinResponse;
import com.tmobile.edat.messaging.dto.response.IamLinkMsisdnResponse;
import com.tmobile.edat.messaging.dto.response.IamPrivacyVaultDataResponse;
import com.tmobile.edat.messaging.dto.response.IamProfileResponse;
import com.tmobile.edat.messaging.dto.response.LockAccountResponse;
import com.tmobile.edat.messaging.dto.response.MessagingDataResponse;
import com.tmobile.edat.messaging.dto.response.UnLockResponse;
import com.tmobile.edat.restservice.BaseResponseObjectModel;
import com.tmobile.edat.restservice.RequestHandler;
import com.tmobile.edat.restservice.RequestObject;
import com.tmobile.edat.restservice.UriMaker;
import com.tmobile.edat.util.ClientUtil;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.Constants.IAMSearchAPIEnum;
import com.tmobile.edat.util.Constants.RequestMediaType;
import com.tmobile.edat.util.Constants.RestAPI;
import com.tmobile.edat.util.IamSearchUtil;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;



@Controller
@RequestMapping("mdt")
/** Description of MessagingDataController 
 *
 * MessagingDataController class is used for Msg. Data tool.
 * 
 * 
 */
public class MessagingDataController {

	private static final Logger logger = Logger
			.getLogger(MessagingDataController.class);

	@Autowired
	@Qualifier("MessagingDataComponent")
	MessagingDataComponent messageDataComponent = null;

	/**
	 *
	 * mdt() method is used for show Msg Data tool main page.
	 * 
	 */
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public String mdt() {
		return "/msg/mdt";
	}

	/**
	 *
	 * iam(@RequestBody MessagingSearchRequest messageSearchRequest,HttpSession
	 * session) method is used for search request IAM . messageSearchRequest
	 * parameter is used for takes input from user. session parameter is used
	 * for get current session of user.
	 * 
	 */
	@RequestMapping(value = "/iam", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessagingDataResponse iam(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) throws UserException {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.getIAMSearch(messageSearchRequest);
	}

	/**
	 *
	 * smsc(@RequestBody MessagingSearchRequest messageSearchRequest,HttpSession
	 * session) method is used for message data search request o SMSC. *
	 * messageSearchRequest parameter is used for takes input from user. session
	 * parameter is used for get current session of user.
	 * 
	 */
	@RequestMapping(value = "/smsc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessagingDataResponse smsc(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) throws UserException {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.getSMSCSearch(messageSearchRequest);
	}

	/**
	 *
	 * mmsc(@RequestBody MessagingSearchRequest messageSearchRequest,HttpSession
	 * session) method is used for show mmsc page. messageSearchRequest
	 * parameter is used for takes input from user. session parameter is used
	 * for get current session of user.
	 * 
	 */
	@RequestMapping(value = "/mmsc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessagingDataResponse mmsc(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) throws UserException {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.getMMSSearch(messageSearchRequest);
	}

	/**
	 *
	 * addSubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for add SMSC-Aci
	 * Subscriber. messageSearchRequest parameter is used for takes input from
	 * user. session parameter is used for get current session of user.
	 */
	@RequestMapping(value = "/addSubscriber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> addSubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.addSubscriber(messageSearchRequest);
	}

	/**
	 *
	 * modifySubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for modify
	 * SMSC-Aci Subscriber. messageSearchRequest parameter is used for takes
	 * input from user. session parameter is used for get current session of
	 * user.
	 */
	@RequestMapping(value = "/modifySubscriber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> modifySubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.modifySubscriber(messageSearchRequest);
	}

	/**
	 *
	 * deleteDubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for delete
	 * SMSC-Aci Subscriber. messageSearchRequest parameter is used for takes
	 * input from user. session parameter is used for get current session of
	 * user.
	 */
	@RequestMapping(value = "/deleteSubscriber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> deleteSubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.deleteSubscriber(messageSearchRequest);
	}

	/**
	 *
	 * deleteSubscriberServiceSpecific(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Delete
	 * Subscriber ServiceSpecific. messageSearchRequest parameter is used for
	 * takes input from user. session parameter is used for get current session
	 * of user.
	 */
	@RequestMapping(value = "/deleteSubscriberServiceSpecific", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> deleteSubscriberServiceSpecific(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent
				.deleteSubscriberServiceSpecific(messageSearchRequest);
	}

	/**
	 *
	 * getSubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Get SMSC-Mav
	 * Subscriber. messageSearchRequest parameter is used for takes input from
	 * user. session parameter is used for get current session of user.
	 */
	@RequestMapping(value = "/getSubscriber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> getSubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.getSubscriber(messageSearchRequest);
	}

	/**
	 *
	 * getSubscriberServiceSpecific(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Get
	 * Subscriber Specific. messageSearchRequest parameter is used for takes
	 * input from user. session parameter is used for get current session of
	 * user.
	 */
	@RequestMapping(value = "/getSubscriberServiceSpecific", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> getSubscriberServiceSpecific(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent
				.getSubscriberServiceSpecific(messageSearchRequest);
	}

	/**
	 *
	 * putSubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Add/Modify
	 * Subscriber of MMSC . messageSearchRequest parameter is used for takes
	 * input from user. session parameter is used for get current session of
	 * user.
	 */
	@RequestMapping(value = "/putSubscriber", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> putSubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.putSubscriber(messageSearchRequest);
	}

	/**
	 *
	 * addSMSCSubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Add SMSC
	 * Subscriber. messageSearchRequest parameter is used for takes input from
	 * user. session parameter is used for get current session of user.
	 */

	@RequestMapping(value = { "/addSMSCSubscriber" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessagingDataResponse addSMSCSubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) throws UserException {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent
				.addSMSCSubscriber(messageSearchRequest);
	}

	/**
	 *
	 * deleteSMSC(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Delete SMSC
	 * Subscriber. messageSearchRequest parameter is used for takes input from
	 * user. session parameter is used for get current session of user.
	 */

	@RequestMapping(value = "/deleteSMSC", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessagingDataResponse deleteSMSC(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) throws UserException {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent.deleteSMSC(messageSearchRequest);
	}

	/**
	 * 
	 * modifySMSCSubscriber(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for Modify SMSC
	 * Subscriber. messageSearchRequest parameter is used for takes input from
	 * user. session parameter is used for get current session of user.
	 */

	@RequestMapping(value = { "/modifySMSCSubscriber" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessagingDataResponse modifySMSCSubscriber(
			@RequestBody MessagingSearchRequest messageSearchRequest,
			HttpSession session) throws UserException {
		messageSearchRequest.setSessionConstants((SessionConstants) session
				.getAttribute("SessionConstants"));
		return this.messageDataComponent
				.modifySMSCSubscriber(messageSearchRequest);
	}

    /**
   	*
   	* deleteMmscSubscriber(@RequestBody MessagingSearchRequest messageSearchRequest,HttpSession session) method is used for Delete MMSC Subscriber.
   	*  messageSearchRequest parameter is used for takes input from user.
   	*  session parameter is used for get current session of user. 
   	*/
    @RequestMapping(value="/deleteMmscSubscriber",
			method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> deleteMmscSubscriber(@RequestBody MessagingSearchRequest messageSearchRequest,HttpSession session){ 
    	messageSearchRequest.setSessionConstants((SessionConstants)session.getAttribute("SessionConstants"));
    	return this.messageDataComponent.deleteMmscSubscriber(messageSearchRequest);
    }   
    
    
    @RequestMapping(value="getClientIds",method=RequestMethod.GET)
    @ResponseBody
    public List<ClientIDResponse> getClientIds(HttpSession session) {    	
    	return this.messageDataComponent.getClientIds();    	
    }      
 
    
    /**
	 *
	 * iamPrivacyVault(@RequestBody MessagingSearchRequest
	 * messageSearchRequest,HttpSession session) method is used for search offer
	 * request IAM Privacy Vault . messageSearchRequest parameter is used for
	 * takes input from user. session parameter is used for get current session
	 * of user.
	 * 
	 */
	@RequestMapping(value = "/iamPrivacyVault", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public IamPrivacyVaultDataResponse iamPrivacyVaultOfferSearch(
			@RequestBody Map<String, Object> requestParams, HttpSession session)
			throws UserException {
		IamPrivacyVaultDataResponse iamPrivacyVaultDataResponse = null;

		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");

		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}

		if (null != requestObject) {

			RequestHandler requestHandler = RequestHandler
					.getInstance(requestObject.getTokenParams());
			// make URL
			String uri = UriMaker.makeUri(RestAPI.REST_IAM_PRIVACY_VAULT,
					requestObject.getUrlParameters());
			iamPrivacyVaultDataResponse = requestHandler.sendHttpRequest(uri,
					RequestMediaType.JSON, RequestMethod.GET, urlHeaderParams,
					IamPrivacyVaultDataResponse.class);

			logger.info("statusCode = "
					+ iamPrivacyVaultDataResponse.getStatusCode()
					+ " message = "
					+ iamPrivacyVaultDataResponse.getStatusMessage());
		}
		return iamPrivacyVaultDataResponse;
	}


	/**
	 * method to link MSISDN Profile
	 * 
	 * @param messageSearchRequest
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/linkmsisdn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public IamLinkMsisdnResponse iamLinkMsisdn(
			@RequestBody Map<String, Object> requestParams, HttpSession session)
			throws UserException {
		IamLinkMsisdnResponse iamLinkMsisdnResponse = null;

		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");

		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}

		if (null != requestObject) {

			RequestHandler requestHandler = RequestHandler
					.getInstance(requestObject.getTokenParams());

			Map<String, Object> smsPinParameters = ClientUtil
					.getSmsPinParameters(requestParams);
			BaseResponseObjectModel iamGenerateSMSPinResponse = null;
			int sms_pin = -1;

			if (null != smsPinParameters) {

				// make URL
				String uri = UriMaker.makeUri(RestAPI.REST_IAM_GENERATE_PIN,
						smsPinParameters);
				iamGenerateSMSPinResponse = requestHandler.sendHttpRequest(uri,
						RequestMediaType.JSON, RequestMethod.GET,
						urlHeaderParams, IamGenerateSMSPinResponse.class);
				if (null != iamGenerateSMSPinResponse
						&& (iamGenerateSMSPinResponse.getStatusCode() == 200)) {
					// cretae Jdbc conection and get generated pin from server
					// database
					if (smsPinParameters.containsKey("msisdn")) {
						sms_pin = getSmsPinFromServerDataBase((String) smsPinParameters
								.get("msisdn"));
					}
				}
			}
			if (sms_pin != -1) {
				if (requestObject.getUrlParameters().containsKey("sms_pin")) {
					requestObject.getUrlParameters().put("sms_pin",
							String.valueOf(sms_pin));
				}
				// make URL
				String uri = UriMaker.makeUri(RestAPI.REST_IAM_LINK_MSISDN,
						requestObject.getUrlParameters());
				iamLinkMsisdnResponse = requestHandler.sendHttpRequest(uri,
						RequestMediaType.JSON, RequestMethod.GET,
						urlHeaderParams, IamLinkMsisdnResponse.class);
			}
		}
		logger.info("statusCode = " + iamLinkMsisdnResponse.getStatusCode()
				+ " message = " + iamLinkMsisdnResponse.getStatusMessage());
		return iamLinkMsisdnResponse;
	}

	@Autowired
	@Qualifier("dataSource")
	DataSource dataSource;

	/**
	 * methode to create JDBC connection with server database and get SmsPin for
	 * specific generate pin request base on msisdn.
	 * 
	 * @param msisdn
	 * @return SMSPin
	 */
	private int getSmsPinFromServerDataBase(String msisdn) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String query = "select pin_code from CONTRACT where msisdn=?";
		int sms_pin = jdbcTemplate.queryForObject(query,
				new Object[] { msisdn }, Integer.class);

		return sms_pin;

	}

	/**
	 * method to inlink MSISDN Profile
	 * 
	 * @param messageSearchRequest
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/unlinkmsisdn", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponseObjectModel unLinkMSISDNProfile(
			@RequestBody Map<String, Object> requestParams, HttpSession session)
			throws UserException {
		BaseResponseObjectModel unLinkMSISDNResponse = null;
		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");

		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}

		IamProfileResponse iamProfileResponse = null;
		String user_id = null;
		if (null != requestObject) {
			RequestHandler requestHandler = RequestHandler
					.getInstance(requestObject.getTokenParams());
			Map<String, Object> userIdParameters = ClientUtil
					.getUserIdParameters(requestParams);

			if (null != userIdParameters) {

				// make URL
				String uri = UriMaker.makeUri(RestAPI.REST_IAM_PROFILE,
						userIdParameters);
				iamProfileResponse = requestHandler.sendHttpRequest(uri,
						RequestMediaType.JSON, RequestMethod.GET,
						urlHeaderParams, IamProfileResponse.class);
				if (null != iamProfileResponse) {
					if (iamProfileResponse.getStatusCode() == 200) {
						user_id = iamProfileResponse.getIamProfile()
								.getUserId();
						logger.info("MessagingDataController.unLinkMSISDNProfile() user_id = "
								+ user_id);
					} else {
						// TODO:handle profile not found exception exception
						// here
						logger.error("iamProfileResponse statusCode = "
								+ iamProfileResponse.getStatusCode()
								+ " message = "
								+ iamProfileResponse.getStatusMessage());
					}
				}
			}

			if (null != user_id
					&& requestObject.getUrlParameters().containsKey("user_id")) {
				requestObject.getUrlParameters().put("user_id", user_id);
			}

			String uri = UriMaker.makeUri(RestAPI.REST_IAM_UNLINK_MSISDN,
					requestObject.getUrlParameters());
			unLinkMSISDNResponse = requestHandler.sendHttpRequest(uri,
					RequestMediaType.JSON, RequestMethod.GET, urlHeaderParams,
					BaseResponseObjectModel.class);
		}
		logger.info("statusCode = " + unLinkMSISDNResponse.getStatusCode()
				+ " message = " + unLinkMSISDNResponse.getStatusMessage());

		return unLinkMSISDNResponse;
}
	
	
	@SuppressWarnings("null")
	@RequestMapping(value = "/iamsearch", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String,ArrayList<IAMSearchResponse>> iamSearch(
			@RequestBody Map<String, Object> requestParams, HttpSession session)
			throws UserException {
		Map<String,ArrayList<IAMSearchResponse>> iamSearchResponses = new LinkedHashMap<String, ArrayList<IAMSearchResponse>>();
		ArrayList<IAMSearchResponse> al = new ArrayList<IAMSearchResponse>();
		IAMSearchResponse iamSearchResponse = new IAMSearchResponse();
		String urlParams = "";
		String keyName = "";
		for (Map.Entry<String, Object> entry : requestParams.entrySet()) {
			urlParams = entry.getKey();
			if(urlParams.equals("urlParams")){
				Map<String,String> hashMap = (HashMap<String, String>)entry.getValue();
				for(Map.Entry<String, String> entrySet :hashMap.entrySet()){
					keyName = entrySet.getKey();
				}
				break;
			}
		}
		
		
		if(Constants.MSISDN.equals(keyName)) {
			iamSearchResponse =	IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.CONSUMER);
			al.add(iamSearchResponse);
			iamSearchResponses.put("consumerapi",al);
			al = new ArrayList<IAMSearchResponse>();
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.CONTRACT);
			al.add(iamSearchResponse);
			iamSearchResponses.put("contractapi",al);
			al = new ArrayList<IAMSearchResponse>();
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.PERMISSION);
			al.add(iamSearchResponse);
			iamSearchResponses.put("permissionapi",al);
			al = new ArrayList<IAMSearchResponse>();
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.PRIVACY);
			al.add(iamSearchResponse);
			iamSearchResponses.put("privacyapi",al);
			al = new ArrayList<IAMSearchResponse>();
			
		}
		
		else if(Constants.EMAIL.equals(keyName) || Constants.USERNAME.equals(keyName) || Constants.USERID.equals(keyName)) {
			iamSearchResponse =	IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.CONSUMER);
			al.add(iamSearchResponse);
			iamSearchResponses.put("consumerapi",al);
			al = new ArrayList<IAMSearchResponse>();
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.REBIILION);
			al.add(iamSearchResponse);
			iamSearchResponses.put("rebillion",al);
			al = new ArrayList<IAMSearchResponse>();
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.PERMISSION);
			al.add(iamSearchResponse);
			iamSearchResponses.put("permissionapi",al);
			al = new ArrayList<IAMSearchResponse>();
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.PRIVACY);
			al.add(iamSearchResponse);
			iamSearchResponses.put("privacyapi",al);
			al = new ArrayList<IAMSearchResponse>();
		
		}
		
		else if(Constants.IMSI.equals(keyName) || Constants.CONTRACTID.equals(keyName)){
			iamSearchResponse = IamSearchUtil.iamSearchApi(requestParams, IAMSearchAPIEnum.CONTRACT);
			al.add(iamSearchResponse);
			iamSearchResponses.put("contractapi",al);
			al = new ArrayList<IAMSearchResponse>();
		}
		
		System.out.println(">>> iamSearchResponses = "+ iamSearchResponses.size());
		return iamSearchResponses;

	}
	
		@RequestMapping(value = "/tokenGenerator", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public AccessTokenResponse generateToken(
			@RequestBody Map<String, Object> requestParams, HttpSession session)
			throws UserException {
		AccessTokenResponse response = null;
		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");

		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}

		if (null != requestObject) {

			RequestHandler requestHandler = RequestHandler.getInstance();
			// make URL
			String uri = UriMaker.makeUri(RestAPI.REST_IAM_TOKEN,
					requestObject.getTokenParams());
			response = requestHandler.sendHttpRequest(uri,
					RequestMediaType.JSON, RequestMethod.GET, urlHeaderParams,
					AccessTokenResponse.class);
		}
		return response;
	}
	
		/**
	 * This method is used to lock the user account.
	 * 
	 * @param requestParams
	 *            is used to take the input of user.
	 * @param session
	 *            parameter is used for get current session of user.
	 * @return
	 * @throws UserException
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/lockAccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public LockAccountResponse lockAccount(
			@RequestBody Map<String, Object> requestParams, HttpSession session)
			throws UserException {
		LockAccountResponse response = null;
		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");

		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}

		if (null != requestObject) {

			RequestHandler requestHandler = RequestHandler
					.getInstance(requestObject.getTokenParams());
			// make URL
			String uri = UriMaker.makeUri(RestAPI.REST_IAM_LOCK,
					requestObject.getUrlParameters());
			response = requestHandler.sendHttpRequest(uri,
					RequestMediaType.JSON, RequestMethod.GET, urlHeaderParams,
					LockAccountResponse.class);
		}
		return response;
	}

	/**
	 * This method is used to unlock the user account.
	 * 
	 * @param requestParams
	 *            is used to take the input of user.
	 * @param session
	 *            parameter is used for get current session of user.
	 * @return
	 * @throws UserException
	 */

	@RequestMapping(value = "/unLockAccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UnLockResponse unLockAccount(
			@RequestBody Map<String, Object> requestParams, HttpSession session) {
		UnLockResponse response = null;
		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");

		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}

		if (null != requestObject) {

			RequestHandler requestHandler = RequestHandler
					.getInstance(requestObject.getTokenParams());
			// make URL
			String uri = UriMaker.makeUri(RestAPI.REST_IAM_UNLOCK,
					requestObject.getUrlParameters());
			response = requestHandler.sendHttpRequest(uri,
					RequestMediaType.JSON, RequestMethod.GET, urlHeaderParams,
					UnLockResponse.class);
		}
		return response;
	}

}
