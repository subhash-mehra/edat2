package com.tmobile.edat.messaging.repository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.LdapName;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.messaging.dto.request.MessagingSearchRequest;
import com.tmobile.edat.messaging.dto.response.AccessTokenResponse;
import com.tmobile.edat.messaging.dto.response.ClientIDResponse;
import com.tmobile.edat.messaging.dto.response.MessagingDataResponse;
import com.tmobile.edat.messaging.dto.response.UnLockResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;


@Repository("MessagingDataRepository")
public class MessagingDataRepositoryImpl implements MessagingDataRepository {

	private static final Logger logger = Logger.getLogger(MessagingDataRepositoryImpl.class);

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";

	private static String SECURITY_PRINCIPAL_IAM = "cn=Directory Manager";
	private static String SECURITY_CREDENTIALS_IAM = "oracle123";
	private String PROVIDER_URL_IAM;

	String SEARCH_QUERY_DEFAULT = "ou=subscribers,c=us,dc=t-mobile,dc=com";
	String SEARCH_FILTER_DEFAULT = "(%s= %s)";

	private static String SECURITY_PRINCIPAL_SMSC = "cn=directory manager,o=ds.t-mobile.com";
	private static String SECURITY_CREDENTIALS_SMSC = "admin123";
	private String PROVIDER_URL_SMSC; // "ldap://10.168.208.206:389/";
										// 10.168.208.206

	String SEARCH_QUERY_SMSC = "o=ds.t-mobile.com";
	String SEARCH_QUERY_SMSC_SEARCH = "o=ds.t-mobile.com,telephonenumber=%s";
	String SEARCH_FILTER_SMSC = "(telephonenumber=%s)";

	// String SEARCH_FILTER_DEFAULT = "(tmIMSI= 310260778885001)";
	@Override
	public MessagingDataResponse doIAMSearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException {
		MessagingDataResponse iamDataResponse = new MessagingDataResponse();
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		Map<String, String> attributes = new HashMap<String, String>();
		String filter = "";
		logger.info("In MessagingDataRepositoryImpl doIAMSearch with search_type  := "
				+ search_type + "++++key+++" + key);
		PROVIDER_URL_IAM = "ldap://"
				+ SessionConstantsFactory.getSessionConstant("LDAP_SERVER_IAM",
						sessionConstants) + "/";
		System.out
				.println("##################### IAM ########################");
		try {

			switch (search_type) {
			case Constants.SEARCH_TYPE_MSISDN:
				logger.info("In MessagingDataRepositoryImpl  providerURL:= "
						+ PROVIDER_URL_IAM);
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				filter = String.format(SEARCH_FILTER_DEFAULT, "mobile", key);
				break;
			case Constants.SEARCH_TYPE_IMSI:
				logger.info("In MessagingDataRepositoryImpl  providerURL:= "
						+ PROVIDER_URL_IAM);

				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				filter = String.format(SEARCH_FILTER_DEFAULT, "tmIMSI", key);
				break;
			case Constants.SEARCH_TYPE_CUSTID:
				logger.info("In MessagingDataRepositoryImpl  providerURL:= "
						+ PROVIDER_URL_IAM);

				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				filter = String.format(SEARCH_FILTER_DEFAULT, "userid", key);
				break;
			case Constants.SEARCH_TYPE_EMAIL:
				logger.info("In MessagingDataRepositoryImpl  providerURL:= "
						+ PROVIDER_URL_IAM);

				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				filter = String.format(SEARCH_FILTER_DEFAULT, "IAMEmail", key);
				break;

			default:
				break;
			}

			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_IAM, SECURITY_CREDENTIALS_IAM,
					PROVIDER_URL_IAM));

			if (ctx != null) {
				try {

					logger.info("IAM search query :" + SEARCH_QUERY_DEFAULT
							+ "\nfilter :" + filter);

					NamingEnumeration<SearchResult> result = ctx.search(
							SEARCH_QUERY_DEFAULT, filter, sc);
					logger.info("IAM search result :"
							+ result.hasMoreElements());

					logger.info("=============================================");
					while (result != null && result.hasMoreElements()) {
						// FIXME read the result and return the response
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());
						logger.info("DN has " + dn.size() + " RDNs: ");
						for (int i = 0; i < dn.size(); i++) {
							// System.out.println("RDNs : " + dn.get(i));
							if (dn.get(i).contains("userid")) {
								String userid = dn.get(i).toString();
								attributes.put("userid", userid.substring(
										userid.indexOf("=") + 1,
										userid.length()));
							}
						}
						Attributes attrs = rs.getAttributes();
						NamingEnumeration<String> atyrs = attrs.getIDs();

						while (atyrs != null && atyrs.hasMoreElements()) {
							try {
								String id = atyrs.next().toString();
								attributes.put(id, attrs.get(id).get()
										.toString());
							} catch (Exception e) {
								logger.error("error : " + e.toString());
								iamDataResponse
										.setApplicationError(new ApplicationError(
												e.getMessage()));
							}
						}
						logger.info("Serarched Attributes is::"
								+ attributes.toString());
					}

				} catch (NamingException e) {
					logger.error("NamingException : " + e.getMessage());
					e.printStackTrace();
					iamDataResponse.setApplicationError(new ApplicationError(e
							.getMessage()));

				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						iamDataResponse
								.setApplicationError(new ApplicationError(e
										.getMessage()));
						logger.error("NamingException in Finally : "
								+ e.getMessage());
						// e.printStackTrace();
					}
				}
			}
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			throw new UserException(e1.getMessage());
		}
		iamDataResponse.setAttributes(attributes);
		return iamDataResponse;
	}

	@Override
	public MessagingDataResponse doSMSCSearch(String msisdn,
			SessionConstants sessionConstants) throws UserException {
		LdapContext ctx = null;
		MessagingDataResponse iamDataResponse = new MessagingDataResponse();
		SearchControls sc = new SearchControls();
		Map<String, String> attributes = new HashMap<String, String>();
		String filter = "";

		try {
			PROVIDER_URL_SMSC = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_SMSC", sessionConstants) + "/";
			logger.info("##################### SMSC ########################"
					+ "providerURL :" + PROVIDER_URL_SMSC);

			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			filter = String.format(SEARCH_FILTER_SMSC, msisdn); // 12064465370

			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));
			if (ctx != null) {
				try {

					logger.info("SMSC search query :" + SEARCH_QUERY_SMSC
							+ "\nfilter :" + filter);

					NamingEnumeration<SearchResult> result = ctx.search(
							SEARCH_QUERY_SMSC, filter, sc);

					logger.info("SMSC search result :"
							+ result.hasMoreElements());
					logger.info("==================######===========================");
					while (result != null && result.hasMoreElements()) {
						// FIXME read the result and return the response
						SearchResult rs = result.next();
						LdapName dn = new LdapName(rs.getNameInNamespace());

						Attributes attrs = rs.getAttributes();
						NamingEnumeration<String> atyrs = attrs.getIDs();

						while (atyrs != null && atyrs.hasMoreElements()) {
							try {
								String id = atyrs.next().toString();
								attributes.put(id, attrs.get(id).get()
										.toString());

							} catch (Exception e) {
								logger.error("error : " + e.getMessage());
								iamDataResponse
										.setApplicationError(new ApplicationError(
												e.getMessage()));
							}
						}
						// System.out.println(attributes.toString());
						logger.info("serarched attributes is::"
								+ attributes.toString());
					}

				} catch (NamingException e) {
					logger.error("NamingException:::" + e.getMessage());
					iamDataResponse.setApplicationError(new ApplicationError(e
							.getMessage()));
					// e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						logger.error("NamingException: in finally::"
								+ e.getMessage());
						iamDataResponse
								.setApplicationError(new ApplicationError(e
										.getMessage()));
						// e.printStackTrace();
					}
				}
			} else {
				iamDataResponse.setApplicationError(new ApplicationError(
						"node not reachable"));
			}
		} catch (Exception ex) {
			throw new UserException(ex.getMessage());
		}
		iamDataResponse.setAttributes(attributes);
		return iamDataResponse;
	}

	@Override
	public MessagingDataResponse doMMSCSearch(String msisdn,
			SessionConstants sessionConstants) throws UserException {
		logger.info("##################### MMSC ########################");
		Map<String, String> attributes = new HashMap<String, String>();
		MessagingDataResponse mmscDataResponse = new MessagingDataResponse();
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant("SERVER_MMSC",
							sessionConstants)
					+ "/SPSSubscriberProvisioningService/SPSSubscriberProvisioningServiceImpl";
			logger.info(" MMSC url is######### :" + url);
			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequest(msisdn), url);
			String result = soapMessageToString(soapResponse);
			logger.info("result===================== :"
					+ soapResponse.toString());
			String xmlString = null;

			attributes = convertToJavaObjectForMMSCSearch(result);

			soapConnection.close();

		} catch (Exception e) {
			logger.error("Error occurred while sending SOAP Request to Server"
					+ e.getMessage());
			throw new UserException(e.getMessage());

		}
		mmscDataResponse.setAttributes(attributes);
		return mmscDataResponse;
	}

	private SOAPMessage createSOAPRequest(String msisdn) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://www.mavenir.com/sps/services/mmsc/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("GetSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("userInfo");
		soapBodyUserInfo.addChildElement("userId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("msisdn").addTextNode(msisdn);
		subscriber.addChildElement("sourceSystem").addTextNode("SwitchControl");

		/*
		 * MimeHeaders headers = soapMessage.getMimeHeaders();
		 * headers.addHeader("SOAPAction", "");
		 */
		soapMessage.saveChanges();

		/* Print the request message */

		soapMessage.writeTo(System.out);

		logger.error("soapMessage==========================="
				+ soapMessage.toString());
		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private String printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();

		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.print("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
		String xmlString = result.getWriter().toString();
		return xmlString;
	}

	private Map<String, String> convertToJavaObject(String xmlString) {
		logger.info("Enter In conver sion String to Map");
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("subscriberData");
			// iterate the Subscriber data
			logger.info("Nodes length:= " + nodes.getLength());
			for (int i = 0; i < nodes.getLength(); i++) {

				NodeList innerList = nodes.item(i).getChildNodes();

				for (int j = 0; j < innerList.getLength(); j++) {
					attributes.put(innerList.item(j).getNodeName(), innerList
							.item(j).getTextContent());
					logger.info("Xml Data In MAp Name:"
							+ innerList.item(j).getNodeName() + "::::Value:::"
							+ innerList.item(j).getTextContent());
				}
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			logger.error("ParserConfigurationException In parsing the Xml :: "
					+ e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("SAXException In  the Xml :: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IOException In  Xml :: " + e.getMessage());
			e.printStackTrace();
		}
		return attributes;
	}

	private Map<String, String> convertToJavaObjectForGetSubscriber(
			String xmlString) {
		logger.info("Enter In conver sion String to Map");
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("SubscriberDetails");
			NodeList subscriberDataNode = doc
					.getElementsByTagName("SubscriberData");
			// iterate the Subscriber data
			logger.info("Nodes length:= " + nodes.getLength());
			if (nodes.getLength() > 0) {
				for (int i = 0; i < nodes.getLength(); i++) {

					NodeList innerList = nodes.item(i).getChildNodes();

					for (int j = 0; j < innerList.getLength(); j++) {
						if (innerList.item(j).getNodeName()
								.equalsIgnoreCase("MSISDN")) {
							attributes.put(innerList.item(j).getNodeName(),
									innerList.item(j).getTextContent());
						}

						logger.info("Xml Data In MAp Name:"
								+ innerList.item(j).getNodeName()
								+ "::::Value:::"
								+ innerList.item(j).getTextContent());
					}
				}

				for (int i = 0; i < subscriberDataNode.getLength(); i++) {
					NodeList innerList = subscriberDataNode.item(i)
							.getChildNodes();

					for (int j = 0; j < innerList.getLength(); j++) {

						attributes.put(innerList.item(j).getNodeName(),
								innerList.item(j).getTextContent());
						logger.info("Xml Data In MAp Name:"
								+ innerList.item(j).getNodeName()
								+ "::::Value:::"
								+ innerList.item(j).getTextContent());
					}
				}
			} else {
				NodeList faultstringNode = doc
						.getElementsByTagName("faultstring");

				if (faultstringNode.getLength() > 0) {
					for (int i = 0; i < faultstringNode.getLength(); i++) {
						attributes.put(faultstringNode.item(i).getNodeName(),
								faultstringNode.item(i).getTextContent());

						NodeList validationErrorNode = doc
								.getElementsByTagName("ns4:ServiceError");
						NodeList innerList = validationErrorNode.item(i)
								.getChildNodes();
						for (int j = 0; j < innerList.getLength(); j++) {
							if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns4:code")) {
								attributes.put("Code", innerList.item(j)
										.getTextContent());
							} else if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns4:reason")) {
								attributes.put("Reason", innerList.item(j)
										.getTextContent());
							} else {
								attributes.put(innerList.item(j).getNodeName(),
										innerList.item(j).getTextContent());
							}

							logger.info("Xml Data In MAp Name:"
									+ innerList.item(j).getNodeName()
									+ "::::Value:::"
									+ innerList.item(j).getTextContent());
						}
					}
				} else {
					attributes.put("success", "success");
				}
			}
			System.out.println("attributes-----" + attributes);

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			logger.error("ParserConfigurationException In parsing the Xml :: "
					+ e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("SAXException In  the Xml :: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IOException In  Xml :: " + e.getMessage());
			e.printStackTrace();
		}
		return attributes;
	}

	private Map<String, String> convertToJavaObjectForAddSubscriber(
			String xmlString) {
		logger.info("Enter In conver sion String to Map");
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);

			NodeList faultstringNode = doc.getElementsByTagName("faultstring");

			if (faultstringNode.getLength() > 0) {
				for (int i = 0; i < faultstringNode.getLength(); i++) {
					attributes.put(faultstringNode.item(i).getNodeName(),
							faultstringNode.item(i).getTextContent());

					NodeList validationErrorNode = doc
							.getElementsByTagName("ns2:ValidationError");
					NodeList innerList = validationErrorNode.item(i)
							.getChildNodes();
					for (int j = 0; j < innerList.getLength(); j++) {
						if (innerList.item(j).getNodeName()
								.equalsIgnoreCase("ns2:code")) {
							attributes.put("Code", innerList.item(j)
									.getTextContent());
						} else if (innerList.item(j).getNodeName()
								.equalsIgnoreCase("ns2:reason")) {
							attributes.put("Reason", innerList.item(j)
									.getTextContent());
						}

					}
				}
			} else {
				attributes.put("success", "success");
			}
		} catch (Exception e) {

		}
		logger.info("Attribute==============================" + attributes);
		return attributes;
	}

	private Map<String, String> convertToJavaObjectForPutSubscriber(
			String xmlString) {
		logger.info("Enter In conver sion String to Map");
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);

			NodeList faultstringNode = doc.getElementsByTagName("faultstring");

			if (faultstringNode.getLength() > 0) {
				for (int i = 0; i < faultstringNode.getLength(); i++) {
					attributes.put(faultstringNode.item(i).getNodeName(),
							faultstringNode.item(i).getTextContent());

					NodeList validationErrorNode = doc
							.getElementsByTagName("ns2:ValidationError");
					NodeList innerList = validationErrorNode.item(i)
							.getChildNodes();
					for (int j = 0; j < innerList.getLength(); j++) {
						if (innerList.item(j).getNodeName()
								.equalsIgnoreCase("ns2:code")) {
							attributes.put("Code", innerList.item(j)
									.getTextContent());
						} else if (innerList.item(j).getNodeName()
								.equalsIgnoreCase("ns2:reason")) {
							attributes.put("Reason", innerList.item(j)
									.getTextContent());
						}

					}
				}
			} else {
				attributes.put("success", "success");
			}
		} catch (Exception e) {

		}
		logger.info("Attribute==============================" + attributes);
		return attributes;
	}

	private Map<String, String> convertToJavaObjectForMMSCDelete(
			String xmlString) {
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);
			// NodeList nodes =
			// doc.getElementsByTagName("ns3:GetSubscriberResponse");
			NodeList nodes = doc
					.getElementsByTagName("ns3:DeleteSubscriberResponse");

			if (nodes.getLength() > 0) {
				for (int i = 0; i < nodes.getLength(); i++) {

					attributes.put("success", nodes.item(i).getTextContent());
				}
			} else {
				NodeList faultstringNode = doc
						.getElementsByTagName("faultstring");

				if (faultstringNode.getLength() > 0) {
					for (int i = 0; i < faultstringNode.getLength(); i++) {
						attributes.put(faultstringNode.item(i).getNodeName(),
								faultstringNode.item(i).getTextContent());

						NodeList validationErrorNode = doc
								.getElementsByTagName("ns4:ServiceError");
						NodeList innerList = validationErrorNode.item(i)
								.getChildNodes();
						for (int j = 0; j < innerList.getLength(); j++) {
							if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns4:code")) {
								attributes.put("Code", innerList.item(j)
										.getTextContent());
							} else if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns4:Reason")) {
								attributes.put("Reason", innerList.item(j)
										.getTextContent());
							} else {
								attributes.put("success", innerList.item(j)
										.getTextContent());
							}

						}
					}
				}
			}

			System.out.println("attributes-----" + attributes);

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		} catch (SAXException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return attributes;
	}

	private Map<String, String> convertToJavaObjectForMMSCSearch(
			String xmlString) {
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);
			// NodeList nodes =
			// doc.getElementsByTagName("ns3:GetSubscriberResponse");
			NodeList nodes = doc.getElementsByTagName("subscriberData");
			System.out.println("1========" + nodes);
			System.out.println("2========" + nodes.getLength());
			// NodeList nl = n.getChildNodes();

			// iterate the Subscriber data

			if (nodes.getLength() > 0) {
				for (int i = 0; i < nodes.getLength(); i++) {
					NodeList subDataList = doc
							.getElementsByTagName("subscriberData");
					for (int j = 0; j < nodes.item(i).getChildNodes()
							.getLength(); j++) {

						NodeList subscriberDataNode = nodes.item(i)
								.getChildNodes();

						attributes.put(
								subscriberDataNode.item(j).getNodeName(),
								subscriberDataNode.item(j).getTextContent());
					}
				}

			} else {
				NodeList faultstringNode = doc
						.getElementsByTagName("faultstring");

				if (faultstringNode.getLength() > 0) {
					for (int i = 0; i < faultstringNode.getLength(); i++) {
						attributes.put(faultstringNode.item(i).getNodeName(),
								faultstringNode.item(i).getTextContent());

						NodeList validationErrorNode = doc
								.getElementsByTagName("detail");
						NodeList innerList = validationErrorNode.item(i)
								.getChildNodes();
						for (int j = 0; j < innerList.getLength(); j++) {
							if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns4:code")) {
								attributes.put("Code", innerList.item(j)
										.getTextContent());
							} else if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns4:Reason")) {
								attributes.put("Reason", innerList.item(j)
										.getTextContent());
							} else {
								attributes.put(innerList.item(j).getNodeName(),
										innerList.item(j).getTextContent());
							}

						}
					}
				} else {
					attributes.put("success", "success");
				}
			}
			System.out.println("attributes-----" + attributes);

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		} catch (SAXException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return attributes;
	}

	public Map<String, String> parseXmltoJava(String xml) {
		Map<String, String> dataMap = new HashMap<String, String>();
		String status = "";
		String errorType = "";
		String message = "";
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			logger.info("Enter with xml");
			DocumentBuilder db = dbf.newDocumentBuilder();
			xml = xml.substring(xml.indexOf("<?xml"));
			logger.info("Enter with xml" + xml);
			InputSource is = new InputSource(new StringReader(xml));
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();
			logger.info("Root element "
					+ doc.getDocumentElement().getNodeName());
			NodeList nodeLst = doc.getElementsByTagName("errorMessage");
			NodeList addResponse = doc.getElementsByTagName("spml:addResponse");
			logger.info("Information of all member" + nodeLst.getLength());
			for (int i = 0; i < nodeLst.getLength(); i++) {
				Node aNode = nodeLst.item(i);
				Node addResponseNode = addResponse.item(i);

				NamedNodeMap attributes = addResponseNode.getAttributes();

				for (int a = 0; a < attributes.getLength(); a++) {
					Node theAttribute = attributes.item(a);
					if (theAttribute.getNodeName() == "AddStatus") {
						status = theAttribute.getNodeValue();
					}

				}

			}

			dataMap.put("status", status);

		} catch (Exception e) {
			logger.info("parseXmltoJava Exception : " + e.getMessage());
			e.printStackTrace();
		}
		return dataMap;

	}

	@Override
	public Map<String, Object> addSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";
		String responseXML = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestAddSubscriber(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getImsi(),
							messagingSearchRequest.getSubStatus(),
							messagingSearchRequest.getLanguage(),
							messagingSearchRequest.getSubType(),
							messagingSearchRequest.getOperatorId(),
							messagingSearchRequest.getProfileId(),
							messagingSearchRequest.getImsEnabled()), url);

			// TODO Process the SOAP Response
			// printSOAPResponse(soapResponse);
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			try {
				dataMap = (Map) convertToJavaObjectForAddSubscriber(result);

				System.out.println();
			} catch (Exception e) {

			}
			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> putSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";
		String responseXML = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant("SERVER_MMSC",
							messagingSearchRequest.getSessionConstants())
					+ "/SPSSubscriberProvisioningService/SPSSubscriberProvisioningServiceImpl";
			logger.info("put Suscriber===========================================================?>");
			SOAPMessage soapResponse = soapConnection
					.call(createSOAPRequestPutSubscriber(messagingSearchRequest),
							url);

			// TODO Process the SOAP Response
			// printSOAPResponse(soapResponse);
			String result = soapMessageToString(soapResponse);
			logger.info("Result===========================================================?>"
					+ result);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForPutSubscriber(result);
			try {
				System.out.println();
			} catch (Exception e) {

			}
			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> modifySubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestModifySubscriber(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getSubType(),
							messagingSearchRequest.getProfileId(),
							messagingSearchRequest.getImsEnabled()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);

			dataMap = (Map) convertToJavaObjectForPutSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> deleteSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestForDelete(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> deleteSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestForDelete(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> getSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequest(messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);
			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}
			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	public String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
				logger.info("In Delete MMSC result in soapMessageToString==================================="
						+ result);
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	@Override
	public Map<String, Object> getSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequest(messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response

			String result = soapMessageToString(soapResponse);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	private SOAPMessage createSOAPRequestPutSubscriber(
			MessagingSearchRequest messagingSearchRequest) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String messageAddress = messagingSearchRequest.getMsisdn()
				+ "/TYPE=PLMN@tmomail.net";
		String npa = "";
		try {
			npa = messagingSearchRequest.getMsisdn().substring(2, 5);
			logger.info("npa===========================================================?>"
					+ npa);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MimeHeaders headers = soapMessage.getMimeHeaders();
		/*
		 * headers.addHeader("SOAPAction", "");
		 * headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		 * headers.addHeader("Accept-Encoding", "gzip,deflate");
		 */

		headers.addHeader("Connection", "keep-alive");
		headers.addHeader("Accept-Encoding", "gzip,deflate");
		headers.addHeader("charset", "utf-8");
		headers.addHeader("Content-Type", "text/xml");
		headers.addHeader("Host", "10.176.75.164:8080");

		String serverURI = "http://www.mavenir.com/sps/services/mmsc/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("soapenv",
				"http://schemas.xmlsoap.org/soap/envelope/");
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("PutSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("userInfo");
		soapBodyUserInfo.addChildElement("userId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("password", "shar").addTextNode(
				"spsuser");

		SOAPElement subscriberData = subscriber
				.addChildElement("subscriberData");
		subscriberData.addChildElement("MSISDN").addTextNode(
				messagingSearchRequest.getMsisdn());
		subscriberData.addChildElement("SubStatus").addTextNode(
				messagingSearchRequest.getSubStatus());
		subscriberData.addChildElement("COS").addTextNode(
				messagingSearchRequest.getCos());
		subscriberData.addChildElement("SubType").addTextNode(
				messagingSearchRequest.getSubType());
		subscriberData.addChildElement("HandsetType").addTextNode(
				messagingSearchRequest.getHandsetType());
		subscriberData.addChildElement("MMSCapable").addTextNode(
				messagingSearchRequest.getMmsCapable());
		subscriberData.addChildElement("MmsAddress")
				.addTextNode(messageAddress);
		;
		subscriberData.addChildElement("NPA").addTextNode(npa);
		subscriberData.addChildElement("OperatorId").addTextNode(
				messagingSearchRequest.getOperatorId());
		subscriberData.addChildElement("SMTPGWDomain").addTextNode(
				"tmomail.net");

		/*
		 * subscriberData.addChildElement("MSISDN").addTextNode("14147121818");
		 * subscriberData.addChildElement("SubStatus").addTextNode("ACT");
		 * subscriberData.addChildElement("COS").addTextNode("20");
		 * subscriberData.addChildElement("SubType").addTextNode("POSTPAID");
		 * subscriberData
		 * .addChildElement("HandsetType").addTextNode("other_mms_phone");
		 * subscriberData.addChildElement("MMSCapable").addTextNode("true");
		 * subscriberData.addChildElement("MmsAddress").addTextNode(
		 * "+14147121818/TYPE=PLMN@tmomail.net");;
		 * subscriberData.addChildElement("NPA").addTextNode("414");
		 * subscriberData.addChildElement("OperatorId").addTextNode("tmus");
		 * subscriberData
		 * .addChildElement("SMTPGWDomain").addTextNode("tmomail.net");
		 */
		subscriber.addChildElement("sourceSystem").addTextNode("SwitchControl");

		soapMessage.saveChanges();

		/* Print the request message */

		soapMessage.writeTo(System.out);

		logger.info("soapMessage===========================================================?>"
				+ soapMessage.toString());
		return soapMessage;
	}

	private SOAPMessage createSOAPRequestAddSubscriber(String msisdn,
			String imsi, String subStatus, String language, String subType,
			String operatorId, String profileId, String imsEnabled)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("AddSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");

		SOAPElement subscriberDetails = subscriber
				.addChildElement("SubscriberDetails");
		subscriberDetails.addChildElement("MSISDN").addTextNode(msisdn);

		SOAPElement subscriberData = subscriberDetails
				.addChildElement("SubscriberData");
		subscriberData.addChildElement("IMSI").addTextNode(imsi);
		subscriberData.addChildElement("SubStatus").addTextNode(subStatus);
		subscriberData.addChildElement("Language").addTextNode(language);
		subscriberData.addChildElement("SubType").addTextNode(subType);
		subscriberData.addChildElement("OperatorId").addTextNode(operatorId);

		SOAPElement subscriberSMSCData = subscriberDetails
				.addChildElement("SubscriberSMSCData");
		subscriberSMSCData.addChildElement("IMSEnabled")
				.addTextNode(imsEnabled);
		subscriberSMSCData.addChildElement("SMSCSubProfileID").addTextNode(
				profileId);

		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	private SOAPMessage createSOAPRequestModifySubscriber(String msisdn,
			String subType, String profileId, String imsEnabled)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("ModifySubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");

		SOAPElement subscriberDetails = subscriber
				.addChildElement("SubscriberDetails");
		subscriberDetails.addChildElement("MSISDN").addTextNode(msisdn);

		SOAPElement subscriberData = subscriberDetails
				.addChildElement("SubscriberData");
		subscriberData.addChildElement("SubType").addTextNode(subType);

		SOAPElement subscriberSMSCData = subscriberDetails
				.addChildElement("SubscriberSMSCData");
		subscriberSMSCData.addChildElement("IMSEnabled")
				.addTextNode(imsEnabled);
		subscriberSMSCData.addChildElement("SMSCSubProfileID").addTextNode(
				profileId);

		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	private SOAPMessage createSOAPRequestForDelete(String msisdn, String service)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("DeleteSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("MSISDN").addTextNode(msisdn);
		if ("SMSC".equalsIgnoreCase(service)) {
			subscriber.addChildElement("ServiceIndicator").addTextNode(service);
		}
		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	private SOAPMessage createSOAPRequestForMmscDelete(String msisdn)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/mmsc/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("DeleteSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("userInfo");
		soapBodyUserInfo.addChildElement("userId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("msisdn").addTextNode(msisdn);

		subscriber.addChildElement("sourceSystem").addTextNode("SwitchControl");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	private SOAPMessage createSOAPRequest(String msisdn, String service)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("GetSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("MSISDN").addTextNode(msisdn);
		if ("SMSC".equalsIgnoreCase(service)) {
			subscriber.addChildElement("ServiceIndicator").addTextNode(service);
		}
		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	public String getUniqueIdentifier() {
		String uniqueIdentifier = "";
		String rnum = "2";
		String currentDateTime = "";
		String subTime = "";
		String subDate = "";
		int indexOfSpace = 0;
		int hexadecimalTime = 0;
		int randomInt = 0;

		DateFormat dateFormat = new SimpleDateFormat("MMddyyyyHHmmss");
		Date date = new Date();

		try {
			currentDateTime = dateFormat.format(date);
			long epoch = System.currentTimeMillis();
			int epoch_t = (int) (epoch / 1000);
			System.out.println("epoch=========== " + epoch_t);
			Random randomGenerator = new Random();
			randomInt = randomGenerator.nextInt((100 - 2) + 1) + 2;

			uniqueIdentifier = currentDateTime + Long.toHexString(epoch_t)
					+ randomInt;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return uniqueIdentifier;

	}

	@Override
	public MessagingDataResponse addSMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) throws UserException {
		logger.info("##################### Starts: Add SMSC Subscriber ########################");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String searchType = "";
		String uniqueIdentifier = "";
		String telephoneNo = "";
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();

		uniqueIdentifier = getUniqueIdentifier();
		query = "uniqueIdentifier=" + uniqueIdentifier
				+ ",ou=subscribers,ou=COI_SMSC,ou=I";

		PROVIDER_URL_SMSC = "ldap://"
				+ SessionConstantsFactory.getSessionConstant(
						"LDAP_SERVER_SMSC",
						messageSearchRequest.getSessionConstants()) + "/";
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));

			if (ctx != null) {
				try {
					if (messageSearchRequest.getPrepaid().equalsIgnoreCase(
							"yes")) {
						BasicAttributes attrs = new BasicAttributes();
						Attribute classes = new BasicAttribute("objectclass");
						classes.add("subscriber");
						classes.add("inetOrgPerson");
						classes.add("person");
						classes.add("top");
						classes.add("smscsubscriber");
						classes.add("organizationalPerson");
						classes.add("psasubscriber");

						attrs.put(classes);

						if (messageSearchRequest.getTelePhoneNo() != ""
								&& messageSearchRequest.getTelePhoneNo() != null) {
							telephoneNo = 1 + messageSearchRequest
									.getTelePhoneNo();
							attrs.put("telephoneNumber", telephoneNo);
						}
						if (messageSearchRequest.getSmscsubprofilenumber() != ""
								&& messageSearchRequest
										.getSmscsubprofilenumber() != null) {
							attrs.put("smscsubprofilenumber",
									messageSearchRequest
											.getSmscsubprofilenumber());
						}
						if (messageSearchRequest.getSubAddress() != ""
								&& messageSearchRequest.getSubAddress() != null) {
							attrs.put("smscsubaddress",
									messageSearchRequest.getSubAddress());
						}
						if (uniqueIdentifier != "") {
							attrs.put("uid", uniqueIdentifier);
						}
						if (messageSearchRequest.getGender() != ""
								&& messageSearchRequest.getGender() != null) {
							attrs.put("gender",
									messageSearchRequest.getGender());
						}
						if (messageSearchRequest.getSn() != ""
								&& messageSearchRequest.getSn() != null) {
							attrs.put("sn", messageSearchRequest.getSn());
						}
						if (messageSearchRequest.getGivenName() != ""
								&& messageSearchRequest.getGivenName() != null) {
							attrs.put("givenName",
									messageSearchRequest.getGivenName());
						}
						if (messageSearchRequest.getEmailId() != ""
								&& messageSearchRequest.getEmailId() != null) {
							attrs.put("mail", messageSearchRequest.getEmailId());
						}
						if (messageSearchRequest.getCn() != ""
								&& messageSearchRequest.getCn() != null) {
							attrs.put("cn", messageSearchRequest.getCn());
						}
						if (uniqueIdentifier != "") {
							attrs.put("uniqueIdentifier", uniqueIdentifier);
						}
						if (messageSearchRequest.getUserAgentProfile() != ""
								&& messageSearchRequest.getUserAgentProfile() != null) {
							attrs.put("useragentprofile",
									messageSearchRequest.getUserAgentProfile());
						}
						if (messageSearchRequest.getUserAgentString() != ""
								&& messageSearchRequest.getUserAgentString() != null) {
							attrs.put("useragentstring",
									messageSearchRequest.getUserAgentString());
						}
						if (messageSearchRequest.getPrepaid() != ""
								&& messageSearchRequest.getPrepaid() != null) {
							attrs.put("prepaid",
									messageSearchRequest.getPrepaid());
						}
						// prepaid Attribute
						if (messageSearchRequest.getTplaCode() != ""
								&& messageSearchRequest.getTplaCode() != null) {
							attrs.put("psatplacode",
									messageSearchRequest.getTplaCode());
						}
						if (messageSearchRequest.getSmscsubprepaid() != ""
								&& messageSearchRequest.getSmscsubprepaid() != null) {
							attrs.put("smscsubprepaid",
									messageSearchRequest.getSmscsubprepaid());
						}
						if (messageSearchRequest.getPsa() != ""
								&& messageSearchRequest.getPsa() != null) {
							attrs.put("psa", messageSearchRequest.getPsa());
						}
						if (messageSearchRequest.getInpCode() != ""
								&& messageSearchRequest.getInpCode() != null) {
							attrs.put("psainpcode",
									messageSearchRequest.getInpCode());
						}
						if (messageSearchRequest.getPsaServices() != ""
								&& messageSearchRequest.getPsaServices() != null) {
							attrs.put("psaservices",
									messageSearchRequest.getPsaServices());
						}
						if (messageSearchRequest.getPsaforceflag() != ""
								&& messageSearchRequest.getPsaforceflag() != null) {
							attrs.put("psaforceflag",
									messageSearchRequest.getPsaforceflag());
						}
						if (messageSearchRequest.getPsaaction() != ""
								&& messageSearchRequest.getPsaaction() != null) {
							attrs.put("psaaction",
									messageSearchRequest.getPsaaction());
						}
						if (messageSearchRequest.getPsamsisdn() != ""
								&& messageSearchRequest.getPsamsisdn() != null) {
							attrs.put("psamsisdn",
									messageSearchRequest.getPsamsisdn());
						}

						if (messageSearchRequest.getPassword() != ""
								&& messageSearchRequest.getPassword() != null) {
							attrs.put("password",
									messageSearchRequest.getPassword());
						}

						if (messageSearchRequest.getMobileType() != ""
								&& messageSearchRequest.getMobileType() != null) {
							attrs.put("mobiletype",
									messageSearchRequest.getMobileType());
						}
						if (messageSearchRequest.getLanguage() != ""
								&& messageSearchRequest.getLanguage() != null) {
							attrs.put("language",
									messageSearchRequest.getLanguage());
						}
						attrs.put("blocked", "no");
						attrs.put("smscsubaddressnumtype", "0");
						attrs.put("smscsubblocked", "0");
						attrs.put("smscsubsmtpfnotif", "0");
						attrs.put("smscsubsmtpfdelack", "0");
						attrs.put("smscsubsmtpfuserack", "0");
						attrs.put("smscsubsmtpfsm", "0");
						attrs.put("smscsubsmtpfsmbinary", "0");
						attrs.put("smscsubsmtpfsmgsmudh", "0");
						attrs.put("smscsubsmtpfsmtdmatsar", "0");
						attrs.put("smscSubMsgArchivingEnabled", "0");
						attrs.put("smscSubMsgArchivingOrg", "0");
						attrs.put("smscSubMsgArchivingRcp", "0");

						ctx.createSubcontext(
								"uniqueidentifier="
										+ uniqueIdentifier
										+ ",ou=subscribers,ou=COI_SMSC,ou=ILN_SMSC1,o=ds.t-mobile.com",
								attrs);
						messagingDataResponse
								.setMessage("Success...............");
					} else {
						BasicAttributes attrs = new BasicAttributes();
						Attribute classes = new BasicAttribute("objectclass");
						classes.add("subscriber");
						classes.add("inetOrgPerson");
						classes.add("person");
						classes.add("top");
						classes.add("smscsubscriber");
						classes.add("organizationalPerson");
						attrs.put(classes);
						if (messageSearchRequest.getTelePhoneNo() != ""
								&& messageSearchRequest.getTelePhoneNo() != null) {
							attrs.put("telephoneNumber",
									messageSearchRequest.getTelePhoneNo());
						}
						if (messageSearchRequest.getSmscsubprofilenumber() != ""
								&& messageSearchRequest
										.getSmscsubprofilenumber() != null) {
							attrs.put("smscsubprofilenumber",
									messageSearchRequest
											.getSmscsubprofilenumber());
						}
						if (messageSearchRequest.getSubAddress() != ""
								&& messageSearchRequest.getSubAddress() != null) {
							attrs.put("smscsubaddress",
									messageSearchRequest.getSubAddress());
						}
						if (uniqueIdentifier != "") {
							attrs.put("uid", uniqueIdentifier);
						}
						if (messageSearchRequest.getGender() != ""
								&& messageSearchRequest.getGender() != null) {
							attrs.put("gender",
									messageSearchRequest.getGender());
						}
						if (messageSearchRequest.getSn() != ""
								&& messageSearchRequest.getSn() != null) {
							attrs.put("sn", messageSearchRequest.getSn());
						}
						if (messageSearchRequest.getGivenName() != ""
								&& messageSearchRequest.getGivenName() != null) {
							attrs.put("givenName",
									messageSearchRequest.getGivenName());
						}
						if (messageSearchRequest.getEmailId() != ""
								&& messageSearchRequest.getEmailId() != null) {
							attrs.put("mail", messageSearchRequest.getEmailId());
						}
						if (messageSearchRequest.getCn() != ""
								&& messageSearchRequest.getCn() != null) {
							attrs.put("cn", messageSearchRequest.getCn());
						}
						if (uniqueIdentifier != "") {
							attrs.put("uniqueIdentifier", uniqueIdentifier);
						}
						if (messageSearchRequest.getUserAgentProfile() != ""
								&& messageSearchRequest.getUserAgentProfile() != null) {
							attrs.put("useragentprofile",
									messageSearchRequest.getUserAgentProfile());
						}
						if (messageSearchRequest.getUserAgentString() != ""
								&& messageSearchRequest.getUserAgentString() != null) {
							attrs.put("useragentstring",
									messageSearchRequest.getUserAgentString());
						}
						if (messageSearchRequest.getPrepaid() != ""
								&& messageSearchRequest.getPrepaid() != null) {
							attrs.put("prepaid",
									messageSearchRequest.getPrepaid());
						}

						if (messageSearchRequest.getPassword() != ""
								&& messageSearchRequest.getPassword() != null) {
							attrs.put("password",
									messageSearchRequest.getPassword());
						}

						if (messageSearchRequest.getMobileType() != ""
								&& messageSearchRequest.getMobileType() != null) {
							attrs.put("mobiletype",
									messageSearchRequest.getMobileType());
						}
						if (messageSearchRequest.getLanguage() != ""
								&& messageSearchRequest.getLanguage() != null) {
							attrs.put("language",
									messageSearchRequest.getLanguage());
						}

						attrs.put("blocked", "no");
						attrs.put("smscsubaddressnumtype", "0");
						attrs.put("smscsubblocked", "0");
						attrs.put("smscsubsmtpfnotif", "0");
						attrs.put("smscsubsmtpfdelack", "0");
						attrs.put("smscsubsmtpfuserack", "0");
						attrs.put("smscsubsmtpfsm", "0");
						attrs.put("smscsubsmtpfsmbinary", "0");
						attrs.put("smscsubsmtpfsmgsmudh", "0");
						attrs.put("smscsubsmtpfsmtdmatsar", "0");
						attrs.put("smscSubMsgArchivingEnabled", "0");
						attrs.put("smscSubMsgArchivingOrg", "0");
						attrs.put("smscSubMsgArchivingRcp", "0");
						ctx.createSubcontext(
								"uniqueidentifier="
										+ uniqueIdentifier
										+ ",ou=subscribers,ou=COI_SMSC,ou=ILN_SMSC1,o=ds.t-mobile.com",
								attrs);
						messagingDataResponse
								.setMessage("Success...............");

					}

				} catch (NamingException e) {
					messagingDataResponse
							.setApplicationError(new ApplicationError(e
									.getMessage()));
					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
		}
		logger.info("##################### Ends: Add TAC ########################");

		return messagingDataResponse;
	}

	@Override
	public MessagingDataResponse deleteSMSC(
			MessagingSearchRequest messageSearchRequest) throws UserException {

		Map<String, String> attributes = new HashMap<String, String>();
		logger.info("##################### Starts: Add SMSC Subscriber ########################");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String searchType = "";
		String uniqueIdentifier = "";
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();

		try {

			PROVIDER_URL_SMSC = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_SMSC",
							messageSearchRequest.getSessionConstants()) + "/";
			logger.info("PROVIDER_URL_SMSC#####################---------------------"
					+ PROVIDER_URL_SMSC);

			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));

			attributes = (Map) doSMSCSearch(messageSearchRequest.getMsisdn(),
					messageSearchRequest.getSessionConstants());
			uniqueIdentifier = attributes.get("uniqueIdentifier");
			logger.info("uniqueIdentifier in Delete#####################---------------------"
					+ uniqueIdentifier);

			List<Attribute> list = new ArrayList<Attribute>();
			if (attributes.size() > 0) {

				if (ctx != null) {
					if (uniqueIdentifier != "") {
						/*
						 * if(attributes.get("telephoneNumber")!="" &&
						 * attributes.get("telephoneNumber")!=null) {
						 * list.add(new
						 * BasicAttribute("telephoneNumber",attributes
						 * .get("telephoneNumber"))); }
						 */

						if (attributes.get("smscsubaddress") != ""
								&& attributes.get("smscsubaddress") != null) {
							list.add(new BasicAttribute("smscsubaddress",
									attributes.get("smscsubaddress")));
						}

						if (attributes.get("gender") != ""
								&& attributes.get("gender") != null) {
							list.add(new BasicAttribute("gender", attributes
									.get("gender")));
						}
						if (attributes.get("sn") != ""
								&& attributes.get("sn") != null) {
							list.add(new BasicAttribute("sn", attributes
									.get("sn")));
						}
						if (attributes.get("givenName") != ""
								&& attributes.get("givenName") != null) {
							list.add(new BasicAttribute("givenName", attributes
									.get("givenName")));
						}
						if (attributes.get("mail") != ""
								&& attributes.get("mail") != null) {
							list.add(new BasicAttribute("mail", attributes
									.get("mail")));
						}
						if (attributes.get("cn") != ""
								&& attributes.get("cn") != null) {
							list.add(new BasicAttribute("cn", attributes
									.get("cn")));
						}

						if (attributes.get("useragentprofile") != ""
								&& attributes.get("useragentprofile") != null) {
							list.add(new BasicAttribute("useragentprofile",
									attributes.get("useragentprofile")));
						}
						/*
						 * if(attributes.get("useragentstring")!="" &&
						 * attributes.get("useragentstring")!=null) {
						 * list.add(new
						 * BasicAttribute("useragentstring",attributes
						 * .get(""))); } if(attributes.get("prepaid")!="" &&
						 * attributes.get("prepaid")!=null) { list.add(new
						 * BasicAttribute("prepaid",attributes.get("prepaid")));
						 * } //prepaid Attribute
						 * if(attributes.get("psatplacode")!="" &&
						 * attributes.get("psatplacode")!=null) { list.add(new
						 * BasicAttribute("psatplacode",attributes.get(""))); }
						 * if(attributes.get("smscsubprepaid")!="" &&
						 * attributes.get("smscsubprepaid")!=null) {
						 * list.add(new
						 * BasicAttribute("smscsubprepaid",attributes
						 * .get("smscsubprepaid"))); }
						 */

						/*
						 * if(attributes.get("psa")!="" &&
						 * attributes.get("psa")!=null) { list.add(new
						 * BasicAttribute("psa",attributes.get("psa"))); }
						 * 
						 * 
						 * 
						 * if(attributes.get("psainpcode")!="" &&
						 * attributes.get("psainpcode")!=null) { list.add(new
						 * BasicAttribute
						 * ("psainpcode",attributes.get("psainpcode"))); }
						 * if(attributes.get("psaservices")!="" &&
						 * attributes.get("psaservices")!=null)
						 * 
						 * { list.add(new
						 * BasicAttribute("psaservices",attributes
						 * .get("psaservices"))); }
						 * 
						 * if(attributes.get("psaforceflag")!="" &&
						 * attributes.get("")!=null) { list.add(new
						 * BasicAttribute("psaforceflag",attributes.get(""))); }
						 * if(attributes.get("psaaction")!="" &&
						 * attributes.get("psaaction")!=null) { list.add(new
						 * BasicAttribute
						 * ("psaaction",attributes.get("psaaction"))); }
						 * if(attributes.get("psamsisdn")!="" &&
						 * attributes.get("psamsisdn")!=null) { list.add(new
						 * BasicAttribute
						 * ("psamsisdn",attributes.get("psamsisdn"))); }
						 */

						/*
						 * if(attributes.get("password")!="" &&
						 * attributes.get("password")!=null) { list.add(new
						 * BasicAttribute
						 * ("password",attributes.get("password"))); }
						 */

						if (attributes.get("mobiletype") != ""
								&& attributes.get("mobiletype") != null) {
							list.add(new BasicAttribute("mobiletype",
									attributes.get("mobiletype")));
						}
						if (attributes.get("language") != ""
								&& attributes.get("language") != null) {
							list.add(new BasicAttribute("language", attributes
									.get("language")));
						}
					}

					ModificationItem[] item = new ModificationItem[list.size()];

					for (int i = 0; i < list.size(); i++) {
						item[i] = new ModificationItem(
								DirContext.REMOVE_ATTRIBUTE, list.get(i));
					}

					try {
						ctx.modifyAttributes(
								String.format(query, uniqueIdentifier), item);

						Attributes attrs1 = ctx.getAttributes(String.format(
								query, uniqueIdentifier));
						if (attrs1.size() > 0) {
							messagingDataResponse
									.setMessage("Success...............");
						} else {
							messagingDataResponse
									.setMessage("some problem occured!!......");
						}
					} catch (Exception e) {
						e.printStackTrace();
						messagingDataResponse
								.setMessage("some problem occured!!......");
						messagingDataResponse
								.setApplicationError(new ApplicationError(e
										.getMessage()));
					}

				}

			} else {
				messagingDataResponse.setMessage("Record does not exist!!....");

			}

			messagingDataResponse.setAttributes(attributes);
			messagingDataResponse.setMessage("Success....");
			System.out.println("Delete Calling----------------");
			logger.info("##################### messagingDataResponse-----------------------"
					+ messagingDataResponse);
			logger.info("##################### Attribute-----------------------"
					+ attributes);

		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
		}
		return messagingDataResponse;
	}

	@Override
	public MessagingDataResponse modifySMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) throws UserException {
		logger.info("##################### Starts: Add SMSC Subscriber ########################");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String searchType = "";
		String uniqueIdentifier = "";
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		uniqueIdentifier = messageSearchRequest.getUniqIdentifier();

		try {
			PROVIDER_URL_SMSC = "ldap://"
					+ SessionConstantsFactory.getSessionConstant(
							"LDAP_SERVER_SMSC",
							messageSearchRequest.getSessionConstants()) + "/";
			logger.info("PROVIDER_URL_SMSC#####################---------------------"
					+ PROVIDER_URL_SMSC);

			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));

			if (ctx != null) {

				try {
					if (messageSearchRequest.getPrepaid().equalsIgnoreCase(
							"yes")) {
						logger.info("in-----prepaid--------------------------");
						query = "uniqueidentifier=%s,ou=subscribers,ou=COI_SMSC,ou=ILN_SMSC1,o=ds.t-mobile.com";
						List<Attribute> list = new ArrayList<Attribute>();
						if (messageSearchRequest.getTelePhoneNo() != ""
								&& messageSearchRequest.getTelePhoneNo() != null) {
							list.add(new BasicAttribute("telephoneNumber",
									messageSearchRequest.getTelePhoneNo()));
						}
						if (messageSearchRequest.getSmscsubprofilenumber() != ""
								&& messageSearchRequest
										.getSmscsubprofilenumber() != null) {
							list.add(new BasicAttribute("smscsubprofilenumber",
									messageSearchRequest
											.getSmscsubprofilenumber()));
						}
						if (messageSearchRequest.getSubAddress() != ""
								&& messageSearchRequest.getSubAddress() != null) {
							list.add(new BasicAttribute("smscsubaddress",
									messageSearchRequest.getSubAddress()));
						}
						if (messageSearchRequest.getUid() != ""
								&& messageSearchRequest.getUid() != null) {
							list.add(new BasicAttribute("uid",
									messageSearchRequest.getUid()));
						}
						if (messageSearchRequest.getGender() != ""
								&& messageSearchRequest.getGender() != null) {
							list.add(new BasicAttribute("gender",
									messageSearchRequest.getGender()));
						}
						if (messageSearchRequest.getSn() != ""
								&& messageSearchRequest.getSn() != null) {
							list.add(new BasicAttribute("sn",
									messageSearchRequest.getSn()));
						}
						if (messageSearchRequest.getGivenName() != ""
								&& messageSearchRequest.getGivenName() != null) {
							list.add(new BasicAttribute("givenName",
									messageSearchRequest.getGivenName()));
						}
						if (messageSearchRequest.getEmailId() != ""
								&& messageSearchRequest.getEmailId() != null) {
							list.add(new BasicAttribute("mail",
									messageSearchRequest.getEmailId()));
						}
						if (messageSearchRequest.getCn() != ""
								&& messageSearchRequest.getCn() != null) {
							list.add(new BasicAttribute("cn",
									messageSearchRequest.getCn()));
						}
						if (uniqueIdentifier != "") {
							list.add(new BasicAttribute("uniqueIdentifier",
									uniqueIdentifier));
						}
						if (messageSearchRequest.getUserAgentProfile() != ""
								&& messageSearchRequest.getUserAgentProfile() != null) {
							list.add(new BasicAttribute("useragentprofile",
									messageSearchRequest.getUserAgentProfile()));
						}
						if (messageSearchRequest.getUserAgentString() != ""
								&& messageSearchRequest.getUserAgentString() != null) {
							list.add(new BasicAttribute("useragentstring",
									messageSearchRequest.getUserAgentString()));
						}
						if (messageSearchRequest.getPrepaid() != ""
								&& messageSearchRequest.getPrepaid() != null) {
							list.add(new BasicAttribute("prepaid",
									messageSearchRequest.getPrepaid()));
						}
						// prepaid Attribute
						if (messageSearchRequest.getTplaCode() != ""
								&& messageSearchRequest.getTplaCode() != null) {
							list.add(new BasicAttribute("psatplacode",
									messageSearchRequest.getTplaCode()));
						}
						if (messageSearchRequest.getSmscsubprepaid() != ""
								&& messageSearchRequest.getSmscsubprepaid() != null) {
							list.add(new BasicAttribute("smscsubprepaid",
									messageSearchRequest.getSmscsubprepaid()));
						}
						if (messageSearchRequest.getPsa() != ""
								&& messageSearchRequest.getPsa() != null) {
							list.add(new BasicAttribute("psa",
									messageSearchRequest.getPsa()));
						}
						if (messageSearchRequest.getInpCode() != ""
								&& messageSearchRequest.getInpCode() != null) {
							list.add(new BasicAttribute("psainpcode",
									messageSearchRequest.getInpCode()));
						}
						if (messageSearchRequest.getPsaServices() != ""
								&& messageSearchRequest.getPsaServices() != null) {
							list.add(new BasicAttribute("psaservices",
									messageSearchRequest.getPsaServices()));
						}
						if (messageSearchRequest.getPsaforceflag() != ""
								&& messageSearchRequest.getPsaforceflag() != null) {
							list.add(new BasicAttribute("psaforceflag",
									messageSearchRequest.getPsaforceflag()));
						}
						if (messageSearchRequest.getPsaaction() != ""
								&& messageSearchRequest.getPsaaction() != null) {
							list.add(new BasicAttribute("psaaction",
									messageSearchRequest.getPsaaction()));
						}
						if (messageSearchRequest.getPsamsisdn() != ""
								&& messageSearchRequest.getPsamsisdn() != null) {
							list.add(new BasicAttribute("psamsisdn",
									messageSearchRequest.getPsamsisdn()));
						}

						if (messageSearchRequest.getPassword() != ""
								&& messageSearchRequest.getPassword() != null) {
							list.add(new BasicAttribute("password",
									messageSearchRequest.getPassword()));
						}

						if (messageSearchRequest.getMobileType() != ""
								&& messageSearchRequest.getMobileType() != null) {
							list.add(new BasicAttribute("mobiletype",
									messageSearchRequest.getMobileType()));
						}
						if (messageSearchRequest.getLanguage() != ""
								&& messageSearchRequest.getLanguage() != null) {
							list.add(new BasicAttribute("language",
									messageSearchRequest.getLanguage()));
						}
						list.add(new BasicAttribute("blocked", "no"));
						list.add(new BasicAttribute("smscsubaddressnumtype",
								"0"));
						list.add(new BasicAttribute("smscsubblocked", "0"));
						list.add(new BasicAttribute("smscsubsmtpfnotif", "0"));
						list.add(new BasicAttribute("smscsubsmtpfdelack", "0"));
						list.add(new BasicAttribute("smscsubsmtpfuserack", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsm", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsmbinary", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsmgsmudh", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsmtdmatsar",
								"0"));
						list.add(new BasicAttribute(
								"smscSubMsgArchivingEnabled", "0"));
						list.add(new BasicAttribute("smscSubMsgArchivingOrg",
								"0"));
						list.add(new BasicAttribute("smscSubMsgArchivingRcp",
								"0"));

						ModificationItem[] item = new ModificationItem[list
								.size()];

						for (int i = 0; i < list.size(); i++) {
							item[i] = new ModificationItem(
									DirContext.REPLACE_ATTRIBUTE, list.get(i));
						}

						try {
							ctx.modifyAttributes(
									String.format(query, uniqueIdentifier),
									item);

							Attributes attrs1 = ctx.getAttributes(String
									.format(query, uniqueIdentifier));
							if (attrs1.size() > 0) {
								messagingDataResponse
										.setMessage("Success...............");
							} else {
								messagingDataResponse
										.setMessage("some problem occured!!......");
							}
						} catch (Exception e) {
							e.printStackTrace();
							messagingDataResponse
									.setMessage("some problem occured!!......");
							messagingDataResponse
									.setApplicationError(new ApplicationError(e
											.getMessage()));
						}

					} else {

						query = "uniqueidentifier=%s,ou=subscribers,ou=COI_SMSC,ou=ILN_SMSC1,o=ds.t-mobile.com";
						List<Attribute> list = new ArrayList<Attribute>();
						if (messageSearchRequest.getTelePhoneNo() != ""
								&& messageSearchRequest.getTelePhoneNo() != null) {
							list.add(new BasicAttribute("telephoneNumber",
									messageSearchRequest.getTelePhoneNo()));
						}
						if (messageSearchRequest.getSmscsubprofilenumber() != ""
								&& messageSearchRequest
										.getSmscsubprofilenumber() != null) {
							list.add(new BasicAttribute("smscsubprofilenumber",
									messageSearchRequest
											.getSmscsubprofilenumber()));
						}
						if (messageSearchRequest.getSubAddress() != ""
								&& messageSearchRequest.getSubAddress() != null) {
							list.add(new BasicAttribute("smscsubaddress",
									messageSearchRequest.getSubAddress()));
						}
						if (messageSearchRequest.getUid() != ""
								&& messageSearchRequest.getUid() != null) {
							list.add(new BasicAttribute("uid",
									messageSearchRequest.getUid()));
						}
						if (messageSearchRequest.getGender() != ""
								&& messageSearchRequest.getGender() != null) {
							list.add(new BasicAttribute("gender",
									messageSearchRequest.getGender()));
						}
						if (messageSearchRequest.getSn() != ""
								&& messageSearchRequest.getSn() != null) {
							list.add(new BasicAttribute("sn",
									messageSearchRequest.getSn()));
						}
						if (messageSearchRequest.getGivenName() != ""
								&& messageSearchRequest.getGivenName() != null) {
							list.add(new BasicAttribute("givenName",
									messageSearchRequest.getGivenName()));
						}
						if (messageSearchRequest.getEmailId() != ""
								&& messageSearchRequest.getEmailId() != null) {
							list.add(new BasicAttribute("mail",
									messageSearchRequest.getEmailId()));
						}
						if (messageSearchRequest.getCn() != ""
								&& messageSearchRequest.getCn() != null) {
							list.add(new BasicAttribute("cn",
									messageSearchRequest.getCn()));
						}
						if (uniqueIdentifier != "") {
							list.add(new BasicAttribute("uniqueIdentifier",
									uniqueIdentifier));
						}
						if (messageSearchRequest.getUserAgentProfile() != ""
								&& messageSearchRequest.getUserAgentProfile() != null) {
							list.add(new BasicAttribute("useragentprofile",
									messageSearchRequest.getUserAgentProfile()));
						}
						if (messageSearchRequest.getUserAgentString() != ""
								&& messageSearchRequest.getUserAgentString() != null) {
							list.add(new BasicAttribute("useragentstring",
									messageSearchRequest.getUserAgentString()));
						}
						if (messageSearchRequest.getPrepaid() != ""
								&& messageSearchRequest.getPrepaid() != null) {
							list.add(new BasicAttribute("prepaid",
									messageSearchRequest.getPrepaid()));
						}

						if (messageSearchRequest.getPassword() != ""
								&& messageSearchRequest.getPassword() != null) {
							list.add(new BasicAttribute("password",
									messageSearchRequest.getPassword()));
						}

						if (messageSearchRequest.getMobileType() != ""
								&& messageSearchRequest.getMobileType() != null) {
							list.add(new BasicAttribute("mobiletype",
									messageSearchRequest.getMobileType()));
						}
						if (messageSearchRequest.getLanguage() != ""
								&& messageSearchRequest.getLanguage() != null) {
							list.add(new BasicAttribute("language",
									messageSearchRequest.getLanguage()));
						}
						list.add(new BasicAttribute("smscsubaddressnumtype",
								"0"));
						list.add(new BasicAttribute("smscsubblocked", "0"));
						list.add(new BasicAttribute("smscsubsmtpfnotif", "0"));
						list.add(new BasicAttribute("smscsubsmtpfdelack", "0"));
						list.add(new BasicAttribute("smscsubsmtpfuserack", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsm", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsmbinary", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsmgsmudh", "0"));
						list.add(new BasicAttribute("smscsubsmtpfsmtdmatsar",
								"0"));
						list.add(new BasicAttribute(
								"smscSubMsgArchivingEnabled", "0"));
						list.add(new BasicAttribute("smscSubMsgArchivingOrg",
								"0"));
						list.add(new BasicAttribute("smscSubMsgArchivingRcp",
								"0"));

						ModificationItem[] item = new ModificationItem[list
								.size()];
						for (int i = 0; i < list.size(); i++) {
							item[i] = new ModificationItem(
									DirContext.REPLACE_ATTRIBUTE, list.get(i));
						}
						ctx.modifyAttributes(
								String.format(query, uniqueIdentifier), item);

						Attributes attrs1 = ctx.getAttributes(String.format(
								query, uniqueIdentifier));
						logger.info("#####################------------------------ Attribute-----------------------"
								+ attrs1);
						if (attrs1.size() > 0) {
							messagingDataResponse
									.setMessage("Success...............");
						} else {
							messagingDataResponse
									.setMessage("some problem occured!!......");
						}

					}

				} catch (NamingException e) {
					messagingDataResponse
							.setApplicationError(new ApplicationError(e
									.getMessage()));
					e.printStackTrace();
				} finally {
					try {
						ctx.close();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (UserException e1) {
			throw new UserException(e1.getMessage());
		}
		logger.info("##################### Ends: Add TAC ########################");

		return messagingDataResponse;
	}

	@Override
	public Map<String, Object> deleteMmscSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant("SERVER_MMSC",
							messagingSearchRequest.getSessionConstants())
					+ "/SPSSubscriberProvisioningService/SPSSubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestForMmscDelete(messagingSearchRequest
							.getMsisdn()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			logger.info("In Delete MMSC result==================================="
					+ result);

			dataMap = (Map) convertToJavaObjectForMMSCDelete(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public List<ClientIDResponse> getClientIds() {

		final List<ClientIDResponse> tgr = new ArrayList<ClientIDResponse>();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		String query = "select * from iam_clients";
		List<ClientIDResponse> tgr1 = jdbcTemplate.query(query,
				new RowMapper<ClientIDResponse>() {
					@Override
					public ClientIDResponse mapRow(ResultSet rs, int rownumber)
							throws SQLException {

						ClientIDResponse generatorResponse = new ClientIDResponse();
						generatorResponse.setClientId(rs.getString(1));
						generatorResponse.setSecretKey(rs.getString(2));
						tgr.add(generatorResponse);
						return generatorResponse;
					}
				});
		return tgr;
	}

}

