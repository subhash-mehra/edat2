package com.tmobile.edat.messaging.repository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.LdapName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.messaging.dto.request.MSISDNProfileRequest;
import com.tmobile.edat.messaging.dto.request.MessagingSearchRequest;
import com.tmobile.edat.messaging.dto.response.AccessTokenResponse;
import com.tmobile.edat.messaging.dto.response.ClientIDResponse;
import com.tmobile.edat.messaging.dto.response.MessagingDataResponse;
import com.tmobile.edat.messaging.dto.response.UnLockResponse;
import com.tmobile.edat.usd.dto.response.ImeiTacDataResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.LDAPConnectionManager;
import com.tmobile.edat.util.LDAPInfo;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;
import com.tmobile.session.SessionConstantsFactory;

public class MessagingDataHandler implements MessagingDataRepository {
	private static final Logger logger = Logger
			.getLogger(MessagingDataRepositoryImpl.class);
	private static String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static String SECURITY_AUTHENTICATION = "simple";
	private static String SECURITY_PRINCIPAL_IAM = "cn=Directory Manager";
	private static String SECURITY_CREDENTIALS_IAM = "oracle123";
	private static String PROVIDER_URL_IAM = "ldap://"
			+ Constants.LDAP_SERVER_IAM + "/";
	String SEARCH_QUERY_DEFAULT = "ou=subscribers,c=us,dc=t-mobile,dc=com";
	String SEARCH_FILTER_DEFAULT = "(%s= %s)";
	private static String SECURITY_PRINCIPAL_SMSC = "cn=directory manager,o=ds.t-mobile.com";
	private static String SECURITY_CREDENTIALS_SMSC = "admin123";
	private static String PROVIDER_URL_SMSC = "ldap://"
			+ Constants.LDAP_SERVER_SMSC + "/"; // "ldap://10.168.208.206:389/";
												// 10.168.208.206
	String SEARCH_QUERY_SMSC_SEARCH = "o=ds.t-mobile.com,telephonenumber=%s";
	String SEARCH_QUERY_SMSC = "o=ds.t-mobile.com";
	String SEARCH_FILTER_SMSC = "(telephonenumber=%s)";

	// String SEARCH_FILTER_DEFAULT = "(tmIMSI= 310260778885001)";
	// @Override
	public MessagingDataResponse doIAMSearch(int search_type, String key,
			SessionConstants sessionConstants) {

		MessagingDataResponse dataResponse = new MessagingDataResponse();
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		Map<String, String> attributes = new HashMap<String, String>();
		String filter = "";
		System.out
				.println("##################### IAM ########################");
		switch (search_type) {
		case Constants.SEARCH_TYPE_MSISDN:
			System.out.println("providerURL :" + PROVIDER_URL_IAM);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			filter = String.format(SEARCH_FILTER_DEFAULT, "mobile", key);
			break;
		case Constants.SEARCH_TYPE_IMSI:
			System.out.println("providerURL :" + PROVIDER_URL_IAM);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			filter = String.format(SEARCH_FILTER_DEFAULT, "tmIMSI", key);
			break;
		case Constants.SEARCH_TYPE_CUSTID:
			System.out.println("providerURL :" + PROVIDER_URL_IAM);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			filter = String.format(SEARCH_FILTER_DEFAULT, "userid", key);
			break;
		case Constants.SEARCH_TYPE_EMAIL:
			System.out.println("providerURL :" + PROVIDER_URL_IAM);
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			filter = String.format(SEARCH_FILTER_DEFAULT, "IAMEmail", key);
			break;

		default:
			break;
		}
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_IAM, SECURITY_CREDENTIALS_IAM,
					PROVIDER_URL_IAM));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {

				System.out.println("IAM search query :" + SEARCH_QUERY_DEFAULT
						+ "\nfilter :" + filter);
				NamingEnumeration<SearchResult> result = ctx.search(
						SEARCH_QUERY_DEFAULT, filter, sc);
				System.out.println("IAM search result :"
						+ result.hasMoreElements());
				System.out.println("----------------------------------------");
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					LdapName dn = new LdapName(rs.getNameInNamespace());
					System.out.println("DN has " + dn.size() + " RDNs: ");
					for (int i = 0; i < dn.size(); i++) {
						// System.out.println("RDNs : " + dn.get(i));
						if (dn.get(i).contains("userid")) {
							String userid = dn.get(i).toString();
							attributes.put("userid", userid.substring(
									userid.indexOf("=") + 1, userid.length()));
						}
					}
					Attributes attrs = rs.getAttributes();
					NamingEnumeration<String> atyrs = attrs.getIDs();

					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							attributes.put(id, attrs.get(id).get().toString());
						} catch (Exception e) {
							System.err.println("error : " + e.toString());
						}

					}

					System.out.println(attributes.toString());
					System.out
							.println("----------------------------------------");
				}

			} catch (NamingException e) {

				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		dataResponse.setAttributes(attributes);
		return dataResponse;
	}

	// @Override
	public MessagingDataResponse doSMSCSearch(String msisdn,
			SessionConstants sessionConstants) {
		MessagingDataResponse dataResponse = new MessagingDataResponse();
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		Map<String, String> attributes = new HashMap<String, String>();
		String filter = "";
		System.out
				.println("##################### SMSC ########################");

		System.out.println("providerURL :" + PROVIDER_URL_SMSC);
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		filter = String.format(SEARCH_FILTER_SMSC, msisdn); // 12064465370

		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {

				System.out.println("SMSC search query :" + SEARCH_QUERY_SMSC
						+ "\nfilter :" + filter);
				NamingEnumeration<SearchResult> result = ctx.search(
						SEARCH_QUERY_SMSC, filter, sc);
				System.out.println("SMSC search result :"
						+ result.hasMoreElements());
				System.out.println("----------------------------------------");
				while (result != null && result.hasMoreElements()) {
					// FIXME read the result and return the response
					SearchResult rs = result.next();
					Attributes attrs = rs.getAttributes();
					NamingEnumeration<String> atyrs = attrs.getIDs();

					while (atyrs != null && atyrs.hasMoreElements()) {
						try {
							String id = atyrs.next().toString();
							attributes.put(id, attrs.get(id).get().toString());

							// System.out.println("ATTR : " + id
							// +" : "+attrs.get(id));
						} catch (Exception e) {
							System.err.println("error : " + e.toString());
						}

					}

					System.out.println(attributes.toString());
					System.out
							.println("----------------------------------------");
				}

			} catch (NamingException e) {

				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		dataResponse.setAttributes(attributes);
		return dataResponse;
	}

	// @Override
	public MessagingDataResponse doMMSCSearch(String msisdn,
			SessionConstants sessionConstants) {
		MessagingDataResponse dataResponse = new MessagingDataResponse();
		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ Constants.SERVER_MMSC
					+ "/SPSSubscriberProvisioningService/SPSSubscriberProvisioningServiceImpl";
			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequest(msisdn), url);

			// TODO Process the SOAP Response
			printSOAPResponse(soapResponse);

			soapConnection.close();
		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return null;
	}

	private SOAPMessage createSOAPRequest(String msisdn) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://www.mavenir.com/sps/services/mmsc/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("GetSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("userInfo");
		soapBodyUserInfo.addChildElement("userId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("msisdn").addTextNode(msisdn);
		subscriber.addChildElement("sourceSystem").addTextNode("SwitchControl");

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.print("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}

	@Override
	public Map<String, Object> addSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ SessionConstantsFactory.getSessionConstant(
							"SMSC_API_URL",
							messagingSearchRequest.getSessionConstants())
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestAddSubscriber(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getImsi(),
							messagingSearchRequest.getSubStatus(),
							messagingSearchRequest.getLanguage(),
							messagingSearchRequest.getSubType(),
							messagingSearchRequest.getOperatorId()), url);

			// TODO Process the SOAP Response
			// printSOAPResponse(soapResponse);
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			try {
				System.out.println();
			} catch (Exception e) {

			}
			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> modifySubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ Constants.SMSC_API_URL
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestModifySubscriber(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getSubType()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> deleteSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ Constants.SMSC_API_URL
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestForDelete(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> deleteSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ Constants.SMSC_API_URL
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequestForDelete(
							messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	@Override
	public Map<String, Object> getSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ Constants.SMSC_API_URL
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequest(messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response
			String result = soapMessageToString(soapResponse);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);
			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}
			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	private SOAPMessage createSOAPRequest(String msisdn, String service)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("GetSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("MSISDN").addTextNode(msisdn);
		if ("SMSC".equalsIgnoreCase(service)) {
			subscriber.addChildElement("ServiceIndicator").addTextNode(service);
		}
		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	@Override
	public Map<String, Object> getSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = "http://"
					+ Constants.SMSC_API_URL
					+ "/SubscriberProvisioningService/SubscriberProvisioningServiceImpl";

			SOAPMessage soapResponse = soapConnection.call(
					createSOAPRequest(messagingSearchRequest.getMsisdn(),
							messagingSearchRequest.getService()), url);
			// TODO Process the SOAP Response

			String result = soapMessageToString(soapResponse);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);

			try {
				if (dataMap.get("status") != null) {
					status = (String) dataMap.get("status");
				}

				System.out.println();
			} catch (Exception e) {

			}

			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	private Map<String, String> convertToJavaObjectForGetSubscriber(
			String xmlString) {
		logger.info("Enter In conver sion String to Map");
		Map<String, String> attributes = new HashMap<String, String>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlString));

			Document doc = null;
			doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("SubscriberDetails");
			NodeList subscriberDataNode = doc
					.getElementsByTagName("SubscriberData");
			// iterate the Subscriber data
			logger.info("Nodes length:= " + nodes.getLength());
			if (nodes.getLength() > 0) {
				for (int i = 0; i < nodes.getLength(); i++) {

					NodeList innerList = nodes.item(i).getChildNodes();

					for (int j = 0; j < innerList.getLength(); j++) {
						if (innerList.item(j).getNodeName()
								.equalsIgnoreCase("MSISDN")) {
							attributes.put(innerList.item(j).getNodeName(),
									innerList.item(j).getTextContent());
						}

						logger.info("Xml Data In MAp Name:"
								+ innerList.item(j).getNodeName()
								+ "::::Value:::"
								+ innerList.item(j).getTextContent());
					}
				}

				for (int i = 0; i < subscriberDataNode.getLength(); i++) {
					NodeList innerList = subscriberDataNode.item(i)
							.getChildNodes();

					for (int j = 0; j < innerList.getLength(); j++) {

						attributes.put(innerList.item(j).getNodeName(),
								innerList.item(j).getTextContent());
						logger.info("Xml Data In MAp Name:"
								+ innerList.item(j).getNodeName()
								+ "::::Value:::"
								+ innerList.item(j).getTextContent());
					}
				}
			} else {
				NodeList faultstringNode = doc
						.getElementsByTagName("faultstring");

				if (faultstringNode.getLength() > 0) {
					for (int i = 0; i < faultstringNode.getLength(); i++) {
						attributes.put(faultstringNode.item(i).getNodeName(),
								faultstringNode.item(i).getTextContent());

						NodeList validationErrorNode = doc
								.getElementsByTagName("ns3:ValidationError");
						NodeList innerList = validationErrorNode.item(i)
								.getChildNodes();
						for (int j = 0; j < innerList.getLength(); j++) {
							if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns3:Code")) {
								attributes.put("Code", innerList.item(j)
										.getTextContent());
							} else if (innerList.item(j).getNodeName()
									.equalsIgnoreCase("ns3:Reason")) {
								attributes.put("Reason", innerList.item(j)
										.getTextContent());
							} else {
								attributes.put(innerList.item(j).getNodeName(),
										innerList.item(j).getTextContent());
							}

							logger.info("Xml Data In MAp Name:"
									+ innerList.item(j).getNodeName()
									+ "::::Value:::"
									+ innerList.item(j).getTextContent());
						}
					}
				} else {
					attributes.put("success", "success");
				}
			}
			System.out.println("attributes-----" + attributes);

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			logger.error("ParserConfigurationException In parsing the Xml :: "
					+ e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			logger.error("SAXException In  the Xml :: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IOException In  Xml :: " + e.getMessage());
			e.printStackTrace();
		}
		return attributes;
	}

	public String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		return result;
	}

	private SOAPMessage createSOAPRequestAddSubscriber(String msisdn,
			String imsi, String subStatus, String language, String subType,
			String operatorId) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("AddSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");

		SOAPElement subscriberDetails = subscriber
				.addChildElement("SubscriberDetails");
		subscriberDetails.addChildElement("MSISDN").addTextNode(msisdn);

		SOAPElement subscriberData = subscriberDetails
				.addChildElement("SubscriberData");
		subscriberData.addChildElement("IMSI").addTextNode(imsi);
		subscriberData.addChildElement("SubStatus").addTextNode(subStatus);
		subscriberData.addChildElement("Language").addTextNode(language);
		subscriberData.addChildElement("SubType").addTextNode(subType);
		subscriberData.addChildElement("OperatorId").addTextNode(operatorId);

		SOAPElement subscriberSMSCData = subscriberDetails
				.addChildElement("SubscriberSMSCData");
		subscriberSMSCData.addChildElement("IMSEnabled").addTextNode("1");
		subscriberSMSCData.addChildElement("SMSCSubProfileID").addTextNode("1");

		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	private SOAPMessage createSOAPRequestModifySubscriber(String msisdn,
			String subType) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("ModifySubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");

		SOAPElement subscriberDetails = subscriber
				.addChildElement("SubscriberDetails");
		subscriberDetails.addChildElement("MSISDN").addTextNode(msisdn);

		SOAPElement subscriberData = subscriberDetails
				.addChildElement("SubscriberData");
		subscriberData.addChildElement("SubType").addTextNode(subType);

		SOAPElement subscriberSMSCData = subscriberDetails
				.addChildElement("SubscriberSMSCData");
		subscriberSMSCData.addChildElement("IMSEnabled").addTextNode("0");

		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	private SOAPMessage createSOAPRequestForDelete(String msisdn, String service)
			throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("DeleteSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");
		subscriber.addChildElement("MSISDN").addTextNode(msisdn);
		if ("SMSC".equalsIgnoreCase(service)) {
			subscriber.addChildElement("ServiceIndicator").addTextNode(service);
		}
		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();

		return soapMessage;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// AIM its LDAP based search can be done with either any on one
		// MSISDN/IMSI/USER_ID/EMAIL
		MessagingDataHandler handler = new MessagingDataHandler();
		// handler.doIAMSearch(Constants.SEARCH_TYPE_IMSI, "310260778885001");
		// handler.doIAMSearch(Constants.SEARCH_TYPE_MSISDN, "7778885001");
		// handler.doIAMSearch(Constants.SEARCH_TYPE_CUSTID, "U-mm65X2Ly6b0");
		// handler.doIAMSearch(Constants.SEARCH_TYPE_EMAIL,"abiabhi@saimail.com");

		// TODO MMSC its SOAP based search can be done with MSISDN :
		// TODO This can be run once u deploy the code on server
		// handler.doMMSCSearch("2064465370"); // 2064465370 - 4253196810

		// SMSC its LDAP based on MSISDN (this can be tested after deployment )
		// handler.doSMSCSearch("12064465370"); // EX MSISDN : 12064465370 -
		// 14254444604
	}

	@Override
	public Map<String, Object> putSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		String status = "";
		String responseXML = "";

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
					.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory
					.createConnection();

			// Send SOAP Message to SOAP Server
			String url = Constants.SMSC_API_URL;

			SOAPMessage soapResponse = soapConnection
					.call(createSOAPRequestPutSubscriber(messagingSearchRequest),
							url);

			// TODO Process the SOAP Response
			// printSOAPResponse(soapResponse);
			String result = soapMessageToString(soapResponse);
			System.out.println("result=============" + result);
			dataMap = (Map) convertToJavaObjectForGetSubscriber(result);
			try {
				System.out.println();
			} catch (Exception e) {

			}
			soapConnection.close();

		} catch (Exception e) {
			System.err
					.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		return dataMap;

	}

	private SOAPMessage createSOAPRequestPutSubscriber(
			MessagingSearchRequest messagingSearchRequest) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		String messageAddress = messagingSearchRequest.getMsisdn()
				+ "/TYPE=PLMN@tmo-dev.msg.lab.t-mobile.com";
		String npa = "";
		try {
			npa = messagingSearchRequest.getMsisdn().substring(1, 4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
		headers.addHeader("Accept-Encoding", "gzip,deflate");

		String serverURI = "http://www.mavenir.com/sps/services/mmsc/subscriber";
		String serverURI2 = "http://www.mavenir.com/sps/services/shareddatatypes";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("sub", serverURI);
		envelope.addNamespaceDeclaration("shar", serverURI2);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement subscriber = soapBody.addChildElement("PutSubscriber",
				"sub");

		SOAPElement soapBodyUserInfo = subscriber.addChildElement("UserInfo");
		soapBodyUserInfo.addChildElement("UserId", "shar").addTextNode(
				"spsuser");
		soapBodyUserInfo.addChildElement("Password", "shar").addTextNode(
				"spsuser");

		SOAPElement subscriberData = subscriber
				.addChildElement("SubscriberData");
		subscriberData.addChildElement("MSISDN").addTextNode(
				messagingSearchRequest.getMsisdn());
		subscriberData.addChildElement("SubStatus").addTextNode(
				messagingSearchRequest.getSubStatus());
		subscriberData.addChildElement("COS").addTextNode(
				messagingSearchRequest.getCos());
		subscriberData.addChildElement("SubType").addTextNode(
				messagingSearchRequest.getSubType());
		subscriberData.addChildElement("HandsetType").addTextNode(
				messagingSearchRequest.getHandsetType());
		subscriberData.addChildElement("MMSCapable").addTextNode(
				messagingSearchRequest.getMmsCapable());
		subscriberData.addChildElement("MmsAddress")
				.addTextNode(messageAddress);
		;
		subscriberData.addChildElement("NPA").addTextNode(npa);
		subscriberData.addChildElement("OperatorId").addTextNode(
				messagingSearchRequest.getOperatorId());
		subscriberData.addChildElement("SMTPGWDomain").addTextNode(
				"tmo-dev.msg.lab.t-mobile.com");

		subscriber.addChildElement("SourceSystem").addTextNode("SC");

		soapMessage.saveChanges();

		/* Print the request message */
		System.out.print("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}

	@Override
	public MessagingDataResponse addSMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) {
		logger.info("##################### Starts: Add SMSC Subscriber ########################");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String searchType = "";
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		query = String.format(SEARCH_QUERY_SMSC_SEARCH,
				messageSearchRequest.getTelePhoneNo());
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {
				if (messageSearchRequest.getPrepaid().equalsIgnoreCase("yes")) {

					Attribute attribute1 = new BasicAttribute(
							"telephoneNumber",
							messageSearchRequest.getTelePhoneNo());
					Attribute attribute2 = new BasicAttribute(
							"uniqueIdentifier",
							messageSearchRequest.getUniqIdentifier());
					Attribute attribute3 = new BasicAttribute("sn",
							messageSearchRequest.getSn());
					Attribute attribute4 = new BasicAttribute("givenName",
							messageSearchRequest.getGivenName());
					Attribute attribute5 = new BasicAttribute("mail",
							messageSearchRequest.getEmailId());
					Attribute attribute6 = new BasicAttribute("cn",
							messageSearchRequest.getCn());
					Attribute attribute7 = new BasicAttribute("gender",
							messageSearchRequest.getGender());
					Attribute attribute8 = new BasicAttribute("uid",
							messageSearchRequest.getUid());
					Attribute attribute9 = new BasicAttribute("password",
							messageSearchRequest.getPassword());
					Attribute attribute10 = new BasicAttribute("mobiletype",
							messageSearchRequest.getMobileType());
					Attribute attribute11 = new BasicAttribute("language",
							messageSearchRequest.getLanguage());
					Attribute attribute12 = new BasicAttribute("blocked", "no");
					Attribute attribute13 = new BasicAttribute(
							"smscsubaddressnumtype",
							messageSearchRequest.getSubAddNumType());
					Attribute attribute14 = new BasicAttribute(
							"smscsubaddress",
							messageSearchRequest.getSubAddress());
					Attribute attribute15 = new BasicAttribute("objectClass",
							"psasubscriber");
					Attribute attribute16 = new BasicAttribute(
							"useragentprofile", "Control");
					Attribute attribute17 = new BasicAttribute("prepaid", "yes");
					Attribute attribute18 = new BasicAttribute("psatplacode",
							messageSearchRequest.getTplaCode());
					Attribute attribute19 = new BasicAttribute(
							"smscsubprepaid", "1");
					Attribute attribute20 = new BasicAttribute("psaservices",
							messageSearchRequest.getPsaServices());
					Attribute attribute21 = new BasicAttribute("psaforceflag",
							"Y");
					Attribute attribute22 = new BasicAttribute("psa",
							messageSearchRequest.getPsa());
					Attribute attribute23 = new BasicAttribute("psainpcode",
							messageSearchRequest.getInpCode());
					Attribute attribute24 = new BasicAttribute("psaaction",
							"RESUME");
					Attribute attribute25 = new BasicAttribute(
							"smscsubprofilenumber",
							messageSearchRequest.getSmscsubprofilenumber());

					ModificationItem[] item = new ModificationItem[25];

					item[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute1);
					item[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute2);
					item[2] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute3);
					item[3] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute4);
					item[4] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute5);
					item[5] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute6);
					item[6] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute7);
					item[7] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute8);
					item[8] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute9);
					item[9] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute10);
					item[10] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute11);
					item[11] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute12);
					item[12] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute13);
					item[13] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute14);
					item[14] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute15);
					item[15] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute16);
					item[16] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute17);
					item[17] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute18);
					item[18] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute19);
					item[19] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute20);
					item[20] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute21);
					item[21] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute22);
					item[22] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute23);
					item[23] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute24);
					item[24] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute25);

					logger.info("query:::::::: " + attribute1);

					ctx.modifyAttributes(query, item);
					System.out.println("======================================"
							+ query);
				} else {
					Attribute attribute1 = new BasicAttribute(
							"telephoneNumber",
							messageSearchRequest.getTelePhoneNo());//
					Attribute attribute2 = new BasicAttribute(
							"uniqueIdentifier",
							messageSearchRequest.getUniqIdentifier());//
					Attribute attribute3 = new BasicAttribute("sn",
							messageSearchRequest.getSn());//
					Attribute attribute4 = new BasicAttribute("givenName",
							messageSearchRequest.getGivenName());//
					Attribute attribute5 = new BasicAttribute("mail",
							messageSearchRequest.getEmailId());//
					Attribute attribute6 = new BasicAttribute("cn",
							messageSearchRequest.getCn());//
					Attribute attribute7 = new BasicAttribute("gender",
							messageSearchRequest.getGender());//
					Attribute attribute8 = new BasicAttribute("uid",
							messageSearchRequest.getUid());//
					Attribute attribute9 = new BasicAttribute("password",
							messageSearchRequest.getPassword());//
					Attribute attribute10 = new BasicAttribute("mobiletype",
							messageSearchRequest.getMobileType());//
					Attribute attribute11 = new BasicAttribute("language",
							messageSearchRequest.getLanguage());//
					Attribute attribute12 = new BasicAttribute("blocked", "no");//
					Attribute attribute13 = new BasicAttribute(
							"smscsubaddressnumtype",
							messageSearchRequest.getSubAddNumType());//
					Attribute attribute14 = new BasicAttribute(
							"smscsubaddress",
							messageSearchRequest.getSubAddress());//
					Attribute attribute15 = new BasicAttribute("objectClass",
							"subscriber");//
					Attribute attribute16 = new BasicAttribute(
							"useragentprofile", "Postpaid");//
					Attribute attribute17 = new BasicAttribute(
							"useragentstring",
							messageSearchRequest.getUserAgentString());
					Attribute attribute18 = new BasicAttribute(
							"smscsubprofilenumber",
							messageSearchRequest.getSmscsubprofilenumber());//

					ModificationItem[] item = new ModificationItem[25];

					item[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute1);
					item[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute2);
					item[2] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute3);
					item[3] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute4);
					item[4] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute5);
					item[5] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute6);
					item[6] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute7);
					item[7] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute8);
					item[8] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute9);
					item[9] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute10);
					item[10] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute11);
					item[11] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute12);
					item[12] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute13);
					item[13] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute14);
					item[14] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute15);
					item[15] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute16);
					item[16] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute17);
					item[18] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
							attribute18);

					logger.info("query:::::::: " + attribute1);

					ctx.modifyAttributes(query, item);
					System.out.println("======================================"
							+ query);

				}

			} catch (NamingException e) {
				imeiTacDataResponse.setApplicationError(new ApplicationError(e
						.getMessage()));
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		logger.info("##################### Ends: Add TAC ########################");

		return messagingDataResponse;
	}

	public Map<String, Object> addSMSCSubscriber() {
		logger.info("##################### Starts: Add SMSC Subscriber ########################");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String searchType = "";

		query = String.format(SEARCH_QUERY_SMSC_SEARCH, "11029204294");
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {

				Attribute attribute1 = new BasicAttribute("telephoneNumber",
						"11029204294");
				Attribute attribute2 = new BasicAttribute("uniqueIdentifier",
						"071014125658053bef00a0003");
				Attribute attribute3 = new BasicAttribute("sn", "IDT");
				Attribute attribute4 = new BasicAttribute("givenName",
						"Regular");
				Attribute attribute5 = new BasicAttribute("mail",
						" 12064465370@tmomail.net");
				Attribute attribute6 = new BasicAttribute("cn", "Cintex");
				Attribute attribute7 = new BasicAttribute("gender", "Limited");
				Attribute attribute8 = new BasicAttribute("uid", "12064465370");
				Attribute attribute9 = new BasicAttribute("password",
						" *CHA*3Z191R240A0L472u13663Q2m160D1S2c142D5B2Q0I3o3S2f0B737X662X4X0T2S");
				Attribute attribute10 = new BasicAttribute("mobiletype",
						" Nokia 5180i");
				Attribute attribute11 = new BasicAttribute("language", "en");
				Attribute attribute12 = new BasicAttribute("blocked", "no");
				Attribute attribute13 = new BasicAttribute(
						"smscsubaddressnumtype", "0");
				Attribute attribute14 = new BasicAttribute("smscsubaddress",
						"01112064465370");
				Attribute attribute15 = new BasicAttribute("objectClass",
						"psasubscriber");
				Attribute attribute16 = new BasicAttribute("useragentprofile",
						"Control");
				Attribute attribute17 = new BasicAttribute("prepaid", "yes");
				Attribute attribute18 = new BasicAttribute("psatplacode",
						"TPLA1 ");
				Attribute attribute19 = new BasicAttribute("smscsubprepaid",
						"1");
				Attribute attribute20 = new BasicAttribute("psaservices",
						"SMS ");
				Attribute attribute21 = new BasicAttribute("psaforceflag", "Y");
				Attribute attribute22 = new BasicAttribute("psa", " PSA1");
				Attribute attribute23 = new BasicAttribute("psainpcode", "TMO");
				Attribute attribute24 = new BasicAttribute("psaaction",
						"RESUME");
				Attribute attribute25 = new BasicAttribute(
						"smscsubprofilenumber", "17");

				ModificationItem[] item = new ModificationItem[25];

				item[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute1);
				item[1] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute2);
				item[2] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute3);
				item[3] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute4);
				item[4] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute5);
				item[5] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute6);
				item[6] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute7);
				item[7] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute8);
				item[8] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute9);
				item[9] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute10);
				item[10] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute11);
				item[11] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute12);
				item[12] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute13);
				item[13] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute14);
				item[14] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute15);
				item[15] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute16);
				item[16] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute17);
				item[17] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute18);
				item[18] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute19);
				item[19] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute20);
				item[20] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute21);
				item[21] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute22);
				item[22] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute23);
				item[23] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute24);
				item[24] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
						attribute25);

				logger.info("query:::::::: " + attribute1);

				ctx.modifyAttributes(query, item);
				System.out.println("======================================"
						+ query);

			} catch (NamingException e) {
				imeiTacDataResponse.setApplicationError(new ApplicationError(e
						.getMessage()));
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		logger.info("##################### Ends: Add TAC ########################");

		return dataMap;
	}

	@Override
	public MessagingDataResponse deleteSMSC(
			MessagingSearchRequest messageSearchRequest) {
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		Map<String, String> attributes = new HashMap<String, String>();
		attributes = (Map) doSMSCSearch(messageSearchRequest.getMsisdn(),
				messageSearchRequest.getSessionConstants());
		messagingDataResponse.setAttributes(attributes);
		messagingDataResponse.setMessage("Success....");
		System.out.println("Delete Calling----------------");
		logger.info("##################### messagingDataResponse-----------------------"
				+ messagingDataResponse);
		logger.info("##################### Attribute-----------------------"
				+ attributes);
		return messagingDataResponse;
	}

	@Override
	public MessagingDataResponse modifySMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) {
		logger.info("##################### Starts: Add SMSC Subscriber ########################");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		ImeiTacDataResponse imeiTacDataResponse = new ImeiTacDataResponse();
		String providerURL = "";
		LdapContext ctx = null;
		SearchControls sc = new SearchControls();
		String query = "";
		String filter = "";
		String searchType = "";
		MessagingDataResponse messagingDataResponse = new MessagingDataResponse();
		query = String.format(SEARCH_QUERY_SMSC_SEARCH,
				messageSearchRequest.getTelePhoneNo());
		PROVIDER_URL_SMSC = "ldap://"
				+ SessionConstantsFactory.getSessionConstant(
						"LDAP_SERVER_SMSC",
						messageSearchRequest.getSessionConstants()) + "/";
		try {
			ctx = LDAPConnectionManager.getLdapContext(new LDAPInfo(
					LDAP_CONTEXT_FACTORY, SECURITY_AUTHENTICATION,
					SECURITY_PRINCIPAL_SMSC, SECURITY_CREDENTIALS_SMSC,
					PROVIDER_URL_SMSC));
		} catch (UserException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (ctx != null) {
			try {
				if (messageSearchRequest.getPrepaid().equalsIgnoreCase("yes")) {
					List<Attribute> list = new ArrayList<Attribute>();
					list.add(new BasicAttribute("telephoneNumber",
							messageSearchRequest.getTelePhoneNo()));
					list.add(new BasicAttribute("uniqueIdentifier",
							messageSearchRequest.getUniqIdentifier()));
					list.add(new BasicAttribute("sn", messageSearchRequest
							.getSn()));
					list.add(new BasicAttribute("givenName",
							messageSearchRequest.getGivenName()));
					list.add(new BasicAttribute("mail", messageSearchRequest
							.getEmailId()));
					list.add(new BasicAttribute("cn", messageSearchRequest
							.getCn()));
					list.add(new BasicAttribute("gender", messageSearchRequest
							.getGender()));
					list.add(new BasicAttribute("uid", messageSearchRequest
							.getUid()));
					list.add(new BasicAttribute("password",
							messageSearchRequest.getPassword()));
					list.add(new BasicAttribute("mobiletype",
							messageSearchRequest.getMobileType()));
					list.add(new BasicAttribute("language",
							messageSearchRequest.getLanguage()));
					list.add(new BasicAttribute("blocked", "no"));
					list.add(new BasicAttribute("smscsubaddressnumtype",
							messageSearchRequest.getSubAddNumType()));
					list.add(new BasicAttribute("smscsubaddress",
							messageSearchRequest.getSubAddress()));
					list.add(new BasicAttribute("objectClass", "psasubscriber"));
					list.add(new BasicAttribute("useragentprofile", "Control"));
					list.add(new BasicAttribute("prepaid", "yes"));
					list.add(new BasicAttribute("psatplacode",
							messageSearchRequest.getTplaCode()));
					list.add(new BasicAttribute("smscsubprepaid", "1"));
					list.add(new BasicAttribute("psaservices",
							messageSearchRequest.getPsaServices()));
					list.add(new BasicAttribute("psaforceflag", "Y"));
					list.add(new BasicAttribute("psa", messageSearchRequest
							.getPsa()));
					list.add(new BasicAttribute("psainpcode",
							messageSearchRequest.getInpCode()));
					list.add(new BasicAttribute("psaaction", "RESUME"));
					list.add(new BasicAttribute("smscsubprofilenumber", "17"));
					list.add(new BasicAttribute("smscsubblocked", "0"));
					list.add(new BasicAttribute("smscsubsmtpfnotif", "0"));
					list.add(new BasicAttribute("smscsubsmtpfdelack", "0"));
					list.add(new BasicAttribute("smscsubsmtpfuserack", "0"));
					list.add(new BasicAttribute("smscsubsmtpfsm", "0"));
					list.add(new BasicAttribute("smscsubsmtpfsmbinary", "0"));
					list.add(new BasicAttribute("smscsubsmtpfsmgsmudh", "0"));
					list.add(new BasicAttribute("smscsubsmtpfsmtdmatsar", "0"));
					list.add(new BasicAttribute("smscSubMsgArchivingEnabled",
							"0"));
					list.add(new BasicAttribute("smscSubMsgArchivingOrg", "0"));
					list.add(new BasicAttribute("smscSubMsgArchivingRcp", "0"));

					ModificationItem[] item = new ModificationItem[list.size()];
					for (int i = 0; i < list.size(); i++) {
						item[i] = new ModificationItem(
								DirContext.REPLACE_ATTRIBUTE, list.get(i));
					}
					ctx.modifyAttributes(query, item);
					System.out.println("======================================"
							+ query);
				} else {
					List<Attribute> list = new ArrayList<Attribute>();
					list.add(new BasicAttribute("telephoneNumber",
							messageSearchRequest.getTelePhoneNo()));
					list.add(new BasicAttribute("uniqueIdentifier",
							messageSearchRequest.getUniqIdentifier()));
					list.add(new BasicAttribute("sn", messageSearchRequest
							.getSn()));
					list.add(new BasicAttribute("givenName",
							messageSearchRequest.getGivenName()));
					list.add(new BasicAttribute("mail", messageSearchRequest
							.getEmailId()));
					list.add(new BasicAttribute("cn", messageSearchRequest
							.getCn()));
					list.add(new BasicAttribute("gender", messageSearchRequest
							.getGender()));
					list.add(new BasicAttribute("uid", messageSearchRequest
							.getUid()));
					list.add(new BasicAttribute("password",
							messageSearchRequest.getPassword()));
					list.add(new BasicAttribute("mobiletype",
							messageSearchRequest.getMobileType()));
					list.add(new BasicAttribute("language",
							messageSearchRequest.getLanguage()));
					list.add(new BasicAttribute("blocked", "no"));
					list.add(new BasicAttribute("smscsubaddressnumtype",
							messageSearchRequest.getSubAddNumType()));
					list.add(new BasicAttribute("smscsubaddress",
							messageSearchRequest.getSubAddress()));
					list.add(new BasicAttribute("objectClass", "subscriber"));
					list.add(new BasicAttribute("useragentprofile", "Postpaid"));
					list.add(new BasicAttribute("useragentstring",
							messageSearchRequest.getUserAgentString()));
					;
					list.add(new BasicAttribute("smscsubprofilenumber",
							messageSearchRequest.getSmscsubprofilenumber()));

					ModificationItem[] item = new ModificationItem[list.size()];
					for (int i = 0; i < list.size(); i++) {
						item[i] = new ModificationItem(
								DirContext.REPLACE_ATTRIBUTE, list.get(i));
					}
					ctx.modifyAttributes(query, item);
					System.out.println("======================================"
							+ query);

				}
				Map<String, String> map = new HashMap<String, String>();
				map.put("telephoneNumber",
						messageSearchRequest.getTelePhoneNo());
				map.put("uniqueIdentifier",
						messageSearchRequest.getUniqIdentifier());
				map.put("givenName", messageSearchRequest.getGivenName());
				map.put("uid", messageSearchRequest.getUid());
				map.put("mobiletype", messageSearchRequest.getMobileType());
				map.put("language", messageSearchRequest.getLanguage());

				messagingDataResponse.setAttributes(map);

			} catch (NamingException e) {
				messagingDataResponse.setApplicationError(new ApplicationError(
						e.getMessage()));
				e.printStackTrace();
			} finally {
				try {
					ctx.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		logger.info("##################### Ends: Add TAC ########################");

		return messagingDataResponse;
	}

	@Override
	public Map<String, Object> deleteMmscSubscriber(
			MessagingSearchRequest messageSearchRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ClientIDResponse> getClientIds() {
		// TODO Auto-generated method stub
		return null;
	}


	public String linkMSISDNProfile(MSISDNProfileRequest msisdnLinkRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	public String unLinkMSISDNProfile(MSISDNProfileRequest msisdnLinkRequest) {
		// TODO Auto-generated method stub
		return null;
	}
	
}