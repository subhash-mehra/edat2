package com.tmobile.edat.messaging.repository;

import java.util.List;
import java.util.Map;

import com.tmobile.edat.messaging.dto.request.MSISDNProfileRequest;
import com.tmobile.edat.messaging.dto.request.MessagingSearchRequest;
import com.tmobile.edat.messaging.dto.response.AccessTokenResponse;
import com.tmobile.edat.messaging.dto.response.ClientIDResponse;
import com.tmobile.edat.messaging.dto.response.MessagingDataResponse;
import com.tmobile.edat.messaging.dto.response.UnLockResponse;
import com.tmobile.edat.util.UserException;
import com.tmobile.session.SessionConstants;

public interface MessagingDataRepository {

	public MessagingDataResponse doIAMSearch(int search_type, String key,
			SessionConstants sessionConstants) throws UserException;

	public MessagingDataResponse doMMSCSearch(String msisdn,
			SessionConstants sessionConstants) throws UserException;

	public MessagingDataResponse doSMSCSearch(String key,
			SessionConstants sessionConstants) throws UserException;

	public Map<String, Object> addSubscriber(
			MessagingSearchRequest messagingSearchRequest);

	public Map<String, Object> modifySubscriber(
			MessagingSearchRequest messagingSearchRequest);

	public Map<String, Object> deleteSubscriber(
			MessagingSearchRequest messagingSearchRequest);

	public Map<String, Object> deleteSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest);

	public Map<String, Object> getSubscriber(
			MessagingSearchRequest messagingSearchRequest);

	public Map<String, Object> getSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest);

	public Map<String, Object> putSubscriber(
			MessagingSearchRequest messageSearchRequest);

	public MessagingDataResponse addSMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) throws UserException;

	public MessagingDataResponse deleteSMSC(
			MessagingSearchRequest messageSearchRequest) throws UserException;

	public MessagingDataResponse modifySMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) throws UserException;

	public Map<String, Object> deleteMmscSubscriber(
			MessagingSearchRequest messageSearchRequest);

	public List<ClientIDResponse> getClientIds();
}
