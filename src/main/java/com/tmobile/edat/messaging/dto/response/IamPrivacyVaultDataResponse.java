package com.tmobile.edat.messaging.dto.response;

import com.tmobile.edat.restservice.BaseResponseObjectModel;

public class IamPrivacyVaultDataResponse extends BaseResponseObjectModel{
	private Offers[] offers;

	public Offers[] getOffers() {
		return offers;
	}

	public void setOffers(Offers[] offers) {
		this.offers = offers;
	}

	@Override
	public String toString() {
		return "ClassPojo [offers = " + offers + "]";
	}
}
