package com.tmobile.edat.messaging.dto.response;

import com.tmobile.edat.login.model.User;

public class ImplicitPermissions
{
    private Lines[] lines;

    private User user;

    public Lines[] getLines ()
    {
        return lines;
    }

    public void setLines (Lines[] lines)
    {
        this.lines = lines;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ImplicitPermissions [lines = "+lines+", user = "+user+"]";
    }
}