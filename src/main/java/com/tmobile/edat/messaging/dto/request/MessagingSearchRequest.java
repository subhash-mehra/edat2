package com.tmobile.edat.messaging.dto.request;

import java.io.Serializable;
import java.util.List;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.session.SessionConstants;

/**
 * Created with IntelliJ IDEA.
 * User: bharatbhushan.m
 * Date: 10/10/14
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessagingSearchRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;
    
    protected String customerId;
    protected String msisdn;
    protected String imsi;
    protected String uid;
    protected String emailId;
    protected String subStatus;
    protected String service;
	protected String language;
    protected String subType;
    protected String operatorId;
    protected String profileId;
    protected String imsEnabled;
    protected String cos;
    protected String handsetType;
    protected String mmsCapable;
    protected String npa;
    protected String SMTPGWDomain;
    protected String subAddNumType;
    protected String subAddress;
    protected String mobileType;
    protected String password;
    protected String gender;
    protected String prepaid;
    protected String cn;
    protected String givenName;
    protected String sn;
    protected String telePhoneNo;
    protected String uniqIdentifier;
    protected String userAgentProfile;
    protected String userAgentString;
    protected String psa;
    protected String inpCode;
    protected String tplaCode;
    protected String psaServices;
    protected String smscsubprofilenumber;
    protected String smscsubprepaid;
    protected String psaforceflag;
    protected String psaaction;
    protected String psamsisdn;
    protected String clientId;
    protected String secretKey;
    private List<String> scope;
	private List<String> grantType;
    
    public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public List<String> getScope() {
		return scope;
	}

	public void setScope(List<String> scope) {
		this.scope = scope;
	}

	public List<String> getGrantType() {
		return grantType;
	}

	public void setGrantType(List<String> grantType) {
		this.grantType = grantType;
	}

	public String getSmscsubprepaid() {
		return smscsubprepaid;
	}

	public void setSmscsubprepaid(String smscsubprepaid) {
		this.smscsubprepaid = smscsubprepaid;
	}

	public String getPsaforceflag() {
		return psaforceflag;
	}

	public void setPsaforceflag(String psaforceflag) {
		this.psaforceflag = psaforceflag;
	}

	public String getPsaaction() {
		return psaaction;
	}

	public void setPsaaction(String psaaction) {
		this.psaaction = psaaction;
	}

	public String getPsamsisdn() {
		return psamsisdn;
	}

	public void setPsamsisdn(String psamsisdn) {
		this.psamsisdn = psamsisdn;
	}

	protected SessionConstants sessionConstants;
    
    public String getSmscsubprofilenumber() {
		return smscsubprofilenumber;
	}

	public void setSmscsubprofilenumber(String smscsubprofilenumber) {
		this.smscsubprofilenumber = smscsubprofilenumber;
	}

	public String getSubAddNumType() {
		return subAddNumType;
	}

	public void setSubAddNumType(String subAddNumType) {
		this.subAddNumType = subAddNumType;
	}

	public String getSubAddress() {
		return subAddress;
	}

	public void setSubAddress(String subAddress) {
		this.subAddress = subAddress;
	}

	public String getMobileType() {
		return mobileType;
	}

	public void setMobileType(String mobileType) {
		this.mobileType = mobileType;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPrepaid() {
		return prepaid;
	}

	public void setPrepaid(String prepaid) {
		this.prepaid = prepaid;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getTelePhoneNo() {
		return telePhoneNo;
	}

	public void setTelePhoneNo(String telePhoneNo) {
		this.telePhoneNo = telePhoneNo;
	}

	public String getUniqIdentifier() {
		return uniqIdentifier;
	}

	public void setUniqIdentifier(String uniqIdentifier) {
		this.uniqIdentifier = uniqIdentifier;
	}

	public String getUserAgentProfile() {
		return userAgentProfile;
	}

	public void setUserAgentProfile(String userAgentProfile) {
		this.userAgentProfile = userAgentProfile;
	}

	public String getUserAgentString() {
		return userAgentString;
	}

	public void setUserAgentString(String userAgentString) {
		this.userAgentString = userAgentString;
	}

	public String getPsa() {
		return psa;
	}

	public void setPsa(String psa) {
		this.psa = psa;
	}

	public String getInpCode() {
		return inpCode;
	}

	public void setInpCode(String inpCode) {
		this.inpCode = inpCode;
	}

	public String getTplaCode() {
		return tplaCode;
	}

	public void setTplaCode(String tplaCode) {
		this.tplaCode = tplaCode;
	}

	public String getPsaServices() {
		return psaServices;
	}

	public void setPsaServices(String psaServices) {
		this.psaServices = psaServices;
	}

	
    public String getCos() {
		return cos;
	}

	public void setCos(String cos) {
		this.cos = cos;
	}

		public String getHandsetType() {
		return handsetType;
	}

	public void setHandsetType(String handsetType) {
		this.handsetType = handsetType;
	}

	public String getMmsCapable() {
		return mmsCapable;
	}

	public void setMmsCapable(String mmsCapable) {
		this.mmsCapable = mmsCapable;
	}

	public String getNpa() {
		return npa;
	}

	public void setNpa(String npa) {
		this.npa = npa;
	}

	public String getSMTPGWDomain() {
		return SMTPGWDomain;
	}

	public void setSMTPGWDomain(String sMTPGWDomain) {
		SMTPGWDomain = sMTPGWDomain;
	}

	    
    public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getImsEnabled() {
		return imsEnabled;
	}

	public void setImsEnabled(String imsEnabled) {
		this.imsEnabled = imsEnabled;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}



    public MessagingSearchRequest() {
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }


    public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	@Override
    public String toString() {
        return "UsdSearchRequest [customerId=" + customerId + ", msisdn="
                + msisdn + ", imsi=" + imsi + "]";
    }
}
