package com.tmobile.edat.messaging.dto.response;

public class Offers {
	private String isvisibleWhenNotApproved;

	private String isVisibleIfUserNotOptin;

	private String offerType;

	private UserPreferences[] userPreferences;

	private String isValid;

	private String offerName;

	private String offerId;

	private String invalidFlag;

	private String approveScopeDescription;
	private String clientCategory;
	private String longDescription;
	private String shortDescription;
	private String thirdPartyPrivacyPolicyLink;
	private String privacyStatementLink;
	private String groupClientId;
	private String termsOfServiceLink;
	
	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getThirdPartyPrivacyPolicyLink() {
		return thirdPartyPrivacyPolicyLink;
	}

	public void setThirdPartyPrivacyPolicyLink(String thirdPartyPrivacyPolicyLink) {
		this.thirdPartyPrivacyPolicyLink = thirdPartyPrivacyPolicyLink;
	}

	public String getPrivacyStatementLink() {
		return privacyStatementLink;
	}

	public void setPrivacyStatementLink(String privacyStatementLink) {
		this.privacyStatementLink = privacyStatementLink;
	}

	public String getGroupClientId() {
		return groupClientId;
	}

	public void setGroupClientId(String groupClientId) {
		this.groupClientId = groupClientId;
	}

	public String getTermsOfServiceLink() {
		return termsOfServiceLink;
	}

	public void setTermsOfServiceLink(String termsOfServiceLink) {
		this.termsOfServiceLink = termsOfServiceLink;
	}

	
	

	public String getIsvisibleWhenNotApproved() {
		return isvisibleWhenNotApproved;
	}

	public void setIsvisibleWhenNotApproved(String isvisibleWhenNotApproved) {
		this.isvisibleWhenNotApproved = isvisibleWhenNotApproved;
	}

	public String getIsVisibleIfUserNotOptin() {
		return isVisibleIfUserNotOptin;
	}

	public void setIsVisibleIfUserNotOptin(String isVisibleIfUserNotOptin) {
		this.isVisibleIfUserNotOptin = isVisibleIfUserNotOptin;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public UserPreferences[] getUserPreferences() {
		return userPreferences;
	}

	public void setUserPreferences(UserPreferences[] userPreferences) {
		this.userPreferences = userPreferences;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getInvalidFlag() {
		return invalidFlag;
	}

	public void setInvalidFlag(String invalidFlag) {
		this.invalidFlag = invalidFlag;
	}

	public String getApproveScopeDescription() {
		return approveScopeDescription;
	}

	public void setApproveScopeDescription(String approveScopeDescription) {
		this.approveScopeDescription = approveScopeDescription;
	}

	@Override
	public String toString() {
		return "ClassPojo [isvisibleWhenNotApproved = "
				+ isvisibleWhenNotApproved + ", isVisibleIfUserNotOptin = "
				+ isVisibleIfUserNotOptin + ", offerType = " + offerType
				+ ", userPreferences = " + userPreferences + ", isValid = "
				+ isValid + ", offerName = " + offerName + ", offerId = "
				+ offerId + ", invalidFlag = " + invalidFlag
				+ ", approveScopeDescription = " + approveScopeDescription
				+ "]";
	}
}
