package com.tmobile.edat.messaging.dto.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.dusupdate.dto.Counter;

public class MessagingDataResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    
	private String uid="";
	private String message="";
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	 // Map store all attributes and values;
    protected Map<String, String> attributes;
    
   public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Map<String, String> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	public String toString() {
		return "MessagingDataResponse [message=="+message+"]";

}

}
