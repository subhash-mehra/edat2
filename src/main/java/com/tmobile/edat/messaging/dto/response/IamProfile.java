package com.tmobile.edat.messaging.dto.response;

public class IamProfile {
	private String totalSuccessLoginCount;

	private String notificationPreference;

	private String securityQuestionAnswer4;

	private String securityQuestionAnswer5;

	private String status;

	private String lastname;

	private String emailNewsLetter;

	private String firstname;

	private String emailLastUpdatedTimestamp;

	private String password;

	private String securityAnswerLastUpdateTimestamp;

	private String passwordLastUpdatedTimestamp;

	private String email;

	private String userId;

	private String statusLastUpdateTimestamp;

	private String securityQuestionAnswer1;

	private String securityQuestionAnswer3;

	private String annualEIPDisclosure;

	private String securityQuestionAnswer2;

	private String language;

	private String creationTimestamp;

	public String getTotalSuccessLoginCount() {
		return totalSuccessLoginCount;
	}

	public void setTotalSuccessLoginCount(String totalSuccessLoginCount) {
		this.totalSuccessLoginCount = totalSuccessLoginCount;
	}

	public String getNotificationPreference() {
		return notificationPreference;
	}

	public void setNotificationPreference(String notificationPreference) {
		this.notificationPreference = notificationPreference;
	}

	public String getSecurityQuestionAnswer4() {
		return securityQuestionAnswer4;
	}

	public void setSecurityQuestionAnswer4(String securityQuestionAnswer4) {
		this.securityQuestionAnswer4 = securityQuestionAnswer4;
	}

	public String getSecurityQuestionAnswer5() {
		return securityQuestionAnswer5;
	}

	public void setSecurityQuestionAnswer5(String securityQuestionAnswer5) {
		this.securityQuestionAnswer5 = securityQuestionAnswer5;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailNewsLetter() {
		return emailNewsLetter;
	}

	public void setEmailNewsLetter(String emailNewsLetter) {
		this.emailNewsLetter = emailNewsLetter;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmailLastUpdatedTimestamp() {
		return emailLastUpdatedTimestamp;
	}

	public void setEmailLastUpdatedTimestamp(String emailLastUpdatedTimestamp) {
		this.emailLastUpdatedTimestamp = emailLastUpdatedTimestamp;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityAnswerLastUpdateTimestamp() {
		return securityAnswerLastUpdateTimestamp;
	}

	public void setSecurityAnswerLastUpdateTimestamp(
			String securityAnswerLastUpdateTimestamp) {
		this.securityAnswerLastUpdateTimestamp = securityAnswerLastUpdateTimestamp;
	}

	public String getPasswordLastUpdatedTimestamp() {
		return passwordLastUpdatedTimestamp;
	}

	public void setPasswordLastUpdatedTimestamp(
			String passwordLastUpdatedTimestamp) {
		this.passwordLastUpdatedTimestamp = passwordLastUpdatedTimestamp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStatusLastUpdateTimestamp() {
		return statusLastUpdateTimestamp;
	}

	public void setStatusLastUpdateTimestamp(String statusLastUpdateTimestamp) {
		this.statusLastUpdateTimestamp = statusLastUpdateTimestamp;
	}

	public String getSecurityQuestionAnswer1() {
		return securityQuestionAnswer1;
	}

	public void setSecurityQuestionAnswer1(String securityQuestionAnswer1) {
		this.securityQuestionAnswer1 = securityQuestionAnswer1;
	}

	public String getSecurityQuestionAnswer3() {
		return securityQuestionAnswer3;
	}

	public void setSecurityQuestionAnswer3(String securityQuestionAnswer3) {
		this.securityQuestionAnswer3 = securityQuestionAnswer3;
	}

	public String getAnnualEIPDisclosure() {
		return annualEIPDisclosure;
	}

	public void setAnnualEIPDisclosure(String annualEIPDisclosure) {
		this.annualEIPDisclosure = annualEIPDisclosure;
	}

	public String getSecurityQuestionAnswer2() {
		return securityQuestionAnswer2;
	}

	public void setSecurityQuestionAnswer2(String securityQuestionAnswer2) {
		this.securityQuestionAnswer2 = securityQuestionAnswer2;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(String creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	@Override
	public String toString() {
		return "ClassPojo [totalSuccessLoginCount = " + totalSuccessLoginCount
				+ ", notificationPreference = " + notificationPreference
				+ ", securityQuestionAnswer4 = " + securityQuestionAnswer4
				+ ", securityQuestionAnswer5 = " + securityQuestionAnswer5
				+ ", status = " + status + ", lastname = " + lastname
				+ ", emailNewsLetter = " + emailNewsLetter + ", firstname = "
				+ firstname + ", emailLastUpdatedTimestamp = "
				+ emailLastUpdatedTimestamp + ", password = " + password
				+ ", securityAnswerLastUpdateTimestamp = "
				+ securityAnswerLastUpdateTimestamp
				+ ", passwordLastUpdatedTimestamp = "
				+ passwordLastUpdatedTimestamp + ", email = " + email
				+ ", userId = " + userId + ", statusLastUpdateTimestamp = "
				+ statusLastUpdateTimestamp + ", securityQuestionAnswer1 = "
				+ securityQuestionAnswer1 + ", securityQuestionAnswer3 = "
				+ securityQuestionAnswer3 + ", annualEIPDisclosure = "
				+ annualEIPDisclosure + ", securityQuestionAnswer2 = "
				+ securityQuestionAnswer2 + ", language = " + language
				+ ", creationTimestamp = " + creationTimestamp + "]";
	}
}
