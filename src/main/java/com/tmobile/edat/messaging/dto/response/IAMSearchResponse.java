package com.tmobile.edat.messaging.dto.response;

public class IAMSearchResponse {
	private ExplicitPermissions[] explicitPermissions;

	private ImplicitPermissions[] implicitPermissions;

	public ExplicitPermissions[] getExplicitPermissions() {
		return explicitPermissions;
	}

	public void setExplicitPermissions(ExplicitPermissions[] explicitPermissions) {
		this.explicitPermissions = explicitPermissions;
	}

	public ImplicitPermissions[] getImplicitPermissions() {
		return implicitPermissions;
	}

	public void setImplicitPermissions(ImplicitPermissions[] implicitPermissions) {
		this.implicitPermissions = implicitPermissions;
	}

	@Override
	public String toString() {
		return "IAMSearchResponse [explicitPermissions = "
				+ explicitPermissions + ", implicitPermissions = "
				+ implicitPermissions + "]";
	}
}