package com.tmobile.edat.messaging.dto.response;

public class ExplicitPermissions
{
    private String grantorEmail;

    private String isPermissionExpired;

    private String grantedPrivilege;

    private String grantedTimestamp;

    private String msisdn;

    private String startTimestamp;

    private String lastUpdatedTimestamp;

    private String state;

    private String givenName;

    private String grantee;

    private String serviceName;

    private String granteeEmail;

    private String isPrimaryMSISDN;

    private String grantor;

    private String imsi;

    private String endTimestamp;

    private String serviceAttributes;

    public String getGrantorEmail ()
    {
        return grantorEmail;
    }

    public void setGrantorEmail (String grantorEmail)
    {
        this.grantorEmail = grantorEmail;
    }

    public String getIsPermissionExpired ()
    {
        return isPermissionExpired;
    }

    public void setIsPermissionExpired (String isPermissionExpired)
    {
        this.isPermissionExpired = isPermissionExpired;
    }

    public String getGrantedPrivilege ()
    {
        return grantedPrivilege;
    }

    public void setGrantedPrivilege (String grantedPrivilege)
    {
        this.grantedPrivilege = grantedPrivilege;
    }

    public String getGrantedTimestamp ()
    {
        return grantedTimestamp;
    }

    public void setGrantedTimestamp (String grantedTimestamp)
    {
        this.grantedTimestamp = grantedTimestamp;
    }

    public String getMsisdn ()
    {
        return msisdn;
    }

    public void setMsisdn (String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getStartTimestamp ()
    {
        return startTimestamp;
    }

    public void setStartTimestamp (String startTimestamp)
    {
        this.startTimestamp = startTimestamp;
    }

    public String getLastUpdatedTimestamp ()
    {
        return lastUpdatedTimestamp;
    }

    public void setLastUpdatedTimestamp (String lastUpdatedTimestamp)
    {
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getGivenName ()
    {
        return givenName;
    }

    public void setGivenName (String givenName)
    {
        this.givenName = givenName;
    }

    public String getGrantee ()
    {
        return grantee;
    }

    public void setGrantee (String grantee)
    {
        this.grantee = grantee;
    }

    public String getServiceName ()
    {
        return serviceName;
    }

    public void setServiceName (String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getGranteeEmail ()
    {
        return granteeEmail;
    }

    public void setGranteeEmail (String granteeEmail)
    {
        this.granteeEmail = granteeEmail;
    }

    public String getIsPrimaryMSISDN ()
    {
        return isPrimaryMSISDN;
    }

    public void setIsPrimaryMSISDN (String isPrimaryMSISDN)
    {
        this.isPrimaryMSISDN = isPrimaryMSISDN;
    }

    public String getGrantor ()
    {
        return grantor;
    }

    public void setGrantor (String grantor)
    {
        this.grantor = grantor;
    }

    public String getImsi ()
    {
        return imsi;
    }

    public void setImsi (String imsi)
    {
        this.imsi = imsi;
    }

    public String getEndTimestamp ()
    {
        return endTimestamp;
    }

    public void setEndTimestamp (String endTimestamp)
    {
        this.endTimestamp = endTimestamp;
    }

    public String getServiceAttributes ()
    {
        return serviceAttributes;
    }

    public void setServiceAttributes (String serviceAttributes)
    {
        this.serviceAttributes = serviceAttributes;
    }

    @Override
    public String toString()
    {
        return "ExplicitPermissions [grantorEmail = "+grantorEmail+", isPermissionExpired = "+isPermissionExpired+", grantedPrivilege = "+grantedPrivilege+", grantedTimestamp = "+grantedTimestamp+", msisdn = "+msisdn+", startTimestamp = "+startTimestamp+", lastUpdatedTimestamp = "+lastUpdatedTimestamp+", state = "+state+", givenName = "+givenName+", grantee = "+grantee+", serviceName = "+serviceName+", granteeEmail = "+granteeEmail+", isPrimaryMSISDN = "+isPrimaryMSISDN+", grantor = "+grantor+", imsi = "+imsi+", endTimestamp = "+endTimestamp+", serviceAttributes = "+serviceAttributes+"]";
    }
}