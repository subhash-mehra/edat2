package com.tmobile.edat.messaging.dto.response;

public class ClientIDResponse {

	private String clientId;
	private String secretKey;
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
		
	public String toString() {
		return "TokenGeneratorResponse [clientId==" + clientId + "secretKey=="
				+ secretKey + "]";
	}
	
}
