package com.tmobile.edat.messaging.dto.response;

public class Lines
{
    private String ban;

    private String customerType;

    private String msisdn;

    private String accountStatus;

    private String isLineGrantable;

    private String isPrimaryMSISDN;

    private String imsi;

    private String givenName;

    private String permissionType;

    public String getBan ()
    {
        return ban;
    }

    public void setBan (String ban)
    {
        this.ban = ban;
    }

    public String getCustomerType ()
    {
        return customerType;
    }

    public void setCustomerType (String customerType)
    {
        this.customerType = customerType;
    }

    public String getMsisdn ()
    {
        return msisdn;
    }

    public void setMsisdn (String msisdn)
    {
        this.msisdn = msisdn;
    }

    public String getAccountStatus ()
    {
        return accountStatus;
    }

    public void setAccountStatus (String accountStatus)
    {
        this.accountStatus = accountStatus;
    }

    public String getIsLineGrantable ()
    {
        return isLineGrantable;
    }

    public void setIsLineGrantable (String isLineGrantable)
    {
        this.isLineGrantable = isLineGrantable;
    }

    public String getIsPrimaryMSISDN ()
    {
        return isPrimaryMSISDN;
    }

    public void setIsPrimaryMSISDN (String isPrimaryMSISDN)
    {
        this.isPrimaryMSISDN = isPrimaryMSISDN;
    }

    public String getImsi ()
    {
        return imsi;
    }

    public void setImsi (String imsi)
    {
        this.imsi = imsi;
    }

    public String getGivenName ()
    {
        return givenName;
    }

    public void setGivenName (String givenName)
    {
        this.givenName = givenName;
    }

    public String getPermissionType ()
    {
        return permissionType;
    }

    public void setPermissionType (String permissionType)
    {
        this.permissionType = permissionType;
    }

    @Override
    public String toString()
    {
        return "Lines [ban = "+ban+", customerType = "+customerType+", msisdn = "+msisdn+", accountStatus = "+accountStatus+", isLineGrantable = "+isLineGrantable+", isPrimaryMSISDN = "+isPrimaryMSISDN+", imsi = "+imsi+", givenName = "+givenName+", permissionType = "+permissionType+"]";
    }
}