package com.tmobile.edat.messaging.dto.response;

import com.tmobile.edat.restservice.BaseResponseObjectModel;

public class IamProfileResponse extends BaseResponseObjectModel {
	private IamProfile iamProfile;

	public IamProfile getIamProfile() {
		return iamProfile;
	}

	public void setIamProfile(IamProfile iamProfile) {
		this.iamProfile = iamProfile;
	}

	@Override
	public String toString() {
		return "ClassPojo [iamProfile = " + iamProfile + "]";
	}
}
