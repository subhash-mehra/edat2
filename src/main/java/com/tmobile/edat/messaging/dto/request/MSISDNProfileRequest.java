package com.tmobile.edat.messaging.dto.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;
import com.tmobile.session.SessionConstants;

public class MSISDNProfileRequest extends BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String msisdn;
	private String prepaid_pin;
	private String sms_pin;
	private String ssn;
	private String user_id;
	
	//for unlink MSISDN Profile
	private boolean revokePermission;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getPrepaid_pin() {
		return prepaid_pin;
	}
	public void setPrepaid_pin(String prepaid_pin) {
		this.prepaid_pin = prepaid_pin;
	}
	public String getSms_pin() {
		return sms_pin;
	}
	public void setSms_pin(String sms_pin) {
		this.sms_pin = sms_pin;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	protected SessionConstants sessionConstants;
	public SessionConstants getSessionConstants() {
		return sessionConstants;
	}

	public void setSessionConstants(SessionConstants sessionConstants) {
		this.sessionConstants = sessionConstants;
	}
	public boolean isRevokePermission() {
		return revokePermission;
	}
	public void setRevokePermission(boolean revokePermission) {
		this.revokePermission = revokePermission;
	}

}
