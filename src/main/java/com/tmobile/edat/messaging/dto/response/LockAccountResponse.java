package com.tmobile.edat.messaging.dto.response;

import com.tmobile.edat.restservice.BaseResponseObjectModel;

public class LockAccountResponse extends BaseResponseObjectModel {
	private String profile_type;

	public String getProfile_type() {
		return profile_type;
	}

	public void setProfile_type(String profile_type) {
		this.profile_type = profile_type;
	}

	@Override
	public String toString() {
		return "LockAccountResponse [profile_type = " + profile_type + "]";
	}
}
