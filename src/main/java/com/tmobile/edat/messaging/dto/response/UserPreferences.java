package com.tmobile.edat.messaging.dto.response;

public class UserPreferences {
	private String isDefault;

	private String preferenceName;

	private String preferenceId;

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getPreferenceName() {
		return preferenceName;
	}

	public void setPreferenceName(String preferenceName) {
		this.preferenceName = preferenceName;
	}

	public String getPreferenceId() {
		return preferenceId;
	}

	public void setPreferenceId(String preferenceId) {
		this.preferenceId = preferenceId;
	}

	@Override
	public String toString() {
		return "ClassPojo [isDefault = " + isDefault + ", preferenceName = "
				+ preferenceName + ", preferenceId = " + preferenceId + "]";
	}
}
