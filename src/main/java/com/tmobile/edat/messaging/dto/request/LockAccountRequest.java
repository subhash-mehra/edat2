package com.tmobile.edat.messaging.dto.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;

public class LockAccountRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String user_id;
    
	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	
}
