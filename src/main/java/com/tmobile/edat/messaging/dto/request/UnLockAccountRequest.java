package com.tmobile.edat.messaging.dto.request;

import java.io.Serializable;

import com.tmobile.edat.base.BaseRequest;

public class UnLockAccountRequest extends BaseRequest  implements Serializable {
    private static final long serialVersionUID = 11L;

	private String  user_id;
    private	Boolean isAccountLocked;
	private String reason;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public Boolean getIsAccountLocked() {
		return isAccountLocked;
	}
	public void setIsAccountLocked(Boolean isAccountLocked) {
		this.isAccountLocked = isAccountLocked;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}	
}
