package com.tmobile.edat.messaging.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.tmobile.edat.base.ApplicationConstants;
import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseRequest;
import com.tmobile.edat.base.BaseResponse;
import com.tmobile.edat.messaging.dto.request.MessagingSearchRequest;
import com.tmobile.edat.messaging.dto.response.AccessTokenResponse;
import com.tmobile.edat.messaging.dto.response.ClientIDResponse;
import com.tmobile.edat.messaging.dto.response.MessagingDataResponse;
import com.tmobile.edat.messaging.dto.response.UnLockResponse;
import com.tmobile.edat.messaging.repository.MessagingDataRepository;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.util.Constants;
import com.tmobile.edat.util.UserException;

@Component("MessagingDataComponent")
public class MessagingDataComponentImpl implements MessagingDataComponent {

	private static final Logger logger = Logger
			.getLogger(MessagingDataComponentImpl.class);

	@Autowired
	@Qualifier("MessagingDataRepository")
	MessagingDataRepository messageDataRepository = null;

	public MessagingDataComponentImpl() {
	}

	@Override
	public boolean validateRequest(BaseRequest baseRequest,
			BaseResponse baseResponse, int key) {

		// Applicable to all methods
		if (baseRequest == null) {
			baseResponse = new NapDataResponse();
			baseResponse.setApplicationError(new ApplicationError(
					ApplicationConstants.NULL_REQUEST));
			return false;
		}

		return true;
	}

	@Override
	public MessagingDataResponse getIAMSearch(
			MessagingSearchRequest messageSearchRequest) throws UserException {
		logger.info("Checking in getIAMSearch with  := "
				+ messageSearchRequest.getMsisdn());

		MessagingDataResponse iamDataResponse = null;
		Map<String, String> attributes = null;

		if (this.validateRequest(messageSearchRequest, iamDataResponse, 0)) {
			// By CustomerId
			if ((messageSearchRequest.getCustomerId() != null)
					&& messageSearchRequest.getMsisdn() == null
					&& messageSearchRequest.getImsi() == null) {
				iamDataResponse = this.messageDataRepository.doIAMSearch(
						Constants.SEARCH_TYPE_CUSTID,
						messageSearchRequest.getCustomerId(),
						messageSearchRequest.getSessionConstants());
				// iamDataResponse = new MessagingDataResponse();
				// iamDataResponse.setAttributes(attributes);
			}
			// By Msisdn
			else if (messageSearchRequest.getMsisdn() != null
					&& messageSearchRequest.getCustomerId() == null
					&& messageSearchRequest.getImsi() == null) {
				iamDataResponse = this.messageDataRepository.doIAMSearch(
						Constants.SEARCH_TYPE_MSISDN,
						messageSearchRequest.getMsisdn(),
						messageSearchRequest.getSessionConstants());
				// iamDataResponse = new MessagingDataResponse();
				// iamDataResponse.setAttributes(attributes);

			}
			// By imsi
			else if (messageSearchRequest.getImsi() != null
					&& messageSearchRequest.getMsisdn() == null
					&& messageSearchRequest.getCustomerId() == null) {
				iamDataResponse = this.messageDataRepository.doIAMSearch(
						Constants.SEARCH_TYPE_IMSI,
						messageSearchRequest.getImsi(),
						messageSearchRequest.getSessionConstants());
				// iamDataResponse = new MessagingDataResponse();
				// iamDataResponse.setAttributes(attributes);
			} else if (messageSearchRequest.getEmailId() != null
					&& messageSearchRequest.getMsisdn() == null
					&& messageSearchRequest.getCustomerId() == null) {
				iamDataResponse = this.messageDataRepository.doIAMSearch(
						Constants.SEARCH_TYPE_EMAIL,
						messageSearchRequest.getEmailId(),
						messageSearchRequest.getSessionConstants());
				// iamDataResponse = new MessagingDataResponse();
				// iamDataResponse.setAttributes(attributes);

				// TODO : Appropriate Return Error Code : INVALID USER INPUT
			}
		}

		return iamDataResponse;
	}

	@Override
	public MessagingDataResponse getSMSCSearch(
			MessagingSearchRequest messageSearchRequest) throws UserException {

		logger.info("Searching in getSMSCSearch with  := "
				+ messageSearchRequest.getMsisdn());

		MessagingDataResponse iamDataResponse = null;
		Map<String, String> attributes = null;

		if (messageSearchRequest.getMsisdn() != null
				&& messageSearchRequest.getCustomerId() == null
				&& messageSearchRequest.getImsi() == null) {
			iamDataResponse = this.messageDataRepository.doSMSCSearch(
					messageSearchRequest.getMsisdn(),
					messageSearchRequest.getSessionConstants());
			// iamDataResponse = new MessagingDataResponse();
			// iamDataResponse.setAttributes(attributes);

		} else {
			// TODO : Appropriate Return Error Code : INVALID USER INPUT
		}
		return iamDataResponse;
	}

	@Override
	public MessagingDataResponse getMMSSearch(
			MessagingSearchRequest messageSearchRequest) throws UserException {

		logger.info("Searching in getMMSSearch with  := "
				+ messageSearchRequest.getMsisdn());

		MessagingDataResponse iamDataResponse = null;
		Map<String, String> attributes = null;

		if (messageSearchRequest.getMsisdn() != null) {
			iamDataResponse = this.messageDataRepository.doMMSCSearch(
					messageSearchRequest.getMsisdn(),
					messageSearchRequest.getSessionConstants());
			// iamDataResponse = new MessagingDataResponse();
			// iamDataResponse.setAttributes(attributes);

		} else {
			// TODO : Appropriate Return Error Code : INVALID USER INPUT
		}
		return iamDataResponse;
	}

	@Override
	public Map<String, Object> addSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		logger.info("====================Add Subscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.addSubscriber(messagingSearchRequest);

		return dataMap;
	}

	@Override
	public Map<String, Object> modifySubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		logger.info("====================Modify Subscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.modifySubscriber(messagingSearchRequest);

		return dataMap;

	}

	@Override
	public Map<String, Object> deleteSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		logger.info("====================Delete Subscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.deleteSubscriber(messagingSearchRequest);

		return dataMap;
	}

	public Map<String, Object> deleteMmscSubscriber(
			MessagingSearchRequest messageSearchRequest) {
		logger.info("====================Add SMSCSubscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		MessagingDataResponse messagingDataResponse = null;
		dataMap = this.messageDataRepository
				.deleteMmscSubscriber(messageSearchRequest);
		return dataMap;
	}

	@Override
	public Map<String, Object> deleteSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest) {
		logger.info("====================Delete Subscriber Service Specific====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.deleteSubscriberServiceSpecific(messagingSearchRequest);

		return dataMap;
	}

	public Map<String, Object> getSubscriber(
			MessagingSearchRequest messagingSearchRequest) {
		logger.info("====================Get Subscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.getSubscriber(messagingSearchRequest);

		return dataMap;
	}

	public Map<String, Object> getSubscriberServiceSpecific(
			MessagingSearchRequest messagingSearchRequest) {
		logger.info("====================Get Subscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.getSubscriberServiceSpecific(messagingSearchRequest);

		return dataMap;
	}

	public Map<String, Object> putSubscriber(
			MessagingSearchRequest messageSearchRequest) {
		logger.info("====================Get Subscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap = this.messageDataRepository
				.putSubscriber(messageSearchRequest);

		return dataMap;
	}

	public MessagingDataResponse addSMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) throws UserException {
		logger.info("====================Add SMSCSubscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		MessagingDataResponse messagingDataResponse = null;
		messagingDataResponse = this.messageDataRepository
				.addSMSCSubscriber(messageSearchRequest);
		return messagingDataResponse;
	}

	public MessagingDataResponse deleteSMSC(
			MessagingSearchRequest messageSearchRequest) throws UserException {
		logger.info("====================Add SMSCSubscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		MessagingDataResponse messagingDataResponse = null;
		messagingDataResponse = this.messageDataRepository
				.deleteSMSC(messageSearchRequest);
		return messagingDataResponse;
	}

	public MessagingDataResponse modifySMSCSubscriber(
			MessagingSearchRequest messageSearchRequest) throws UserException {
		logger.info("====================Add SMSCSubscriber====================");
		Map<String, Object> dataMap = new HashMap<String, Object>();
		MessagingDataResponse messagingDataResponse = null;
		messagingDataResponse = this.messageDataRepository
				.modifySMSCSubscriber(messageSearchRequest);
		return messagingDataResponse;
	}

	@Override
	public List<ClientIDResponse> getClientIds() {

		return this.messageDataRepository.getClientIds();
	}

}
