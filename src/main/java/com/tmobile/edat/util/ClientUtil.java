package com.tmobile.edat.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.tmobile.edat.restservice.RequestObject;

public class ClientUtil {
	private static final Logger logger = Logger.getLogger(ClientUtil.class);

	/**
	 * It parse the @param requestParams and return RequestObject which contains
	 * urlParameters, tokenParameters and headerParameters
	 * 
	 * @param requestParams
	 * @return
	 */
	public static RequestObject getRequestObject(
			Map<String, Object> requestParams) {
		Map<String, Object> urlParameters = null;
		Map<String, Object> tokenParams = null;
		Set<String> requestParamsKeys = requestParams.keySet();
		if (!(requestParamsKeys.contains("urlParams") && requestParamsKeys
				.contains("tokenParams"))) {
			logger.error("Please pass urlParams and tokenParams as key in ajax call");
			return null;
		}
		for (String key : requestParams.keySet()) {

			if (requestParams.get(key) instanceof LinkedHashMap<?, ?>) {

				if ("urlParams" == key) {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, String> urlParams = (LinkedHashMap<String, String>) requestParams
							.get(key);
					if (null != urlParams) {
						urlParameters = new LinkedHashMap<String, Object>();
						for (String urlParamKey : urlParams.keySet()) {
							logger.info("urlParamKey = " + urlParamKey
									+ " urlParamVal = "
									+ urlParams.get(urlParamKey));
							// url parameters
							urlParameters.put(urlParamKey,
									urlParams.get(urlParamKey));
						}
					}

				} else if ("tokenParams" == key) {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, Object> tokenReqParams = (LinkedHashMap<String, Object>) requestParams
							.get(key);
					if (null != tokenReqParams) {
						tokenParams = new LinkedHashMap<String, Object>();
						;
						for (String tokenParamKey : tokenReqParams.keySet()) {
							logger.info("tokenParamKey = "
									+ tokenParamKey
									+ " tokenParamVal = "
									+ tokenReqParams.get(tokenParamKey)
									+ " instance = "
									+ tokenReqParams.get(tokenParamKey)
											.getClass());
							// token parameters
							tokenParams.put(tokenParamKey,
									tokenReqParams.get(tokenParamKey));
						}
					}

				}
			}

		}

		RequestObject requestObject = new RequestObject();
		requestObject.setTokenParams(tokenParams);
		requestObject.setUrlParameters(urlParameters);

		return requestObject;

	}

	/**
	 * It parse the @param requestParams for generate sms pin request and return
	 * request parameters
	 * 
	 * @param requestParams
	 * @return
	 */
	public static Map<String, Object> getSmsPinParameters(
			Map<String, Object> requestParams) {
		Set<String> requestParamsKeys = requestParams.keySet();
		if (!(requestParamsKeys.contains("smsPinParams"))) {
			logger.error("Please pass urlParams and tokenParams as key in ajax call");
			return null;
		}
		LinkedHashMap<String, Object> smsPinParameters = null;
		for (String key : requestParams.keySet()) {

			if (requestParams.get(key) instanceof LinkedHashMap<?, ?>) {

				if ("smsPinParams" == key) {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, String> smsPinParams = (LinkedHashMap<String, String>) requestParams
							.get(key);
					if (null != smsPinParams) {
						smsPinParameters = new LinkedHashMap<String, Object>();
						for (String paramKey : smsPinParams.keySet()) {
							logger.info("generatePinParamsKey = " + paramKey
									+ " generatePinParamsVal = "
									+ smsPinParams.get(paramKey));
							// url parameters
							smsPinParameters.put(paramKey,
									smsPinParams.get(paramKey));
						}
					}

				}
			}
		}
		return smsPinParameters;
	}

	/**
	 * It parse the @param requestParams for get IamProfile Request request and
	 * return request parameters
	 * 
	 * @param requestParams
	 * @return
	 */
	public static Map<String, Object> getUserIdParameters(
			Map<String, Object> requestParams) {
		Set<String> requestParamsKeys = requestParams.keySet();
		if (!(requestParamsKeys.contains("userIdUrlParams"))) {
			logger.error("Please pass urlParams and tokenParams as key in ajax call");
			return null;
		}
		LinkedHashMap<String, Object> userIdParameters = null;
		for (String key : requestParams.keySet()) {

			if (requestParams.get(key) instanceof LinkedHashMap<?, ?>) {

				if ("userIdUrlParams" == key) {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, String> userIdParams = (LinkedHashMap<String, String>) requestParams
							.get(key);
					if (null != userIdParams) {
						userIdParameters = new LinkedHashMap<String, Object>();
						for (String paramKey : userIdParams.keySet()) {
							logger.info("generatePinParamsKey = " + paramKey
									+ " generatePinParamsVal = "
									+ userIdParams.get(paramKey));
							// url parameters
							userIdParameters.put(paramKey,
									userIdParams.get(paramKey));
						}
					}

				}
			}
		}
		return userIdParameters;
	}

}
