package com.tmobile.edat.util;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class LDAPConnectionManager {

	public static LdapContext getLdapContext(LDAPInfo info) throws UserException {
		LdapContext ctx = null;
		try {
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, info.LDAP_CONTEXT_FACTORY);
			env.put(Context.SECURITY_AUTHENTICATION,
					info.SECURITY_AUTHENTICATION);
			env.put(Context.SECURITY_PRINCIPAL, info.SECURITY_PRINCIPAL_NAP);
			env.put(Context.SECURITY_CREDENTIALS, info.SECURITY_CREDENTIALS_NAP);
			env.put(Context.PROVIDER_URL, info.PROVIDER_URL);
			ctx = new InitialLdapContext(env, null);
			// System.out.println("Connection Successful.");
		} catch (NamingException nex) {
			System.err.println("LDAP Connection: FAILED");
			throw new UserException(nex.getMessage());
			//nex.printStackTrace();
		}

		return ctx;
	}

	public static DirContext getDirContext(LDAPInfo info) throws UserException{
		DirContext ctx = null;
		try {

			Properties properties = new Properties();
			properties.put(Context.INITIAL_CONTEXT_FACTORY,
					info.LDAP_CONTEXT_FACTORY);
			properties.put(Context.PROVIDER_URL, info.PROVIDER_URL);

			properties.put(Context.SECURITY_AUTHENTICATION,
					info.SECURITY_AUTHENTICATION);
			properties.put(Context.SECURITY_PRINCIPAL,
					info.SECURITY_PRINCIPAL_NAP);
			properties.put(Context.SECURITY_CREDENTIALS,
					info.SECURITY_CREDENTIALS_NAP);
			ctx = new InitialDirContext(properties);
			// System.out.println("Connection Successful.");
		} catch (NamingException nex) {
			System.err.println("LDAP Connection: FAILED");
			throw new UserException(nex.getMessage());
		}

		return ctx;
	}

}
