package com.tmobile.edat.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

	public static Date stringToDate(String dateInString) {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		// String dateInString = "07/06/2013";

		try {

			date = formatter.parse(dateInString);
			System.out.println(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static void main(String[] args) throws ParseException {

		Date myDate = new Date();
		System.out.println("Current Date: " + myDate);

		java.util.Date temp = new SimpleDateFormat("MM/dd/yyyy")
				.parse("11/14/2014");
		stringToDate("11/14/2014");
		//System.out.println(temp);

		/*
		 * System.out.println("MM-DD-YYY Date "+new
		 * SimpleDateFormat("MM-dd-yyyy").format(myDate));
		 * System.out.println(new
		 * SimpleDateFormat("yyyy-MM-dd").format(myDate));
		 * System.out.println(myDate);
		 */
		String result="(iid 123456, rc 1013)";
		int index=result.indexOf("rc");
		index=index+3;
		System.out.println("======="+result.substring(index, index+4));
	}

	public static Date stringDateFormater(String dateInString) {
		java.util.Date changeDate = null;
		try {
			changeDate = new SimpleDateFormat("MM/dd/yyyy").parse(dateInString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*
		 * SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); Date
		 * date = null;
		 * 
		 * 
		 * DateFormat DATE_FORMAT_1 = new
		 * SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ"); //String dateInString =
		 * "07/06/2013";
		 * 
		 * try {
		 * 
		 * date = DATE_FORMAT_1.parse(dateInString); System.out.println(date);
		 * System.out.println(formatter.format(date));
		 * 
		 * } catch (ParseException e) { e.printStackTrace(); }
		 */
		return changeDate;
	}

	public static String formateDate(String inputDate, String format1, String format2) {
		
		String dateString = null;
		try{
				SimpleDateFormat startFormat = new SimpleDateFormat(format1);
		
				SimpleDateFormat toFormat = new SimpleDateFormat(format2);
		
				Date date = startFormat.parse(inputDate);
			
				dateString = toFormat.format(date);
		}catch(ParseException p){
			System.out.println("ParseException::::::::: "+p.getMessage());
		}

		return dateString;

	}
}
