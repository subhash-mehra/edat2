package com.tmobile.edat.util;

import com.tmobile.edat.base.ApplicationError;
import com.tmobile.edat.base.BaseResponse;

public class UserException extends Exception{
	
	BaseResponse userException = new BaseResponse();
	public UserException(String exception)
	{
		userException.setApplicationError(new ApplicationError(exception));
	}

}
