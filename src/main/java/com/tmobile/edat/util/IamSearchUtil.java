package com.tmobile.edat.util;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMethod;

import com.tmobile.edat.messaging.dto.response.IAMSearchResponse;
import com.tmobile.edat.restservice.RequestHandler;
import com.tmobile.edat.restservice.RequestObject;
import com.tmobile.edat.restservice.UriMaker;
import com.tmobile.edat.util.Constants.IAMSearchAPIEnum;
import com.tmobile.edat.util.Constants.RequestMediaType;
import com.tmobile.edat.util.Constants.RestAPI;

public class IamSearchUtil {
	
	
	public static IAMSearchResponse iamSearchApi(Map<String, Object> requestParams, Constants.IAMSearchAPIEnum apiName) {
		IAMSearchResponse iamSearchResponse = null;
		
		Map<String, String> urlHeaderParams = new LinkedHashMap<String, String>();
		urlHeaderParams.put("Accept", "application/json");
		urlHeaderParams.put("Content-Type", "application/json");
		
		RequestObject requestObject = null;
		if (null != requestParams) {
			requestObject = ClientUtil.getRequestObject(requestParams);
		}
		RequestHandler requestHandler = RequestHandler
				.getInstance(requestObject.getTokenParams());
		
		String uri = null;
		if(apiName.equals(IAMSearchAPIEnum.CONSUMER)){
			uri = UriMaker.makeUri(RestAPI.REST_IAM_SEARCH_CONSUMERAPI,
					requestObject.getUrlParameters());
		}
		else if(apiName.equals(IAMSearchAPIEnum.REBIILION)){
			uri = UriMaker.makeUri(RestAPI.REST_IAM_SEARCH_IAMPROVISIONINGAPI,
					requestObject.getUrlParameters());
		}
		else if(apiName.equals(IAMSearchAPIEnum.CONTRACT)){
			uri = UriMaker.makeUri(RestAPI.REST_IAM_SEARCH_CONTRACTAPI,
					requestObject.getUrlParameters());
		}
		else if(apiName.equals(IAMSearchAPIEnum.PERMISSION)){
			uri = UriMaker.makeUri(RestAPI.REST_IAM_SEARCH_PERMISSIONAPI,
					requestObject.getUrlParameters());
		}
		else if(apiName.equals(IAMSearchAPIEnum.PRIVACY)){
			uri = UriMaker.makeUri(RestAPI.REST_IAM_SEARCH_PRIVACYAPI,
					requestObject.getUrlParameters());
		}
		
		iamSearchResponse = requestHandler.sendHttpRequest(uri,
				RequestMediaType.JSON, RequestMethod.GET, urlHeaderParams,
				IAMSearchResponse.class);
		System.out.println(">>>>getImplicitPermissions = "+iamSearchResponse.getImplicitPermissions());
		
		/*IAMSearchResponse obj = new IAMSearchResponse();
		obj.setMsisdnNum("1234567890");
		obj.setMsisdn("msisdn");*/
		return iamSearchResponse;
	}
}
