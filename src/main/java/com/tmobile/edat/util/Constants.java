package com.tmobile.edat.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.tmobile.listener.ServeletListener;

@Component("constants")
public class Constants {

	public static final int SEARCH_TYPE_IMSI = 1;
	public static final int SEARCH_TYPE_MSISDN = 2;
	public static final int SEARCH_TYPE_CUSTID = 3;
	public static final int SEARCH_TYPE_UID = 4;
	public static final int SEARCH_TYPE_EMAIL = 5;
	public static final int SEARCH_TYPE_USER_ID = 6;
	public static final int SEARCH_TYPE_IMEI = 7;

	/*
	 * edit by Vikas Choudhary
	 */
	// comment for change ip 10.25.72.247 with new IP 10.178.69.1
	/* public static String LDAP_SERVER = "10.25.72.247:16800"; */
	public static String LDAP_SERVER = "10.178.69.1:16800";

	/* For USD Module */
	// comment for change ip 10.25.72.247 with new IP 10.178.69.1

	/*
	 * public static String LDAP_SERVER_NAP = "10.25.72.247:16800"; public
	 * static String LDAP_SERVER_DUS = "10.25.72.247:16800"; public static
	 * String LDAP_SERVERL_PCRF = "10.25.72.247:16800"; public static String
	 * LDAP_SERVER_UMA = "10.25.72.247:16800"; public static String
	 * LDAP_SERVER_HLR = "10.25.72.247:16800"; public static String
	 * LDAP_SERVER_EIR = "10.25.72.247:16800"; public static String
	 * LDAP_SERVER_HSS = "10.25.72.247:16800";
	 */

	public static String LDAP_SERVER_NAP = "10.178.69.1:16800";
	public static String LDAP_SERVER_DUS = "10.178.69.1:16800";
	public static String LDAP_SERVERL_PCRF = "10.178.69.1:16800";
	public static String LDAP_SERVER_UMA = "10.178.69.1:16800";
	public static String LDAP_SERVER_HLR = "10.178.69.1:16800";
	public static String LDAP_SERVER_EIR = "10.178.69.1:16800";
	public static String LDAP_SERVER_HSS = "10.178.69.1:16800";

	public static String LDAP_SERVER_PCRF_SESSION = "10.169.56.3:389";

	// comment for change ip 10.25.72.247 with new IP 10.178.69.1
	/*
	 * public static String LDAP_SERVER_EIR_ADD_IMSI = "10.25.72.247:16800";
	 * public static String LDAP_SERVER_UNLOCK_IMSI_IMEI = "10.25.72.247:16800";
	 * public static String LDAP_SERVER_TAC_SEARCH= "10.25.72.247:16800"; public
	 * static String LDAP_SERVER_ADD_TAC= "10.25.72.247:16800"; public static
	 * String LDAP_SERVER_ENABLE_MMS= "10.25.72.247:16800";
	 */

	public static String LDAP_SERVER_EIR_ADD_IMSI = "10.178.69.1:16800";
	public static String LDAP_SERVER_UNLOCK_IMSI_IMEI = "10.178.69.1:16800";
	public static String LDAP_SERVER_TAC_SEARCH = "10.178.69.1:16800";
	public static String LDAP_SERVER_ADD_TAC = "10.178.69.1:16800";
	public static String LDAP_SERVER_ENABLE_MMS = "10.178.69.1:16800";

	/*
	 * //comment for change ip 10.25.72.247 with new IP 10.178.69.1 For CNTDB
	 * module public static String LDAP_SERVER_CNTB = "10.25.72.247:16800";
	 * public static String CNTB_StringUrl = "10.25.72.241:8081"; // For
	 * WebService / HOST
	 */
	/* For CNTDB module */
	public static String LDAP_SERVER_CNTB = "10.178.69.1:16800";
	public static String CNTB_StringUrl = "10.25.72.241:8081"; // For WebService
																// / HOST

	/*
	 * For DUS Update public static String LDAP_SERVER_MSISDN =
	 * "10.25.72.247:16800"; public static String LDAP_SERVER_BAN =
	 * "10.25.72.247:16800";
	 */

	// comment for change ip 10.25.72.247 with new IP 10.178.69.1
	/* For DUS Update */
	public static String LDAP_SERVER_MSISDN = "10.178.69.1:16800";
	public static String LDAP_SERVER_BAN = "10.178.69.1:16800";

	/* For Messaging Module */
	public static String LDAP_SERVER_IAM = "10.169.53.250:389";
	public static String LDAP_SERVER_SMSC = "10.168.208.206:389";
	public static String SERVER_MMSC = "10.169.53.152:8080";
	public static String SMSC_API_URL = "10.168.217.73:8080";
	public static String MMSC_API_URL = "10.169.53.152:8080";

	/* For OTA module */
	public static String OTA_SERVER_URL = "10.160.68.107:9010";

	/* For INSDP module */
	public static String INSDP_API_URL = "10.168.251.148:10010";
	public static String INSDP_USER_PASS = "dXNlcjp1c2Vy";
	public static String INSDP_StringUrl = "10.169.53.250:389";
	public static String INSDP_HOST = "10.25.76.213:10010";

	/* for Gflex Module */
	public static String TELNET_HOST = "10.25.19.162";

	public static int DEFAULT_SERVER = 0;
	/*
	 * finish by Vikas Choudhary
	 */

	// Not In Use
	public static String SERVER_INSDP = "10.168.251.148:10010";
	String stringUrl = "http://10.169.53.250:389";

	public static String INSDP_BASIC_AUTH = "Basic ";
	public static String FILE_PATH = "/opt/Oracle/Middleware/wlserver_12.1/user_projects/domains/vmdd_domain/edat_config.properties";

	public static String MMddyyyy = "MM/dd/yyyy";
	public static String yyyyMMdd = "yyyy-MM-dd";

	private static final Logger logger = Logger.getLogger(Constants.class);

/*
	 * This constant is used in MessagingDataController to compare searchType
	 */

	public static String MSISDN = "msisdn";
	public static String EMAIL = "email";
	public static String USERID = "userid";
	public static String USERNAME = "username";
	public static String CONTRACTID = "contractid";
	public static String IMSI = "imsi";
	
	@PostConstruct
	public void loadProperties() {
		FILE_PATH = ServeletListener.FILE_PATH;
		Properties prop = new Properties();
		InputStream input = null;
		try {
			if (FILE_PATH != null) {
				logger.info("Calling loadProperties, FILE_PATH := " + FILE_PATH);
				input = new FileInputStream(FILE_PATH);
				// load a properties file
				prop.load(input);
				logger.info("FileInputStream is created and loaded into Properties object");
				// get the property value and print it out
				LDAP_SERVER = prop.getProperty("LDAP_SERVER");
				logger.info("LDAP_SERVER := " + LDAP_SERVER);

				LDAP_SERVER_NAP = prop.getProperty("LDAP_SERVER_NAP");
				logger.info("LDAP_SERVER_NAP := " + LDAP_SERVER_NAP);

				LDAP_SERVER_DUS = prop.getProperty("LDAP_SERVER_DUS");
				logger.info("LDAP_SERVER_DUS := " + LDAP_SERVER_DUS);

				LDAP_SERVERL_PCRF = prop.getProperty("LDAP_SERVERL_PCRF");
				logger.info("LDAP_SERVERL_PCRF := " + LDAP_SERVERL_PCRF);

				LDAP_SERVER_UMA = prop.getProperty("LDAP_SERVER_UMA");
				logger.info("LDAP_SERVER_UMA := " + LDAP_SERVER_UMA);

				LDAP_SERVER_HLR = prop.getProperty("LDAP_SERVER_HLR");
				logger.info("LDAP_SERVER_HLR := " + LDAP_SERVER_HLR);

				LDAP_SERVER_EIR = prop.getProperty("LDAP_SERVER_EIR");
				logger.info("LDAP_SERVER_EIR := " + LDAP_SERVER_EIR);

				LDAP_SERVER_HSS = prop.getProperty("LDAP_SERVER_HSS");
				logger.info("LDAP_SERVER_HSS := " + LDAP_SERVER_HSS);

				LDAP_SERVER_PCRF_SESSION = prop
						.getProperty("LDAP_SERVER_PCRF_SESSION");
				logger.info("LDAP_SERVER_PCRF_SESSION := "
						+ LDAP_SERVER_PCRF_SESSION);

				LDAP_SERVER_CNTB = prop.getProperty("LDAP_SERVER_CNTB");
				logger.info("LDAP_SERVER_CNTB := " + LDAP_SERVER_CNTB);

				CNTB_StringUrl = prop.getProperty("CNTB_StringUrl");
				logger.info("CNTB_StringUrl := " + CNTB_StringUrl);

				LDAP_SERVER_MSISDN = prop.getProperty("LDAP_SERVER_MSISDN");
				logger.info("LDAP_SERVER_MSISDN := " + LDAP_SERVER_MSISDN);

				LDAP_SERVER_BAN = prop.getProperty("LDAP_SERVER_BAN");
				logger.info("LDAP_SERVER_BAN := " + LDAP_SERVER_BAN);

				LDAP_SERVER_IAM = prop.getProperty("LDAP_SERVER_IAM");
				logger.info("LDAP_SERVER_IAM := " + LDAP_SERVER_IAM);

				LDAP_SERVER_SMSC = prop.getProperty("LDAP_SERVER_SMSC");
				logger.info("LDAP_SERVER_SMSC := " + LDAP_SERVER_SMSC);

				SERVER_MMSC = prop.getProperty("SERVER_MMSC");
				logger.info("SERVER_MMSC := " + SERVER_MMSC);

				SMSC_API_URL = prop.getProperty("SMSC_API_URL");
				logger.info("SMSC_API_URL := " + SMSC_API_URL);

				MMSC_API_URL = prop.getProperty("MMSC_API_URL");
				logger.info("MMSC_API_URL := " + MMSC_API_URL);

				OTA_SERVER_URL = prop.getProperty("OTA_SERVER_URL");
				logger.info("OTA_SERVER_URL := " + OTA_SERVER_URL);

				INSDP_API_URL = prop.getProperty("INSDP_API_URL");
				logger.info("INSDP_API_URL := " + INSDP_API_URL);

				INSDP_StringUrl = prop.getProperty("INSDP_StringUrl");
				logger.info("INSDP_StringUrl := " + INSDP_StringUrl);

				INSDP_HOST = prop.getProperty("INSDP_HOST");
				logger.info("INSDP_HOST := " + INSDP_HOST);

				TELNET_HOST = prop.getProperty("TELNET_HOST");
				logger.info("TELNET_HOST := " + TELNET_HOST);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			// System.err.println("Error in loading property....! default value loaded");
			logger.info("Error in loading edat_config.properties....! default value loaded");
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * This constant is used in URIMaker for different module requests
	 */
	public static enum RestAPI {

		REST_IAM_TOKEN, REST_IAM_SEARCH, REST_IAM_PRIVACY_VAULT,REST_IAM_LINK_MSISDN,REST_IAM_UNLINK_MSISDN,REST_IAM_GENERATE_PIN,REST_IAM_PROFILE,
		REST_IAM_SEARCH_CONSUMERAPI, REST_IAM_SEARCH_IAMPROVISIONINGAPI, REST_IAM_SEARCH_CONTRACTAPI, REST_IAM_SEARCH_PERMISSIONAPI, REST_IAM_SEARCH_PRIVACYAPI,
		REST_IAM_LOCK,REST_IAM_UNLOCK

	};
	
	/*
	 * HTTP request media type
	 *
	 */
	public static enum RequestMediaType {
		JSON, XML
	};

/*
	 * This constant is used in MessagingDataController to set parameter and call iamSearchAPI
	 */
	public static enum IAMSearchAPIEnum {
		CONSUMER, REBIILION, CONTRACT, PERMISSION, PRIVACY
	};
}
