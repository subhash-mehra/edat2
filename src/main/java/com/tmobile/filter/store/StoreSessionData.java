package com.tmobile.filter.store;

import java.net.URL;

import javax.servlet.http.HttpSession;

public class StoreSessionData {
	
	public StoreSessionData(){
		
	}
	
public static String getHitPath(String url1){
	String hitPath="";
	try {
		URL url = new URL(url1);
		if (url != null && url.getPath() != null) {
			int lastIndex = url.getPath().lastIndexOf("/");
			hitPath = url.getPath().substring(lastIndex+1);	
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	return hitPath;
}
public static boolean storeData(HttpSession session,String hitPath){
	boolean flag=false;
	LinkData linkData = null;
	if(session.getAttribute("linkData")!=null){
		linkData = (LinkData)session.getAttribute("linkData");
	}
	try {
		/* USD Data Retrieval link */
		if(hitPath!=null && hitPath.equalsIgnoreCase("usd")){
			linkData.setUsd(linkData.getUsd()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("nap")){
			linkData.setNap(linkData.getNap()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("dus")){
			linkData.setDus(linkData.getDus()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("pcrf")){
			linkData.setPcrf(linkData.getPcrf()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("uma")){
			linkData.setUma(linkData.getUma()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("hss")){
			linkData.setHss(linkData.getHss()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("hlr")){
			linkData.setHlr(linkData.getHlr()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("eir")){
			linkData.setEir(linkData.getEir()+1);
		}
		
		else if(hitPath!=null && hitPath.equalsIgnoreCase("addEir")){
			linkData.setAddEir(linkData.getAddEir()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("unlockEir")){
			linkData.setUnlockEir(linkData.getUnlockEir()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("searchTac")){
			linkData.setSearchTac(linkData.getSearchTac()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("addtac")){
			linkData.setAddtac(linkData.getAddtac()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("enableMMS")){
			linkData.setEnableMMS(linkData.getEnableMMS()+1);
		}
		
		/* CNTB DB Link */
		else if(hitPath!=null && hitPath.equalsIgnoreCase("cntb")){
			linkData.setCntb(linkData.getCntb()+1);
		}
		else if(hitPath!=null && (hitPath.equalsIgnoreCase("cntbDBSearch") || hitPath.equalsIgnoreCase("msisdn"))){
			linkData.setCntbDBSearch(linkData.getCntbDBSearch()+1);
		}
		else if(hitPath!=null && (hitPath.equalsIgnoreCase("cntbDBAdd") || hitPath.equalsIgnoreCase("cntbDBAdd"))){
			linkData.setCntbDBAdd(linkData.getCntbDBAdd()+1);
		}
		else if(hitPath!=null &&  (hitPath.equalsIgnoreCase("cntbDBDelete") || hitPath.equalsIgnoreCase("existDelete"))){
			linkData.setCntbDBDelete(linkData.getCntbDBDelete()+1);
		}
		
		/* Messaging Data Link */
		else if(hitPath!=null && hitPath.equalsIgnoreCase("mdt")){
			linkData.setMdt(linkData.getMdt()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("smsc")){
			linkData.setSmsc(linkData.getSmsc()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("iam")){
			linkData.setIam(linkData.getIam()+1);
		}
		
		/* DUS Update */
		else if(hitPath!=null && hitPath.equalsIgnoreCase("dusupdate")){
			linkData.setDusupdate(linkData.getDusupdate()+1);
		}
		
		/* GFlex Module Link */
		else if(hitPath!=null && hitPath.equalsIgnoreCase("gflex")){
			linkData.setGflex(linkData.getGflex()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("search")){
			linkData.setSearch(linkData.getSearch()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("update")){
			linkData.setUpdate(linkData.getUpdate()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("delete")){
			linkData.setDelete(linkData.getDelete()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("cleanUp")){
			linkData.setCleanUp(linkData.getCleanUp()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("gFlexReports")){
			linkData.setgFlexReports(linkData.getgFlexReports()+1);
		}
		
		/* IN/SDP module Link */
		else if(hitPath!=null && hitPath.equalsIgnoreCase("insdp")){
			linkData.setInsdp(linkData.getInsdp()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("BalanceAndDate")){
			linkData.setBalanceAndDate(linkData.getBalanceAndDate()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("UpdateService")){
			linkData.setUpdateService(linkData.getUpdateService()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("UpdateBalanceAndDate")){
			linkData.setUpdateBalanceAndDate(linkData.getUpdateBalanceAndDate()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("InstallSubscriber")){
			linkData.setInstallSubscriber(linkData.getInstallSubscriber()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("UpdateAccumulators")){
			linkData.setUpdateAccumulators(linkData.getUpdateAccumulators()+1);
		}		
		else if(hitPath!=null && hitPath.equalsIgnoreCase("UpdateSubscriber")){
			linkData.setUpdateSubscriber(linkData.getUpdateSubscriber()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("ActivateSubscriber")){
			linkData.setActivateSubscriber(linkData.getActivateSubscriber()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("DeleteSubscriber")){
			linkData.setDeleteSubscriber(linkData.getDeleteSubscriber()+1);
		}
		/* OTA module */
		
		else if(hitPath!=null && hitPath.equalsIgnoreCase("otaSearch")){
			linkData.setOtaSearch(linkData.getOtaSearch()+1);
		}
		/* Admin Module Link */
		else if(hitPath!=null && hitPath.equalsIgnoreCase("showAdminTemplate")){
			linkData.setShowAdminTemplate(linkData.getShowAdminTemplate()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("saveUser")){
			linkData.setSaveUser(linkData.getSaveUser()+1);
		}
		else if(hitPath!=null && hitPath.equalsIgnoreCase("showUpdateUser")){
			linkData.setShowUpdateUser(linkData.getShowUpdateUser()+1);
		}
		else if(hitPath!=null && (hitPath.equalsIgnoreCase("updateExistUser") || hitPath.equalsIgnoreCase("updateUser"))){
			linkData.setUpdateUser(linkData.getUpdateUser()+1);
		}
		session.setAttribute("linkData", linkData);
		flag=true;
	} catch (Exception e) {
		// TODO: handle exception
	}
	return flag;
}

}
