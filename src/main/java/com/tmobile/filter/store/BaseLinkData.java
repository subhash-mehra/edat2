package com.tmobile.filter.store;

import java.io.Serializable;

public class BaseLinkData implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String loginId;
	private String messageData;
	private String logoutTime;
	/* USD Data Retrieval link */
	private int usd;
	private int	nap;
	private int	dus;
	private int	pcrf;
	private int	uma;
	private int	hss;
	private int	hlr;
	private int	eir;
	private int	addEir;
	private int	unlockEir;
	private int	searchTac;
	private int	addtac;
	private int	enableMMS;
	
	
	/* CNTB DB Link */
	private int	cntb;
	private int	cntbDBSearch,msisdn;
	private int	cntbDBAdd,existAdd;
	private int	cntbDBDelete,existDelete;
	
	/* Messaging Data Link */
	private int	mdt;
	private int	smsc;
	private int	iam;

	/* DUS Update */
	private int	dusupdate;
	
	/* GFlex Module Link */
	private int	gflex;
	private int	search;
	private int	add;
	private int	update;
	private int	delete;
	private int cleanUp;
	private int	gFlexReports;

	/* IN/SDP module Link */
	private int	insdp;
	private int	BalanceAndDate;
	private int	UpdateService;
	private int	UpdateBalanceAndDate;
	private int	InstallSubscriber;
	private int	UpdateAccumulators;
	private int	UpdateSubscriber;
	private int	ActivateSubscriber;
	private int	DeleteSubscriber;
	
	/* Admin Module Link */
	private int	showAdminTemplate;
	private int	saveUser;
	private int	showUpdateUser;
	private int	updateExistUser,updateUser;
	
	/* OTA module */
	private int otaSearch;

	public int getUsd() {
		return usd;
	}

	public void setUsd(int usd) {
		this.usd = usd;
	}

	public int getNap() {
		return nap;
	}

	public void setNap(int nap) {
		this.nap = nap;
	}

	public int getDus() {
		return dus;
	}

	public void setDus(int dus) {
		this.dus = dus;
	}

	public int getPcrf() {
		return pcrf;
	}

	public void setPcrf(int pcrf) {
		this.pcrf = pcrf;
	}

	public int getUma() {
		return uma;
	}

	public void setUma(int uma) {
		this.uma = uma;
	}

	public int getHss() {
		return hss;
	}

	public void setHss(int hss) {
		this.hss = hss;
	}

	public int getHlr() {
		return hlr;
	}

	public void setHlr(int hlr) {
		this.hlr = hlr;
	}

	public int getEir() {
		return eir;
	}

	public void setEir(int eir) {
		this.eir = eir;
	}

	public int getCntb() {
		return cntb;
	}

	public void setCntb(int cntb) {
		this.cntb = cntb;
	}

	public int getCntbDBSearch() {
		return cntbDBSearch;
	}

	public void setCntbDBSearch(int cntbDBSearch) {
		this.cntbDBSearch = cntbDBSearch;
	}

	public int getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(int msisdn) {
		this.msisdn = msisdn;
	}

	public int getCntbDBAdd() {
		return cntbDBAdd;
	}

	public void setCntbDBAdd(int cntbDBAdd) {
		this.cntbDBAdd = cntbDBAdd;
	}

	public int getExistAdd() {
		return existAdd;
	}

	public void setExistAdd(int existAdd) {
		this.existAdd = existAdd;
	}

	public int getCntbDBDelete() {
		return cntbDBDelete;
	}

	public void setCntbDBDelete(int cntbDBDelete) {
		this.cntbDBDelete = cntbDBDelete;
	}

	public int getExistDelete() {
		return existDelete;
	}

	public void setExistDelete(int existDelete) {
		this.existDelete = existDelete;
	}

	public int getMdt() {
		return mdt;
	}

	public void setMdt(int mdt) {
		this.mdt = mdt;
	}

	public int getSmsc() {
		return smsc;
	}

	public void setSmsc(int smsc) {
		this.smsc = smsc;
	}

	public int getIam() {
		return iam;
	}

	public void setIam(int iam) {
		this.iam = iam;
	}

	public int getDusupdate() {
		return dusupdate;
	}

	public void setDusupdate(int dusupdate) {
		this.dusupdate = dusupdate;
	}

	public int getGflex() {
		return gflex;
	}

	public void setGflex(int gflex) {
		this.gflex = gflex;
	}

	public int getSearch() {
		return search;
	}

	public void setSearch(int search) {
		this.search = search;
	}

	public int getAdd() {
		return add;
	}

	public void setAdd(int add) {
		this.add = add;
	}

	public int getUpdate() {
		return update;
	}

	public void setUpdate(int update) {
		this.update = update;
	}

	public int getDelete() {
		return delete;
	}

	public void setDelete(int delete) {
		this.delete = delete;
	}

	public int getgFlexReports() {
		return gFlexReports;
	}

	public void setgFlexReports(int gFlexReports) {
		this.gFlexReports = gFlexReports;
	}

	public int getInsdp() {
		return insdp;
	}

	public void setInsdp(int insdp) {
		this.insdp = insdp;
	}

	public int getBalanceAndDate() {
		return BalanceAndDate;
	}

	public void setBalanceAndDate(int balanceAndDate) {
		BalanceAndDate = balanceAndDate;
	}

	public int getUpdateService() {
		return UpdateService;
	}

	public void setUpdateService(int updateService) {
		UpdateService = updateService;
	}

	public int getUpdateBalanceAndDate() {
		return UpdateBalanceAndDate;
	}

	public void setUpdateBalanceAndDate(int updateBalanceAndDate) {
		UpdateBalanceAndDate = updateBalanceAndDate;
	}

	public int getInstallSubscriber() {
		return InstallSubscriber;
	}

	public void setInstallSubscriber(int installSubscriber) {
		InstallSubscriber = installSubscriber;
	}

	public int getUpdateAccumulators() {
		return UpdateAccumulators;
	}

	public void setUpdateAccumulators(int updateAccumulators) {
		UpdateAccumulators = updateAccumulators;
	}

	public int getUpdateSubscriber() {
		return UpdateSubscriber;
	}

	public void setUpdateSubscriber(int updateSubscriber) {
		UpdateSubscriber = updateSubscriber;
	}

	public int getActivateSubscriber() {
		return ActivateSubscriber;
	}

	public void setActivateSubscriber(int activateSubscriber) {
		ActivateSubscriber = activateSubscriber;
	}

	public int getDeleteSubscriber() {
		return DeleteSubscriber;
	}

	public void setDeleteSubscriber(int deleteSubscriber) {
		DeleteSubscriber = deleteSubscriber;
	}

	public int getShowAdminTemplate() {
		return showAdminTemplate;
	}

	public void setShowAdminTemplate(int showAdminTemplate) {
		this.showAdminTemplate = showAdminTemplate;
	}

	public int getSaveUser() {
		return saveUser;
	}

	public void setSaveUser(int saveUser) {
		this.saveUser = saveUser;
	}

	public int getShowUpdateUser() {
		return showUpdateUser;
	}

	public void setShowUpdateUser(int showUpdateUser) {
		this.showUpdateUser = showUpdateUser;
	}

	public int getUpdateExistUser() {
		return updateExistUser;
	}

	public void setUpdateExistUser(int updateExistUser) {
		this.updateExistUser = updateExistUser;
	}

	public int getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(int updateUser) {
		this.updateUser = updateUser;
	}

	public int getCleanUp() {
		return cleanUp;
	}

	public void setCleanUp(int cleanUp) {
		this.cleanUp = cleanUp;
	}

	public int getOtaSearch() {
		return otaSearch;
	}

	public void setOtaSearch(int otaSearch) {
		this.otaSearch = otaSearch;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMessageData() {
		return messageData;
	}

	public void setMessageData(String messageData) {
		this.messageData = messageData;
	}

	public String getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(String logoutTime) {
		this.logoutTime = logoutTime;
	}

		
	public int getAddEir() {
		return addEir;
	}

	public void setAddEir(int addEir) {
		this.addEir = addEir;
	}

	public int getUnlockEir() {
		return unlockEir;
	}

	public void setUnlockEir(int unlockEir) {
		this.unlockEir = unlockEir;
	}

	public int getSearchTac() {
		return searchTac;
	}

	public void setSearchTac(int searchTac) {
		this.searchTac = searchTac;
	}

	public int getAddtac() {
		return addtac;
	}

	public void setAddtac(int addtac) {
		this.addtac = addtac;
	}

	public int getEnableMMS() {
		return enableMMS;
	}

	public void setEnableMMS(int enableMMS) {
		this.enableMMS = enableMMS;
	}

	@Override
    public String toString() {
        return "{\"usd\":"+usd+", \"nap\":"+ nap+ ", \"dus\":"+dus+", \"pcrf\":"+pcrf+", \"uma\":"+uma+
        		", \"hss\":"+hss+", \"hlr\":"+hlr+", \"eir\":"+eir+",\"cntb\":"+cntb+", \"cntbDBSearch\":"+cntbDBSearch+", \"msisdn\":"+msisdn+", \"cntbDBAdd\":"+cntbDBAdd
        		+ ", \"existAdd\":"+existAdd+", \"cntbDBDelete\":"+cntbDBDelete+", \"existDelete\":"+existDelete+", \"mdt\":"+mdt+", \"smsc\":"+smsc+", \"iam\":"+iam+", "
        						+ "\"dusupdate\":"+dusupdate+", \"gflex\":"+gflex+", \"search\":"+search+""
        						+ ", \"add\":"+add+", \"update\":"+update+", \"delete_\":"+delete+", \"gFlexReports\":"+gFlexReports+", \"insdp\":"+insdp+", \"BalanceAndDate\":"+BalanceAndDate
        						+ ", \"UpdateService\":"+UpdateService+", \"UpdateBalanceAndDate\":"+UpdateBalanceAndDate+", \"InstallSubscriber\":"+InstallSubscriber+", \"UpdateAccumulators\":"+UpdateAccumulators
        						+ ", \"UpdateSubscriber\":"+UpdateSubscriber+", \"ActivateSubscriber\":"+ActivateSubscriber+", \"DeleteSubscriber\":"+DeleteSubscriber+", \"showAdminTemplate\":"+showAdminTemplate
        						+ ", \"saveUser\":"+saveUser+", \"showUpdateUser\":"+showUpdateUser+", \"updateExistUser\":"+updateExistUser+", \"updateUser\":"+updateUser+", \"cleanUp\":"+cleanUp+", \"otaSearch\":"+otaSearch+",\"addEir\":"+addEir+",\"unlockEir\":"+unlockEir+",\"searchTac\":"+searchTac+",\"addtac\":"+addtac+",\"enableMMS\":"+enableMMS+"}";
    }

	
}
