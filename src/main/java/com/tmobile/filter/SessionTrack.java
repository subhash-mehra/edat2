package com.tmobile.filter;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.tmobile.filter.store.StoreSessionData;

/**
 * Servlet Filter implementation class SessionTrack
 */
public class SessionTrack implements Filter {

	/**
	 * Default constructor.
	 */
	public SessionTrack() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain
		if (request instanceof HttpServletRequest) {
			try {
				String url1 = ((HttpServletRequest) request).getRequestURL().toString();
				StoreSessionData.storeData(((HttpServletRequest) request).getSession(), StoreSessionData.getHitPath(url1));			
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
