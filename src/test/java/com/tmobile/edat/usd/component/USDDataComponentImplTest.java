package com.tmobile.edat.usd.component;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.tmobile.edat.test.base.BaseTestClass;
import com.tmobile.edat.usd.dto.request.UsdSearchRequest;
import com.tmobile.edat.usd.dto.response.NapDataResponse;
import com.tmobile.edat.util.UserException;

public class USDDataComponentImplTest extends BaseTestClass{
	
	//@Autowired
    //@Qualifier("USDDataComponent")
    USDDataComponent usdDataComponent = null;

	//@Test
	public void testGetNapData() throws UserException {		
		// By CustomerId
        UsdSearchRequest usdSearchRequest = new UsdSearchRequest();
        usdSearchRequest.setCustomerId("4254444604");
        NapDataResponse napDataResponse = usdDataComponent.getNapData(usdSearchRequest);		
        assertNotNull("NapDataResponse is not null.", napDataResponse);
        
        logger.info("NapDataResponse == "+napDataResponse);
	}

	@Test
	public void testGetDUSData() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetPCRFData() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetUMAData() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetHSSData() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetHLRData() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetEIR() {
		//fail("Not yet implemented");
	}

}
